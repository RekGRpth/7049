.class public Lcom/android/soundrecorder/SoundRecorder;
.super Landroid/app/Activity;
.source "SoundRecorder.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/soundrecorder/Recorder$OnStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;
    }
.end annotation


# static fields
.field private static final AUDIO_3GPP:Ljava/lang/String; = "audio/3gpp"

.field private static final AUDIO_AAC:Ljava/lang/String; = "audio/aac"

.field private static final AUDIO_AMR:Ljava/lang/String; = "audio/amr"

.field private static final AUDIO_AWB:Ljava/lang/String; = "audio/awb"

.field private static final AUDIO_NOT_LIMIT_TYPE:Ljava/lang/String; = "audio/*"

.field private static final AUDIO_OGG:Ljava/lang/String; = "application/ogg"

.field private static final AUDIO_VORBIS:Ljava/lang/String; = "audio/vorbis"

.field public static final BITRATE_AAC:I = 0x1f400

.field public static final BITRATE_AMR:I = 0x2fa8

.field public static final BITRATE_AWB:I = 0x6f54

.field public static final BITRATE_VORBIS:I = 0x1f400

.field private static final BIT_RATE:I = 0x8

.field private static final BYTE_BASE:I = 0x400

.field private static final BYTE_PER_SEC:I = 0x640

.field private static final BYTE_RATE:Ljava/lang/String; = "byte_rate"

.field private static final CANNOT_STAT_ERROR:J = -0x2L

.field public static final DIALOG_DB_ERROR:I = 0x2

.field public static final DIALOG_RECORD_ERROR:I = 0x3

.field public static final DIALOG_SELECT_FORMAT:I = 0x1

.field public static final DIALOG_SELECT_MODE:I = 0x0

.field private static final DIALOG_TAG_SELECT_FORMAT:Ljava/lang/String; = "SelectFormat"

.field private static final DIALOG_TAG_SELECT_MODE:Ljava/lang/String; = "SelectMode"

.field public static final DOWHAT:Ljava/lang/String; = "dowhat"

.field private static final DURATION:Ljava/lang/String; = "duration"

.field private static final ERROR:Ljava/lang/String; = "error"

.field private static final EXTRA_MAX_BYTES:Ljava/lang/String; = "android.provider.MediaStore.extra.MAX_BYTES"

.field private static final FACTOR_FOR_SECOND_AND_MINUTE:J = 0x3e8L

.field private static final FORMAT_HIGH:I = 0x0

.field private static final FORMAT_LOW:I = 0x2

.field private static final FORMAT_MID:I = 0x1

.field private static final HAVE_AACENCODE_FEATURE:Z = true

.field private static final HAVE_AWBENCODE_FEATURE:Z = true

.field private static final HAVE_VORBISENC_FEATURE:Z = true

.field public static final INIT:Ljava/lang/String; = "init"

.field private static final INTENT_ACTION_MAIN:Ljava/lang/String; = "android.intent.action.MAIN"

.field public static final LOW_STORAGE_THRESHOLD:J = 0x200000L

.field private static final MAX_FILE_SIZE_KEY:Ljava/lang/String; = "max_file_size"

.field private static final MAX_FILE_SIZE_NULL:J = -0x1L

.field private static final MODE_INDOOR:I = 0x1

.field private static final MODE_NORMAL:I = 0x0

.field private static final MODE_OUTDOOR:I = 0x2

.field private static final MTK_AUDIO_HD_REC_SUPPORT:Z = true

.field private static final NOT_LIMIT_TYPE:Ljava/lang/String; = "*/*"

.field public static final NOT_SAVED_KEY:Ljava/lang/String; = "not_saved"

.field private static final NO_STORAGE_ERROR:J = -0x1L

.field private static final OPTIONMENU_SELECT_FORMAT:I = 0x0

.field private static final OPTIONMENU_SELECT_MODE:I = 0x1

.field private static final PATH:Ljava/lang/String; = "path"

.field public static final PLAY:Ljava/lang/String; = "play"

.field private static final PLAYLIST_ID_NULL:I = -0x1

.field public static final RECORD:Ljava/lang/String; = "record"

.field private static final RECORDER_STATE_KEY:Ljava/lang/String; = "recorder_state"

.field private static final REQURST_FILE_LIST:I = 0x1

.field private static final SAMPLE_INTERRUPTED_KEY:Ljava/lang/String; = "sample_interrupted"

.field public static final SAMPLE_RATE_AAC:I = 0xbb80

.field public static final SAMPLE_RATE_AMR:I = 0x1f40

.field public static final SAMPLE_RATE_AWB:I = 0x3e80

.field public static final SAMPLE_RATE_VORBIS:I = 0xbb80

.field private static final SELECTED_RECORDING_FORMAT:Ljava/lang/String; = "selected_recording_format"

.field private static final SELECTED_RECORDING_MODE:Ljava/lang/String; = "selected_recording_mode"

.field private static final SOUND_RECORDER_DATA:Ljava/lang/String; = "sound_recorder_data"

.field private static final SOUND_RECORDER_DURATION:Ljava/lang/String; = "sound_recorder_duration"

.field private static final STORAGE_STATUS_LOW:I = 0x1

.field private static final STORAGE_STATUS_NONE:I = 0x2

.field private static final STORAGE_STATUS_OK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SR/SoundRecorder"

.field private static final THREE_BUTTON_WEIGHT_SUM:I = 0x3

.field private static final THREE_MINS_IN_SECONDS:I = 0xb4

.field private static final TIMER_VIEW_TEXT_SIZE_BIG:I = 0x5a

.field private static final TIMER_VIEW_TEXT_SIZE_SMALL:I = 0x46

.field private static final TIME_BASE:I = 0x3c

.field private static final TIME_NINE_MIN:I = 0x21c

.field private static final TIME_THREE_MIN:I = 0xb4

.field private static final TOTAL_RECORDING_TIME_NULL:J = -0x1L

.field private static final TO_BE_DELETED_FILEPATH_KEY:Ljava/lang/String; = "to_be_deleted_file_path"

.field private static final TWO_BUTTON_WEIGHT_SUM:I = 0x2

.field private static final UPDATE_TIMER_VIRE_POST_DELAYED:I = 0x32


# instance fields
.field private mAcceptButton:Landroid/widget/Button;

.field private mActivityForeground:Z

.field private mButtonParent:Landroid/widget/LinearLayout;

.field private mByteRate:I

.field private mConfigChangeRunFromApp:Z

.field private mConnection:Landroid/media/MediaScannerConnection;

.field private mDiscardButton:Landroid/widget/Button;

.field private mDoWhat:Ljava/lang/String;

.field private mErrorTextView:Landroid/widget/TextView;

.field private mErrorUiMessage:Ljava/lang/String;

.field private mExitButtons:Landroid/widget/LinearLayout;

.field private mFileListButton:Landroid/widget/ImageButton;

.field private mFileName:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mHasFileSizeLimitation:Z

.field private mIsDeleted:Z

.field private mIsSDCardFull:Z

.field private mIsSDCardPlugOut:Z

.field private mIsSDCardmounted:Z

.field private mMaxFileSize:J

.field private mMimeType:Ljava/lang/String;

.field private mOptionsMenu:Landroid/view/Menu;

.field private mPauseRecordingButton:Landroid/widget/ImageButton;

.field private mPlayButton:Landroid/widget/ImageButton;

.field private mPlayingStateImageView:Landroid/widget/ImageView;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mRecordButton:Landroid/widget/ImageButton;

.field private mRecordParamsExtension:Ljava/lang/String;

.field private mRecordParamsOutputFileFormat:I

.field private mRecordParamsRecordingType:I

.field private mRecorder:Lcom/android/soundrecorder/Recorder;

.field private mRecorderErrorListener:Landroid/content/DialogInterface$OnClickListener;

.field private mRecordingFile:Ljava/io/File;

.field private mRecordingFileNameTextView:Landroid/widget/TextView;

.field private mRecordingFilePath:Ljava/lang/String;

.field private mRecordingFinishedRunFromApp:Z

.field private mRecordingLeftTime:J

.field private mRecordingStateImageView:Landroid/widget/ImageView;

.field private mRecordingTimeLimitation:J

.field private mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

.field private mRequestedType:Ljava/lang/String;

.field private mRetainNonConfigChangeHasRun:Z

.field private mRunFromLauncher:Z

.field private mSDCardMountEventReceiver:Landroid/content/BroadcastReceiver;

.field private mSampleInterrupted:Z

.field private final mSaveErrorHandler:Landroid/os/Handler;

.field private final mSelectFormatListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSelectedFormat:I

.field private mSelectedMode:I

.field private mStartRecordingTime:J

.field private mStateProgressBar:Landroid/widget/ProgressBar;

.field private mStateTextView:Landroid/widget/TextView;

.field private mStopButton:Landroid/widget/ImageButton;

.field private mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mStorageStatus:I

.field private mSuffixArray:[Ljava/lang/CharSequence;

.field private mSuspended:Z

.field private mTimerFormat:Ljava/lang/String;

.field private mTimerTextView:Landroid/widget/TextView;

.field private mTotalRecordingTime:J

.field private final mUpdateTimer:Ljava/lang/Runnable;

.field private mVUMeter:Lcom/android/soundrecorder/VUMeter;

.field private mVoiceQualityFormatArray:[I

.field private mVoiceQualityStringIDArray:[I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x1

    const-wide/16 v4, -0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v0, "audio/*"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    const-string v0, "audio/*"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsExtension:Ljava/lang/String;

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsOutputFileFormat:I

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsRecordingType:I

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRetainNonConfigChangeHasRun:Z

    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mActivityForeground:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsDeleted:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardmounted:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardFull:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuspended:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFinishedRunFromApp:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mConfigChangeRunFromApp:Z

    const-wide/16 v0, 0xb4

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingTimeLimitation:J

    iput-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    iput-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStartRecordingTime:J

    iput-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityStringIDArray:[I

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuffixArray:[Ljava/lang/CharSequence;

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityFormatArray:[I

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSDCardMountEventReceiver:Landroid/content/BroadcastReceiver;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mConnection:Landroid/media/MediaScannerConnection;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFile:Ljava/io/File;

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$1;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$1;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectFormatListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$2;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$2;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$3;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$3;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorderErrorListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$4;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$4;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mUpdateTimer:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/soundrecorder/SoundRecorder$5;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/SoundRecorder$5;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSaveErrorHandler:Landroid/os/Handler;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/android/soundrecorder/SoundRecorder;)V
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateTimerView()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/soundrecorder/SoundRecorder;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/soundrecorder/SoundRecorder;)V
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->deleteRecordingFile()V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/soundrecorder/SoundRecorder;)Z
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/android/soundrecorder/SoundRecorder;)Lcom/android/soundrecorder/RemainingTimeCalculator;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/soundrecorder/SoundRecorder;)J
    .locals 2
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-wide v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/android/soundrecorder/SoundRecorder;J)J
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/android/soundrecorder/SoundRecorder;Ljava/io/File;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/android/soundrecorder/SoundRecorder;->addToMediaDB(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/soundrecorder/SoundRecorder;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/android/soundrecorder/SoundRecorder;->afterSave(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/soundrecorder/SoundRecorder;)Lcom/android/soundrecorder/Recorder;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/soundrecorder/SoundRecorder;)Z
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mActivityForeground:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/soundrecorder/SoundRecorder;)Z
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    return v0
.end method

.method static synthetic access$402(Lcom/android/soundrecorder/SoundRecorder;Z)Z
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    return p1
.end method

.method static synthetic access$502(Lcom/android/soundrecorder/SoundRecorder;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/android/soundrecorder/SoundRecorder;Z)Z
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/soundrecorder/SoundRecorder;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/soundrecorder/SoundRecorder;->removeOldFragmentByTag(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$802(Lcom/android/soundrecorder/SoundRecorder;Z)Z
    .locals 0
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardmounted:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/soundrecorder/SoundRecorder;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/soundrecorder/SoundRecorder;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    return-object v0
.end method

.method private addToMediaDB(Ljava/io/File;)Landroid/net/Uri;
    .locals 21
    .param p1    # Ljava/io/File;

    const-string v17, "SR/SoundRecorder"

    const-string v18, "<addToMediaDB> begin"

    invoke-static/range {v17 .. v18}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string v17, "SR/SoundRecorder"

    const-string v18, "<addToMediaDB> file is null, return null"

    invoke-static/range {v17 .. v18}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    :goto_0
    return-object v12

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v13

    if-eqz v13, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v17

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/soundrecorder/SoundRecorderUtils;->deleteFileFromMediaDB(Landroid/content/Context;Ljava/lang/String;)Z

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v5, v6}, Ljava/util/Date;-><init>(J)V

    new-instance v14, Ljava/text/SimpleDateFormat;

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f070033

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v14, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    const/4 v15, 0x6

    new-instance v7, Landroid/content/ContentValues;

    const/16 v17, 0x6

    move/from16 v0, v17

    invoke-direct {v7, v0}, Landroid/content/ContentValues;-><init>(I)V

    const-string v17, "is_music"

    const-string v18, "0"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v17, "title"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v17, "date_added"

    const-wide/16 v18, 0x3e8

    div-long v18, v5, v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v17, "SR/SoundRecorder"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "<addToMediaDB> File type is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/soundrecorder/SRLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v17, "mime_type"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v17, "artist"

    const/high16 v18, 0x7f070000

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v17, "album"

    const v18, 0x7f07002e

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v17, "_data"

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v17, "duration"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v17, "SR/SoundRecorder"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "<addToMediaDB> Reocrding time output to database is :DURATION= "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    move-wide/from16 v19, v0

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/soundrecorder/SRLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFile:Ljava/io/File;

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v4, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v17, "SR/SoundRecorder"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "<addToMediaDB> ContentURI: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/soundrecorder/SRLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    :try_start_0
    invoke-virtual {v11, v4, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    :goto_1
    if-nez v12, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorder;->mSaveErrorHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :catch_0
    move-exception v9

    const-string v17, "SR/SoundRecorder"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "<addToMediaDB> Save in DB failed: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v9}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string v17, "SR/SoundRecorder"

    const-string v18, "<addToMediaDB> Save susceeded in DB"

    invoke-static/range {v17 .. v18}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/android/soundrecorder/SoundRecorder;->getPlaylistId(Landroid/content/res/Resources;)I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/android/soundrecorder/SoundRecorder;->createPlaylist(Landroid/content/res/Resources;Landroid/content/ContentResolver;)Landroid/net/Uri;

    :cond_3
    invoke-virtual {v12}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/android/soundrecorder/SoundRecorder;->getPlaylistId(Landroid/content/res/Resources;)I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/android/soundrecorder/SoundRecorder;->getPlaylistId(Landroid/content/res/Resources;)I

    move-result v17

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v17

    invoke-direct {v0, v11, v3, v1, v2}, Lcom/android/soundrecorder/SoundRecorder;->addToPlaylist(Landroid/content/ContentResolver;IJ)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/soundrecorder/SoundRecorder;->mConnection:Landroid/media/MediaScannerConnection;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/media/MediaScannerConnection;->connect()V

    goto/16 :goto_0
.end method

.method private addToPlaylist(Landroid/content/ContentResolver;IJ)V
    .locals 11
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # I
    .param p3    # J

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "count(*)"

    aput-object v3, v2, v0

    const-string v0, "external"

    invoke-static {v0, p3, p4}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_1

    const-string v0, "SR/SoundRecorder"

    const-string v3, "<addToPlaylist> cursor is null"

    invoke-static {v0, v3}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    new-instance v10, Landroid/content/ContentValues;

    const/4 v0, 0x2

    invoke-direct {v10, v0}, Landroid/content/ContentValues;-><init>(I)V

    const-string v0, "play_order"

    add-int v3, v6, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "audio_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v9, 0x0

    :try_start_0
    invoke-virtual {p1, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    :goto_1
    if-nez v9, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSaveErrorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :catch_0
    move-exception v8

    const-string v0, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<addToPlaylist> insert in DB failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x0

    goto :goto_1
.end method

.method private afterSave(Landroid/net/Uri;)V
    .locals 8
    .param p1    # Landroid/net/Uri;

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Performance test][SoundRecorder] recording save end ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<afterSave> start"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->finish()V

    if-eqz p1, :cond_0

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<afterSave> set Result as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v3, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :cond_0
    iget-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDiscardButton:Landroid/widget/Button;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    iput-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    iget-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsDeleted:Z

    if-nez v2, :cond_1

    if-eqz p1, :cond_1

    const v2, 0x7f070001

    invoke-static {p0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_1
    :goto_0
    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsDeleted:Z

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<afterSave> set mRecordingFilePath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v3, 0x0

    iput v3, v2, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<afterSave> end"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method private createPlaylist(Landroid/content/res/Resources;Landroid/content/ContentResolver;)Landroid/net/Uri;
    .locals 6
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Landroid/content/ContentResolver;

    new-instance v0, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "name"

    const v4, 0x7f07002f

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Audio$Playlists;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSaveErrorHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-object v2

    :catch_0
    move-exception v1

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<createPlaylist> insert in DB failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private deleteRecordingFile()V
    .locals 4

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<deleteRecordingFile> delete file success"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<deleteRecordingFile> set mRecordingFilePath = null"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "SR/SoundRecorder"

    const-string v2, "<deleteRecordingFile> delete file fail"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<deleteRecordingFile> file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not exist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getAvailableStorage()J
    .locals 8

    :try_start_0
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-static {}, Landroid/os/storage/StorageManager;->getDefaultPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "mounted"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-static {}, Landroid/os/storage/StorageManager;->getDefaultPath()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    :goto_0
    return-wide v4

    :cond_1
    const-wide/16 v4, -0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-wide/16 v4, -0x2

    goto :goto_0
.end method

.method private getPlaylistId(Landroid/content/res/Resources;)I
    .locals 10
    .param p1    # Landroid/content/res/Resources;

    const/4 v5, 0x1

    const/4 v3, 0x0

    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Audio$Playlists;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v3

    const-string v9, "name=?"

    new-array v4, v5, [Ljava/lang/String;

    const v0, 0x7f07002f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "name=?"

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/android/soundrecorder/SoundRecorderUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v8, -0x1

    if-nez v6, :cond_2

    :try_start_0
    const-string v0, "SR/SoundRecorder"

    const-string v3, "<getPlaylistId> query returns null"

    invoke-static {v0, v3}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    return v8

    :cond_2
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    goto :goto_0

    :catch_0
    move-exception v7

    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private getStorageStatus(Z)I
    .locals 7
    .param p1    # Z

    const-wide/16 v2, -0x1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->getAvailableStorage()J

    move-result-wide v0

    :goto_0
    const-string v4, "SR/SoundRecorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<getStorageStatus> remaining storate is :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    const/4 v2, 0x2

    :goto_1
    return v2

    :cond_0
    move-wide v0, v2

    goto :goto_0

    :cond_1
    const-wide/32 v2, 0x200000

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private initFromIntent()Z
    .locals 14

    const-wide/16 v12, 0x640

    const-wide/16 v10, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v4, "audio/amr"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "audio/3gpp"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "audio/*"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "*/*"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    iput-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    :cond_1
    const-string v4, "android.provider.MediaStore.extra.MAX_BYTES"

    invoke-virtual {v2, v4, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v4}, Lcom/android/soundrecorder/RemainingTimeCalculator;->diskSpaceRemaining()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-ltz v4, :cond_4

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    :goto_0
    iput-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    const-string v4, "SR/SoundRecorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<initFromIntent> mMaxFileSize = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    cmp-long v4, v4, v10

    if-eqz v4, :cond_5

    move v4, v6

    :goto_1
    iput-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    cmp-long v4, v4, v12

    if-lez v4, :cond_2

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    const-wide/16 v7, 0xc80

    sub-long/2addr v4, v7

    div-long/2addr v4, v12

    iput-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingTimeLimitation:J

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingTimeLimitation:J

    iput-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    :cond_2
    move v7, v6

    :goto_2
    return v7

    :cond_3
    const-string v4, "SR/SoundRecorder"

    const-string v5, "<initFromIntent> return false"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    goto :goto_0

    :cond_5
    move v4, v7

    goto :goto_1
.end method

.method private initFromLastNonConfigurationInstance()V
    .locals 6

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<initFromLastNon>"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    if-nez v0, :cond_1

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<initFromLastNon> myIcycle == null"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "recorder_state"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2, v1}, Lcom/android/soundrecorder/Recorder;->restoreState(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    const-string v2, "sample_interrupted"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    const-string v2, "max_file_size"

    const-wide/16 v3, -0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<initFromLastNon> mMaxFileSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<initFromLastNon> byteRate got is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "to_be_deleted_file_path"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "to_be_deleted_file_path"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->deleteRecordingFile()V

    :cond_2
    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    mul-int/lit8 v3, v3, 0x8

    invoke-virtual {v2, v3}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    :cond_3
    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<initFromLastNon> Byte rate in onCreate() is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private initRecorder()V
    .locals 3

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    new-instance v1, Lcom/android/soundrecorder/Recorder;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-direct {v1, v2, v0}, Lcom/android/soundrecorder/Recorder;-><init>(Landroid/os/storage/StorageManager;Landroid/media/AudioManager;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1, p0}, Lcom/android/soundrecorder/Recorder;->setOnStateChangedListener(Lcom/android/soundrecorder/Recorder$OnStateChangedListener;)V

    new-instance v1, Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-direct {v1, v2}, Lcom/android/soundrecorder/RemainingTimeCalculator;-><init>(Landroid/os/storage/StorageManager;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    return-void
.end method

.method private initResourceRefs()V
    .locals 2

    const/16 v1, 0x8

    const v0, 0x7f090014

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    const v0, 0x7f090017

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    const v0, 0x7f090016

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    const v0, 0x7f090018

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    const v0, 0x7f090015

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    const v0, 0x7f090013

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09000b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    const v0, 0x7f09000d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    const v0, 0x7f09000c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    const v0, 0x7f09000e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    const v0, 0x7f090006

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    const v0, 0x7f090008

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    const v0, 0x7f090009

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    const v0, 0x7f09000f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    const v0, 0x7f090011

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    const v0, 0x7f090010

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDiscardButton:Landroid/widget/Button;

    const v0, 0x7f090012

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/soundrecorder/VUMeter;

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDiscardButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070037

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/VUMeter;->setRecorder(Lcom/android/soundrecorder/Recorder;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private judgeStorageBeforeRecord()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v3}, Lcom/android/soundrecorder/RemainingTimeCalculator;->reset()V

    iget-wide v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v4}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setFileSizeLimit(Ljava/io/File;J)V

    :cond_0
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-static {}, Landroid/os/storage/StorageManager;->getDefaultPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v3, "mounted"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<judgeStorageBeforeRecord>storageState.equals(Environment.MEDIA_MOUNTED) == false"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardmounted:Z

    const/4 v2, 0x4

    invoke-static {p0, v2}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->delete()V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v2}, Lcom/android/soundrecorder/RemainingTimeCalculator;->reset()V

    :goto_0
    return v1

    :cond_1
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v3, v1}, Lcom/android/soundrecorder/RemainingTimeCalculator;->timeRemaining(Z)J

    move-result-wide v3

    const-wide/16 v5, 0x2

    cmp-long v3, v3, v5

    if-gtz v3, :cond_2

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<judgeStorageBeforeRecord> space is not enough"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardFull:Z

    const/4 v2, 0x2

    invoke-static {p0, v2}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v2}, Lcom/android/soundrecorder/RemainingTimeCalculator;->reset()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v1}, Lcom/android/soundrecorder/RemainingTimeCalculator;->reset()V

    move v1, v2

    goto :goto_0
.end method

.method private recordRunFromOthers()V
    .locals 5

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<recordRunFromOthers> in mms start branch"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mUpdateTimer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<recordRunFromOthers> delete file ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-wide v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingTimeLimitation:J

    iput-wide v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mConfigChangeRunFromApp:Z

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    const/16 v2, 0x2fa8

    invoke-virtual {v1, v2}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    const-string v1, "audio/amr"

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    const/4 v2, 0x3

    const/4 v3, 0x1

    const-string v4, ".amr"

    invoke-virtual {v1, v2, v3, v4, p0}, Lcom/android/soundrecorder/Recorder;->startRecording(IILjava/lang/String;Landroid/content/Context;)V

    return-void
.end method

.method private registerExternalStorageListener()V
    .locals 3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSDCardMountEventReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/soundrecorder/SoundRecorder$6;

    invoke-direct {v1, p0}, Lcom/android/soundrecorder/SoundRecorder$6;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSDCardMountEventReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSDCardMountEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<registerExternalStorageListener> registerReceiver"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private releaseWakeLock()V
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<releaseWakeLock>"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private removeOldFragmentByTag(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/app/DialogFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    return-void
.end method

.method private saveSample()V
    .locals 5

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onResume> begin"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onResume> mRecorder.sampleLength() == 0, return"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->sampleFileDelSuffix()V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->getRecordingFilePath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<saveSample> set mRecordingFilePath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<saveSample> mRecorder.sampleFile() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v4}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;

    invoke-direct {v1, p0}, Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;-><init>(Lcom/android/soundrecorder/SoundRecorder;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onResume> end"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<onResume> catch exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setRecordRelatedParams()V
    .locals 7

    const v6, 0x1f400

    const/16 v5, 0x6f54

    const/16 v4, 0x2fa8

    const/4 v3, 0x3

    const/4 v2, 0x1

    const-string v0, "audio/amr"

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v0, v4}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordRelatedParams> setBitRate(BITRATE_AMR);"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "audio/amr"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsOutputFileFormat:I

    iput v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsRecordingType:I

    const-string v0, ".amr"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsExtension:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "audio/awb"

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v0, v5}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordRelatedParams> setBitRate(BITRATE_AWB);"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "audio/awb"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    iput v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsOutputFileFormat:I

    iput v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsRecordingType:I

    const-string v0, ".awb"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsExtension:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "audio/aac"

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v0, v6}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordRelatedParams> setBitRate(BITRATE_AAC);"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "audio/aac"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsOutputFileFormat:I

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsRecordingType:I

    const-string v0, ".aac"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsExtension:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "audio/3gpp"

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "audio/vorbis"

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordRelatedParams> mSelectedFormat is out of range"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v0, v6}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordRelatedParams> setBitRate(BITRATE_VORBIS);"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "application/ogg"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsOutputFileFormat:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsRecordingType:I

    const-string v0, ".ogg"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsExtension:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v0, v5}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordRelatedParams> setBitRate(BITRATE_AWB);"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "audio/3gpp"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    iput v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsOutputFileFormat:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsRecordingType:I

    const-string v0, ".3gpp"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsExtension:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v0, v4}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordRelatedParams> setBitRate(BITRATE_AMR);"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "audio/amr"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mMimeType:Ljava/lang/String;

    iput v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsOutputFileFormat:I

    iput v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsRecordingType:I

    const-string v0, ".amr"

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsExtension:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid output file type requested"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setRecordingMode()V
    .locals 2

    iget v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/Recorder;->setRecordMode(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordingMode> mSelectedModeis MODE_NORMAL"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/Recorder;->setRecordMode(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordingMode> mSelectedModeis MODE_INDOOR"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/Recorder;->setRecordMode(I)V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<setRecordingMode> mSelectedModeis MODE_OUTDOOR"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setTimerViewTextSize(J)V
    .locals 5
    .param p1    # J

    const/16 v0, 0x64

    const-wide/16 v1, 0x3c

    div-long v1, p1, v1

    const-wide/16 v3, 0x64

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    const/high16 v2, 0x428c0000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    const/high16 v2, 0x42b40000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_0
.end method

.method private showDialogFragment(ILandroid/os/Bundle;)V
    .locals 10
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->initEncodeFormatArray()V

    const-string v6, "SelectFormat"

    invoke-direct {p0, v6}, Lcom/android/soundrecorder/SoundRecorder;->removeOldFragmentByTag(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityStringIDArray:[I

    iget-object v7, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuffixArray:[Ljava/lang/CharSequence;

    const v8, 0x7f070012

    iget v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    invoke-static {v6, v7, v8, v9}, Lcom/android/soundrecorder/SelectDialogFragment;->newInstance([I[Ljava/lang/CharSequence;II)Lcom/android/soundrecorder/SelectDialogFragment;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v7, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectFormatListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v6, v7}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    const-string v6, "SelectFormat"

    invoke-virtual {v4, v0, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v6, "SelectMode"

    invoke-direct {p0, v6}, Lcom/android/soundrecorder/SoundRecorder;->removeOldFragmentByTag(Ljava/lang/String;)V

    const/4 v3, 0x3

    const/4 v6, 0x3

    new-array v2, v6, [I

    const/4 v6, 0x0

    const v7, 0x7f070014

    aput v7, v2, v6

    const/4 v6, 0x1

    const v7, 0x7f070016

    aput v7, v2, v6

    const/4 v6, 0x2

    const v7, 0x7f070017

    aput v7, v2, v6

    const/4 v6, 0x0

    const v7, 0x7f070013

    iget v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    invoke-static {v2, v6, v7, v8}, Lcom/android/soundrecorder/SelectDialogFragment;->newInstance([I[Ljava/lang/CharSequence;II)Lcom/android/soundrecorder/SelectDialogFragment;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v7, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v6, v7}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    const-string v6, "SelectMode"

    invoke-virtual {v4, v0, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showStorageHint()V
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageStatus:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    invoke-virtual {v1}, Lcom/android/soundrecorder/OnScreenHint;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    :cond_0
    :goto_1
    return-void

    :pswitch_1
    const v1, 0x7f070019

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    if-nez v1, :cond_2

    invoke-static {p0, v0}, Lcom/android/soundrecorder/OnScreenHint;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;)Lcom/android/soundrecorder/OnScreenHint;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    :goto_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    invoke-virtual {v1}, Lcom/android/soundrecorder/OnScreenHint;->show()V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageHint:Lcom/android/soundrecorder/OnScreenHint;

    invoke-virtual {v1, v0}, Lcom/android/soundrecorder/OnScreenHint;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateAndShowStorageHint(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/soundrecorder/SoundRecorder;->getStorageStatus(Z)I

    move-result v0

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageStatus:I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->showStorageHint()V

    return-void
.end method

.method private updateTimeRemaining()V
    .locals 13

    const-wide/16 v11, 0x3c

    const/4 v10, 0x0

    const/4 v9, 0x1

    const-string v5, "SR/SoundRecorder"

    const-string v6, "<updateTimeRemaining> begin"

    invoke-static {v5, v6}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    iget-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    if-nez v5, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v5, v10}, Lcom/android/soundrecorder/RemainingTimeCalculator;->timeRemaining(Z)J

    move-result-wide v2

    :cond_1
    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-gtz v5, :cond_3

    iput-boolean v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v5}, Lcom/android/soundrecorder/RemainingTimeCalculator;->currentLowerLimit()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    :goto_0
    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v5}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v5

    if-ne v5, v9, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/android/soundrecorder/SoundRecorder;->mStartRecordingTime:J

    sub-long/2addr v5, v7

    :goto_1
    iput-wide v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    const-string v5, "SR/SoundRecorder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<updateTimeRemaining> mTotalRecordingTime="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v5}, Lcom/android/soundrecorder/Recorder;->stop()V

    const-string v5, "SR/SoundRecorder"

    const-string v6, "<updateTimeRemaining> end"

    invoke-static {v5, v6}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void

    :pswitch_0
    const-string v5, "SR/SoundRecorder"

    const-string v6, "<updateTimeRemaining> Is dist space limit"

    invoke-static {v5, v6}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardFull:Z

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070027

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070028

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-wide v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v4, ""

    cmp-long v5, v2, v11

    if-gez v5, :cond_5

    const v5, 0x7f07002a

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :cond_4
    :goto_3
    const-string v5, "SR/SoundRecorder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<updateTimeRemaining> mErrorTextView.setText: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v5, "SR/SoundRecorder"

    const-string v6, "<updateTimeRemaining> end"

    invoke-static {v5, v6}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_5
    const-wide/16 v5, 0x21c

    cmp-long v5, v2, v5

    if-gez v5, :cond_4

    const-wide/16 v5, 0xb4

    cmp-long v5, v5, v2

    if-nez v5, :cond_6

    iget-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v5, :cond_6

    const v5, 0x7f070029

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    div-long v7, v2, v11

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_6
    const v5, 0x7f070015

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    div-long v7, v2, v11

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    rem-long v7, v2, v11

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updateTimerView()V
    .locals 14

    const-string v9, "SR/SoundRecorder"

    const-string v10, "<updateTimerView> begin"

    invoke-static {v9, v10}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFinishedRunFromApp:Z

    if-eqz v9, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v9}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v3

    if-eqz v3, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v9}, Lcom/android/soundrecorder/Recorder;->progress()I

    move-result v9

    int-to-long v5, v9

    :goto_2
    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerFormat:Ljava/lang/String;

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-wide/16 v12, 0x3c

    div-long v12, v5, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-wide/16 v12, 0x3c

    rem-long v12, v5, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mTimerTextView:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v5, v6}, Lcom/android/soundrecorder/SoundRecorder;->setTimerViewTextSize(J)V

    packed-switch v3, :pswitch_data_0

    :cond_1
    :goto_3
    if-eqz v0, :cond_3

    iget-boolean v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v9, :cond_2

    iget-boolean v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    if-nez v9, :cond_b

    :cond_2
    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/android/soundrecorder/SoundRecorder;->mUpdateTimer:Ljava/lang/Runnable;

    const-wide/16 v11, 0x32

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    :goto_4
    const-string v9, "SR/SoundRecorder"

    const-string v10, "<updateTimerView> end"

    invoke-static {v9, v10}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v9}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v9

    int-to-long v5, v9

    goto :goto_2

    :pswitch_0
    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v9}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v9

    if-eqz v9, :cond_1

    const/16 v1, 0x64

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    const-wide/16 v10, 0x64

    mul-long/2addr v10, v5

    iget-object v12, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v12}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v12

    int-to-long v12, v12

    div-long/2addr v10, v12

    long-to-int v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_3

    :pswitch_1
    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lcom/android/soundrecorder/SoundRecorder;->updateAndShowStorageHint(Z)V

    iget-boolean v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v9, :cond_6

    iget-boolean v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mHasFileSizeLimitation:Z

    if-nez v9, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateTimeRemaining()V

    goto :goto_3

    :cond_7
    iget-wide v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingTimeLimitation:J

    long-to-int v9, v9

    iget-object v10, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v10}, Lcom/android/soundrecorder/Recorder;->progress()I

    move-result v10

    sub-int v4, v9, v10

    const-string v8, ""

    const/16 v9, 0x3c

    if-ge v4, v9, :cond_9

    const v9, 0x7f07002a

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    :cond_8
    :goto_5
    const-string v9, "SR/SoundRecorder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<updateTimerView> mErrorTextView.setText: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_9
    int-to-long v9, v4

    iget-wide v11, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingTimeLimitation:J

    cmp-long v9, v9, v11

    if-gtz v9, :cond_8

    rem-int/lit8 v9, v4, 0x3c

    if-nez v9, :cond_a

    const v9, 0x7f070029

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    div-int/lit8 v12, v4, 0x3c

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    :cond_a
    const v9, 0x7f070015

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    div-int/lit8 v12, v4, 0x3c

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    rem-int/lit8 v12, v4, 0x3c

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    :pswitch_2
    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v9}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v9

    if-eqz v9, :cond_1

    iget-boolean v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v9, :cond_1

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/android/soundrecorder/SoundRecorder;->mUpdateTimer:Ljava/lang/Runnable;

    const-wide/16 v11, 0x32

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_3

    :cond_b
    iget-wide v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-lez v9, :cond_c

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v9}, Lcom/android/soundrecorder/Recorder;->progress()I

    move-result v9

    int-to-long v9, v9

    iget-wide v11, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingTimeLimitation:J

    cmp-long v9, v9, v11

    if-gez v9, :cond_c

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v9}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    iget-wide v11, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    cmp-long v9, v9, v11

    if-gez v9, :cond_c

    iget-wide v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    const-wide/16 v11, 0x1

    sub-long/2addr v9, v11

    iput-wide v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/android/soundrecorder/SoundRecorder;->mUpdateTimer:Ljava/lang/Runnable;

    const-wide/16 v11, 0x3e8

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_4

    :cond_c
    const-string v9, "SR/SoundRecorder"

    const-string v10, "<updateTimerView> mRecorder.stop();"

    invoke-static {v9, v10}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v9}, Lcom/android/soundrecorder/Recorder;->stop()V

    iget-object v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/android/soundrecorder/SoundRecorder;->mUpdateTimer:Ljava/lang/Runnable;

    const-wide/16 v11, 0x32

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-wide v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingTimeLimitation:J

    iput-wide v9, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateUIInPauseRecordingState()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    const v2, 0x7f020003

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    const v2, 0x7f07000f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method private updateUIOnIdleState()V
    .locals 7

    const/high16 v2, 0x40000000

    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v5, 0x4

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v2, 0x40400000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    const v2, 0x7f070026

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardmounted:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070032

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mButtonParent:Landroid/widget/LinearLayout;

    const/high16 v2, 0x40400000

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    const v2, 0x7f020008

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    :cond_4
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    const v2, 0x7f020011

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v2, "play"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_5
    const-string v1, "SR/SoundRecorder"

    const-string v2, "<updateUIOnIdleState> in update ui idlestate length>0"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardFull:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070027

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_7
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private updateUIOnPausePlayingState()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    const v1, 0x7f020008

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    const v1, 0x7f020005

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v1, "play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<updateUIOnPausePlayingState> in update ui idlestate length>0"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateUIOnPlayingState()V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    const v1, 0x7f020005

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    const v1, 0x7f020008

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v1, "play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateUIOnRecordingState()V
    .locals 6

    const/4 v5, 0x4

    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPauseRecordingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStopButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingStateImageView:Landroid/widget/ImageView;

    const v2, 0x7f020010

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateTextView:Landroid/widget/TextView;

    const v2, 0x7f070025

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPlayingStateImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStateProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public initEncodeFormatArray()V
    .locals 13

    const/4 v9, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-string v6, "(.ogg)"

    const-string v3, "(.3gpp)"

    const-string v4, "(.amr)"

    const/4 v0, -0x1

    const/4 v1, -0x1

    const/4 v2, -0x1

    const/4 v5, 0x3

    const/4 v7, 0x3

    new-array v8, v9, [Ljava/lang/CharSequence;

    iput-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuffixArray:[Ljava/lang/CharSequence;

    new-array v8, v9, [I

    iput-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityStringIDArray:[I

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuffixArray:[Ljava/lang/CharSequence;

    aput-object v6, v8, v10

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuffixArray:[Ljava/lang/CharSequence;

    aput-object v3, v8, v11

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuffixArray:[Ljava/lang/CharSequence;

    aput-object v4, v8, v12

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityStringIDArray:[I

    const v9, 0x7f070004

    aput v9, v8, v10

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityStringIDArray:[I

    const v9, 0x7f070005

    aput v9, v8, v11

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityStringIDArray:[I

    const v9, 0x7f070006

    aput v9, v8, v12

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x2

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityFormatArray:[I

    aput v0, v8, v10

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityFormatArray:[I

    aput v1, v8, v11

    iget-object v8, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityFormatArray:[I

    aput v2, v8, v12

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v7, 0x1

    const/4 v5, -0x1

    if-ne v5, p2, :cond_1

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, v7}, Landroid/view/View;->setEnabled(Z)V

    move-object v2, p3

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v5, "dowhat"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v6, "record"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->record()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v6, "play"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget v5, v5, Lcom/android/soundrecorder/Recorder;->mState:I

    if-ne v5, v7, :cond_3

    const-string v5, "SR/SoundRecorder"

    const-string v6, "<onActivityResult> in RECORDING_STATE, cannot play"

    invoke-static {v5, v6}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "path"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "path"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iput-object v1, v5, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    const/16 v3, 0x3e8

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "duration"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    div-int/lit16 v6, v6, 0x3e8

    iput v6, v5, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->play()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initRecorder()V

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    iget-object v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v5, v6}, Lcom/android/soundrecorder/VUMeter;->setRecorder(Lcom/android/soundrecorder/Recorder;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mExitButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->delete()V

    :cond_1
    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onBackPressed> call saveSample when case IDLE_STATE"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->saveSample()V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->stop()V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onBackPressed> saveSample in PLAYING/PAUSE_PLAYING_STATE"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->saveSample()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->stop()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackToInit()V
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget v0, v0, Lcom/android/soundrecorder/Recorder;->mState:I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget v0, v0, Lcom/android/soundrecorder/Recorder;->mState:I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->stop()V

    :cond_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initRecorder()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/VUMeter;->setRecorder(Lcom/android/soundrecorder/Recorder;)V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> discardButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickDiscardButton()V

    goto :goto_0

    :pswitch_2
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> recordButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickRecordButton()V

    goto :goto_0

    :pswitch_3
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> playButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickPlayButton()V

    goto :goto_0

    :pswitch_4
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> stopButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickStopButton()V

    goto :goto_0

    :pswitch_5
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> acceptButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickAcceptButton()V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickFileListButton()V

    goto :goto_0

    :pswitch_7
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<onClick> pauseRecordingButton"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->onClickPauseRecordingButton()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090010
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_3
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method onClickAcceptButton()V
    .locals 6

    const/4 v5, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDiscardButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-static {}, Landroid/os/storage/StorageManager;->getDefaultPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    iput-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardmounted:Z

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onClick> acceptButton: sd card is unmounted, show error dialog"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xb

    invoke-static {p0, v1}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v1, :cond_1

    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFinishedRunFromApp:Z

    :cond_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v1

    if-ne v1, v5, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mStartRecordingTime:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    :cond_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->stop()V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_3

    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsDeleted:Z

    :cond_3
    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onClick> call saveSample when click accept"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->saveSample()V

    goto :goto_0
.end method

.method onClickDiscardButton()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-static {}, Landroid/os/storage/StorageManager;->getDefaultPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "mounted"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardmounted:Z

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v4}, Lcom/android/soundrecorder/Recorder;->delete()V

    const-string v4, "SR/SoundRecorder"

    const-string v5, "<onClick> discardButton update UI"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v4, :cond_1

    iput-boolean v7, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFinishedRunFromApp:Z

    :cond_1
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v4}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SoundRecorderUtils;->deleteFileFromMediaDB(Landroid/content/Context;Ljava/lang/String;)Z

    :cond_2
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v4}, Lcom/android/soundrecorder/Recorder;->delete()V

    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardFull:Z

    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v4, v7}, Lcom/android/soundrecorder/RemainingTimeCalculator;->timeRemaining(Z)J

    move-result-wide v0

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_3

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    :cond_3
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v4, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_4
    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v5, 0x0

    iput v5, v4, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v4}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method

.method onClickFileListButton()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onClick> fileListButton"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iput v3, v1, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/android/soundrecorder/RecordingFileList;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method onClickPauseRecordingButton()V
    .locals 2

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->pauseRecording()V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setPauseTimeRemaining(Z)V

    :cond_0
    return-void
.end method

.method onClickPlayButton()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->play()V

    return-void
.end method

.method onClickRecordButton()V
    .locals 5

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->goOnRecording()V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->record()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Performance test][SoundRecorder] recording end ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method onClickStopButton()V
    .locals 5

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->stop()V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget-wide v2, v2, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    iput-wide v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Performance test][SoundRecorder] recording stop end ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1    # Landroid/content/res/Configuration;

    const/4 v5, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    const/4 v2, -0x1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    :cond_0
    const v3, 0x7f030001

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initResourceRefs()V

    iget-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v3

    if-ne v5, v3, :cond_1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f050000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-nez v3, :cond_1

    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mConfigChangeRunFromApp:Z

    :cond_1
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    if-ltz v2, :cond_2

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onCreate>"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "storage"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initRecorder()V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initFromIntent()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v1, "audio/*"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "*/*"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const-string v1, "audio/vorbis"

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRequestedType:Ljava/lang/String;

    :cond_2
    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    new-instance v1, Landroid/media/MediaScannerConnection;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mConnection:Landroid/media/MediaScannerConnection;

    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    if-nez v0, :cond_5

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onCreate> PowerManager == null"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initResourceRefs()V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->registerExternalStorageListener()V

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->initFromLastNonConfigurationInstance()V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_3

    const-string v1, "sound_recorder_data"

    invoke-virtual {p0, v1, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    :cond_3
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "selected_recording_format"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "selected_recording_mode"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "byte_rate"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onCreate> byte rate which gets from SharedPreferences is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    mul-int/lit8 v2, v2, 0x8

    invoke-virtual {v1, v2}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    :cond_4
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x6

    const-string v2, "SR/SoundRecorder"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    const v0, 0x7f070011

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    return v2
.end method

.method public onDestroy()V
    .locals 5

    const/4 v4, 0x0

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onDestroy> begin"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->stop()V

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSaveErrorHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRetainNonConfigChangeHasRun:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    if-nez v1, :cond_3

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onDestroy> mRecorder is null"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSDCardMountEventReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSDCardMountEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSDCardMountEventReceiver:Landroid/content/BroadcastReceiver;

    :cond_2
    iput-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iput-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onDestroy> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->getRecordingFilePath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onDestroy> recording file path is null"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    if-eqz v0, :cond_1

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onDestroy> run mRecorder.delete()"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->delete()V

    goto :goto_0

    :cond_4
    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onDestroy> recording file path is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onError(I)V
    .locals 0
    .param p1    # I

    invoke-static {p0, p1}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    return-void
.end method

.method public onMediaScannerConnected()V
    .locals 3

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mConnection:Landroid/media/MediaScannerConnection;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v2, v3}, Lcom/android/soundrecorder/SoundRecorder;->showDialogFragment(ILandroid/os/Bundle;)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-ne v2, v0, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v3}, Lcom/android/soundrecorder/SoundRecorder;->showDialogFragment(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onPause> begin"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v2, :cond_0

    const-string v2, "sound_recorder_data"

    invoke-virtual {p0, v2, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<onPause> mIsSDCardPlugOut = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v2, "to_be_deleted_file_path"

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<onPause> put mRecordingFilePath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "into SharedPreferences"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onPause> ed.putInt(BYTE_RATE, mRemainingTimeCalculator.getByteRate())"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<onPause> mRemainingTimeCalculator.getByteRate() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v4}, Lcom/android/soundrecorder/RemainingTimeCalculator;->getByteRate()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "byte_rate"

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v3}, Lcom/android/soundrecorder/RemainingTimeCalculator;->getByteRate()I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "byte rate is recorded ,is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    invoke-virtual {v4}, Lcom/android/soundrecorder/RemainingTimeCalculator;->getByteRate()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuspended:Z

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v1

    if-ne v1, v6, :cond_3

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->stop()V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget-wide v2, v2, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    iput-wide v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    :cond_2
    :goto_0
    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mActivityForeground:Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onPause> end"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    :cond_4
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->stop()V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onPrepareOptionsMenu> Option menu is tapped!"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mOptionsMenu:Landroid/view/Menu;

    iget-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return v1

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v3, "recorder_state"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3, v2}, Lcom/android/soundrecorder/Recorder;->restoreState(Landroid/os/Bundle;)V

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onRestoreInstanceState> mRecorder.restoreState(recorderState)"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "SelectFormat"

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectFormatListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_1
    const-string v3, "SelectMode"

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_2
    const-string v3, "SelectMode"

    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Lcom/android/soundrecorder/SelectDialogFragment;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectModeListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/android/soundrecorder/SelectDialogFragment;->setOnClickListener(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_3
    return-void
.end method

.method protected onResume()V
    .locals 6

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onResume> begin"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRemainingTimeCalculator:Lcom/android/soundrecorder/RemainingTimeCalculator;

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    mul-int/lit8 v2, v2, 0x8

    invoke-virtual {v1, v2}, Lcom/android/soundrecorder/RemainingTimeCalculator;->setBitRate(I)V

    :cond_0
    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onResume> byte rate is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mByteRate:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onResume> sample file whether exist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->delete()V

    iput-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardFull:Z

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/soundrecorder/VUMeter;->mCurrentAngle:F

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    :cond_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_2

    const-string v1, "sound_recorder_data"

    invoke-virtual {p0, v1, v4}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    :cond_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "to_be_deleted_file_path"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onResume> set mRecordingFilePath as "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from SharedPreferences"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "sound_recorder_duration"

    const-wide/16 v3, -0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->deleteRecordingFile()V

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onResume> deleteRecordingFile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    if-nez v1, :cond_4

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onResume> RecordingPath is null"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mActivityForeground:Z

    const-string v1, "record"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "play"

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-direct {p0, v5}, Lcom/android/soundrecorder/SoundRecorder;->getStorageStatus(Z)I

    move-result v0

    const/4 v1, 0x2

    if-ne v1, v0, :cond_6

    const/4 v1, 0x4

    invoke-static {p0, v1}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    :cond_5
    :goto_0
    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onResume> end"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_6
    if-ne v5, v0, :cond_5

    const/4 v1, 0x3

    invoke-static {p0, v1}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x4

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onRetainNon>"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v6, p0, Lcom/android/soundrecorder/SoundRecorder;->mRetainNonConfigChangeHasRun:Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v3

    if-ne v3, v5, :cond_0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->stop()V

    :cond_0
    const/4 v0, 0x4

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, v5}, Landroid/os/Bundle;-><init>(I)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2, v5}, Landroid/os/Bundle;-><init>(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    return-object v1

    :cond_1
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v4, "play"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v3, v2}, Lcom/android/soundrecorder/Recorder;->saveState(Landroid/os/Bundle;)V

    :cond_3
    const-string v3, "sample_interrupted"

    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v3, "max_file_size"

    iget-wide v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v3, "not_saved"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onRetainNon> mIsSDCardPlugOut = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-nez v3, :cond_4

    const-string v3, "to_be_deleted_file_path"

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onRetainNon> put mRecordingFilePath = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "into SharedPreferences"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v3, "recorder_state"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onSaveInstanceState>"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v1

    if-ne v1, v5, :cond_1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->stop()V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget-wide v2, v2, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    iput-wide v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->sampleLength()I

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "SR/SoundRecorder"

    const-string v3, "<onSaveInstanceState> sampleLength() == 0"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    :goto_1
    return-void

    :cond_1
    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2}, Lcom/android/soundrecorder/Recorder;->stop()V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v3, "play"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v2, v0}, Lcom/android/soundrecorder/Recorder;->saveState(Landroid/os/Bundle;)V

    :cond_5
    const-string v2, "sample_interrupted"

    iget-boolean v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "max_file_size"

    iget-wide v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mMaxFileSize:J

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v2, "not_saved"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<onSaveInstanceState> mIsSDCardPlugOut = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mIsSDCardPlugOut:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-nez v2, :cond_6

    const-string v2, "to_be_deleted_file_path"

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "SR/SoundRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<onSaveInstanceState> put mRecordingFilePath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "into SharedPreferences"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const-string v2, "recorder_state"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    goto/16 :goto_1
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 16
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v2, v3}, Ljava/util/Date;-><init>(J)V

    new-instance v9, Ljava/text/SimpleDateFormat;

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f070033

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "_data LIKE \'%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "file:///"

    const-string v15, ""

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v10, 0x6

    new-instance v4, Landroid/content/ContentValues;

    const/4 v13, 0x6

    invoke-direct {v4, v13}, Landroid/content/ContentValues;-><init>(I)V

    const-string v13, "is_music"

    const-string v14, "0"

    invoke-virtual {v4, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "title"

    invoke-virtual {v4, v13, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "date_added"

    const-wide/16 v14, 0x3e8

    div-long v14, v2, v14

    long-to-int v14, v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v4, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v13, "_data"

    move-object/from16 v0, p1

    invoke-virtual {v4, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "artist"

    const/high16 v14, 0x7f070000

    invoke-virtual {v6, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "album"

    const v14, 0x7f07002e

    invoke-virtual {v6, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v7, v1, v4, v12, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    if-lez v8, :cond_0

    const-string v13, "SR/SoundRecorder"

    const-string v14, "<onScanCompleted> update success!"

    invoke-static {v13, v14}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/soundrecorder/SoundRecorder;->mConnection:Landroid/media/MediaScannerConnection;

    invoke-virtual {v13}, Landroid/media/MediaScannerConnection;->disconnect()V

    return-void
.end method

.method public onStateChanged(I)V
    .locals 6
    .param p1    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v1, "SR/SoundRecorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<onStateChanged> state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    :cond_0
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuspended:Z

    if-nez v1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, v4}, Landroid/os/PowerManager;->userActivity(JZ)V

    :cond_1
    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSuspended:Z

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    if-ne p1, v4, :cond_4

    :cond_2
    iput-boolean v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mSampleInterrupted:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mErrorUiMessage:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onStateChanged> acquire WakeLock"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    return-void

    :cond_4
    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-nez v1, :cond_5

    iget-wide v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingLeftTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gtz v1, :cond_5

    if-eqz v0, :cond_5

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, v5}, Landroid/os/PowerManager;->userActivity(JZ)V

    :cond_5
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->releaseWakeLock()V

    iput v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mStorageStatus:I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->showStorageHint()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 4

    const-string v1, "SR/SoundRecorder"

    const-string v2, "<onStop>"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->stop()V

    :cond_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_2

    const-string v1, "sound_recorder_data"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    :cond_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "selected_recording_format"

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "selected_recording_mode"

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "sound_recorder_duration"

    iget-wide v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mTotalRecordingTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public play()V
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->pausePlayback()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->startPlayback()V

    goto :goto_0
.end method

.method public record()V
    .locals 6

    const/4 v5, 0x0

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<record> start!"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->close()V

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    const-string v1, "play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget v1, v1, Lcom/android/soundrecorder/Recorder;->mState:I

    if-eq v0, v1, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget v1, v1, Lcom/android/soundrecorder/Recorder;->mState:I

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget v0, v0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-nez v0, :cond_5

    :cond_1
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<record> mDoWhat != null"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->stopPlayback()V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iput-object v5, v0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    const/4 v1, 0x0

    iput v1, v0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    :cond_2
    :goto_0
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<record> setRecordRelatedParams() begin"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->setRecordRelatedParams()V

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<record> setRecordRelatedParams() end"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->judgeStorageBeforeRecord()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->setRecordingMode()V

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/soundrecorder/Recorder;->mSoundRecorderDoWhat:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsOutputFileFormat:I

    iget v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsRecordingType:I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordParamsExtension:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/soundrecorder/Recorder;->startRecording(IILjava/lang/String;Landroid/content/Context;)V

    :goto_1
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->getRecordingFilePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    const-string v0, "SR/SoundRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<record> set mRecordingFilePath = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mStartRecordingTime:J

    const-string v0, "play"

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v5, p0, Lcom/android/soundrecorder/SoundRecorder;->mDoWhat:Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_5
    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    iget v0, v0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-nez v0, :cond_2

    const-string v0, "SR/SoundRecorder"

    const-string v1, "<record> IDLE_State, mRecorder.delete()"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->delete()V

    goto :goto_0

    :cond_6
    const-string v0, "SR/SoundRecorder"

    const-string v1, "<record> run recordRunFromOthers()"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->recordRunFromOthers()V

    goto :goto_1
.end method

.method public setSelectedFormat(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mVoiceQualityFormatArray:[I

    aget v0, v0, p1

    iput v0, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedFormat:I

    return-void
.end method

.method public setSelectedMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/soundrecorder/SoundRecorder;->mSelectedMode:I

    return-void
.end method

.method public updateUi()V
    .locals 5

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFinishedRunFromApp:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    const-string v2, ".tmp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecordingFileNameTextView:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    const v2, 0x7f07002b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRunFromLauncher:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mAcceptButton:Landroid/widget/Button;

    const v2, 0x7f070002

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_3
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mRecorder:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->state()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_2
    iget-boolean v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mConfigChangeRunFromApp:Z

    if-nez v1, :cond_4

    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateTimerView()V

    :cond_4
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mVUMeter:Lcom/android/soundrecorder/VUMeter;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder;->mFileName:Ljava/lang/String;

    goto :goto_1

    :pswitch_0
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUIOnIdleState()V

    goto :goto_2

    :pswitch_1
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUIOnPausePlayingState()V

    goto :goto_2

    :pswitch_2
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUIOnRecordingState()V

    goto :goto_2

    :pswitch_3
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUIInPauseRecordingState()V

    goto :goto_2

    :pswitch_4
    invoke-direct {p0}, Lcom/android/soundrecorder/SoundRecorder;->updateUIOnPlayingState()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
