.class Lcom/android/stk/StkAppInstaller;
.super Ljava/lang/Object;
.source "StkAppInstaller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/stk/StkAppInstaller$1;,
        Lcom/android/stk/StkAppInstaller$UnInstallThread;,
        Lcom/android/stk/StkAppInstaller$InstallThread;
    }
.end annotation


# static fields
.field public static final STK_INSTALLED:I = 0x1

.field public static final STK_NOT_INSTALLED:I

.field private static mInstance:Lcom/android/stk/StkAppInstaller;

.field private static miSTKInstalled:I


# instance fields
.field private installThread:Lcom/android/stk/StkAppInstaller$InstallThread;

.field mContext:Landroid/content/Context;

.field private uninstallThread:Lcom/android/stk/StkAppInstaller$UnInstallThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/stk/StkAppInstaller;

    invoke-direct {v0}, Lcom/android/stk/StkAppInstaller;-><init>()V

    sput-object v0, Lcom/android/stk/StkAppInstaller;->mInstance:Lcom/android/stk/StkAppInstaller;

    const/4 v0, -0x1

    sput v0, Lcom/android/stk/StkAppInstaller;->miSTKInstalled:I

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/stk/StkAppInstaller$InstallThread;

    invoke-direct {v0, p0, v1}, Lcom/android/stk/StkAppInstaller$InstallThread;-><init>(Lcom/android/stk/StkAppInstaller;Lcom/android/stk/StkAppInstaller$1;)V

    iput-object v0, p0, Lcom/android/stk/StkAppInstaller;->installThread:Lcom/android/stk/StkAppInstaller$InstallThread;

    new-instance v0, Lcom/android/stk/StkAppInstaller$UnInstallThread;

    invoke-direct {v0, p0, v1}, Lcom/android/stk/StkAppInstaller$UnInstallThread;-><init>(Lcom/android/stk/StkAppInstaller;Lcom/android/stk/StkAppInstaller$1;)V

    iput-object v0, p0, Lcom/android/stk/StkAppInstaller;->uninstallThread:Lcom/android/stk/StkAppInstaller$UnInstallThread;

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Z)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    invoke-static {p0, p1}, Lcom/android/stk/StkAppInstaller;->setAppState(Landroid/content/Context;Z)V

    return-void
.end method

.method public static getInstance()Lcom/android/stk/StkAppInstaller;
    .locals 1

    sget-object v0, Lcom/android/stk/StkAppInstaller;->mInstance:Lcom/android/stk/StkAppInstaller;

    return-object v0
.end method

.method public static getIsInstalled()I
    .locals 1

    sget v0, Lcom/android/stk/StkAppInstaller;->miSTKInstalled:I

    return v0
.end method

.method private static setAppState(Landroid/content/Context;Z)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const/4 v6, 0x2

    const/4 v5, 0x1

    const-string v7, "StkAppInstaller"

    const-string v8, "[setAppState]+"

    invoke-static {v7, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v0, Landroid/content/ComponentName;

    const-string v7, "com.android.stk"

    const-string v8, "com.android.stk.StkLauncherActivity"

    invoke-direct {v0, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    const-string v7, "com.android.stk"

    const-string v8, "com.android.stk.StkMenuActivity"

    invoke-direct {v1, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_4

    move v4, v5

    :goto_1
    const-string v7, "StkAppInstaller"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Stk1 - setAppState - curState["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Lcom/android/stk/StkAppInstaller;->miSTKInstalled:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] to state["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-ne v5, v4, :cond_2

    sget v7, Lcom/android/stk/StkAppInstaller;->miSTKInstalled:I

    if-eq v5, v7, :cond_3

    :cond_2
    if-ne v6, v4, :cond_5

    sget v6, Lcom/android/stk/StkAppInstaller;->miSTKInstalled:I

    if-nez v6, :cond_5

    :cond_3
    const-string v5, "StkAppInstaller"

    const-string v6, "Stk1 - Need not change app state!!"

    invoke-static {v5, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const-string v5, "StkAppInstaller"

    const-string v6, "[setAppState]-"

    invoke-static {v5, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v4, v6

    goto :goto_1

    :cond_5
    const-string v6, "StkAppInstaller"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Stk1 - StkAppInstaller - Change app state["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_6

    :goto_3
    sput v5, Lcom/android/stk/StkAppInstaller;->miSTKInstalled:I

    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {v3, v0, v4, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v2

    const-string v5, "StkAppInstaller"

    const-string v6, "Could not change STK1 app state"

    invoke-static {v5, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    const/4 v5, 0x0

    goto :goto_3
.end method


# virtual methods
.method install(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/stk/StkAppInstaller;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/stk/StkAppInstaller;->installThread:Lcom/android/stk/StkAppInstaller$InstallThread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method unInstall(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/stk/StkAppInstaller;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/stk/StkAppInstaller;->uninstallThread:Lcom/android/stk/StkAppInstaller$UnInstallThread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
