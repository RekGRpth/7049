.class Lcom/android/stk/StkAppService$5;
.super Landroid/content/BroadcastReceiver;
.source "StkAppService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/stk/StkAppService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/stk/StkAppService;


# direct methods
.method constructor <init>(Lcom/android/stk/StkAppService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/16 v6, 0x14d

    const/4 v5, 0x0

    const-string v2, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "ss"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "simId"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v2, "Stk-SAS "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSIMStateChangeReceiver() - simId["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]  state["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] bStkEventReceiverReged["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$2100(Lcom/android/stk/StkAppService;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_1

    const-string v2, "NOT_READY"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v2}, Lcom/android/stk/StkAppService;->access$2100(Lcom/android/stk/StkAppService;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Stk-SAS "

    const-string v3, "mSIMStateChangeReceiver() - unReg stk Event EvDl"

    invoke-static {v2, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v2}, Lcom/android/stk/StkAppService;->access$2200(Lcom/android/stk/StkAppService;)V

    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v2, v5}, Lcom/android/stk/StkAppService;->access$2102(Lcom/android/stk/StkAppService;Z)Z

    :cond_0
    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v2}, Lcom/android/stk/StkAppService;->access$2300(Lcom/android/stk/StkAppService;)Landroid/app/NotificationManager;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v2}, Lcom/android/stk/StkAppService;->access$2100(Lcom/android/stk/StkAppService;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "Stk-SAS "

    const-string v3, "mSIMStateChangeReceiver() - Reg stk Event EvDl"

    invoke-static {v2, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v2}, Lcom/android/stk/StkAppService;->access$2400(Lcom/android/stk/StkAppService;)V

    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/stk/StkAppService;->access$2102(Lcom/android/stk/StkAppService;Z)Z

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.ACTION_SHUTDOWN_IPO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "Stk-SAS "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[IPO_SHUTDOWN][initial mMainCmd] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$200(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/stk/StkAppService;->access$202(Lcom/android/stk/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage;)Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-static {v5}, Lcom/android/stk/StkAppService;->access$2502(Z)Z

    invoke-static {v5}, Lcom/android/stk/StkAppService;->access$2602(Z)Z

    const-string v2, "Stk-SAS "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[IPO_SHUTDOWN][mMainCmd] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v4}, Lcom/android/stk/StkAppService;->access$200(Lcom/android/stk/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.aciton.stk.REMOVE_IDLE_TEXT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "Stk-SAS "

    const-string v3, "remove Stk1 idle mode text by Refresh command"

    invoke-static {v2, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/stk/StkAppService$5;->this$0:Lcom/android/stk/StkAppService;

    invoke-static {v2}, Lcom/android/stk/StkAppService;->access$2300(Lcom/android/stk/StkAppService;)Landroid/app/NotificationManager;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/NotificationManager;->cancel(I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.aciton.stk.REMOVE_IDLE_TEXT_2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto/16 :goto_0
.end method
