.class public Lcom/android/stk/StkMenuAdapter;
.super Landroid/widget/ArrayAdapter;
.source "StkMenuAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/stk/StkMenuAdapter$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/internal/telephony/cat/Item;",
        ">;"
    }
.end annotation


# instance fields
.field private final SHOW_NEXTACTION:Z

.field private mIcosSelfExplanatory:Z

.field private final mInflater:Landroid/view/LayoutInflater;

.field private m_NextActionIndicator:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;[BZ)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p3    # [B
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/cat/Item;",
            ">;[BZ)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-boolean v0, p0, Lcom/android/stk/StkMenuAdapter;->mIcosSelfExplanatory:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/stk/StkMenuAdapter;->SHOW_NEXTACTION:Z

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk/StkMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-boolean p4, p0, Lcom/android/stk/StkMenuAdapter;->mIcosSelfExplanatory:Z

    iput-object p3, p0, Lcom/android/stk/StkMenuAdapter;->m_NextActionIndicator:[B

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/telephony/cat/Item;

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-boolean v6, p0, Lcom/android/stk/StkMenuAdapter;->mIcosSelfExplanatory:Z

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/android/stk/StkMenuAdapter;->mIcosSelfExplanatory:Z

    if-eqz v6, :cond_4

    iget-object v6, v2, Lcom/android/internal/telephony/cat/Item;->icon:Landroid/graphics/Bitmap;

    if-nez v6, :cond_4

    :cond_0
    iget-object v6, p0, Lcom/android/stk/StkMenuAdapter;->m_NextActionIndicator:[B

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/stk/StkMenuAdapter;->m_NextActionIndicator:[B

    aget-byte v6, v6, p1

    invoke-static {v6}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->fromInt(I)Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v5

    if-eqz v5, :cond_1

    const/4 v0, 0x1

    sget-object v6, Lcom/android/stk/StkMenuAdapter$1;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    const/4 v0, 0x0

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/android/stk/StkMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f030003

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v6, 0x7f07000b

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iget-object v7, v2, Lcom/android/internal/telephony/cat/Item;->text:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f07000c

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const v6, 0x7f07000a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v6, v2, Lcom/android/internal/telephony/cat/Item;->icon:Landroid/graphics/Bitmap;

    if-nez v6, :cond_3

    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    return-object p2

    :pswitch_0
    const v4, 0x7f06001d

    goto :goto_0

    :pswitch_1
    const v4, 0x7f060010

    goto :goto_0

    :pswitch_2
    const v4, 0x7f060015

    goto :goto_0

    :pswitch_3
    const v4, 0x7f06001c

    goto :goto_0

    :pswitch_4
    const v4, 0x7f060013

    goto :goto_0

    :pswitch_5
    const v4, 0x7f060016

    goto :goto_0

    :pswitch_6
    const v4, 0x7f060012

    goto :goto_0

    :pswitch_7
    const v4, 0x7f060011

    goto :goto_0

    :pswitch_8
    const v4, 0x7f06001a

    goto :goto_0

    :pswitch_9
    const v4, 0x7f06001b

    goto :goto_0

    :pswitch_a
    const v4, 0x7f060019

    goto :goto_0

    :pswitch_b
    const v4, 0x7f060017

    goto :goto_0

    :pswitch_c
    const v4, 0x7f060018

    goto :goto_0

    :pswitch_d
    const v4, 0x7f060014

    goto :goto_0

    :pswitch_e
    const v4, 0x7f06001e

    goto :goto_0

    :pswitch_f
    const v4, 0x7f06001f

    goto :goto_0

    :pswitch_10
    const v4, 0x7f060020

    goto/16 :goto_0

    :pswitch_11
    const v4, 0x7f060021

    goto/16 :goto_0

    :pswitch_12
    const v4, 0x7f060022

    goto/16 :goto_0

    :pswitch_13
    const v4, 0x7f060023

    goto/16 :goto_0

    :pswitch_14
    const v4, 0x7f060024

    goto/16 :goto_0

    :pswitch_15
    const v4, 0x7f060025

    goto/16 :goto_0

    :pswitch_16
    const v4, 0x7f060026

    goto/16 :goto_0

    :pswitch_17
    const v4, 0x7f060027

    goto/16 :goto_0

    :pswitch_18
    const v4, 0x7f060028

    goto/16 :goto_0

    :pswitch_19
    const v4, 0x7f060029

    goto/16 :goto_0

    :pswitch_1a
    const v4, 0x7f06002a

    goto/16 :goto_0

    :pswitch_1b
    const v4, 0x7f06002b

    goto/16 :goto_0

    :pswitch_1c
    const v4, 0x7f06002c

    goto/16 :goto_0

    :pswitch_1d
    const v4, 0x7f06002d

    goto/16 :goto_0

    :pswitch_1e
    const v4, 0x7f06002e

    goto/16 :goto_0

    :pswitch_1f
    const v4, 0x7f06002f

    goto/16 :goto_0

    :pswitch_20
    const v4, 0x7f060030

    goto/16 :goto_0

    :pswitch_21
    const v4, 0x7f060031

    goto/16 :goto_0

    :pswitch_22
    const v4, 0x7f060032

    goto/16 :goto_0

    :pswitch_23
    const v4, 0x7f060033

    goto/16 :goto_0

    :pswitch_24
    const v4, 0x7f060034

    goto/16 :goto_0

    :pswitch_25
    const v4, 0x7f060035

    goto/16 :goto_0

    :pswitch_26
    const v4, 0x7f060036

    goto/16 :goto_0

    :pswitch_27
    const v4, 0x7f060037

    goto/16 :goto_0

    :cond_2
    iget-object v6, p0, Lcom/android/stk/StkMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f030002

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v6, 0x7f07000b

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iget-object v7, v2, Lcom/android/internal/telephony/cat/Item;->text:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_3
    iget-object v6, v2, Lcom/android/internal/telephony/cat/Item;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_4
    iget-object v6, p0, Lcom/android/stk/StkMenuAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f030002

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v6, 0x7f07000b

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    const v6, 0x7f07000a

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v6, v2, Lcom/android/internal/telephony/cat/Item;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
    .end packed-switch
.end method
