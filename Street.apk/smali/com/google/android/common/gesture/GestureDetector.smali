.class public Lcom/google/android/common/gesture/GestureDetector;
.super Ljava/lang/Object;
.source "GestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/common/gesture/GestureDetector$GestureHandler;
    }
.end annotation


# static fields
.field private static final DOUBLE_TAP_TIMEOUT:I

.field private static final LONGPRESS_TIMEOUT:I

.field private static final TAP_TIMEOUT:I


# instance fields
.field private mAlwaysInBiggerTapRegion:Z

.field private mAlwaysInTapRegion:Z

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private mDoubleTapSlopSquare:I

.field private mDoubleTapTouchSlopSquare:I

.field private mDownFocusX:F

.field private mDownFocusY:F

.field private final mHandler:Landroid/os/Handler;

.field private mInLongPress:Z

.field private mIsDoubleTapping:Z

.field private mIsLongpressEnabled:Z

.field private mLastFocusX:F

.field private mLastFocusY:F

.field private final mListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mMaximumFlingVelocity:I

.field private mMinimumFlingVelocity:I

.field private mPreviousUpEvent:Landroid/view/MotionEvent;

.field private mStillDown:Z

.field private mTouchSlopSquare:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lcom/google/android/common/gesture/GestureDetector;->LONGPRESS_TIMEOUT:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/common/gesture/GestureDetector;->TAP_TIMEOUT:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/common/gesture/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/GestureDetector$OnGestureListener;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/common/gesture/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/GestureDetector$OnGestureListener;
    .param p3    # Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p3, :cond_1

    new-instance v0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;

    invoke-direct {v0, p0, p3}, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;-><init>(Lcom/google/android/common/gesture/GestureDetector;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    :goto_0
    iput-object p2, p0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    instance-of v0, p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    check-cast p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-virtual {p0, p2}, Lcom/google/android/common/gesture/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/common/gesture/GestureDetector;->init(Landroid/content/Context;)V

    return-void

    :cond_1
    new-instance v0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;

    invoke-direct {v0, p0}, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;-><init>(Lcom/google/android/common/gesture/GestureDetector;)V

    iput-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/MotionEvent;
    .locals 1
    .param p0    # Lcom/google/android/common/gesture/GestureDetector;

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/GestureDetector$OnGestureListener;
    .locals 1
    .param p0    # Lcom/google/android/common/gesture/GestureDetector;

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/common/gesture/GestureDetector;)V
    .locals 0
    .param p0    # Lcom/google/android/common/gesture/GestureDetector;

    invoke-direct {p0}, Lcom/google/android/common/gesture/GestureDetector;->dispatchLongPress()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/GestureDetector$OnDoubleTapListener;
    .locals 1
    .param p0    # Lcom/google/android/common/gesture/GestureDetector;

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/common/gesture/GestureDetector;)Z
    .locals 1
    .param p0    # Lcom/google/android/common/gesture/GestureDetector;

    iget-boolean v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z

    return v0
.end method

.method private cancel()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    iget-boolean v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    :cond_0
    return-void
.end method

.method private cancelTaps()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    iget-boolean v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    :cond_0
    return-void
.end method

.method private dispatchLongPress()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v1, p0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "OnGestureListener must not be null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mIsLongpressEnabled:Z

    if-nez p1, :cond_1

    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v3

    move v2, v3

    mul-int/lit8 v1, v3, 0x2

    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mMinimumFlingVelocity:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mMaximumFlingVelocity:I

    :goto_0
    mul-int v4, v3, v3

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mTouchSlopSquare:I

    mul-int v4, v2, v2

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapTouchSlopSquare:I

    mul-int v4, v1, v1

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapSlopSquare:I

    return-void

    :cond_1
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mMinimumFlingVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mMaximumFlingVelocity:I

    goto :goto_0
.end method

.method private isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # Landroid/view/MotionEvent;

    const/4 v6, 0x0

    iget-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    if-nez v2, :cond_0

    move v2, v6

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget v4, Lcom/google/android/common/gesture/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    move v2, v6

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    sub-int v0, v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int v1, v2, v3

    mul-int v2, v0, v0

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapSlopSquare:I

    if-ge v2, v3, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    move v2, v6

    goto :goto_0
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 41
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    if-nez v35, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    move v0, v5

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    const/16 v36, 0x6

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_1

    const/16 v35, 0x1

    move/from16 v21, v35

    :goto_0
    if-eqz v21, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v35

    move/from16 v24, v35

    :goto_1
    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    const/16 v17, 0x0

    :goto_2
    move/from16 v0, v17

    move v1, v6

    if-ge v0, v1, :cond_4

    move/from16 v0, v24

    move/from16 v1, v17

    if-ne v0, v1, :cond_3

    :goto_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    :cond_1
    const/16 v35, 0x0

    move/from16 v21, v35

    goto :goto_0

    :cond_2
    const/16 v35, -0x1

    move/from16 v24, v35

    goto :goto_1

    :cond_3
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v35

    add-float v25, v25, v35

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v35

    add-float v26, v26, v35

    goto :goto_3

    :cond_4
    if-eqz v21, :cond_6

    const/16 v35, 0x1

    sub-int v35, v6, v35

    move/from16 v11, v35

    :goto_4
    move v0, v11

    int-to-float v0, v0

    move/from16 v35, v0

    div-float v13, v25, v35

    move v0, v11

    int-to-float v0, v0

    move/from16 v35, v0

    div-float v14, v26, v35

    const/16 v16, 0x0

    move v0, v5

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    packed-switch v35, :pswitch_data_0

    :cond_5
    :goto_5
    :pswitch_0
    return v16

    :cond_6
    move v11, v6

    goto :goto_4

    :pswitch_1
    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusX:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusY:F

    invoke-direct/range {p0 .. p0}, Lcom/google/android/common/gesture/GestureDetector;->cancelTaps()V

    goto :goto_5

    :pswitch_2
    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusX:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusY:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    const/16 v36, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mMaximumFlingVelocity:I

    move/from16 v37, v0

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v34

    const/16 v17, 0x0

    :goto_6
    move/from16 v0, v17

    move v1, v6

    if-ge v0, v1, :cond_5

    move/from16 v0, v17

    move/from16 v1, v27

    if-ne v0, v1, :cond_8

    :cond_7
    add-int/lit8 v17, v17, 0x1

    goto :goto_6

    :cond_8
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v35

    mul-float v31, v32, v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v35

    mul-float v33, v34, v35

    add-float v12, v31, v33

    const/16 v35, 0x0

    cmpg-float v35, v12, v35

    if-gez v35, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_5

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    if-eqz v35, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v15

    if-eqz v15, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    if-eqz v35, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    if-eqz v35, :cond_d

    if-eqz v15, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/common/gesture/GestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v35

    if-eqz v35, :cond_d

    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    invoke-interface/range {v35 .. v36}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    :cond_a
    :goto_7
    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusX:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusY:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    if-eqz v35, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/MotionEvent;->recycle()V

    :cond_b
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z

    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mIsLongpressEnabled:Z

    move/from16 v35, v0

    if-eqz v35, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v37

    sget v39, Lcom/google/android/common/gesture/GestureDetector;->TAP_TIMEOUT:I

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v39, v0

    add-long v37, v37, v39

    sget v39, Lcom/google/android/common/gesture/GestureDetector;->LONGPRESS_TIMEOUT:I

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v39, v0

    add-long v37, v37, v39

    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v37

    sget v39, Lcom/google/android/common/gesture/GestureDetector;->TAP_TIMEOUT:I

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v39, v0

    add-long v37, v37, v39

    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    goto/16 :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    sget v37, Lcom/google/android/common/gesture/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    move/from16 v0, v37

    int-to-long v0, v0

    move-wide/from16 v37, v0

    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_7

    :pswitch_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    move/from16 v35, v0

    if-nez v35, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move/from16 v35, v0

    sub-float v22, v35, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    move/from16 v35, v0

    sub-float v23, v35, v14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    move/from16 v35, v0

    if-eqz v35, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    goto/16 :goto_5

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v35, v0

    if-eqz v35, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusX:F

    move/from16 v35, v0

    sub-float v35, v13, v35

    move/from16 v0, v35

    float-to-int v0, v0

    move v8, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusY:F

    move/from16 v35, v0

    sub-float v35, v14, v35

    move/from16 v0, v35

    float-to-int v0, v0

    move v9, v0

    mul-int v35, v8, v8

    mul-int v36, v9, v9

    add-int v10, v35, v36

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mTouchSlopSquare:I

    move/from16 v35, v0

    move v0, v10

    move/from16 v1, v35

    if-le v0, v1, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, p1

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x1

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapTouchSlopSquare:I

    move/from16 v35, v0

    move v0, v10

    move/from16 v1, v35

    if-le v0, v1, :cond_5

    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    goto/16 :goto_5

    :cond_10
    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(F)F

    move-result v35

    const/high16 v36, 0x3f800000

    cmpl-float v35, v35, v36

    if-gez v35, :cond_11

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v35

    const/high16 v36, 0x3f800000

    cmpl-float v35, v35, v36

    if-ltz v35, :cond_5

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, p1

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    goto/16 :goto_5

    :pswitch_5
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z

    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    move/from16 v35, v0

    if-eqz v35, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    :cond_12
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    if-eqz v35, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/MotionEvent;->recycle()V

    :cond_13
    move-object v0, v7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    if-eqz v35, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/VelocityTracker;->recycle()V

    const/16 v35, 0x0

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_14
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x1

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_5

    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    move/from16 v35, v0

    if-eqz v35, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    goto :goto_8

    :cond_16
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v35, v0

    if-eqz v35, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v16

    goto/16 :goto_8

    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v28, v0

    const/16 v35, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v20

    const/16 v35, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mMaximumFlingVelocity:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    move-object/from16 v0, v28

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    move-object/from16 v0, v28

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v30

    move-object/from16 v0, v28

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v29

    invoke-static/range {v30 .. v30}, Ljava/lang/Math;->abs(F)F

    move-result v35

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mMinimumFlingVelocity:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    cmpl-float v35, v35, v36

    if-gtz v35, :cond_18

    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(F)F

    move-result v35

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mMinimumFlingVelocity:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    cmpl-float v35, v35, v36

    if-lez v35, :cond_12

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, p1

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    goto/16 :goto_8

    :pswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/common/gesture/GestureDetector;->cancel()V

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setIsLongpressEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/common/gesture/GestureDetector;->mIsLongpressEnabled:Z

    return-void
.end method

.method public setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V
    .locals 0
    .param p1    # Landroid/view/GestureDetector$OnDoubleTapListener;

    iput-object p1, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    return-void
.end method
