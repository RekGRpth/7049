.class public Lcom/google/android/common/gesture/ScaleEvent;
.super Ljava/lang/Object;
.source "ScaleEvent.java"


# instance fields
.field private final detector:Lcom/google/android/common/gesture/ScaleGestureDetector;

.field private final eventType:I


# direct methods
.method public constructor <init>(ILcom/google/android/common/gesture/ScaleGestureDetector;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/common/gesture/ScaleEvent;->eventType:I

    iput-object p2, p0, Lcom/google/android/common/gesture/ScaleEvent;->detector:Lcom/google/android/common/gesture/ScaleGestureDetector;

    return-void
.end method


# virtual methods
.method public getCurrentSpan()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/common/gesture/ScaleEvent;->detector:Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-virtual {v0}, Lcom/google/android/common/gesture/ScaleGestureDetector;->getCurrentSpan()F

    move-result v0

    return v0
.end method

.method public getFocusX()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/common/gesture/ScaleEvent;->detector:Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-virtual {v0}, Lcom/google/android/common/gesture/ScaleGestureDetector;->getFocusX()F

    move-result v0

    return v0
.end method

.method public getFocusY()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/common/gesture/ScaleEvent;->detector:Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-virtual {v0}, Lcom/google/android/common/gesture/ScaleGestureDetector;->getFocusY()F

    move-result v0

    return v0
.end method

.method public getPreviousSpan()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/common/gesture/ScaleEvent;->detector:Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-virtual {v0}, Lcom/google/android/common/gesture/ScaleGestureDetector;->getPreviousSpan()F

    move-result v0

    return v0
.end method
