.class public Lcom/google/android/common/gesture/ScaleGestureDetector;
.super Ljava/lang/Object;
.source "ScaleGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrSpan:F

.field private mCurrSpanX:F

.field private mCurrSpanY:F

.field private mFocusX:F

.field private mFocusY:F

.field private mInProgress:Z

.field private mInitialSpan:F

.field private final mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

.field private mPrevSpan:F

.field private mPrevSpanX:F

.field private mPrevSpanY:F

.field private final mSpanSlop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mSpanSlop:I

    iput-object p1, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    return-void
.end method


# virtual methods
.method public getCurrentSpan()F
    .locals 1

    iget v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    return v0
.end method

.method public getFocusX()F
    .locals 1

    iget v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mFocusX:F

    return v0
.end method

.method public getFocusY()F
    .locals 1

    iget v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mFocusY:F

    return v0
.end method

.method public getPreviousSpan()F
    .locals 1

    iget v0, p0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpan:F

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 25
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    const/16 v23, 0x1

    move v0, v2

    move/from16 v1, v23

    if-eq v0, v1, :cond_0

    const/16 v23, 0x3

    move v0, v2

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    :cond_0
    const/16 v23, 0x1

    move/from16 v18, v23

    :goto_0
    if-eqz v2, :cond_1

    if-eqz v18, :cond_4

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;->onScaleEnd(Lcom/google/android/common/gesture/ScaleGestureDetector;)V

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInitialSpan:F

    :cond_2
    if-eqz v18, :cond_4

    const/16 v23, 0x1

    :goto_1
    return v23

    :cond_3
    const/16 v23, 0x0

    move/from16 v18, v23

    goto :goto_0

    :cond_4
    const/16 v23, 0x6

    move v0, v2

    move/from16 v1, v23

    if-eq v0, v1, :cond_5

    const/16 v23, 0x5

    move v0, v2

    move/from16 v1, v23

    if-ne v0, v1, :cond_6

    :cond_5
    const/16 v23, 0x1

    move/from16 v3, v23

    :goto_2
    const/16 v23, 0x6

    move v0, v2

    move/from16 v1, v23

    if-ne v0, v1, :cond_7

    const/16 v23, 0x1

    move/from16 v13, v23

    :goto_3
    if-eqz v13, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v23

    move/from16 v14, v23

    :goto_4
    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    const/4 v12, 0x0

    :goto_5
    if-ge v12, v4, :cond_a

    if-ne v14, v12, :cond_9

    :goto_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    :cond_6
    const/16 v23, 0x0

    move/from16 v3, v23

    goto :goto_2

    :cond_7
    const/16 v23, 0x0

    move/from16 v13, v23

    goto :goto_3

    :cond_8
    const/16 v23, -0x1

    move/from16 v14, v23

    goto :goto_4

    :cond_9
    move-object/from16 v0, p1

    move v1, v12

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v23

    add-float v19, v19, v23

    move-object/from16 v0, p1

    move v1, v12

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v23

    add-float v20, v20, v23

    goto :goto_6

    :cond_a
    if-eqz v13, :cond_b

    const/16 v23, 0x1

    sub-int v23, v4, v23

    move/from16 v9, v23

    :goto_7
    move v0, v9

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v10, v19, v23

    move v0, v9

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v11, v20, v23

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v12, 0x0

    :goto_8
    if-ge v12, v4, :cond_d

    if-ne v14, v12, :cond_c

    :goto_9
    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    :cond_b
    move v9, v4

    goto :goto_7

    :cond_c
    move-object/from16 v0, p1

    move v1, v12

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v23

    sub-float v23, v23, v10

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v23

    add-float v5, v5, v23

    move-object/from16 v0, p1

    move v1, v12

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v23

    sub-float v23, v23, v11

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v23

    add-float v6, v6, v23

    goto :goto_9

    :cond_d
    move v0, v9

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v7, v5, v23

    move v0, v9

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v8, v6, v23

    const/high16 v23, 0x40000000

    mul-float v16, v7, v23

    const/high16 v23, 0x40000000

    mul-float v17, v8, v23

    mul-float v23, v16, v16

    mul-float v24, v17, v17

    add-float v23, v23, v24

    invoke-static/range {v23 .. v23}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v22, v0

    move v0, v10

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mFocusX:F

    move v0, v11

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mFocusY:F

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    if-eqz v23, :cond_f

    const/16 v23, 0x0

    cmpl-float v23, v15, v23

    if-eqz v23, :cond_e

    if-eqz v3, :cond_f

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;->onScaleEnd(Lcom/google/android/common/gesture/ScaleGestureDetector;)V

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInitialSpan:F

    :cond_f
    if-eqz v3, :cond_10

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanX:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanX:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanY:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanY:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpan:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInitialSpan:F

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    if-nez v23, :cond_12

    const/16 v23, 0x0

    cmpl-float v23, v15, v23

    if-eqz v23, :cond_12

    if-nez v22, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInitialSpan:F

    move/from16 v23, v0

    sub-float v23, v15, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v23

    const/high16 v24, -0x40800000

    cmpl-float v23, v23, v24

    if-lez v23, :cond_12

    :cond_11
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanX:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanX:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanY:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanY:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpan:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;->onScaleBegin(Lcom/google/android/common/gesture/ScaleGestureDetector;)Z

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    :cond_12
    const/16 v23, 0x2

    move v0, v2

    move/from16 v1, v23

    if-ne v0, v1, :cond_14

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanX:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanY:F

    move v0, v15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    const/16 v21, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    if-eqz v23, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mListener:Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;->onScale(Lcom/google/android/common/gesture/ScaleGestureDetector;)Z

    move-result v21

    :cond_13
    if-eqz v21, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanX:F

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanX:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpanY:F

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpanY:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/ScaleGestureDetector;->mCurrSpan:F

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/ScaleGestureDetector;->mPrevSpan:F

    :cond_14
    const/16 v23, 0x1

    goto/16 :goto_1
.end method
