.class public Lcom/google/android/common/gesture/StreetViewGestureController;
.super Ljava/lang/Object;
.source "StreetViewGestureController.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;


# instance fields
.field private final gestureDetector:Lcom/google/android/common/gesture/GestureDetector;

.field private isMultiTouchSupported:Z

.field private scaleEventBeginTime:J

.field private final scaleEventListener:Lcom/google/android/common/gesture/ScaleEventListener;

.field private final scaleGestureDetector:Lcom/google/android/common/gesture/ScaleGestureDetector;

.field private scaleTotalSpan:F

.field private final screenDensity:F

.field private final wrappedDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private final wrappedGestureListener:Landroid/view/GestureDetector$OnGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/view/GestureDetector$OnDoubleTapListener;Lcom/google/android/common/gesture/ScaleEventListener;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/GestureDetector$OnGestureListener;
    .param p3    # Landroid/view/GestureDetector$OnDoubleTapListener;
    .param p4    # Lcom/google/android/common/gesture/ScaleEventListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->isMultiTouchSupported:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleTotalSpan:F

    iput-object p2, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    iput-object p3, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    new-instance v0, Lcom/google/android/common/gesture/GestureDetector;

    invoke-direct {v0, p1, p0}, Lcom/google/android/common/gesture/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->gestureDetector:Lcom/google/android/common/gesture/GestureDetector;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->gestureDetector:Lcom/google/android/common/gesture/GestureDetector;

    invoke-virtual {v0, p0}, Lcom/google/android/common/gesture/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->gestureDetector:Lcom/google/android/common/gesture/GestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/common/gesture/GestureDetector;->setIsLongpressEnabled(Z)V

    iput-object p4, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleEventListener:Lcom/google/android/common/gesture/ScaleEventListener;

    new-instance v0, Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Lcom/google/android/common/gesture/ScaleGestureDetector;-><init>(Landroid/content/Context;Lcom/google/android/common/gesture/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleGestureDetector:Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->screenDensity:F

    return-void
.end method

.method private isTwoFingerTap()Z
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleEventBeginTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x12c

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleTotalSpan:F

    const/high16 v1, 0x41b00000

    iget v2, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->screenDensity:F

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isMultiTouchSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->isMultiTouchSupported:Z

    return v0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public onScale(Lcom/google/android/common/gesture/ScaleGestureDetector;)Z
    .locals 3
    .param p1    # Lcom/google/android/common/gesture/ScaleGestureDetector;

    iget v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleTotalSpan:F

    invoke-virtual {p1}, Lcom/google/android/common/gesture/ScaleGestureDetector;->getCurrentSpan()F

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/common/gesture/ScaleGestureDetector;->getPreviousSpan()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleTotalSpan:F

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleEventListener:Lcom/google/android/common/gesture/ScaleEventListener;

    new-instance v1, Lcom/google/android/common/gesture/ScaleEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lcom/google/android/common/gesture/ScaleEvent;-><init>(ILcom/google/android/common/gesture/ScaleGestureDetector;)V

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleEventListener;->handleScaleEvent(Lcom/google/android/common/gesture/ScaleEvent;)Z

    move-result v0

    return v0
.end method

.method public onScaleBegin(Lcom/google/android/common/gesture/ScaleGestureDetector;)Z
    .locals 3
    .param p1    # Lcom/google/android/common/gesture/ScaleGestureDetector;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleTotalSpan:F

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleEventBeginTime:J

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleEventListener:Lcom/google/android/common/gesture/ScaleEventListener;

    new-instance v1, Lcom/google/android/common/gesture/ScaleEvent;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lcom/google/android/common/gesture/ScaleEvent;-><init>(ILcom/google/android/common/gesture/ScaleGestureDetector;)V

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleEventListener;->handleScaleEvent(Lcom/google/android/common/gesture/ScaleEvent;)Z

    move-result v0

    return v0
.end method

.method public onScaleEnd(Lcom/google/android/common/gesture/ScaleGestureDetector;)V
    .locals 3
    .param p1    # Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-direct {p0}, Lcom/google/android/common/gesture/StreetViewGestureController;->isTwoFingerTap()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleEventListener:Lcom/google/android/common/gesture/ScaleEventListener;

    invoke-interface {v0}, Lcom/google/android/common/gesture/ScaleEventListener;->handleTwoFingerTap()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleEventListener:Lcom/google/android/common/gesture/ScaleEventListener;

    new-instance v1, Lcom/google/android/common/gesture/ScaleEvent;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lcom/google/android/common/gesture/ScaleEvent;-><init>(ILcom/google/android/common/gesture/ScaleGestureDetector;)V

    invoke-interface {v0, v1}, Lcom/google/android/common/gesture/ScaleEventListener;->handleScaleEvent(Lcom/google/android/common/gesture/ScaleEvent;)Z

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->wrappedDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->gestureDetector:Lcom/google/android/common/gesture/GestureDetector;

    invoke-virtual {v0, p1}, Lcom/google/android/common/gesture/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->scaleGestureDetector:Lcom/google/android/common/gesture/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Lcom/google/android/common/gesture/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public setIsLongpressEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->gestureDetector:Lcom/google/android/common/gesture/GestureDetector;

    invoke-virtual {v0, p1}, Lcom/google/android/common/gesture/GestureDetector;->setIsLongpressEnabled(Z)V

    return-void
.end method

.method public setMultiTouchSupported(Landroid/content/pm/PackageManager;)V
    .locals 1
    .param p1    # Landroid/content/pm/PackageManager;

    const-string v0, "android.hardware.touchscreen.multitouch"

    invoke-virtual {p1, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/common/gesture/StreetViewGestureController;->isMultiTouchSupported:Z

    return-void
.end method
