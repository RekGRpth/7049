.class Lcom/google/android/street/ExponentialFilter;
.super Ljava/lang/Object;
.source "ExponentialFilter.java"


# instance fields
.field private final mAlpha:F

.field private final mExponent:I


# direct methods
.method public constructor <init>(FI)V
    .locals 2
    .param p1    # F
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "exponent must be >= 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_1

    const/high16 v0, 0x3f800000

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "alpha must be in [0 .. 1]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput p1, p0, Lcom/google/android/street/ExponentialFilter;->mAlpha:F

    iput p2, p0, Lcom/google/android/street/ExponentialFilter;->mExponent:I

    return-void
.end method


# virtual methods
.method public filter([F[F)V
    .locals 6
    .param p1    # [F
    .param p2    # [F

    array-length v4, p2

    array-length v5, p1

    if-eq v4, v5, :cond_0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "length mismatch"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const/4 v2, 0x0

    :goto_0
    array-length v4, p2

    if-ge v2, v4, :cond_2

    aget v4, p2, v2

    aget v5, p1, v2

    sub-float v1, v4, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v4, 0x3f800000

    cmpg-float v4, v0, v4

    if-gez v4, :cond_1

    const/4 v3, 0x1

    :goto_1
    iget v4, p0, Lcom/google/android/street/ExponentialFilter;->mExponent:I

    if-ge v3, v4, :cond_1

    mul-float/2addr v1, v0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    aget v4, p1, v2

    iget v5, p0, Lcom/google/android/street/ExponentialFilter;->mAlpha:F

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    aput v4, p1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
