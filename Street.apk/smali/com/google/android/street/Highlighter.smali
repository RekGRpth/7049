.class public Lcom/google/android/street/Highlighter;
.super Ljava/lang/Object;
.source "Highlighter.java"


# instance fields
.field private mCurrentItemIsHighlighted:Z

.field private mCurrentlyPressedItem:I

.field private mIsTracking:Z

.field private mRenderer:Lcom/google/android/street/Renderer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/google/android/street/Highlighter;->reset()V

    return-void
.end method

.method private isReady()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reset()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/street/Highlighter;->mIsTracking:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/street/Highlighter;->mCurrentlyPressedItem:I

    iput-boolean v1, p0, Lcom/google/android/street/Highlighter;->mCurrentItemIsHighlighted:Z

    return-void
.end method


# virtual methods
.method public down(FF)Z
    .locals 5
    .param p1    # F
    .param p2    # F

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/google/android/street/Highlighter;->isReady()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    float-to-int v2, p1

    float-to-int v3, p2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/street/Renderer;->hit(II)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iput-boolean v4, p0, Lcom/google/android/street/Highlighter;->mIsTracking:Z

    iput v0, p0, Lcom/google/android/street/Highlighter;->mCurrentlyPressedItem:I

    iget-object v1, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    iget v2, p0, Lcom/google/android/street/Highlighter;->mCurrentlyPressedItem:I

    invoke-virtual {v1, v2}, Lcom/google/android/street/Renderer;->setHighlight(I)V

    iput-boolean v4, p0, Lcom/google/android/street/Highlighter;->mCurrentItemIsHighlighted:Z

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/street/Highlighter;->mIsTracking:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/street/Highlighter;->reset()V

    goto :goto_1
.end method

.method public getCurrentlyPressedItem()I
    .locals 1

    iget v0, p0, Lcom/google/android/street/Highlighter;->mCurrentlyPressedItem:I

    return v0
.end method

.method public isTracking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/street/Highlighter;->mIsTracking:Z

    return v0
.end method

.method public move(FF)Z
    .locals 5
    .param p1    # F
    .param p2    # F

    invoke-direct {p0}, Lcom/google/android/street/Highlighter;->isReady()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/street/Highlighter;->mIsTracking:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    float-to-int v3, p1

    float-to-int v4, p2

    invoke-virtual {v2, v3, v4}, Lcom/google/android/street/Renderer;->hit(II)I

    move-result v0

    iget v2, p0, Lcom/google/android/street/Highlighter;->mCurrentlyPressedItem:I

    if-ne v0, v2, :cond_1

    const/4 v2, 0x1

    move v1, v2

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/street/Highlighter;->mCurrentItemIsHighlighted:Z

    if-eq v1, v2, :cond_0

    iput-boolean v1, p0, Lcom/google/android/street/Highlighter;->mCurrentItemIsHighlighted:Z

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    iget v3, p0, Lcom/google/android/street/Highlighter;->mCurrentlyPressedItem:I

    invoke-virtual {v2, v3}, Lcom/google/android/street/Renderer;->setHighlight(I)V

    :cond_0
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/street/Highlighter;->mIsTracking:Z

    return v2

    :cond_1
    const/4 v2, 0x0

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/google/android/street/Renderer;->setHighlight(I)V

    goto :goto_1
.end method

.method public setRenderer(Lcom/google/android/street/Renderer;)V
    .locals 0
    .param p1    # Lcom/google/android/street/Renderer;

    iput-object p1, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-direct {p0}, Lcom/google/android/street/Highlighter;->reset()V

    return-void
.end method

.method public up(FF)I
    .locals 5
    .param p1    # F
    .param p2    # F

    const/4 v4, -0x1

    invoke-direct {p0}, Lcom/google/android/street/Highlighter;->isReady()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/street/Highlighter;->mIsTracking:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/street/Highlighter;->mCurrentItemIsHighlighted:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    invoke-virtual {v1, v4}, Lcom/google/android/street/Renderer;->setHighlight(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/street/Highlighter;->mRenderer:Lcom/google/android/street/Renderer;

    float-to-int v2, p1

    float-to-int v3, p2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/street/Renderer;->hit(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/street/Highlighter;->mCurrentlyPressedItem:I

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/street/Highlighter;->reset()V

    move v1, v0

    :goto_0
    return v1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/street/Highlighter;->reset()V

    move v1, v4

    goto :goto_0
.end method
