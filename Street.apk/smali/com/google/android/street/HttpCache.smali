.class public Lcom/google/android/street/HttpCache;
.super Ljava/lang/Object;
.source "HttpCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/HttpCache$LruHttpCache;,
        Lcom/google/android/street/HttpCache$CacheEntry;,
        Lcom/google/android/street/HttpCache$AbortableInputStream;,
        Lcom/google/android/street/HttpCache$Aborter;
    }
.end annotation


# static fields
.field private static msHttpClient:Lorg/apache/http/client/HttpClient;


# instance fields
.field private mCacheDir:Ljava/io/File;

.field private final mCacheDirPath:Ljava/lang/String;

.field private final mDownloading:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIndex:Lcom/google/android/street/HttpCache$LruHttpCache;

.field private final mMaxHttpRequests:I


# direct methods
.method public constructor <init>(ILjava/lang/String;I)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/HttpCache;->mDownloading:Ljava/util/HashSet;

    new-instance v0, Lcom/google/android/street/HttpCache$LruHttpCache;

    invoke-direct {v0, p3}, Lcom/google/android/street/HttpCache$LruHttpCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/street/HttpCache;->mIndex:Lcom/google/android/street/HttpCache$LruHttpCache;

    iput p1, p0, Lcom/google/android/street/HttpCache;->mMaxHttpRequests:I

    iput-object p2, p0, Lcom/google/android/street/HttpCache;->mCacheDirPath:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/street/HttpCache;->initializeCache()V

    return-void
.end method

.method private cacheFileRef(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/street/HttpCache;->mCacheDir:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static checkInterrupts(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0, p0}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public static closeStream(Ljava/io/Closeable;)V
    .locals 1
    .param p0    # Ljava/io/Closeable;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private createUncachedUrlStream(Ljava/lang/String;Lcom/google/android/street/HttpCache$Aborter;)Ljava/io/InputStream;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/street/HttpCache$Aborter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v3, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    :try_start_1
    invoke-virtual {p2, v3}, Lcom/google/android/street/HttpCache$Aborter;->add(Lorg/apache/http/client/methods/AbortableHttpRequest;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/street/HttpCache;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-interface {v6, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    const/16 v7, 0xc8

    if-eq v6, v7, :cond_2

    new-instance v6, Ljava/io/IOException;

    const-string v7, "Bad status code"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catch_0
    move-exception v6

    move-object v0, v6

    move-object v2, v3

    :goto_0
    :try_start_2
    new-instance v6, Ljava/io/IOException;

    const-string v7, "IllegalStateException"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v6

    :goto_1
    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2, v2}, Lcom/google/android/street/HttpCache$Aborter;->remove(Lorg/apache/http/client/methods/AbortableHttpRequest;)V

    :cond_1
    throw v6

    :cond_2
    :try_start_3
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    invoke-static {v6, p2, v3}, Lcom/google/android/street/HttpCache$AbortableInputStream;->getStream(Ljava/io/InputStream;Lcom/google/android/street/HttpCache$Aborter;Lorg/apache/http/client/methods/AbortableHttpRequest;)Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v5

    const/4 v2, 0x0

    if-eqz v2, :cond_3

    if-eqz p2, :cond_3

    invoke-virtual {p2, v2}, Lcom/google/android/street/HttpCache$Aborter;->remove(Lorg/apache/http/client/methods/AbortableHttpRequest;)V

    :cond_3
    return-object v5

    :catchall_1
    move-exception v6

    move-object v2, v3

    goto :goto_1

    :catch_1
    move-exception v6

    move-object v0, v6

    goto :goto_0
.end method

.method private fetchToCache(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/street/HttpCache$Aborter;)[B
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/street/HttpCache$Aborter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/street/HttpCache;->mDownloading:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v2

    if-eqz v1, :cond_0

    monitor-enter p0

    :try_start_3
    iget-object v3, p0, Lcom/google/android/street/HttpCache;->mDownloading:Ljava/util/HashSet;

    invoke-virtual {v3, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    :cond_0
    throw v2

    :cond_1
    :try_start_4
    iget-object v2, p0, Lcom/google/android/street/HttpCache;->mIndex:Lcom/google/android/street/HttpCache$LruHttpCache;

    invoke-virtual {v2, p2}, Lcom/google/android/street/HttpCache$LruHttpCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/street/HttpCache;->mDownloading:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    const-string v2, "fetchToCache"

    invoke-static {v2}, Lcom/google/android/street/HttpCache;->checkInterrupts(Ljava/lang/String;)V

    invoke-direct {p0, p1, p3}, Lcom/google/android/street/HttpCache;->readUncachedUrl(Ljava/lang/String;Lcom/google/android/street/HttpCache$Aborter;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    invoke-direct {p0, v0, p2}, Lcom/google/android/street/HttpCache;->saveToCacheInternal([BLjava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_2
    if-eqz v1, :cond_3

    monitor-enter p0

    :try_start_6
    iget-object v2, p0, Lcom/google/android/street/HttpCache;->mDownloading:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :cond_3
    move-object v2, v0

    :cond_4
    :goto_1
    return-object v2

    :cond_5
    const-wide/32 v2, 0x36ee80

    :try_start_7
    invoke-direct {p0, p2, v2, v3}, Lcom/google/android/street/HttpCache;->getCachedFile(Ljava/lang/String;J)[B

    move-result-object v2

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v1, :cond_4

    monitor-enter p0

    :try_start_8
    iget-object v3, p0, Lcom/google/android/street/HttpCache;->mDownloading:Ljava/util/HashSet;

    invoke-virtual {v3, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0

    goto :goto_1

    :catchall_2
    move-exception v2

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v2

    :catchall_3
    move-exception v2

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v2

    :catchall_4
    move-exception v2

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v2
.end method

.method private declared-synchronized getCachedFile(Ljava/lang/String;J)[B
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/HttpCache;->mIndex:Lcom/google/android/street/HttpCache$LruHttpCache;

    invoke-virtual {v1, p1}, Lcom/google/android/street/HttpCache$LruHttpCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/street/HttpCache$CacheEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/street/HttpCache;->readCachedFile(Lcom/google/android/street/HttpCache$CacheEntry;J)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private static getCurrentTime()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private static declared-synchronized getHttpClientImp(I)Lorg/apache/http/client/HttpClient;
    .locals 8
    .param p0    # I

    const-class v3, Lcom/google/android/street/HttpCache;

    monitor-enter v3

    :try_start_0
    sget-object v4, Lcom/google/android/street/HttpCache;->msHttpClient:Lorg/apache/http/client/HttpClient;

    if-nez v4, :cond_0

    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    const/16 v4, 0x4e20

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    const/16 v4, 0x4e20

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    const/16 v4, 0x2000

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    const-string v4, "Android StreetView"

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    new-instance v2, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v6

    const/16 v7, 0x50

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v6

    const/16 v7, 0x1bb

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v2, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    new-instance v4, Lorg/apache/http/conn/params/ConnPerRouteBean;

    invoke-direct {v4, p0}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {v1, v4}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    new-instance v4, Lcom/google/android/street/HttpCache$1;

    invoke-direct {v4, v0, v1}, Lcom/google/android/street/HttpCache$1;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    sput-object v4, Lcom/google/android/street/HttpCache;->msHttpClient:Lorg/apache/http/client/HttpClient;

    :cond_0
    sget-object v4, Lcom/google/android/street/HttpCache;->msHttpClient:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-object v4

    :catchall_0
    move-exception v4

    monitor-exit v3

    throw v4
.end method

.method private declared-synchronized initializeCache()V
    .locals 20

    monitor-enter p0

    :try_start_0
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/HttpCache;->mCacheDirPath:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object v0, v3

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v18

    if-nez v18, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v18

    if-nez v18, :cond_1

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Could not open cache directory "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/HttpCache;->mCacheDirPath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/android/street/HttpCache;->getCurrentTime()J

    move-result-wide v10

    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/android/street/HttpCache;->getCurrentTime()J

    move-result-wide v12

    move-object v0, v3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/street/HttpCache;->mCacheDir:Ljava/io/File;

    move-object v0, v6

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v0, v0, [Lcom/google/android/street/HttpCache$CacheEntry;

    move-object v4, v0

    const/4 v7, 0x0

    :goto_0
    move-object v0, v6

    array-length v0, v0

    move/from16 v18, v0

    move v0, v7

    move/from16 v1, v18

    if-ge v0, v1, :cond_2

    new-instance v18, Lcom/google/android/street/HttpCache$CacheEntry;

    aget-object v19, v6, v7

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/street/HttpCache;->cacheFileRef(Ljava/lang/String;)Ljava/io/File;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lcom/google/android/street/HttpCache$CacheEntry;-><init>(Ljava/io/File;)V

    aput-object v18, v4, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/android/street/HttpCache;->getCurrentTime()J

    move-result-wide v14

    invoke-static {v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/android/street/HttpCache;->getCurrentTime()J

    move-result-wide v16

    move-object v2, v4

    array-length v9, v2

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v9, :cond_0

    aget-object v5, v2, v8

    move-object v0, v5

    iget-object v0, v0, Lcom/google/android/street/HttpCache$CacheEntry;->mFile:Ljava/io/File;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->isFile()Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/HttpCache;->mIndex:Lcom/google/android/street/HttpCache$LruHttpCache;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/street/HttpCache$LruHttpCache;->insert(Lcom/google/android/street/HttpCache$CacheEntry;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :catchall_0
    move-exception v18

    monitor-exit p0

    throw v18
.end method

.method private readCachedFile(Lcom/google/android/street/HttpCache$CacheEntry;J)[B
    .locals 10
    .param p1    # Lcom/google/android/street/HttpCache$CacheEntry;
    .param p2    # J

    const/4 v9, 0x0

    invoke-static {}, Lcom/google/android/street/HttpCache;->getCurrentTime()J

    move-result-wide v4

    iget-wide v7, p1, Lcom/google/android/street/HttpCache$CacheEntry;->mLastModifiedTime:J

    sub-long v7, v4, v7

    cmp-long v7, v7, p2

    if-lez v7, :cond_0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Expired cache file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/street/HttpCache;->removeCacheEntry(Lcom/google/android/street/HttpCache$CacheEntry;)V

    move-object v7, v9

    :goto_0
    return-object v7

    :cond_0
    const/4 v1, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/street/HttpCache$CacheEntry;->openData()Ljava/io/DataInputStream;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/street/HttpCache$CacheEntry;->length()J

    move-result-wide v7

    long-to-int v7, v7

    new-array v0, v7, [B

    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v6, 0x1

    invoke-static {v1}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    if-nez v6, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/street/HttpCache;->removeCacheEntry(Lcom/google/android/street/HttpCache$CacheEntry;)V

    :cond_1
    move-object v7, v0

    goto :goto_0

    :catch_0
    move-exception v7

    move-object v3, v7

    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not find cache file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    if-nez v6, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/street/HttpCache;->removeCacheEntry(Lcom/google/android/street/HttpCache$CacheEntry;)V

    :cond_2
    :goto_1
    move-object v7, v9

    goto :goto_0

    :catch_1
    move-exception v7

    move-object v2, v7

    :try_start_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Could not read cache file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    if-nez v6, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/street/HttpCache;->removeCacheEntry(Lcom/google/android/street/HttpCache$CacheEntry;)V

    goto :goto_1

    :catchall_0
    move-exception v7

    invoke-static {v1}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    if-nez v6, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/street/HttpCache;->removeCacheEntry(Lcom/google/android/street/HttpCache$CacheEntry;)V

    :cond_3
    throw v7
.end method

.method private readUncachedUrl(Ljava/lang/String;Lcom/google/android/street/HttpCache$Aborter;)[B
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/street/HttpCache$Aborter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x2000

    const/4 v4, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/street/HttpCache;->createUncachedUrlStream(Ljava/lang/String;Lcom/google/android/street/HttpCache$Aborter;)Ljava/io/InputStream;

    move-result-object v4

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v5, 0x2000

    invoke-direct {v1, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const/16 v5, 0x2000

    new-array v2, v5, [B

    :goto_0
    invoke-virtual {v4, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/street/HttpCache$Aborter;->aborted()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    invoke-static {v4}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    :goto_1
    return-object v5

    :cond_0
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v1, v2, v5, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-static {v4}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    throw v5

    :cond_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    invoke-static {v4}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1
.end method

.method private declared-synchronized removeCacheEntry(Lcom/google/android/street/HttpCache$CacheEntry;)V
    .locals 2
    .param p1    # Lcom/google/android/street/HttpCache$CacheEntry;

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/street/HttpCache;->mIndex:Lcom/google/android/street/HttpCache$LruHttpCache;

    iget-object v1, p1, Lcom/google/android/street/HttpCache$CacheEntry;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/street/HttpCache$LruHttpCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private saveToCacheInternal([BLjava/lang/String;)V
    .locals 7
    .param p1    # [B
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/street/HttpCache;->cacheFileRef(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    monitor-enter p0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v3, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v3}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v2, 0x0

    :try_start_3
    iget-object v4, p0, Lcom/google/android/street/HttpCache;->mIndex:Lcom/google/android/street/HttpCache$LruHttpCache;

    new-instance v5, Lcom/google/android/street/HttpCache$CacheEntry;

    invoke-direct {v5, v1}, Lcom/google/android/street/HttpCache$CacheEntry;-><init>(Ljava/io/File;)V

    invoke-virtual {v4, v5}, Lcom/google/android/street/HttpCache$LruHttpCache;->insert(Lcom/google/android/street/HttpCache$CacheEntry;)V

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v1, 0x0

    invoke-static {v2}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    return-void

    :catchall_0
    move-exception v4

    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v4
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v4

    move-object v0, v4

    :try_start_6
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t create cache file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v4

    invoke-static {v2}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_1
    throw v4

    :catchall_2
    move-exception v4

    move-object v2, v3

    goto :goto_0
.end method


# virtual methods
.method getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 1

    iget v0, p0, Lcom/google/android/street/HttpCache;->mMaxHttpRequests:I

    invoke-static {v0}, Lcom/google/android/street/HttpCache;->getHttpClientImp(I)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized isInCache(Ljava/lang/String;J)Z
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # J

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x5

    if-ge v3, v4, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "persistentKey"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_1
    const-wide/16 v3, 0x1

    cmp-long v3, p2, v3

    if-gez v3, :cond_2

    :try_start_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "shelfLife"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-static {}, Lcom/google/android/street/HttpCache;->getCurrentTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/street/HttpCache;->mIndex:Lcom/google/android/street/HttpCache$LruHttpCache;

    invoke-virtual {v3, p1}, Lcom/google/android/street/HttpCache$LruHttpCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/street/HttpCache$CacheEntry;

    if-eqz v0, :cond_3

    iget-wide v3, v0, Lcom/google/android/street/HttpCache$CacheEntry;->mLastModifiedTime:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long v3, v1, v3

    cmp-long v3, v3, p2

    if-gtz v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    monitor-exit p0

    return v3

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public read(Ljava/lang/String;ZLcom/google/android/street/HttpCache$Aborter;Ljava/lang/String;J)[B
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Lcom/google/android/street/HttpCache$Aborter;
    .param p4    # Ljava/lang/String;
    .param p5    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x8

    if-ge v1, v2, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "url"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "persistentKey"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const-wide/16 v1, 0x1

    cmp-long v1, p5, v1

    if-gez v1, :cond_3

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "shelfLife"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    iget-object v1, p0, Lcom/google/android/street/HttpCache;->mCacheDir:Ljava/io/File;

    if-eqz v1, :cond_6

    if-eqz p4, :cond_6

    invoke-direct {p0, p4, p5, p6}, Lcom/google/android/street/HttpCache;->getCachedFile(Ljava/lang/String;J)[B

    move-result-object v0

    if-eqz v0, :cond_4

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_4
    const-string v1, "read"

    invoke-static {v1}, Lcom/google/android/street/HttpCache;->checkInterrupts(Ljava/lang/String;)V

    if-nez p2, :cond_5

    move-object v1, v3

    goto :goto_0

    :cond_5
    invoke-direct {p0, p1, p4, p3}, Lcom/google/android/street/HttpCache;->fetchToCache(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/street/HttpCache$Aborter;)[B

    move-result-object v0

    if-eqz v0, :cond_6

    move-object v1, v0

    goto :goto_0

    :cond_6
    if-nez p2, :cond_7

    move-object v1, v3

    goto :goto_0

    :cond_7
    invoke-direct {p0, p1, p3}, Lcom/google/android/street/HttpCache;->readUncachedUrl(Ljava/lang/String;Lcom/google/android/street/HttpCache$Aborter;)[B

    move-result-object v1

    goto :goto_0
.end method

.method public saveToCache([BLjava/lang/String;)V
    .locals 2
    .param p1    # [B
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "persistentKey"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/HttpCache;->mCacheDir:Ljava/io/File;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/street/HttpCache;->saveToCacheInternal([BLjava/lang/String;)V

    :cond_2
    return-void
.end method
