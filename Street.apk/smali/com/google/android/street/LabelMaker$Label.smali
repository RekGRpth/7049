.class Lcom/google/android/street/LabelMaker$Label;
.super Ljava/lang/Object;
.source "LabelMaker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/LabelMaker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Label"
.end annotation


# instance fields
.field public final baseline:F

.field public final grid:Lcom/google/android/street/Grid;

.field public final height:F

.field public final mCrop:[I

.field public final width:F


# direct methods
.method public constructor <init>(Lcom/google/android/street/Grid;FFFIIII)V
    .locals 2
    .param p1    # Lcom/google/android/street/Grid;
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/street/LabelMaker$Label;->grid:Lcom/google/android/street/Grid;

    iput p2, p0, Lcom/google/android/street/LabelMaker$Label;->width:F

    iput p3, p0, Lcom/google/android/street/LabelMaker$Label;->height:F

    iput p4, p0, Lcom/google/android/street/LabelMaker$Label;->baseline:F

    const/4 v1, 0x4

    new-array v0, v1, [I

    const/4 v1, 0x0

    aput p5, v0, v1

    const/4 v1, 0x1

    aput p6, v0, v1

    const/4 v1, 0x2

    aput p7, v0, v1

    const/4 v1, 0x3

    aput p8, v0, v1

    iput-object v0, p0, Lcom/google/android/street/LabelMaker$Label;->mCrop:[I

    return-void
.end method
