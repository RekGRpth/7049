.class public Lcom/google/android/street/Overlay$Pancake;
.super Ljava/lang/Object;
.source "Overlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/Overlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Pancake"
.end annotation


# instance fields
.field public final mDistance:F

.field public final mIsGround:Z

.field public final mNx:F

.field public final mNy:F

.field public final mNz:F

.field public final mPitch:F

.field public final mYaw:F


# direct methods
.method public constructor <init>(FFFFFFZ)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F
    .param p7    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/street/Overlay$Pancake;->mNx:F

    iput p2, p0, Lcom/google/android/street/Overlay$Pancake;->mNy:F

    iput p3, p0, Lcom/google/android/street/Overlay$Pancake;->mNz:F

    iput p4, p0, Lcom/google/android/street/Overlay$Pancake;->mDistance:F

    iput p5, p0, Lcom/google/android/street/Overlay$Pancake;->mYaw:F

    iput p6, p0, Lcom/google/android/street/Overlay$Pancake;->mPitch:F

    iput-boolean p7, p0, Lcom/google/android/street/Overlay$Pancake;->mIsGround:Z

    return-void
.end method
