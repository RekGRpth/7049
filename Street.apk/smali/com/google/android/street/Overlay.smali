.class Lcom/google/android/street/Overlay;
.super Ljava/lang/Object;
.source "Overlay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/Overlay$Pancake;,
        Lcom/google/android/street/Overlay$HitTester;,
        Lcom/google/android/street/Overlay$Polygon;,
        Lcom/google/android/street/Overlay$FadeAnimation;,
        Lcom/google/android/street/Overlay$Label;
    }
.end annotation


# static fields
.field private static final ARROW_DATA:[F

.field private static final BAR_DATA:[F

.field private static final COS_NO_SHOW:F

.field private static final COS_SHOW_BELOW:F

.field private static final GROUND_PANCAKE_DATA:[F

.field private static final GROUND_PANCAKE_DATA_FILL_INDICES:[B

.field private static final GROUND_PANCAKE_DATA_OUTLINE_INDICES:[B

.field private static final GROUND_PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

.field private static final INTERIOR_ARROW_DATA:[F

.field private static final INTERIOR_ARROW_SHADOW_DATA:[F

.field private static final PANCAKE_DATA:[F

.field private static final PANCAKE_DATA_FILL_INDICES:[B

.field private static final PANCAKE_DATA_OUTLINE_INDICES:[B

.field private static final PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

.field private static final PYRAMID_DATA:[F

.field private static final PYRAMID_FILL_INDEX:[B

.field private static final PYRAMID_OUTLINE_INDEX:[B

.field private static final STREET_ANCHOR:[F

.field private static final STREET_ANCHOR_DIR_ARROW:[F


# instance fields
.field private m3DLabelMaker:Lcom/google/android/street/LabelMaker;

.field private mAddressBubble:[Landroid/graphics/drawable/Drawable;

.field private final mArrow:Lcom/google/android/street/Overlay$Polygon;

.field private mAspectRatio:F

.field private final mCompassDirectionNames:[Ljava/lang/CharSequence;

.field private mConfig:Lcom/google/android/street/PanoramaConfig;

.field private final mContext:Landroid/content/Context;

.field private final mDirectionPaint:Landroid/graphics/Paint;

.field private mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

.field private final mDisplayDensity:F

.field private mDrawDisabled:Z

.field private mDrawRoadLabels:Z

.field private final mEnablePancake:Z

.field private final mEnablePanoPoints:Z

.field private mFancyStreetLabelIds:[[I

.field private mHighlight:I

.field private final mHitTesterLock:Ljava/lang/Object;

.field private mIncomingLink:Lcom/google/android/street/PanoramaLink;

.field private mIncomingYaw:F

.field private final mInteriorArrow:Lcom/google/android/street/Overlay$Polygon;

.field private final mInteriorArrowShadow:Lcom/google/android/street/Overlay$Polygon;

.field private mIsIndoorScene:Z

.field private final mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

.field private mLabelMaker:Lcom/google/android/street/LabelMaker;

.field private mLabelsComputed:Z

.field private mLabelsInitialized:Z

.field private mLastTrackballTime:J

.field private mLinks:[Lcom/google/android/street/PanoramaLink;

.field private mNextDrawTime:J

.field private mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

.field private mOutgoingLink:Lcom/google/android/street/PanoramaLink;

.field private mOutgoingYaw:F

.field private mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

.field private mPegmanOnPancakeLabelId:I

.field private final mProjector:Lcom/google/android/street/Projector;

.field private mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

.field private final mRoad:Lcom/google/android/street/Overlay$Polygon;

.field private final mScratch:[F

.field private mSelectedLink:I

.field private final mStreetOutlinePaint:Landroid/graphics/Paint;

.field private final mStreetPaint:Landroid/graphics/Paint;

.field private mStreets:[[Lcom/google/android/street/Overlay$Label;

.field private mTouchUsed:Z

.field private mTrackballUsed:Z

.field private mViewHeight:I

.field private mViewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/16 v6, 0x20

    const/16 v1, 0x12

    const/16 v5, 0xc

    const/4 v4, 0x4

    new-array v0, v5, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/street/Overlay;->BAR_DATA:[F

    const/16 v0, 0x15

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/street/Overlay;->ARROW_DATA:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/street/Overlay;->INTERIOR_ARROW_DATA:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/street/Overlay;->INTERIOR_ARROW_SHADOW_DATA:[F

    new-array v0, v5, [F

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/street/Overlay;->PANCAKE_DATA:[F

    new-array v0, v4, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/street/Overlay;->PANCAKE_DATA_FILL_INDICES:[B

    new-array v0, v4, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/street/Overlay;->PANCAKE_DATA_OUTLINE_INDICES:[B

    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->PANCAKE_DATA:[F

    sget-object v2, Lcom/google/android/street/Overlay;->PANCAKE_DATA_FILL_INDICES:[B

    sget-object v3, Lcom/google/android/street/Overlay;->PANCAKE_DATA_OUTLINE_INDICES:[B

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    sput-object v0, Lcom/google/android/street/Overlay;->PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

    const/high16 v0, 0x40400000

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/street/Overlay;->getCircle(FI)[F

    move-result-object v0

    sput-object v0, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA:[F

    invoke-static {v6}, Lcom/google/android/street/Overlay;->getPolygonStripIndices(I)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA_FILL_INDICES:[B

    invoke-static {v6}, Lcom/google/android/street/Overlay;->getRange(I)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA_OUTLINE_INDICES:[B

    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA:[F

    sget-object v2, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA_FILL_INDICES:[B

    sget-object v3, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA_OUTLINE_INDICES:[B

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    sput-object v0, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

    const/16 v0, 0xf

    new-array v0, v0, [F

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/street/Overlay;->PYRAMID_DATA:[F

    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/street/Overlay;->PYRAMID_FILL_INDEX:[B

    new-array v0, v5, [B

    fill-array-data v0, :array_9

    sput-object v0, Lcom/google/android/street/Overlay;->PYRAMID_OUTLINE_INDEX:[B

    new-array v0, v4, [F

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/street/Overlay;->STREET_ANCHOR:[F

    new-array v0, v4, [F

    fill-array-data v0, :array_b

    sput-object v0, Lcom/google/android/street/Overlay;->STREET_ANCHOR_DIR_ARROW:[F

    const/high16 v0, 0x42d20000

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, Lcom/google/android/street/Overlay;->COS_NO_SHOW:F

    const/high16 v0, 0x428c0000

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, Lcom/google/android/street/Overlay;->COS_SHOW_BELOW:F

    return-void

    nop

    :array_0
    .array-data 4
        0x3d6bedfa
        0x3e4ccccd
        0x0
        0x3d6bedfa
        0x3e4ccccd
        0x41a00000
        -0x42941206
        0x3e4ccccd
        0x41a00000
        -0x42941206
        0x3e4ccccd
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3e570a3e
        0x3ee1c582
        -0x41c538ef
        0x3e570a3e
        0x3e394d94
        -0x42941206
        0x3e570a3e
        0x3e394d94
        -0x42941206
        0x3e570a3e
        0x0
        0x3d6bedfa
        0x3e570a3e
        0x0
        0x3d6bedfa
        0x3e570a3e
        0x3e394d94
        0x3e3ac711
        0x3e570a3e
        0x3e394d94
    .end array-data

    :array_2
    .array-data 4
        0x0
        -0x40b33333
        0x3ed78812
        -0x41c538ef
        -0x40b33333
        0x3e9a176e
        -0x41c538ef
        -0x40b33333
        0x3e4ccccd
        0x0
        -0x40b33333
        0x3ea3d70a
        0x3e3ac711
        -0x40b33333
        0x3e4ccccd
        0x3e3ac711
        -0x40b33333
        0x3e9a176e
    .end array-data

    :array_3
    .array-data 4
        0x0
        -0x40a8f5c2
        0x3ed78812
        -0x41c538ef
        -0x40a8f5c2
        0x3e9a176e
        -0x41c538ef
        -0x40a8f5c2
        0x3e4ccccd
        0x0
        -0x40a8f5c2
        0x3ea3d70a
        0x3e3ac711
        -0x40a8f5c2
        0x3e4ccccd
        0x3e3ac711
        -0x40a8f5c2
        0x3e9a176e
    .end array-data

    :array_4
    .array-data 4
        0x40400000
        0x40400000
        0x0
        -0x3fc00000
        0x40400000
        0x0
        -0x3fc00000
        -0x3fc00000
        0x0
        0x40400000
        -0x3fc00000
        0x0
    .end array-data

    :array_5
    .array-data 1
        0x0t
        0x1t
        0x3t
        0x2t
    .end array-data

    :array_6
    .array-data 1
        0x0t
        0x1t
        0x2t
        0x3t
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x3f800000
        0x0
        0x3f800000
        0x0
        0x3f800000
        0x3f800000
        0x0
        -0x40800000
        -0x40800000
        0x0
        -0x40800000
        -0x40800000
        0x0
        0x3f800000
    .end array-data

    :array_8
    .array-data 1
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x0t
    .end array-data

    nop

    :array_9
    .array-data 1
        0x0t
        0x1t
        0x2t
        0x0t
        0x2t
        0x3t
        0x0t
        0x3t
        0x4t
        0x0t
        0x4t
        0x1t
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x3e570a3e
        0x3de1c582
        0x3f800000
    .end array-data

    :array_b
    .array-data 4
        0x0
        0x3e570a3e
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/street/Projector;ZZZ)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/street/Projector;
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z

    const/high16 v0, -0x40800000

    const/16 v5, 0x60

    const/4 v4, 0x1

    const/16 v3, 0xff

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    iput v0, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    iput-object p1, p0, Lcom/google/android/street/Overlay;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->BAR_DATA:[F

    invoke-direct {v0, v1}, Lcom/google/android/street/Overlay$Polygon;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mRoad:Lcom/google/android/street/Overlay$Polygon;

    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->ARROW_DATA:[F

    invoke-direct {v0, v1}, Lcom/google/android/street/Overlay$Polygon;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mArrow:Lcom/google/android/street/Overlay$Polygon;

    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->INTERIOR_ARROW_DATA:[F

    invoke-direct {v0, v1}, Lcom/google/android/street/Overlay$Polygon;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mInteriorArrow:Lcom/google/android/street/Overlay$Polygon;

    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->INTERIOR_ARROW_SHADOW_DATA:[F

    invoke-direct {v0, v1}, Lcom/google/android/street/Overlay$Polygon;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mInteriorArrowShadow:Lcom/google/android/street/Overlay$Polygon;

    iput-object p2, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    iput-boolean p5, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    invoke-direct {p0}, Lcom/google/android/street/Overlay;->createLabelMakers()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mCompassDirectionNames:[Ljava/lang/CharSequence;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x42700000

    iget v2, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    const/16 v1, 0x40

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000

    iget v2, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41600000

    iget v2, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    new-instance v0, Lcom/google/android/street/Overlay$HitTester;

    invoke-direct {v0}, Lcom/google/android/street/Overlay$HitTester;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    new-instance v0, Lcom/google/android/street/Overlay$HitTester;

    invoke-direct {v0}, Lcom/google/android/street/Overlay$HitTester;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mHitTesterLock:Ljava/lang/Object;

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mScratch:[F

    new-instance v0, Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-direct {v0, v4}, Lcom/google/android/street/Overlay$FadeAnimation;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    iput-boolean p3, p0, Lcom/google/android/street/Overlay;->mEnablePancake:Z

    iput-boolean p4, p0, Lcom/google/android/street/Overlay;->mEnablePanoPoints:Z

    return-void
.end method

.method private addLabels(Ljavax/microedition/khronos/opengles/GL10;Z)V
    .locals 12

    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsComputed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/street/Overlay;->createLabelMakers()V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsInitialized:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->initialize(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->initialize(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsInitialized:Z

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-eqz v0, :cond_5

    if-nez p2, :cond_5

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/16 v1, 0x200

    const/16 v2, 0x400

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/street/LabelMaker;->beginAdding(Ljavax/microedition/khronos/opengles/GL10;II)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v8, v0

    const/4 v0, 0x3

    filled-new-array {v8, v0}, [I

    move-result-object v0

    const-class v1, Lcom/google/android/street/Overlay$Label;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/google/android/street/Overlay$Label;

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v8, :cond_6

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v0, v0, v9

    iget-object v1, v0, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/google/android/street/Overlay;->getDirectionText(Lcom/google/android/street/PanoramaLink;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    move v10, v0

    :goto_2
    const/4 v0, 0x3

    if-ge v10, v0, :cond_4

    new-instance v11, Lcom/google/android/street/Overlay$Label;

    invoke-direct {v11}, Lcom/google/android/street/Overlay$Label;-><init>()V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v1, v10

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x20

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/street/LabelMaker;->add(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Paint;II)I

    move-result v0

    iput v0, v11, Lcom/google/android/street/Overlay$Label;->mLabelID:I

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v1, v0}, Lcom/google/android/street/LabelMaker;->getWidth(I)F

    move-result v1

    iput v1, v11, Lcom/google/android/street/Overlay$Label;->mWidth:F

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v1, v0}, Lcom/google/android/street/LabelMaker;->getHeight(I)F

    move-result v0

    iput v0, v11, Lcom/google/android/street/Overlay$Label;->mHeight:F

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    aget-object v0, v0, v9

    aput-object v11, v0, v10

    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/16 v1, 0x100

    const/16 v2, 0x100

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/street/LabelMaker;->beginAdding(Ljavax/microedition/khronos/opengles/GL10;II)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/street/LabelMaker;->add(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    iput v0, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancakeLabelId:I

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->endAdding(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-eqz v0, :cond_a

    if-eqz p2, :cond_a

    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->beginAdding(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v0, v0

    const/4 v1, 0x3

    filled-new-array {v0, v1}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [[I

    iput-object p2, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v0, :cond_9

    iget-object v2, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v2, v2, v1

    iget-object v3, v2, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, v2, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v2}, Lcom/google/android/street/Overlay;->getDirectionText(Lcom/google/android/street/PanoramaLink;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :goto_4
    const/4 v4, 0x3

    if-ge v3, v4, :cond_8

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    aget-object v4, v4, v1

    iget-object v5, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    iget-object v7, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v5, p1, v2, v6, v7}, Lcom/google/android/street/LabelMaker;->add(Ljavax/microedition/khronos/opengles/GL10;Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Paint;)I

    move-result v5

    aput v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_7
    iget-object v2, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    aget-object v2, v2, v1

    const/4 v3, 0x0

    const/4 v4, -0x1

    aput v4, v2, v3

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->endAdding(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsComputed:Z

    goto/16 :goto_0
.end method

.method private beginLabelOpacity(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v0}, Lcom/google/android/street/Overlay$FadeAnimation;->getOpacity()I

    move-result v0

    const/high16 v1, 0x10000

    if-eq v0, v1, :cond_0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/high16 v3, 0x46040000

    invoke-interface {p1, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    invoke-interface {p1, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    :cond_0
    return-void
.end method

.method private clearLabelMakers(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 2
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsComputed:Z

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsInitialized:Z

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->shutdown(Ljavax/microedition/khronos/opengles/GL10;)V

    iput-object v1, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->shutdown(Ljavax/microedition/khronos/opengles/GL10;)V

    iput-object v1, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    :cond_1
    return-void
.end method

.method private computeAnimation(Lcom/google/android/street/UserOrientation;J)V
    .locals 6

    const-wide/16 v3, 0x7d0

    invoke-direct {p0, p2, p3}, Lcom/google/android/street/Overlay;->computeLabelOpacity(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/street/Overlay;->mSelectedLink:I

    iget-wide v0, p0, Lcom/google/android/street/Overlay;->mLastTrackballTime:J

    sub-long v0, p2, v0

    cmp-long v0, v0, v3

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    invoke-virtual {p1}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v1

    const/high16 v2, 0x42f00000

    invoke-static {v0, v1, v2}, Lcom/google/android/street/PanoramaConfig;->getClosestLinkIndex([Lcom/google/android/street/PanoramaLink;FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/street/Overlay;->mSelectedLink:I

    iget-wide v0, p0, Lcom/google/android/street/Overlay;->mLastTrackballTime:J

    add-long/2addr v0, v3

    iget-wide v2, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    :cond_0
    iput-wide v0, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    :cond_1
    return-void
.end method

.method private computeLabelOpacity(J)J
    .locals 5
    .param p1    # J

    const/4 v4, 0x0

    iget-boolean v3, p0, Lcom/google/android/street/Overlay;->mTrackballUsed:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/street/Overlay;->mTouchUsed:Z

    if-eqz v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    move v2, v3

    :goto_0
    iput-boolean v4, p0, Lcom/google/android/street/Overlay;->mTrackballUsed:Z

    iput-boolean v4, p0, Lcom/google/android/street/Overlay;->mTouchUsed:Z

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v3, v2, p1, p2}, Lcom/google/android/street/Overlay$FadeAnimation;->computeLabelOpacity(ZJ)J

    move-result-wide v0

    return-wide v0

    :cond_1
    move v2, v4

    goto :goto_0
.end method

.method private static createDirectionsArrow(Lcom/google/android/street/PanoramaLink;Lcom/google/android/street/PanoramaLink;)Lcom/google/android/street/Overlay$Polygon;
    .locals 20
    .param p0    # Lcom/google/android/street/PanoramaLink;
    .param p1    # Lcom/google/android/street/PanoramaLink;

    const/4 v13, 0x0

    const v14, 0x3f51ac9b

    const/4 v9, 0x0

    const v10, 0x3ea35936

    const/high16 v4, 0x42b40000

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    move/from16 p1, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    move/from16 p0, v0

    sub-float p0, p1, p0

    sub-float p0, v4, p0

    invoke-static/range {p0 .. p0}, Lcom/google/android/street/StreetMath;->normalizeDegrees(F)F

    move-result p0

    invoke-static/range {p0 .. p0}, Lcom/google/android/street/StreetMath;->degreesToUnit(F)F

    move-result p0

    const/high16 p1, 0x3f000000

    invoke-static/range {p0 .. p0}, Lcom/google/android/street/StreetMath;->cosUnit(F)F

    move-result v4

    mul-float v7, p1, v4

    const/high16 p1, 0x3f000000

    invoke-static/range {p0 .. p0}, Lcom/google/android/street/StreetMath;->sinUnit(F)F

    move-result p0

    mul-float p0, p0, p1

    const p1, 0x3ea35936

    add-float v8, p0, p1

    const/16 v11, 0x17

    mul-int/lit8 p0, v11, 0x3

    move/from16 v0, p0

    new-array v0, v0, [F

    move-object v15, v0

    const v4, 0x3de38e39

    const/16 p0, 0x0

    const/16 p1, 0x0

    move/from16 v6, p1

    move/from16 p1, p0

    :goto_0
    const/16 p0, 0xa

    move v0, v6

    move/from16 v1, p0

    if-ge v0, v1, :cond_0

    move v0, v6

    int-to-float v0, v0

    move/from16 p0, v0

    mul-float v5, v4, p0

    invoke-static {v7, v9, v13, v5}, Lcom/google/android/street/StreetMath;->bezier(FFFF)F

    move-result v16

    invoke-static {v8, v10, v14, v5}, Lcom/google/android/street/StreetMath;->bezier(FFFF)F

    move-result v17

    invoke-static {v7, v9, v13, v5}, Lcom/google/android/street/StreetMath;->bezierTangent(FFFF)F

    move-result p0

    invoke-static {v8, v10, v14, v5}, Lcom/google/android/street/StreetMath;->bezierTangent(FFFF)F

    move-result v5

    const v12, 0x3d6bedfa

    mul-float v18, p0, p0

    mul-float v19, v5, v5

    add-float v18, v18, v19

    invoke-static/range {v18 .. v18}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v18

    div-float v12, v12, v18

    neg-float v5, v5

    mul-float/2addr v5, v12

    mul-float v12, v12, p0

    add-int/lit8 p0, p1, 0x1

    add-float v18, v16, v5

    aput v18, v15, p1

    add-int/lit8 p1, p0, 0x1

    const v18, 0x3e570a3e

    aput v18, v15, p0

    add-int/lit8 p0, p1, 0x1

    add-float v18, v17, v12

    aput v18, v15, p1

    add-int/lit8 p1, p0, 0x1

    sub-float v5, v16, v5

    aput v5, v15, p0

    add-int/lit8 p0, p1, 0x1

    const v5, 0x3e570a3e

    aput v5, v15, p1

    add-int/lit8 p1, p0, 0x1

    sub-float v5, v17, v12

    aput v5, v15, p0

    add-int/lit8 p0, v6, 0x1

    move/from16 v6, p0

    goto :goto_0

    :cond_0
    add-int/lit8 p0, p1, 0x1

    const v4, -0x41c538ef

    aput v4, v15, p1

    add-int/lit8 p1, p0, 0x1

    const v4, 0x3e570a3e

    aput v4, v15, p0

    add-int/lit8 p0, p1, 0x1

    const v4, 0x3f51ac9b

    aput v4, v15, p1

    add-int/lit8 p1, p0, 0x1

    const v4, 0x3e3ac711

    aput v4, v15, p0

    add-int/lit8 p0, p1, 0x1

    const v4, 0x3e570a3e

    aput v4, v15, p1

    add-int/lit8 p1, p0, 0x1

    const v4, 0x3f51ac9b

    aput v4, v15, p0

    add-int/lit8 p0, p1, 0x1

    const/4 v4, 0x0

    aput v4, v15, p1

    add-int/lit8 p1, p0, 0x1

    const v4, 0x3e570a3e

    aput v4, v15, p0

    add-int/lit8 p0, p1, 0x1

    const p0, 0x3f8a1dfc

    aput p0, v15, p1

    invoke-static {v11}, Lcom/google/android/street/Overlay$Polygon;->identityIndexArray(I)[B

    move-result-object p0

    new-array v5, v11, [B

    div-int/lit8 p1, v11, 0x2

    const/4 v4, 0x0

    :goto_1
    move v0, v4

    move/from16 v1, p1

    if-ge v0, v1, :cond_1

    mul-int/lit8 v6, v4, 0x2

    int-to-byte v6, v6

    aput-byte v6, v5, v4

    add-int v6, p1, v4

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v7, v4, 0x1

    mul-int/lit8 v7, v7, 0x2

    sub-int v7, v11, v7

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x1

    sub-int v4, v11, v4

    int-to-byte v4, v4

    aput-byte v4, v5, p1

    new-instance p1, Lcom/google/android/street/Overlay$Polygon;

    move-object/from16 v0, p1

    move-object v1, v15

    move-object/from16 v2, p0

    move-object v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    return-object p1
.end method

.method private createLabelMakers()V
    .locals 6

    const/4 v5, 0x1

    new-instance v1, Lcom/google/android/street/LabelMaker;

    const/16 v2, 0x200

    const/16 v3, 0x400

    invoke-direct {v1, v5, v2, v3, v5}, Lcom/google/android/street/LabelMaker;-><init>(ZIIZ)V

    iput-object v1, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/16 v0, 0x800

    iget v1, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    float-to-double v1, v1

    const-wide/high16 v3, 0x4000000000000000L

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    div-int/lit8 v0, v0, 0x2

    :cond_0
    new-instance v1, Lcom/google/android/street/LabelMaker;

    const/16 v2, 0x800

    const/4 v3, 0x0

    invoke-direct {v1, v5, v2, v0, v3}, Lcom/google/android/street/LabelMaker;-><init>(ZIIZ)V

    iput-object v1, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    return-void
.end method

.method private drawFancyStreetLabels(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V
    .locals 13
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Lcom/google/android/street/UserOrientation;

    const/16 v12, 0x1700

    const/4 v11, 0x0

    const/high16 v10, 0x3f800000

    const v9, 0x3ccccccd

    const/4 v8, 0x0

    iget-boolean v5, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    iget v6, p0, Lcom/google/android/street/Overlay;->mViewWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/street/Overlay;->mViewHeight:I

    int-to-float v7, v7

    invoke-virtual {v5, p1, v6, v7}, Lcom/google/android/street/LabelMaker;->beginDrawing(Ljavax/microedition/khronos/opengles/GL10;FF)V

    invoke-interface {p1, v12}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getRotationMatrix()[F

    move-result-object v5

    invoke-interface {p1, v5, v11}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    const/4 v3, 0x0

    :goto_1
    iget-object v5, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v5, v5

    if-ge v3, v5, :cond_3

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    aget-object v5, v5, v3

    aget v5, v5, v11

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/high16 v5, 0x43340000

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v6, v6, Lcom/google/android/street/PanoramaConfig;->mTiltYawDeg:F

    sub-float v1, v5, v6

    invoke-static {v1}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v2

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    invoke-static {v2}, Landroid/util/FloatMath;->sin(F)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v5, v5, Lcom/google/android/street/PanoramaConfig;->mTiltPitchDeg:F

    neg-float v5, v5

    neg-float v6, v4

    invoke-interface {p1, v5, v0, v8, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v5, v5, v3

    iget v5, v5, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    neg-float v5, v5

    invoke-interface {p1, v5, v8, v10, v8}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v5, -0x40000000

    invoke-interface {p1, v8, v8, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v5, -0x3f600000

    invoke-interface {p1, v8, v5, v8}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v5, -0x3d4c0000

    invoke-interface {p1, v5, v10, v8, v8}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v5, 0x42b40000

    invoke-interface {p1, v5, v8, v8, v10}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    invoke-interface {p1, v9, v9, v9}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/high16 v5, -0x3d900000

    iget v6, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000

    div-float/2addr v5, v6

    invoke-interface {p1, v8, v5, v8}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v5, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    aget-object v6, v6, v3

    aget v6, v6, v11

    invoke-virtual {v5, p1, v6}, Lcom/google/android/street/LabelMaker;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_2

    :cond_3
    invoke-interface {p1, v12}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    iget-object v5, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v5, p1}, Lcom/google/android/street/LabelMaker;->endDrawing(Ljavax/microedition/khronos/opengles/GL10;)V

    goto/16 :goto_0
.end method

.method private drawLabel(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/Overlay$Label;)V
    .locals 5
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Lcom/google/android/street/Overlay$Label;

    iget-object v0, p2, Lcom/google/android/street/Overlay$Label;->mPosition:[F

    const/4 v2, 0x0

    aget v1, v0, v2

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/4 v3, 0x1

    aget v3, v0, v3

    iget v4, p2, Lcom/google/android/street/Overlay$Label;->mLabelID:I

    invoke-virtual {v2, p1, v1, v3, v4}, Lcom/google/android/street/LabelMaker;->draw(Ljavax/microedition/khronos/opengles/GL10;FFI)V

    :cond_0
    return-void
.end method

.method private drawLabels(Ljavax/microedition/khronos/opengles/GL10;ZZ)V
    .locals 8

    const/16 v5, 0x80

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget v1, p0, Lcom/google/android/street/Overlay;->mViewWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/street/Overlay;->mViewHeight:I

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/street/LabelMaker;->beginDrawing(Ljavax/microedition/khronos/opengles/GL10;FF)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v0}, Lcom/google/android/street/Overlay$FadeAnimation;->isTransparent()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->beginLabelOpacity(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-eqz v0, :cond_2

    if-nez p3, :cond_2

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v0, v0

    move v1, v4

    :goto_0
    if-ge v1, v0, :cond_2

    iget v2, p0, Lcom/google/android/street/Overlay;->mHighlight:I

    if-ne v1, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    aget-object v3, v3, v1

    aget-object v2, v3, v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p1, v2}, Lcom/google/android/street/Overlay;->drawLabel(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/Overlay$Label;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/google/android/street/Overlay;->mSelectedLink:I

    if-ne v1, v2, :cond_5

    const/4 v2, 0x2

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->endLabelOpacity(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_3
    if-eqz p2, :cond_4

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/high16 v1, 0x41f00000

    const/high16 v2, 0x42700000

    iget v3, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancakeLabelId:I

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/street/LabelMaker;->draw(Ljavax/microedition/khronos/opengles/GL10;FFI)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    const/16 v1, 0x1e

    const/16 v2, 0x3c

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x3c

    const/4 v7, -0x2

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/street/Overlay$HitTester;->add(IIIIIII)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->endDrawing(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void

    :cond_5
    move v2, v4

    goto :goto_1
.end method

.method private drawLink(Ljavax/microedition/khronos/opengles/GL10;FI)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    move-object v5, v0

    aget-object v5, v5, p3

    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v6, v5, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    sub-float v13, p2, v6

    const/high16 v6, 0x43340000

    iget v7, v5, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    sub-float/2addr v6, v7

    const/high16 v7, 0x43340000

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    move-object v8, v0

    iget v8, v8, Lcom/google/android/street/PanoramaConfig;->mTiltYawDeg:F

    sub-float/2addr v7, v8

    invoke-static {v7}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v7

    invoke-static {v7}, Landroid/util/FloatMath;->cos(F)F

    move-result v8

    invoke-static {v7}, Landroid/util/FloatMath;->sin(F)F

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    move-object v9, v0

    iget v9, v9, Lcom/google/android/street/PanoramaConfig;->mTiltPitchDeg:F

    neg-float v9, v9

    const/4 v10, 0x0

    neg-float v7, v7

    move-object/from16 v0, p1

    move v1, v9

    move v2, v8

    move v3, v10

    move v4, v7

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move v1, v6

    move v2, v7

    move v3, v8

    move v4, v9

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget v6, v5, Lcom/google/android/street/PanoramaLink;->mRoadARGB:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-eq v5, v7, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-ne v5, v7, :cond_1

    :cond_0
    const v6, -0x7fa9a934

    :cond_1
    shr-int/lit8 v7, v6, 0x18

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    const v8, 0x3f8ccccd

    int-to-float v7, v7

    mul-float/2addr v7, v8

    float-to-int v7, v7

    const/high16 v8, 0x10000

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    shr-int/lit8 v8, v6, 0x10

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    shr-int/lit8 v9, v6, 0x8

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    shr-int/lit8 v6, v6, 0x0

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    move-object/from16 v0, p1

    move v1, v8

    move v2, v9

    move v3, v6

    move v4, v7

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    move v6, v0

    if-nez v6, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mRoad:Lcom/google/android/street/Overlay$Polygon;

    move-object v6, v0

    const/4 v7, 0x6

    move-object v0, v6

    move-object/from16 v1, p1

    move v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mRoad:Lcom/google/android/street/Overlay$Polygon;

    move-object v6, v0

    move-object v0, v6

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_2
    invoke-static {v13}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v6

    invoke-static {v6}, Landroid/util/FloatMath;->cos(F)F

    move-result v14

    const/high16 v6, 0x40000000

    mul-float/2addr v6, v14

    mul-float/2addr v6, v14

    const/high16 v7, 0x3f800000

    sub-float/2addr v6, v7

    const/high16 v7, 0x40800000

    mul-float/2addr v7, v14

    mul-float/2addr v7, v14

    const/high16 v8, 0x40400000

    sub-float/2addr v7, v8

    mul-float/2addr v7, v14

    const v8, 0x3e4ccccd

    const/high16 v9, 0x3e800000

    const v10, 0x3f2e147b

    const v11, 0x3eb5c28f

    mul-float/2addr v11, v14

    add-float/2addr v10, v11

    const v11, 0x3ea3d70a

    mul-float/2addr v6, v11

    sub-float v6, v10, v6

    const v10, 0x3e5c28f6

    mul-float/2addr v7, v10

    sub-float/2addr v6, v7

    mul-float/2addr v6, v9

    add-float/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v7, v0

    if-eqz v7, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-ne v5, v7, :cond_9

    const v7, 0x3f1b8bad

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    :cond_3
    :goto_0
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move v1, v7

    move v2, v8

    move v3, v6

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v6, v0

    if-eqz v6, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    move-object v6, v0

    if-eq v5, v6, :cond_4

    const v6, 0x3f19999a

    const/high16 v7, 0x3f800000

    const v8, 0x3f19999a

    move-object/from16 v0, p1

    move v1, v6

    move v2, v7

    move v3, v8

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/Overlay;->mSelectedLink:I

    move v6, v0

    move v0, v6

    move/from16 v1, p3

    if-ne v0, v1, :cond_a

    const/4 v6, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/Overlay;->mHighlight:I

    move v7, v0

    move v0, v7

    move/from16 v1, p3

    if-ne v0, v1, :cond_b

    const/4 v7, 0x1

    :goto_2
    if-eqz v7, :cond_5

    const/4 v6, 0x0

    :cond_5
    if-nez v6, :cond_6

    if-eqz v7, :cond_c

    :cond_6
    const/4 v7, 0x1

    move v15, v7

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v7, v0

    if-eqz v7, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-ne v5, v7, :cond_d

    const/4 v5, 0x1

    move/from16 v16, v5

    :goto_4
    if-eqz v15, :cond_10

    if-eqz v6, :cond_e

    const v5, 0x8800

    const/16 v6, 0x6d00

    const v7, 0xad00

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    :goto_5
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    if-eqz v16, :cond_f

    const/4 v5, 0x0

    const/4 v6, 0x0

    const v7, -0x42fd5239

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const v5, 0x3f8ccccd

    const/high16 v6, 0x3f800000

    const v7, 0x3f8ccccd

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    move-object v5, v0

    move-object v0, v5

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/street/Projector;->getCurrentModelView(Ljavax/microedition/khronos/opengles/GL10;)V

    if-eqz v16, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    const/4 v6, 0x5

    move-object v0, v5

    move-object/from16 v1, p1

    move v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    const/high16 v7, 0x10000

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    move-object v0, v5

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    move-object v6, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mScratch:[F

    move-object v8, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v9, v0

    const/4 v10, 0x0

    const/16 v11, 0x40

    move-object/from16 v7, p1

    move/from16 v12, p3

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/street/Overlay$HitTester;->add(Lcom/google/android/street/Projector;Ljavax/microedition/khronos/opengles/GL10;[FLcom/google/android/street/Overlay$Polygon;III)V

    :goto_7
    if-eqz v15, :cond_7

    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    move-object v5, v0

    aget-object v5, v5, p3

    const/4 v6, 0x0

    aget-object v5, v5, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    move-object v6, v0

    invoke-virtual {v6}, Lcom/google/android/street/Overlay$FadeAnimation;->isTransparent()Z

    move-result v6

    if-nez v6, :cond_18

    if-eqz v5, :cond_18

    iget-object v6, v5, Lcom/google/android/street/Overlay$Label;->mPosition:[F

    sget v7, Lcom/google/android/street/Overlay;->COS_NO_SHOW:F

    cmpl-float v7, v14, v7

    if-lez v7, :cond_17

    iget v5, v5, Lcom/google/android/street/Overlay$Label;->mLabelID:I

    if-eqz v16, :cond_14

    sget-object v7, Lcom/google/android/street/Overlay;->STREET_ANCHOR_DIR_ARROW:[F

    move-object/from16 v0, p0

    move-object v1, v7

    move-object v2, v6

    invoke-direct {v0, v1, v2}, Lcom/google/android/street/Overlay;->getBaseProjection([F[F)V

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    move-object v7, v0

    invoke-virtual {v7, v5}, Lcom/google/android/street/LabelMaker;->getWidth(I)F

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    move-object v8, v0

    invoke-virtual {v8, v5}, Lcom/google/android/street/LabelMaker;->getHeight(I)F

    move-result v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/Overlay;->mViewHeight:I

    move v8, v0

    int-to-float v8, v8

    const/high16 v9, 0x3f000000

    mul-float/2addr v8, v9

    const v9, 0x3e19999a

    mul-float/2addr v8, v9

    neg-float v9, v7

    const/high16 v10, 0x3f000000

    mul-float/2addr v9, v10

    neg-float v7, v7

    sub-float/2addr v7, v8

    neg-float v5, v5

    const v10, 0x3ecccccd

    mul-float/2addr v5, v10

    const/4 v10, 0x0

    aget v10, v6, v10

    const/4 v11, 0x1

    aget v11, v6, v11

    sget v12, Lcom/google/android/street/Overlay;->COS_SHOW_BELOW:F

    cmpg-float v12, v14, v12

    if-gtz v12, :cond_15

    add-float v5, v10, v9

    add-float v7, v11, v8

    move/from16 v17, v7

    move v7, v5

    move/from16 v5, v17

    :goto_9
    const/4 v8, 0x0

    aput v7, v6, v8

    const/4 v7, 0x1

    aput v5, v6, v7

    :goto_a
    const/4 v5, 0x1

    :goto_b
    const/4 v7, 0x3

    if-ge v5, v7, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    move-object v7, v0

    aget-object v7, v7, p3

    aget-object v7, v7, v5

    if-eqz v7, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    move-object v7, v0

    aget-object v7, v7, p3

    aget-object v7, v7, v5

    iget-object v7, v7, Lcom/google/android/street/Overlay$Label;->mPosition:[F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget v9, v6, v9

    aput v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x1

    aget v9, v6, v9

    aput v9, v7, v8

    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-ne v5, v7, :cond_3

    const v6, -0x415ca6ca

    goto/16 :goto_0

    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_1

    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_2

    :cond_c
    const/4 v7, 0x0

    move v15, v7

    goto/16 :goto_3

    :cond_d
    const/4 v5, 0x0

    move/from16 v16, v5

    goto/16 :goto_4

    :cond_e
    const v5, 0xf600

    const v6, 0x8a00

    const/16 v7, 0x1f00

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto/16 :goto_5

    :cond_f
    const v5, 0x3fa66666

    const/high16 v6, 0x3f800000

    const v7, 0x3fa66666

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    goto/16 :goto_6

    :cond_10
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const v8, 0x8000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto/16 :goto_6

    :cond_11
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    move v5, v0

    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    move v5, v0

    const/high16 v6, 0x3f800000

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_12

    const/4 v5, 0x0

    const/high16 v6, 0x3f000000

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_12
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x5000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mInteriorArrowShadow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    const/4 v6, 0x6

    move-object v0, v5

    move-object/from16 v1, p1

    move v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    const/high16 v7, 0x10000

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mInteriorArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    const/4 v6, 0x6

    move-object v0, v5

    move-object/from16 v1, p1

    move v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    move-object v6, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mScratch:[F

    move-object v8, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mInteriorArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v9, v0

    const/4 v10, 0x0

    const/16 v11, 0x40

    move-object/from16 v7, p1

    move/from16 v12, p3

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/street/Overlay$HitTester;->add(Lcom/google/android/street/Projector;Ljavax/microedition/khronos/opengles/GL10;[FLcom/google/android/street/Overlay$Polygon;III)V

    goto/16 :goto_7

    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    const/4 v6, 0x6

    move-object v0, v5

    move-object/from16 v1, p1

    move v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    const/high16 v7, 0x10000

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    move-object v0, v5

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    move-object v6, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mScratch:[F

    move-object v8, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v9, v0

    const/4 v10, 0x0

    const/16 v11, 0x40

    move-object/from16 v7, p1

    move/from16 v12, p3

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/street/Overlay$HitTester;->add(Lcom/google/android/street/Projector;Ljavax/microedition/khronos/opengles/GL10;[FLcom/google/android/street/Overlay$Polygon;III)V

    goto/16 :goto_7

    :cond_14
    sget-object v7, Lcom/google/android/street/Overlay;->STREET_ANCHOR:[F

    move-object/from16 v0, p0

    move-object v1, v7

    move-object v2, v6

    invoke-direct {v0, v1, v2}, Lcom/google/android/street/Overlay;->getBaseProjection([F[F)V

    goto/16 :goto_8

    :cond_15
    invoke-static {v13}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v9

    invoke-static {v9}, Landroid/util/FloatMath;->sin(F)F

    move-result v9

    const/4 v12, 0x0

    cmpl-float v9, v9, v12

    if-ltz v9, :cond_16

    add-float/2addr v7, v10

    add-float/2addr v5, v11

    goto/16 :goto_9

    :cond_16
    add-float v7, v10, v8

    add-float/2addr v5, v11

    goto/16 :goto_9

    :cond_17
    const/4 v5, 0x0

    const/high16 v7, 0x7fc00000

    aput v7, v6, v5

    const/4 v5, 0x1

    const/high16 v7, 0x7fc00000

    aput v7, v6, v5

    goto/16 :goto_a

    :cond_18
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method

.method private drawLinks(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V
    .locals 9
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Lcom/google/android/street/UserOrientation;

    const/16 v8, 0x1700

    const/16 v7, 0xbe2

    const/4 v6, 0x0

    iget-boolean v3, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v3, p0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    cmpl-float v3, v3, v6

    if-eqz v3, :cond_0

    const/16 v3, 0x1701

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getScale()F

    move-result v2

    iget v3, p0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    invoke-static {v3}, Lcom/google/android/street/Renderer;->getUnzoomedVerticalFov(F)F

    move-result v3

    mul-float v0, v3, v2

    iget v3, p0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    const v4, 0x3dcccccd

    const/high16 v5, 0x42c80000

    invoke-static {p1, v0, v3, v4, v5}, Landroid/opengl/GLU;->gluPerspective(Ljavax/microedition/khronos/opengles/GL10;FFFF)V

    invoke-interface {p1, v8}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/high16 v3, -0x40000000

    invoke-interface {p1, v6, v6, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getRotationMatrix()[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {p1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    const/high16 v3, -0x40800000

    invoke-interface {p1, v6, v3, v6}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/16 v3, 0x1d00

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glShadeModel(I)V

    invoke-interface {p1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/16 v3, 0x302

    const/16 v4, 0x303

    invoke-interface {p1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v3, p1}, Lcom/google/android/street/Projector;->getCurrentProjection(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-interface {p1, v8}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v3

    invoke-direct {p0, p1, v3, v1}, Lcom/google/android/street/Overlay;->drawLink(Ljavax/microedition/khronos/opengles/GL10;FI)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {p1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_0
.end method

.method private drawPancake(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/Overlay$Pancake;)V
    .locals 10
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Lcom/google/android/street/UserOrientation;
    .param p3    # Lcom/google/android/street/Overlay$Pancake;

    const/4 v9, 0x0

    const/high16 v8, 0x3e800000

    const/high16 v7, 0x3f800000

    const/high16 v6, 0x10000

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v2, v2, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v2, 0x1700

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    if-eqz p3, :cond_0

    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getRotationMatrix()[F

    move-result-object v2

    invoke-interface {p1, v2, v9}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mYaw:F

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    neg-float v2, v2

    invoke-interface {p1, v2, v5, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mPitch:F

    sub-float v2, v8, v2

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    neg-float v2, v2

    invoke-interface {p1, v2, v7, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mDistance:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_2

    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mDistance:F

    neg-float v2, v2

    invoke-interface {p1, v5, v5, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mPitch:F

    sub-float v2, v8, v2

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    invoke-interface {p1, v2, v7, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mYaw:F

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    invoke-interface {p1, v2, v5, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/4 v2, 0x2

    new-array v0, v2, [F

    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mNx:F

    iget v3, p3, Lcom/google/android/street/Overlay$Pancake;->mNy:F

    iget v4, p3, Lcom/google/android/street/Overlay$Pancake;->mNz:F

    neg-float v4, v4

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/street/StreetMath;->rectangularToSphericalCoords(FFF[F)V

    aget v2, v0, v9

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    neg-float v2, v2

    invoke-interface {p1, v2, v5, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/4 v2, 0x1

    aget v2, v0, v2

    sub-float v2, v8, v2

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    invoke-interface {p1, v2, v7, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_2
    if-eqz p3, :cond_3

    iget-boolean v2, p3, Lcom/google/android/street/Overlay$Pancake;->mIsGround:Z

    if-eqz v2, :cond_3

    sget-object v1, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

    :goto_1
    const/16 v2, 0xbe2

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-interface {p1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const v2, 0x8000

    invoke-interface {p1, v6, v6, v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/4 v2, 0x5

    invoke-virtual {v1, p1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    invoke-interface {p1, v6, v6, v6, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    invoke-virtual {v1, p1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    const/16 v2, 0xbe2

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    iget-object v2, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v2, p1}, Lcom/google/android/street/Projector;->getCurrentModelView(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v2, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v2, p1}, Lcom/google/android/street/Projector;->getCurrentProjection(Ljavax/microedition/khronos/opengles/GL10;)V

    goto/16 :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/street/Overlay;->PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

    goto :goto_1
.end method

.method private drawPanoPoints(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 13
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    const/4 v12, 0x0

    const/high16 v11, 0x10000

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v0, v0, Lcom/google/android/street/PanoramaConfig;->mPanoYawDeg:F

    neg-float v0, v0

    const/high16 v1, 0x43340000

    sub-float/2addr v0, v1

    const/high16 v1, 0x3f800000

    invoke-interface {p1, v0, v12, v1, v12}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/16 v0, 0x10

    new-array v9, v0, [F

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v0}, Lcom/google/android/street/PanoramaConfig;->getTiltMatrix()[F

    move-result-object v0

    invoke-static {v9, v5, v0, v5}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    invoke-interface {p1, v9, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    new-instance v4, Lcom/google/android/street/Overlay$Polygon;

    sget-object v0, Lcom/google/android/street/Overlay;->PYRAMID_DATA:[F

    sget-object v1, Lcom/google/android/street/Overlay;->PYRAMID_FILL_INDEX:[B

    sget-object v2, Lcom/google/android/street/Overlay;->PYRAMID_OUTLINE_INDEX:[B

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    const/4 v8, 0x1

    :goto_0
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0}, Lcom/google/android/street/DepthMap;->numPanos()I

    move-result v0

    if-ge v8, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0, v8}, Lcom/google/android/street/DepthMap;->getPanoId(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v1, v1, Lcom/google/android/street/PanoramaConfig;->mPanoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/street/Overlay;->mHighlight:I

    add-int/lit8 v1, v8, 0x14

    if-ne v0, v1, :cond_3

    invoke-interface {p1, v5, v11, v5, v11}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    :goto_2
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0, v8}, Lcom/google/android/street/DepthMap;->getPanoPoint(I)Lcom/google/android/street/DepthMap$Point;

    move-result-object v10

    iget v0, v10, Lcom/google/android/street/DepthMap$Point;->x:F

    neg-float v0, v0

    iget v1, v10, Lcom/google/android/street/DepthMap$Point;->y:F

    invoke-interface {p1, v0, v12, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Projector;->getCurrentModelView(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Projector;->getCurrentProjection(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-virtual {v4, p1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mScratch:[F

    add-int/lit8 v7, v8, 0x14

    move-object v2, p1

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/street/Overlay$HitTester;->add(Lcom/google/android/street/Projector;Ljavax/microedition/khronos/opengles/GL10;[FLcom/google/android/street/Overlay$Polygon;III)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_1

    :cond_3
    invoke-interface {p1, v5, v5, v11, v11}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto :goto_2
.end method

.method private endLabelOpacity(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    const/16 v3, 0x1000

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v0}, Lcom/google/android/street/Overlay$FadeAnimation;->getOpacity()I

    move-result v0

    const/high16 v1, 0x10000

    if-eq v0, v1, :cond_0

    const/16 v0, 0x2300

    const/16 v1, 0x2200

    const v2, 0x45f00800

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    invoke-interface {p1, v3, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    :cond_0
    return-void
.end method

.method private getBaseProjection([F[F)V
    .locals 2
    .param p1    # [F
    .param p2    # [F

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v0, p1, v1, p2, v1}, Lcom/google/android/street/Projector;->project([FI[FI)V

    return-void
.end method

.method private static getCircle(FI)[F
    .locals 9

    mul-int/lit8 v0, p1, 0xc

    new-array v0, v0, [F

    const v1, 0x3fc90fdb

    int-to-float v2, p1

    div-float/2addr v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x0

    move v8, v3

    move v3, v2

    move v2, v8

    :goto_0
    if-gt v2, p1, :cond_1

    invoke-static {v3}, Landroid/util/FloatMath;->cos(F)F

    move-result v4

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v5

    mul-int/lit8 v6, v2, 0x3

    aput v4, v0, v6

    mul-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x1

    aput v5, v0, v6

    mul-int/lit8 v6, p1, 0x2

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    add-int/2addr v6, v7

    neg-float v7, v4

    aput v7, v0, v6

    mul-int/lit8 v6, p1, 0x2

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x1

    neg-float v7, v5

    aput v7, v0, v6

    if-lez v2, :cond_0

    if-ge v2, p1, :cond_0

    mul-int/lit8 v6, p1, 0x2

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    sub-int/2addr v6, v7

    neg-float v7, v4

    aput v7, v0, v6

    mul-int/lit8 v6, p1, 0x2

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x1

    aput v5, v0, v6

    mul-int/lit8 v6, p1, 0x4

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    sub-int/2addr v6, v7

    aput v4, v0, v6

    mul-int/lit8 v4, p1, 0x4

    mul-int/lit8 v4, v4, 0x3

    mul-int/lit8 v6, v2, 0x3

    sub-int/2addr v4, v6

    add-int/lit8 v4, v4, 0x1

    neg-float v5, v5

    aput v5, v0, v4

    :cond_0
    add-float/2addr v3, v1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private getDirectionText(Lcom/google/android/street/PanoramaLink;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Lcom/google/android/street/PanoramaLink;

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mCompassDirectionNames:[Ljava/lang/CharSequence;

    iget v1, p1, Lcom/google/android/street/PanoramaLink;->mDirection:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method private static getPolygonStripIndices(I)[B
    .locals 4
    .param p0    # I

    new-array v1, p0, [B

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_1

    rem-int/lit8 v2, v0, 0x2

    if-nez v2, :cond_0

    div-int/lit8 v2, v0, 0x2

    :goto_1
    int-to-byte v2, v2

    aput-byte v2, v1, v0

    add-int/lit8 v2, v0, 0x1

    int-to-byte v0, v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    sub-int v2, p0, v2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    goto :goto_1

    :cond_1
    return-object v1
.end method

.method private static getRange(I)[B
    .locals 3
    .param p0    # I

    new-array v1, p0, [B

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_0

    aput-byte v0, v1, v0

    add-int/lit8 v2, v0, 0x1

    int-to-byte v0, v2

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private handleLabelsOutOfMemory(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 1
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->clearLabelMakers(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    invoke-direct {p0}, Lcom/google/android/street/Overlay;->createLabelMakers()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawDisabled:Z

    goto :goto_0
.end method

.method private updateLinkInfo()V
    .locals 10

    const/high16 v5, 0x41a00000

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v3, v3, Lcom/google/android/street/PanoramaConfig;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget v3, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    iget v3, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v4, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    invoke-virtual {v3, v4, v5}, Lcom/google/android/street/PanoramaConfig;->getClosestLink(FF)Lcom/google/android/street/PanoramaLink;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v4, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    invoke-virtual {v3, v4, v5}, Lcom/google/android/street/PanoramaConfig;->getClosestLink(FF)Lcom/google/android/street/PanoramaLink;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-ne v3, v4, :cond_0

    new-instance v3, Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget v4, v4, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v5, v5, Lcom/google/android/street/PanoramaLink;->mPanoId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget v6, v6, Lcom/google/android/street/PanoramaLink;->mRoadARGB:I

    iget-object v7, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v7, v7, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/street/PanoramaLink;-><init>(FLjava/lang/String;ILjava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    :cond_0
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    if-nez v3, :cond_5

    move v3, v9

    :goto_0
    iget-object v4, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-nez v4, :cond_6

    move v4, v9

    :goto_1
    add-int v1, v3, v4

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v3, v3

    add-int/2addr v3, v1

    new-array v3, v3, [Lcom/google/android/street/PanoramaLink;

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v3, v3, Lcom/google/android/street/PanoramaConfig;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v5, v5, Lcom/google/android/street/PanoramaConfig;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    if-nez v3, :cond_1

    new-instance v3, Lcom/google/android/street/PanoramaLink;

    iget v4, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    const-string v5, ""

    const-string v6, ""

    invoke-direct {v3, v4, v5, v8, v6}, Lcom/google/android/street/PanoramaLink;-><init>(FLjava/lang/String;ILjava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v4, v4

    add-int/lit8 v2, v1, -0x1

    sub-int/2addr v4, v1

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    aput-object v5, v3, v4

    move v1, v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-nez v3, :cond_2

    new-instance v3, Lcom/google/android/street/PanoramaLink;

    iget v4, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    const-string v5, ""

    const-string v6, ""

    invoke-direct {v3, v4, v5, v8, v6}, Lcom/google/android/street/PanoramaLink;-><init>(FLjava/lang/String;ILjava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v4, v4

    sub-int/2addr v4, v1

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    aput-object v5, v3, v4

    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v3, v3

    sub-int/2addr v3, v9

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v5, v5

    sub-int/2addr v5, v9

    aget-object v4, v4, v5

    aput-object v4, v3, v0

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v4, v4

    sub-int/2addr v4, v9

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    aput-object v5, v3, v4

    :cond_3
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    invoke-static {v3, v4}, Lcom/google/android/street/Overlay;->createDirectionsArrow(Lcom/google/android/street/PanoramaLink;Lcom/google/android/street/PanoramaLink;)Lcom/google/android/street/Overlay$Polygon;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    :cond_4
    return-void

    :cond_5
    move v3, v8

    goto/16 :goto_0

    :cond_6
    move v4, v8

    goto/16 :goto_1

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public doSetMotionUse(IJ)V
    .locals 1
    .param p1    # I
    .param p2    # J

    const/4 v0, 0x1

    if-nez p1, :cond_1

    iput-wide p2, p0, Lcom/google/android/street/Overlay;->mLastTrackballTime:J

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mTrackballUsed:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mTouchUsed:Z

    goto :goto_0
.end method

.method public draw(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/Overlay$Pancake;J)V
    .locals 9
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Lcom/google/android/street/UserOrientation;
    .param p3    # Lcom/google/android/street/Overlay$Pancake;
    .param p4    # J

    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mDrawDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v6, 0x1

    new-array v3, v6, [I

    const/16 v6, 0xd33

    const/4 v7, 0x0

    invoke-interface {p1, v6, v3, v7}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    move v2, v6

    :goto_1
    iget-object v6, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v6, v6, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    move v1, v6

    :goto_2
    if-nez v2, :cond_5

    const/4 v6, 0x0

    aget v6, v3, v6

    const/16 v7, 0x800

    if-lt v6, v7, :cond_5

    if-eqz v1, :cond_5

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-boolean v6, v6, Lcom/google/android/street/PanoramaConfig;->mDisabled:Z

    if-nez v6, :cond_5

    const/4 v6, 0x1

    move v5, v6

    :goto_3
    invoke-direct {p0, p2, p4, p5}, Lcom/google/android/street/Overlay;->computeAnimation(Lcom/google/android/street/UserOrientation;J)V

    if-eqz v5, :cond_6

    :try_start_0
    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    if-nez v6, :cond_6

    const/4 v6, 0x1

    :goto_4
    invoke-direct {p0, p1, v6}, Lcom/google/android/street/Overlay;->addLabels(Ljavax/microedition/khronos/opengles/GL10;Z)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    iget v7, p0, Lcom/google/android/street/Overlay;->mViewWidth:I

    iget v8, p0, Lcom/google/android/street/Overlay;->mViewHeight:I

    invoke-virtual {v6, v7, v8}, Lcom/google/android/street/Overlay$HitTester;->reset(II)V

    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    if-eqz v6, :cond_7

    invoke-direct {p0, p1, p2}, Lcom/google/android/street/Overlay;->drawLinks(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, p1, v6, v7}, Lcom/google/android/street/Overlay;->drawLabels(Ljavax/microedition/khronos/opengles/GL10;ZZ)V

    :cond_2
    :goto_5
    iget-object v6, p0, Lcom/google/android/street/Overlay;->mHitTesterLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_1
    iget-object v4, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    iget-object v7, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    iput-object v7, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    iput-object v4, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :cond_3
    const/4 v6, 0x0

    move v2, v6

    goto :goto_1

    :cond_4
    const/4 v6, 0x0

    move v1, v6

    goto :goto_2

    :cond_5
    const/4 v6, 0x0

    move v5, v6

    goto :goto_3

    :cond_6
    const/4 v6, 0x0

    goto :goto_4

    :catch_0
    move-exception v6

    move-object v0, v6

    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->handleLabelsOutOfMemory(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0

    :cond_7
    if-eqz v5, :cond_9

    invoke-direct {p0, p1, p2}, Lcom/google/android/street/Overlay;->drawFancyStreetLabels(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V

    :goto_6
    if-nez p3, :cond_a

    if-eqz v1, :cond_a

    const/4 v6, 0x1

    :goto_7
    invoke-direct {p0, p1, v6, v5}, Lcom/google/android/street/Overlay;->drawLabels(Ljavax/microedition/khronos/opengles/GL10;ZZ)V

    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mEnablePanoPoints:Z

    if-eqz v6, :cond_8

    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->drawPanoPoints(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_8
    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mEnablePancake:Z

    if-eqz v6, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/street/Overlay;->drawPancake(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/Overlay$Pancake;)V

    goto :goto_5

    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/google/android/street/Overlay;->drawLinks(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V

    goto :goto_6

    :cond_a
    const/4 v6, 0x0

    goto :goto_7
.end method

.method public getNextDrawTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    return-wide v0
.end method

.method public getPanoramaLink(I)Lcom/google/android/street/PanoramaLink;
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v1, v1, p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v1

    move-object v0, v1

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hit(II)I
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mHitTesterLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/street/Overlay$HitTester;->hit(II)I

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public initialize(Lcom/google/android/street/PanoramaConfig;II)V
    .locals 5
    .param p1    # Lcom/google/android/street/PanoramaConfig;
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v1, v1, Lcom/google/android/street/PanoramaConfig;->mSceneType:I

    if-ne v1, v3, :cond_0

    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    invoke-direct {p0}, Lcom/google/android/street/Overlay;->updateLinkInfo()V

    iput p2, p0, Lcom/google/android/street/Overlay;->mViewWidth:I

    iput p3, p0, Lcom/google/android/street/Overlay;->mViewHeight:I

    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    iput-boolean v4, p0, Lcom/google/android/street/Overlay;->mLabelsComputed:Z

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v1, v3}, Lcom/google/android/street/Overlay$FadeAnimation;->reset(Z)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/street/Overlay;->mHighlight:I

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    const/high16 v2, 0x7f020000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    const v2, 0x7f020001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x2

    const v3, 0x7f020002

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    const v1, 0x7f020008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

    return-void

    :cond_0
    move v1, v4

    goto :goto_0
.end method

.method public setDirectionsArrowParams(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iput p1, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    iput p2, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/street/Overlay;->updateLinkInfo()V

    :cond_0
    return-void
.end method

.method public setHighlight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/street/Overlay;->mHighlight:I

    return-void
.end method

.method public shutdown(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 0
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->clearLabelMakers(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method
