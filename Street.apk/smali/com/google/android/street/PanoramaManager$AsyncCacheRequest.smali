.class Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;
.super Ljava/lang/Object;
.source "PanoramaManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/PanoramaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncCacheRequest"
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

.field private final mPersistentKey:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/street/PanoramaManager;


# direct methods
.method public constructor <init>(Lcom/google/android/street/PanoramaManager;Lcom/google/android/street/PanoramaManager$ConfigFetchListener;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/google/android/street/PanoramaManager$ConfigFetchListener;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->this$0:Lcom/google/android/street/PanoramaManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    iput-object p3, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mPersistentKey:Ljava/lang/String;

    const-string v0, "persistentKey"

    # invokes: Lcom/google/android/street/PanoramaManager;->assertNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    invoke-static {p3, v0}, Lcom/google/android/street/PanoramaManager;->access$000(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v9, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->this$0:Lcom/google/android/street/PanoramaManager;

    # getter for: Lcom/google/android/street/PanoramaManager;->mHttpCache:Lcom/google/android/street/HttpCache;
    invoke-static {v0}, Lcom/google/android/street/PanoramaManager;->access$100(Lcom/google/android/street/PanoramaManager;)Lcom/google/android/street/HttpCache;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mPersistentKey:Ljava/lang/String;

    const-wide/32 v5, 0x6ddd00

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/street/HttpCache;->read(Ljava/lang/String;ZLcom/google/android/street/HttpCache$Aborter;Ljava/lang/String;J)[B

    move-result-object v7

    new-instance v10, Ljava/io/ByteArrayInputStream;

    invoke-direct {v10, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {v10}, Lcom/google/android/street/PanoramaConfig;->parseMetadataProto(Ljava/io/InputStream;)Lcom/google/android/street/PanoramaConfig;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    :try_start_2
    invoke-static {v10}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    invoke-interface {v0, v9, v12}, Lcom/google/android/street/PanoramaManager$ConfigFetchListener;->postPanoramaInfo(ZLcom/google/android/street/PanoramaConfig;)V

    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->this$0:Lcom/google/android/street/PanoramaManager;

    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mPersistentKey:Ljava/lang/String;

    # invokes: Lcom/google/android/street/PanoramaManager;->doneRetrieving(Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/google/android/street/PanoramaManager;->access$200(Lcom/google/android/street/PanoramaManager;Ljava/lang/Object;)V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {v10}, Lcom/google/android/street/HttpCache;->closeStream(Ljava/io/Closeable;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v0

    move-object v8, v0

    :try_start_4
    const-string v0, "PM failed to load config"

    invoke-static {v0, v8}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    invoke-interface {v0, v9, v12}, Lcom/google/android/street/PanoramaManager$ConfigFetchListener;->postPanoramaInfo(ZLcom/google/android/street/PanoramaConfig;)V

    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->this$0:Lcom/google/android/street/PanoramaManager;

    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mPersistentKey:Ljava/lang/String;

    # invokes: Lcom/google/android/street/PanoramaManager;->doneRetrieving(Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/google/android/street/PanoramaManager;->access$200(Lcom/google/android/street/PanoramaManager;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v8, v0

    :try_start_5
    const-string v0, "PM was interrupted loading config"

    invoke-static {v0}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v9, 0x1

    const/4 v12, 0x0

    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    invoke-interface {v0, v9, v12}, Lcom/google/android/street/PanoramaManager$ConfigFetchListener;->postPanoramaInfo(ZLcom/google/android/street/PanoramaConfig;)V

    iget-object v0, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->this$0:Lcom/google/android/street/PanoramaManager;

    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mPersistentKey:Ljava/lang/String;

    # invokes: Lcom/google/android/street/PanoramaManager;->doneRetrieving(Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/google/android/street/PanoramaManager;->access$200(Lcom/google/android/street/PanoramaManager;Ljava/lang/Object;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mListener:Lcom/google/android/street/PanoramaManager$ConfigFetchListener;

    invoke-interface {v1, v9, v12}, Lcom/google/android/street/PanoramaManager$ConfigFetchListener;->postPanoramaInfo(ZLcom/google/android/street/PanoramaConfig;)V

    iget-object v1, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->this$0:Lcom/google/android/street/PanoramaManager;

    iget-object v2, p0, Lcom/google/android/street/PanoramaManager$AsyncCacheRequest;->mPersistentKey:Ljava/lang/String;

    # invokes: Lcom/google/android/street/PanoramaManager;->doneRetrieving(Ljava/lang/Object;)V
    invoke-static {v1, v2}, Lcom/google/android/street/PanoramaManager;->access$200(Lcom/google/android/street/PanoramaManager;Ljava/lang/Object;)V

    throw v0
.end method
