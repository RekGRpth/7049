.class Lcom/google/android/street/Renderer;
.super Ljava/lang/Thread;
.source "Renderer.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/google/android/street/PanoramaManager$PanoFetchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/Renderer$1;,
        Lcom/google/android/street/Renderer$Transition;,
        Lcom/google/android/street/Renderer$MessageQueue;,
        Lcom/google/android/street/Renderer$Message;,
        Lcom/google/android/street/Renderer$RenderStatus;,
        Lcom/google/android/street/Renderer$PixelToYawPitchArgs;,
        Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;,
        Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;,
        Lcom/google/android/street/Renderer$PanoramaDrawable;,
        Lcom/google/android/street/Renderer$RenderStatusReceiver;
    }
.end annotation


# static fields
.field private static final LONGER_HALF_FOV_TANGENT:D

.field private static final sEglSemaphore:Ljava/util/concurrent/Semaphore;


# instance fields
.field private mConfigurationChanged:Z

.field private mContext:Landroid/content/Context;

.field private mCopyrightID:I

.field private mCopyrightPaint:Landroid/graphics/Paint;

.field private mCurrentRenderStatus:Lcom/google/android/street/Renderer$RenderStatus;

.field private mEGLConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field private mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

.field private mEGLDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

.field private mEgl:Ljavax/microedition/khronos/egl/EGL10;

.field private mGl:Ljavax/microedition/khronos/opengles/GL10;

.field private mGoogleLogo:Landroid/graphics/drawable/Drawable;

.field private mGoogleLogoLabelId:I

.field private mHaveSurface:Z

.field private mHeight:I

.field private mLabelMaker:Lcom/google/android/street/LabelMaker;

.field private mLabelsDirty:Z

.field private mLastPanIdDrawn:Ljava/lang/String;

.field private mLastRenderProgress:I

.field private mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

.field private mNextPanoramaConfig:Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

.field private mOverlay:Lcom/google/android/street/Overlay;

.field private mPancake:Lcom/google/android/street/Overlay$Pancake;

.field private mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

.field private mPanoramaDrawable:Lcom/google/android/street/Renderer$PanoramaDrawable;

.field private final mPanoramaManager:Lcom/google/android/street/PanoramaManager;

.field private mProjector:Lcom/google/android/street/Projector;

.field private mReadyForMessages:Z

.field private mRenderStatusReceiver:Lcom/google/android/street/Renderer$RenderStatusReceiver;

.field private volatile mShuttingDown:Z

.field private mSizeChanged:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mTextureCache:Lcom/google/android/street/TextureCache;

.field private mTransition:Lcom/google/android/street/Renderer$Transition;

.field private mUserOrientation:Lcom/google/android/street/UserOrientation;

.field private final mViewpointDetector:Lcom/google/android/street/ViewpointDetector;

.field private mWidth:I

.field private mZoomLevels:I

.field private newTimer:Lcom/google/android/street/Timer;

.field private timer:Lcom/google/android/street/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/high16 v0, 0x42340000

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/street/Renderer;->LONGER_HALF_FOV_TANGENT:D

    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lcom/google/android/street/Renderer;->sEglSemaphore:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method constructor <init>(Lcom/google/android/street/PanoramaManager;F)V
    .locals 5
    .param p1    # Lcom/google/android/street/PanoramaManager;
    .param p2    # F

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x90

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object v3, p0, Lcom/google/android/street/Renderer;->timer:Lcom/google/android/street/Timer;

    iput-object v3, p0, Lcom/google/android/street/Renderer;->newTimer:Lcom/google/android/street/Timer;

    iput-object v3, p0, Lcom/google/android/street/Renderer;->mLastPanIdDrawn:Ljava/lang/String;

    new-instance v0, Lcom/google/android/street/ViewpointDetector;

    invoke-direct {v0}, Lcom/google/android/street/ViewpointDetector;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mViewpointDetector:Lcom/google/android/street/ViewpointDetector;

    iput-object p1, p0, Lcom/google/android/street/Renderer;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    iput v1, p0, Lcom/google/android/street/Renderer;->mWidth:I

    iput v1, p0, Lcom/google/android/street/Renderer;->mHeight:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mCopyrightPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mCopyrightPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mCopyrightPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000

    mul-float/2addr v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mCopyrightPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    new-instance v0, Lcom/google/android/street/LabelMaker;

    const/16 v1, 0x200

    const/16 v2, 0x80

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/street/LabelMaker;-><init>(ZII)V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iput-boolean v4, p0, Lcom/google/android/street/Renderer;->mLabelsDirty:Z

    new-instance v0, Lcom/google/android/street/Renderer$RenderStatus;

    invoke-direct {v0}, Lcom/google/android/street/Renderer$RenderStatus;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mCurrentRenderStatus:Lcom/google/android/street/Renderer$RenderStatus;

    new-instance v0, Lcom/google/android/street/Renderer$MessageQueue;

    invoke-direct {v0, v3}, Lcom/google/android/street/Renderer$MessageQueue;-><init>(Lcom/google/android/street/Renderer$1;)V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    new-instance v0, Lcom/google/android/street/Projector;

    invoke-direct {v0}, Lcom/google/android/street/Projector;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mProjector:Lcom/google/android/street/Projector;

    return-void
.end method

.method private addPanoramaTile(Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;)V
    .locals 3
    .param p1    # Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;

    iget-object v1, p1, Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;->mKey:Lcom/google/android/street/PanoramaImageKey;

    iget-object v0, p1, Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mTextureCache:Lcom/google/android/street/TextureCache;

    invoke-virtual {v2, v1}, Lcom/google/android/street/TextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mTextureCache:Lcom/google/android/street/TextureCache;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/street/TextureCache;->insertImage(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method private checkEglError(Ljavax/microedition/khronos/egl/EGL10;)Z
    .locals 6
    .param p1    # Ljavax/microedition/khronos/egl/EGL10;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-interface {p1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    const-string v1, "EGL error: %d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    move v1, v5

    :goto_0
    return v1

    :cond_0
    move v1, v4

    goto :goto_0
.end method

.method private createLabels(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v1, p1}, Lcom/google/android/street/LabelMaker;->beginAdding(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/street/Renderer;->mCopyrightID:I

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v1, Lcom/google/android/street/PanoramaConfig;->mCopyright:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mCopyrightPaint:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v0, v2, v3}, Lcom/google/android/street/LabelMaker;->add(Ljavax/microedition/khronos/opengles/GL10;Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Paint;)I

    move-result v1

    iput v1, p0, Lcom/google/android/street/Renderer;->mCopyrightID:I

    :cond_0
    iget-object v1, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mGoogleLogo:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1, v2}, Lcom/google/android/street/LabelMaker;->add(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/drawable/Drawable;)I

    move-result v1

    iput v1, p0, Lcom/google/android/street/Renderer;->mGoogleLogoLabelId:I

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v1, p1}, Lcom/google/android/street/LabelMaker;->endAdding(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method private dispatch(Lcom/google/android/street/Renderer$Message;)V
    .locals 3
    .param p1    # Lcom/google/android/street/Renderer$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget v0, p1, Lcom/google/android/street/Renderer$Message;->what:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/street/Renderer$Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->doSurfaceCreated()V

    :goto_0
    :pswitch_1
    return-void

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->doSurfaceDestroyed()V

    goto :goto_0

    :pswitch_3
    iget v0, p1, Lcom/google/android/street/Renderer$Message;->arg1:I

    iget v1, p1, Lcom/google/android/street/Renderer$Message;->arg2:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/Renderer;->doSurfaceChanged(II)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p1, Lcom/google/android/street/Renderer$Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doSetPanoramaConfig(Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;)V

    goto :goto_0

    :pswitch_5
    iget v0, p1, Lcom/google/android/street/Renderer$Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doSetZoomLevels(I)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p1, Lcom/google/android/street/Renderer$Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/street/UserOrientation;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doSetUserOrientation(Lcom/google/android/street/UserOrientation;)V

    goto :goto_0

    :pswitch_7
    iget v0, p1, Lcom/google/android/street/Renderer$Message;->arg1:I

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iget v1, p1, Lcom/google/android/street/Renderer$Message;->arg2:I

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/Renderer;->doSetPancake(FF)V

    goto :goto_0

    :pswitch_8
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->doClearPancake()V

    goto :goto_0

    :pswitch_9
    iget-object v0, p1, Lcom/google/android/street/Renderer$Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/street/Renderer$Transition;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doStartTransition(Lcom/google/android/street/Renderer$Transition;)V

    goto :goto_0

    :pswitch_a
    iget-object v0, p1, Lcom/google/android/street/Renderer$Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->addPanoramaTile(Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;)V

    goto :goto_0

    :pswitch_b
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->doRenderingPause()V

    goto :goto_0

    :pswitch_c
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->doRenderingResume()V

    goto :goto_0

    :pswitch_d
    iget v0, p1, Lcom/google/android/street/Renderer$Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doSetHighlight(I)V

    goto :goto_0

    :pswitch_e
    iget v0, p1, Lcom/google/android/street/Renderer$Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doSetMotionUse(I)V

    goto :goto_0

    :pswitch_f
    iget-object v0, p1, Lcom/google/android/street/Renderer$Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doFence(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_10
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->doShutdown()V

    goto :goto_0

    :pswitch_11
    iget v0, p1, Lcom/google/android/street/Renderer$Message;->arg1:I

    iget v1, p1, Lcom/google/android/street/Renderer$Message;->arg2:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/Renderer;->doSetRouteParamters(II)V

    goto :goto_0

    :pswitch_12
    iget-object v0, p1, Lcom/google/android/street/Renderer$Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doPixelToYawPitch(Lcom/google/android/street/Renderer$PixelToYawPitchArgs;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_a
        :pswitch_1
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private doClearPancake()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mPancake:Lcom/google/android/street/Overlay$Pancake;

    return-void
.end method

.method private doFence(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private doPixelToYawPitch(Lcom/google/android/street/Renderer$PixelToYawPitchArgs;)V
    .locals 12
    .param p1    # Lcom/google/android/street/Renderer$PixelToYawPitchArgs;

    const/high16 v11, 0x3f800000

    const/4 v7, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    if-nez v2, :cond_0

    const/4 v1, 0x0

    iput-object v1, p1, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mYawPitchOut:[F

    :goto_0
    monitor-enter p1

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p1, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mDone:Z

    invoke-virtual {p1}, Ljava/lang/Object;->notify()V

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/street/Renderer;->mProjector:Lcom/google/android/street/Projector;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    invoke-virtual {v2, v3}, Lcom/google/android/street/Projector;->getCurrentProjection(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-boolean v2, p1, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mIsVehicleRelative:Z

    if-eqz v2, :cond_3

    const/16 v2, 0x10

    new-array v0, v2, [F

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getRotationMatrix()[F

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v3}, Lcom/google/android/street/PanoramaConfig;->getTiltMatrix()[F

    move-result-object v4

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget v2, v2, Lcom/google/android/street/PanoramaConfig;->mPanoYawDeg:F

    neg-float v2, v2

    move v3, v7

    move v4, v11

    move v5, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    :goto_1
    const/4 v2, 0x4

    new-array v6, v2, [F

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mProjector:Lcom/google/android/street/Projector;

    const/4 v3, 0x3

    new-array v3, v3, [F

    iget v4, p1, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mX:F

    aput v4, v3, v1

    iget v4, p0, Lcom/google/android/street/Renderer;->mHeight:I

    int-to-float v4, v4

    iget v5, p1, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mY:F

    sub-float/2addr v4, v5

    aput v4, v3, v9

    aput v11, v3, v10

    move v4, v1

    move-object v5, v0

    move v7, v1

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/street/Projector;->unproject([FI[F[FI)V

    new-array v8, v10, [F

    aget v2, v6, v1

    aget v3, v6, v9

    aget v4, v6, v10

    invoke-static {v2, v3, v4, v8}, Lcom/google/android/street/StreetMath;->rectangularToSphericalCoords(FFF[F)V

    iget-boolean v2, p1, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mIsVehicleRelative:Z

    if-nez v2, :cond_1

    aget v2, v8, v1

    const/high16 v3, 0x3f000000

    add-float/2addr v2, v3

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->normalizeUnitAngle(F)F

    move-result v2

    aput v2, v8, v1

    :cond_1
    aget v1, v8, v1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_2

    aget v1, v8, v9

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    const/4 v1, 0x0

    :goto_2
    iput-object v1, p1, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mYawPitchOut:[F

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getRotationMatrix()[F

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v1, v8

    goto :goto_2

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private doRenderingPause()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->shutdownEgl()V

    return-void
.end method

.method private doRenderingResume()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->startEgl()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Renderer;->mSizeChanged:Z

    return-void
.end method

.method private doSetHighlight(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Overlay;->setHighlight(I)V

    return-void
.end method

.method private doSetMotionUse(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/street/Overlay;->doSetMotionUse(IJ)V

    return-void
.end method

.method private doSetPancake(FF)V
    .locals 12
    .param p1    # F
    .param p2    # F

    const/4 v5, 0x2

    const/4 v11, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v8, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;

    invoke-direct {v8, p1, p2, v6}, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;-><init>(FFZ)V

    invoke-direct {p0, v8}, Lcom/google/android/street/Renderer;->doPixelToYawPitch(Lcom/google/android/street/Renderer$PixelToYawPitchArgs;)V

    new-array v10, v5, [F

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v1, v8, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mYawPitchOut:[F

    aget v1, v1, v6

    iget-object v2, v8, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mYawPitchOut:[F

    aget v2, v2, v11

    invoke-virtual {v0, v1, v2, v10}, Lcom/google/android/street/PanoramaConfig;->worldToVehicleYawPitch(FF[F)V

    const/4 v0, 0x3

    new-array v9, v0, [F

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0}, Lcom/google/android/street/DepthMap;->decompress()Z

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    aget v1, v10, v6

    aget v2, v10, v11

    invoke-virtual {v0, v1, v2, v9}, Lcom/google/android/street/DepthMap;->computeDepthAndNormal(FF[F)F

    move-result v4

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    aget v1, v10, v6

    aget v2, v10, v11

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/DepthMap;->isGround(FF)Z

    move-result v7

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    aget v1, v9, v6

    aget v2, v9, v11

    aget v3, v9, v5

    invoke-virtual {v0, v1, v2, v3, v9}, Lcom/google/android/street/PanoramaConfig;->vehicleToWorld(FFF[F)V

    new-instance v0, Lcom/google/android/street/Overlay$Pancake;

    aget v1, v9, v6

    aget v2, v9, v11

    aget v3, v9, v5

    iget-object v5, v8, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mYawPitchOut:[F

    aget v5, v5, v6

    iget-object v6, v8, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mYawPitchOut:[F

    aget v6, v6, v11

    invoke-direct/range {v0 .. v7}, Lcom/google/android/street/Overlay$Pancake;-><init>(FFFFFFZ)V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mPancake:Lcom/google/android/street/Overlay$Pancake;

    goto :goto_0
.end method

.method private declared-synchronized doSetPanoramaConfig(Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;)V
    .locals 3
    .param p1    # Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/street/Renderer;->mNextPanoramaConfig:Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0}, Lcom/google/android/street/DepthMap;->compress()Z

    :cond_2
    iget-object v0, p1, Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Renderer;->mConfigurationChanged:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Renderer;->mLabelsDirty:Z

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mViewpointDetector:Lcom/google/android/street/ViewpointDetector;

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/ViewpointDetector;->checkConfig(Lcom/google/android/street/PanoramaConfig;Lcom/google/android/street/UserOrientation;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private doSetRouteParamters(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay;->setDirectionsArrowParams(FF)V

    return-void
.end method

.method private doSetUserOrientation(Lcom/google/android/street/UserOrientation;)V
    .locals 2
    .param p1    # Lcom/google/android/street/UserOrientation;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mViewpointDetector:Lcom/google/android/street/ViewpointDetector;

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v0, v1}, Lcom/google/android/street/ViewpointDetector;->checkOrientation(Lcom/google/android/street/UserOrientation;)V

    goto :goto_0
.end method

.method private doSetZoomLevels(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/street/Renderer;->mZoomLevels:I

    goto :goto_0
.end method

.method private doShutdown()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Renderer;->mShuttingDown:Z

    return-void
.end method

.method private doStartTransition(Lcom/google/android/street/Renderer$Transition;)V
    .locals 1
    .param p1    # Lcom/google/android/street/Renderer$Transition;

    iput-object p1, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mNextPanoramaConfig:Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    return-void
.end method

.method private doSurfaceChanged(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    const/4 v6, 0x1

    iget v2, p0, Lcom/google/android/street/Renderer;->mWidth:I

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/android/street/Renderer;->mHeight:I

    if-nez v2, :cond_1

    move v0, v6

    :goto_0
    if-nez v0, :cond_0

    const-string v2, "Window changed size: %d,%d -> %d,%d Recreating OpenGL surface"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/street/Renderer;->mWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget v4, p0, Lcom/google/android/street/Renderer;->mHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->recreateSurface()V

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->recreateSurface()V

    :cond_0
    iput p1, p0, Lcom/google/android/street/Renderer;->mWidth:I

    iput p2, p0, Lcom/google/android/street/Renderer;->mHeight:I

    iput-boolean v6, p0, Lcom/google/android/street/Renderer;->mSizeChanged:Z

    iget v2, p0, Lcom/google/android/street/Renderer;->mWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/street/Renderer;->mHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Lcom/google/android/street/Renderer;->getUnzoomedHorizontalFov(F)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mViewpointDetector:Lcom/google/android/street/ViewpointDetector;

    invoke-virtual {v2, v1}, Lcom/google/android/street/ViewpointDetector;->setHorizontalFovDegrees(F)V

    return-void

    :cond_1
    move v0, v5

    goto :goto_0
.end method

.method private doSurfaceCreated()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Renderer;->mHaveSurface:Z

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->startEgl()V

    return-void
.end method

.method private doSurfaceDestroyed()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->shutdownEgl()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/Renderer;->mHaveSurface:Z

    return-void
.end method

.method private draw(J)V
    .locals 5
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget v3, p0, Lcom/google/android/street/Renderer;->mWidth:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/street/Renderer;->mHeight:I

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-boolean v3, p0, Lcom/google/android/street/Renderer;->mSizeChanged:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x2

    move v2, v3

    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/street/Renderer;->drawFrame(J)V

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mEGLDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x1

    move v2, v3

    goto :goto_0
.end method

.method private drawFrame(J)V
    .locals 2
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/street/Renderer;->drawPanorama(Ljavax/microedition/khronos/opengles/GL10;J)V

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->drawLabels(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/street/Renderer;->mLabelsDirty:Z

    iput-boolean v1, p0, Lcom/google/android/street/Renderer;->mSizeChanged:Z

    iput-boolean v1, p0, Lcom/google/android/street/Renderer;->mConfigurationChanged:Z

    return-void
.end method

.method private drawLabels(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    const/high16 v4, 0x41200000

    const/high16 v3, 0x40000000

    iget-boolean v0, p0, Lcom/google/android/street/Renderer;->mLabelsDirty:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/street/Renderer;->createLabels(Ljavax/microedition/khronos/opengles/GL10;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget v1, p0, Lcom/google/android/street/Renderer;->mWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/street/Renderer;->mHeight:I

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/street/LabelMaker;->beginDrawing(Ljavax/microedition/khronos/opengles/GL10;FF)V

    iget v0, p0, Lcom/google/android/street/Renderer;->mCopyrightID:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget v1, p0, Lcom/google/android/street/Renderer;->mCopyrightID:I

    invoke-virtual {v0, v1}, Lcom/google/android/street/LabelMaker;->getWidth(I)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget v2, p0, Lcom/google/android/street/Renderer;->mWidth:I

    int-to-float v2, v2

    add-float/2addr v0, v3

    sub-float v0, v2, v0

    iget v2, p0, Lcom/google/android/street/Renderer;->mCopyrightID:I

    invoke-virtual {v1, p1, v0, v3, v2}, Lcom/google/android/street/LabelMaker;->draw(Ljavax/microedition/khronos/opengles/GL10;FFI)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget v1, p0, Lcom/google/android/street/Renderer;->mGoogleLogoLabelId:I

    invoke-virtual {v0, p1, v4, v4, v1}, Lcom/google/android/street/LabelMaker;->draw(Ljavax/microedition/khronos/opengles/GL10;FFI)V

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->endDrawing(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method private drawPanorama(Ljavax/microedition/khronos/opengles/GL10;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/google/android/street/Renderer;->mConfigurationChanged:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget v0, v0, Lcom/google/android/street/PanoramaConfig;->mProjectionType:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/street/Sphere;

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    invoke-direct {v0, v1, p0}, Lcom/google/android/street/Sphere;-><init>(Lcom/google/android/street/PanoramaManager;Lcom/google/android/street/PanoramaManager$PanoFetchListener;)V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaDrawable:Lcom/google/android/street/Renderer$PanoramaDrawable;

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->isReadyToEndTransition()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0}, Lcom/google/android/street/DepthMap;->compress()Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    iget-object v0, v0, Lcom/google/android/street/Renderer$Transition;->mNewUserOrientation:Lcom/google/android/street/UserOrientation;

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    iput-object v2, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mNextPanoramaConfig:Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->doSetPanoramaConfig(Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;)V

    iput-object v2, p0, Lcom/google/android/street/Renderer;->mNextPanoramaConfig:Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    :cond_2
    iget v0, p0, Lcom/google/android/street/Renderer;->mWidth:I

    iget v1, p0, Lcom/google/android/street/Renderer;->mHeight:I

    iget-boolean v2, p0, Lcom/google/android/street/Renderer;->mSizeChanged:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/street/Renderer;->mConfigurationChanged:Z

    if-eqz v2, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mPanoramaDrawable:Lcom/google/android/street/Renderer$PanoramaDrawable;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mTextureCache:Lcom/google/android/street/TextureCache;

    invoke-interface {v2, v3, v4, v0, v1}, Lcom/google/android/street/Renderer$PanoramaDrawable;->initialize(Lcom/google/android/street/PanoramaConfig;Lcom/google/android/street/TextureCache;II)V

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v2, v6, v6, v0, v1}, Lcom/google/android/street/Projector;->setCurrentView(IIII)V

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/street/Overlay;->initialize(Lcom/google/android/street/PanoramaConfig;II)V

    :cond_4
    const/high16 v2, 0x3f800000

    invoke-interface {p1, v5, v5, v5, v2}, Ljavax/microedition/khronos/opengles/GL10;->glClearColor(FFFF)V

    const/16 v2, 0x4000

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v2}, Lcom/google/android/street/PanoramaConfig;->serviceTemporarilyUnavailable()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mPanoramaDrawable:Lcom/google/android/street/Renderer$PanoramaDrawable;

    if-nez v2, :cond_6

    :cond_5
    :goto_1
    return-void

    :pswitch_0
    new-instance v0, Lcom/google/android/street/Cube;

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mPanoramaManager:Lcom/google/android/street/PanoramaManager;

    invoke-direct {v0, v1, p0}, Lcom/google/android/street/Cube;-><init>(Lcom/google/android/street/PanoramaManager;Lcom/google/android/street/PanoramaManager$PanoFetchListener;)V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaDrawable:Lcom/google/android/street/Renderer$PanoramaDrawable;

    goto :goto_0

    :cond_6
    invoke-interface {p1, v6, v6, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    :goto_2
    iget-object v1, p0, Lcom/google/android/street/Renderer;->mPanoramaDrawable:Lcom/google/android/street/Renderer$PanoramaDrawable;

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mCurrentRenderStatus:Lcom/google/android/street/Renderer$RenderStatus;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    invoke-interface {v1, p1, v0, v2, v3}, Lcom/google/android/street/Renderer$PanoramaDrawable;->draw(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/Renderer$RenderStatus;Lcom/google/android/street/Renderer$Transition;)V

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mRenderStatusReceiver:Lcom/google/android/street/Renderer$RenderStatusReceiver;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v0}, Lcom/google/android/street/PanoramaConfig;->someRequestsWillBeDenied()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x2710

    :goto_3
    iget v1, p0, Lcom/google/android/street/Renderer;->mLastRenderProgress:I

    if-eq v0, v1, :cond_7

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mRenderStatusReceiver:Lcom/google/android/street/Renderer$RenderStatusReceiver;

    invoke-interface {v1, v0}, Lcom/google/android/street/Renderer$RenderStatusReceiver;->reportRenderStatusProgress(I)V

    iput v0, p0, Lcom/google/android/street/Renderer;->mLastRenderProgress:I

    :cond_7
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mRenderStatusReceiver:Lcom/google/android/street/Renderer$RenderStatusReceiver;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/google/android/street/Renderer$RenderStatusReceiver;->reportTransitionProgress(I)V

    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mPancake:Lcom/google/android/street/Overlay$Pancake;

    move-object v1, p1

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/street/Overlay;->draw(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/Overlay$Pancake;J)V

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    iget-object v0, v0, Lcom/google/android/street/Renderer$Transition;->mNewUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v0}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v1}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/street/StreetMath;->angleSubtractDegrees(FF)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    iget-object v1, v1, Lcom/google/android/street/Renderer$Transition;->mNewUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v1}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v2}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v2

    sub-float/2addr v1, v2

    new-instance v2, Lcom/google/android/street/UserOrientation;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    invoke-virtual {v4}, Lcom/google/android/street/Renderer$Transition;->getProgress()F

    move-result v4

    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mUserOrientation:Lcom/google/android/street/UserOrientation;

    invoke-virtual {v3}, Lcom/google/android/street/UserOrientation;->getTilt()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    invoke-virtual {v4}, Lcom/google/android/street/Renderer$Transition;->getProgress()F

    move-result v4

    mul-float/2addr v1, v4

    add-float/2addr v1, v3

    invoke-direct {v2, v0, v1, v5}, Lcom/google/android/street/UserOrientation;-><init>(FFF)V

    move-object v0, v2

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mCurrentRenderStatus:Lcom/google/android/street/Renderer$RenderStatus;

    invoke-virtual {v0}, Lcom/google/android/street/Renderer$RenderStatus;->getProgress()I

    move-result v0

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lcom/google/android/street/Renderer;->mRenderStatusReceiver:Lcom/google/android/street/Renderer$RenderStatusReceiver;

    const v1, 0x461c4000

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    invoke-virtual {v2}, Lcom/google/android/street/Renderer$Transition;->getUnboundedProgress()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-interface {v0, v1}, Lcom/google/android/street/Renderer$RenderStatusReceiver;->reportTransitionProgress(I)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private finishMainLoop()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->shutdownEgl()V

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    return-void
.end method

.method public static getUnzoomedHorizontalFov(F)F
    .locals 5
    .param p0    # F

    invoke-static {p0}, Lcom/google/android/street/Renderer;->isLandscape(F)Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x42b40000

    :goto_0
    return v1

    :cond_0
    sget-wide v1, Lcom/google/android/street/Renderer;->LONGER_HALF_FOV_TANGENT:D

    float-to-double v3, p0

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    move-result-wide v1

    double-to-float v0, v1

    const/high16 v1, 0x40000000

    mul-float/2addr v1, v0

    invoke-static {v1}, Lcom/google/android/street/StreetMath;->radiansToDegrees(F)F

    move-result v1

    goto :goto_0
.end method

.method public static getUnzoomedVerticalFov(F)F
    .locals 5
    .param p0    # F

    invoke-static {p0}, Lcom/google/android/street/Renderer;->isLandscape(F)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-wide v1, Lcom/google/android/street/Renderer;->LONGER_HALF_FOV_TANGENT:D

    float-to-double v3, p0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    move-result-wide v1

    double-to-float v0, v1

    const/high16 v1, 0x40000000

    mul-float/2addr v1, v0

    invoke-static {v1}, Lcom/google/android/street/StreetMath;->radiansToDegrees(F)F

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/high16 v1, 0x42b40000

    goto :goto_0
.end method

.method private initGL(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v2

    check-cast v2, Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mEGLDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEGLConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v4, 0x0

    invoke-interface {v2, v1, v0, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v5, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v2, v1, v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mTextureCache:Lcom/google/android/street/TextureCache;

    invoke-virtual {v3, p1}, Lcom/google/android/street/TextureCache;->initialize(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v3, p1}, Lcom/google/android/street/LabelMaker;->initialize(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/street/Renderer;->mLabelsDirty:Z

    const/16 v3, 0xb71

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v3, 0xc11

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v3, 0xbd0

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v3, 0xb50

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v3, 0xbe2

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v3, 0xc50

    const/16 v4, 0x1102

    invoke-interface {p1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glHint(II)V

    const/16 v3, 0x1d00

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glShadeModel(I)V

    const/16 v3, 0xb44

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v3, 0x901

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glFrontFace(I)V

    const/16 v3, 0x203

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    return-void
.end method

.method private static isLandscape(F)Z
    .locals 1
    .param p0    # F

    const/high16 v0, 0x3f800000

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isReadyToEndTransition()Z
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v5, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    invoke-virtual {v5}, Lcom/google/android/street/Renderer$Transition;->isComplete()Z

    move-result v5

    if-eqz v5, :cond_4

    move v3, v7

    :goto_0
    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/google/android/street/Renderer;->mNextPanoramaConfig:Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    if-eqz v5, :cond_2

    const/4 v2, 0x1

    iget-object v5, p0, Lcom/google/android/street/Renderer;->mNextPanoramaConfig:Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    iget-object v5, v5, Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v5, v5, Lcom/google/android/street/PanoramaConfig;->mRootImageKeys:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/street/PanoramaImageKey;

    iget-object v5, p0, Lcom/google/android/street/Renderer;->mTextureCache:Lcom/google/android/street/TextureCache;

    invoke-virtual {v5, v4}, Lcom/google/android/street/TextureCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    const/4 v2, 0x0

    :cond_1
    iget-object v5, p0, Lcom/google/android/street/Renderer;->mNextPanoramaConfig:Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    iget-object v5, v5, Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-boolean v1, v5, Lcom/google/android/street/PanoramaConfig;->mDisabled:Z

    :cond_2
    if-eqz v3, :cond_5

    if-nez v2, :cond_3

    if-eqz v1, :cond_5

    :cond_3
    move v5, v7

    :goto_1
    return v5

    :cond_4
    move v3, v6

    goto :goto_0

    :cond_5
    move v5, v6

    goto :goto_1
.end method

.method private nextMessage(Lcom/google/android/street/Renderer$Message;)Lcom/google/android/street/Renderer$Message;
    .locals 2
    .param p1    # Lcom/google/android/street/Renderer$Message;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    monitor-enter v0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v1, p1}, Lcom/google/android/street/Renderer$MessageQueue;->recycle(Lcom/google/android/street/Renderer$Message;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v1}, Lcom/google/android/street/Renderer$MessageQueue;->get()Lcom/google/android/street/Renderer$Message;

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private outerDraw()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/Renderer;->draw(J)V

    :cond_0
    return-void
.end method

.method private recreateSurface()V
    .locals 7

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mEGLDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v2, v1, v4, v5, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    invoke-interface {v2, v1, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEGLConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v5, 0x0

    invoke-interface {v2, v1, v0, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v5, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v6, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v2, v1, v4, v5, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    invoke-direct {p0, v2}, Lcom/google/android/street/Renderer;->checkEglError(Ljavax/microedition/khronos/egl/EGL10;)Z

    :cond_0
    return-void
.end method

.method private final send(IIILjava/lang/Object;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v1}, Lcom/google/android/street/Renderer$MessageQueue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/android/street/Renderer$MessageQueue;->send(IIILjava/lang/Object;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private final send(ILjava/lang/Object;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0, p2}, Lcom/google/android/street/Renderer;->send(IIILjava/lang/Object;)V

    return-void
.end method

.method private final sendCoalesce(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v1, v0}, Lcom/google/android/street/Renderer;->sendCoalesce(IIILjava/lang/Object;)V

    return-void
.end method

.method private final sendCoalesce(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/street/Renderer;->sendCoalesce(IIILjava/lang/Object;)V

    return-void
.end method

.method private final sendCoalesce(III)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/street/Renderer;->sendCoalesce(IIILjava/lang/Object;)V

    return-void
.end method

.method private final sendCoalesce(IIILjava/lang/Object;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v1}, Lcom/google/android/street/Renderer$MessageQueue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/android/street/Renderer$MessageQueue;->sendCoalesce(IIILjava/lang/Object;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private final sendCoalesce(ILjava/lang/Object;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0, p2}, Lcom/google/android/street/Renderer;->sendCoalesce(IIILjava/lang/Object;)V

    return-void
.end method

.method private shutdownEgl()V
    .locals 9

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mEGLDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/android/street/Renderer;->mEGLSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mTextureCache:Lcom/google/android/street/TextureCache;

    invoke-virtual {v4}, Lcom/google/android/street/TextureCache;->shutdown()V

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v4, v2}, Lcom/google/android/street/LabelMaker;->shutdown(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    invoke-virtual {v4, v2}, Lcom/google/android/street/Overlay;->shutdown(Ljavax/microedition/khronos/opengles/GL10;)V

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v7, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v4, v1, v5, v6, v7}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4, v1, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4, v1, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    :cond_0
    iget-object v4, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    :cond_1
    iput-object v8, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iput-object v8, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v4, Lcom/google/android/street/Renderer;->sEglSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_2
    return-void
.end method

.method private startEgl()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v10, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/street/Renderer;->mHaveSurface:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/street/Renderer;->sEglSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v9, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v9}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    const/4 v0, 0x2

    new-array v8, v0, [I

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0, v1, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    const/4 v0, 0x3

    new-array v2, v0, [I

    fill-array-data v2, :array_0

    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    new-array v5, v4, [I

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    const/4 v0, 0x0

    aget-object v6, v3, v0

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v6, v4, v10}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v7

    iput-object v1, p0, Lcom/google/android/street/Renderer;->mEGLDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iput-object v6, p0, Lcom/google/android/street/Renderer;->mEGLConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    iput-object v7, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->checkEglError(Ljavax/microedition/khronos/egl/EGL10;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v10, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    sget-object v0, Lcom/google/android/street/Renderer;->sEglSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v7}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    new-instance v0, Lcom/google/android/street/GLMatrixWrapper;

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    invoke-direct {v0, v4}, Lcom/google/android/street/GLMatrixWrapper;-><init>(Ljavax/microedition/khronos/opengles/GL;)V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->initGL(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0

    :array_0
    .array-data 4
        0x3025
        0x10
        0x3038
    .end array-data
.end method

.method private waitForFence()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-boolean v1, p0, Lcom/google/android/street/Renderer;->mShuttingDown:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/street/Renderer;->join()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    monitor-enter v0

    const/16 v1, 0xc

    :try_start_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/street/Renderer;->send(ILjava/lang/Object;)V

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private waitForWork()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const-wide/16 v7, 0x0

    iget-object v4, p0, Lcom/google/android/street/Renderer;->mTransition:Lcom/google/android/street/Renderer$Transition;

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    monitor-enter v4

    :goto_1
    :try_start_0
    iget-object v5, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v5}, Lcom/google/android/street/Renderer$MessageQueue;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/street/Renderer;->mEGLContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-nez v5, :cond_2

    move-wide v0, v7

    :goto_2
    cmp-long v5, v0, v7

    if-eqz v5, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v2, v0, v5

    cmp-long v5, v2, v7

    if-gtz v5, :cond_3

    :cond_1
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    invoke-virtual {v5}, Lcom/google/android/street/Overlay;->getNextDrawTime()J

    move-result-wide v5

    move-wide v0, v5

    goto :goto_2

    :cond_3
    iget-object v5, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v5, v2, v3}, Ljava/lang/Object;->wait(J)V

    goto :goto_1

    :cond_4
    iget-object v5, p0, Lcom/google/android/street/Renderer;->mMessageQueue:Lcom/google/android/street/Renderer$MessageQueue;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public assertNotOnRenderingThread()V
    .locals 2

    const-string v0, "This code can only be called outside the rendering thread."

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/google/mobile/googlenav/common/util/Assert;->assertNotEquals(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public clearPancake()V
    .locals 1

    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->sendCoalesce(I)V

    return-void
.end method

.method public getPanoramaLink(I)Lcom/google/android/street/PanoramaLink;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Overlay;->getPanoramaLink(I)Lcom/google/android/street/PanoramaLink;

    move-result-object v0

    return-object v0
.end method

.method public hit(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    iget v1, p0, Lcom/google/android/street/Renderer;->mHeight:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    sub-int v0, v1, p2

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/street/Overlay;->hit(II)I

    move-result v1

    return v1
.end method

.method public initialize(Landroid/content/Context;Landroid/view/SurfaceHolder;Lcom/google/android/street/Renderer$RenderStatusReceiver;ZZZ)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/SurfaceHolder;
    .param p3    # Lcom/google/android/street/Renderer$RenderStatusReceiver;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z

    iput-object p1, p0, Lcom/google/android/street/Renderer;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/street/Renderer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    new-instance v0, Lcom/google/android/street/TextureCache;

    const/16 v1, 0x11

    invoke-direct {v0, v1}, Lcom/google/android/street/TextureCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mTextureCache:Lcom/google/android/street/TextureCache;

    iput-object p3, p0, Lcom/google/android/street/Renderer;->mRenderStatusReceiver:Lcom/google/android/street/Renderer$RenderStatusReceiver;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mPanoramaDrawable:Lcom/google/android/street/Renderer$PanoramaDrawable;

    iget-object v0, p0, Lcom/google/android/street/Renderer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    new-instance v0, Lcom/google/android/street/Overlay;

    iget-object v1, p0, Lcom/google/android/street/Renderer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/street/Renderer;->mProjector:Lcom/google/android/street/Projector;

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/street/Overlay;-><init>(Landroid/content/Context;Lcom/google/android/street/Projector;ZZZ)V

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mOverlay:Lcom/google/android/street/Overlay;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/street/Renderer;->mGoogleLogo:Landroid/graphics/drawable/Drawable;

    const-string v0, "Renderer"

    invoke-virtual {p0, v0}, Lcom/google/android/street/Renderer;->setName(Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/street/Renderer;->setPriority(I)V

    invoke-virtual {p0}, Lcom/google/android/street/Renderer;->start()V

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/street/Renderer;->mReadyForMessages:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public pixelToYawPitch(FFZ)[F
    .locals 2
    .param p1    # F
    .param p2    # F
    .param p3    # Z

    invoke-virtual {p0}, Lcom/google/android/street/Renderer;->assertNotOnRenderingThread()V

    new-instance v0, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;-><init>(FFZ)V

    monitor-enter v0

    const/16 v1, 0xf

    :try_start_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/street/Renderer;->send(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    iget-boolean v1, v0, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mDone:Z

    if-eqz v1, :cond_0

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v1, v0, Lcom/google/android/street/Renderer$PixelToYawPitchArgs;->mYawPitchOut:[F

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public postPanoramaTileImage(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Lcom/google/android/street/PanoramaImageKey;
    .param p2    # Landroid/graphics/Bitmap;

    const/4 v0, 0x6

    new-instance v1, Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;

    invoke-direct {v1, p1, p2}, Lcom/google/android/street/Renderer$PanoramaKeyAndTexture;-><init>(Lcom/google/android/street/PanoramaImageKey;Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/Renderer;->send(ILjava/lang/Object;)V

    return-void
.end method

.method public renderingPause()V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->sendCoalesce(I)V

    return-void
.end method

.method public renderingResume()V
    .locals 1

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->sendCoalesce(I)V

    return-void
.end method

.method public run()V
    .locals 2

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/street/Renderer;->mReadyForMessages:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    :goto_0
    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/street/Renderer;->mShuttingDown:Z

    if-nez v1, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->nextMessage(Lcom/google/android/street/Renderer$Message;)Lcom/google/android/street/Renderer$Message;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->dispatch(Lcom/google/android/street/Renderer$Message;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->finishMainLoop()V

    :goto_2
    return-void

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v1

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->finishMainLoop()V

    throw v1

    :cond_0
    :try_start_5
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->outerDraw()V

    invoke-direct {p0}, Lcom/google/android/street/Renderer;->waitForWork()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->finishMainLoop()V

    goto :goto_2
.end method

.method public setDirectionsArrowParams(FF)V
    .locals 3
    .param p1    # F
    .param p2    # F

    const/16 v0, 0xe

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/street/Renderer;->sendCoalesce(III)V

    return-void
.end method

.method public setHighlight(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0xa

    invoke-direct {p0, v0, p1}, Lcom/google/android/street/Renderer;->sendCoalesce(II)V

    return-void
.end method

.method public setMotionUse(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0xb

    invoke-direct {p0, v0, p1}, Lcom/google/android/street/Renderer;->sendCoalesce(II)V

    return-void
.end method

.method public setPancake(FF)V
    .locals 3
    .param p1    # F
    .param p2    # F

    const/16 v0, 0x10

    invoke-static {p1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v1

    invoke-static {p2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/street/Renderer;->sendCoalesce(III)V

    return-void
.end method

.method public setPanoramaConfig(Lcom/google/android/street/PanoramaConfig;)V
    .locals 2
    .param p1    # Lcom/google/android/street/PanoramaConfig;

    const/4 v0, 0x3

    new-instance v1, Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;

    invoke-direct {v1, p1}, Lcom/google/android/street/Renderer$SetPanoramaConfigArgs;-><init>(Lcom/google/android/street/PanoramaConfig;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/street/Renderer;->sendCoalesce(ILjava/lang/Object;)V

    return-void
.end method

.method public setTimer(Lcom/google/android/street/Timer;)V
    .locals 0
    .param p1    # Lcom/google/android/street/Timer;

    iput-object p1, p0, Lcom/google/android/street/Renderer;->newTimer:Lcom/google/android/street/Timer;

    return-void
.end method

.method public setUserOrientation(Lcom/google/android/street/UserOrientation;)V
    .locals 1
    .param p1    # Lcom/google/android/street/UserOrientation;

    const/4 v0, 0x5

    invoke-direct {p0, v0, p1}, Lcom/google/android/street/Renderer;->sendCoalesce(ILjava/lang/Object;)V

    return-void
.end method

.method public setZoomLevels(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Lcom/google/android/street/Renderer;->sendCoalesce(II)V

    return-void
.end method

.method public startTransition(Lcom/google/android/street/Renderer$Transition;)V
    .locals 1
    .param p1    # Lcom/google/android/street/Renderer$Transition;

    const/16 v0, 0x12

    invoke-direct {p0, v0, p1}, Lcom/google/android/street/Renderer;->sendCoalesce(ILjava/lang/Object;)V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x2

    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/street/Renderer;->sendCoalesce(III)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->sendCoalesce(I)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/street/Renderer;->sendCoalesce(I)V

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/street/Renderer;->waitForFence()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
