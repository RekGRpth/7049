.class Lcom/google/android/street/Sphere;
.super Lcom/google/android/street/GeometryDrawer;
.source "Sphere.java"


# instance fields
.field private final mTempV:[F


# direct methods
.method public constructor <init>(Lcom/google/android/street/PanoramaManager;Lcom/google/android/street/PanoramaManager$PanoFetchListener;)V
    .locals 1
    .param p1    # Lcom/google/android/street/PanoramaManager;
    .param p2    # Lcom/google/android/street/PanoramaManager$PanoFetchListener;

    invoke-direct {p0, p1, p2}, Lcom/google/android/street/GeometryDrawer;-><init>(Lcom/google/android/street/PanoramaManager;Lcom/google/android/street/PanoramaManager$PanoFetchListener;)V

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/street/Sphere;->mTempV:[F

    return-void
.end method


# virtual methods
.method public genGrid(IIFFIFFFFZ)Lcom/google/android/street/Grid;
    .locals 18
    .param p1    # I
    .param p2    # I
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # F
    .param p7    # F
    .param p8    # F
    .param p9    # F
    .param p10    # Z

    if-eqz p5, :cond_0

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Invalid sphere face."

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    add-int/lit8 v14, p1, 0x1

    add-int/lit8 v13, p2, 0x1

    const/high16 v4, 0x3f800000

    sub-float v4, v4, p4

    sub-float v11, v4, p7

    new-instance v1, Lcom/google/android/street/Grid;

    invoke-direct {v1, v14, v13}, Lcom/google/android/street/Grid;-><init>(II)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v14, :cond_4

    int-to-float v4, v2

    move/from16 v0, p1

    int-to-float v0, v0

    move v5, v0

    div-float v16, v4, v5

    mul-float v7, v16, p8

    mul-float v4, v16, p6

    add-float v4, v4, p3

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    invoke-static {v4, v5, v6}, Lcom/google/android/street/StreetMath;->clamp(FFF)F

    move-result v9

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v13, :cond_3

    int-to-float v4, v3

    move/from16 v0, p2

    int-to-float v0, v0

    move v5, v0

    div-float v17, v4, v5

    const/high16 v4, 0x3f800000

    sub-float v4, v4, v17

    mul-float v8, v4, p9

    mul-float v4, v17, p7

    add-float/2addr v4, v11

    const/high16 v5, 0x3f000000

    mul-float/2addr v4, v5

    const/4 v5, 0x0

    const/high16 v6, 0x3f000000

    invoke-static {v4, v5, v6}, Lcom/google/android/street/StreetMath;->clamp(FFF)F

    move-result v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Sphere;->mTempV:[F

    move-object v4, v0

    const/4 v5, 0x0

    invoke-static {v9, v10, v4, v5}, Lcom/google/android/street/StreetMath;->sphericalToRectangularCoords(FF[FI)V

    if-eqz p10, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Sphere;->mConfig:Lcom/google/android/street/PanoramaConfig;

    move-object v4, v0

    iget-object v4, v4, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-eqz v4, :cond_2

    const/high16 v4, 0x43480000

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Sphere;->mConfig:Lcom/google/android/street/PanoramaConfig;

    move-object v5, v0

    iget-object v5, v5, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    const/4 v6, 0x0

    invoke-virtual {v5, v9, v10, v6}, Lcom/google/android/street/DepthMap;->computeDepthAndNormal(FF[F)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v12

    const/4 v4, 0x0

    cmpl-float v4, v12, v4

    if-nez v4, :cond_1

    const/high16 v12, 0x43480000

    :cond_1
    const/4 v15, 0x0

    :goto_2
    const/4 v4, 0x3

    if-ge v15, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Sphere;->mTempV:[F

    move-object v4, v0

    aget v5, v4, v15

    mul-float/2addr v5, v12

    aput v5, v4, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Sphere;->mTempV:[F

    move-object v4, v0

    const/4 v5, 0x3

    const/high16 v6, 0x3f800000

    aput v6, v4, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Sphere;->mTempV:[F

    move-object v4, v0

    const/4 v5, 0x0

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Sphere;->mTempV:[F

    move-object v5, v0

    const/4 v6, 0x1

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Sphere;->mTempV:[F

    move-object v6, v0

    const/4 v10, 0x2

    aget v6, v6, v10

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/street/Grid;->set(IIFFFFF)V

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_4
    return-object v1
.end method

.method public getFaceEnumValue(I)I
    .locals 4
    .param p1    # I

    const-wide/16 v0, 0x0

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/google/mobile/googlenav/common/util/Assert;->assertEquals(JJ)V

    const/4 v0, -0x1

    return v0
.end method

.method public initialize(Lcom/google/android/street/PanoramaConfig;Lcom/google/android/street/TextureCache;II)V
    .locals 7
    .param p1    # Lcom/google/android/street/PanoramaConfig;
    .param p2    # Lcom/google/android/street/TextureCache;
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/high16 v5, 0x3f800000

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/street/Sphere;->initializeImpl(Lcom/google/android/street/PanoramaConfig;Lcom/google/android/street/TextureCache;IIFI)V

    return-void
.end method
