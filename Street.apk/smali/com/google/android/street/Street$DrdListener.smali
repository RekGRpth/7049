.class Lcom/google/android/street/Street$DrdListener;
.super Ljava/lang/Object;
.source "Street.java"

# interfaces
.implements Lcom/google/mobile/googlenav/datarequest/DataRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/Street;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrdListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/street/Street;


# direct methods
.method private constructor <init>(Lcom/google/android/street/Street;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/street/Street;Lcom/google/android/street/Street$1;)V
    .locals 0
    .param p1    # Lcom/google/android/street/Street;
    .param p2    # Lcom/google/android/street/Street$1;

    invoke-direct {p0, p1}, Lcom/google/android/street/Street$DrdListener;-><init>(Lcom/google/android/street/Street;)V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/mobile/googlenav/datarequest/DataRequest;)V
    .locals 2
    .param p1    # Lcom/google/mobile/googlenav/datarequest/DataRequest;

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # getter for: Lcom/google/android/street/Street;->mDrdNetworkError:I
    invoke-static {v0}, Lcom/google/android/street/Street;->access$000(Lcom/google/android/street/Street;)I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # setter for: Lcom/google/android/street/Street;->mDrdNetworkError:I
    invoke-static {v0, v1}, Lcom/google/android/street/Street;->access$002(Lcom/google/android/street/Street;I)I

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # getter for: Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;
    invoke-static {v0}, Lcom/google/android/street/Street;->access$100(Lcom/google/android/street/Street;)Lcom/google/android/street/StreetView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # getter for: Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;
    invoke-static {v0}, Lcom/google/android/street/Street;->access$100(Lcom/google/android/street/Street;)Lcom/google/android/street/StreetView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->postInvalidate()V

    :cond_0
    return-void
.end method

.method public onNetworkError(IZLjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Z
    .param p3    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NetworkError "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # getter for: Lcom/google/android/street/Street;->mDrdNetworkError:I
    invoke-static {v0}, Lcom/google/android/street/Street;->access$000(Lcom/google/android/street/Street;)I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # setter for: Lcom/google/android/street/Street;->mDrdNetworkError:I
    invoke-static {v0, p1}, Lcom/google/android/street/Street;->access$002(Lcom/google/android/street/Street;I)I

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # getter for: Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;
    invoke-static {v0}, Lcom/google/android/street/Street;->access$100(Lcom/google/android/street/Street;)Lcom/google/android/street/StreetView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # getter for: Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;
    invoke-static {v0}, Lcom/google/android/street/Street;->access$100(Lcom/google/android/street/Street;)Lcom/google/android/street/StreetView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->postInvalidate()V

    :cond_0
    return-void
.end method

.method public onPermanentFailure(Lcom/google/mobile/googlenav/datarequest/DataRequest;)V
    .locals 2
    .param p1    # Lcom/google/mobile/googlenav/datarequest/DataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NetworkFailure "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/mobile/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/street/Street;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # getter for: Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;
    invoke-static {v0}, Lcom/google/android/street/Street;->access$100(Lcom/google/android/street/Street;)Lcom/google/android/street/StreetView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Street$DrdListener;->this$0:Lcom/google/android/street/Street;

    # getter for: Lcom/google/android/street/Street;->mStreetView:Lcom/google/android/street/StreetView;
    invoke-static {v0}, Lcom/google/android/street/Street;->access$100(Lcom/google/android/street/Street;)Lcom/google/android/street/StreetView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/street/StreetView;->postInvalidate()V

    :cond_0
    return-void
.end method
