.class Lcom/google/android/street/StreetView$OrientationSensorListener;
.super Ljava/lang/Object;
.source "StreetView.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/street/StreetView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OrientationSensorListener"
.end annotation


# instance fields
.field private final mAcceleration:[F

.field private final mAccelerationFilter:Lcom/google/android/street/ExponentialFilter;

.field private mDeclination:F

.field private mHasDeclination:Z

.field private mHasRotationVectorSensor:Z

.field private final mMagneticField:[F

.field private final mMagneticFieldFilter:Lcom/google/android/street/ExponentialFilter;

.field private final mRemappedRotationMatrix:[F

.field private final mRotationMatrix:[F

.field final synthetic this$0:Lcom/google/android/street/StreetView;


# direct methods
.method private constructor <init>(Lcom/google/android/street/StreetView;)V
    .locals 4

    const/16 v3, 0x10

    const/high16 v2, 0x3f000000

    const/4 v1, 0x3

    iput-object p1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mAcceleration:[F

    new-instance v0, Lcom/google/android/street/ExponentialFilter;

    invoke-direct {v0, v2, v1}, Lcom/google/android/street/ExponentialFilter;-><init>(FI)V

    iput-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mAccelerationFilter:Lcom/google/android/street/ExponentialFilter;

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mMagneticField:[F

    new-instance v0, Lcom/google/android/street/ExponentialFilter;

    invoke-direct {v0, v2, v1}, Lcom/google/android/street/ExponentialFilter;-><init>(FI)V

    iput-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mMagneticFieldFilter:Lcom/google/android/street/ExponentialFilter;

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mRotationMatrix:[F

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mRemappedRotationMatrix:[F

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/street/StreetView;Lcom/google/android/street/StreetView$1;)V
    .locals 0
    .param p1    # Lcom/google/android/street/StreetView;
    .param p2    # Lcom/google/android/street/StreetView$1;

    invoke-direct {p0, p1}, Lcom/google/android/street/StreetView$OrientationSensorListener;-><init>(Lcom/google/android/street/StreetView;)V

    return-void
.end method

.method private canUseRotationVectorSensor(Landroid/hardware/SensorManager;)Z
    .locals 1
    .param p1    # Landroid/hardware/SensorManager;

    invoke-static {}, Lcom/google/mobile/googlenav/android/AndroidBuilds;->isHoneycombSdk()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private orientedRotationMatrix()[F
    .locals 5

    iget-object v3, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    # getter for: Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;
    invoke-static {v3}, Lcom/google/android/street/StreetView;->access$700(Lcom/google/android/street/StreetView;)Lcom/google/android/street/Street;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/street/Street;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getOrientation()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    iget-object v3, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mRotationMatrix:[F

    :goto_0
    return-object v3

    :pswitch_0
    const/4 v0, 0x2

    const/16 v1, 0x81

    :goto_1
    iget-object v3, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mRotationMatrix:[F

    iget-object v4, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mRemappedRotationMatrix:[F

    invoke-static {v3, v0, v1, v4}, Landroid/hardware/SensorManager;->remapCoordinateSystem([FII[F)Z

    iget-object v3, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mRemappedRotationMatrix:[F

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x81

    const/16 v1, 0x82

    goto :goto_1

    :pswitch_2
    const/16 v0, 0x82

    const/4 v1, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateOrientation()V
    .locals 13

    const/4 v1, 0x0

    const/high16 v5, 0x3f800000

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    # invokes: Lcom/google/android/street/StreetView;->okToAct()Z
    invoke-static {v2}, Lcom/google/android/street/StreetView;->access$800(Lcom/google/android/street/StreetView;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mHasRotationVectorSensor:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mRotationMatrix:[F

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mAcceleration:[F

    iget-object v7, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mMagneticField:[F

    invoke-static {v2, v4, v6, v7}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z

    move-result v12

    if-nez v12, :cond_1

    const-string v1, "SV couldn\'t get an orientation reading"

    invoke-static {v1}, Lcom/google/android/street/Street;->logI(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/street/StreetView$OrientationSensorListener;->orientedRotationMatrix()[F

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mHasDeclination:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mDeclination:F

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    :cond_2
    const/high16 v8, 0x42b40000

    move-object v6, v0

    move v7, v1

    move v9, v5

    move v10, v3

    move v11, v3

    invoke-static/range {v6 .. v11}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    iget-object v1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    # invokes: Lcom/google/android/street/StreetView;->reportUserActivity()V
    invoke-static {v1}, Lcom/google/android/street/StreetView;->access$900(Lcom/google/android/street/StreetView;)V

    iget-object v1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    # getter for: Lcom/google/android/street/StreetView;->mUserOrientation:Lcom/google/android/street/UserOrientation;
    invoke-static {v1}, Lcom/google/android/street/StreetView;->access$1000(Lcom/google/android/street/StreetView;)Lcom/google/android/street/UserOrientation;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/street/UserOrientation;->setRotationMatrix([F)V

    iget-object v1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    # invokes: Lcom/google/android/street/StreetView;->updateRendererUserOrientation()V
    invoke-static {v1}, Lcom/google/android/street/StreetView;->access$1100(Lcom/google/android/street/StreetView;)V

    goto :goto_0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1    # Landroid/hardware/SensorEvent;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mAccelerationFilter:Lcom/google/android/street/ExponentialFilter;

    iget-object v1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mAcceleration:[F

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/ExponentialFilter;->filter([F[F)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mMagneticFieldFilter:Lcom/google/android/street/ExponentialFilter;

    iget-object v1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mMagneticField:[F

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/ExponentialFilter;->filter([F[F)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView$OrientationSensorListener;->updateOrientation()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mRotationMatrix:[F

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getRotationMatrixFromVector([F[F)V

    invoke-direct {p0}, Lcom/google/android/street/StreetView$OrientationSensorListener;->updateOrientation()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xb -> :sswitch_2
    .end sparse-switch
.end method

.method public register()V
    .locals 9

    const/4 v8, 0x1

    iget-object v1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    # getter for: Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;
    invoke-static {v1}, Lcom/google/android/street/StreetView;->access$700(Lcom/google/android/street/StreetView;)Lcom/google/android/street/Street;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/street/Street;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/street/StreetView$OrientationSensorListener;->canUseRotationVectorSensor(Landroid/hardware/SensorManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0xb

    invoke-virtual {v7, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    const/16 v2, 0x3e80

    invoke-virtual {v7, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    iput-boolean v8, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mHasRotationVectorSensor:Z

    :goto_0
    iget-object v1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    # getter for: Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;
    invoke-static {v1}, Lcom/google/android/street/StreetView;->access$700(Lcom/google/android/street/StreetView;)Lcom/google/android/street/Street;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/street/Street;->getApproximateLocation()Landroid/location/Location;

    move-result-object v6

    if-eqz v6, :cond_0

    new-instance v0, Landroid/hardware/GeomagneticField;

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v6}, Landroid/location/Location;->getAltitude()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v1

    iput v1, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mDeclination:F

    iput-boolean v8, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->mHasDeclination:Z

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v7, v8}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v7, p0, v1, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    const/4 v1, 0x2

    invoke-virtual {v7, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v7, p0, v1, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0
.end method

.method public unregister()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/street/StreetView$OrientationSensorListener;->this$0:Lcom/google/android/street/StreetView;

    # getter for: Lcom/google/android/street/StreetView;->mStreet:Lcom/google/android/street/Street;
    invoke-static {v0}, Lcom/google/android/street/StreetView;->access$700(Lcom/google/android/street/StreetView;)Lcom/google/android/street/Street;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/street/Street;->getSensorManager()Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method
