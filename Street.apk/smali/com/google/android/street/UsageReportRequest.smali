.class public Lcom/google/android/street/UsageReportRequest;
.super Lcom/google/mobile/googlenav/datarequest/BaseDataRequest;
.source "UsageReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/UsageReportRequest$Counters;
    }
.end annotation


# instance fields
.field private final indoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

.field private final outdoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

.field private wasQueued:Z

.field private wasSent:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/mobile/googlenav/datarequest/BaseDataRequest;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/street/UsageReportRequest;->wasQueued:Z

    iput-boolean v1, p0, Lcom/google/android/street/UsageReportRequest;->wasSent:Z

    new-instance v0, Lcom/google/android/street/UsageReportRequest$Counters;

    invoke-direct {v0, v1}, Lcom/google/android/street/UsageReportRequest$Counters;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/street/UsageReportRequest;->outdoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    new-instance v0, Lcom/google/android/street/UsageReportRequest$Counters;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/street/UsageReportRequest$Counters;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/street/UsageReportRequest;->indoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    return-void
.end method


# virtual methods
.method public addCounts(IIII)Z
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/street/UsageReportRequest;->wasSent:Z

    if-eqz v4, :cond_0

    monitor-exit p0

    move v4, v6

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/street/UsageReportRequest;->getCountersForSceneType(I)Lcom/google/android/street/UsageReportRequest$Counters;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/street/UsageReportRequest;->isImmediate()Z

    move-result v4

    if-nez v4, :cond_4

    if-lez p4, :cond_4

    move v0, v5

    :goto_1
    iget v4, v1, Lcom/google/android/street/UsageReportRequest$Counters;->panningCount:I

    add-int/2addr v4, p2

    iput v4, v1, Lcom/google/android/street/UsageReportRequest$Counters;->panningCount:I

    iget v4, v1, Lcom/google/android/street/UsageReportRequest$Counters;->zoomingCount:I

    add-int/2addr v4, p3

    iput v4, v1, Lcom/google/android/street/UsageReportRequest$Counters;->zoomingCount:I

    iget v4, v1, Lcom/google/android/street/UsageReportRequest$Counters;->navigationCount:I

    add-int/2addr v4, p4

    iput v4, v1, Lcom/google/android/street/UsageReportRequest$Counters;->navigationCount:I

    iget-boolean v4, p0, Lcom/google/android/street/UsageReportRequest;->wasQueued:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/street/UsageReportRequest;->indoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    invoke-virtual {v4}, Lcom/google/android/street/UsageReportRequest$Counters;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/street/UsageReportRequest;->outdoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    invoke-virtual {v4}, Lcom/google/android/street/UsageReportRequest$Counters;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/street/UsageReportRequest;->wasQueued:Z

    const/4 v3, 0x1

    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->getInstance()Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;

    move-result-object v2

    if-eqz v2, :cond_3

    if-eqz v3, :cond_5

    invoke-virtual {v2, p0}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->addDataRequest(Lcom/google/mobile/googlenav/datarequest/DataRequest;)V

    :cond_3
    :goto_2
    move v4, v5

    goto :goto_0

    :cond_4
    move v0, v6

    goto :goto_1

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_5
    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->suspend()V

    invoke-virtual {v2}, Lcom/google/mobile/googlenav/datarequest/DataRequestDispatcher;->unsuspend()V

    goto :goto_2
.end method

.method getCountersForSceneType(I)Lcom/google/android/street/UsageReportRequest$Counters;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/UsageReportRequest;->indoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/street/UsageReportRequest;->outdoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    goto :goto_0
.end method

.method public getRequestType()I
    .locals 1

    const/16 v0, 0x2d

    return v0
.end method

.method public declared-synchronized isImmediate()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/street/UsageReportRequest;->outdoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    iget v0, v0, Lcom/google/android/street/UsageReportRequest$Counters;->navigationCount:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/UsageReportRequest;->indoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    iget v0, v0, Lcom/google/android/street/UsageReportRequest$Counters;->navigationCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public readResponseData(Ljava/io/DataInput;)Z
    .locals 1
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/Streetview;->STREET_VIEW_REPORT_RESPONSE_PROTO:Lcom/google/mobile/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/mobile/googlenav/common/io/protocol/ProtoBufUtil;->readProtoBufResponse(Lcom/google/mobile/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized writeRequestData(Ljava/io/DataOutput;)V
    .locals 2
    .param p1    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/street/UsageReportRequest;->wasSent:Z

    new-instance v0, Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/Streetview;->STREET_VIEW_REPORT_PROTO:Lcom/google/mobile/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/mobile/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/android/street/UsageReportRequest;->outdoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    invoke-virtual {v1, v0}, Lcom/google/android/street/UsageReportRequest$Counters;->addViewpointProto(Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;)V

    iget-object v1, p0, Lcom/google/android/street/UsageReportRequest;->indoorCounters:Lcom/google/android/street/UsageReportRequest$Counters;

    invoke-virtual {v1, v0}, Lcom/google/android/street/UsageReportRequest$Counters;->addViewpointProto(Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/mobile/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
