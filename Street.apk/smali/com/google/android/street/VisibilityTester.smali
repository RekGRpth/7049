.class Lcom/google/android/street/VisibilityTester;
.super Ljava/lang/Object;
.source "VisibilityTester.java"


# instance fields
.field private final mMVP:[F

.field private final mMatrixGrabber:Lcom/google/android/street/MatrixGrabber;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/street/VisibilityTester;->mMVP:[F

    new-instance v0, Lcom/google/android/street/MatrixGrabber;

    invoke-direct {v0}, Lcom/google/android/street/MatrixGrabber;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/VisibilityTester;->mMatrixGrabber:Lcom/google/android/street/MatrixGrabber;

    return-void
.end method


# virtual methods
.method public frustumCullSpheres([FII[III)I
    .locals 8
    .param p1    # [F
    .param p2    # I
    .param p3    # I
    .param p4    # [I
    .param p5    # I
    .param p6    # I

    iget-object v0, p0, Lcom/google/android/street/VisibilityTester;->mMVP:[F

    const/4 v1, 0x0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Landroid/opengl/Visibility;->frustumCullSpheres([FI[FII[III)I

    move-result v0

    return v0
.end method

.method public getCurrentState(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/street/VisibilityTester;->mMatrixGrabber:Lcom/google/android/street/MatrixGrabber;

    invoke-virtual {v0, p1}, Lcom/google/android/street/MatrixGrabber;->getCurrentState(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lcom/google/android/street/VisibilityTester;->mMVP:[F

    iget-object v2, p0, Lcom/google/android/street/VisibilityTester;->mMatrixGrabber:Lcom/google/android/street/MatrixGrabber;

    iget-object v2, v2, Lcom/google/android/street/MatrixGrabber;->mProjection:[F

    iget-object v3, p0, Lcom/google/android/street/VisibilityTester;->mMatrixGrabber:Lcom/google/android/street/MatrixGrabber;

    iget-object v4, v3, Lcom/google/android/street/MatrixGrabber;->mModelView:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    return-void
.end method

.method public isVisibileTriangleList([FI[CII)I
    .locals 7
    .param p1    # [F
    .param p2    # I
    .param p3    # [C
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/street/VisibilityTester;->mMVP:[F

    const/4 v1, 0x0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Landroid/opengl/Visibility;->visibilityTest([FI[FI[CII)I

    move-result v0

    return v0
.end method
