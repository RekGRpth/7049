.class public Lcom/android/systemui/BeanBag$Board;
.super Landroid/widget/FrameLayout;
.source "BeanBag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/BeanBag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Board"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/BeanBag$Board$Bean;
    }
.end annotation


# static fields
.field static BEANS:[I

.field static COLORS:[I

.field static LUCKY:F

.field static MAX_RADIUS:I

.field static MAX_SCALE:F

.field static MIN_SCALE:F

.field static NUM_BEANS:I

.field static sRNG:Ljava/util/Random;


# instance fields
.field private boardHeight:I

.field private boardWidth:I

.field mAnim:Landroid/animation/TimeAnimator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/android/systemui/BeanBag$Board;->sRNG:Ljava/util/Random;

    const/16 v0, 0x28

    sput v0, Lcom/android/systemui/BeanBag$Board;->NUM_BEANS:I

    const v0, 0x3e4ccccd

    sput v0, Lcom/android/systemui/BeanBag$Board;->MIN_SCALE:F

    const/high16 v0, 0x3f800000

    sput v0, Lcom/android/systemui/BeanBag$Board;->MAX_SCALE:F

    const v0, 0x3a83126f

    sput v0, Lcom/android/systemui/BeanBag$Board;->LUCKY:F

    const/high16 v0, 0x44100000

    sget v1, Lcom/android/systemui/BeanBag$Board;->MAX_SCALE:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sput v0, Lcom/android/systemui/BeanBag$Board;->MAX_RADIUS:I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/systemui/BeanBag$Board;->BEANS:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/systemui/BeanBag$Board;->COLORS:[I

    return-void

    :array_0
    .array-data 4
        0x7f020063
        0x7f020063
        0x7f020063
        0x7f020063
        0x7f020064
        0x7f020064
        0x7f020065
        0x7f020065
        0x7f020066
    .end array-data

    :array_1
    .array-data 4
        -0xff3400
        -0x340000
        -0xffff34
        -0x100
        -0x8000
        -0xff3301
        -0xff80
        -0x7fff01
        -0x7f80
        -0x7f7f01
        -0x4f3f30
        -0x222223
        -0xcccccd
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setWillNotDraw(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/systemui/BeanBag$Board;)I
    .locals 1
    .param p0    # Lcom/android/systemui/BeanBag$Board;

    iget v0, p0, Lcom/android/systemui/BeanBag$Board;->boardHeight:I

    return v0
.end method

.method static synthetic access$100(Lcom/android/systemui/BeanBag$Board;)I
    .locals 1
    .param p0    # Lcom/android/systemui/BeanBag$Board;

    iget v0, p0, Lcom/android/systemui/BeanBag$Board;->boardWidth:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/systemui/BeanBag$Board;)V
    .locals 0
    .param p0    # Lcom/android/systemui/BeanBag$Board;

    invoke-direct {p0}, Lcom/android/systemui/BeanBag$Board;->reset()V

    return-void
.end method

.method static clamp(FFF)F
    .locals 1
    .param p0    # F
    .param p1    # F
    .param p2    # F

    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    cmpl-float v0, p0, p2

    if-lez v0, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    move p1, p0

    goto :goto_0
.end method

.method static dot(FFFF)F
    .locals 1
    .param p0    # F
    .param p1    # F
    .param p2    # F
    .param p3    # F

    mul-float v0, p0, p2

    add-float/2addr v0, p1

    add-float/2addr v0, p3

    return v0
.end method

.method static flip()Z
    .locals 1

    sget-object v0, Lcom/android/systemui/BeanBag$Board;->sRNG:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextBoolean()Z

    move-result v0

    return v0
.end method

.method static lerp(FFF)F
    .locals 1
    .param p0    # F
    .param p1    # F
    .param p2    # F

    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method static mag(FF)F
    .locals 2
    .param p0    # F
    .param p1    # F

    mul-float v0, p0, p0

    mul-float v1, p1, p1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method static pick([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)TE;"
        }
    .end annotation

    array-length v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/systemui/BeanBag$Board;->sRNG:Ljava/util/Random;

    array-length v1, p0

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    aget-object v0, p0, v0

    goto :goto_0
.end method

.method static pickInt([I)I
    .locals 2
    .param p0    # [I

    array-length v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/systemui/BeanBag$Board;->sRNG:Ljava/util/Random;

    array-length v1, p0

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    aget v0, p0, v0

    goto :goto_0
.end method

.method static randfrange(FF)F
    .locals 1
    .param p0    # F
    .param p1    # F

    sget-object v0, Lcom/android/systemui/BeanBag$Board;->sRNG:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/android/systemui/BeanBag$Board;->lerp(FFF)F

    move-result v0

    return v0
.end method

.method static randsign()I
    .locals 1

    sget-object v0, Lcom/android/systemui/BeanBag$Board;->sRNG:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private reset()V
    .locals 6

    const/4 v3, -0x2

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    const/4 v0, 0x0

    :goto_0
    sget v3, Lcom/android/systemui/BeanBag$Board;->NUM_BEANS:I

    if-ge v0, v3, :cond_0

    new-instance v1, Lcom/android/systemui/BeanBag$Board$Bean;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, p0, v3, v4}, Lcom/android/systemui/BeanBag$Board$Bean;-><init>(Lcom/android/systemui/BeanBag$Board;Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    int-to-float v3, v0

    sget v4, Lcom/android/systemui/BeanBag$Board;->NUM_BEANS:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, v1, Lcom/android/systemui/BeanBag$Board$Bean;->z:F

    iget v3, v1, Lcom/android/systemui/BeanBag$Board$Bean;->z:F

    iget v4, v1, Lcom/android/systemui/BeanBag$Board$Bean;->z:F

    mul-float/2addr v3, v4

    iput v3, v1, Lcom/android/systemui/BeanBag$Board$Bean;->z:F

    invoke-virtual {v1}, Lcom/android/systemui/BeanBag$Board$Bean;->reset()V

    iget v3, p0, Lcom/android/systemui/BeanBag$Board;->boardWidth:I

    int-to-float v3, v3

    invoke-static {v5, v3}, Lcom/android/systemui/BeanBag$Board;->randfrange(FF)F

    move-result v3

    iput v3, v1, Lcom/android/systemui/BeanBag$Board$Bean;->x:F

    iget v3, p0, Lcom/android/systemui/BeanBag$Board;->boardHeight:I

    int-to-float v3, v3

    invoke-static {v5, v3}, Lcom/android/systemui/BeanBag$Board;->randfrange(FF)F

    move-result v3

    iput v3, v1, Lcom/android/systemui/BeanBag$Board$Bean;->y:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/BeanBag$Board;->mAnim:Landroid/animation/TimeAnimator;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/BeanBag$Board;->mAnim:Landroid/animation/TimeAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_1
    new-instance v3, Landroid/animation/TimeAnimator;

    invoke-direct {v3}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v3, p0, Lcom/android/systemui/BeanBag$Board;->mAnim:Landroid/animation/TimeAnimator;

    iget-object v3, p0, Lcom/android/systemui/BeanBag$Board;->mAnim:Landroid/animation/TimeAnimator;

    new-instance v4, Lcom/android/systemui/BeanBag$Board$1;

    invoke-direct {v4, p0}, Lcom/android/systemui/BeanBag$Board$1;-><init>(Lcom/android/systemui/BeanBag$Board;)V

    invoke-virtual {v3, v4}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    return-void
.end method


# virtual methods
.method public isOpaque()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/android/systemui/BeanBag$Board;->stopAnimation()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1    # Landroid/graphics/Canvas;

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    iput p1, p0, Lcom/android/systemui/BeanBag$Board;->boardWidth:I

    iput p2, p0, Lcom/android/systemui/BeanBag$Board;->boardHeight:I

    return-void
.end method

.method public startAnimation()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/BeanBag$Board;->stopAnimation()V

    iget-object v0, p0, Lcom/android/systemui/BeanBag$Board;->mAnim:Landroid/animation/TimeAnimator;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/systemui/BeanBag$Board$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/BeanBag$Board$2;-><init>(Lcom/android/systemui/BeanBag$Board;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/BeanBag$Board;->mAnim:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public stopAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/BeanBag$Board;->mAnim:Landroid/animation/TimeAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/BeanBag$Board;->mAnim:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method
