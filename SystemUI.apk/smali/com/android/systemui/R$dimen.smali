.class public final Lcom/android/systemui/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final blinds_pop_threshold:I = 0x7f0b0030

.field public static final carrier_label_height:I = 0x7f0b002f

.field public static final close_handle_height:I = 0x7f0b0028

.field public static final close_handle_underlap:I = 0x7f0b0029

.field public static final collapse_accel:I = 0x7f0b001f

.field public static final collapse_min_display_fraction:I = 0x7f0b001c

.field public static final expand_accel:I = 0x7f0b001e

.field public static final expand_min_display_fraction:I = 0x7f0b001d

.field public static final fling_collapse_min_velocity:I = 0x7f0b0019

.field public static final fling_expand_min_velocity:I = 0x7f0b0018

.field public static final fling_gesture_max_output_velocity:I = 0x7f0b001b

.field public static final fling_gesture_max_x_velocity:I = 0x7f0b001a

.field public static final global_screenshot_bg_padding:I = 0x7f0b0020

.field public static final navbar_search_outerring_diameter:I = 0x7f0b0024

.field public static final navbar_search_outerring_radius:I = 0x7f0b0025

.field public static final navbar_search_panel_height:I = 0x7f0b0027

.field public static final navbar_search_snap_margin:I = 0x7f0b0023

.field public static final navbar_search_up_threshhold:I = 0x7f0b0026

.field public static final navigation_bar_deadzone_size:I = 0x7f0b000c

.field public static final navigation_bar_size:I = 0x7f0b000b

.field public static final navigation_key_width:I = 0x7f0b0021

.field public static final navigation_menu_key_width:I = 0x7f0b0022

.field public static final notification_divider_height:I = 0x7f0b0015

.field public static final notification_max_height:I = 0x7f0b000f

.field public static final notification_min_height:I = 0x7f0b000e

.field public static final notification_panel_header_height:I = 0x7f0b002a

.field public static final notification_panel_header_padding_top:I = 0x7f0b002c

.field public static final notification_panel_margin_bottom:I = 0x7f0b002d

.field public static final notification_panel_margin_left:I = 0x7f0b002e

.field public static final notification_panel_min_height:I = 0x7f0b003d

.field public static final notification_panel_padding_top:I = 0x7f0b002b

.field public static final notification_panel_width:I = 0x7f0b003a

.field public static final notification_row_max_height:I = 0x7f0b0011

.field public static final notification_row_min_height:I = 0x7f0b0010

.field public static final notification_ticker_width:I = 0x7f0b003b

.field public static final panel_float:I = 0x7f0b003e

.field public static final peek_window_y_offset:I = 0x7f0b000a

.field public static final pull_span_min:I = 0x7f0b0031

.field public static final scroll_view_margin_top:I = 0x7f0b0040

.field public static final self_collapse_velocity:I = 0x7f0b0017

.field public static final self_expand_velocity:I = 0x7f0b0016

.field public static final status_bar_edge_ignore:I = 0x7f0b0000

.field public static final status_bar_icon_drawing_alpha:I = 0x7f0b0013

.field public static final status_bar_icon_drawing_size:I = 0x7f0b0012

.field public static final status_bar_icon_padding:I = 0x7f0b0014

.field public static final status_bar_icon_size:I = 0x7f0b000d

.field public static final status_bar_panel_bottom_offset:I = 0x7f0b003c

.field public static final status_bar_recents_app_description_text_size:I = 0x7f0b0006

.field public static final status_bar_recents_app_icon_left_margin:I = 0x7f0b0038

.field public static final status_bar_recents_app_icon_max_height:I = 0x7f0b0002

.field public static final status_bar_recents_app_icon_max_width:I = 0x7f0b0001

.field public static final status_bar_recents_app_icon_top_margin:I = 0x7f0b0039

.field public static final status_bar_recents_app_label_left_margin:I = 0x7f0b0036

.field public static final status_bar_recents_app_label_text_size:I = 0x7f0b0005

.field public static final status_bar_recents_app_label_width:I = 0x7f0b0035

.field public static final status_bar_recents_item_padding:I = 0x7f0b0037

.field public static final status_bar_recents_right_glow_margin:I = 0x7f0b0009

.field public static final status_bar_recents_scroll_fading_edge_length:I = 0x7f0b0008

.field public static final status_bar_recents_text_description_padding:I = 0x7f0b0034

.field public static final status_bar_recents_text_fading_edge_length:I = 0x7f0b0007

.field public static final status_bar_recents_thumbnail_height:I = 0x7f0b0004

.field public static final status_bar_recents_thumbnail_left_margin:I = 0x7f0b0032

.field public static final status_bar_recents_thumbnail_top_margin:I = 0x7f0b0033

.field public static final status_bar_recents_thumbnail_width:I = 0x7f0b0003

.field public static final status_bar_recents_width:I = 0x7f0b003f

.field public static final status_bar_signal_bg_padding:I = 0x7f0b0043

.field public static final status_bar_signal_icon_expanded_inner_cu_width:I = 0x7f0b0047

.field public static final status_bar_signal_icon_expanded_outer_cu_width:I = 0x7f0b0046

.field public static final status_bar_signal_icon_inner_size:I = 0x7f0b0045

.field public static final status_bar_signal_icon_outer_size:I = 0x7f0b0044

.field public static final status_bar_sim_indicator_max_width:I = 0x7f0b0042

.field public static final status_bar_sim_indicator_min_width:I = 0x7f0b0041


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
