.class public final Lcom/android/systemui/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final adaptive:I = 0x7f0c0064

.field public static final airplane:I = 0x7f0c0059

.field public static final airplane_checkbox:I = 0x7f0c00b0

.field public static final airplane_icon:I = 0x7f0c00ae

.field public static final airplane_label:I = 0x7f0c00af

.field public static final alt:I = 0x7f0c00ab

.field public static final app_description:I = 0x7f0c006b

.field public static final app_icon:I = 0x7f0c0069

.field public static final app_label:I = 0x7f0c006a

.field public static final app_thumbnail:I = 0x7f0c0067

.field public static final app_thumbnail_image:I = 0x7f0c0068

.field public static final back:I = 0x7f0c004f

.field public static final bar_contents:I = 0x7f0c007b

.field public static final bar_contents_holder:I = 0x7f0c007a

.field public static final bar_shadow:I = 0x7f0c0080

.field public static final bar_shadow_holder:I = 0x7f0c007f

.field public static final battery:I = 0x7f0c0036

.field public static final battery_text:I = 0x7f0c00a5

.field public static final bluetooth:I = 0x7f0c009a

.field public static final bottom_glow:I = 0x7f0c0065

.field public static final brightness:I = 0x7f0c00ba

.field public static final brightness_icon:I = 0x7f0c00b9

.field public static final button:I = 0x7f0c000b

.field public static final carrier1:I = 0x7f0c003e

.field public static final carrier2:I = 0x7f0c0040

.field public static final carrier_divider:I = 0x7f0c003f

.field public static final carrier_label:I = 0x7f0c005a

.field public static final carrier_label_gemini:I = 0x7f0c003d

.field public static final clearButtonImg:I = 0x7f0c0044

.field public static final clear_all_button:I = 0x7f0c0043

.field public static final clock:I = 0x7f0c0037

.field public static final close:I = 0x7f0c0047

.field public static final closeImg:I = 0x7f0c0048

.field public static final compatModeButton:I = 0x7f0c0097

.field public static final compat_mode_off_radio:I = 0x7f0c0086

.field public static final compat_mode_on_radio:I = 0x7f0c0087

.field public static final compat_mode_radio_group:I = 0x7f0c0085

.field public static final config_icon:I = 0x7f0c00c5

.field public static final config_icon_view:I = 0x7f0c00c4

.field public static final config_name:I = 0x7f0c00c7

.field public static final configuration_switch:I = 0x7f0c00d8

.field public static final connection_switch:I = 0x7f0c00c9

.field public static final content:I = 0x7f0c0063

.field public static final contentHolder:I = 0x7f0c004c

.field public static final content_frame:I = 0x7f0c009d

.field public static final content_parent:I = 0x7f0c009b

.field public static final date:I = 0x7f0c005c

.field public static final deadzone:I = 0x7f0c0054

.field public static final description:I = 0x7f0c00a9

.field public static final diagram:I = 0x7f0c0008

.field public static final do_not_disturb:I = 0x7f0c00bb

.field public static final do_not_disturb_checkbox:I = 0x7f0c00bd

.field public static final do_not_disturb_icon:I = 0x7f0c00bc

.field public static final dot0:I = 0x7f0c0081

.field public static final dot1:I = 0x7f0c0082

.field public static final dot2:I = 0x7f0c0083

.field public static final dot3:I = 0x7f0c0084

.field public static final emergency_calls_only:I = 0x7f0c005b

.field public static final enable_state:I = 0x7f0c00d0

.field public static final expandable_tag:I = 0x7f0c0000

.field public static final explanation:I = 0x7f0c000a

.field public static final fake_space_bar:I = 0x7f0c007d

.field public static final feedbackIconArea:I = 0x7f0c0095

.field public static final flight_mode:I = 0x7f0c0023

.field public static final global_screenshot:I = 0x7f0c004a

.field public static final global_screenshot_background:I = 0x7f0c0049

.field public static final global_screenshot_flash:I = 0x7f0c004b

.field public static final glow:I = 0x7f0c008f

.field public static final glow_pad_view:I = 0x7f0c0076

.field public static final hard_keyboard_section:I = 0x7f0c0090

.field public static final hard_keyboard_switch:I = 0x7f0c0092

.field public static final header:I = 0x7f0c0007

.field public static final home:I = 0x7f0c0050

.field public static final icon:I = 0x7f0c0009

.field public static final icons:I = 0x7f0c002d

.field public static final image:I = 0x7f0c0006

.field public static final imeSwitchButton:I = 0x7f0c0096

.field public static final ime_settings_shortcut:I = 0x7f0c0094

.field public static final indicator:I = 0x7f0c00d1

.field public static final input_method_menu_list:I = 0x7f0c0093

.field public static final item_icon:I = 0x7f0c008a

.field public static final item_radio:I = 0x7f0c0089

.field public static final item_settings_icon:I = 0x7f0c008e

.field public static final item_subtitle:I = 0x7f0c008c

.field public static final item_subtype:I = 0x7f0c0088

.field public static final item_title:I = 0x7f0c008b

.field public static final item_vertical_separator:I = 0x7f0c008d

.field public static final label:I = 0x7f0c0079

.field public static final large_icon:I = 0x7f0c00bf

.field public static final latestItems:I = 0x7f0c0046

.field public static final left_icon:I = 0x7f0c00c0

.field public static final level_percent:I = 0x7f0c0005

.field public static final lights_out:I = 0x7f0c0053

.field public static final listview_footer_padding:I = 0x7f0c00ad

.field public static final menu:I = 0x7f0c0052

.field public static final mobile_combo:I = 0x7f0c0012

.field public static final mobile_combo_gemini:I = 0x7f0c001c

.field public static final mobile_icon:I = 0x7f0c00a0

.field public static final mobile_icon_gemini:I = 0x7f0c00a7

.field public static final mobile_inout:I = 0x7f0c0017

.field public static final mobile_inout_gemini:I = 0x7f0c0021

.field public static final mobile_roaming:I = 0x7f0c0016

.field public static final mobile_roaming_gemini:I = 0x7f0c0020

.field public static final mobile_signal:I = 0x7f0c0013

.field public static final mobile_signal2:I = 0x7f0c0014

.field public static final mobile_signal_gemini:I = 0x7f0c001d

.field public static final mobile_signal_gemini2:I = 0x7f0c001e

.field public static final mobile_slot_indicator:I = 0x7f0c0018

.field public static final mobile_slot_indicator_gemini:I = 0x7f0c0022

.field public static final mobile_text:I = 0x7f0c00a1

.field public static final mobile_text_gemini:I = 0x7f0c00a8

.field public static final mobile_type:I = 0x7f0c0015

.field public static final mobile_type_gemini:I = 0x7f0c001f

.field public static final moreIcon:I = 0x7f0c0030

.field public static final nav_buttons:I = 0x7f0c004e

.field public static final navigationArea:I = 0x7f0c007c

.field public static final network:I = 0x7f0c00b1

.field public static final network_icon:I = 0x7f0c00b2

.field public static final network_label:I = 0x7f0c00b3

.field public static final network_text:I = 0x7f0c0099

.field public static final network_type:I = 0x7f0c0011

.field public static final network_type_combo:I = 0x7f0c0010

.field public static final network_type_combo_gemini:I = 0x7f0c001a

.field public static final network_type_gemini:I = 0x7f0c001b

.field public static final notificationArea:I = 0x7f0c007e

.field public static final notificationIcons:I = 0x7f0c0031

.field public static final notificationTrigger:I = 0x7f0c0098

.field public static final notification_adaptive_wrapper:I = 0x7f0c0057

.field public static final notification_button:I = 0x7f0c00a6

.field public static final notification_icon_area:I = 0x7f0c002e

.field public static final notification_inspect_item:I = 0x7f0c00d9

.field public static final notification_lights_out:I = 0x7f0c002c

.field public static final notification_panel:I = 0x7f0c003c

.field public static final notification_scroller:I = 0x7f0c009e

.field public static final notification_title:I = 0x7f0c0042

.field public static final number:I = 0x7f0c005f

.field public static final on_indicator:I = 0x7f0c00c6

.field public static final padding:I = 0x7f0c0003

.field public static final percentage:I = 0x7f0c0035

.field public static final preview:I = 0x7f0c00aa

.field public static final profile_switch:I = 0x7f0c00c8

.field public static final recent_apps:I = 0x7f0c0051

.field public static final recent_inspect_item:I = 0x7f0c00db

.field public static final recent_item:I = 0x7f0c0066

.field public static final recent_remove_item:I = 0x7f0c00da

.field public static final recents_bg_protect:I = 0x7f0c006f

.field public static final recents_callout_line:I = 0x7f0c006c

.field public static final recents_container:I = 0x7f0c0071

.field public static final recents_linear_layout:I = 0x7f0c0072

.field public static final recents_no_apps:I = 0x7f0c0073

.field public static final recents_no_apps_text:I = 0x7f0c0060

.field public static final recents_root:I = 0x7f0c006d

.field public static final recents_transition_background:I = 0x7f0c006e

.field public static final recents_transition_placeholder_icon:I = 0x7f0c0070

.field public static final right_icon:I = 0x7f0c00c2

.field public static final rot0:I = 0x7f0c004d

.field public static final rot270:I = 0x7f0c0056

.field public static final rot90:I = 0x7f0c0055

.field public static final rotate:I = 0x7f0c00b4

.field public static final rotate_checkbox:I = 0x7f0c00b7

.field public static final rotate_icon:I = 0x7f0c00b5

.field public static final rotate_label:I = 0x7f0c00b6

.field public static final rotate_separator:I = 0x7f0c00b8

.field public static final rotation_lock_button:I = 0x7f0c005d

.field public static final scroll:I = 0x7f0c0045

.field public static final search_bg_protect:I = 0x7f0c0075

.field public static final search_panel_container:I = 0x7f0c0074

.field public static final settings:I = 0x7f0c00be

.field public static final settings_button:I = 0x7f0c005e

.field public static final signal_battery_cluster:I = 0x7f0c0033

.field public static final signal_cluster:I = 0x7f0c0034

.field public static final sim_bg_view:I = 0x7f0c00d2

.field public static final sim_icon:I = 0x7f0c00d3

.field public static final sim_icon_bg:I = 0x7f0c00ca

.field public static final sim_indicator:I = 0x7f0c0038

.field public static final sim_indicator_bg:I = 0x7f0c0024

.field public static final sim_indicator_image:I = 0x7f0c0026

.field public static final sim_indicator_text:I = 0x7f0c0027

.field public static final sim_info:I = 0x7f0c0025

.field public static final sim_name:I = 0x7f0c00d5

.field public static final sim_number:I = 0x7f0c00cf

.field public static final sim_op_name:I = 0x7f0c00ce

.field public static final sim_short_number:I = 0x7f0c00cd

.field public static final sim_state:I = 0x7f0c00d4

.field public static final sim_status:I = 0x7f0c00cb

.field public static final sim_switch:I = 0x7f0c00d6

.field public static final sim_switch_tip_check:I = 0x7f0c002a

.field public static final sim_switch_tip_close:I = 0x7f0c0029

.field public static final sim_switch_tip_content:I = 0x7f0c0028

.field public static final sim_type:I = 0x7f0c00cc

.field public static final simicon:I = 0x7f0c002f

.field public static final slider:I = 0x7f0c0078

.field public static final spacer:I = 0x7f0c000f

.field public static final spacer_gemini:I = 0x7f0c0019

.field public static final statusIcons:I = 0x7f0c0032

.field public static final status_bar:I = 0x7f0c002b

.field public static final status_bar_touch_proxy:I = 0x7f0c00ac

.field public static final subtitle:I = 0x7f0c0004

.field public static final system_bar_notification_panel_bottom_space:I = 0x7f0c009c

.field public static final text:I = 0x7f0c00c1

.field public static final ticker:I = 0x7f0c0039

.field public static final tickerIcon:I = 0x7f0c003a

.field public static final tickerText:I = 0x7f0c003b

.field public static final ticker_expanded:I = 0x7f0c00c3

.field public static final title_area:I = 0x7f0c009f

.field public static final toggle:I = 0x7f0c0077

.field public static final toolBarSwitchPanel:I = 0x7f0c0041

.field public static final tool_bar_view:I = 0x7f0c00d7

.field public static final top_glow:I = 0x7f0c0061

.field public static final use_physical_keyboard_label:I = 0x7f0c0091

.field public static final user_expanded_tag:I = 0x7f0c0001

.field public static final user_lock_tag:I = 0x7f0c0002

.field public static final veto:I = 0x7f0c0062

.field public static final wifi_combo:I = 0x7f0c000c

.field public static final wifi_direction:I = 0x7f0c00a3

.field public static final wifi_icon:I = 0x7f0c00a2

.field public static final wifi_inout:I = 0x7f0c000e

.field public static final wifi_signal:I = 0x7f0c000d

.field public static final wifi_text:I = 0x7f0c00a4

.field public static final wifispacer:I = 0x7f0c0058


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
