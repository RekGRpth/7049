.class public final Lcom/android/systemui/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final battery_low:I = 0x7f030000

.field public static final compat_mode_help:I = 0x7f030001

.field public static final gemini_signal_cluster_view:I = 0x7f030002

.field public static final gemini_sim_indicator:I = 0x7f030003

.field public static final gemini_sim_switch_tip:I = 0x7f030004

.field public static final gemini_status_bar:I = 0x7f030005

.field public static final gemini_status_bar_expanded:I = 0x7f030006

.field public static final gemini_super_status_bar:I = 0x7f030007

.field public static final global_screenshot:I = 0x7f030008

.field public static final intruder_alert:I = 0x7f030009

.field public static final navigation_bar:I = 0x7f03000a

.field public static final notification_adaptive_wrapper:I = 0x7f03000b

.field public static final signal_cluster_view:I = 0x7f03000c

.field public static final status_bar:I = 0x7f03000d

.field public static final status_bar_expanded:I = 0x7f03000e

.field public static final status_bar_expanded_header:I = 0x7f03000f

.field public static final status_bar_icon:I = 0x7f030010

.field public static final status_bar_no_recent_apps:I = 0x7f030011

.field public static final status_bar_notification_row:I = 0x7f030012

.field public static final status_bar_recent_item:I = 0x7f030013

.field public static final status_bar_recent_panel:I = 0x7f030014

.field public static final status_bar_search_panel:I = 0x7f030015

.field public static final status_bar_toggle_slider:I = 0x7f030016

.field public static final super_status_bar:I = 0x7f030017

.field public static final system_bar:I = 0x7f030018

.field public static final system_bar_compat_mode_panel:I = 0x7f030019

.field public static final system_bar_gemini:I = 0x7f03001a

.field public static final system_bar_input_methods_item:I = 0x7f03001b

.field public static final system_bar_input_methods_panel:I = 0x7f03001c

.field public static final system_bar_no_recent_apps:I = 0x7f03001d

.field public static final system_bar_notification_area:I = 0x7f03001e

.field public static final system_bar_notification_area_gemini:I = 0x7f03001f

.field public static final system_bar_notification_panel:I = 0x7f030020

.field public static final system_bar_notification_panel_gemini:I = 0x7f030021

.field public static final system_bar_notification_panel_title:I = 0x7f030022

.field public static final system_bar_notification_panel_title_gemini:I = 0x7f030023

.field public static final system_bar_notification_peek:I = 0x7f030024

.field public static final system_bar_pocket_panel:I = 0x7f030025

.field public static final system_bar_recent_item:I = 0x7f030026

.field public static final system_bar_recent_panel:I = 0x7f030027

.field public static final system_bar_recent_panel_footer:I = 0x7f030028

.field public static final system_bar_settings_view:I = 0x7f030029

.field public static final system_bar_ticker_compat:I = 0x7f03002a

.field public static final system_bar_ticker_panel:I = 0x7f03002b

.field public static final toolbar_configuration_icon_view:I = 0x7f03002c

.field public static final toolbar_configuration_switch_panel:I = 0x7f03002d

.field public static final toolbar_connection_switch_panel:I = 0x7f03002e

.field public static final toolbar_dialog_sim_icon:I = 0x7f03002f

.field public static final toolbar_indicator:I = 0x7f030030

.field public static final toolbar_profile_switch_panel:I = 0x7f030031

.field public static final toolbar_sim_icon_view:I = 0x7f030032

.field public static final toolbar_sim_switch_panel:I = 0x7f030033

.field public static final toolbar_view:I = 0x7f030034


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
