.class public final Lcom/android/systemui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accessibility_airplane_mode:I = 0x7f09008d

.field public static final accessibility_back:I = 0x7f090062

.field public static final accessibility_battery_full:I = 0x7f09006f

.field public static final accessibility_battery_level:I = 0x7f09008e

.field public static final accessibility_battery_one_bar:I = 0x7f09006c

.field public static final accessibility_battery_three_bars:I = 0x7f09006e

.field public static final accessibility_battery_two_bars:I = 0x7f09006d

.field public static final accessibility_bluetooth_connected:I = 0x7f090069

.field public static final accessibility_bluetooth_disconnected:I = 0x7f09006a

.field public static final accessibility_bluetooth_tether:I = 0x7f09008c

.field public static final accessibility_clear_all:I = 0x7f09009c

.field public static final accessibility_compatibility_zoom_button:I = 0x7f090067

.field public static final accessibility_compatibility_zoom_example:I = 0x7f090068

.field public static final accessibility_data_connection_3_5g:I = 0x7f090086

.field public static final accessibility_data_connection_3g:I = 0x7f090085

.field public static final accessibility_data_connection_4g:I = 0x7f090087

.field public static final accessibility_data_connection_cdma:I = 0x7f090088

.field public static final accessibility_data_connection_edge:I = 0x7f090089

.field public static final accessibility_data_connection_gprs:I = 0x7f090084

.field public static final accessibility_data_connection_wifi:I = 0x7f09008a

.field public static final accessibility_data_one_bar:I = 0x7f090076

.field public static final accessibility_data_signal_full:I = 0x7f090079

.field public static final accessibility_data_three_bars:I = 0x7f090078

.field public static final accessibility_data_two_bars:I = 0x7f090077

.field public static final accessibility_gps_acquiring:I = 0x7f090093

.field public static final accessibility_gps_enabled:I = 0x7f090092

.field public static final accessibility_home:I = 0x7f090063

.field public static final accessibility_ime_switch_button:I = 0x7f090066

.field public static final accessibility_menu:I = 0x7f090064

.field public static final accessibility_no_battery:I = 0x7f09006b

.field public static final accessibility_no_data:I = 0x7f090075

.field public static final accessibility_no_phone:I = 0x7f090070

.field public static final accessibility_no_sim:I = 0x7f09008b

.field public static final accessibility_no_wifi:I = 0x7f09007a

.field public static final accessibility_no_wimax:I = 0x7f09007f

.field public static final accessibility_notifications_button:I = 0x7f090090

.field public static final accessibility_phone_four_bars:I = 0x7f090004

.field public static final accessibility_phone_one_bar:I = 0x7f090071

.field public static final accessibility_phone_signal_full:I = 0x7f090074

.field public static final accessibility_phone_three_bars:I = 0x7f090073

.field public static final accessibility_phone_two_bars:I = 0x7f090072

.field public static final accessibility_recent:I = 0x7f090065

.field public static final accessibility_recents_item_dismissed:I = 0x7f090097

.field public static final accessibility_remove_notification:I = 0x7f090091

.field public static final accessibility_ringer_silent:I = 0x7f090096

.field public static final accessibility_ringer_vibrate:I = 0x7f090095

.field public static final accessibility_rotation_lock_off:I = 0x7f0900a1

.field public static final accessibility_rotation_lock_on_landscape:I = 0x7f0900a2

.field public static final accessibility_rotation_lock_on_portrait:I = 0x7f0900a3

.field public static final accessibility_settings_button:I = 0x7f09008f

.field public static final accessibility_tty_enabled:I = 0x7f090094

.field public static final accessibility_wifi_one_bar:I = 0x7f09007b

.field public static final accessibility_wifi_signal_full:I = 0x7f09007e

.field public static final accessibility_wifi_three_bars:I = 0x7f09007d

.field public static final accessibility_wifi_two_bars:I = 0x7f09007c

.field public static final accessibility_wimax_one_bar:I = 0x7f090080

.field public static final accessibility_wimax_signal_full:I = 0x7f090083

.field public static final accessibility_wimax_three_bars:I = 0x7f090082

.field public static final accessibility_wimax_two_bars:I = 0x7f090081

.field public static final always_use_accessory:I = 0x7f090059

.field public static final always_use_device:I = 0x7f090058

.field public static final app_label:I = 0x7f09003c

.field public static final autorotate:I = 0x7f09000e

.field public static final battery_low_percent_format:I = 0x7f09002a

.field public static final battery_low_subtitle:I = 0x7f090028

.field public static final battery_low_title:I = 0x7f090027

.field public static final battery_low_why:I = 0x7f090029

.field public static final bluetooth:I = 0x7f09000b

.field public static final bluetooth_tethered:I = 0x7f09004e

.field public static final brightness:I = 0x7f090014

.field public static final builtin_installer_always_allowed:I = 0x7f090024

.field public static final builtin_installer_dlg_message:I = 0x7f090023

.field public static final builtin_installer_title_message:I = 0x7f090022

.field public static final compat_mode_help_body:I = 0x7f09005d

.field public static final compat_mode_help_header:I = 0x7f09005c

.field public static final compat_mode_off:I = 0x7f09005b

.field public static final compat_mode_on:I = 0x7f09005a

.field public static final config_statusBarComponent:I = 0x7f090000

.field public static final config_systemBarComponent:I = 0x7f090001

.field public static final confirm_radio_lbutton:I = 0x7f090021

.field public static final confirm_radio_msg:I = 0x7f09001f

.field public static final confirm_radio_msg_single:I = 0x7f090020

.field public static final confirm_radio_title:I = 0x7f09001e

.field public static final data_connection_attach_timeout_error_msg:I = 0x7f090016

.field public static final data_connection_connected:I = 0x7f090018

.field public static final data_connection_datach_timeout_error_msg:I = 0x7f09003b

.field public static final data_connection_dettach_timeout_error_msg:I = 0x7f090017

.field public static final data_connection_service:I = 0x7f090009

.field public static final data_usage_disabled_dialog:I = 0x7f09002f

.field public static final data_usage_disabled_dialog_3g_title:I = 0x7f09002b

.field public static final data_usage_disabled_dialog_4g_title:I = 0x7f09002c

.field public static final data_usage_disabled_dialog_enable:I = 0x7f090030

.field public static final data_usage_disabled_dialog_mobile_title:I = 0x7f09002d

.field public static final data_usage_disabled_dialog_title:I = 0x7f09002e

.field public static final dlg_no:I = 0x7f090025

.field public static final dlg_yes:I = 0x7f090026

.field public static final dreams_dock_launcher:I = 0x7f09009d

.field public static final gemini_3g_disable_warning:I = 0x7f09001c

.field public static final gemini_3g_disable_warning_cu:I = 0x7f09001d

.field public static final gemini_3g_indic:I = 0x7f09001b

.field public static final gemini_default_sim_always_ask:I = 0x7f090003

.field public static final gemini_default_sim_never:I = 0x7f09001a

.field public static final gemini_intenet_call:I = 0x7f090019

.field public static final gps:I = 0x7f09000d

.field public static final gps_notification_found_text:I = 0x7f09009b

.field public static final gps_notification_searching_text:I = 0x7f09009a

.field public static final installer_cd_button_title:I = 0x7f090061

.field public static final invalid_charger:I = 0x7f090044

.field public static final label_view:I = 0x7f090057

.field public static final meeting:I = 0x7f090010

.field public static final messages_service:I = 0x7f090008

.field public static final mobile:I = 0x7f09000c

.field public static final mute:I = 0x7f090011

.field public static final normal:I = 0x7f09000f

.field public static final notifications_off_text:I = 0x7f0900a0

.field public static final notifications_off_title:I = 0x7f09009f

.field public static final offline:I = 0x7f090013

.field public static final outdoor:I = 0x7f090012

.field public static final remind_next_time:I = 0x7f090006

.field public static final screenshot_failed_text:I = 0x7f09003a

.field public static final screenshot_failed_title:I = 0x7f090039

.field public static final screenshot_saved_text:I = 0x7f090038

.field public static final screenshot_saved_title:I = 0x7f090037

.field public static final screenshot_saving_text:I = 0x7f090036

.field public static final screenshot_saving_ticker:I = 0x7f090034

.field public static final screenshot_saving_title:I = 0x7f090035

.field public static final sim_switch_tip:I = 0x7f090005

.field public static final status_bar_accessibility_dismiss_recents:I = 0x7f090041

.field public static final status_bar_clear_all_button:I = 0x7f090031

.field public static final status_bar_date_formatter:I = 0x7f090002

.field public static final status_bar_do_not_disturb_button:I = 0x7f09003d

.field public static final status_bar_input_method_settings_configure_input_methods:I = 0x7f09004f

.field public static final status_bar_latest_events_title:I = 0x7f090032

.field public static final status_bar_network_name_separator:I = 0x7f09004d

.field public static final status_bar_no_notifications_title:I = 0x7f090042

.field public static final status_bar_no_recent_apps:I = 0x7f090033

.field public static final status_bar_notification_inspect_item_title:I = 0x7f09009e

.field public static final status_bar_ongoing_events_title:I = 0x7f090043

.field public static final status_bar_please_disturb_button:I = 0x7f09003e

.field public static final status_bar_recent_inspect_item_title:I = 0x7f090040

.field public static final status_bar_recent_remove_item_title:I = 0x7f09003f

.field public static final status_bar_settings_airplane:I = 0x7f090047

.field public static final status_bar_settings_auto_brightness_label:I = 0x7f09004a

.field public static final status_bar_settings_auto_rotation:I = 0x7f090048

.field public static final status_bar_settings_battery_meter_format:I = 0x7f09004c

.field public static final status_bar_settings_mute_label:I = 0x7f090049

.field public static final status_bar_settings_notifications:I = 0x7f09004b

.field public static final status_bar_settings_settings_button:I = 0x7f090045

.field public static final status_bar_settings_signal_meter_disconnected:I = 0x7f090098

.field public static final status_bar_settings_signal_meter_wifi_nossid:I = 0x7f090099

.field public static final status_bar_settings_wifi_button:I = 0x7f090046

.field public static final status_bar_use_physical_keyboard:I = 0x7f090050

.field public static final timeout:I = 0x7f090015

.field public static final title_usb_accessory:I = 0x7f090056

.field public static final toast_rotation_locked:I = 0x7f0900a4

.field public static final usb_accessory_confirm_prompt:I = 0x7f090054

.field public static final usb_accessory_permission_prompt:I = 0x7f090052

.field public static final usb_accessory_uri_prompt:I = 0x7f090055

.field public static final usb_device_confirm_prompt:I = 0x7f090053

.field public static final usb_device_permission_prompt:I = 0x7f090051

.field public static final usb_preference_title:I = 0x7f09005e

.field public static final use_mtp_button_title:I = 0x7f09005f

.field public static final use_ptp_button_title:I = 0x7f090060

.field public static final voice_call_service:I = 0x7f090007

.field public static final wifi:I = 0x7f09000a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
