.class Lcom/android/systemui/recent/Choreographer;
.super Ljava/lang/Object;
.source "Choreographer.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# static fields
.field private static final CLOSE_DURATION:I = 0xe6

.field private static final DEBUG:Z = false

.field private static final OPEN_DURATION:I = 0x88

.field private static final SCRIM_DURATION:I = 0x190

.field private static final TAG:Ljava/lang/String; = "RecentsPanelView"


# instance fields
.field final HYPERSPACE_OFFRAMP:I

.field mContentAnim:Landroid/animation/AnimatorSet;

.field mContentView:Landroid/view/View;

.field mListener:Landroid/animation/Animator$AnimatorListener;

.field mNoRecentAppsView:Landroid/view/View;

.field mPanelHeight:I

.field mRootView:Lcom/android/systemui/recent/RecentsPanelView;

.field mScrimView:Landroid/view/View;

.field mVisible:Z


# direct methods
.method public constructor <init>(Lcom/android/systemui/recent/RecentsPanelView;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/animation/Animator$AnimatorListener;)V
    .locals 1
    .param p1    # Lcom/android/systemui/recent/RecentsPanelView;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/View;
    .param p4    # Landroid/view/View;
    .param p5    # Landroid/animation/Animator$AnimatorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xc8

    iput v0, p0, Lcom/android/systemui/recent/Choreographer;->HYPERSPACE_OFFRAMP:I

    iput-object p1, p0, Lcom/android/systemui/recent/Choreographer;->mRootView:Lcom/android/systemui/recent/RecentsPanelView;

    iput-object p2, p0, Lcom/android/systemui/recent/Choreographer;->mScrimView:Landroid/view/View;

    iput-object p3, p0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    iput-object p5, p0, Lcom/android/systemui/recent/Choreographer;->mListener:Landroid/animation/Animator$AnimatorListener;

    iput-object p4, p0, Lcom/android/systemui/recent/Choreographer;->mNoRecentAppsView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method createAnimation(Z)V
    .locals 19
    .param p1    # Z

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getTranslationY()F

    move-result v13

    if-eqz p1, :cond_5

    const/high16 v14, 0x43480000

    cmpg-float v14, v13, v14

    if-gez v14, :cond_4

    move v12, v13

    :goto_0
    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    const-string v15, "translationY"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput v12, v16, v17

    const/16 v17, 0x1

    aput v5, v16, v17

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    if-eqz p1, :cond_6

    new-instance v14, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v15, 0x40200000

    invoke-direct {v14, v15}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    :goto_2
    invoke-virtual {v9, v14}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    if-eqz p1, :cond_7

    const-wide/16 v14, 0x88

    :goto_3
    invoke-virtual {v9, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/Animator;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    const-string v16, "alpha"

    const/4 v14, 0x2

    new-array v0, v14, [F

    move-object/from16 v17, v0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getAlpha()F

    move-result v18

    aput v18, v17, v14

    const/16 v18, 0x1

    if-eqz p1, :cond_8

    const/high16 v14, 0x3f800000

    :goto_4
    aput v14, v17, v18

    invoke-static/range {v15 .. v17}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    if-eqz p1, :cond_9

    new-instance v14, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v15, 0x3f800000

    invoke-direct {v14, v15}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    :goto_5
    invoke-virtual {v6, v14}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    if-eqz p1, :cond_a

    const-wide/16 v14, 0x88

    :goto_6
    invoke-virtual {v6, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/Animator;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mNoRecentAppsView:Landroid/view/View;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mNoRecentAppsView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/recent/Choreographer;->mNoRecentAppsView:Landroid/view/View;

    const-string v16, "alpha"

    const/4 v14, 0x2

    new-array v0, v14, [F

    move-object/from16 v17, v0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getAlpha()F

    move-result v18

    aput v18, v17, v14

    const/16 v18, 0x1

    if-eqz p1, :cond_b

    const/high16 v14, 0x3f800000

    :goto_7
    aput v14, v17, v18

    invoke-static/range {v15 .. v17}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    if-eqz p1, :cond_c

    new-instance v14, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v15, 0x3f800000

    invoke-direct {v14, v15}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    :goto_8
    invoke-virtual {v8, v14}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    if-eqz p1, :cond_d

    const-wide/16 v14, 0x88

    :goto_9
    invoke-virtual {v8, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/Animator;

    :cond_0
    new-instance v14, Landroid/animation/AnimatorSet;

    invoke-direct {v14}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mContentAnim:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mContentAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v14, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v14

    invoke-virtual {v14, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    if-eqz v8, :cond_1

    invoke-virtual {v4, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_1
    if-eqz p1, :cond_11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mScrimView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v15, "alpha"

    const/4 v14, 0x2

    new-array v0, v14, [I

    move-object/from16 v16, v0

    const/16 v17, 0x0

    if-eqz p1, :cond_e

    const/4 v14, 0x0

    :goto_a
    aput v14, v16, v17

    const/16 v17, 0x1

    if-eqz p1, :cond_f

    const/16 v14, 0xff

    :goto_b
    aput v14, v16, v17

    move-object/from16 v0, v16

    invoke-static {v1, v15, v0}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    if-eqz p1, :cond_10

    const-wide/16 v14, 0x190

    :goto_c
    invoke-virtual {v2, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/Animator;

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_2
    :goto_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mContentAnim:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mListener:Landroid/animation/Animator$AnimatorListener;

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mContentAnim:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/recent/Choreographer;->mListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v14, v15}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_3
    return-void

    :cond_4
    const/high16 v12, 0x43480000

    goto/16 :goto_0

    :cond_5
    move v12, v13

    move v5, v13

    goto/16 :goto_1

    :cond_6
    new-instance v14, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v15, 0x40200000

    invoke-direct {v14, v15}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    goto/16 :goto_2

    :cond_7
    const-wide/16 v14, 0xe6

    goto/16 :goto_3

    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_4

    :cond_9
    new-instance v14, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v15, 0x40200000

    invoke-direct {v14, v15}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    goto/16 :goto_5

    :cond_a
    const-wide/16 v14, 0xe6

    goto/16 :goto_6

    :cond_b
    const/4 v14, 0x0

    goto/16 :goto_7

    :cond_c
    new-instance v14, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v15, 0x3f800000

    invoke-direct {v14, v15}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    goto/16 :goto_8

    :cond_d
    const-wide/16 v14, 0xe6

    goto/16 :goto_9

    :cond_e
    const/16 v14, 0xff

    goto :goto_a

    :cond_f
    const/4 v14, 0x0

    goto :goto_b

    :cond_10
    const-wide/16 v14, 0xe6

    goto :goto_c

    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mRootView:Lcom/android/systemui/recent/RecentsPanelView;

    invoke-virtual {v14}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const/high16 v14, 0x7f080000

    invoke-virtual {v11, v14}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    if-nez v7, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recent/Choreographer;->mRootView:Lcom/android/systemui/recent/RecentsPanelView;

    const v15, 0x7f0c006e

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const/4 v14, 0x0

    invoke-virtual {v10, v14}, Landroid/view/View;->setVisibility(I)V

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v14, -0x1000000

    invoke-direct {v3, v14}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v10, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v14, "alpha"

    const/4 v15, 0x2

    new-array v15, v15, [I

    fill-array-data v15, :array_0

    invoke-static {v3, v14, v15}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v14, 0xe6

    invoke-virtual {v2, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/Animator;

    new-instance v14, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v15, 0x3f800000

    invoke-direct {v14, v15}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v2, v14}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_d

    nop

    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method jumpTo(Z)V
    .locals 3
    .param p1    # Z

    iget-object v2, p0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationY(F)V

    iget-object v1, p0, Lcom/android/systemui/recent/Choreographer;->mScrimView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/recent/Choreographer;->mScrimView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz p1, :cond_2

    const/16 v1, 0xff

    :goto_1
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/recent/Choreographer;->mRootView:Lcom/android/systemui/recent/RecentsPanelView;

    const v2, 0x7f0c006e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/systemui/recent/Choreographer;->mRootView:Lcom/android/systemui/recent/RecentsPanelView;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_1
    iget v1, p0, Lcom/android/systemui/recent/Choreographer;->mPanelHeight:I

    int-to-float v1, v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/recent/Choreographer;->mVisible:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1    # Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/systemui/recent/Choreographer;->mVisible:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/recent/Choreographer;->mRootView:Lcom/android/systemui/recent/RecentsPanelView;

    invoke-virtual {v0}, Lcom/android/systemui/recent/RecentsPanelView;->hideWindow()V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iput-object v2, p0, Lcom/android/systemui/recent/Choreographer;->mContentAnim:Landroid/animation/AnimatorSet;

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public setPanelHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/systemui/recent/Choreographer;->mPanelHeight:I

    return-void
.end method

.method startAnimation(Z)V
    .locals 3
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/android/systemui/recent/Choreographer;->createAnimation(Z)V

    iget-object v0, p0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/systemui/recent/Choreographer;->mContentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->buildLayer()V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/recent/Choreographer;->mContentAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    iput-boolean p1, p0, Lcom/android/systemui/recent/Choreographer;->mVisible:Z

    return-void
.end method
