.class Lcom/android/systemui/statusbar/SIMIndicator$1;
.super Landroid/content/BroadcastReceiver;
.source "SIMIndicator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/SIMIndicator;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/SIMIndicator;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/SIMIndicator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/SIMIndicator$1;->this$0:Lcom/android/systemui/statusbar/SIMIndicator;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-wide/16 v10, -0x1

    const/4 v9, -0x1

    iget-object v6, p0, Lcom/android/systemui/statusbar/SIMIndicator$1;->this$0:Lcom/android/systemui/statusbar/SIMIndicator;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/SIMIndicator;->isShowing()Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "SIMIndicator"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive, intent action is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "android.intent.action.VOICE_CALL_DEFAULT_SIM"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "android.intent.action.SMS_DEFAULT_SIM"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_2
    const-string v6, "simid"

    invoke-virtual {p2, v6, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v6, "android.intent.action.VOICE_CALL_DEFAULT_SIM"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-wide/16 v6, -0x2

    cmp-long v6, v1, v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/android/systemui/statusbar/SIMIndicator$1;->this$0:Lcom/android/systemui/statusbar/SIMIndicator;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/SIMIndicator;->setSIPInfo()Z

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/systemui/statusbar/SIMIndicator$1;->this$0:Lcom/android/systemui/statusbar/SIMIndicator;

    invoke-virtual {v6, v1, v2}, Lcom/android/systemui/statusbar/SIMIndicator;->setSIMInfo(J)Z

    goto :goto_0

    :cond_4
    const-string v6, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "slotId"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iget-object v6, p0, Lcom/android/systemui/statusbar/SIMIndicator$1;->this$0:Lcom/android/systemui/statusbar/SIMIndicator;

    invoke-static {v6}, Lcom/android/systemui/statusbar/SIMIndicator;->access$000(Lcom/android/systemui/statusbar/SIMIndicator;)Landroid/provider/Telephony$SIMInfo;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/SIMIndicator$1;->this$0:Lcom/android/systemui/statusbar/SIMIndicator;

    invoke-static {v6}, Lcom/android/systemui/statusbar/SIMIndicator;->access$000(Lcom/android/systemui/statusbar/SIMIndicator;)Landroid/provider/Telephony$SIMInfo;

    move-result-object v6

    iget v6, v6, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne v6, v4, :cond_0

    const-string v6, "state"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v9, :cond_0

    const-string v6, "SIMIndicator"

    const-string v7, "updateSIMState."

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/systemui/statusbar/SIMIndicator$1;->this$0:Lcom/android/systemui/statusbar/SIMIndicator;

    invoke-static {v3}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMStateIcon(I)I

    move-result v7

    invoke-static {v6, v7}, Lcom/android/systemui/statusbar/SIMIndicator;->access$100(Lcom/android/systemui/statusbar/SIMIndicator;I)V

    goto/16 :goto_0

    :cond_5
    const-string v6, "android.intent.action.SIM_SETTING_INFO_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {p1}, Lcom/android/systemui/statusbar/util/SIMHelper;->updateSIMInfos(Landroid/content/Context;)V

    const-string v6, "type"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v6, "simid"

    invoke-virtual {p2, v6, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/SIMIndicator$1;->this$0:Lcom/android/systemui/statusbar/SIMIndicator;

    invoke-virtual {v6, v1, v2}, Lcom/android/systemui/statusbar/SIMIndicator;->setSIMInfo(J)Z

    goto/16 :goto_0
.end method
