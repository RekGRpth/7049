.class public final Lcom/android/systemui/statusbar/SIMIndicator;
.super Landroid/widget/RelativeLayout;
.source "SIMIndicator.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SIMIndicator"


# instance fields
.field private mBgView:Landroid/widget/ImageView;

.field private mCurrentSIMInfo:Landroid/provider/Telephony$SIMInfo;

.field private mMaxWidth:I

.field private mMinWidth:I

.field private mShowing:Z

.field private mSimInfoLayout:Landroid/view/ViewGroup;

.field private mStateView:Landroid/widget/ImageView;

.field private mStateViewShouldShow:Z

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/SIMIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateViewShouldShow:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SIMIndicator;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/systemui/statusbar/SIMIndicator;)Landroid/provider/Telephony$SIMInfo;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/SIMIndicator;

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mCurrentSIMInfo:Landroid/provider/Telephony$SIMInfo;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/systemui/statusbar/SIMIndicator;I)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/SIMIndicator;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/SIMIndicator;->updateStateView(I)V

    return-void
.end method

.method private init()V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mMinWidth:I

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0042

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mMaxWidth:I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.VOICE_CALL_DEFAULT_SIM"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SMS_DEFAULT_SIM"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SIM_SETTING_INFO_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/systemui/statusbar/SIMIndicator$1;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/SIMIndicator$1;-><init>(Lcom/android/systemui/statusbar/SIMIndicator;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private updateStateView(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mSimInfoLayout:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mSimInfoLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    add-int v2, v3, v4

    iget v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mMinWidth:I

    sub-int v1, v3, v2

    iget v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mMaxWidth:I

    sub-int v0, v3, v2

    if-lez p1, :cond_0

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateViewShouldShow:Z

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int/2addr v1, v3

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int/2addr v0, v3

    :goto_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setMinWidth(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    return-void

    :cond_0
    iput-boolean v5, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateViewShouldShow:Z

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public clearSIMInfo()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "SIMIndicator"

    const-string v1, "clearSIMInfo."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mBgView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mTextView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mCurrentSIMInfo:Landroid/provider/Telephony$SIMInfo;

    return-void
.end method

.method public getCurrentSIMInfo()Landroid/provider/Telephony$SIMInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mCurrentSIMInfo:Landroid/provider/Telephony$SIMInfo;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mShowing:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0c0024

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mBgView:Landroid/widget/ImageView;

    const v0, 0x7f0c0025

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mSimInfoLayout:Landroid/view/ViewGroup;

    const v0, 0x7f0c0026

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    const v0, 0x7f0c0027

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mTextView:Landroid/widget/TextView;

    return-void
.end method

.method public setSIMInfo(Landroid/provider/Telephony$SIMInfo;)V
    .locals 3
    .param p1    # Landroid/provider/Telephony$SIMInfo;

    const-string v0, "SIMIndicator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSIMInfo, mDisplayName is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mBgView:Landroid/widget/ImageView;

    iget v1, p1, Landroid/provider/Telephony$SIMInfo;->mSimBackgroundRes:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {p1}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMStateIcon(Landroid/provider/Telephony$SIMInfo;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/SIMIndicator;->updateStateView(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mTextView:Landroid/widget/TextView;

    iget-object v1, p1, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mCurrentSIMInfo:Landroid/provider/Telephony$SIMInfo;

    return-void
.end method

.method public setSIMInfo(J)Z
    .locals 5
    .param p1    # J

    const/4 v1, 0x0

    const-string v2, "SIMIndicator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSIMInfo, simId is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gtz v2, :cond_0

    const-string v2, "SIMIndicator"

    const-string v3, "setSIMInfo error, the simId is <= 0."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, p2}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfo(Landroid/content/Context;J)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v2, "SIMIndicator"

    const-string v3, "setSIMInfo error, the simInfo is null."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/SIMIndicator;->setSIMInfo(Landroid/provider/Telephony$SIMInfo;)V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setSIPInfo()Z
    .locals 3

    const-string v0, "SIMIndicator"

    const-string v1, "setSIPInfo."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mBgView:Landroid/widget/ImageView;

    const v1, 0x20200d3

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/SIMIndicator;->updateStateView(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mCurrentSIMInfo:Landroid/provider/Telephony$SIMInfo;

    const/4 v0, 0x1

    return v0
.end method

.method public setShowing(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mShowing:Z

    return-void
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1    # I

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mBgView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateViewShouldShow:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMIndicator;->mStateView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method
