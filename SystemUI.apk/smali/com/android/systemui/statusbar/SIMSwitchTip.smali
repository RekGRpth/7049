.class public Lcom/android/systemui/statusbar/SIMSwitchTip;
.super Ljava/lang/Object;
.source "SIMSwitchTip.java"


# instance fields
.field private DATA_CONNECTION_SERVICE:Ljava/lang/String;

.field private MESSAGES_SERVICE:Ljava/lang/String;

.field private REMIND_NEXT_TIME:Ljava/lang/String;

.field private SIM_SWITCH_TIP_SETTING:Ljava/lang/String;

.field private VOICE_CALL_SERVICE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mRemindCheckBox:Landroid/widget/CheckBox;

.field private mShowing:Z

.field private mStatusBarHeight:I

.field private mSwitchTipView:Landroid/view/View;

.field private mTipContentView:Landroid/widget/TextView;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "settings"

    iput-object v0, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->SIM_SWITCH_TIP_SETTING:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mShowing:Z

    iput-object p1, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/SIMSwitchTip;->init()V

    return-void
.end method

.method private init()V
    .locals 5

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->VOICE_CALL_SERVICE:Ljava/lang/String;

    const v2, 0x7f090008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->MESSAGES_SERVICE:Ljava/lang/String;

    const v2, 0x7f090009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->DATA_CONNECTION_SERVICE:Ljava/lang/String;

    const v2, 0x7f090006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->REMIND_NEXT_TIME:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mWindowManager:Landroid/view/WindowManager;

    const v2, 0x105000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mStatusBarHeight:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030004

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mSwitchTipView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mSwitchTipView:Landroid/view/View;

    const v3, 0x7f0c0028

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mTipContentView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mSwitchTipView:Landroid/view/View;

    const v3, 0x7f0c002a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mRemindCheckBox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mRemindCheckBox:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->REMIND_NEXT_TIME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mSwitchTipView:Landroid/view/View;

    const v3, 0x7f0c0029

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Lcom/android/systemui/statusbar/SIMSwitchTip$1;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/SIMSwitchTip$1;-><init>(Lcom/android/systemui/statusbar/SIMSwitchTip;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setTipContent(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const-string v0, ""

    const-string v1, "voice_call_sim_setting"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->VOICE_CALL_SERVICE:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mTipContentView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090005

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mRemindCheckBox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->REMIND_NEXT_TIME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    const-string v1, "sms_sim_setting"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->MESSAGES_SERVICE:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "gprs_connection_sim_setting"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->DATA_CONNECTION_SERVICE:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public hide()V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mShowing:Z

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->SIM_SWITCH_TIP_SETTING:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "remind_me_next_time"

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mRemindCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "1"

    :goto_0
    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mSwitchTipView:Landroid/view/View;

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v5, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mShowing:Z

    :cond_0
    return-void

    :cond_1
    :try_start_1
    const-string v2, "0"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    iput-boolean v5, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mShowing:Z

    throw v2
.end method

.method public isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mShowing:Z

    return v0
.end method

.method public show(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v6, -0x2

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mShowing:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->SIM_SWITCH_TIP_SETTING:Ljava/lang/String;

    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v4, "remind_me_next_time"

    const-string v5, "1"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iget-object v4, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mRemindCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/SIMSwitchTip;->setTipContent(Ljava/lang/String;)V

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v4, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mStatusBarHeight:I

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/16 v4, 0x33

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v4, -0x3

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    const/16 v4, 0x7d3

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v4, 0x308

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v4, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mWindowManager:Landroid/view/WindowManager;

    iget-object v5, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mSwitchTipView:Landroid/view/View;

    invoke-interface {v4, v5, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mShowing:Z

    :cond_0
    return-void
.end method

.method public updateResources()V
    .locals 4

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->VOICE_CALL_SERVICE:Ljava/lang/String;

    const v2, 0x7f090008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->MESSAGES_SERVICE:Ljava/lang/String;

    const v2, 0x7f090009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->DATA_CONNECTION_SERVICE:Ljava/lang/String;

    const v2, 0x7f090006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->REMIND_NEXT_TIME:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/systemui/statusbar/SIMSwitchTip;->mSwitchTipView:Landroid/view/View;

    const v3, 0x7f0c0029

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f02001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
