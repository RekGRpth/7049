.class Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;
.super Landroid/telephony/PhoneStateListener;
.source "NetworkControllerGemini.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "NetworkControllerGemini"

    const-string v1, "PhoneStateListener:onCallStateChanged, sim2 before."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "NetworkControllerGemini"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhoneStateListener:onCallStateChanged, state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$600(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$100(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$200(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->refreshViews(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$500(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v4}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$100(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v4}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$500(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-virtual {v0, v4}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->refreshViews(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->refreshViews(I)V

    const-string v0, "NetworkControllerGemini"

    const-string v1, "PhoneStateListener:onCallStateChanged, sim2 after."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDataActivity(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const-string v0, "NetworkControllerGemini"

    const-string v1, "PhoneStateListener:onDataActivity, sim2 before."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "NetworkControllerGemini"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhoneStateListener:onDataActivity, direction="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$1502(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$500(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->refreshViews(I)V

    const-string v0, "NetworkControllerGemini"

    const-string v1, "PhoneStateListener:onDataActivity, sim2 after."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x1

    const-string v0, "NetworkControllerGemini"

    const-string v1, "PhoneStateListener:onDataConnectionStateChanged, sim2 before."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "NetworkControllerGemini"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhoneStateListener:onDataConnectionStateChanged, state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$1402(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, p2}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$1302(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$100(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$500(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->refreshViews(I)V

    const-string v0, "NetworkControllerGemini"

    const-string v1, "PhoneStateListener:onDataConnectionStateChanged, sim2 after."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 5
    .param p1    # Landroid/telephony/ServiceState;

    const/4 v4, 0x1

    const-string v1, "NetworkControllerGemini"

    const-string v2, "PhoneStateListener:onServiceStateChanged, sim2 before."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "NetworkControllerGemini"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhoneStateListener:onServiceStateChanged, state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v1, p1}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$1202(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-virtual {v0, v4}, Landroid/telephony/TelephonyManager;->getNetworkTypeGemini(I)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$1302(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)I

    const-string v1, "NetworkControllerGemini"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhoneStateListener:onServiceStateChanged sim2 mDataNetTypeGemini= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$1300(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v1, v4}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$100(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v1, v4}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$200(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v1, v4}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$500(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-virtual {v1, v4}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->refreshViews(I)V

    const-string v1, "NetworkControllerGemini"

    const-string v2, "PhoneStateListener:onServiceStateChanged, sim2 after."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 4
    .param p1    # Landroid/telephony/SignalStrength;

    const/4 v3, 0x1

    const-string v0, "NetworkControllerGemini"

    const-string v1, "PhoneStateListener:onSignalStrengthsChanged, sim2 before."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "NetworkControllerGemini"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhoneStateListener:onSignalStrengthsChanged, signalStrength="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLevel()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$1102(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;Landroid/telephony/SignalStrength;)Landroid/telephony/SignalStrength;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$100(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-static {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->access$200(Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini$2;->this$0:Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/policy/NetworkControllerGemini;->refreshViews(I)V

    const-string v0, "NetworkControllerGemini"

    const-string v1, "PhoneStateListener:onSignalStrengthsChanged, sim2 after."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
