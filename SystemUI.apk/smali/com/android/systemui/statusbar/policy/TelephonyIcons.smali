.class Lcom/android/systemui/statusbar/policy/TelephonyIcons;
.super Ljava/lang/Object;
.source "TelephonyIcons.java"


# static fields
.field static final DATA_1X:[[I

.field static final DATA_3G:[[I

.field static final DATA_4G:[[I

.field static final DATA_E:[[I

.field static final DATA_G:[[I

.field static final DATA_H:[[I

.field static final DATA_SIGNAL_STRENGTH:[[I

.field static final TELEPHONY_SIGNAL_STRENGTH:[[I

.field static final TELEPHONY_SIGNAL_STRENGTH_ROAMING:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    new-array v0, v5, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v6, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    new-array v0, v5, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v6, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH_ROAMING:[[I

    sget-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_SIGNAL_STRENGTH:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_5

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_G:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_6

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_7

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_3G:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_8

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_9

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_E:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_a

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_b

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_H:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_c

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_d

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_1X:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_e

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_f

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_4G:[[I

    return-void

    :array_0
    .array-data 4
        0x7f0200eb
        0x7f0200ed
        0x7f0200ef
        0x7f0200f1
        0x7f0200f3
    .end array-data

    :array_1
    .array-data 4
        0x7f0200ec
        0x7f0200ee
        0x7f0200f0
        0x7f0200f2
        0x7f0200f4
    .end array-data

    :array_2
    .array-data 4
        0x7f0200eb
        0x7f0200ed
        0x7f0200ef
        0x7f0200f1
        0x7f0200f3
    .end array-data

    :array_3
    .array-data 4
        0x7f0200ec
        0x7f0200ee
        0x7f0200f0
        0x7f0200f2
        0x7f0200f4
    .end array-data

    :array_4
    .array-data 4
        0x7f020084
        0x7f020084
        0x7f020084
        0x7f020084
    .end array-data

    :array_5
    .array-data 4
        0x7f02008b
        0x7f02008b
        0x7f02008b
        0x7f02008b
    .end array-data

    :array_6
    .array-data 4
        0x7f020081
        0x7f020081
        0x7f020081
        0x7f020081
    .end array-data

    :array_7
    .array-data 4
        0x7f020088
        0x7f020088
        0x7f020088
        0x7f020088
    .end array-data

    :array_8
    .array-data 4
        0x7f020083
        0x7f020083
        0x7f020083
        0x7f020083
    .end array-data

    :array_9
    .array-data 4
        0x7f02008a
        0x7f02008a
        0x7f02008a
        0x7f02008a
    .end array-data

    :array_a
    .array-data 4
        0x7f020085
        0x7f020085
        0x7f020085
        0x7f020085
    .end array-data

    :array_b
    .array-data 4
        0x7f02008c
        0x7f02008c
        0x7f02008c
        0x7f02008c
    .end array-data

    :array_c
    .array-data 4
        0x7f020080
        0x7f020080
        0x7f020080
        0x7f020080
    .end array-data

    :array_d
    .array-data 4
        0x7f020087
        0x7f020087
        0x7f020087
        0x7f020087
    .end array-data

    :array_e
    .array-data 4
        0x7f020082
        0x7f020082
        0x7f020082
        0x7f020082
    .end array-data

    :array_f
    .array-data 4
        0x7f020089
        0x7f020089
        0x7f020089
        0x7f020089
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
