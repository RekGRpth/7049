.class public Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;
.super Ljava/lang/Object;
.source "TelephonyIconsGemini.java"


# static fields
.field static final DATA:[[I

.field static final DATA_1X:[I

.field static final DATA_1X_ROAM:[I

.field static final DATA_3G:[I

.field static final DATA_3G_ROAM:[I

.field static final DATA_4G:[I

.field static final DATA_4G_ROAM:[I

.field static final DATA_E:[I

.field static final DATA_E_ROAM:[I

.field static final DATA_G:[I

.field static final DATA_G_ROAM:[I

.field static final DATA_H:[I

.field static final DATA_H_ROAM:[I

.field static final DATA_ROAM:[[I

.field static final ROAMING:[I

.field public static final SIM_INDICATOR:[I

.field private static final TELEPHONY_SIGNAL_STRENGTH:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x5

    const/4 v3, 0x4

    new-array v0, v3, [[I

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v6

    new-array v1, v4, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v7

    const/4 v1, 0x3

    new-array v2, v4, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->TELEPHONY_SIGNAL_STRENGTH:[[I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_1X:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_3G:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_4G:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_E:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_G:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_H:[I

    const/4 v0, 0x6

    new-array v0, v0, [[I

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_1X:[I

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_3G:[I

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_4G:[I

    aput-object v1, v0, v7

    const/4 v1, 0x3

    sget-object v2, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_E:[I

    aput-object v2, v0, v1

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_G:[I

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_H:[I

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA:[[I

    new-array v0, v3, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_1X_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_3G_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_4G_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_E_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_G_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_H_ROAM:[I

    const/4 v0, 0x6

    new-array v0, v0, [[I

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_1X_ROAM:[I

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_3G_ROAM:[I

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_4G_ROAM:[I

    aput-object v1, v0, v7

    const/4 v1, 0x3

    sget-object v2, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_E_ROAM:[I

    aput-object v2, v0, v1

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_G_ROAM:[I

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_H_ROAM:[I

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_ROAM:[[I

    new-array v0, v3, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->ROAMING:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_11

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->SIM_INDICATOR:[I

    return-void

    :array_0
    .array-data 4
        0x7f0200cd
        0x7f0200ce
        0x7f0200d2
        0x7f0200d6
        0x7f0200da
    .end array-data

    :array_1
    .array-data 4
        0x7f0200cd
        0x7f0200d0
        0x7f0200d4
        0x7f0200d8
        0x7f0200dc
    .end array-data

    :array_2
    .array-data 4
        0x7f0200cd
        0x7f0200cf
        0x7f0200d3
        0x7f0200d7
        0x7f0200db
    .end array-data

    :array_3
    .array-data 4
        0x7f0200cd
        0x7f0200d1
        0x7f0200d5
        0x7f0200d9
        0x7f0200dd
    .end array-data

    :array_4
    .array-data 4
        0x7f020098
        0x7f02009c
        0x7f02009a
        0x7f02009e
    .end array-data

    :array_5
    .array-data 4
        0x7f0200a0
        0x7f0200a4
        0x7f0200a2
        0x7f0200a6
    .end array-data

    :array_6
    .array-data 4
        0x7f0200a8
        0x7f0200ac
        0x7f0200aa
        0x7f0200ae
    .end array-data

    :array_7
    .array-data 4
        0x7f0200b0
        0x7f0200b4
        0x7f0200b2
        0x7f0200b6
    .end array-data

    :array_8
    .array-data 4
        0x7f0200b8
        0x7f0200bc
        0x7f0200ba
        0x7f0200be
    .end array-data

    :array_9
    .array-data 4
        0x7f0200c0
        0x7f0200c4
        0x7f0200c2
        0x7f0200c6
    .end array-data

    :array_a
    .array-data 4
        0x7f020099
        0x7f02009d
        0x7f02009b
        0x7f02009f
    .end array-data

    :array_b
    .array-data 4
        0x7f0200a1
        0x7f0200a5
        0x7f0200a3
        0x7f0200a7
    .end array-data

    :array_c
    .array-data 4
        0x7f0200a9
        0x7f0200ad
        0x7f0200ab
        0x7f0200af
    .end array-data

    :array_d
    .array-data 4
        0x7f0200b1
        0x7f0200b5
        0x7f0200b3
        0x7f0200b7
    .end array-data

    :array_e
    .array-data 4
        0x7f0200b9
        0x7f0200bd
        0x7f0200bb
        0x7f0200bf
    .end array-data

    :array_f
    .array-data 4
        0x7f0200c1
        0x7f0200c5
        0x7f0200c3
        0x7f0200c7
    .end array-data

    :array_10
    .array-data 4
        0x7f0200c8
        0x7f0200ca
        0x7f0200c9
        0x7f0200cb
    .end array-data

    :array_11
    .array-data 4
        0x7f020013
        0x7f020016
        0x7f020014
        0x7f020017
        0x7f020012
        0x7f020015
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDataTypeIconListGemini(ZLcom/mediatek/systemui/ext/DataType;)[I
    .locals 3
    .param p0    # Z
    .param p1    # Lcom/mediatek/systemui/ext/DataType;

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA_ROAM:[[I

    invoke-virtual {p1}, Lcom/mediatek/systemui/ext/DataType;->getTypeId()I

    move-result v2

    aget-object v0, v1, v2

    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->DATA:[[I

    invoke-virtual {p1}, Lcom/mediatek/systemui/ext/DataType;->getTypeId()I

    move-result v2

    aget-object v0, v1, v2

    goto :goto_0
.end method

.method public static getTelephonySignalStrengthIconList(I)[I
    .locals 1
    .param p0    # I

    sget-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIconsGemini;->TELEPHONY_SIGNAL_STRENGTH:[[I

    aget-object v0, v0, p0

    return-object v0
.end method
