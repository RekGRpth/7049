.class public Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
.super Landroid/widget/LinearLayout;
.source "ConfigurationIconView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ConfigurationIconView"


# instance fields
.field private mConfigIconLayout:Landroid/widget/FrameLayout;

.field private mConfigImage:Landroid/widget/ImageView;

.field private mConfigName:Landroid/widget/TextView;

.field private mOnIndicator:Landroid/widget/ImageView;

.field private mSwitchIngGifView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private initSwitchingGifView()V
    .locals 4

    const/4 v2, -0x1

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mSwitchIngGifView:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mSwitchIngGifView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mSwitchIngGifView:Landroid/widget/ImageView;

    const v2, 0x7f020145

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigIconLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mSwitchIngGifView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mSwitchIngGifView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public enlargeTouchRegion()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput v3, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    iput v3, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigImage:Landroid/widget/ImageView;

    invoke-direct {v1, v0, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    return-void
.end method

.method public getConfigView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getIndicatorView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mOnIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getSwitchingGifView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mSwitchIngGifView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->initSwitchingGifView()V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mSwitchIngGifView:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0c00c7

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigName:Landroid/widget/TextView;

    const v0, 0x7f0c00c5

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigImage:Landroid/widget/ImageView;

    const v0, 0x7f0c00c6

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mOnIndicator:Landroid/widget/ImageView;

    const v0, 0x7f0c00c4

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigIconLayout:Landroid/widget/FrameLayout;

    return-void
.end method

.method public setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setConfigDrawable(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public setConfigName(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setOnIndicator(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mOnIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mOnIndicator:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTagForIcon(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->mConfigImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void
.end method
