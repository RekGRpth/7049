.class final Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;
.super Lcom/android/systemui/statusbar/util/StateTracker;
.source "ConfigurationSwitchPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AutoRotationStateTracker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;


# direct methods
.method private constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/util/StateTracker;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
    .param p2    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V

    return-void
.end method


# virtual methods
.method public getActualState(Landroid/content/Context;)I
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    const/4 v4, -0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    if-nez v0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, -0x2

    goto :goto_0
.end method

.method public getDisabledResource()I
    .locals 1

    const v0, 0x7f02011e

    return v0
.end method

.method public getEnabledResource()I
    .locals 1

    const v0, 0x7f02011d

    return v0
.end method

.method public getImageButtonView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$1200(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getIndicatorView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$1200(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getIndicatorView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;->getActualState(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setCurrentState(Landroid/content/Context;I)V

    return-void
.end method

.method public requestStateChange(Landroid/content/Context;Z)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker$1;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;Landroid/content/Context;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
