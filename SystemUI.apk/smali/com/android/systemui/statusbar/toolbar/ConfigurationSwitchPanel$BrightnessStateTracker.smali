.class final Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;
.super Lcom/android/systemui/statusbar/util/StateTracker;
.source "ConfigurationSwitchPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BrightnessStateTracker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;


# direct methods
.method private constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/util/StateTracker;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
    .param p2    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V

    return-void
.end method


# virtual methods
.method public getActualState(Landroid/content/Context;)I
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    return v0
.end method

.method public getDisabledResource()I
    .locals 1

    const v0, 0x7f020124

    return v0
.end method

.method public getEnabledResource()I
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$700(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v1, 0x7f020122

    :goto_0
    return v1

    :cond_0
    const v1, 0x7f020125

    goto :goto_0
.end method

.method public getImageButtonView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$800(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getIndicatorView()Landroid/widget/ImageView;
    .locals 6

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$800(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getIndicatorView()Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$700(Landroid/content/Context;)Z

    move-result v1

    const-string v3, "ConfigurationSwitchPanel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Brightnesst getIndicatorView: brightnessMode is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-object v2

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->getBrightness(Landroid/content/Context;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const v3, 0x7f02012e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :sswitch_1
    const v3, 0x7f02012f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :sswitch_2
    const v3, 0x7f02012d

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1e -> :sswitch_0
        0x66 -> :sswitch_1
        0xff -> :sswitch_2
    .end sparse-switch
.end method

.method public onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setCurrentState(Landroid/content/Context;I)V

    return-void
.end method

.method protected requestStateChange(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setCurrentState(Landroid/content/Context;I)V

    return-void
.end method

.method public toggleState(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$900(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Landroid/content/Context;)V

    return-void
.end method
