.class final Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;
.super Lcom/android/systemui/statusbar/util/StateTracker;
.source "ConfigurationSwitchPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TimeoutStateTracker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;


# direct methods
.method private constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/util/StateTracker;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
    .param p2    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V

    return-void
.end method


# virtual methods
.method public getActualState(Landroid/content/Context;)I
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    return v0
.end method

.method public getDisabledResource()I
    .locals 1

    const v0, 0x7f02015a

    return v0
.end method

.method public getEnabledResource()I
    .locals 1

    const v0, 0x7f02015b

    return v0
.end method

.method public getImageButtonView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$1000(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getIndicatorView()Landroid/widget/ImageView;
    .locals 3

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$1000(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getIndicatorView()Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->getTimeout(Landroid/content/Context;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-object v1

    :sswitch_0
    const v2, 0x7f02012e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :sswitch_1
    const v2, 0x7f02012f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :sswitch_2
    const v2, 0x7f02012d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3a98 -> :sswitch_0
        0x7530 -> :sswitch_1
        0xea60 -> :sswitch_2
    .end sparse-switch
.end method

.method public onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setCurrentState(Landroid/content/Context;I)V

    return-void
.end method

.method protected requestStateChange(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setCurrentState(Landroid/content/Context;I)V

    return-void
.end method

.method public toggleState(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->access$1100(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Landroid/content/Context;)V

    return-void
.end method
