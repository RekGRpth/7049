.class public final Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
.super Landroid/widget/LinearLayout;
.source "ConfigurationSwitchPanel.java"

# interfaces
.implements Lcom/android/systemui/statusbar/util/Configurable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;,
        Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;,
        Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;
    }
.end annotation


# static fields
.field private static final COUNT:I = 0x3

.field private static final DBG:Z = true

.field public static final DEFAULT_BACKLIGHT:I = 0x66

.field private static final FALLBACK_SCREEN_TIMEOUT_VALUE:I = 0x7530

.field public static final MAXIMUM_BACKLIGHT:I = 0xff

.field public static final MAXIMUM_TIMEOUT:I = 0xea60

.field public static final MEDIUM_TIMEOUT:I = 0x7530

.field public static final MINIMUM_BACKLIGHT:I = 0x1e

.field public static final MINIMUM_TIMEOUT:I = 0x3a98

.field private static final TAG:Ljava/lang/String; = "ConfigurationSwitchPanel"


# instance fields
.field private mAutoRatationIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mAutoRotationChangeObserver:Landroid/database/ContentObserver;

.field private mAutoRotationStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;

.field private mAutomaticAvailable:Z

.field private mBrightnessChangeObserver:Landroid/database/ContentObserver;

.field private mBrightnessIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mBrightnessModeChangeObserver:Landroid/database/ContentObserver;

.field private mBrightnessStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;

.field private mContext:Landroid/content/Context;

.field private mIndicatorView:Landroid/graphics/drawable/Drawable;

.field private mTimeoutChangeObserver:Landroid/database/ContentObserver;

.field private mTimeoutIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mTimeoutStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;

.field private mToolBarView:Lcom/android/systemui/statusbar/toolbar/ToolBarView;

.field private mUpdating:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mUpdating:Z

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessChangeObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$2;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessModeChangeObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$3;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutChangeObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$4;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$4;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRotationChangeObserver:Landroid/database/ContentObserver;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Landroid/content/Context;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->toggleTimeout(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRatationIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRotationStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;

    return-object v0
.end method

.method static synthetic access$700(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->getBrightnessMode(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Landroid/content/Context;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->toggleBrightness(Landroid/content/Context;)V

    return-void
.end method

.method public static getBrightness(Landroid/content/Context;)I
    .locals 7
    .param p0    # Landroid/content/Context;

    const/16 v3, 0x66

    const-string v4, "ConfigurationSwitchPanel"

    const-string v5, "getBrightness called."

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v4, "power"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "screen_brightness"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/16 v4, 0x1e

    if-gt v0, v4, :cond_0

    const/16 v0, 0x1e

    :goto_0
    return v0

    :cond_0
    if-gt v0, v3, :cond_1

    const/16 v0, 0x66

    goto :goto_0

    :cond_1
    const/16 v0, 0xff

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "ConfigurationSwitchPanel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getBrightness: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method private static getBrightnessMode(Landroid/content/Context;)Z
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_0
    const-string v5, "power"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "screen_brightness_mode"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-ne v0, v3, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "ConfigurationSwitchPanel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getBrightnessMode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v3, v4

    goto :goto_0
.end method

.method public static getTimeout(Landroid/content/Context;)I
    .locals 6
    .param p0    # Landroid/content/Context;

    const/16 v2, 0x7530

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_off_timeout"

    const/16 v5, 0x7530

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/16 v3, 0x3a98

    if-gt v1, v3, :cond_0

    const/16 v1, 0x3a98

    :goto_0
    return v1

    :cond_0
    if-gt v1, v2, :cond_1

    const/16 v1, 0x7530

    goto :goto_0

    :cond_1
    const v1, 0xea60

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "ConfigurationSwitchPanel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTimeout: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    goto :goto_0
.end method

.method private toggleBrightness(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v7, 0x1

    :try_start_0
    const-string v6, "power"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v8, "sensor"

    invoke-virtual {v6, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/SensorManager;

    const/4 v6, 0x5

    invoke-virtual {v4, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v6

    if-eqz v6, :cond_2

    move v6, v7

    :goto_0
    iput-boolean v6, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutomaticAvailable:Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "screen_brightness"

    invoke-static {v2, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutomaticAvailable:Z

    if-eqz v6, :cond_0

    const-string v6, "screen_brightness_mode"

    invoke-static {v2, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    :cond_0
    if-ne v1, v7, :cond_3

    const/16 v0, 0x1e

    const/4 v1, 0x0

    :goto_1
    iget-boolean v6, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutomaticAvailable:Z

    if-eqz v6, :cond_6

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "screen_brightness_mode"

    invoke-static {v6, v7, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_2
    if-nez v1, :cond_1

    invoke-interface {v5, v0}, Landroid/os/IPowerManager;->setBacklightBrightness(I)V

    const-string v6, "screen_brightness"

    invoke-static {v2, v6, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_3
    return-void

    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    :cond_3
    const/16 v6, 0x66

    if-ge v0, v6, :cond_4

    const/16 v0, 0x66

    goto :goto_1

    :cond_4
    const/16 v6, 0xff

    if-ge v0, v6, :cond_5

    const/16 v0, 0xff

    goto :goto_1

    :cond_5
    const/4 v1, 0x1

    const/16 v0, 0x1e

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    :catch_0
    move-exception v3

    const-string v6, "ConfigurationSwitchPanel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "toggleBrightness: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_1
    move-exception v3

    const-string v6, "ConfigurationSwitchPanel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "toggleBrightness: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private toggleTimeout(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/16 v6, 0x7530

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "screen_off_timeout"

    const/16 v4, 0x7530

    invoke-static {v0, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const-string v3, "ConfigurationSwitchPanel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "toggleTimeout, before is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x3a98

    if-gt v2, v3, :cond_0

    const/16 v2, 0x7530

    :goto_0
    const-string v3, "screen_off_timeout"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v3, "ConfigurationSwitchPanel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "toggleTimeout, after is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    if-gt v2, v6, :cond_1

    const v2, 0xea60

    goto :goto_0

    :cond_1
    const/16 v2, 0x3a98

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "ConfigurationSwitchPanel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "toggleTimeout: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public buildIconViews()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    new-instance v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;

    invoke-direct {v3, p0, v5}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;)V

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;

    new-instance v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;

    invoke-direct {v3, p0, v5}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;)V

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;

    new-instance v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;

    invoke-direct {v3, p0, v5}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$1;)V

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRotationStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/high16 v4, 0x3f800000

    invoke-direct {v2, v6, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    const v4, 0x7f03002c

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRatationIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v4, 0x7f090014

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v4, 0x7f090015

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRatationIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v4, 0x7f09000e

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    new-instance v4, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$5;

    invoke-direct {v4, p0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$5;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    new-instance v4, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$6;

    invoke-direct {v4, p0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$6;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRatationIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    new-instance v4, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$7;

    invoke-direct {v4, p0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$7;-><init>(Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;)V

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public enlargeTouchRegion()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRatationIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    return-void
.end method

.method protected getSuggestedMinimumWidth()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public initConfigurationState()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$BrightnessStateTracker;

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$TimeoutStateTracker;

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRotationStateTracker:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel$AutoRotationStateTracker;

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    return-void
.end method

.method public setToolBar(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mToolBarView:Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    return-void
.end method

.method setUpdates(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mUpdating:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mUpdating:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessModeChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_off_timeout"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRotationChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessModeChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRotationChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public updateResources()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mBrightnessIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f090014

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mTimeoutIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->mAutoRatationIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f09000e

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    return-void
.end method
