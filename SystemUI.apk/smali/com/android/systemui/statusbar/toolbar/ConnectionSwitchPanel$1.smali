.class Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;
.super Landroid/os/Handler;
.source "ConnectionSwitchPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    const-wide/16 v11, 0x0

    const-wide/16 v9, -0x5

    const/16 v8, 0x8

    const/4 v7, -0x1

    const/4 v6, 0x0

    iget v3, p1, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gprs_connection_sim_setting"

    invoke-static {v3, v4, v9, v10}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v1

    const-string v3, "ConnectionSwitchPanelView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "detach time out......simId is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setIsUserSwitching(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->isClickable()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    cmp-long v3, v1, v11

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getIndicatorView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1, v2}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/systemui/statusbar/util/SIMHelper;->getDataConnectionIconIdBySlotId(Landroid/content/Context;I)I

    move-result v0

    if-eq v0, v7, :cond_0

    if-eq v0, v7, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getIndicatorView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->getDisabledResource()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gprs_connection_sim_setting"

    invoke-static {v3, v4, v9, v10}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v1

    const-string v3, "ConnectionSwitchPanelView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "attach time out......simId is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setIsUserSwitching(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->isClickable()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    cmp-long v3, v1, v11

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3, v1, v2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$400(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;J)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getIndicatorView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1, v2}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v4

    invoke-static {v3, v4}, Lcom/android/systemui/statusbar/util/SIMHelper;->getDataConnectionIconIdBySlotId(Landroid/content/Context;I)I

    move-result v0

    if-eq v0, v7, :cond_0

    if-eq v0, v7, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getIndicatorView()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->getDisabledResource()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$500(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->isStatusBarExpanded()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v4

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7d0 -> :sswitch_0
        0x7d1 -> :sswitch_1
        0x7da -> :sswitch_2
    .end sparse-switch
.end method
