.class Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;
.super Ljava/lang/Object;
.source "ConnectionSwitchPanel.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->buildIconViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ClickEvent"

    const-string v1, "AirPlane button click"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;->setAirPlaneModeClickable(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;->toggleState(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    new-instance v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11$1;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;)V

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
