.class Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;
.super Landroid/content/BroadcastReceiver;
.source "ConnectionSwitchPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v13, "ConnectionSwitchPanelView"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onReceive called, action is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v13, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$900(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v13, v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$900(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v13, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v13, v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;->onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const-string v13, "android.location.PROVIDERS_CHANGED"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v13, v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;->onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    const-string v13, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    const-string v13, "state"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v13, "ConnectionSwitchPanelView"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "airline mode changed: state is "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z

    move-result v13

    if-nez v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v13

    invoke-virtual {v13, v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setAirlineMode(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->isClickable()Z

    move-result v14

    invoke-virtual {v13, v14}, Landroid/view/View;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z

    move-result v13

    if-eqz v13, :cond_5

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-string v13, "state"

    invoke-virtual {v6, v13, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v13, v14, v6}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;->onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/mediatek/systemui/ext/PluginFactory;->getStatusBarPlugin(Landroid/content/Context;)Lcom/mediatek/systemui/ext/IStatusBarPlugin;

    move-result-object v13

    invoke-interface {v13}, Lcom/mediatek/systemui/ext/IStatusBarPlugin;->supportDisableWifiAtAirplaneMode()Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$900(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    move-result-object v13

    invoke-virtual {v13, v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->setAirlineMode(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$900(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->isClickable()Z

    move-result v14

    invoke-virtual {v13, v14}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_6
    const-string v13, "android.intent.action.ANY_DATA_STATE"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    invoke-static/range {p2 .. p2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1400(Landroid/content/Intent;)Lcom/android/internal/telephony/Phone$DataState;

    move-result-object v9

    const/4 v7, 0x0

    const-string v13, "apnType"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_7

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    move-object v3, v11

    array-length v8, v3

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v8, :cond_7

    aget-object v10, v3, v5

    const-string v13, "default"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    const/4 v7, 0x1

    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z

    move-result v13

    if-nez v13, :cond_0

    const-string v13, "ConnectionSwitchPanelView"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "isApnType = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " , state = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " , mMobileStateTracker.mGprsTargSim = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v15}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v15

    invoke-static {v15}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->access$1500(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;)Z

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " , mMobileStateTracker.mIsMmsOngoing = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v15}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v15

    invoke-static {v15}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->access$1600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;)Z

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v7, :cond_0

    sget-object v13, Lcom/android/internal/telephony/Phone$DataState;->CONNECTED:Lcom/android/internal/telephony/Phone$DataState;

    if-eq v9, v13, :cond_8

    sget-object v13, Lcom/android/internal/telephony/Phone$DataState;->DISCONNECTED:Lcom/android/internal/telephony/Phone$DataState;

    if-ne v9, v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v13

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->access$1500(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;)Z

    move-result v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v13

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->access$1600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;)Z

    move-result v13

    if-nez v13, :cond_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v13, v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_a
    const-string v13, "com.android.mms.transaction.START"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z

    move-result v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setIsMmsOngoing(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->isClickable()Z

    move-result v14

    invoke-virtual {v13, v14}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_b
    const-string v13, "com.android.mms.transaction.STOP"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z

    move-result v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setIsMmsOngoing(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v13}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->isClickable()Z

    move-result v14

    invoke-virtual {v13, v14}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0
.end method
