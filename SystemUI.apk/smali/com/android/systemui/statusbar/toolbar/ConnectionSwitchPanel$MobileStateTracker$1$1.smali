.class Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;
.super Ljava/lang/Object;
.source "ConnectionSwitchPanel.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;->this$2:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/SimIconsListView$SimItem;

    if-eqz v0, :cond_3

    iget-boolean v1, v0, Lcom/android/systemui/statusbar/toolbar/SimIconsListView$SimItem;->mIsSim:Z

    if-eqz v1, :cond_1

    iget-wide v1, v0, Lcom/android/systemui/statusbar/toolbar/SimIconsListView$SimItem;->mSimID:J

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;->this$2:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;

    iget-object v3, v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v3, v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "gprs_connection_sim_setting"

    invoke-static {v3, v4}, Lcom/android/systemui/statusbar/util/SIMHelper;->getDefaultSIM(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;->this$2:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    :cond_1
    iget-boolean v1, v0, Lcom/android/systemui/statusbar/toolbar/SimIconsListView$SimItem;->mIsSim:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;->this$2:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "gprs_connection_sim_setting"

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/util/SIMHelper;->getDefaultSIM(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;->this$2:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;->this$2:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-static {v1, v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->access$2700(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;Lcom/android/systemui/statusbar/toolbar/SimIconsListView$SimItem;)V

    goto :goto_0

    :cond_3
    const-string v1, "ConnectionSwitchPanelView"

    const-string v2, "MobileIcon clicked and clicked a null sim item"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
