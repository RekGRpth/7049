.class Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;
.super Ljava/lang/Object;
.source "ConnectionSwitchPanel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->toggleState(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    new-instance v2, Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v3, v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "gprs_connection_sim_setting"

    invoke-direct {v2, v3, v4}, Lcom/android/systemui/statusbar/toolbar/SimIconsListView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2602(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Lcom/android/systemui/statusbar/toolbar/SimIconsListView;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    move-result-object v1

    new-instance v2, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1$1;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v2, v2, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v3, v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    move-result-object v3

    const v4, 0x7f09000c

    invoke-static {v2, v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2800(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Landroid/view/View;I)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2302(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/toolbar/SimIconsListView;->initSimList()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/toolbar/SimIconsListView;->notifyDataChange()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7de

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v1, v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$2300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker$1;->this$1:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->access$2902(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;Z)Z

    return-void
.end method
