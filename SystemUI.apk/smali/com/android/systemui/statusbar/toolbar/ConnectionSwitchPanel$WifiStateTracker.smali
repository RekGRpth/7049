.class final Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;
.super Lcom/android/systemui/statusbar/util/StateTracker;
.source "ConnectionSwitchPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WifiStateTracker"
.end annotation


# instance fields
.field private mIsAirlineMode:Z

.field final synthetic this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;


# direct methods
.method private constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/util/StateTracker;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->mIsAirlineMode:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .param p2    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    return-void
.end method

.method private wifiStateToFiveState(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getActualState(Landroid/content/Context;)I
    .locals 2
    .param p1    # Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->wifiStateToFiveState(I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDisabledResource()I
    .locals 1

    const v0, 0x7f02015e

    return v0
.end method

.method public getEnabledResource()I
    .locals 1

    const v0, 0x7f02015d

    return v0
.end method

.method public getImageButtonView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getIndicatorView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getIndicatorView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getInterMedateResource()I
    .locals 1

    const v0, 0x7f020154

    return v0
.end method

.method public getSwitchingGifView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->this$0:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-static {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->access$1300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getSwitchingGifView()Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public isClickable()Z
    .locals 3

    const-string v0, "ConnectionSwitchPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wifi mIsAirlineMode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->mIsAirlineMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mIsUserSwitching is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/util/StateTracker;->mIsUserSwitching:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->mIsAirlineMode:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/android/systemui/statusbar/util/StateTracker;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "wifi_state"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->wifiStateToFiveState(I)I

    move-result v1

    invoke-virtual {p0, p1, v1}, Lcom/android/systemui/statusbar/util/StateTracker;->setCurrentState(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected requestStateChange(Landroid/content/Context;Z)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v3, 0x0

    const-string v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_0

    const-string v1, "ConnectionSwitchPanelView"

    const-string v2, "No wifiManager."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v3}, Lcom/android/systemui/statusbar/util/StateTracker;->setCurrentState(Landroid/content/Context;I)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker$1;

    invoke-direct {v1, p0, v0, p2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker$1;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;Landroid/net/wifi/WifiManager;Z)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public setAirlineMode(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "ConnectionSwitchPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mobile setAirlineMode called, enabled is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->mIsAirlineMode:Z

    return-void
.end method
