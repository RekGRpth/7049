.class public final Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
.super Landroid/widget/LinearLayout;
.source "ConnectionSwitchPanel.java"

# interfaces
.implements Lcom/android/systemui/statusbar/util/Configurable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$13;,
        Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;,
        Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;,
        Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;,
        Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;,
        Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;
    }
.end annotation


# static fields
.field private static final ATTACH_TIME_OUT_LENGTH:I = 0x7530

.field private static final COUNT:I = 0x5

.field private static final DBG:Z = true

.field private static final DETACH_TIME_OUT_LENGTH:I = 0x2710

.field private static final EVENT_ATTACH_TIME_OUT:I = 0x7d1

.field private static final EVENT_DATA_CONNECTION_RESULT:I = 0x7da

.field private static final EVENT_DETACH_TIME_OUT:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "ConnectionSwitchPanelView"

.field private static final TRANSACTION_START:Ljava/lang/String; = "com.android.mms.transaction.START"

.field private static final TRANSACTION_STOP:Ljava/lang/String; = "com.android.mms.transaction.STOP"


# instance fields
.field private mAirlineModeIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mAirlineModeStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

.field private mBluetoothIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mBluetoothStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;

.field private mContext:Landroid/content/Context;

.field private mDataConnectionSimSlot:I

.field private mDataTimerHandler:Landroid/os/Handler;

.field private mGpsIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mGpsStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;

.field private mIndicatorView:Landroid/graphics/drawable/Drawable;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mMobileStateChangeObserver:Landroid/database/ContentObserver;

.field private mMobileStateForSingleCardChangeObserver:Landroid/database/ContentObserver;

.field private mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

.field mPhoneStateListener1:Landroid/telephony/PhoneStateListener;

.field mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

.field private mServiceState1:I

.field private mServiceState2:I

.field private mSimCardReady:Z

.field private mStatusBarService:Lcom/android/systemui/statusbar/BaseStatusBar;

.field private mSwitchDialog:Landroid/app/AlertDialog;

.field private mSwitchListview:Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mToolBarView:Lcom/android/systemui/statusbar/toolbar/ToolBarView;

.field private mUpdating:Z

.field private mWifiIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mWifiStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mUpdating:Z

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSimCardReady:Z

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mDataTimerHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$2;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateChangeObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$3;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateForSingleCardChangeObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$4;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$4;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mPhoneStateListener1:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$5;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$5;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$6;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-void
.end method

.method static synthetic access$000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isWifiOnlyDevice()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    return-object v0
.end method

.method static synthetic access$1400(Landroid/content/Intent;)Lcom/android/internal/telephony/Phone$DataState;
    .locals 1
    .param p0    # Landroid/content/Intent;

    invoke-static {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->getMobileDataState(Landroid/content/Intent;)Lcom/android/internal/telephony/Phone$DataState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Z
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->wasItTheMonkey()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchListview:Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Lcom/android/systemui/statusbar/toolbar/SimIconsListView;)Lcom/android/systemui/statusbar/toolbar/SimIconsListView;
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .param p1    # Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchListview:Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Landroid/view/View;I)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->createDialog(Landroid/view/View;I)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mDataTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;I)I
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .param p1    # I

    iput p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mDataConnectionSimSlot:I

    return p1
.end method

.method static synthetic access$3200(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;J)Z
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isValidSimCardDataConn(J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ToolBarView;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mToolBarView:Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;I)I
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .param p1    # I

    iput p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mServiceState1:I

    return p1
.end method

.method static synthetic access$700(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->onAirplaneModeChanged()V

    return-void
.end method

.method static synthetic access$802(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;I)I
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .param p1    # I

    iput p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mServiceState2:I

    return p1
.end method

.method static synthetic access$900(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    return-object v0
.end method

.method private createDialog(Landroid/view/View;I)Landroid/app/AlertDialog;
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v7, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    move-object v1, p1

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;IIII)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$12;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$12;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7e1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    return-object v6
.end method

.method private static getMobileDataState(Landroid/content/Intent;)Lcom/android/internal/telephony/Phone$DataState;
    .locals 2
    .param p0    # Landroid/content/Intent;

    const-string v1, "state"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v1, Lcom/android/internal/telephony/Phone$DataState;

    invoke-static {v1, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/Phone$DataState;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/android/internal/telephony/Phone$DataState;->DISCONNECTED:Lcom/android/internal/telephony/Phone$DataState;

    goto :goto_0
.end method

.method public static isAirplaneModeOn(Landroid/content/Context;)Z
    .locals 3
    .param p0    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isCallStateIdle()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4, v3}, Landroid/telephony/TelephonyManager;->getCallStateGemini(I)I

    move-result v0

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4, v2}, Landroid/telephony/TelephonyManager;->getCallStateGemini(I)I

    move-result v1

    const-string v4, "ConnectionSwitchPanelView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stateSim1 is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " stateSim2 is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method private isValidSimCardDataConn(J)Z
    .locals 4
    .param p1    # J

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfo(Landroid/content/Context;J)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "ConnectionSwitchPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isValidSimCardDataConn called, SimSlot is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    iget v2, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/util/SIMHelper;->checkSimCardDataConnBySlotId(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isWifiOnlyDevice()Z
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private onAirplaneModeChanged()V
    .locals 5

    const/4 v3, 0x3

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mServiceState1:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mServiceState2:I

    if-eq v2, v3, :cond_1

    :cond_0
    const-string v2, "ConnectionSwitchPanelView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unfinish! serviceState1:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mServiceState1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " serviceState2:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mServiceState2:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v2, "ConnectionSwitchPanelView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onServiceStateChanged called, inAirplaneMode is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "state"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;->onActualStateChange(Landroid/content/Context;Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private wasItTheMonkey()Z
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ConnectionSwitchPanelView"

    const-string v2, "it was the monkey"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const-string v1, "ConnectionSwitchPanelView"

    const-string v2, "it was an user"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public buildIconViews()V
    .locals 8

    const v7, 0x7f03002c

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v2, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    invoke-direct {v2, p0, v5}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    new-instance v2, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;

    invoke-direct {v2, p0, v5}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;

    new-instance v2, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;

    invoke-direct {v2, p0, v5}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isWifiOnlyDevice()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-direct {v2, p0, v5}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    :cond_0
    new-instance v2, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    invoke-direct {v2, p0, v5}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$1;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/high16 v4, 0x3f800000

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2, v7, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v3, 0x7f09000a

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    new-instance v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$7;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$7;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2, v7, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v3, 0x7f09000b

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    new-instance v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$8;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$8;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2, v7, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v3, 0x7f09000d

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    new-instance v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$9;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$9;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isWifiOnlyDevice()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2, v7, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v3, 0x7f09000c

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    new-instance v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$10;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$10;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2, v7, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v3, 0x7f090013

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    new-instance v3, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;

    invoke-direct {v3, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$11;-><init>(Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;)V

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020131

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mIndicatorView:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public dismissDialogs()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public enlargeTouchRegion()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isWifiOnlyDevice()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    return-void
.end method

.method protected getSuggestedMinimumWidth()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public initConfigurationState()V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v0

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mediatek/systemui/ext/PluginFactory;->getStatusBarPlugin(Landroid/content/Context;)Lcom/mediatek/systemui/ext/IStatusBarPlugin;

    move-result-object v2

    invoke-interface {v2}, Lcom/mediatek/systemui/ext/IStatusBarPlugin;->supportDisableWifiAtAirplaneMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    invoke-virtual {v2, v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;->setAirlineMode(Z)V

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$WifiStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$BluetoothStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$GpsStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$AirlineModeStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isWifiOnlyDevice()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-virtual {v2, v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setAirlineMode(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-virtual {v2, v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setHasSim(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v4}, Lcom/android/systemui/statusbar/util/StateTracker;->setCurrentState(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    const-string v2, "gsm.siminfo.ready"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSimCardReady:Z

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSimCardReady:Z

    if-eqz v2, :cond_2

    const-string v2, "ConnectionSwitchPanelView"

    const-string v3, "Oops, sim ready, maybe phone is drop down and restarted"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_3

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-virtual {v2, v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setHasSim(Z)V

    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->isClickable()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    :cond_2
    return-void

    :cond_3
    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setHasSim(Z)V

    goto :goto_0
.end method

.method public setStatusBarService(Lcom/android/systemui/statusbar/BaseStatusBar;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/BaseStatusBar;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mStatusBarService:Lcom/android/systemui/statusbar/BaseStatusBar;

    return-void
.end method

.method public setToolBar(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mToolBarView:Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    return-void
.end method

.method setUpdates(Z)V
    .locals 7
    .param p1    # Z

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mUpdating:Z

    if-eq p1, v2, :cond_0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mUpdating:Z

    if-eqz p1, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.location.PROVIDERS_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.ANY_DATA_STATE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.android.mms.transaction.START"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.android.mms.transaction.STOP"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "gprs_connection_sim_setting"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mPhoneStateListener1:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v5, v6}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v5, v5}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mPhoneStateListener1:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v6, v6}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mPhoneStateListener2:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v6, v5}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    goto :goto_0
.end method

.method public updateForSimReady()V
    .locals 4

    const/4 v3, 0x1

    const-string v1, "ConnectionSwitchPanelView"

    const-string v2, "Panel sim ready called"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isWifiOnlyDevice()Z

    move-result v1

    if-nez v1, :cond_1

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSimCardReady:Z

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setHasSim(Z)V

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->isClickable()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/util/StateTracker;->setImageViewResources(Landroid/content/Context;)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileStateTracker:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;

    invoke-virtual {v1, v3}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel$MobileStateTracker;->setHasSim(Z)V

    goto :goto_0
.end method

.method public updateResources()V
    .locals 3

    const v2, 0x7f09000c

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mWifiIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f09000a

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mBluetoothIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mGpsIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isWifiOnlyDevice()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mAirlineModeIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f090013

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchListview:Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mSwitchListview:Lcom/android/systemui/statusbar/toolbar/SimIconsListView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/SimIconsListView;->updateResources()V

    :cond_2
    return-void
.end method

.method public updateSimInfo(Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Intent;

    const/4 v9, -0x1

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isWifiOnlyDevice()Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "type"

    invoke-virtual {p1, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    const-string v6, "simid"

    const-wide/16 v7, -0x1

    invoke-virtual {p1, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iget-object v6, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    const-string v7, "gprs_connection_sim_setting"

    invoke-static {v6, v7}, Lcom/android/systemui/statusbar/util/SIMHelper;->getDefaultSIM(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v6, v3, v0

    if-nez v6, :cond_0

    const-string v6, "ConnectionSwitchPanelView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sim setting changed, simId is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->isValidSimCardDataConn(J)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mContext:Landroid/content/Context;

    invoke-static {v7, v3, v4}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v7

    invoke-static {v6, v7}, Lcom/android/systemui/statusbar/util/SIMHelper;->getDataConnectionIconIdBySlotId(Landroid/content/Context;I)I

    move-result v2

    const-string v6, "ConnectionSwitchPanelView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sim resId is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq v2, v9, :cond_0

    if-eq v2, v9, :cond_0

    iget-object v6, p0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->mMobileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->getConfigView()Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
