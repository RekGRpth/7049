.class Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$2;
.super Lcom/mediatek/common/audioprofile/AudioProfileListener;
.source "ProfileSwitchPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$2;->this$0:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    invoke-direct {p0}, Lcom/mediatek/common/audioprofile/AudioProfileListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioProfileChanged(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$2;->this$0:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    invoke-static {v1}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->access$300(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/mediatek/audioprofile/AudioProfileManager;->getScenario(Ljava/lang/String;)Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    move-result-object v0

    const-string v1, "ProfileSwitchPanelView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive called, profile type is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$2;->this$0:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    invoke-static {v1, v0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->access$200(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)V

    goto :goto_0
.end method
