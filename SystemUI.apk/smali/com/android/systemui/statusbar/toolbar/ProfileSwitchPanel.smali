.class public final Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;
.super Landroid/widget/LinearLayout;
.source "ProfileSwitchPanel.java"

# interfaces
.implements Lcom/android/systemui/statusbar/util/Configurable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$3;
    }
.end annotation


# static fields
.field private static final COUNT:I = 0x4

.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "ProfileSwitchPanelView"


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioProfileListenr:Lcom/mediatek/common/audioprofile/AudioProfileListener;

.field private mIndicatorView:Landroid/graphics/drawable/Drawable;

.field private mMettingOnIndicator:Landroid/widget/ImageView;

.field private mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mMuteOnIndicator:Landroid/widget/ImageView;

.field private mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mNormalOnIndicator:Landroid/widget/ImageView;

.field private mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mOutdoorOnIndicator:Landroid/widget/ImageView;

.field private mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

.field private mProfileKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileManager:Lcom/mediatek/audioprofile/AudioProfileManager;

.field private mProfileSwitchListener:Landroid/view/View$OnClickListener;

.field private mToolBarView:Lcom/android/systemui/statusbar/toolbar/ToolBarView;

.field private mUpdating:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mUpdating:Z

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$1;-><init>(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileSwitchListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$2;-><init>(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mAudioProfileListenr:Lcom/mediatek/common/audioprofile/AudioProfileListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileKeys:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->updateAudioProfile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;
    .param p1    # Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->updateProfileView(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;)Z
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mUpdating:Z

    return v0
.end method

.method private loadDisabledProfileResouceForAll()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f020140

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigDrawable(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f020132

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigDrawable(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f02013c

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigDrawable(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f020143

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigDrawable(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setOnIndicator(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setOnIndicator(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setOnIndicator(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setOnIndicator(Z)V

    return-void
.end method

.method private loadEnabledProfileResource(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)V
    .locals 4
    .param p1    # Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    const/4 v3, 0x1

    const-string v0, "ProfileSwitchPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadEnabledProfileResource called, profile is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel$3;->$SwitchMap$com$mediatek$audioprofile$AudioProfileManager$Scenario:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f020141

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigDrawable(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setOnIndicator(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f020133

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigDrawable(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setOnIndicator(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f020144

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigDrawable(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setOnIndicator(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f02013d

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigDrawable(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v3}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setOnIndicator(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateAudioProfile(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "ProfileSwitchPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateAudioProfile called, selected profile is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileManager:Lcom/mediatek/audioprofile/AudioProfileManager;

    invoke-virtual {v0, p1}, Lcom/mediatek/audioprofile/AudioProfileManager;->setActiveProfile(Ljava/lang/String;)V

    const-string v0, "ProfileSwitchPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateAudioProfile called, setActiveProfile is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateProfileView(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)V
    .locals 0
    .param p1    # Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->loadDisabledProfileResouceForAll()V

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->loadEnabledProfileResource(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)V

    return-void
.end method


# virtual methods
.method public buildProfileIconViews()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const-string v4, "audio"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const-string v4, "audioprofile"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/audioprofile/AudioProfileManager;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileManager:Lcom/mediatek/audioprofile/AudioProfileManager;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/high16 v4, 0x3f800000

    invoke-direct {v2, v6, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x4

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    const v4, 0x7f03002c

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p0, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020131

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mIndicatorView:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iput-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v4, 0x7f09000f

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v4, 0x7f090011

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v4, 0x7f090010

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v4, 0x7f090012

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileSwitchListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    sget-object v4, Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;->GENERAL:Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    invoke-static {v4}, Lcom/mediatek/audioprofile/AudioProfileManager;->getProfileKey(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setTagForIcon(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileSwitchListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    sget-object v4, Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;->SILENT:Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    invoke-static {v4}, Lcom/mediatek/audioprofile/AudioProfileManager;->getProfileKey(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setTagForIcon(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileSwitchListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    sget-object v4, Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;->MEETING:Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    invoke-static {v4}, Lcom/mediatek/audioprofile/AudioProfileManager;->getProfileKey(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setTagForIcon(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileSwitchListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    sget-object v4, Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;->OUTDOOR:Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    invoke-static {v4}, Lcom/mediatek/audioprofile/AudioProfileManager;->getProfileKey(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setTagForIcon(Ljava/lang/Object;)V

    return-void
.end method

.method public enlargeTouchRegion()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->enlargeTouchRegion()V

    return-void
.end method

.method protected getSuggestedMinimumWidth()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public initConfigurationState()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileManager:Lcom/mediatek/audioprofile/AudioProfileManager;

    invoke-virtual {v0}, Lcom/mediatek/audioprofile/AudioProfileManager;->getActiveProfileKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileManager:Lcom/mediatek/audioprofile/AudioProfileManager;

    invoke-virtual {v0}, Lcom/mediatek/audioprofile/AudioProfileManager;->getActiveProfileKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/audioprofile/AudioProfileManager;->getScenario(Ljava/lang/String;)Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->updateProfileView(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;)V

    :cond_0
    return-void
.end method

.method public setToolBar(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mToolBarView:Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    return-void
.end method

.method setUpdates(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mUpdating:Z

    if-eq p1, v0, :cond_0

    const-string v0, "ProfileSwitchPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setUpdates: update = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileKeys:Ljava/util/List;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileManager:Lcom/mediatek/audioprofile/AudioProfileManager;

    invoke-virtual {v0}, Lcom/mediatek/audioprofile/AudioProfileManager;->getPredefinedProfileKeys()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileKeys:Ljava/util/List;

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mUpdating:Z

    if-eqz p1, :cond_1

    const-string v0, "ProfileSwitchPanelView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setUpdates: listenAudioProfie with mAudioProfileListenr = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mAudioProfileListenr:Lcom/mediatek/common/audioprofile/AudioProfileListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileManager:Lcom/mediatek/audioprofile/AudioProfileManager;

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mAudioProfileListenr:Lcom/mediatek/common/audioprofile/AudioProfileListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/audioprofile/AudioProfileManager;->listenAudioProfie(Lcom/mediatek/common/audioprofile/AudioProfileListener;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mProfileManager:Lcom/mediatek/audioprofile/AudioProfileManager;

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mAudioProfileListenr:Lcom/mediatek/common/audioprofile/AudioProfileListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/audioprofile/AudioProfileManager;->listenAudioProfie(Lcom/mediatek/common/audioprofile/AudioProfileListener;I)V

    goto :goto_0
.end method

.method public updateResources()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mNormalProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMuteProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f090011

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mMettingProfileIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f090010

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->mOutdoorSwitchIcon:Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ConfigurationIconView;->setConfigName(I)V

    return-void
.end method
