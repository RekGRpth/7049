.class public Lcom/android/systemui/statusbar/toolbar/ToolBarView;
.super Landroid/view/ViewGroup;
.source "ToolBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;,
        Lcom/android/systemui/statusbar/toolbar/ToolBarView$SavedState;,
        Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;
    }
.end annotation


# static fields
.field private static final BASELINE_FLING_VELOCITY:F = 2500.0f

.field private static final DEBUG:Z = true

.field private static final FLING_VELOCITY_INFLUENCE:F = 0.4f

.field private static final INVALID_POINTER:I = -0x1

.field private static final INVALID_SCREEN:I = -0x1

.field private static final SNAP_VELOCITY:I = 0x258

.field private static final TAG:Ljava/lang/String; = "ToolBarView"

.field private static final TOUCH_STATE_REST:I = 0x0

.field private static final TOUCH_STATE_SCROLLING:I = 0x1


# instance fields
.field private mActivePointerId:I

.field private mAllowLongPress:Z

.field private mConfigurationSwitchPanel:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

.field private mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

.field private mCurrentScreen:I

.field private mDefaultScreen:I

.field private mDisplay:Landroid/view/Display;

.field private mFirstLayout:Z

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsUpdated:Z

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mMaximumVelocity:I

.field private mNextScreen:I

.field private mOverscrollDistance:I

.field private mProfileSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

.field private mScrollInterpolator:Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;

.field private mScrollToScreenCallback:Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;

.field private mScroller:Landroid/widget/Scroller;

.field private mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

.field private mStatusBarService:Lcom/android/systemui/statusbar/BaseStatusBar;

.field private mToolBarIndicator:Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;

.field private mTouchSlop:I

.field private mTouchState:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mFirstLayout:Z

    iput v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    iput v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mAllowLongPress:Z

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mIsUpdated:Z

    iput v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ToolBarView$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$3;-><init>(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    iput v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDefaultScreen:I

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDisplay:Landroid/view/Display;

    invoke-virtual {p0, v1}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->initWorkspace()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->updateForSimNameUpdate()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConfigurationSwitchPanel:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;
    .locals 1
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mProfileSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->setUpdate()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->initConfigurationPanels()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V
    .locals 0
    .param p0    # Lcom/android/systemui/statusbar/toolbar/ToolBarView;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->updateForSimReady()V

    return-void
.end method

.method private acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private initConfigurationPanels()V
    .locals 3

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    instance-of v2, v0, Lcom/android/systemui/statusbar/util/Configurable;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/android/systemui/statusbar/util/Configurable;

    invoke-interface {v0}, Lcom/android/systemui/statusbar/util/Configurable;->initConfigurationState()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method private initWorkspace()V
    .locals 4

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;

    invoke-direct {v2}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;-><init>()V

    iput-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollInterpolator:Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;

    new-instance v2, Landroid/widget/Scroller;

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollInterpolator:Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;

    invoke-direct {v2, v1, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    iget v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDefaultScreen:I

    iput v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mMaximumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mOverscrollDistance:I

    return-void
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const v4, 0xff00

    and-int/2addr v3, v4

    shr-int/lit8 v2, v3, 0x8

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    if-ne v1, v3, :cond_0

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iput v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionX:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iput v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionY:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    iput v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releaseVelocityTracker()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method private setUpdate()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SIM_INFO_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SIM_NAME_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method private snapToScreen(IIZ)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0, v0, p1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->enableChildrenCache(II)V

    iput p1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollToScreenCallback:Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;

    iget v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    invoke-interface {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;->onScrollFinish(I)V

    const-string v0, "ToolBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "snapToScreen  mNextScreen is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-ne v6, v0, :cond_0

    invoke-virtual {v6}, Landroid/view/View;->clearFocus()V

    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    sub-int v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    mul-int v7, p1, v0

    iget v0, p0, Landroid/view/View;->mScrollX:I

    sub-int v3, v7, v0

    add-int/lit8 v0, v8, 0x1

    mul-int/lit8 v5, v0, 0x64

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_1
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollInterpolator:Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;

    invoke-virtual {v0, v8}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;->setDistance(I)V

    :goto_0
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    if-lez p2, :cond_3

    int-to-float v0, v5

    int-to-float v1, v5

    int-to-float v4, p2

    const v9, 0x451c4000

    div-float/2addr v4, v9

    div-float/2addr v1, v4

    const v4, 0x3ecccccd

    mul-float/2addr v1, v4

    add-float/2addr v0, v1

    float-to-int v5, v0

    :goto_1
    invoke-virtual {p0, v5}, Landroid/view/View;->awakenScrollBars(I)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Landroid/view/View;->mScrollX:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollInterpolator:Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$WorkspaceOvershootInterpolator;->disableSettle()V

    goto :goto_0

    :cond_3
    add-int/lit8 v5, v5, 0x64

    goto :goto_1
.end method

.method private updateForSimNameUpdate()V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->updateForSimReady()V

    return-void
.end method

.method private updateForSimReady()V
    .locals 1

    iget-object v0, p0, Landroid/view/View;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/statusbar/util/SIMHelper;->updateSIMInfos(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->updateForSimReady()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;->updateSimInfo()V

    return-void
.end method


# virtual methods
.method public allowLongPress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mAllowLongPress:Z

    return v0
.end method

.method clearChildrenCache()V
    .locals 4

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public computeScroll()V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iput v0, p0, Landroid/view/View;->mScrollX:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    iput v0, p0, Landroid/view/View;->mScrollY:I

    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    if-eq v0, v3, :cond_0

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    const-string v0, "ToolBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "computeScroll  mCurrentScreen is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollToScreenCallback:Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;

    iget v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-interface {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;->onScrollFinish(I)V

    iput v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->clearChildrenCache()V

    goto :goto_0
.end method

.method public dismissDialogs()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->dismissDialogs()V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v8, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    if-eq v8, v2, :cond_2

    iget v8, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_2

    :goto_0
    if-eqz v2, :cond_3

    iget v8, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p0}, Landroid/view/View;->getDrawingTime()J

    move-result-wide v9

    invoke-virtual {p0, p1, v8, v9, v10}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    :cond_0
    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getDrawingTime()J

    move-result-wide v0

    iget v8, p0, Landroid/view/View;->mScrollX:I

    int-to-float v8, v8

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float v7, v8, v9

    float-to-int v3, v7

    add-int/lit8 v6, v3, 0x1

    if-ltz v3, :cond_4

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p0, p1, v8, v0, v1}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    :cond_4
    int-to-float v8, v3

    cmpl-float v8, v7, v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    if-ge v6, v8, :cond_0

    invoke-virtual {p0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p0, p1, v8, v0, v1}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method enableChildrenCache(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x1

    if-le p1, p2, :cond_0

    move v3, p1

    move p1, p2

    move p2, v3

    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v4, 0x0

    invoke-static {p1, v4}, Ljava/lang/Math;->max(II)I

    move-result p1

    add-int/lit8 v4, v0, -0x1

    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    move-result p2

    move v1, p1

    :goto_0
    if-gt v1, p2, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public enlargeTouchRegion()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConfigurationSwitchPanel:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->enlargeTouchRegion()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mProfileSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->enlargeTouchRegion()V

    return-void
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, p1

    :goto_0
    if-ne v2, v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->focusableViewAvailable(Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    if-eq v2, p0, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    goto :goto_0
.end method

.method public getCurrentScreen()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    return v0
.end method

.method public getIndicatorCount()I
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getScreenForView(Landroid/view/View;)I
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v2, -0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-ne v3, v4, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final getStatusBarService()Lcom/android/systemui/statusbar/BaseStatusBar;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mStatusBarService:Lcom/android/systemui/statusbar/BaseStatusBar;

    return-object v0
.end method

.method public hideSimSwithPanel()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->setSimSwitchPanleVisibility(Z)V

    iput v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDefaultScreen:I

    invoke-virtual {p0, v1, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->setCurrentScreen(II)V

    return-void
.end method

.method isDefaultScreenShowing()Z
    .locals 2

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    iget v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDefaultScreen:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusBarExpanded()Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mStatusBarService:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/BaseStatusBar;->isExpanded()Z

    move-result v0

    return v0
.end method

.method public moveToCurrentScreen(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0, v0, p1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->setCurrentScreen(II)V

    return-void
.end method

.method public moveToDefaultScreen(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDefaultScreen:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(I)V

    :goto_0
    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDefaultScreen:I

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void

    :cond_0
    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDefaultScreen:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->setCurrentScreen(II)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ToolBarView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$2;-><init>(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V

    const-wide/16 v1, 0xc8

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->computeScroll()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ToolBarView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$1;-><init>(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V

    const-wide/16 v1, 0xc8

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    const v0, 0x7f0c00d6

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    const v0, 0x7f0c00d8

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConfigurationSwitchPanel:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    const v0, 0x7f0c00c9

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    const v0, 0x7f0c00c8

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    iput-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mProfileSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mProfileSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->setToolBar(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->setToolBar(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;->setToolBar(Lcom/android/systemui/statusbar/toolbar/ToolBarView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConfigurationSwitchPanel:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->buildIconViews()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->buildIconViews()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mProfileSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->buildProfileIconViews()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mIsUpdated:Z

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1    # Landroid/view/MotionEvent;

    const/4 v12, 0x1

    const/4 v11, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v10, 0x2

    if-ne v0, v10, :cond_1

    iget v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    if-eqz v10, :cond_1

    :cond_0
    :goto_0
    return v12

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V

    and-int/lit16 v10, v0, 0xff

    packed-switch v10, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    iget v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    if-nez v10, :cond_0

    move v12, v11

    goto :goto_0

    :pswitch_1
    iget v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    iget v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionX:F

    sub-float v10, v4, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    float-to-int v5, v10

    iget v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionY:F

    sub-float v10, v7, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    float-to-int v8, v10

    iget v3, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchSlop:I

    if-le v5, v3, :cond_5

    move v6, v12

    :goto_2
    if-le v8, v3, :cond_6

    move v9, v12

    :goto_3
    if-nez v6, :cond_3

    if-eqz v9, :cond_2

    :cond_3
    if-eqz v6, :cond_4

    iput v12, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    iget v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    add-int/lit8 v10, v10, -0x1

    iget v13, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {p0, v10, v13}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->enableChildrenCache(II)V

    :cond_4
    iget-boolean v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mAllowLongPress:Z

    if-eqz v10, :cond_2

    iput-boolean v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mAllowLongPress:Z

    iget v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0, v10}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->cancelLongPress()V

    goto :goto_1

    :cond_5
    move v6, v11

    goto :goto_2

    :cond_6
    move v9, v11

    goto :goto_3

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v4, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionX:F

    iput v7, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionY:F

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v10

    iput v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    iput-boolean v12, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mAllowLongPress:Z

    iget-object v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v10}, Landroid/widget/Scroller;->isFinished()Z

    move-result v10

    if-eqz v10, :cond_7

    move v10, v11

    :goto_4
    iput v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    goto :goto_1

    :cond_7
    move v10, v12

    goto :goto_4

    :pswitch_3
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->clearChildrenCache()V

    iput v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    const/4 v10, -0x1

    iput v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    iput-boolean v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mAllowLongPress:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->releaseVelocityTracker()V

    goto/16 :goto_1

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    invoke-virtual {p0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    const/4 v5, 0x0

    add-int v6, v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v1, v2

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Landroid/view/View;->measure(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v4, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mFirstLayout:Z

    if-eqz v4, :cond_1

    invoke-virtual {p0, v5}, Landroid/view/View;->setHorizontalScrollBarEnabled(Z)V

    iget v4, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    mul-int/2addr v4, v2

    invoke-virtual {p0, v4, v5}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->scrollTo(II)V

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Landroid/view/View;->setHorizontalScrollBarEnabled(Z)V

    iput-boolean v5, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mFirstLayout:Z

    :cond_1
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/ToolBarView$SavedState;

    invoke-virtual {v0}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v1, v0, Lcom/android/systemui/statusbar/toolbar/ToolBarView$SavedState;->currentScreen:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, v0, Lcom/android/systemui/statusbar/toolbar/ToolBarView$SavedState;->currentScreen:I

    iput v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/android/systemui/statusbar/toolbar/ToolBarView$SavedState;

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    iput v1, v0, Lcom/android/systemui/statusbar/toolbar/ToolBarView$SavedState;->currentScreen:I

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v11, v0, 0xff

    packed-switch v11, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v11, 0x1

    return v11

    :pswitch_1
    iget-object v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v11}, Landroid/widget/Scroller;->isFinished()Z

    move-result v11

    if-nez v11, :cond_1

    iget-object v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v11}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    iput v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionX:F

    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v11

    iput v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    add-int/lit8 v11, v11, -0x1

    iget v12, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    add-int/lit8 v12, v12, 0x1

    invoke-virtual {p0, v11, v12}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->enableChildrenCache(II)V

    goto :goto_0

    :pswitch_2
    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionX:F

    sub-float v3, v11, v10

    iput v10, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mLastMotionX:F

    const/4 v11, 0x0

    cmpg-float v11, v3, v11

    if-gez v11, :cond_2

    iget v11, p0, Landroid/view/View;->mScrollX:I

    iget v12, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mOverscrollDistance:I

    add-int v1, v11, v12

    if-lez v1, :cond_0

    neg-int v11, v1

    int-to-float v11, v11

    invoke-static {v11, v3}, Ljava/lang/Math;->max(FF)F

    move-result v11

    float-to-int v11, v11

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Landroid/view/View;->scrollBy(II)V

    goto :goto_0

    :cond_2
    const/4 v11, 0x0

    cmpl-float v11, v3, v11

    if-lez v11, :cond_3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {p0, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    iget v12, p0, Landroid/view/View;->mScrollX:I

    sub-int/2addr v11, v12

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v12

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mOverscrollDistance:I

    add-int v1, v11, v12

    if-lez v1, :cond_0

    int-to-float v11, v1

    invoke-static {v11, v3}, Ljava/lang/Math;->min(FF)F

    move-result v11

    float-to-int v11, v11

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Landroid/view/View;->scrollBy(II)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->awakenScrollBars()Z

    goto/16 :goto_0

    :pswitch_3
    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_4

    iget-object v7, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v11, 0x3e8

    iget v12, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mMaximumVelocity:I

    int-to-float v12, v12

    invoke-virtual {v7, v11, v12}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    invoke-virtual {v7, v11}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v11

    float-to-int v8, v11

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    iget v11, p0, Landroid/view/View;->mScrollX:I

    div-int/lit8 v12, v5, 0x2

    add-int/2addr v11, v12

    div-int v9, v11, v5

    iget v11, p0, Landroid/view/View;->mScrollX:I

    int-to-float v11, v11

    int-to-float v12, v5

    div-float v6, v11, v12

    const/16 v11, 0x258

    if-le v8, v11, :cond_6

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    if-lez v11, :cond_6

    int-to-float v11, v9

    cmpg-float v11, v6, v11

    if-gez v11, :cond_5

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    add-int/lit8 v2, v11, -0x1

    :goto_1
    invoke-static {v9, v2}, Ljava/lang/Math;->min(II)I

    move-result v11

    const/4 v12, 0x1

    invoke-direct {p0, v11, v8, v12}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(IIZ)V

    :cond_4
    :goto_2
    const/4 v11, 0x0

    iput v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    const/4 v11, -0x1

    iput v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->releaseVelocityTracker()V

    goto/16 :goto_0

    :cond_5
    iget v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    goto :goto_1

    :cond_6
    const/16 v11, -0x258

    if-ge v8, v11, :cond_8

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ge v11, v12, :cond_8

    int-to-float v11, v9

    cmpl-float v11, v6, v11

    if-lez v11, :cond_7

    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    add-int/lit8 v2, v11, 0x1

    :goto_3
    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v11

    const/4 v12, 0x1

    invoke-direct {p0, v11, v8, v12}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(IIZ)V

    goto :goto_2

    :cond_7
    iget v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    goto :goto_3

    :cond_8
    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-direct {p0, v9, v11, v12}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(IIZ)V

    goto :goto_2

    :pswitch_4
    iget v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_9

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    iget v11, p0, Landroid/view/View;->mScrollX:I

    div-int/lit8 v12, v5, 0x2

    add-int/2addr v11, v12

    div-int v9, v11, v5

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-direct {p0, v9, v11, v12}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(IIZ)V

    :cond_9
    const/4 v11, 0x0

    iput v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mTouchState:I

    const/4 v11, -0x1

    iput v11, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mActivePointerId:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->releaseVelocityTracker()V

    goto/16 :goto_0

    :pswitch_5
    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public scrollLeft()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(I)V

    goto :goto_0
.end method

.method public scrollRight()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mNextScreen:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(I)V

    goto :goto_0
.end method

.method public scrollTo(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/view/View;->scrollTo(II)V

    return-void
.end method

.method public setAllowLongPress(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mAllowLongPress:Z

    return-void
.end method

.method public setCurrentScreen(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollToScreenCallback:Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollToScreenCallback:Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;

    iget v2, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    invoke-interface {v1, v2}, Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;->onScrollFinish(I)V

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDisplay:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v0

    if-eqz p2, :cond_2

    move v0, p2

    :cond_2
    const-string v1, "Width"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "width is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mCurrentScreen:I

    mul-int/2addr v1, v0

    invoke-virtual {p0, v1, v4}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->scrollTo(II)V

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public setScrollToScreenCallback(Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mScrollToScreenCallback:Lcom/android/systemui/statusbar/toolbar/ToolBarView$ScrollToScreenCallback;

    return-void
.end method

.method setSimSwitchPanleVisibility(Z)V
    .locals 4
    .param p1    # Z

    const v3, 0x7f0c00d6

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    invoke-virtual {p0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;->setPanelShowing(Z)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->updateIndicator()V

    return-void

    :cond_1
    if-nez p1, :cond_0

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;->setPanelShowing(Z)V

    goto :goto_0
.end method

.method public setStatusBarService(Lcom/android/systemui/statusbar/BaseStatusBar;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/BaseStatusBar;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mStatusBarService:Lcom/android/systemui/statusbar/BaseStatusBar;

    return-void
.end method

.method public setToolBarIndicator(Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;)V
    .locals 0
    .param p1    # Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;

    iput-object p1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mToolBarIndicator:Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->updateIndicator()V

    return-void
.end method

.method public showSimSwithPanel(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;->updateSimService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->setSimSwitchPanleVisibility(Z)V

    :goto_0
    iput v1, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mDefaultScreen:I

    invoke-virtual {p0, v1, v1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->setCurrentScreen(II)V

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->setSimSwitchPanleVisibility(Z)V

    goto :goto_0
.end method

.method public snapToScreen(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->snapToScreen(IIZ)V

    return-void
.end method

.method public updateIndicator()V
    .locals 3

    const-string v0, "ToolBarView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateIndicator called, and indicator count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mToolBarIndicator:Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->getIndicatorCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;->setCount(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mToolBarIndicator:Lcom/android/systemui/statusbar/toolbar/ToolBarIndicator;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public updateResources()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mIsUpdated:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;->updateResources()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConfigurationSwitchPanel:Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConfigurationSwitchPanel;->updateResources()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->updateResources()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mProfileSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/ProfileSwitchPanel;->updateResources()V

    goto :goto_0
.end method

.method public updateSimInfos(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mConnectionSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/toolbar/ConnectionSwitchPanel;->updateSimInfo(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/toolbar/ToolBarView;->mSimSwitchPanelView:Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/toolbar/SimSwitchPanel;->updateSimInfo()V

    return-void
.end method
