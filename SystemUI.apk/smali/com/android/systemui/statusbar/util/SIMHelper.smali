.class public Lcom/android/systemui/statusbar/util/SIMHelper;
.super Ljava/lang/Object;
.source "SIMHelper.java"


# static fields
.field private static final MOBILE_ICON_COUNT:I = 0x4

.field private static final SIM_STATUS_COUNT:I = 0x9

.field public static final TAG:Ljava/lang/String; = "SIMHelper"

.field private static iTelephony:Lcom/android/internal/telephony/ITelephony;

.field private static mBaseband:Ljava/lang/String;

.field private static mIsOptr:Ljava/lang/String;

.field private static mMobileIconResIds:[I

.field private static mSimInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static mSimStatusViews:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mIsOptr:Ljava/lang/String;

    sput-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mBaseband:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkSimCardDataConnBySlotId(Landroid/content/Context;I)Z
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v3

    iget v4, v0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-virtual {v3, v4}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimIndicatorStateGemini(I)I

    move-result v1

    const/4 v3, 0x6

    if-eq v1, v3, :cond_2

    const/4 v3, 0x7

    if-eq v1, v3, :cond_2

    const/16 v3, 0x8

    if-eq v1, v3, :cond_2

    const/4 v3, 0x5

    if-ne v1, v3, :cond_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static getDataConnectionIconIdBySlotId(Landroid/content/Context;I)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v1, -0x1

    invoke-static {p0, p1}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-object v2, Lcom/android/systemui/statusbar/util/SIMHelper;->mMobileIconResIds:[I

    if-nez v2, :cond_2

    invoke-static {}, Lcom/android/systemui/statusbar/util/SIMHelper;->initMobileIcons()V

    :cond_2
    iget v2, v0, Landroid/provider/Telephony$SIMInfo;->mColor:I

    if-eq v2, v1, :cond_0

    sget-object v1, Lcom/android/systemui/statusbar/util/SIMHelper;->mMobileIconResIds:[I

    iget v2, v0, Landroid/provider/Telephony$SIMInfo;->mColor:I

    aget v1, v1, v2

    goto :goto_0
.end method

.method public static getDefaultSIM(Landroid/content/Context;Ljava/lang/String;)J
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-wide/16 v1, -0x1

    invoke-static {v0, p1, v1, v2}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getSIMColorIdBySlot(Landroid/content/Context;I)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-static {p0, p1}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Landroid/provider/Telephony$SIMInfo;->mColor:I

    goto :goto_0
.end method

.method public static getSIMIdBySlot(Landroid/content/Context;I)J
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-static {p0, p1}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    :cond_0
    iget-wide v1, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    goto :goto_0
.end method

.method public static getSIMInfo(Landroid/content/Context;J)Landroid/provider/Telephony$SIMInfo;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # J

    sget-object v2, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfoList(Landroid/content/Context;)Ljava/util/List;

    :cond_1
    sget-object v2, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget-wide v2, v1, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_2

    :goto_0
    return-object v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v2, 0x0

    sget-object v3, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMInfoList(Landroid/content/Context;)Ljava/util/List;

    :cond_1
    sget-object v3, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    if-nez v3, :cond_2

    move-object v1, v2

    :goto_0
    return-object v1

    :cond_2
    sget-object v3, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget v3, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ne v3, p1, :cond_3

    goto :goto_0

    :cond_4
    move-object v1, v2

    goto :goto_0
.end method

.method public static getSIMInfoList(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p0}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSortedSIMInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    :cond_1
    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    return-object v0
.end method

.method public static getSIMStateIcon(I)I
    .locals 2
    .param p0    # I

    const/4 v0, -0x1

    if-le p0, v0, :cond_0

    const/16 v1, 0x9

    if-lt p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/systemui/statusbar/util/SIMHelper;->initStatusIcons()V

    :cond_2
    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    aget v0, v0, p0

    goto :goto_0
.end method

.method public static getSIMStateIcon(Landroid/provider/Telephony$SIMInfo;)I
    .locals 2
    .param p0    # Landroid/provider/Telephony$SIMInfo;

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v0

    iget v1, p0, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-virtual {v0, v1}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimIndicatorStateGemini(I)I

    move-result v0

    invoke-static {v0}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSIMStateIcon(I)I

    move-result v0

    return v0
.end method

.method private static getSortedSIMInfoList(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget v4, v3, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget v3, v3, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-le v4, v3, :cond_0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/Telephony$SIMInfo;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public static initMobileIcons()V
    .locals 3

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mMobileIconResIds:[I

    if-nez v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [I

    sput-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mMobileIconResIds:[I

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mMobileIconResIds:[I

    const/4 v1, 0x0

    const v2, 0x7f020135

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mMobileIconResIds:[I

    const/4 v1, 0x1

    const v2, 0x7f02013a

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mMobileIconResIds:[I

    const/4 v1, 0x2

    const v2, 0x7f020138

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mMobileIconResIds:[I

    const/4 v1, 0x3

    const v2, 0x7f02013b

    aput v2, v0, v1

    :cond_0
    return-void
.end method

.method public static initStatusIcons()V
    .locals 3

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    if-nez v0, :cond_0

    const/16 v0, 0x9

    new-array v0, v0, [I

    sput-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    const/4 v1, 0x1

    const v2, 0x20200f7

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    const/4 v1, 0x2

    const v2, 0x20200e4

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    const/4 v1, 0x3

    const v2, 0x20200e1

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    const/4 v1, 0x4

    const v2, 0x20200fe

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    const/4 v1, 0x6

    const v2, 0x20200fc

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    const/4 v1, 0x7

    const v2, 0x20200dc

    aput v2, v0, v1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimStatusViews:[I

    const/16 v1, 0x8

    const v2, 0x20200fd

    aput v2, v0, v1

    :cond_0
    return-void
.end method

.method public static is3GSupported()Z
    .locals 2

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mBaseband:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "gsm.baseband.capability"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mBaseband:Ljava/lang/String;

    :cond_0
    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mBaseband:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mBaseband:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mBaseband:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static setDefaultSIM(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method public static updateSIMInfos(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    invoke-static {p0}, Lcom/android/systemui/statusbar/util/SIMHelper;->getSortedSIMInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/util/SIMHelper;->mSimInfos:Ljava/util/List;

    return-void
.end method
