.class public Lcom/mediatek/systemui/ext/DefaultStatusBarPlugin;
.super Landroid/content/ContextWrapper;
.source "DefaultStatusBarPlugin.java"

# interfaces
.implements Lcom/mediatek/systemui/ext/IStatusBarPlugin;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public get3gDisabledWarningString()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDataActivityIconList()[I
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDataNetworkTypeIconGemini(Lcom/mediatek/systemui/ext/NetworkType;I)I
    .locals 1
    .param p1    # Lcom/mediatek/systemui/ext/NetworkType;
    .param p2    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getDataTypeIconListGemini(ZLcom/mediatek/systemui/ext/DataType;)[I
    .locals 1
    .param p1    # Z
    .param p2    # Lcom/mediatek/systemui/ext/DataType;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPluginResources()Landroid/content/res/Resources;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSignalIndicatorIconGemini(I)I
    .locals 1
    .param p1    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getSignalStrengthDescription(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSignalStrengthIcon(ZII)I
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getSignalStrengthIconGemini(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getSignalStrengthIconGemini(III)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getSignalStrengthNullIconGemini(I)I
    .locals 1
    .param p1    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getSignalStrengthSearchingIconGemini(I)I
    .locals 1
    .param p1    # I

    const/4 v0, -0x1

    return v0
.end method

.method public isHspaDataDistinguishable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public supportDataTypeAlwaysDisplayWhileOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public supportDisableWifiAtAirplaneMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
