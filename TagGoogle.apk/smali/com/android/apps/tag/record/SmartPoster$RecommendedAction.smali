.class final enum Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
.super Ljava/lang/Enum;
.source "SmartPoster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/apps/tag/record/SmartPoster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RecommendedAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

.field public static final enum DO_ACTION:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

.field private static final LOOKUP:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/Byte;",
            "Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum OPEN_FOR_EDITING:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

.field public static final enum SAVE_FOR_LATER:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

.field public static final enum UNKNOWN:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;


# instance fields
.field private final mAction:B


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    const-string v6, "UNKNOWN"

    const/4 v7, -0x1

    invoke-direct {v5, v6, v8, v7}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;-><init>(Ljava/lang/String;IB)V

    sput-object v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->UNKNOWN:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    new-instance v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    const-string v6, "DO_ACTION"

    invoke-direct {v5, v6, v9, v8}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;-><init>(Ljava/lang/String;IB)V

    sput-object v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->DO_ACTION:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    new-instance v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    const-string v6, "SAVE_FOR_LATER"

    invoke-direct {v5, v6, v10, v9}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;-><init>(Ljava/lang/String;IB)V

    sput-object v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->SAVE_FOR_LATER:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    new-instance v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    const-string v6, "OPEN_FOR_EDITING"

    invoke-direct {v5, v6, v11, v10}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;-><init>(Ljava/lang/String;IB)V

    sput-object v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->OPEN_FOR_EDITING:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    const/4 v5, 0x4

    new-array v5, v5, [Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    sget-object v6, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->UNKNOWN:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    aput-object v6, v5, v8

    sget-object v6, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->DO_ACTION:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    aput-object v6, v5, v9

    sget-object v6, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->SAVE_FOR_LATER:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    aput-object v6, v5, v10

    sget-object v6, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->OPEN_FOR_EDITING:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    aput-object v6, v5, v11

    sput-object v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->$VALUES:[Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v2

    invoke-static {}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->values()[Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    move-result-object v1

    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v1, v3

    invoke-direct {v0}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->getByte()B

    move-result v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v2, v5, v0}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v5

    sput-object v5, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->LOOKUP:Lcom/google/common/collect/ImmutableMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .param p3    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-byte p3, p0, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->mAction:B

    return-void
.end method

.method static synthetic access$000()Lcom/google/common/collect/ImmutableMap;
    .locals 1

    sget-object v0, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->LOOKUP:Lcom/google/common/collect/ImmutableMap;

    return-object v0
.end method

.method private getByte()B
    .locals 1

    iget-byte v0, p0, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->mAction:B

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    .locals 1

    const-class v0, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    return-object v0
.end method

.method public static values()[Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    .locals 1

    sget-object v0, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->$VALUES:[Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    invoke-virtual {v0}, [Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    return-object v0
.end method
