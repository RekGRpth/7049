.class public Lcom/android/apps/tag/record/SmartPoster;
.super Lcom/android/apps/tag/record/ParsedNdefRecord;
.source "SmartPoster.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    }
.end annotation


# static fields
.field private static final ACTION_RECORD_TYPE:[B

.field private static final TYPE_TYPE:[B


# instance fields
.field private final mAction:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

.field private final mImageRecord:Lcom/android/apps/tag/record/ImageRecord;

.field private final mTitleRecord:Lcom/android/apps/tag/record/TextRecord;

.field private final mType:Ljava/lang/String;

.field private final mUriRecord:Lcom/android/apps/tag/record/UriRecord;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/apps/tag/record/SmartPoster;->ACTION_RECORD_TYPE:[B

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x74

    aput-byte v2, v0, v1

    sput-object v0, Lcom/android/apps/tag/record/SmartPoster;->TYPE_TYPE:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x61t
        0x63t
        0x74t
    .end array-data
.end method

.method private constructor <init>(Lcom/android/apps/tag/record/UriRecord;Lcom/android/apps/tag/record/TextRecord;Lcom/android/apps/tag/record/ImageRecord;Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/android/apps/tag/record/UriRecord;
    .param p2    # Lcom/android/apps/tag/record/TextRecord;
    .param p3    # Lcom/android/apps/tag/record/ImageRecord;
    .param p4    # Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/apps/tag/record/ParsedNdefRecord;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/apps/tag/record/UriRecord;

    iput-object v0, p0, Lcom/android/apps/tag/record/SmartPoster;->mUriRecord:Lcom/android/apps/tag/record/UriRecord;

    iput-object p2, p0, Lcom/android/apps/tag/record/SmartPoster;->mTitleRecord:Lcom/android/apps/tag/record/TextRecord;

    iput-object p3, p0, Lcom/android/apps/tag/record/SmartPoster;->mImageRecord:Lcom/android/apps/tag/record/ImageRecord;

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    iput-object v0, p0, Lcom/android/apps/tag/record/SmartPoster;->mAction:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    iput-object p5, p0, Lcom/android/apps/tag/record/SmartPoster;->mType:Ljava/lang/String;

    return-void
.end method

.method private static getByType([B[Landroid/nfc/NdefRecord;)Landroid/nfc/NdefRecord;
    .locals 5
    .param p0    # [B
    .param p1    # [Landroid/nfc/NdefRecord;

    move-object v0, p1

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {v3}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v4

    invoke-static {p0, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getFirstIfExists(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<*>;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0}, Lcom/google/common/collect/Iterables;->isEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/common/collect/Iterables;->get(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public static isPoster(Landroid/nfc/NdefRecord;)Z
    .locals 2
    .param p0    # Landroid/nfc/NdefRecord;

    :try_start_0
    invoke-static {p0}, Lcom/android/apps/tag/record/SmartPoster;->parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse(Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;
    .locals 4
    .param p0    # Landroid/nfc/NdefRecord;

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getTnf()S

    move-result v3

    if-ne v3, v2, :cond_0

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getType()[B

    move-result-object v2

    sget-object v3, Landroid/nfc/NdefRecord;->RTD_SMART_POSTER:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    :try_start_0
    new-instance v1, Landroid/nfc/NdefMessage;

    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/nfc/NdefMessage;-><init>([B)V

    invoke-virtual {v1}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v2

    invoke-static {v2}, Lcom/android/apps/tag/record/SmartPoster;->parse([Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;
    :try_end_0
    .catch Landroid/nfc/FormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static parse([Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster;
    .locals 6

    :try_start_0
    invoke-static {p0}, Lcom/android/apps/tag/message/NdefMessageParser;->getRecords([Landroid/nfc/NdefRecord;)Ljava/util/List;

    move-result-object v0

    const-class v1, Lcom/android/apps/tag/record/UriRecord;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/Iterables;->getOnlyElement(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/apps/tag/record/UriRecord;

    const-class v2, Lcom/android/apps/tag/record/TextRecord;

    invoke-static {v0, v2}, Lcom/android/apps/tag/record/SmartPoster;->getFirstIfExists(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/apps/tag/record/TextRecord;

    const-class v3, Lcom/android/apps/tag/record/ImageRecord;

    invoke-static {v0, v3}, Lcom/android/apps/tag/record/SmartPoster;->getFirstIfExists(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/apps/tag/record/ImageRecord;

    invoke-static {p0}, Lcom/android/apps/tag/record/SmartPoster;->parseRecommendedAction([Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    move-result-object v4

    invoke-static {p0}, Lcom/android/apps/tag/record/SmartPoster;->parseType([Landroid/nfc/NdefRecord;)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/android/apps/tag/record/SmartPoster;

    invoke-direct/range {v0 .. v5}, Lcom/android/apps/tag/record/SmartPoster;-><init>(Lcom/android/apps/tag/record/UriRecord;Lcom/android/apps/tag/record/TextRecord;Lcom/android/apps/tag/record/ImageRecord;Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static parseRecommendedAction([Landroid/nfc/NdefRecord;)Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;
    .locals 4
    .param p0    # [Landroid/nfc/NdefRecord;

    sget-object v2, Lcom/android/apps/tag/record/SmartPoster;->ACTION_RECORD_TYPE:[B

    invoke-static {v2, p0}, Lcom/android/apps/tag/record/SmartPoster;->getByType([B[Landroid/nfc/NdefRecord;)Landroid/nfc/NdefRecord;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v2, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->UNKNOWN:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v1}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    const/4 v3, 0x0

    aget-byte v0, v2, v3

    # getter for: Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->LOOKUP:Lcom/google/common/collect/ImmutableMap;
    invoke-static {}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->access$000()Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/common/collect/ImmutableMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    # getter for: Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->LOOKUP:Lcom/google/common/collect/ImmutableMap;
    invoke-static {}, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->access$000()Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;->UNKNOWN:Lcom/android/apps/tag/record/SmartPoster$RecommendedAction;

    goto :goto_0
.end method

.method private static parseType([Landroid/nfc/NdefRecord;)Ljava/lang/String;
    .locals 4
    .param p0    # [Landroid/nfc/NdefRecord;

    sget-object v1, Lcom/android/apps/tag/record/SmartPoster;->TYPE_TYPE:[B

    invoke-static {v1, p0}, Lcom/android/apps/tag/record/SmartPoster;->getByType([B[Landroid/nfc/NdefRecord;)Landroid/nfc/NdefRecord;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v2

    sget-object v3, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0
.end method


# virtual methods
.method public getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # I

    iget-object v1, p0, Lcom/android/apps/tag/record/SmartPoster;->mTitleRecord:Lcom/android/apps/tag/record/TextRecord;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/apps/tag/record/SmartPoster;->mTitleRecord:Lcom/android/apps/tag/record/TextRecord;

    invoke-virtual {v1, p1, p2, v0, p4}, Lcom/android/apps/tag/record/TextRecord;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const/high16 v1, 0x7f030000

    invoke-virtual {p2, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    iget-object v1, p0, Lcom/android/apps/tag/record/SmartPoster;->mUriRecord:Lcom/android/apps/tag/record/UriRecord;

    invoke-virtual {v1, p1, p2, v0, p4}, Lcom/android/apps/tag/record/UriRecord;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/apps/tag/record/SmartPoster;->mUriRecord:Lcom/android/apps/tag/record/UriRecord;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/android/apps/tag/record/UriRecord;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
