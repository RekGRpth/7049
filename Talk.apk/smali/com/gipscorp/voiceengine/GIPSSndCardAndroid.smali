.class Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;
.super Ljava/lang/Object;
.source "GIPSSndCardAndroid.java"


# instance fields
.field private _audioManager:Landroid/media/AudioManager;

.field private _audioRecord:Landroid/media/AudioRecord;

.field private _audioTrack:Landroid/media/AudioTrack;

.field private _bufferedPlaySamples:I

.field private _bufferedRecSamples:I

.field private _context:Landroid/content/Context;

.field private _doPlayInit:Z

.field private _doRecInit:Z

.field private _isPlaying:Z

.field private _isRecording:Z

.field private _playBuffer:Ljava/nio/ByteBuffer;

.field private final _playLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private _playPosition:I

.field private _recBuffer:Ljava/nio/ByteBuffer;

.field private final _recLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private _tempBufPlay:[B

.field private _tempBufRec:[B

.field final logTag:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/16 v3, 0x3c0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doPlayInit:Z

    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doRecInit:Z

    iput-boolean v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isRecording:Z

    iput-boolean v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isPlaying:Z

    iput v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedRecSamples:I

    iput v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedPlaySamples:I

    iput v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playPosition:I

    const-string v1, "GIPS Snd Card"

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->logTag:Ljava/lang/String;

    const/16 v1, 0x3c0

    :try_start_0
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playBuffer:Ljava/nio/ByteBuffer;

    const/16 v1, 0x3c0

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recBuffer:Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-array v1, v3, [B

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_tempBufPlay:[B

    new-array v1, v3, [B

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_tempBufRec:[B

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private GIPSLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "GIPS Snd Card"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private GIPSLogErr(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "GIPS Snd Card"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private GetPlayoutVolume()I
    .locals 3

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    :cond_0
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    :cond_1
    return v0
.end method

.method private InitPlayback(I)I
    .locals 12
    .param p1    # I

    const/4 v11, 0x1

    const/4 v9, -0x1

    const/4 v0, 0x2

    const/4 v10, 0x0

    invoke-static {p1, v0, v0}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v8

    move v5, v8

    const/16 v0, 0x1770

    if-ge v5, v0, :cond_0

    mul-int/lit8 v5, v5, 0x2

    :cond_0
    iput v10, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedPlaySamples:I

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    :cond_1
    :try_start_0
    new-instance v0, Landroid/media/AudioTrack;

    const/4 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x2

    const/4 v6, 0x1

    move v2, p1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    if-eq v0, v11, :cond_2

    move v0, v9

    :goto_0
    return v0

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLog(Ljava/lang/String;)V

    move v0, v9

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    :cond_3
    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_4

    move v0, v10

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v10}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    goto :goto_0
.end method

.method private InitRecording(II)I
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v8, -0x1

    const/4 v0, 0x2

    invoke-static {p2, v0, v0}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v7

    mul-int/lit8 v5, v7, 0x2

    mul-int/lit8 v0, p2, 0x5

    div-int/lit16 v0, v0, 0xc8

    iput v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedRecSamples:I

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    :cond_0
    :try_start_0
    new-instance v0, Landroid/media/AudioRecord;

    const/4 v3, 0x2

    const/4 v4, 0x2

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    move v0, v8

    :goto_0
    return v0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLog(Ljava/lang/String;)V

    move v0, v8

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedRecSamples:I

    goto :goto_0
.end method

.method private PlayAudio(I)I
    .locals 7
    .param p1    # I

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v4, -0x2

    :goto_0
    return v4

    :cond_0
    :try_start_1
    iget-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doPlayInit:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_1

    const/16 v4, -0x13

    :try_start_2
    invoke-static {v4}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    const/4 v4, 0x0

    :try_start_3
    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doPlayInit:Z

    :cond_1
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playBuffer:Ljava/nio/ByteBuffer;

    iget-object v5, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_tempBufPlay:[B

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    iget-object v5, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_tempBufPlay:[B

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, p1}, Landroid/media/AudioTrack;->write([BII)I

    move-result v3

    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedPlaySamples:I

    shr-int/lit8 v5, v3, 0x1

    add-int/2addr v4, v5

    iput v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedPlaySamples:I

    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v4}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v2

    iget v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playPosition:I

    if-ge v2, v4, :cond_2

    const/4 v4, 0x0

    iput v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playPosition:I

    :cond_2
    iget v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedPlaySamples:I

    iget v5, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playPosition:I

    sub-int v5, v2, v5

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedPlaySamples:I

    iput v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playPosition:I

    iget-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isRecording:Z

    if-nez v4, :cond_3

    iget v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedPlaySamples:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    if-eq v3, p1, :cond_4

    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v4, -0x1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Set play thread priority failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLog(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v4

    :cond_4
    iget-object v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    move v4, v0

    goto :goto_0
.end method

.method private RecordAudio(I)I
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v2, -0x2

    :goto_0
    return v2

    :cond_0
    :try_start_1
    iget-boolean v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doRecInit:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_1

    const/16 v2, -0x13

    :try_start_2
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    const/4 v2, 0x0

    :try_start_3
    iput-boolean v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doRecInit:Z

    :cond_1
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    iget-object v3, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_tempBufRec:[B

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p1}, Landroid/media/AudioRecord;->read([BII)I

    move-result v1

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recBuffer:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_tempBufRec:[B

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eq v1, p1, :cond_2

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v2, -0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Set rec thread priority failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLog(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RecordAudio try failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLogErr(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_2
    iget v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_bufferedPlaySamples:I

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    :cond_2
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_2
.end method

.method private SetAudioMode(Z)V
    .locals 3

    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    :cond_0
    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_2

    const-string v0, "Could not set audio mode - no audio manager"

    invoke-direct {p0, v0}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLogErr(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v2, "Samsung"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v2, "samsung"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_3
    const/16 v0, 0x8

    if-ne v0, v1, :cond_1

    if-eqz p1, :cond_4

    const/4 v0, 0x4

    :goto_1
    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->setMode(I)V

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    if-eq v1, v0, :cond_1

    const-string v0, "Could not set audio mode for Samsung device"

    invoke-direct {p0, v0}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLogErr(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private SetPlayoutSpeaker(Z)I
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    :cond_0
    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_1

    const-string v0, "Could not change audio routing - no audio manager"

    invoke-direct {p0, v0}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->GIPSLogErr(Ljava/lang/String;)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x3

    if-eq v2, v0, :cond_2

    const/4 v2, 0x4

    if-ne v2, v0, :cond_4

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->setMode(I)V

    goto :goto_1

    :cond_4
    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v3, "Samsung"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v3, "samsung"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_5
    const/4 v2, 0x5

    if-eq v2, v0, :cond_6

    const/4 v2, 0x6

    if-eq v2, v0, :cond_6

    const/4 v2, 0x7

    if-ne v2, v0, :cond_8

    :cond_6
    if-eqz p1, :cond_7

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->setMode(I)V

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_1
.end method

.method private SetPlayoutVolume(I)I
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_context:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    :cond_0
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3, p1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method private StartPlayback()I
    .locals 3

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isRecording:Z

    if-nez v1, :cond_0

    invoke-direct {p0, v2}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->SetAudioMode(Z)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->play()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    iput-boolean v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isPlaying:Z

    const/4 v1, 0x0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method private StartRecording()I
    .locals 3

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isPlaying:Z

    if-nez v1, :cond_0

    invoke-direct {p0, v2}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->SetAudioMode(Z)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v1}, Landroid/media/AudioRecord;->startRecording()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    iput-boolean v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isRecording:Z

    const/4 v1, 0x0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method private StopPlayback()I
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getPlayState()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->stop()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->flush()V

    :cond_0
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->release()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioTrack:Landroid/media/AudioTrack;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doPlayInit:Z

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    iget-boolean v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isRecording:Z

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->SetAudioMode(Z)V

    :cond_1
    iput-boolean v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isPlaying:Z

    :goto_0
    return v1

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doPlayInit:Z

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v1, -0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doPlayInit:Z

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_playLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method private StopRecording()I
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getRecordingState()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->stop()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->release()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_audioRecord:Landroid/media/AudioRecord;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doRecInit:Z

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    iget-boolean v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isPlaying:Z

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->SetAudioMode(Z)V

    :cond_1
    iput-boolean v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_isRecording:Z

    :goto_0
    return v1

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doRecInit:Z

    iget-object v1, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    const/4 v1, -0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    iput-boolean v4, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_doRecInit:Z

    iget-object v2, p0, Lcom/gipscorp/voiceengine/GIPSSndCardAndroid;->_recLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method
