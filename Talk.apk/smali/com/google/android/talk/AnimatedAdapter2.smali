.class public Lcom/google/android/talk/AnimatedAdapter2;
.super Landroid/database/DataSetObserver;
.source "AnimatedAdapter2.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/AnimatedAdapter2$1;,
        Lcom/google/android/talk/AnimatedAdapter2$Status;,
        Lcom/google/android/talk/AnimatedAdapter2$TooLongException;,
        Lcom/google/android/talk/AnimatedAdapter2$Diff;,
        Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;,
        Lcom/google/android/talk/AnimatedAdapter2$ViewCache;,
        Lcom/google/android/talk/AnimatedAdapter2$ListViewBindHelper;
    }
.end annotation


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field public mAddingViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimatedScale:F

.field private mAnimationTime:I

.field private mAnimator:Landroid/animation/Animator;

.field private mCurrentIds:[J

.field private final mDataSetObservable:Landroid/database/DataSetObservable;

.field public mDeletingViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;",
            ">;"
        }
    .end annotation
.end field

.field private mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

.field private mViewCache:Lcom/google/android/talk/AnimatedAdapter2$ViewCache;


# direct methods
.method public constructor <init>(Landroid/widget/BaseAdapter;I)V
    .locals 2
    .param p1    # Landroid/widget/BaseAdapter;
    .param p2    # I

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    const/16 v0, 0x78

    iput v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimationTime:I

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDataSetObservable:Landroid/database/DataSetObservable;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAddingViews:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDeletingViews:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/talk/AnimatedAdapter2$ViewCache;

    invoke-direct {v0, p0}, Lcom/google/android/talk/AnimatedAdapter2$ViewCache;-><init>(Lcom/google/android/talk/AnimatedAdapter2;)V

    iput-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mViewCache:Lcom/google/android/talk/AnimatedAdapter2$ViewCache;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->hasStableIds()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Adapters must have stable IDs to animate."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    iput p2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimationTime:I

    invoke-virtual {p1, p0}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/talk/AnimatedAdapter2;)F
    .locals 1
    .param p0    # Lcom/google/android/talk/AnimatedAdapter2;

    iget v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimatedScale:F

    return v0
.end method

.method private getAdapterView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 13
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # I

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mViewCache:Lcom/google/android/talk/AnimatedAdapter2$ViewCache;

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Lcom/google/android/talk/AnimatedAdapter2$ViewCache;->getScrap(I)Landroid/view/View;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    move-object/from16 v0, p3

    invoke-virtual {v1, p1, v10, v0}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    if-eqz v11, :cond_0

    check-cast v11, Landroid/view/ViewGroup;

    invoke-virtual {v11}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    const/4 v5, 0x0

    if-eqz p2, :cond_1

    move-object v7, p2

    check-cast v7, Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->removeAllViews()V

    move-object v5, v7

    :cond_1
    if-nez v5, :cond_2

    new-instance v5, Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v4, -0x1

    const/4 v6, -0x1

    invoke-direct {v1, v4, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v1, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v1, v12, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v1, v12, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p1}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mViewCache:Lcom/google/android/talk/AnimatedAdapter2$ViewCache;

    move v4, p1

    move/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/talk/AnimatedAdapter2$ViewCache;->put(JILandroid/view/View;I)V

    :cond_3
    return-object v5
.end method

.method private getAnimatingView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_4

    new-instance p2, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {p2, p0, v10}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;-><init>(Lcom/google/android/talk/AnimatedAdapter2;Landroid/content/Context;)V

    :goto_0
    move-object v6, p2

    check-cast v6, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;

    invoke-virtual {v6}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->removeAllViews()V

    const/4 v5, 0x0

    iget-object v10, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v10, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->getStatus(I)Lcom/google/android/talk/AnimatedAdapter2$Status;

    move-result-object v7

    sget-object v10, Lcom/google/android/talk/AnimatedAdapter2$Status;->Deleted:Lcom/google/android/talk/AnimatedAdapter2$Status;

    if-ne v7, v10, :cond_6

    iget-object v10, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDeletingViews:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    iget-object v10, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v10, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->getDeletingRowId(I)J

    move-result-wide v2

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/talk/AnimatedAdapter2;->mViewCache:Lcom/google/android/talk/AnimatedAdapter2$ViewCache;

    invoke-virtual {v10, v2, v3}, Lcom/google/android/talk/AnimatedAdapter2$ViewCache;->getView(J)Landroid/view/View;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    iget-object v10, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v10, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->translate(I)I

    move-result v8

    if-ltz v8, :cond_1

    invoke-direct {p0, p1, p3}, Lcom/google/android/talk/AnimatedAdapter2;->getInnerViewForPosition(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    instance-of v10, v1, Lcom/google/android/talk/AnimatedAdapter2$ListViewBindHelper;

    if-eqz v10, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/google/android/talk/AnimatedAdapter2$ListViewBindHelper;

    invoke-interface {v0}, Lcom/google/android/talk/AnimatedAdapter2$ListViewBindHelper;->rebindAlternate()V

    :cond_1
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    if-eqz v9, :cond_2

    check-cast v9, Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    invoke-virtual {v6, v1, v10}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, v1, p3}, Lcom/google/android/talk/AnimatedAdapter2;->measureChildHeight(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v5

    :goto_1
    const/4 v10, 0x1

    invoke-virtual {v6, v5, v10}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->setForcedHeight(IZ)V

    :cond_3
    :goto_2
    return-object p2

    :cond_4
    move-object v10, p2

    check-cast v10, Landroid/view/ViewGroup;

    invoke-virtual {v10}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_1

    :cond_6
    sget-object v10, Lcom/google/android/talk/AnimatedAdapter2$Status;->Added:Lcom/google/android/talk/AnimatedAdapter2$Status;

    if-ne v7, v10, :cond_3

    iget-object v10, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAddingViews:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1, p3}, Lcom/google/android/talk/AnimatedAdapter2;->getInnerViewForPosition(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    invoke-virtual {v6, v1, v10}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0, v1, p3}, Lcom/google/android/talk/AnimatedAdapter2;->measureChildHeight(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v4

    move v5, v4

    const/4 v10, 0x0

    invoke-virtual {v6, v5, v10}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->setForcedHeight(IZ)V

    goto :goto_2
.end method

.method private getInnerViewForPosition(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v3, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->translate(I)I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/talk/AnimatedAdapter2;->getItemViewType(I)I

    move-result v4

    invoke-direct {p0, v1, v3, p2, v4}, Lcom/google/android/talk/AnimatedAdapter2;->getAdapterView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_0
    return-object v0
.end method

.method private isAnimating(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v0, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->isAddedOrDeleted(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeAnimator()Landroid/animation/Animator;
    .locals 3

    const-string v1, "animatedScale"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimationTime:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-object v0

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method private measureChildHeight(Landroid/view/View;Landroid/view/ViewGroup;)I
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/ViewGroup;

    const/high16 v11, 0x40000000

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    const/4 v7, -0x1

    const/4 v6, -0x1

    if-eqz v5, :cond_0

    iget v7, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_0
    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    add-int v10, v3, v4

    invoke-static {v9, v10, v7}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    move v2, v6

    if-lez v2, :cond_1

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    return v9

    :cond_1
    const/4 v9, 0x0

    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method private translateItemPosition(I)I
    .locals 2
    .param p1    # I

    move v0, p1

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v1, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->translate(I)I

    move-result v0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :cond_0
    return v0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v0}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/talk/AnimatedAdapter2;->translateItemPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 6
    .param p1    # I

    const-wide/16 v2, 0x0

    move v1, p1

    iget-object v4, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v4, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->getStatus(I)Lcom/google/android/talk/AnimatedAdapter2$Status;

    move-result-object v4

    sget-object v5, Lcom/google/android/talk/AnimatedAdapter2$Status;->Deleted:Lcom/google/android/talk/AnimatedAdapter2$Status;

    if-ne v4, v5, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return-wide v2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v4, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->translate(I)I

    move-result v1

    if-ltz v1, :cond_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v2, v1}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v2

    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v0, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->isAddedOrDeleted(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/talk/AnimatedAdapter2;->translateItemPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    move v0, p1

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v1, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->isAddedOrDeleted(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/talk/AnimatedAdapter2;->getAnimatingView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v1, p1}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->translate(I)I

    move-result v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/talk/AnimatedAdapter2;->getItemViewType(I)I

    move-result v1

    invoke-direct {p0, v0, p2, p3, v1}, Lcom/google/android/talk/AnimatedAdapter2;->getAdapterView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/talk/AnimatedAdapter2;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/talk/AnimatedAdapter2;->isAnimating(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/talk/AnimatedAdapter2;->translateItemPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 7
    .param p1    # Landroid/animation/Animator;

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAddingViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;

    const-string v2, "talk"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "add view count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mLayoutCount:I
    invoke-static {v1}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->access$100(Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    # setter for: Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mLayoutCount:I
    invoke-static {v1, v5}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->access$102(Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;I)I

    :cond_0
    iget-object v2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDeletingViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;

    const-string v2, "talk"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "del view count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mLayoutCount:I
    invoke-static {v1}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->access$100(Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    # setter for: Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->mLayoutCount:I
    invoke-static {v1, v5}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->access$102(Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;I)I

    :cond_1
    iget-object v2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDeletingViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAddingViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iput-object v6, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimator:Landroid/animation/Animator;

    iput-object v6, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    iget-object v2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v2}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    invoke-virtual {p0, p1}, Lcom/google/android/talk/AnimatedAdapter2;->onAnimationCancel(Landroid/animation/Animator;)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onChanged()V
    .locals 18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/16 v1, 0x3e8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v14}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v14

    const/16 v15, 0x3e8

    if-le v14, v15, :cond_5

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    :cond_0
    if-eqz v9, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimator:Landroid/animation/Animator;

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v14}, Landroid/animation/Animator;->cancel()V

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimator:Landroid/animation/Animator;

    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    array-length v14, v14

    if-lez v14, :cond_2

    if-eqz v8, :cond_2

    array-length v14, v8

    if-lez v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    array-length v14, v14

    array-length v15, v8

    sub-int/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v14

    const/16 v15, 0x64

    if-ge v14, v15, :cond_2

    new-instance v4, Lcom/google/android/talk/AnimatedAdapter2$Diff;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v14, v8}, Lcom/google/android/talk/AnimatedAdapter2$Diff;-><init>(Lcom/google/android/talk/AnimatedAdapter2;[J[J)V

    const-wide/16 v14, 0x19

    invoke-virtual {v4, v14, v15}, Lcom/google/android/talk/AnimatedAdapter2$Diff;->compute(J)Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v14}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->getAddingCount()I

    move-result v14

    if-lez v14, :cond_9

    const/4 v3, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    invoke-virtual {v14}, Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;->getDeletionCount()I

    move-result v14

    if-lez v14, :cond_a

    const/4 v2, 0x1

    :goto_1
    const/high16 v14, 0x3f800000

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimatedScale:F

    if-nez v2, :cond_3

    if-eqz v3, :cond_b

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/talk/AnimatedAdapter2;->makeAnimator()Landroid/animation/Animator;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimator:Landroid/animation/Animator;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v14}, Landroid/animation/Animator;->start()V

    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-string v14, "talk"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ">>>>> total overhead: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v12, v10

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v14}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void

    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v14}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v14

    new-array v8, v14, [J

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    if-nez v14, :cond_6

    const/4 v9, 0x0

    const/4 v5, 0x0

    :goto_3
    array-length v14, v8

    if-ge v5, v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v14, v5}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v14

    aput-wide v14, v8, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    array-length v14, v14

    array-length v15, v8

    if-eq v14, v15, :cond_7

    const/4 v9, 0x1

    const/4 v5, 0x0

    :goto_4
    array-length v14, v8

    if-ge v5, v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v14, v5}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v14

    aput-wide v14, v8, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_7
    const/4 v5, 0x0

    :goto_5
    array-length v14, v8

    if-ge v5, v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v14, v5}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v6

    aput-wide v6, v8, v5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mCurrentIds:[J

    aget-wide v14, v14, v5

    cmp-long v14, v6, v14

    if-eqz v14, :cond_8

    const/4 v9, 0x1

    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_b
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    goto/16 :goto_2
.end method

.method public onInvalidated()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mMapping:Lcom/google/android/talk/AnimatedAdapter2$Diff$Mapping;

    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    instance-of v0, v0, Landroid/widget/AbsListView$RecyclerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAdapter:Landroid/widget/BaseAdapter;

    check-cast v0, Landroid/widget/AbsListView$RecyclerListener;

    invoke-interface {v0, p1}, Landroid/widget/AbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mViewCache:Lcom/google/android/talk/AnimatedAdapter2$ViewCache;

    invoke-virtual {v0, p1}, Lcom/google/android/talk/AnimatedAdapter2$ViewCache;->onMovedToScrapHeap(Landroid/view/View;)V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public setAnimatedScale(F)V
    .locals 3
    .param p1    # F

    iput p1, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAnimatedScale:F

    iget-object v2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDeletingViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;

    invoke-virtual {v1}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->requestLayout()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/talk/AnimatedAdapter2;->mAddingViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;

    invoke-virtual {v1}, Lcom/google/android/talk/AnimatedAdapter2$AnimatingItemView;->requestLayout()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/talk/AnimatedAdapter2;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method
