.class public Lcom/google/android/talk/AnimatedBackgroundImageButton;
.super Landroid/widget/ImageButton;
.source "AnimatedBackgroundImageButton.java"


# instance fields
.field private mAnimationStarted:Z

.field private mCurrentAnimator:Landroid/animation/Animator;

.field protected mDurationMs:I

.field protected mEndAlpha:I

.field protected mStartAlpha:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mAnimationStarted:Z

    iput v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mStartAlpha:I

    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mEndAlpha:I

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mDurationMs:I

    return-void
.end method

.method private resetAnimator()V
    .locals 7

    const/4 v6, 0x2

    const-string v2, "ImageAlpha"

    new-array v3, v6, [I

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mStartAlpha:I

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mEndAlpha:I

    aput v5, v3, v4

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget v2, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mDurationMs:I

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    invoke-virtual {v0, v6}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    iget-boolean v1, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mAnimationStarted:Z

    iget-object v2, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mCurrentAnimator:Landroid/animation/Animator;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/AnimatedBackgroundImageButton;->stopImageAnimation()V

    :cond_0
    iput-object v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mCurrentAnimator:Landroid/animation/Animator;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/talk/AnimatedBackgroundImageButton;->startImageAnimation()V

    :cond_1
    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mCurrentAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    invoke-super {p0}, Landroid/widget/ImageButton;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/ImageButton;->onFinishInflate()V

    invoke-direct {p0}, Lcom/google/android/talk/AnimatedBackgroundImageButton;->resetAnimator()V

    return-void
.end method

.method public setImageAlpha(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/talk/AnimatedBackgroundImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    return-void
.end method

.method public startImageAnimation()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mAnimationStarted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mCurrentAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mAnimationStarted:Z

    :cond_0
    return-void
.end method

.method public stopImageAnimation()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mAnimationStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mCurrentAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/AnimatedBackgroundImageButton;->mAnimationStarted:Z

    :cond_0
    return-void
.end method
