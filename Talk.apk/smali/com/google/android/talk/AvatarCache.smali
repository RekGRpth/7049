.class public Lcom/google/android/talk/AvatarCache;
.super Ljava/lang/Object;
.source "AvatarCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/AvatarCache$WorkItem;,
        Lcom/google/android/talk/AvatarCache$BitmapCache;
    }
.end annotation


# static fields
.field private static volatile sDone:Z

.field private static sInstances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/talk/AvatarCache;",
            ">;"
        }
    .end annotation
.end field

.field private static final sInstancesLock:Ljava/lang/Object;

.field private static sQueryThread:Ljava/lang/Thread;

.field private static final sQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/talk/AvatarCache$WorkItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccountId:J

.field private mAvatarUri:Landroid/net/Uri;

.field private final mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

.field private mNoAvatarList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/talk/AvatarCache;->sInstancesLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/talk/AvatarCache;->sQueue:Ljava/util/ArrayList;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/talk/AvatarCache;->sDone:Z

    return-void
.end method

.method private constructor <init>(J)V
    .locals 1
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/talk/AvatarCache$BitmapCache;

    invoke-direct {v0, p0}, Lcom/google/android/talk/AvatarCache$BitmapCache;-><init>(Lcom/google/android/talk/AvatarCache;)V

    iput-object v0, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/AvatarCache;->mNoAvatarList:Ljava/util/ArrayList;

    sget-object v0, Lcom/google/android/gsf/TalkContract$Avatars;->CONTENT_URI_AVATARS_BY:Landroid/net/Uri;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/talk/AvatarCache;->computeAvatarUri(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/AvatarCache;->mAvatarUri:Landroid/net/Uri;

    iput-wide p1, p0, Lcom/google/android/talk/AvatarCache;->mAccountId:J

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/talk/AvatarCache;->sDone:Z

    return v0
.end method

.method static synthetic access$100()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/google/android/talk/AvatarCache;->sQueue:Ljava/util/ArrayList;

    return-object v0
.end method

.method private computeAvatarUri(Landroid/net/Uri;J)Landroid/net/Uri;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static destroyAll(Landroid/content/Context;)V
    .locals 4
    .param p0    # Landroid/content/Context;

    sget-object v3, Lcom/google/android/talk/AvatarCache;->sInstancesLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/google/android/talk/AvatarCache;->sInstances:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/talk/AvatarCache;

    invoke-virtual {v2}, Lcom/google/android/talk/AvatarCache;->destroy()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    const/4 v2, 0x0

    :try_start_1
    sput-object v2, Lcom/google/android/talk/AvatarCache;->sInstances:Ljava/util/Map;

    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/talk/AvatarCache;->sDone:Z

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private findAvatarInCache(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    invoke-virtual {v2, p1}, Lcom/google/android/talk/AvatarCache$BitmapCache;->get(Ljava/lang/String;)Lcom/google/android/talk/AvatarCache$BitmapCache$CacheItem;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/talk/AvatarCache$BitmapCache$CacheItem;->getHash()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/talk/AvatarCache$BitmapCache$CacheItem;->getDrawable()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    :cond_0
    :goto_0
    monitor-exit v3

    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    invoke-virtual {v2, p1}, Lcom/google/android/talk/AvatarCache$BitmapCache;->clear(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static getInstance(JZ)Lcom/google/android/talk/AvatarCache;
    .locals 4
    .param p0    # J
    .param p2    # Z

    const-wide/16 v1, 0x0

    cmp-long v1, p0, v1

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getInstance: invalid account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/talk/AvatarCache;->loge(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v2, Lcom/google/android/talk/AvatarCache;->sInstancesLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/talk/AvatarCache;->sInstances:Ljava/util/Map;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v3, "AvatarCache hasn\'t been initialized"

    invoke-direct {v1, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    sget-object v1, Lcom/google/android/talk/AvatarCache;->sInstances:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/AvatarCache;

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    new-instance v0, Lcom/google/android/talk/AvatarCache;

    invoke-direct {v0, p0, p1}, Lcom/google/android/talk/AvatarCache;-><init>(J)V

    sget-object v1, Lcom/google/android/talk/AvatarCache;->sInstances:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static initialize(Lcom/google/android/talk/TalkApp;)V
    .locals 2
    .param p0    # Lcom/google/android/talk/TalkApp;

    sget-object v1, Lcom/google/android/talk/AvatarCache;->sInstancesLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/talk/AvatarCache;->sInstances:Ljava/util/Map;

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/talk/AvatarCache;->sInstances:Ljava/util/Map;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/android/talk/AvatarCache;->startQueryThread()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static loge(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[AvatarCache] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static startQueryThread()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/talk/AvatarCache$1;

    invoke-direct {v1}, Lcom/google/android/talk/AvatarCache$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/google/android/talk/AvatarCache;->sQueryThread:Ljava/lang/Thread;

    sget-object v0, Lcom/google/android/talk/AvatarCache;->sQueryThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public clear(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    invoke-virtual {v0, p1}, Lcom/google/android/talk/AvatarCache$BitmapCache;->clear(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/AvatarCache;->mNoAvatarList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    invoke-virtual {v0}, Lcom/google/android/talk/AvatarCache$BitmapCache;->clearAll()V

    return-void
.end method

.method public getAvatar(Landroid/database/Cursor;IILjava/lang/String;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/graphics/drawable/Drawable;

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p4, v3}, Lcom/google/android/talk/AvatarCache;->findAvatarInCache(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    if-ltz p3, :cond_1

    invoke-static {p1, p3}, Lcom/google/android/talk/DatabaseUtils;->getAvatarFromCursor(Landroid/database/Cursor;I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v4, 0x1

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v5, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    invoke-virtual {v5, p4, v1, v3, v4}, Lcom/google/android/talk/AvatarCache$BitmapCache;->add(Ljava/lang/String;Landroid/graphics/drawable/BitmapDrawable;Ljava/lang/String;Z)V

    move-object v2, v1

    goto :goto_0

    :cond_1
    move-object v2, p5

    goto :goto_0
.end method

.method public getAvatarIfInCache(Landroid/database/Cursor;IILjava/lang/String;Landroid/os/Message;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 10
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/os/Message;
    .param p6    # Landroid/graphics/drawable/Drawable;

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p4, v4}, Lcom/google/android/talk/AvatarCache;->findAvatarInCache(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v9

    if-eqz v9, :cond_0

    :goto_0
    return-object v9

    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    if-ltz p3, :cond_2

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    new-instance v0, Lcom/google/android/talk/AvatarCache$WorkItem;

    iget-wide v5, p0, Lcom/google/android/talk/AvatarCache;->mAccountId:J

    iget-object v7, p0, Lcom/google/android/talk/AvatarCache;->mCache:Lcom/google/android/talk/AvatarCache$BitmapCache;

    move-object v1, p0

    move-object v2, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/talk/AvatarCache$WorkItem;-><init>(Lcom/google/android/talk/AvatarCache;Ljava/lang/String;[BLjava/lang/String;JLcom/google/android/talk/AvatarCache$BitmapCache;Landroid/os/Message;)V

    sget-object v2, Lcom/google/android/talk/AvatarCache;->sQueue:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/talk/AvatarCache;->sQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/talk/AvatarCache;->sQueue:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v1, v5, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    sget-object v1, Lcom/google/android/talk/AvatarCache;->sQueue:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    :cond_1
    monitor-exit v2

    :cond_2
    move-object/from16 v9, p6

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAvatarUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/AvatarCache;->mAvatarUri:Landroid/net/Uri;

    return-object v0
.end method
