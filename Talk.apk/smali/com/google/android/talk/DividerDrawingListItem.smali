.class public Lcom/google/android/talk/DividerDrawingListItem;
.super Landroid/widget/LinearLayout;
.source "DividerDrawingListItem.java"


# instance fields
.field private mDivider:Landroid/graphics/drawable/Drawable;

.field private mDividerDrawRect:Landroid/graphics/Rect;

.field private mDividerHeight:I

.field private mShoveDistance:I

.field private mShoverEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerDrawRect:Landroid/graphics/Rect;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mShoveDistance:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mShoverEnabled:Z

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0x333334

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lcom/google/android/talk/DividerDrawingListItem;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerDrawRect:Landroid/graphics/Rect;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mShoveDistance:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mShoverEnabled:Z

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0x333334

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lcom/google/android/talk/DividerDrawingListItem;->init()V

    return-void
.end method

.method private init()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/talk/DividerDrawingListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mShoveDistance:I

    invoke-virtual {p0}, Lcom/google/android/talk/DividerDrawingListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerHeight:I

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v2, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerDrawRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/talk/DividerDrawingListItem;->shoverEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mShoveDistance:I

    :goto_0
    iput v0, v2, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerDrawRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/talk/DividerDrawingListItem;->getRight()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerDrawRect:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerDrawRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerHeight:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDivider:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDividerDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public setShoverEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/DividerDrawingListItem;->mShoverEnabled:Z

    return-void
.end method

.method protected shoverEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/talk/DividerDrawingListItem;->mShoverEnabled:Z

    return v0
.end method
