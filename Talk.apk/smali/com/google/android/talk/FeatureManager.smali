.class public Lcom/google/android/talk/FeatureManager;
.super Ljava/lang/Object;
.source "FeatureManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/FeatureManager$1;,
        Lcom/google/android/talk/FeatureManager$SetFeaturesTask;,
        Lcom/google/android/talk/FeatureManager$DeviceCapabilities;
    }
.end annotation


# static fields
.field private static final HVGA_SIZE:Lcom/google/android/videochat/VideoSpecification;

.field private static final QVGA_SIZE:Lcom/google/android/videochat/VideoSpecification;

.field private static sAudioChatEnabled:Z

.field private static sDefaultVideoChatEffect:Ljava/lang/String;

.field private static sEffectsEnabledMask:I

.field private static sVideoChatEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0xf

    new-instance v0, Lcom/google/android/videochat/VideoSpecification;

    new-instance v1, Lcom/google/android/videochat/Size;

    const/16 v2, 0x140

    const/16 v3, 0xc8

    invoke-direct {v1, v2, v3}, Lcom/google/android/videochat/Size;-><init>(II)V

    invoke-direct {v0, v1, v4}, Lcom/google/android/videochat/VideoSpecification;-><init>(Lcom/google/android/videochat/Size;I)V

    sput-object v0, Lcom/google/android/talk/FeatureManager;->QVGA_SIZE:Lcom/google/android/videochat/VideoSpecification;

    new-instance v0, Lcom/google/android/videochat/VideoSpecification;

    new-instance v1, Lcom/google/android/videochat/Size;

    const/16 v2, 0x1e0

    const/16 v3, 0x12c

    invoke-direct {v1, v2, v3}, Lcom/google/android/videochat/Size;-><init>(II)V

    invoke-direct {v0, v1, v4}, Lcom/google/android/videochat/VideoSpecification;-><init>(Lcom/google/android/videochat/Size;I)V

    sput-object v0, Lcom/google/android/talk/FeatureManager;->HVGA_SIZE:Lcom/google/android/videochat/VideoSpecification;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$200(Landroid/content/Context;)Lcom/google/android/talk/FeatureManager$DeviceCapabilities;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/talk/FeatureManager;->determineDeviceCapabilities(Landroid/content/Context;)Lcom/google/android/talk/FeatureManager$DeviceCapabilities;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/talk/FeatureManager;->sVideoChatEnabled:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/google/android/talk/FeatureManager;->sVideoChatEnabled:Z

    return p0
.end method

.method static synthetic access$400()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/talk/FeatureManager;->sAudioChatEnabled:Z

    return v0
.end method

.method static synthetic access$402(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/google/android/talk/FeatureManager;->sAudioChatEnabled:Z

    return p0
.end method

.method static synthetic access$500()Lcom/google/android/videochat/VideoSpecification;
    .locals 1

    sget-object v0, Lcom/google/android/talk/FeatureManager;->QVGA_SIZE:Lcom/google/android/videochat/VideoSpecification;

    return-object v0
.end method

.method static synthetic access$602(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/google/android/talk/FeatureManager;->sDefaultVideoChatEffect:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700()Lcom/google/android/videochat/VideoSpecification;
    .locals 1

    sget-object v0, Lcom/google/android/talk/FeatureManager;->HVGA_SIZE:Lcom/google/android/videochat/VideoSpecification;

    return-object v0
.end method

.method static synthetic access$800()I
    .locals 1

    sget v0, Lcom/google/android/talk/FeatureManager;->sEffectsEnabledMask:I

    return v0
.end method

.method static synthetic access$802(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/google/android/talk/FeatureManager;->sEffectsEnabledMask:I

    return p0
.end method

.method static synthetic access$872(I)I
    .locals 1
    .param p0    # I

    sget v0, Lcom/google/android/talk/FeatureManager;->sEffectsEnabledMask:I

    and-int/2addr v0, p0

    sput v0, Lcom/google/android/talk/FeatureManager;->sEffectsEnabledMask:I

    return v0
.end method

.method static synthetic access$900(J)V
    .locals 0
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/talk/FeatureManager;->setAccountFeatures(J)V

    return-void
.end method

.method public static areAnyEffectsAvailable()Z
    .locals 1

    sget v0, Lcom/google/android/talk/FeatureManager;->sEffectsEnabledMask:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static audioChatEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/talk/FeatureManager;->sAudioChatEnabled:Z

    return v0
.end method

.method public static clearDeviceFeatures(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    const-string v1, "deviceCapabilities"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dirty"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static determineDeviceCapabilities(Landroid/content/Context;)Lcom/google/android/talk/FeatureManager$DeviceCapabilities;
    .locals 10
    .param p0    # Landroid/content/Context;

    const/4 v7, 0x1

    const/4 v8, 0x0

    new-instance v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;

    const/4 v6, 0x0

    invoke-direct {v2, v6}, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;-><init>(Lcom/google/android/talk/FeatureManager$1;)V

    const-string v6, "deviceCapabilities"

    invoke-virtual {p0, v6, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-boolean v8, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mHasNEON:Z

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v6

    const/4 v9, 0x2

    if-lt v6, v9, :cond_1

    move v6, v7

    :goto_0
    iput-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mMulticore:Z

    const-string v6, "armeabi-v7a"

    sget-object v9, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "armeabi-v7a"

    sget-object v9, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    move v6, v7

    :goto_1
    iput-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mArmv7:Z

    const-string v6, "dirty"

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mArmv7:Z

    if-nez v6, :cond_3

    const-string v6, "talk"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Not arm7 "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v9, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v6, "dirty"

    invoke-interface {v4, v6, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v6, "frontCamera"

    iget-boolean v7, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mFrontCamera:Z

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v6, "glv2"

    iget-boolean v7, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mGlv2:Z

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v6, "armv7"

    iget-boolean v7, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mArmv7:Z

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v6, "neon"

    iget-boolean v7, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mHasNEON:Z

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v6, "effectsMask"

    iget v7, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mEffectsEnabledMask:I

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_3
    return-object v2

    :cond_1
    move v6, v8

    goto :goto_0

    :cond_2
    move v6, v8

    goto :goto_1

    :cond_3
    invoke-static {p0}, Lcom/google/android/talk/FeatureManager;->getOpenGlEsVersion(Landroid/content/Context;)F

    move-result v6

    const/high16 v9, 0x40000000

    cmpl-float v6, v6, v9

    if-ltz v6, :cond_4

    iput-boolean v7, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mGlv2:Z

    :cond_4
    invoke-static {}, Lcom/google/android/talk/FeatureManager;->hasNEON()Z

    move-result v6

    iput-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mHasNEON:Z

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v1, :cond_6

    iget-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mFrontCamera:Z

    if-nez v6, :cond_6

    invoke-static {v5, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v6, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v6, v7, :cond_5

    move v6, v7

    :goto_5
    iput-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mFrontCamera:Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_5
    move v6, v8

    goto :goto_5

    :cond_6
    const-string v6, "talk"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "supported front camera "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v9, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mFrontCamera:Z

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/talk/videochat/EffectsController;->generateCapabilitiesMask()I

    move-result v6

    iput v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mEffectsEnabledMask:I

    goto/16 :goto_2

    :cond_7
    const-string v6, "frontCamera"

    invoke-interface {v3, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mFrontCamera:Z

    const-string v6, "glv2"

    invoke-interface {v3, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mGlv2:Z

    const-string v6, "effectsMask"

    invoke-interface {v3, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mEffectsEnabledMask:I

    const-string v6, "neon"

    invoke-interface {v3, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, v2, Lcom/google/android/talk/FeatureManager$DeviceCapabilities;->mHasNEON:Z

    goto :goto_3
.end method

.method public static getDefaultVideoChatEffect()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/talk/FeatureManager;->sDefaultVideoChatEffect:Ljava/lang/String;

    return-object v0
.end method

.method private static getOpenGlEsVersion(Landroid/content/Context;)F
    .locals 10
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    array-length v7, v2

    if-lez v7, :cond_1

    move-object v0, v2

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    iget-object v7, v1, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    if-nez v7, :cond_0

    invoke-virtual {v1}, Landroid/content/pm/FeatureInfo;->getGlEsVersion()Ljava/lang/String;

    move-result-object v3

    const-string v7, "talk"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "gl version: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Ljava/lang/Float;

    invoke-direct {v7, v3}, Ljava/lang/Float;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    :goto_1
    return v7

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private static hasNEON()Z
    .locals 13

    const/4 v12, 0x2

    const/4 v6, 0x0

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    const-string v10, "/proc/cpuinfo"

    invoke-direct {v3, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    new-instance v11, Ljava/io/DataInputStream;

    invoke-direct {v11, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v10, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v10, "[ \t]*:[ \t]*"

    const/4 v11, 0x2

    invoke-virtual {v1, v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    array-length v10, v9

    if-ne v10, v12, :cond_0

    const-string v10, "Features"

    const/4 v11, 0x0

    aget-object v11, v9, v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v10, 0x1

    aget-object v10, v9, v10

    const-string v11, "[ \t]"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    array-length v8, v0

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v4, v0, v7

    const-string v10, "neon"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v10

    if-eqz v10, :cond_2

    const/4 v6, 0x1

    :cond_1
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_2
    return v6

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catchall_0
    move-exception v10

    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0

    :goto_3
    :try_start_5
    throw v10
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception v10

    goto :goto_2

    :catch_1
    move-exception v10

    goto :goto_2

    :catch_2
    move-exception v11

    goto :goto_3

    :catch_3
    move-exception v10

    goto :goto_1
.end method

.method public static isEffectAvailable(I)Z
    .locals 4
    .param p0    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    const/16 v2, 0x20

    if-le p0, v2, :cond_2

    :cond_0
    const-string v0, "talk"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isEffectAvailable bad id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    sget v2, Lcom/google/android/talk/FeatureManager;->sEffectsEnabledMask:I

    add-int/lit8 v3, p0, -0x1

    shl-int v3, v0, v3

    and-int/2addr v2, v3

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method private static setAccountFeatures(J)V
    .locals 2
    .param p0    # J

    invoke-static {}, Lcom/google/android/talk/SettingsCache;->getInstance()Lcom/google/android/talk/SettingsCache;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/google/android/talk/SettingsCache;->getSettingsMap(J)Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    move-result-object v0

    sget-boolean v1, Lcom/google/android/talk/FeatureManager;->sVideoChatEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setCameraEnabled(Z)V

    sget-boolean v1, Lcom/google/android/talk/FeatureManager;->sVideoChatEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setVideoChatEnabled(Z)V

    sget-boolean v1, Lcom/google/android/talk/FeatureManager;->sAudioChatEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->setAudioChatEnabled(Z)V

    return-void
.end method

.method public static setAvailableFeatures(Landroid/content/Context;JLjava/lang/Runnable;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/talk/FeatureManager$SetFeaturesTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/talk/FeatureManager$SetFeaturesTask;-><init>(Landroid/content/Context;JLjava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/google/android/talk/FeatureManager$SetFeaturesTask;->execute()V

    return-void
.end method

.method public static setAvailableFeaturesAllAccounts(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/talk/FeatureManager$SetFeaturesTask;

    invoke-direct {v0, p0, p1}, Lcom/google/android/talk/FeatureManager$SetFeaturesTask;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/google/android/talk/FeatureManager$SetFeaturesTask;->execute()V

    return-void
.end method

.method public static videoChatEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/talk/FeatureManager;->sVideoChatEnabled:Z

    return v0
.end method
