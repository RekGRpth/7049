.class final Lcom/google/android/talk/PictureCache$1;
.super Ljava/lang/Object;
.source "PictureCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/talk/PictureCache;->startQueryThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const/4 v12, 0x1

    const/16 v9, 0xa

    invoke-static {v9}, Landroid/os/Process;->setThreadPriority(I)V

    :cond_0
    :goto_0
    # getter for: Lcom/google/android/talk/PictureCache;->sDone:Z
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$000()Z

    move-result v9

    if-nez v9, :cond_a

    const/4 v8, 0x0

    # getter for: Lcom/google/android/talk/PictureCache;->sQueue:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$100()Ljava/util/ArrayList;

    move-result-object v10

    monitor-enter v10

    :try_start_0
    # getter for: Lcom/google/android/talk/PictureCache;->sQueue:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$100()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_4

    # getter for: Lcom/google/android/talk/PictureCache;->sQueue:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$100()Ljava/util/ArrayList;

    move-result-object v9

    const/4 v11, 0x0

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/google/android/talk/PictureCache$WorkItem;

    move-object v8, v0

    :goto_1
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v8, :cond_0

    const/4 v2, 0x0

    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    const-string v9, "http"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "https"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_1
    const-string v9, "www.flickr.com"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    # getter for: Lcom/google/android/talk/PictureCache;->WIDTH:I
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$200()I

    move-result v10

    # getter for: Lcom/google/android/talk/PictureCache;->HEIGHT:I
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$300()I

    move-result v11

    # invokes: Lcom/google/android/talk/PictureCache;->getFlickrPicture(Ljava/lang/String;II)Lcom/google/android/talk/PictureCache$PictureData;
    invoke-static {v9, v10, v11}, Lcom/google/android/talk/PictureCache;->access$400(Ljava/lang/String;II)Lcom/google/android/talk/PictureCache$PictureData;

    move-result-object v2

    :cond_2
    :goto_2
    if-nez v2, :cond_9

    new-instance v2, Lcom/google/android/talk/PictureCache$PictureData;

    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    invoke-direct {v2, v9}, Lcom/google/android/talk/PictureCache$PictureData;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Lcom/google/android/talk/PictureCache$PictureData;->setDrawable(Landroid/graphics/drawable/BitmapDrawable;)V

    :cond_3
    :goto_3
    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mCache:Lcom/google/android/talk/PictureCache$BitmapCache;

    invoke-virtual {v9, v2}, Lcom/google/android/talk/PictureCache$BitmapCache;->add(Lcom/google/android/talk/PictureCache$PictureData;)V

    # getter for: Lcom/google/android/talk/PictureCache;->sQueue:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$100()Ljava/util/ArrayList;

    move-result-object v10

    monitor-enter v10

    :try_start_1
    # getter for: Lcom/google/android/talk/PictureCache;->sQueue:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$100()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v2}, Lcom/google/android/talk/PictureCache$PictureData;->getDrawable()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v5, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mPictureLoadedMessage:Landroid/os/Message;

    if-eqz v5, :cond_0

    iget-object v4, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/util/HashMap;

    const-string v9, "bitmap"

    invoke-virtual {v2}, Lcom/google/android/talk/PictureCache$PictureData;->getDrawable()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :cond_4
    :try_start_2
    # getter for: Lcom/google/android/talk/PictureCache;->sQueue:Ljava/util/ArrayList;
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$100()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v9

    goto :goto_1

    :catchall_0
    move-exception v9

    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v9

    :cond_5
    const-string v9, "picasaweb.google.com"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    # getter for: Lcom/google/android/talk/PictureCache;->WIDTH:I
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$200()I

    move-result v10

    # getter for: Lcom/google/android/talk/PictureCache;->HEIGHT:I
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$300()I

    move-result v11

    # invokes: Lcom/google/android/talk/PictureCache;->getPicasaPicture(Ljava/lang/String;II)Lcom/google/android/talk/PictureCache$PictureData;
    invoke-static {v9, v10, v11}, Lcom/google/android/talk/PictureCache;->access$500(Ljava/lang/String;II)Lcom/google/android/talk/PictureCache$PictureData;

    move-result-object v2

    goto :goto_2

    :cond_6
    const-string v9, "picasaweb.google.com"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "www.youtube.com"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    # getter for: Lcom/google/android/talk/PictureCache;->WIDTH:I
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$200()I

    move-result v10

    # getter for: Lcom/google/android/talk/PictureCache;->HEIGHT:I
    invoke-static {}, Lcom/google/android/talk/PictureCache;->access$300()I

    move-result v11

    # invokes: Lcom/google/android/talk/PictureCache;->getYouTubeThumbnail(Ljava/lang/String;II)Lcom/google/android/talk/PictureCache$PictureData;
    invoke-static {v9, v10, v11}, Lcom/google/android/talk/PictureCache;->access$600(Ljava/lang/String;II)Lcom/google/android/talk/PictureCache$PictureData;

    move-result-object v2

    goto :goto_2

    :cond_7
    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, ".jpg"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, ".gif"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_8
    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    # invokes: Lcom/google/android/talk/PictureCache;->getJpgPicture(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v9}, Lcom/google/android/talk/PictureCache;->access$700(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v2, Lcom/google/android/talk/PictureCache$PictureData;

    iget-object v9, v8, Lcom/google/android/talk/PictureCache$WorkItem;->mUrl:Ljava/lang/String;

    invoke-direct {v2, v9}, Lcom/google/android/talk/PictureCache$PictureData;-><init>(Ljava/lang/String;)V

    new-instance v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v9, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v9}, Lcom/google/android/talk/PictureCache$PictureData;->setDrawable(Landroid/graphics/drawable/BitmapDrawable;)V

    invoke-virtual {v2, v12}, Lcom/google/android/talk/PictureCache$PictureData;->setType(I)V

    goto/16 :goto_2

    :cond_9
    invoke-virtual {v2}, Lcom/google/android/talk/PictureCache$PictureData;->getDrawable()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-virtual {v2, v12}, Lcom/google/android/talk/PictureCache$PictureData;->setIsSourceBitmap(Z)V

    goto/16 :goto_3

    :catchall_1
    move-exception v9

    :try_start_4
    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v9

    :cond_a
    return-void
.end method
