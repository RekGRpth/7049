.class public Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;
.super Lcom/google/android/talk/TalkApp$AccountInfo;
.source "PublicIntentDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/PublicIntentDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FromAccountInfo"
.end annotation


# instance fields
.field public mHasTargetUser:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/talk/TalkApp$AccountInfo;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/talk/TalkApp$AccountInfo;Z)V
    .locals 2
    .param p1    # Lcom/google/android/talk/TalkApp$AccountInfo;
    .param p2    # Z

    invoke-direct {p0}, Lcom/google/android/talk/TalkApp$AccountInfo;-><init>()V

    iget-object v0, p1, Lcom/google/android/talk/TalkApp$AccountInfo;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->username:Ljava/lang/String;

    iget-wide v0, p1, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    iput-wide v0, p0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->accountId:J

    iget-boolean v0, p1, Lcom/google/android/talk/TalkApp$AccountInfo;->signedIn:Z

    iput-boolean v0, p0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->signedIn:Z

    iget-boolean v0, p1, Lcom/google/android/talk/TalkApp$AccountInfo;->active:Z

    iput-boolean v0, p0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->active:Z

    iput-boolean p2, p0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->mHasTargetUser:Z

    return-void
.end method
