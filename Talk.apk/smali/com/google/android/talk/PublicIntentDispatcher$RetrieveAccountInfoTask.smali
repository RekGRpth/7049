.class Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;
.super Landroid/os/AsyncTask;
.source "PublicIntentDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/PublicIntentDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RetrieveAccountInfoTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mCr:Landroid/content/ContentResolver;

.field final synthetic this$0:Lcom/google/android/talk/PublicIntentDispatcher;


# direct methods
.method public constructor <init>(Lcom/google/android/talk/PublicIntentDispatcher;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2    # Landroid/content/ContentResolver;

    iput-object p1, p0, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;->this$0:Lcom/google/android/talk/PublicIntentDispatcher;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;->mCr:Landroid/content/ContentResolver;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;->doInBackground([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    aget-object v4, p1, v5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;->mCr:Landroid/content/ContentResolver;

    invoke-static {v5}, Lcom/google/android/talk/DatabaseUtils;->getAllAccountInfos(Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/TalkApp$AccountInfo;

    iget-object v5, p0, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;->mCr:Landroid/content/ContentResolver;

    iget-wide v6, v0, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-static {v5, v4, v6, v7}, Lcom/google/android/talk/DatabaseUtils;->IsUserInRosterList(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    move-result v3

    new-instance v5, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    invoke-direct {v5, v0, v3}, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;-><init>(Lcom/google/android/talk/TalkApp$AccountInfo;Z)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;->this$0:Lcom/google/android/talk/PublicIntentDispatcher;

    # invokes: Lcom/google/android/talk/PublicIntentDispatcher;->onAccountStateAvailable(Ljava/util/List;)V
    invoke-static {v0, p1}, Lcom/google/android/talk/PublicIntentDispatcher;->access$100(Lcom/google/android/talk/PublicIntentDispatcher;Ljava/util/List;)V

    return-void
.end method
