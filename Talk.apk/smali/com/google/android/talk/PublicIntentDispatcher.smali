.class public Lcom/google/android/talk/PublicIntentDispatcher;
.super Landroid/app/Activity;
.source "PublicIntentDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;,
        Lcom/google/android/talk/PublicIntentDispatcher$XmppUri;,
        Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;
    }
.end annotation


# instance fields
.field private mAction:I

.field private mApp:Lcom/google/android/talk/TalkApp;

.field private mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

.field private final mHandler:Landroid/os/Handler;

.field private mMessageBody:Ljava/lang/String;

.field mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mToAddress:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/talk/PublicIntentDispatcher$1;

    invoke-direct {v0, p0}, Lcom/google/android/talk/PublicIntentDispatcher$1;-><init>(Lcom/google/android/talk/PublicIntentDispatcher;)V

    iput-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/talk/PublicIntentDispatcher;Lcom/google/android/gtalkservice/IGTalkService;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/PublicIntentDispatcher;
    .param p1    # Lcom/google/android/gtalkservice/IGTalkService;

    invoke-direct {p0, p1}, Lcom/google/android/talk/PublicIntentDispatcher;->serviceStateChanged(Lcom/google/android/gtalkservice/IGTalkService;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/talk/PublicIntentDispatcher;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/PublicIntentDispatcher;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/talk/PublicIntentDispatcher;->onAccountStateAvailable(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/talk/PublicIntentDispatcher;)I
    .locals 1
    .param p0    # Lcom/google/android/talk/PublicIntentDispatcher;

    iget v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/talk/PublicIntentDispatcher;I)I
    .locals 0
    .param p0    # Lcom/google/android/talk/PublicIntentDispatcher;
    .param p1    # I

    iput p1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/talk/PublicIntentDispatcher;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/talk/PublicIntentDispatcher;

    iget-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/talk/PublicIntentDispatcher;)Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;
    .locals 1
    .param p0    # Lcom/google/android/talk/PublicIntentDispatcher;

    iget-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/talk/PublicIntentDispatcher;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/talk/PublicIntentDispatcher;

    iget-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mMessageBody:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Z)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Z

    invoke-static/range {p0 .. p6}, Lcom/google/android/talk/PublicIntentDispatcher;->executeRequestedAction(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$700(Landroid/content/Context;JLjava/lang/String;ZZ)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z

    invoke-static/range {p0 .. p5}, Lcom/google/android/talk/PublicIntentDispatcher;->startCall(Landroid/content/Context;JLjava/lang/String;ZZ)V

    return-void
.end method

.method static synthetic access$800(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    invoke-static/range {p0 .. p5}, Lcom/google/android/talk/PublicIntentDispatcher;->openChat(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/talk/PublicIntentDispatcher;ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/Runnable;)Landroid/app/Dialog;
    .locals 1
    .param p0    # Lcom/google/android/talk/PublicIntentDispatcher;
    .param p1    # I
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Ljava/lang/Runnable;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/talk/PublicIntentDispatcher;->makeConfirmationDialog(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/Runnable;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static chooseFromAccount(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;
    .locals 7
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;"
        }
    .end annotation

    const/4 v6, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    iget-object v5, v0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->username:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v5, v0

    goto :goto_0

    :cond_2
    iget-boolean v5, v0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->mHasTargetUser:Z

    if-eqz v5, :cond_1

    iget-boolean v5, v0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->signedIn:Z

    if-eqz v5, :cond_4

    iget-boolean v5, v0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->active:Z

    if-eqz v5, :cond_4

    move-object v3, v0

    :cond_3
    :goto_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-boolean v5, v0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->signedIn:Z

    if-eqz v5, :cond_3

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    if-eqz v3, :cond_6

    move-object v5, v3

    goto :goto_0

    :cond_6
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_7

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    goto :goto_0

    :cond_7
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_8

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    goto :goto_0

    :cond_8
    invoke-interface {p0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    goto :goto_0
.end method

.method public static executeRequestedAction(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/talk/PublicIntentDispatcher;->executeRequestedAction(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Z)V

    return-void
.end method

.method private static executeRequestedAction(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Z)V
    .locals 20
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Z

    const/16 v19, 0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    move/from16 v9, v19

    :goto_1
    return-void

    :pswitch_0
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    move-object/from16 v3, p2

    move/from16 v4, p6

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/talk/PublicIntentDispatcher;->inviteFriend(Landroid/content/Context;JLjava/lang/String;Z)V

    move/from16 v9, v19

    goto :goto_1

    :pswitch_1
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    move/from16 v3, p6

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/talk/PublicIntentDispatcher;->showFriendList(Landroid/content/Context;JZ)V

    move/from16 v9, v19

    goto :goto_1

    :pswitch_2
    move-object/from16 v5, p0

    move-wide/from16 v6, p3

    move-object/from16 v8, p2

    move-object/from16 v9, p5

    move/from16 v10, p6

    invoke-static/range {v5 .. v10}, Lcom/google/android/talk/PublicIntentDispatcher;->openChat(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Z)V

    move/from16 v9, v19

    goto :goto_1

    :pswitch_3
    const/4 v9, 0x0

    :goto_2
    move-object/from16 v5, p0

    move-wide/from16 v6, p3

    move-object/from16 v8, p2

    move/from16 v10, p6

    invoke-static/range {v5 .. v10}, Lcom/google/android/talk/PublicIntentDispatcher;->startCall(Landroid/content/Context;JLjava/lang/String;ZZ)V

    goto :goto_1

    :pswitch_4
    new-instance v10, Lcom/google/android/talk/ContactInfoQuery;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v11, p0

    move-wide/from16 v12, p3

    move-object/from16 v14, p2

    invoke-direct/range {v10 .. v16}, Lcom/google/android/talk/ContactInfoQuery;-><init>(Landroid/content/Context;JLjava/lang/String;Landroid/database/ContentObserver;Z)V

    new-instance v11, Lcom/google/android/talk/PublicIntentDispatcher$4;

    move-object v12, v10

    move-object/from16 v13, p0

    move-wide/from16 v14, p3

    move-object/from16 v16, p2

    move/from16 v17, p6

    move-object/from16 v18, p5

    invoke-direct/range {v11 .. v18}, Lcom/google/android/talk/PublicIntentDispatcher$4;-><init>(Lcom/google/android/talk/ContactInfoQuery;Landroid/content/Context;JLjava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v10, v11}, Lcom/google/android/talk/ContactInfoQuery;->setContactInfoCallback(Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;)V

    invoke-virtual {v10}, Lcom/google/android/talk/ContactInfoQuery;->startQueryForContactInfo()V

    goto :goto_0

    :pswitch_5
    move/from16 v9, v19

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private executeRequestedActionWithConfirm(Z)V
    .locals 6
    .param p1    # Z

    iget v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    invoke-static {v0}, Lcom/google/android/talk/PublicIntentDispatcher$XmppUri;->needsConfirmation(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    iget-object v2, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    iget-wide v3, v0, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->accountId:J

    iget-object v5, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mMessageBody:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/talk/PublicIntentDispatcher;->executeRequestedAction(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/talk/PublicIntentDispatcher$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/talk/PublicIntentDispatcher$3;-><init>(Lcom/google/android/talk/PublicIntentDispatcher;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/PublicIntentDispatcher;->getConfirmation(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private static findMatchingProvider(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "GTalk"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GTalk"

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getConfirmation(Ljava/lang/Runnable;)V
    .locals 7
    .param p1    # Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/talk/ContactInfoQuery;

    iget-object v1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    iget-wide v2, v1, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->accountId:J

    iget-object v4, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/talk/ContactInfoQuery;-><init>(Landroid/content/Context;JLjava/lang/String;Landroid/database/ContentObserver;Z)V

    new-instance v1, Lcom/google/android/talk/PublicIntentDispatcher$5;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/talk/PublicIntentDispatcher$5;-><init>(Lcom/google/android/talk/PublicIntentDispatcher;Lcom/google/android/talk/ContactInfoQuery;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/talk/ContactInfoQuery;->setContactInfoCallback(Lcom/google/android/talk/ContactInfoQuery$ContactInfoQueryCallback;)V

    invoke-virtual {v0}, Lcom/google/android/talk/ContactInfoQuery;->startQueryForContactInfo()V

    return-void
.end method

.method private static getProviderNameForCategory(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    const-string v0, "com.android.im.category.GTALK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GTalk"

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static inviteFriend(Landroid/content/Context;JLjava/lang/String;Z)V
    .locals 3

    if-eqz p4, :cond_0

    const/4 v0, 0x4

    invoke-static {p0, v0, p1, p2, p3}, Lcom/google/android/talk/PublicIntentDispatcher;->startLogin(Landroid/content/Context;IJLjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-class v1, Lcom/google/android/talk/AddBuddyScreen;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "accountId"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "start_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private isCallAction()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    if-eq v1, v2, :cond_0

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isValidAddress(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    const/4 v2, -0x1

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ne v1, v2, :cond_0

    const/16 v1, 0x40

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[PublicIntentDispatcherActivity] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private makeConfirmationDialog(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/Runnable;)Landroid/app/Dialog;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Landroid/graphics/Bitmap;
    .param p4    # Ljava/lang/Runnable;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    const/4 v6, 0x3

    invoke-direct {v1, p0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    const v6, 0x7f040023

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v6, 0x7f100054

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    if-eqz p3, :cond_0

    invoke-virtual {v4, p3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    const v6, 0x7f100053

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {v2, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    const v6, 0x7f100056

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    iget-object v7, v7, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->username:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f100057

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f0c00d9

    new-instance v7, Lcom/google/android/talk/PublicIntentDispatcher$6;

    invoke-direct {v7, p0, p4}, Lcom/google/android/talk/PublicIntentDispatcher$6;-><init>(Lcom/google/android/talk/PublicIntentDispatcher;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v6, 0x7f0c00da

    new-instance v7, Lcom/google/android/talk/PublicIntentDispatcher$7;

    invoke-direct {v7, p0}, Lcom/google/android/talk/PublicIntentDispatcher$7;-><init>(Lcom/google/android/talk/PublicIntentDispatcher;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :pswitch_0
    const v6, 0x7f0c00d7

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :pswitch_1
    const v6, 0x7f0c00d8

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onAccountStateAvailable(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;",
            ">;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    iget-object v1, v1, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->username:Ljava/lang/String;

    :goto_0
    invoke-static {p1, v1}, Lcom/google/android/talk/PublicIntentDispatcher;->chooseFromAccount(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    invoke-static {p0}, Lcom/google/android/talk/TalkApp;->getApplication(Landroid/app/Activity;)Lcom/google/android/talk/TalkApp;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v2, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/talk/PublicIntentDispatcher$2;

    invoke-direct {v3, p0}, Lcom/google/android/talk/PublicIntentDispatcher$2;-><init>(Lcom/google/android/talk/PublicIntentDispatcher;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/talk/TalkApp;->addServiceAvailableCallback(Landroid/os/Handler;Lcom/google/android/talk/ServiceAvailableRunnable;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "Couldn\'t find a valid gtalk account to send from"

    invoke-static {v1}, Lcom/google/android/talk/PublicIntentDispatcher;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->finish()V

    goto :goto_1
.end method

.method private static openChat(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ImUrlReceiver: openChat with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/talk/PublicIntentDispatcher;->log(Ljava/lang/String;)V

    :cond_0
    if-eqz p5, :cond_1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2, p3}, Lcom/google/android/talk/PublicIntentDispatcher;->startLogin(Landroid/content/Context;IJLjava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/talk/BuddyListCombo;->startChatScreenActivity(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0
.end method

.method private parseIntent(Landroid/content/Intent;)Z
    .locals 14
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v11

    if-eqz v11, :cond_0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "parseIntent: host="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/talk/PublicIntentDispatcher;->log(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v11

    const-string v12, "xmpp"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/talk/PublicIntentDispatcher$XmppUri;->parse(Landroid/net/Uri;)Lcom/google/android/talk/PublicIntentDispatcher$XmppUri;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/talk/PublicIntentDispatcher$XmppUri;->getAction()I

    move-result v11

    iput v11, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    invoke-virtual {v10}, Lcom/google/android/talk/PublicIntentDispatcher$XmppUri;->getToAddress()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    new-instance v11, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    invoke-direct {v11}, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;-><init>()V

    iput-object v11, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    iget-object v11, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    invoke-virtual {v10}, Lcom/google/android/talk/PublicIntentDispatcher$XmppUri;->getFromAddress()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->username:Ljava/lang/String;

    iget v11, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    if-nez v11, :cond_1

    invoke-virtual {v10}, Lcom/google/android/talk/PublicIntentDispatcher$XmppUri;->getMessageBody()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mMessageBody:Ljava/lang/String;

    :cond_1
    :goto_0
    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v11

    if-eqz v11, :cond_2

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "parseIntent: to="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/talk/PublicIntentDispatcher;->log(Ljava/lang/String;)V

    :cond_2
    const/4 v11, 0x1

    :goto_1
    return v11

    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/talk/PublicIntentDispatcher;->isValidAddress(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    :goto_2
    iput-object v9, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    const/4 v11, 0x0

    iput v11, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/talk/PublicIntentDispatcher;->getProviderNameForCategory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/talk/PublicIntentDispatcher;->findMatchingProvider(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    const-string v11, "talk"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "parseIntent: IM provider "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " not supported"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    goto :goto_1

    :cond_4
    const/4 v9, 0x0

    goto :goto_2

    :cond_5
    invoke-static {v3}, Lcom/google/android/talk/PublicIntentDispatcher;->findMatchingProvider(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_6

    const-string v11, "talk"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "parseIntent: IM provider "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " not supported"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v11

    if-eqz v11, :cond_7

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "parseIntent: path="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/talk/PublicIntentDispatcher;->log(Ljava/lang/String;)V

    :cond_7
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    const/16 v11, 0x2f

    invoke-virtual {v7, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/4 v11, -0x1

    if-eq v4, v11, :cond_1

    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v7, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/talk/PublicIntentDispatcher;->isValidAddress(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    :goto_3
    iput-object v9, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    const/4 v11, 0x0

    iput v11, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    goto/16 :goto_0

    :cond_8
    const/4 v9, 0x0

    goto :goto_3
.end method

.method private requestAccountInfo()V
    .locals 4

    new-instance v0, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;

    invoke-virtual {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;-><init>(Lcom/google/android/talk/PublicIntentDispatcher;Landroid/content/ContentResolver;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/talk/PublicIntentDispatcher$RetrieveAccountInfoTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private serviceStateChanged(Lcom/google/android/gtalkservice/IGTalkService;)V
    .locals 7
    .param p1    # Lcom/google/android/gtalkservice/IGTalkService;

    if-eqz p1, :cond_5

    const/4 v1, 0x1

    :try_start_0
    iget-object v4, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    iget-wide v4, v4, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->accountId:J

    invoke-interface {p1, v4, v5}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Lcom/google/android/gtalkservice/IImSession;->getPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v1, 0x1

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mToAddress:Ljava/lang/String;

    if-nez v4, :cond_4

    const/4 v4, 0x5

    iput v4, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    :cond_1
    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/talk/PublicIntentDispatcher;->executeRequestedActionWithConfirm(Z)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mFromAccount:Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;

    iget-boolean v4, v4, Lcom/google/android/talk/PublicIntentDispatcher$FromAccountInfo;->mHasTargetUser:Z

    if-nez v4, :cond_1

    const/4 v4, 0x4

    iput v4, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v4, "talk"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[PublicIntentDispatcherActivity] onServiceConnected: caught "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->finish()V

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "service disconnected, wait..."

    invoke-static {v4}, Lcom/google/android/talk/PublicIntentDispatcher;->log(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static showFriendList(Landroid/content/Context;JZ)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Z

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ImUrlReceiver: show friendlist screen"

    invoke-static {v1}, Lcom/google/android/talk/PublicIntentDispatcher;->log(Ljava/lang/String;)V

    :cond_0
    if-eqz p3, :cond_1

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {p0, v1, p1, p2, v2}, Lcom/google/android/talk/PublicIntentDispatcher;->startLogin(Landroid/content/Context;IJLjava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gsf/TalkContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "com.android.im.category.GTALK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "accountId"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private static startCall(Landroid/content/Context;JLjava/lang/String;ZZ)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z

    if-eqz p5, :cond_1

    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0, p1, p2, p3}, Lcom/google/android/talk/PublicIntentDispatcher;->startLogin(Landroid/content/Context;IJLjava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/talk/videochat/VideoChatActivity;->startActivityToInitiate(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/talk/BuddyListCombo;->startVoiceChat(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_1
.end method

.method private static startLogin(Landroid/content/Context;IJLjava/lang/String;)V
    .locals 5

    sget-object v0, Lcom/google/android/gsf/TalkContract$Account;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "show Signin screen for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/talk/PublicIntentDispatcher;->log(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "im:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-class v2, Lcom/google/android/talk/AccountSelectionActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v0, "Send2_U"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "Send2_A"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.intent.action.SENDTO"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0, v1}, Lcom/google/android/talk/PublicIntentDispatcher;->parseIntent(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/talk/videochat/WifiPolicyUtils;->wifiRequiredForVideoChat(Landroid/content/ContentResolver;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->isCallAction()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mAction:I

    if-eq v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_1
    iget-object v3, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-static {p0, v2, v3}, Lcom/google/android/talk/videochat/WifiPolicyUtils;->showAlertIfNoWifi(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->requestAccountInfo()V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/talk/PublicIntentDispatcher;->finish()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mApp:Lcom/google/android/talk/TalkApp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v1, p0, Lcom/google/android/talk/PublicIntentDispatcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/google/android/talk/TalkApp;->removeServiceAvailableCallback(Landroid/os/Handler;)V

    :cond_0
    return-void
.end method
