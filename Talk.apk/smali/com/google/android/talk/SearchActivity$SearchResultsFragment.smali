.class public Lcom/google/android/talk/SearchActivity$SearchResultsFragment;
.super Landroid/app/ListFragment;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchResultsFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/SearchActivity$SearchResultsFragment$QueryHandler;
    }
.end annotation


# static fields
.field public static final EXTRA_USERNAME:Ljava/lang/String; = "username"

.field private static final LOG_TAG:Ljava/lang/String; = "talk"


# instance fields
.field private mApp:Lcom/google/android/talk/TalkApp;

.field private mCountView:Landroid/widget/TextView;

.field private mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

.field private mEmptyView:Landroid/widget/TextView;

.field private mGmail:Lcom/google/android/talk/GmailProviderWrapper;

.field private mLastActivatedView:Landroid/view/View;

.field private mMustRequeryCursor:Z

.field private mQueryHandler:Lcom/google/android/talk/SearchActivity$SearchResultsFragment$QueryHandler;

.field private mQueryString:Ljava/lang/String;

.field private mSinglePane:Z

.field private mTitleView:Landroid/widget/TextView;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mMustRequeryCursor:Z

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/talk/SearchActivity$SearchResultsFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/talk/SearchActivity$SearchResultsFragment;

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/talk/SearchActivity$SearchResultsFragment;)Lcom/google/android/talk/GmailProviderWrapper;
    .locals 1
    .param p0    # Lcom/google/android/talk/SearchActivity$SearchResultsFragment;

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mGmail:Lcom/google/android/talk/GmailProviderWrapper;

    return-object v0
.end method

.method private maybeRequery()V
    .locals 2

    iget-boolean v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mMustRequeryCursor:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {v1}, Lcom/google/android/talk/SearchHeaderCursorAdapter;->getCursor()Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mMustRequeryCursor:Z

    invoke-virtual {v0}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->becomeActiveNetworkCursor()V

    invoke-virtual {v0}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->requery()Z

    :cond_0
    return-void
.end method

.method private openConversation(I)V
    .locals 7
    .param p1    # I

    iget-object v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {v4}, Lcom/google/android/talk/SearchHeaderCursorAdapter;->getCursor()Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->moveTo(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {v4, p1}, Lcom/google/android/talk/SearchHeaderCursorAdapter;->setActivatedItem(I)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/google/android/talk/SearchActivity;

    invoke-virtual {v0}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getConversationId()J

    move-result-wide v4

    # invokes: Lcom/google/android/talk/SearchActivity;->setConversationId(J)V
    invoke-static {v2, v4, v5}, Lcom/google/android/talk/SearchActivity;->access$200(Lcom/google/android/talk/SearchActivity;J)V

    iget-boolean v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mSinglePane:Z

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v4, "show_fragment"

    const-string v5, "SearchChatResultFragment"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "thread-id"

    invoke-virtual {v2}, Lcom/google/android/talk/SearchActivity;->getConversationId()J

    move-result-wide v5

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v4, "query"

    iget-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    const v4, 0x7f100077

    new-instance v5, Lcom/google/android/talk/fragments/SearchChatResultFragment;

    invoke-direct {v5}, Lcom/google/android/talk/fragments/SearchChatResultFragment;-><init>()V

    # getter for: Lcom/google/android/talk/SearchActivity;->CHAT_RESULT_FRAGMENT_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/talk/SearchActivity;->access$300()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method private updateTitle(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mTitleView:Landroid/widget/TextView;

    const v2, 0x7f0c0070

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCountView:Landroid/widget/TextView;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public contentChanged()V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mEmptyView:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    if-nez v4, :cond_3

    const-string v3, ""

    :goto_1
    iget-object v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    const-string v4, "talk"

    const-string v5, "SearchActivity.onContentChanged"

    invoke-static {v4, v5}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {v4}, Lcom/google/android/talk/SearchHeaderCursorAdapter;->getCount()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    invoke-direct {p0, v4, v1}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->updateTitle(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    if-lez v1, :cond_0

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mSinglePane:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/ListView;->setSelection(I)V

    invoke-direct {p0, v7}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->openConversation(I)V

    goto :goto_0

    :cond_3
    const v4, 0x7f0c00b3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v0, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public doSearchQuery(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 12
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {v1}, Lcom/google/android/talk/SearchHeaderCursorAdapter;->getCount()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->updateTitle(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    const-string v0, "content"

    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v9

    new-instance v7, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v7, v0, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v0, "from"

    invoke-virtual {v7, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "accountId"

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "accountId"

    const-wide/16 v10, 0x0

    invoke-virtual {v1, v2, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-virtual {v7, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v7}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0}, Lcom/google/android/talk/TalkApp;->getActiveAccount()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v0, v6, Lcom/google/android/talk/TalkApp$AccountInfo;->username:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, "talk"

    const-string v1, "No AccountInfo & no username, bailing from search."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    iget-object v0, v6, Lcom/google/android/talk/TalkApp$AccountInfo;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mUserName:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doSearchQuery queryString: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " userName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mUserName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "in:chats "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/talk/SearchHeaderCursorAdapter;->setSearch(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryHandler:Lcom/google/android/talk/SearchActivity$SearchResultsFragment$QueryHandler;

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment$QueryHandler;->setAdapter(Lcom/google/android/talk/SearchHeaderCursorAdapter;)V

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mGmail:Lcom/google/android/talk/GmailProviderWrapper;

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mUserName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryHandler:Lcom/google/android/talk/SearchActivity$SearchResultsFragment$QueryHandler;

    const/4 v3, 0x0

    sget-object v5, Lcom/google/android/talk/GmailProviderWrapper$BecomeActiveNetworkCursor;->YES:Lcom/google/android/talk/GmailProviderWrapper$BecomeActiveNetworkCursor;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/talk/GmailProviderWrapper;->runQueryForConversations(Ljava/lang/String;Landroid/content/AsyncQueryHandler;ILjava/lang/String;Lcom/google/android/talk/GmailProviderWrapper$BecomeActiveNetworkCursor;)V

    goto/16 :goto_0
.end method

.method public isSelected(J)Z
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x1

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v5, Lcom/google/android/talk/SearchActivity$SearchResultsFragment$QueryHandler;

    invoke-direct {v5, p0, v0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment$QueryHandler;-><init>(Lcom/google/android/talk/SearchActivity$SearchResultsFragment;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mQueryHandler:Lcom/google/android/talk/SearchActivity$SearchResultsFragment$QueryHandler;

    invoke-static {v0}, Lcom/google/android/talk/TalkApp;->getApplication(Landroid/app/Activity;)Lcom/google/android/talk/TalkApp;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v5}, Lcom/google/android/talk/TalkApp;->getGmailProvider()Lcom/google/android/talk/GmailProviderWrapper;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mGmail:Lcom/google/android/talk/GmailProviderWrapper;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "talk"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SearchActivity.onCreate queryAction: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v2, Lcom/google/android/talk/NetworkProgressMonitor;

    new-instance v5, Lcom/google/android/talk/SearchActivity$SearchResultsFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment$1;-><init>(Lcom/google/android/talk/SearchActivity$SearchResultsFragment;)V

    invoke-direct {v2, v0, v5}, Lcom/google/android/talk/NetworkProgressMonitor;-><init>(Landroid/app/Activity;Ljava/lang/Runnable;)V

    new-instance v5, Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-direct {v5, p0, v2}, Lcom/google/android/talk/SearchHeaderCursorAdapter;-><init>(Lcom/google/android/talk/SearchActivity$SearchResultsFragment;Lcom/google/android/talk/IProgressMonitor;)V

    iput-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    iget-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {p0, v5}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v1, v8}, Landroid/widget/ListView;->setFocusable(Z)V

    invoke-virtual {v1, v8}, Landroid/widget/ListView;->setClickable(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->registerForContextMenu(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f100080

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mEmptyView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f10006e

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f10007f

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCountView:Landroid/widget/TextView;

    const-string v5, "onCreate"

    invoke-virtual {p0, v4, v5}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->doSearchQuery(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/talk/ActivityUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mSinglePane:Z

    invoke-virtual {p0, v1}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->setHasOptionsMenu(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v0, 0x7f040037

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {v1}, Lcom/google/android/talk/SearchHeaderCursorAdapter;->getCursor()Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    const/4 v1, 0x0

    invoke-direct {p0, p3}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->openConversation(I)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/view/View;->setActivated(Z)V

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mLastActivatedView:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mLastActivatedView:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mLastActivatedView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mLastActivatedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    :cond_1
    iput-object p2, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mLastActivatedView:Landroid/view/View;

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mMustRequeryCursor:Z

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    invoke-direct {p0}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->maybeRequery()V

    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mCursorAdapter:Lcom/google/android/talk/SearchHeaderCursorAdapter;

    invoke-virtual {v1}, Lcom/google/android/talk/SearchHeaderCursorAdapter;->getCursor()Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->deactivate()V

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->mMustRequeryCursor:Z

    invoke-super {p0}, Landroid/app/ListFragment;->onStop()V

    return-void
.end method
