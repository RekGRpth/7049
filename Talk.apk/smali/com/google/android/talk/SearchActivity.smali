.class public Lcom/google/android/talk/SearchActivity;
.super Landroid/app/Activity;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/SearchActivity$SearchResultsFragment;
    }
.end annotation


# static fields
.field private static CHAT_RESULT_FRAGMENT_TAG:Ljava/lang/String;

.field private static RESULTS_FRAGMENT_TAG:Ljava/lang/String;


# instance fields
.field private mConversationId:J

.field mSearchItem:Landroid/view/MenuItem;

.field mSearchView:Landroid/widget/SearchView;

.field private mSinglePane:Z

.field private mUsername:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "results_fragment"

    sput-object v0, Lcom/google/android/talk/SearchActivity;->RESULTS_FRAGMENT_TAG:Ljava/lang/String;

    const-string v0, "chat_result_fragment"

    sput-object v0, Lcom/google/android/talk/SearchActivity;->CHAT_RESULT_FRAGMENT_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/talk/SearchActivity;->RESULTS_FRAGMENT_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/talk/SearchActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/SearchActivity;

    invoke-direct {p0}, Lcom/google/android/talk/SearchActivity;->clearChatResult()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/talk/SearchActivity;J)V
    .locals 0
    .param p0    # Lcom/google/android/talk/SearchActivity;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/talk/SearchActivity;->setConversationId(J)V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/talk/SearchActivity;->CHAT_RESULT_FRAGMENT_TAG:Ljava/lang/String;

    return-object v0
.end method

.method private clearChatResult()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v2, Lcom/google/android/talk/SearchActivity;->CHAT_RESULT_FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/talk/fragments/SearchChatResultFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/talk/fragments/SearchChatResultFragment;->clear()V

    :cond_0
    return-void
.end method

.method private setConversationId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/talk/SearchActivity;->mConversationId:J

    return-void
.end method

.method private setupActionBar()V
    .locals 4

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x4

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v0, v3, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method

.method private setupSearchUI(Landroid/app/ActionBar;)V
    .locals 5
    .param p1    # Landroid/app/ActionBar;

    new-instance v1, Lcom/google/android/talk/SearchActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/talk/SearchActivity$1;-><init>(Lcom/google/android/talk/SearchActivity;)V

    new-instance v0, Lcom/google/android/talk/SearchActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/talk/SearchActivity$2;-><init>(Lcom/google/android/talk/SearchActivity;)V

    iget-object v2, p0, Lcom/google/android/talk/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v2, p0, Lcom/google/android/talk/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2, v0}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    iget-object v2, p0, Lcom/google/android/talk/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    const v3, 0x7f0c00a1

    invoke-virtual {p0, v3}, Lcom/google/android/talk/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/talk/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "query"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    return-void
.end method


# virtual methods
.method public getConversationId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/talk/SearchActivity;->mConversationId:J

    return-wide v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/SearchActivity;->mUsername:Ljava/lang/String;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const v4, 0x7f100077

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "from"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/talk/SearchActivity;->mUsername:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/talk/ActivityUtils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/talk/SearchActivity;->mSinglePane:Z

    iget-boolean v2, p0, Lcom/google/android/talk/SearchActivity;->mSinglePane:Z

    if-eqz v2, :cond_2

    const v2, 0x7f040030

    invoke-virtual {p0, v2}, Lcom/google/android/talk/SearchActivity;->setContentView(I)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "show_fragment"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/talk/SearchActivity;->mSinglePane:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    if-eqz v0, :cond_3

    new-instance v2, Lcom/google/android/talk/fragments/SearchChatResultFragment;

    invoke-direct {v2}, Lcom/google/android/talk/fragments/SearchChatResultFragment;-><init>()V

    sget-object v3, Lcom/google/android/talk/SearchActivity;->CHAT_RESULT_FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v1, v4, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    :goto_2
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/talk/SearchActivity;->setupActionBar()V

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const v2, 0x7f040031

    invoke-virtual {p0, v2}, Lcom/google/android/talk/SearchActivity;->setContentView(I)V

    goto :goto_1

    :cond_3
    new-instance v2, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;

    invoke-direct {v2}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;-><init>()V

    sget-object v3, Lcom/google/android/talk/SearchActivity;->RESULTS_FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v1, v4, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f1000b7

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/SearchActivity;->mSearchItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/android/talk/SearchActivity;->mSearchItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/google/android/talk/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/talk/SearchActivity;->setupSearchUI(Landroid/app/ActionBar;)V

    const/4 v1, 0x1

    return v1
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/talk/SearchActivity;->clearChatResult()V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v2, Lcom/google/android/talk/SearchActivity;->RESULTS_FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;

    if-eqz v1, :cond_0

    const-string v2, "onNewIntent"

    invoke-virtual {v1, p1, v2}, Lcom/google/android/talk/SearchActivity$SearchResultsFragment;->doSearchQuery(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->finish()V

    goto :goto_0

    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/talk/GTalkPreferencesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/google/android/talk/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "accountId"

    const-string v4, "accountId"

    const-wide/16 v5, 0x0

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "username"

    const-string v3, "username"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/talk/SearchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_2
    const-string v1, "talk_search"

    invoke-static {p0, v1}, Lcom/google/android/talk/HelpUtils;->showHelp(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f1000af -> :sswitch_1
        0x7f1000b0 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x1

    return v0
.end method
