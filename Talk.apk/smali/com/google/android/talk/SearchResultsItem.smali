.class public Lcom/google/android/talk/SearchResultsItem;
.super Landroid/widget/LinearLayout;
.source "SearchResultsItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/SearchResultsItem$1;
    }
.end annotation


# instance fields
.field private mDate:Landroid/widget/TextView;

.field private mPersonalLevel:Landroid/widget/ImageView;

.field private mSender:Landroid/widget/TextView;

.field private mSnippet:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private init()V
    .locals 1

    const v0, 0x7f100083

    invoke-virtual {p0, v0}, Lcom/google/android/talk/SearchResultsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/talk/SearchResultsItem;->mSender:Landroid/widget/TextView;

    const v0, 0x7f10006e

    invoke-virtual {p0, v0}, Lcom/google/android/talk/SearchResultsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/talk/SearchResultsItem;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f100081

    invoke-virtual {p0, v0}, Lcom/google/android/talk/SearchResultsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/talk/SearchResultsItem;->mSnippet:Landroid/widget/TextView;

    const v0, 0x7f100084

    invoke-virtual {p0, v0}, Lcom/google/android/talk/SearchResultsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/talk/SearchResultsItem;->mDate:Landroid/widget/TextView;

    const v0, 0x7f100082

    invoke-virtual {p0, v0}, Lcom/google/android/talk/SearchResultsItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/talk/SearchResultsItem;->mPersonalLevel:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public final bind(Landroid/content/Context;Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;Z)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;
    .param p3    # Z

    iget-object v8, p0, Lcom/google/android/talk/SearchResultsItem;->mTitle:Landroid/widget/TextView;

    if-nez v8, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/SearchResultsItem;->init()V

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getAccount()Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/talk/SearchResultsItem;->mSender:Landroid/widget/TextView;

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getSubject()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/talk/SearchResultsItem;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getSnippet()Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/talk/SearchResultsItem;->mSnippet:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getDateMs()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/talk/SearchResultsItem;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v0, v1}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/talk/SearchResultsItem;->mDate:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/talk/GmailProviderWrapper$ConversationCursor;->getPersonalLevel()Lcom/google/android/talk/GmailProviderWrapper$PersonalLevel;

    move-result-object v4

    const/4 v3, 0x0

    sget-object v8, Lcom/google/android/talk/SearchResultsItem$1;->$SwitchMap$com$google$android$talk$GmailProviderWrapper$PersonalLevel:[I

    invoke-virtual {v4}, Lcom/google/android/talk/GmailProviderWrapper$PersonalLevel;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    :goto_0
    iget-object v8, p0, Lcom/google/android/talk/SearchResultsItem;->mPersonalLevel:Landroid/widget/ImageView;

    invoke-virtual {v8, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0, p3}, Lcom/google/android/talk/SearchResultsItem;->setActivated(Z)V

    return-void

    :pswitch_0
    const v3, 0x7f02004b

    goto :goto_0

    :pswitch_1
    const v3, 0x7f02004a

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
