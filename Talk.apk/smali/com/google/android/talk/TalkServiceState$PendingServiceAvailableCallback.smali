.class Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;
.super Lcom/google/android/talk/TalkServiceState$PendingCallback;
.source "TalkServiceState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/TalkServiceState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PendingServiceAvailableCallback"
.end annotation


# instance fields
.field final r:Lcom/google/android/talk/ServiceAvailableRunnable;

.field volatile service:Lcom/google/android/gtalkservice/IGTalkService;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/google/android/talk/ServiceAvailableRunnable;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
    .param p2    # Lcom/google/android/talk/ServiceAvailableRunnable;

    invoke-direct {p0, p1}, Lcom/google/android/talk/TalkServiceState$PendingCallback;-><init>(Landroid/os/Handler;)V

    iput-object p2, p0, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->r:Lcom/google/android/talk/ServiceAvailableRunnable;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->r:Lcom/google/android/talk/ServiceAvailableRunnable;

    iget-object v1, p0, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->service:Lcom/google/android/gtalkservice/IGTalkService;

    invoke-interface {v0, v1}, Lcom/google/android/talk/ServiceAvailableRunnable;->run(Lcom/google/android/gtalkservice/IGTalkService;)V

    return-void
.end method

.method setService(Lcom/google/android/gtalkservice/IGTalkService;)V
    .locals 0
    .param p1    # Lcom/google/android/gtalkservice/IGTalkService;

    iput-object p1, p0, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->service:Lcom/google/android/gtalkservice/IGTalkService;

    return-void
.end method
