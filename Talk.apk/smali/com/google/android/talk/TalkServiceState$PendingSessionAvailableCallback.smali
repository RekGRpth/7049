.class Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;
.super Lcom/google/android/talk/TalkServiceState$PendingCallback;
.source "TalkServiceState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/TalkServiceState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PendingSessionAvailableCallback"
.end annotation


# instance fields
.field final accountId:J

.field final r:Lcom/google/android/talk/SessionAvailableRunnable;

.field volatile service:Lcom/google/android/gtalkservice/IGTalkService;

.field volatile session:Lcom/google/android/gtalkservice/IImSession;


# direct methods
.method constructor <init>(Landroid/os/Handler;JLcom/google/android/talk/SessionAvailableRunnable;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
    .param p2    # J
    .param p4    # Lcom/google/android/talk/SessionAvailableRunnable;

    invoke-direct {p0, p1}, Lcom/google/android/talk/TalkServiceState$PendingCallback;-><init>(Landroid/os/Handler;)V

    iput-wide p2, p0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->accountId:J

    iput-object p4, p0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->r:Lcom/google/android/talk/SessionAvailableRunnable;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->r:Lcom/google/android/talk/SessionAvailableRunnable;

    iget-object v1, p0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->service:Lcom/google/android/gtalkservice/IGTalkService;

    iget-object v2, p0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->session:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v0, v1, v2}, Lcom/google/android/talk/SessionAvailableRunnable;->run(Lcom/google/android/gtalkservice/IGTalkService;Lcom/google/android/gtalkservice/IImSession;)V

    return-void
.end method

.method setServiceAndSession(Lcom/google/android/gtalkservice/IGTalkService;Lcom/google/android/gtalkservice/IImSession;)V
    .locals 0
    .param p1    # Lcom/google/android/gtalkservice/IGTalkService;
    .param p2    # Lcom/google/android/gtalkservice/IImSession;

    iput-object p1, p0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->service:Lcom/google/android/gtalkservice/IGTalkService;

    iput-object p2, p0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->session:Lcom/google/android/gtalkservice/IImSession;

    return-void
.end method
