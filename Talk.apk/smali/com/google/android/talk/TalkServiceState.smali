.class Lcom/google/android/talk/TalkServiceState;
.super Ljava/lang/Object;
.source "TalkServiceState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;,
        Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;,
        Lcom/google/android/talk/TalkServiceState$PendingCallback;
    }
.end annotation


# instance fields
.field private final mLock:Ljava/lang/Object;

.field private mService:Lcom/google/android/gtalkservice/IGTalkService;

.field private final mServiceAvailableListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mSessionAvailableListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mSessionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/gtalkservice/IImSession;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/TalkServiceState;->mSessionMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addServiceAvailableCallback(Landroid/os/Handler;Lcom/google/android/talk/ServiceAvailableRunnable;)V
    .locals 6
    .param p1    # Landroid/os/Handler;
    .param p2    # Lcom/google/android/talk/ServiceAvailableRunnable;

    new-instance v2, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;

    invoke-direct {v2, p1, p2}, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;-><init>(Landroid/os/Handler;Lcom/google/android/talk/ServiceAvailableRunnable;)V

    iget-object v5, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    invoke-virtual {v2, v4}, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->setService(Lcom/google/android/gtalkservice/IGTalkService;)V

    :goto_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->post()V

    :cond_0
    return-void

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v3, 0x1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;

    invoke-virtual {v4}, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->getTarget()Landroid/os/Handler;

    move-result-object v4

    if-ne v4, p1, :cond_4

    const/4 v3, 0x0

    :cond_2
    if-eqz v3, :cond_3

    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public addSessionAvailableCallback(Landroid/os/Handler;JLcom/google/android/talk/SessionAvailableRunnable;)V
    .locals 9
    .param p1    # Landroid/os/Handler;
    .param p2    # J
    .param p4    # Lcom/google/android/talk/SessionAvailableRunnable;

    new-instance v2, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;-><init>(Landroid/os/Handler;JLcom/google/android/talk/SessionAvailableRunnable;)V

    iget-object v7, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/google/android/talk/TalkServiceState;->mSessionMap:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gtalkservice/IImSession;

    iget-object v3, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    if-nez v4, :cond_0

    :try_start_1
    invoke-interface {v3, p2, p3}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, p2, p3, v4}, Lcom/google/android/talk/TalkServiceState;->setImSessionAvailable(JLcom/google/android/gtalkservice/IImSession;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_2
    iget-object v6, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    if-eqz v6, :cond_2

    if-eqz v4, :cond_2

    iget-object v6, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    invoke-virtual {v2, v6, v4}, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->setServiceAndSession(Lcom/google/android/gtalkservice/IGTalkService;Lcom/google/android/gtalkservice/IImSession;)V

    :goto_1
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->post()V

    :cond_1
    return-void

    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    :cond_2
    :try_start_4
    iget-object v6, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v5, 0x1

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_3

    iget-object v6, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;

    invoke-virtual {v6}, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->getTarget()Landroid/os/Handler;

    move-result-object v6

    if-ne v6, p1, :cond_5

    const/4 v5, 0x0

    :cond_3
    if-eqz v5, :cond_4

    iget-object v6, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catchall_1
    move-exception v6

    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v6

    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method public clearService()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v2

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    iget-object v1, p0, Lcom/google/android/talk/TalkServiceState;->mSessionMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-object v1, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const-string v1, "talk/Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TalkServiceState - clearService but pending service queue had "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " items."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/talk/TalkApp;->Logwtf(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public getService()Lcom/google/android/gtalkservice/IGTalkService;
    .locals 2

    iget-object v1, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeServiceAvailableCallback(Landroid/os/Handler;)V
    .locals 4
    .param p1    # Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;

    invoke-virtual {v2}, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->getTarget()Landroid/os/Handler;

    move-result-object v2

    if-ne v2, p1, :cond_1

    iget-object v2, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    monitor-exit v3

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public removeSessionAvailableCallback(Landroid/os/Handler;)V
    .locals 4
    .param p1    # Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;

    invoke-virtual {v2}, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->getTarget()Landroid/os/Handler;

    move-result-object v2

    if-ne v2, p1, :cond_1

    iget-object v2, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    monitor-exit v3

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setImSessionAvailable(JLcom/google/android/gtalkservice/IImSession;)V
    .locals 8
    .param p1    # J
    .param p3    # Lcom/google/android/gtalkservice/IImSession;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    if-nez v4, :cond_1

    const-string v4, "talk/Service"

    const-string v6, "setImSessionAvailable while mService == null"

    invoke-static {v4, v6}, Lcom/google/android/talk/TalkApp;->Logwtf(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v5

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mSessionMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v6, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    :goto_0
    if-ltz v2, :cond_3

    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;

    iget-wide v6, v0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->accountId:J

    cmp-long v4, v6, p1

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    invoke-virtual {v0, v4, p3}, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->setServiceAndSession(Lcom/google/android/gtalkservice/IGTalkService;Lcom/google/android/gtalkservice/IImSession;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_3
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;

    invoke-virtual {v0}, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->post()V

    goto :goto_1

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public setService(Lcom/google/android/gtalkservice/IGTalkService;)V
    .locals 12
    .param p1    # Lcom/google/android/gtalkservice/IGTalkService;

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/talk/TalkServiceState;->clearService()V

    :cond_0
    return-void

    :cond_1
    iget-object v9, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    iput-object p1, p0, Lcom/google/android/talk/TalkServiceState;->mService:Lcom/google/android/gtalkservice/IGTalkService;

    iget-object v8, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;

    iget-object v8, p0, Lcom/google/android/talk/TalkServiceState;->mServiceAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v4

    array-length v6, v2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v6, :cond_2

    aget-object v3, v2, v5

    invoke-virtual {v3, p1}, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->setService(Lcom/google/android/gtalkservice/IGTalkService;)V

    invoke-virtual {v3}, Lcom/google/android/talk/TalkServiceState$PendingServiceAvailableCallback;->post()V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v9, p0, Lcom/google/android/talk/TalkServiceState;->mLock:Ljava/lang/Object;

    monitor-enter v9

    :try_start_2
    iget-object v8, p0, Lcom/google/android/talk/TalkServiceState;->mSessionAvailableListeners:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;

    iget-wide v10, v3, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->accountId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-wide v10, v3, Lcom/google/android/talk/TalkServiceState$PendingSessionAvailableCallback;->accountId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_1
    move-exception v8

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v8

    :cond_4
    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-interface {p1, v8, v9}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9, v7}, Lcom/google/android/talk/TalkServiceState;->setImSessionAvailable(JLcom/google/android/gtalkservice/IImSession;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    :catch_0
    move-exception v8

    goto :goto_2
.end method
