.class public Lcom/google/android/talk/fragments/BuddyListFragment;
.super Lcom/google/android/talk/fragments/RosterListFragment;
.source "BuddyListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/fragments/BuddyListFragment$14;,
        Lcom/google/android/talk/fragments/BuddyListFragment$LocalCallStateClient;,
        Lcom/google/android/talk/fragments/BuddyListFragment$AnimatedRosterAdapter;,
        Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;,
        Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;,
        Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;,
        Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;,
        Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;,
        Lcom/google/android/talk/fragments/BuddyListFragment$Controller;,
        Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;
    }
.end annotation


# static fields
.field private static final SINGLE_CONTACT_USERNAME_PROJECTION:[Ljava/lang/String;

.field private static sInjectedAdapter:Lcom/google/android/talk/IRosterListAdapter;


# instance fields
.field private mCallStateClient:Lcom/google/android/videochat/CallStateClient;

.field private mCallStateUpdateHack:Ljava/lang/Runnable;

.field private mChatListener:Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

.field private mCloseAllChatsMenuItem:Landroid/view/MenuItem;

.field private mColumnAccount:I

.field private mColumnCapabilities:I

.field private mColumnContactId:I

.field private mColumnGroupChat:I

.field private mColumnNickname:I

.field private mColumnSubscStatus:I

.field private mColumnSubscType:I

.field private mColumnType:I

.field private mColumnUsername:I

.field private mConnectionListener:Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

.field mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

.field private mCurrentSearchString:Ljava/lang/String;

.field private mFilter:Landroid/content/IntentFilter;

.field private mFullSearchListener:Landroid/view/View$OnClickListener;

.field private mGroupChatInvitationListener:Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsOnline:Z

.field private mNarrow:Z

.field private mRosterListener:Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

.field private mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

.field private mSortChoiceDialog:Landroid/app/Dialog;

.field private mStatusButton:Landroid/widget/ImageButton;

.field private mUpdateStatusRunnable:Ljava/lang/Runnable;

.field private mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "username"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/talk/fragments/BuddyListFragment;->SINGLE_CONTACT_USERNAME_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/talk/fragments/RosterListFragment;-><init>()V

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$1;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mUpdateStatusRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$2;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mIsOnline:Z

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$13;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$13;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mFullSearchListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/talk/fragments/BuddyListFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->updateStatus()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/talk/fragments/BuddyListFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->accountInfoAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/talk/fragments/BuddyListFragment;)Lcom/google/android/videochat/VideoChatServiceBinder;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/talk/fragments/BuddyListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCurrentSearchString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/talk/fragments/BuddyListFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/BuddyListFragment;->logd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/talk/fragments/BuddyListFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->loadSelfStatus()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/talk/fragments/BuddyListFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/talk/fragments/BuddyListFragment;Z)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/BuddyListFragment;->setIsOnline(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/talk/fragments/BuddyListFragment;)Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCloseAllChatsMenuItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/talk/fragments/BuddyListFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateUpdateHack:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/talk/fragments/BuddyListFragment;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateUpdateHack:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/talk/fragments/BuddyListFragment;)Landroid/app/Dialog;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSortChoiceDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/talk/fragments/BuddyListFragment;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/BuddyListFragment;
    .param p1    # Landroid/app/Dialog;

    iput-object p1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSortChoiceDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method private accountInfoAvailable()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private blockContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-nez v0, :cond_1

    const-string v0, "talk"

    const-string v1, "[BuddyList] blockContact: mImSession is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/BuddyListFragment;->getUsernameFromMenuInfo(Landroid/widget/AdapterView$AdapterContextMenuInfo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v2, v0}, Lcom/google/android/gtalkservice/IImSession;->blockContact(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v1, p1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-static {v0, v1, v2}, Lcom/google/android/talk/DatabaseUtils;->removeChatsByContactId(Landroid/content/ContentResolver;J)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "talk"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BuddyList] blockContact caught "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private closeAllChats()V
    .locals 1

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$11;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$11;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->endActiveVideoOrVoiceChat(Ljava/lang/Runnable;)V

    return-void
.end method

.method private confirmRemoveSelectedContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v1, p1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-static {v0, v1, v2}, Lcom/google/android/talk/fragments/BuddyListFragment;->getUsernameForId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    const v4, 0x7f0c0022

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x1010355

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v0, 0x104000a

    new-instance v2, Lcom/google/android/talk/fragments/BuddyListFragment$9;

    invoke-direct {v2, p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$9;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;Ljava/lang/String;)V

    invoke-virtual {v3, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v0, 0x1040000

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private dismissStatusbarNotifications(Lcom/google/android/gtalkservice/IGTalkService;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-interface {p1, v0, v1}, Lcom/google/android/gtalkservice/IGTalkService;->dismissNotificationsForAccount(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/talk/ActivityUtils;->dismissPopupNotification(Landroid/app/Activity;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "talk"

    const-string v2, "dismissStatusbarNotifications caught "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private endActiveVideoOrVoiceChat(Ljava/lang/Runnable;)V
    .locals 4
    .param p1    # Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-interface {v2}, Lcom/google/android/talk/IRosterListAdapter;->getActiveCallState()Lcom/google/android/talk/RosterListAdapter$CallState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/talk/RosterListAdapter$CallState;->mBareJid:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;

    new-instance v3, Lcom/google/android/talk/fragments/BuddyListFragment$12;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/talk/fragments/BuddyListFragment$12;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3}, Lcom/google/android/videochat/VideoChatServiceBinder;->bind(Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private getFilterMode(Landroid/app/Activity;)Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;
    .locals 4
    .param p1    # Landroid/app/Activity;

    const/4 v3, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "gtalk-view-mode"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;->QUICK_CONTACTS:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    :goto_0
    return-object v2

    :cond_1
    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;->ALL_MINUS_HIDDEN:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    goto :goto_0
.end method

.method private static getStatusUrls(Ljava/lang/String;)[Landroid/text/style/URLSpan;
    .locals 4

    const/4 v3, 0x0

    if-nez p0, :cond_0

    new-array v0, v3, [Landroid/text/style/URLSpan;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v1

    const-class v2, Landroid/text/style/URLSpan;

    invoke-virtual {v0, v3, v1, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    goto :goto_0

    :cond_1
    new-array v0, v3, [Landroid/text/style/URLSpan;

    goto :goto_0
.end method

.method private static getUsernameForId(Landroid/content/Context;J)Ljava/lang/String;
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/TalkContract$Contacts;->CONTENT_URI_CONTACT_ID:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/talk/fragments/BuddyListFragment;->SINGLE_CONTACT_USERNAME_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v3

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    const-string v0, "getUsernameForId"

    invoke-static {v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->logEmptyCursor(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getUsernameFromMenuInfo(Landroid/widget/AdapterView$AdapterContextMenuInfo;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/widget/AdapterView$AdapterContextMenuInfo;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    iget-wide v1, p1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-static {v0, v1, v2}, Lcom/google/android/talk/fragments/BuddyListFragment;->getUsernameForId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private hideContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-nez v0, :cond_1

    const-string v0, "talk"

    const-string v1, "[BuddyList] hideContact: mImSession is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/BuddyListFragment;->getUsernameFromMenuInfo(Landroid/widget/AdapterView$AdapterContextMenuInfo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v1, v0}, Lcom/google/android/gtalkservice/IImSession;->hideContact(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "talk"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BuddyList] hideContact caught "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v1, v0}, Lcom/google/android/gtalkservice/IImSession;->clearContactFlags(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private isInvite(Landroid/database/Cursor;)Z
    .locals 6
    .param p1    # Landroid/database/Cursor;

    iget v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnSubscStatus:I

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v0, v4

    iget v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnSubscType:I

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v2, v4

    const-wide/16 v4, 0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_0

    const-wide/16 v4, 0x5

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static launchSearchActivity(Ljava/lang/String;Lcom/google/android/talk/TalkApp$AccountInfo;Landroid/app/Activity;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "from"

    iget-object v2, p1, Lcom/google/android/talk/TalkApp$AccountInfo;->username:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "accountId"

    iget-wide v2, p1, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-class v1, Lcom/google/android/talk/SearchActivity;

    invoke-virtual {v0, p2, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private launchStatusUrl(Landroid/widget/AdapterView$AdapterContextMenuInfo;I)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz p1, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget v2, p1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const-string v2, "status"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getStatusUrls(Ljava/lang/String;)[Landroid/text/style/URLSpan;

    move-result-object v0

    if-ltz p2, :cond_0

    array-length v2, v0

    if-ge p2, v2, :cond_0

    aget-object v0, v0, p2

    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v0, "com.android.browser.application_id"

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private leaveChat(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V
    .locals 7

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->accountInfoAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget v1, p1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const-string v1, "username"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "account"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "leaveChat: for contact "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0}, Lcom/google/android/talk/TalkApp;->getGTalkService()Lcom/google/android/gtalkservice/IGTalkService;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-interface {v0, v4, v5}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->getChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->createChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/gtalkservice/IChatSession;->leave()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onLeaveChat(JLjava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v0, "talk"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "leaveChat: no ChatSession found for contact "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v4, "talk"

    const-string v5, "leaveChat: caught "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_3
    :try_start_2
    const-string v0, "talk"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "leaveChat: no ImSession found for account id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v5

    iget-wide v5, v5, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method private loadSelfStatus()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->loadSelfStatusCursor()Lcom/google/android/talk/SelfStatusCursor;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "loadSelfStatus: null cursor returned!"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    invoke-virtual {v1}, Lcom/google/android/talk/SelfStatusCursor;->close()V

    :cond_2
    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-interface {v1, v0}, Lcom/google/android/talk/IRosterListAdapter;->setSelfStatusCursor(Landroid/database/Cursor;)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mUpdateStatusRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/talk/SelfStatusCursor;->setOnAvatarRunnable(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private loadSelfStatusCursor()Lcom/google/android/talk/SelfStatusCursor;
    .locals 5

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gtalkservice/Presence;->OFFLINE:Lcom/google/android/gtalkservice/Presence;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v0}, Lcom/google/android/gtalkservice/IImSession;->getPresence()Lcom/google/android/gtalkservice/Presence;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    new-instance v1, Lcom/google/android/talk/SelfStatusCursor;

    invoke-direct {v1, v3, v0, v2}, Lcom/google/android/talk/SelfStatusCursor;-><init>(Landroid/app/Activity;Lcom/google/android/gtalkservice/Presence;Lcom/google/android/talk/TalkApp$AccountInfo;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v2, "talk"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadSelfStatusCursor: caught "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private static logEmptyCursor(Ljava/lang/String;)V
    .locals 3

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BuddyList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": empty cursor, possibly low memory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private logd(Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BuddyList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private logout()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v0}, Lcom/google/android/gtalkservice/IImSession;->logout()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0}, Lcom/google/android/talk/TalkApp;->getActiveAccount()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    iget-wide v1, v1, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    iget-wide v3, v0, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    cmp-long v0, v1, v3

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-static {v0, v1}, Lcom/google/android/talk/AccountLoginUtils;->setInactiveAccount(Lcom/google/android/talk/TalkApp$AccountInfo;Lcom/google/android/talk/TalkApp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$10;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$10;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->endActiveVideoOrVoiceChat(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    :try_start_1
    const-string v0, "talk"

    const-string v1, "[BuddyList] logout: mImSession is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "talk"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BuddyList] logout: caught "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private logv(Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[BuddyList] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private pinContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;Z)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-nez v0, :cond_1

    const-string v0, "talk"

    const-string v1, "[BuddyList] pinContact: mImSession is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/BuddyListFragment;->getUsernameFromMenuInfo(Landroid/widget/AdapterView$AdapterContextMenuInfo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v1, v0}, Lcom/google/android/gtalkservice/IImSession;->pinContact(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "talk"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BuddyList] pinContact caught "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v1, v0}, Lcom/google/android/gtalkservice/IImSession;->clearContactFlags(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private registerCallStateListener()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$LocalCallStateClient;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$LocalCallStateClient;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    invoke-virtual {v0}, Lcom/google/android/videochat/CallStateClient;->startListening()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    invoke-virtual {v0}, Lcom/google/android/videochat/CallStateClient;->requestUpdate()V

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$6;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$6;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateUpdateHack:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateUpdateHack:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private registerForIntentBroadcast()V
    .locals 3

    const-string v0, "registerForIntentBroadcast"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.GTALK_AVATAR_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private setIsOnline(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mIsOnline:Z

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-interface {v1, p1}, Lcom/google/android/talk/IRosterListAdapter;->setIsOnline(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->updateStatus(Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mStatusButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mStatusButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method private setQuickContactsMenuItemState()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getFilterMode()Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getSortMode()Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    move-result-object v2

    const/4 v1, 0x0

    sget-object v3, Lcom/google/android/talk/fragments/BuddyListFragment$14;->$SwitchMap$com$google$android$talk$loaders$RosterListLoader$FilterMode:[I

    invoke-virtual {v0}, Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const v1, 0x7f0c001f

    goto :goto_0

    :pswitch_1
    const v1, 0x7f0c0020

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showSelectedContactInfo(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V
    .locals 3
    .param p1    # Landroid/widget/AdapterView$AdapterContextMenuInfo;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;

    invoke-direct {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;-><init>()V

    iget-wide v1, p1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    iput-wide v1, v0, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;->mContactId:J

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v1

    iget-wide v1, v1, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    iput-wide v1, v0, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;->mAccountId:J

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-interface {v1, v0}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onShowContactInfo(Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;)V

    goto :goto_0
.end method

.method private showSortChoicesDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x3

    new-array v6, v1, [Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ACTIVE_STATUS_ALPHABETICAL:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    aput-object v2, v6, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ACTIVE_ALPHABETICAL:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    aput-object v2, v6, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->RECENCY_ALPHABETICAL:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    aput-object v2, v6, v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-direct {v9, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c00f9

    invoke-virtual {v9, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    const v2, 0x7f04003f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    const v1, 0x7f100090

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    if-ge v3, v2, :cond_2

    invoke-virtual {v1, v3}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    aget-object v4, v7, v3

    aget-object v11, v8, v3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Landroid/text/SpannableString;

    invoke-direct {v12, v11}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    new-instance v13, Landroid/text/style/TextAppearanceSpan;

    const v14, 0x1030044

    invoke-direct {v13, v5, v14}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    new-instance v14, Landroid/text/style/TextAppearanceSpan;

    const v15, 0x1030046

    invoke-direct {v14, v5, v15}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v12, v13, v15, v4, v0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v13, 0x0

    invoke-virtual {v12, v14, v4, v11, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v2, v12}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    aget-object v4, v6, v3

    invoke-virtual {v4}, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ordinal()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setId(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getSortMode()Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    move-result-object v4

    aget-object v11, v6, v3

    if-ne v4, v11, :cond_1

    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    const v2, 0x7f100015

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/google/android/talk/fragments/BuddyListFragment$7;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/talk/fragments/BuddyListFragment$7;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/google/android/talk/fragments/BuddyListFragment$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$8;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;Landroid/widget/RadioGroup;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSortChoiceDialog:Landroid/app/Dialog;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSortChoiceDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0
.end method

.method private startSelfStatusActivityIfOnline()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->accountInfoAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mIsOnline:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v1

    iget-wide v1, v1, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-interface {v0, v1, v2}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onRequestSetSelfStatus(J)V

    goto :goto_0
.end method

.method private startVideoChat(I)V
    .locals 6
    .param p1    # I

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->startSelfStatusActivityIfOnline()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/talk/videochat/WifiPolicyUtils;->wifiRequiredForVideoChat(Landroid/content/ContentResolver;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/talk/videochat/WifiPolicyUtils;->showAlertIfNoWifi(Landroid/content/Context;Z)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_3
    iget v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnUsername:I

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnAccount:I

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v0, v1, v3}, Lcom/google/android/talk/videochat/VideoChatActivity;->startActivityToInitiate(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0
.end method

.method private startVoiceChat(I)V
    .locals 6
    .param p1    # I

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->startSelfStatusActivityIfOnline()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/talk/videochat/WifiPolicyUtils;->wifiRequiredForVideoChat(Landroid/content/ContentResolver;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/talk/videochat/WifiPolicyUtils;->showAlertIfNoWifi(Landroid/content/Context;Z)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_3
    iget v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnUsername:I

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnAccount:I

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v0, v1, v3}, Lcom/google/android/talk/BuddyListCombo;->startVoiceChat(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0
.end method

.method private unregisterCallStateListener()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    invoke-virtual {v0}, Lcom/google/android/videochat/CallStateClient;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCallStateClient:Lcom/google/android/videochat/CallStateClient;

    :cond_0
    return-void
.end method

.method private unregisterForIntentBroadcast()V
    .locals 2

    const-string v0, "unregisterForIntentBroadcast"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private updateStatus()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->updateStatus(Z)V

    return-void
.end method

.method private updateStatus(Z)V
    .locals 4
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->accountInfoAvailable()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v1

    iget-wide v1, v1, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/talk/AvatarCache;->getInstance(JZ)Lcom/google/android/talk/AvatarCache;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/talk/TalkApp$AccountInfo;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/talk/AvatarCache;->clear(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    invoke-interface {v1, v2}, Lcom/google/android/talk/IRosterListAdapter;->setSelfStatusCursor(Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method protected addRemoteListeners()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mConnectionListener:Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mConnectionListener:Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mConnectionListener:Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->addConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListener:Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListener:Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListener:Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->addRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mChatListener:Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mChatListener:Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mChatListener:Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->addRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mGroupChatInvitationListener:Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-direct {v0, p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;Lcom/google/android/talk/TalkApp;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mGroupChatInvitationListener:Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mGroupChatInvitationListener:Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->addGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0}, Lcom/google/android/talk/TalkApp;->getGTalkService()Lcom/google/android/gtalkservice/IGTalkService;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->dismissStatusbarNotifications(Lcom/google/android/gtalkservice/IGTalkService;)V

    :cond_4
    return-void

    :catch_0
    move-exception v0

    const-string v1, "talk"

    const-string v2, "addRemoteListeners caught "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public addToContacts(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-interface {v0, p2}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onAddToContacts(Ljava/lang/String;)V

    return-void
.end method

.method public cancelSearch()V
    .locals 2

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCurrentSearchString:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getFilterMode()Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getSortMode()Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)V

    return-void
.end method

.method protected createRosterListAdapter()Lcom/google/android/talk/IRosterListAdapter;
    .locals 7

    sget-object v3, Lcom/google/android/talk/fragments/BuddyListFragment;->sInjectedAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-eqz v3, :cond_1

    new-instance v1, Lcom/google/android/talk/fragments/BuddyListFragment$AnimatedRosterAdapter;

    sget-object v3, Lcom/google/android/talk/fragments/BuddyListFragment;->sInjectedAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-direct {v1, p0, v3}, Lcom/google/android/talk/fragments/BuddyListFragment$AnimatedRosterAdapter;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;Lcom/google/android/talk/IRosterListAdapter;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v3, v0, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    new-instance v6, Lcom/google/android/talk/fragments/BuddyListFragment$4;

    invoke-direct {v6, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$4;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    invoke-interface {v1, v3, v4, v5, v6}, Lcom/google/android/talk/IRosterListAdapter;->init(JLandroid/app/Activity;Ljava/lang/Runnable;)V

    :cond_0
    new-instance v3, Lcom/google/android/talk/fragments/BuddyListFragment$5;

    invoke-direct {v3, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$5;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    invoke-interface {v1, v3}, Lcom/google/android/talk/IRosterListAdapter;->setOnVideoButtonClickedListener(Lcom/google/android/talk/IRosterListAdapter$VideoButtonClickHandler;)V

    return-object v1

    :cond_1
    new-instance v2, Lcom/google/android/talk/RosterListAdapter;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/talk/RosterListAdapter;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/talk/fragments/BuddyListFragment$AnimatedRosterAdapter;

    invoke-direct {v1, p0, v2}, Lcom/google/android/talk/fragments/BuddyListFragment$AnimatedRosterAdapter;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;Lcom/google/android/talk/IRosterListAdapter;)V

    iget-boolean v3, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mNarrow:Z

    invoke-interface {v1, v3}, Lcom/google/android/talk/IRosterListAdapter;->setNarrowLayout(Z)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getFilterMode()Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getSortMode()Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/google/android/talk/IRosterListAdapter;->setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)V

    goto :goto_0
.end method

.method public doHistorySearch()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mFullSearchListener:Landroid/view/View$OnClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    return-void
.end method

.method protected getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    :cond_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    return-object v1
.end method

.method public getFilterMode()Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getFilterMode(Landroid/app/Activity;)Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    move-result-object v0

    return-object v0
.end method

.method public getSortMode()Lcom/google/android/talk/loaders/RosterListLoader$SortMode;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getSortMode(Landroid/app/Activity;)Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    move-result-object v0

    return-object v0
.end method

.method public getSortMode(Landroid/app/Activity;)Lcom/google/android/talk/loaders/RosterListLoader$SortMode;
    .locals 3

    sget-object v0, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ACTIVE_STATUS_ALPHABETICAL:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    invoke-virtual {v0}, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ordinal()I

    move-result v0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "gtalk-sort-mode"

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ACTIVE_STATUS_ALPHABETICAL:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    invoke-virtual {v2}, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    move v1, v0

    :goto_0
    const-class v0, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    aget-object v0, v0, v1

    return-object v0

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    invoke-super {p0, p1}, Lcom/google/android/talk/fragments/RosterListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/videochat/VideoChatServiceBinder;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/google/android/talk/videochat/VideoChatOutputReceiver;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/videochat/VideoChatServiceBinder;-><init>(Landroid/content/Context;Landroid/content/ComponentName;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mVcBinder:Lcom/google/android/videochat/VideoChatServiceBinder;

    invoke-virtual {p0, v5}, Lcom/google/android/talk/fragments/BuddyListFragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->createRosterListAdapter()Lcom/google/android/talk/IRosterListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/talk/IRosterListAdapter;->suppressVideoButton(Z)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getFilterMode()Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getSortMode()Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->setListAdapter(Lcom/google/android/talk/IRosterListAdapter;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    iget-boolean v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mNarrow:Z

    invoke-interface {v0, v1}, Lcom/google/android/talk/IRosterListAdapter;->setNarrowLayout(Z)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    instance-of v0, v0, Landroid/widget/AbsListView$RecyclerListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    check-cast v0, Landroid/widget/AbsListView$RecyclerListener;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-interface {v0, v5}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onDisplayProgress(Z)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const-string v1, "onAttach"

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->logd(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/google/android/talk/fragments/RosterListFragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    check-cast v0, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;->getBuddyListController()Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->setController(Lcom/google/android/talk/fragments/BuddyListFragment$Controller;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 11
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    const/4 v10, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v7

    check-cast v7, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    const/16 v1, 0x64

    if-lt v8, v1, :cond_0

    add-int/lit8 v0, v8, -0x64

    invoke-direct {p0, v7, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->launchStatusUrl(Landroid/widget/AdapterView$AdapterContextMenuInfo;I)V

    move v0, v10

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    if-eqz v7, :cond_1

    if-nez v6, :cond_2

    :cond_1
    move v0, v10

    goto :goto_0

    :cond_2
    iget-wide v0, v7, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-static {v6, v0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->getUsernameForId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v6, v9}, Lcom/google/android/talk/fragments/BuddyListFragment;->addToContacts(Landroid/app/Activity;Ljava/lang/String;)V

    move v0, v10

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v7}, Lcom/google/android/talk/fragments/BuddyListFragment;->confirmRemoveSelectedContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V

    move v0, v10

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v7}, Lcom/google/android/talk/fragments/BuddyListFragment;->blockContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V

    move v0, v10

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v7, v10}, Lcom/google/android/talk/fragments/BuddyListFragment;->hideContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;Z)V

    move v0, v10

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, v7, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->hideContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;Z)V

    move v0, v10

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, v7, v10}, Lcom/google/android/talk/fragments/BuddyListFragment;->pinContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;Z)V

    move v0, v10

    goto :goto_0

    :pswitch_6
    invoke-direct {p0, v7, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->pinContact(Landroid/widget/AdapterView$AdapterContextMenuInfo;Z)V

    move v0, v10

    goto :goto_0

    :pswitch_7
    invoke-direct {p0, v7}, Lcom/google/android/talk/fragments/BuddyListFragment;->showSelectedContactInfo(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V

    move v0, v10

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget v3, v7, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/talk/fragments/BuddyListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    move v0, v10

    goto :goto_0

    :pswitch_9
    iget v0, v7, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->startVoiceChat(I)V

    move v0, v10

    goto :goto_0

    :pswitch_a
    iget v0, v7, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->startVideoChat(I)V

    move v0, v10

    goto :goto_0

    :pswitch_b
    invoke-direct {p0, v7}, Lcom/google/android/talk/fragments/BuddyListFragment;->leaveChat(Landroid/widget/AdapterView$AdapterContextMenuInfo;)V

    move v0, v10

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 18
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    move-object/from16 v1, p3

    check-cast v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget v11, v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    if-nez v11, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v11

    iget v12, v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v11, v12}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/talk/fragments/BuddyListFragment;->isInvite(Landroid/database/Cursor;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "last_message_date"

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v11, 0x0

    cmp-long v11, v6, v11

    if-eqz v11, :cond_2

    const/4 v8, 0x1

    :goto_1
    const-string v11, "status"

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/talk/fragments/BuddyListFragment;->getStatusUrls(Ljava/lang/String;)[Landroid/text/style/URLSpan;

    move-result-object v9

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnNickname:I

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnType:I

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v11, 0x1

    if-eq v10, v11, :cond_0

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnCapabilities:I

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v11, 0x2

    if-ne v10, v11, :cond_3

    const/4 v11, 0x0

    const/16 v12, 0xa

    const/4 v13, 0x0

    const v14, 0x7f0c0016

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v11, 0x0

    const/16 v12, 0xb

    const/4 v13, 0x0

    const v14, 0x7f0c0017

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :goto_2
    const/4 v11, 0x2

    if-eq v10, v11, :cond_0

    const/4 v11, 0x0

    const/16 v12, 0x8

    const/4 v13, 0x0

    const v14, 0x7f0c001e

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v11

    const/16 v12, 0x40

    invoke-interface {v11, v12}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    const/4 v11, 0x5

    if-ne v10, v11, :cond_a

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    const v14, 0x7f0c000f

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :goto_3
    const/4 v11, 0x4

    if-ne v10, v11, :cond_b

    const/4 v11, 0x0

    const/4 v12, 0x6

    const/4 v13, 0x0

    const v14, 0x7f0c0011

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    :cond_3
    if-eqz v8, :cond_6

    invoke-static {v2}, Lcom/google/android/talk/ActivityUtils;->isAudioChatCapable(I)Z

    move-result v11

    if-eqz v11, :cond_4

    const/4 v11, 0x0

    const/16 v12, 0xc

    const/4 v13, 0x0

    const v14, 0x7f0c0013

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_4
    invoke-static {v2}, Lcom/google/android/talk/ActivityUtils;->isVideoChatCapable(I)Z

    move-result v11

    if-eqz v11, :cond_5

    const/4 v11, 0x0

    const/16 v12, 0xd

    const/4 v13, 0x0

    const v14, 0x7f0c0014

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_5
    const/4 v11, 0x0

    const/16 v12, 0xa

    const/4 v13, 0x0

    const v14, 0x7f0c0016

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v11, 0x0

    const/16 v12, 0xb

    const/4 v13, 0x0

    const v14, 0x7f0c0017

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v11, 0x0

    const/4 v12, 0x7

    const/4 v13, 0x0

    const v14, 0x7f0c0018

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v11, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    const v14, 0x7f0c000d

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    goto/16 :goto_2

    :cond_6
    const/4 v11, 0x0

    const/16 v12, 0x9

    const/4 v13, 0x0

    const v14, 0x7f0c0012

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    invoke-static {v2}, Lcom/google/android/talk/ActivityUtils;->isAudioChatCapable(I)Z

    move-result v11

    if-eqz v11, :cond_7

    const/4 v11, 0x0

    const/16 v12, 0xc

    const/4 v13, 0x0

    const v14, 0x7f0c0013

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_7
    invoke-static {v2}, Lcom/google/android/talk/ActivityUtils;->isVideoChatCapable(I)Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v11, 0x0

    const/16 v12, 0xd

    const/4 v13, 0x0

    const v14, 0x7f0c0014

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_8
    const/4 v11, 0x0

    const/4 v12, 0x7

    const/4 v13, 0x0

    const v14, 0x7f0c0018

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    array-length v11, v9

    if-lez v11, :cond_9

    if-eqz v3, :cond_9

    const/4 v5, 0x0

    :goto_4
    array-length v11, v9

    if-ge v5, v11, :cond_9

    const/4 v11, 0x0

    add-int/lit8 v12, v5, 0x64

    const/4 v13, 0x0

    const v14, 0x7f0c0019

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aget-object v17, v9, v5

    invoke-virtual/range {v17 .. v17}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v3, v14, v15}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_9
    const/4 v11, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    const v14, 0x7f0c000d

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    const v14, 0x7f0c000c

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    goto/16 :goto_2

    :cond_a
    const/4 v11, 0x0

    const/4 v12, 0x3

    const/4 v13, 0x0

    const v14, 0x7f0c000e

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    goto/16 :goto_3

    :cond_b
    const/4 v11, 0x0

    const/4 v12, 0x5

    const/4 v13, 0x0

    const v14, 0x7f0c0010

    move-object/from16 v0, p1

    invoke-interface {v0, v11, v12, v13, v14}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const-string v2, "onCreateLoader"

    invoke-direct {p0, v2}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->accountInfoAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/talk/loaders/RosterListLoader;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->getFilterMode(Landroid/app/Activity;)Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    move-result-object v4

    invoke-virtual {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->getSortMode(Landroid/app/Activity;)Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    move-result-object v5

    new-instance v6, Lcom/google/android/talk/fragments/BuddyListFragment$3;

    invoke-direct {v6, p0}, Lcom/google/android/talk/fragments/BuddyListFragment$3;-><init>(Lcom/google/android/talk/fragments/BuddyListFragment;)V

    invoke-direct/range {v0 .. v6}, Lcom/google/android/talk/loaders/RosterListLoader;-><init>(Landroid/content/Context;JLcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    const v0, 0x7f0f0003

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1000ba

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCloseAllChatsMenuItem:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCloseAllChatsMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCloseAllChatsMenuItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-interface {v1}, Lcom/google/android/talk/IRosterListAdapter;->hasActiveChats()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCloseAllChatsMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/talk/fragments/RosterListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mList:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-virtual {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->setListAdapter(Lcom/google/android/talk/IRosterListAdapter;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->updateStatus()V

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCurrentSearchString:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mList:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->requestFocus()Z

    :cond_1
    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 5
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCurrentSearchString:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p3, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->startSelfStatusActivityIfOnline()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v1, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;

    iget v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnUsername:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnAccount:I

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;-><init>(Ljava/lang/String;J)V

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->isInvite(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-interface {v2, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onMakeInvite(Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnGroupChat:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnContactId:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;->mContactId:J

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-interface {v2, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onOpenGroupChat(Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-interface {v2, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onOpenChat(Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mList:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const-string v0, "onLoadFinished"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/google/android/talk/fragments/BuddyListFragment;->setData(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->loadSelfStatus()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->setEnabled(Z)V

    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/talk/fragments/BuddyListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->accountInfoAvailable()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v3

    iget-wide v3, v3, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-interface {v1, v3, v4}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onInviteFriend(J)V

    move v1, v2

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->showSortChoicesDialog()V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;

    invoke-direct {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v1

    iget-wide v3, v1, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    iput-wide v3, v0, Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;->mAccountId:J

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    invoke-interface {v1, v0}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onShowInvites(Lcom/google/android/talk/fragments/BuddyListFragment$ListItemInfo;)V

    move v1, v2

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->logout()V

    move v1, v2

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->closeAllChats()V

    move v1, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f1000b8
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/talk/fragments/RosterListFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    invoke-virtual {v0}, Lcom/google/android/talk/SelfStatusCursor;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/talk/fragments/RosterListFragment;->onResume()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->loadSelfStatus()V

    return-void
.end method

.method public onSessionCreated(Lcom/google/android/gtalkservice/IImSession;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/google/android/talk/fragments/RosterListFragment;->onSessionCreated(Lcom/google/android/gtalkservice/IImSession;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0, p1}, Lcom/google/android/talk/TalkApp;->asyncPruneOldChatsAndMessages(Lcom/google/android/gtalkservice/IImSession;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->updateStatus()V

    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gtalkservice/IImSession;->getPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/talk/TalkApp$AccountInfo;->username:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "login for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/google/android/gtalkservice/IImSession;->login(Ljava/lang/String;Z)V

    :cond_2
    const-string v0, "onSessionCreated: request batch presence"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gtalkservice/IImSession;->requestBatchedBuddyPresence()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->loadSelfStatus()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "talk"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IImSession login: caught "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->loadSelfStatus()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->loadSelfStatus()V

    throw v0
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/talk/fragments/RosterListFragment;->onStart()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->registerForIntentBroadcast()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->registerCallStateListener()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/talk/fragments/RosterListFragment;->onStop()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->unregisterForIntentBroadcast()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->unregisterCallStateListener()V

    invoke-static {}, Lcom/google/android/talk/RosterListItem;->onStop()V

    return-void
.end method

.method public reload()V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->getLoader(I)Landroid/content/Loader;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/loaders/RosterListLoader;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/talk/loaders/RosterListLoader;->forceLoad()V

    :cond_0
    return-void
.end method

.method protected removeRemoteListeners()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mConnectionListener:Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mConnectionListener:Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->removeConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mConnectionListener:Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

    invoke-virtual {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;->clearRefs()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mConnectionListener:Lcom/google/android/talk/fragments/BuddyListFragment$ConnectionStateListener;

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListener:Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListener:Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->removeRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListener:Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

    invoke-virtual {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;->clearRefs()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListener:Lcom/google/android/talk/fragments/BuddyListFragment$RosterListener;

    :cond_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mChatListener:Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mChatListener:Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->removeRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mChatListener:Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

    invoke-virtual {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;->clearRefs()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mChatListener:Lcom/google/android/talk/fragments/BuddyListFragment$ChatListener;

    :cond_2
    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mGroupChatInvitationListener:Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mGroupChatInvitationListener:Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->removeGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mGroupChatInvitationListener:Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;

    invoke-virtual {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;->clearRefs()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mGroupChatInvitationListener:Lcom/google/android/talk/fragments/BuddyListFragment$GroupChatInvitationListener;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "talk"

    const-string v2, "removeRemoteListeners caught "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setController(Lcom/google/android/talk/fragments/BuddyListFragment$Controller;)V
    .locals 0
    .param p1    # Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    iput-object p1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    return-void
.end method

.method public setData(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    iget-object v1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCurrentSearchString:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/talk/IRosterListAdapter;->changeCursor(Landroid/database/Cursor;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mController:Lcom/google/android/talk/fragments/BuddyListFragment$Controller;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment$Controller;->onDisplayProgress(Z)V

    :cond_0
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnContactId:I

    const-string v0, "username"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnUsername:I

    const-string v0, "nickname"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnNickname:I

    const-string v0, "account"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnAccount:I

    const-string v0, "type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnType:I

    const-string v0, "subscriptionStatus"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnSubscStatus:I

    const-string v0, "subscriptionType"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnSubscType:I

    const-string v0, "groupchat"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnGroupChat:I

    const-string v0, "cap"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mColumnCapabilities:I

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->restoreListViewState()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)V
    .locals 7
    .param p1    # Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;
    .param p2    # Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v3}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v5, "gtalk-view-mode"

    sget-object v6, Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;->QUICK_CONTACTS:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    if-ne p1, v6, :cond_2

    :goto_1
    invoke-interface {v1, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v3, "gtalk-sort-mode"

    invoke-virtual {p2}, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ordinal()I

    move-result v5

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->setQuickContactsMenuItemState()V

    iget-object v3, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getFilterMode()Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getSortMode()Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lcom/google/android/talk/IRosterListAdapter;->setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)V

    invoke-virtual {p0, v4}, Lcom/google/android/talk/fragments/BuddyListFragment;->getLoader(I)Landroid/content/Loader;

    move-result-object v2

    check-cast v2, Lcom/google/android/talk/loaders/RosterListLoader;

    if-eqz v2, :cond_1

    invoke-virtual {v2, p1, p2}, Lcom/google/android/talk/loaders/RosterListLoader;->setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;)V

    invoke-virtual {v2}, Lcom/google/android/talk/loaders/RosterListLoader;->resetProjection()V

    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1
.end method

.method public setFocus(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-interface {v0, p1}, Lcom/google/android/talk/IRosterListAdapter;->setFocus(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public setListAdapter(Lcom/google/android/talk/IRosterListAdapter;)V
    .locals 1
    .param p1    # Lcom/google/android/talk/IRosterListAdapter;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/android/talk/IRosterListAdapter;->setIncludeSelfItem(Z)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/talk/fragments/RosterListFragment;->setListAdapter(Lcom/google/android/talk/IRosterListAdapter;)V

    return-void
.end method

.method public setNarrowMode(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mNarrow:Z

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-interface {v0, p1}, Lcom/google/android/talk/IRosterListAdapter;->setNarrowLayout(Z)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->updateStatus(Z)V

    return-void
.end method

.method public setQueryString(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCurrentSearchString:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/talk/fragments/BuddyListFragment;->getLoader(I)Landroid/content/Loader;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/loaders/RosterListLoader;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;->ALL:Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;

    sget-object v2, Lcom/google/android/talk/loaders/RosterListLoader$SortMode;->ACTIVE_ALPHABETICAL:Lcom/google/android/talk/loaders/RosterListLoader$SortMode;

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/talk/loaders/RosterListLoader;->setFilterMode(Lcom/google/android/talk/loaders/RosterListLoader$FilterMode;Lcom/google/android/talk/loaders/RosterListLoader$SortMode;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public softUiReset()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCreated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mList:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mDontRestoreListViewState:Z

    iget-object v0, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v0}, Lcom/google/android/talk/TalkApp;->getGTalkService()Lcom/google/android/gtalkservice/IGTalkService;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/BuddyListFragment;->dismissStatusbarNotifications(Lcom/google/android/gtalkservice/IGTalkService;)V

    :cond_0
    return-void
.end method

.method public switchAccounts(Lcom/google/android/talk/TalkApp$AccountInfo;)V
    .locals 7
    .param p1    # Lcom/google/android/talk/TalkApp$AccountInfo;

    const-string v4, "switchAccounts"

    invoke-direct {p0, v4}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mCreated:Z

    if-nez v4, :cond_1

    const-string v4, "switchAccounts: fragment\'s onCreate not called yet"

    invoke-direct {p0, v4}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/android/talk/fragments/BuddyListFragment;->getLoader(I)Landroid/content/Loader;

    move-result-object v3

    check-cast v3, Lcom/google/android/talk/loaders/RosterListLoader;

    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-nez v4, :cond_4

    :cond_2
    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    if-nez v3, :cond_3

    const-string v4, "switchAccounts: loader is NULL"

    invoke-direct {p0, v4}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    :cond_3
    iget-object v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    if-nez v4, :cond_0

    const-string v4, "switchAccounts: mRosterListAdapter is NULL"

    invoke-direct {p0, v4}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v4

    iget-wide v0, v4, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    iget-object v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mRosterListAdapter:Lcom/google/android/talk/IRosterListAdapter;

    invoke-interface {v4, v0, v1}, Lcom/google/android/talk/IRosterListAdapter;->setAccountId(J)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->removeRemoteListeners()V

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    :try_start_0
    iget-object v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v4}, Lcom/google/android/talk/TalkApp;->getGTalkService()Lcom/google/android/gtalkservice/IGTalkService;

    move-result-object v4

    invoke-interface {v4, v0, v1}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-eqz v4, :cond_5

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->loadSelfStatus()V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->addRemoteListeners()V

    const-string v4, "switchAccounts: request batch presence"

    invoke-direct {p0, v4}, Lcom/google/android/talk/fragments/BuddyListFragment;->logv(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/talk/fragments/BuddyListFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v4}, Lcom/google/android/gtalkservice/IImSession;->requestBatchedBuddyPresence()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_1
    invoke-virtual {v3, v0, v1}, Lcom/google/android/talk/loaders/RosterListLoader;->switchAccounts(J)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->unregisterCallStateListener()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/BuddyListFragment;->registerCallStateListener()V

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v4, "talk"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "switchAccounts: caught "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
