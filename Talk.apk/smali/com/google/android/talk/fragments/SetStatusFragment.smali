.class public Lcom/google/android/talk/fragments/SetStatusFragment;
.super Landroid/app/Fragment;
.source "SetStatusFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;,
        Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;,
        Lcom/google/android/talk/fragments/SetStatusFragment$ReadSettingsTask;,
        Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;,
        Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;,
        Lcom/google/android/talk/fragments/SetStatusFragment$Host;,
        Lcom/google/android/talk/fragments/SetStatusFragment$Controller;
    }
.end annotation


# static fields
.field private static final PHOTO_DIR:Ljava/io/File;


# instance fields
.field private mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

.field private mActivity:Landroid/app/Activity;

.field private mApp:Lcom/google/android/talk/TalkApp;

.field private mAsyncReadSettingsTask:Lcom/google/android/talk/fragments/SetStatusFragment$ReadSettingsTask;

.field private mAudioChatEnabled:Z

.field private mAvatarPicker:Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;

.field private mAvatarView:Landroid/widget/ImageView;

.field private mColumnAvatarData:I

.field private mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

.field private mController:Lcom/google/android/talk/fragments/SetStatusFragment$Controller;

.field private mCreated:Z

.field private mCustomStatusEditButton:Landroid/view/View;

.field private mCustomStatusHistoryButton:Landroid/widget/Button;

.field private mCustomStatusText:Landroid/widget/TextView;

.field private mEnabled:Z

.field mFilter:Landroid/content/IntentFilter;

.field private mHandler:Landroid/os/Handler;

.field private mHaveAvatar:Z

.field private mImSession:Lcom/google/android/gtalkservice/IImSession;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mListenersRegistered:Z

.field private mOfflineWarning:Landroid/widget/TextView;

.field mOnActivityResultRunnable:Ljava/lang/Runnable;

.field private mPaused:Z

.field private mPendingAvatarUpdate:Landroid/graphics/Bitmap;

.field private mPendingAvatarUpdateDeleteMagic:Landroid/graphics/Bitmap;

.field private mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

.field mRoot:Landroid/view/View;

.field private mRosterListener:Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

.field private mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

.field private mSettings:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

.field private mStandardStatusButton:Landroid/widget/Button;

.field private mStandardStatusButtonBackground:Landroid/view/View;

.field private mUsername:Landroid/widget/TextView;

.field private mVideoChatEnabled:Z

.field mViewsToDisableWhenOffline:[Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/DCIM/Camera"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/talk/fragments/SetStatusFragment;->PHOTO_DIR:Ljava/io/File;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdateDeleteMagic:Landroid/graphics/Bitmap;

    iput-boolean v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mEnabled:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mListenersRegistered:Z

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$12;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/SetStatusFragment$12;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdateDeleteMagic:Landroid/graphics/Bitmap;

    iput-boolean v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mEnabled:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mListenersRegistered:Z

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$12;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/SetStatusFragment$12;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->initialize()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->enableUI()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/talk/fragments/SetStatusFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mVideoChatEnabled:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/talk/fragments/SetStatusFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mVideoChatEnabled:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/android/talk/fragments/SetStatusFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAudioChatEnabled:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/android/talk/fragments/SetStatusFragment;Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;)Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    iput-object p1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSettings:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->clearStatusLists()V

    return-void
.end method

.method static synthetic access$1502(Lcom/google/android/talk/fragments/SetStatusFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->savePendingAvatarChanges()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/talk/fragments/SetStatusFragment;)Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAvatarPicker:Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/talk/fragments/SetStatusFragment;)Lcom/google/android/talk/TalkApp;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/talk/fragments/SetStatusFragment;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->disableUI()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/talk/fragments/SetStatusFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->uploadAvatar()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/talk/fragments/SetStatusFragment;)Lcom/google/android/talk/fragments/SetStatusFragment$Controller;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mController:Lcom/google/android/talk/fragments/SetStatusFragment$Controller;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/talk/fragments/SetStatusFragment;Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/SetStatusFragment;->makeCustomStatusItems(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/talk/fragments/SetStatusFragment;Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/SetStatusFragment;->makeDefaultStatusItems(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/talk/fragments/SetStatusFragment;Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/SetStatusFragment;->handlePresenceChoice(Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/talk/fragments/SetStatusFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->saveStatusMessageIfChanged()V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/talk/fragments/SetStatusFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mHaveAvatar:Z

    return v0
.end method

.method static synthetic access$2800(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->discardAvatar()V

    return-void
.end method

.method static synthetic access$2900()Ljava/io/File;
    .locals 1

    sget-object v0, Lcom/google/android/talk/fragments/SetStatusFragment;->PHOTO_DIR:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/talk/fragments/SetStatusFragment;)Lcom/google/android/talk/TalkApp$AccountInfo;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/talk/fragments/SetStatusFragment;)Lcom/google/android/gtalkservice/IImSession;
    .locals 1
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/talk/fragments/SetStatusFragment;Lcom/google/android/gtalkservice/IImSession;)Lcom/google/android/gtalkservice/IImSession;
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Lcom/google/android/gtalkservice/IImSession;

    iput-object p1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->initializeFromImSession()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->registerListeners()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/talk/fragments/SetStatusFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/talk/fragments/SetStatusFragment;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->bindViews()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/talk/fragments/SetStatusFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/fragments/SetStatusFragment;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->checkRunnable()V

    return-void
.end method

.method private asyncLoadSettings()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->waitForAsyncTasksToComplete()V

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$ReadSettingsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/talk/fragments/SetStatusFragment$ReadSettingsTask;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Lcom/google/android/talk/fragments/SetStatusFragment$1;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAsyncReadSettingsTask:Lcom/google/android/talk/fragments/SetStatusFragment$ReadSettingsTask;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAsyncReadSettingsTask:Lcom/google/android/talk/fragments/SetStatusFragment$ReadSettingsTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SetStatusFragment$ReadSettingsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private bindViews()V
    .locals 11

    const/4 v10, 0x0

    const/4 v8, 0x0

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButton:Landroid/widget/Button;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-static {v7}, Lcom/google/android/talk/util/PresenceUtils;->translatePresence(Lcom/google/android/gtalkservice/Presence;)I

    move-result v4

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mUsername:Landroid/widget/TextView;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mUsername:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    iget-object v9, v9, Lcom/google/android/talk/TalkApp$AccountInfo;->username:Ljava/lang/String;

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v7, v4, v8}, Lcom/google/android/talk/TalkApp;->getStatusIcon(II)I

    move-result v5

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v9, v9, Lcom/google/android/talk/TalkApp;->mDefaultStatusStrings:[Ljava/lang/String;

    aget-object v9, v9, v4

    invoke-virtual {v7, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButton:Landroid/widget/Button;

    const/4 v9, 0x6

    invoke-virtual {v7, v9}, Landroid/widget/Button;->setCompoundDrawablePadding(I)V

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButton:Landroid/widget/Button;

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-virtual {v7, v9, v10, v10, v10}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButtonBackground:Landroid/view/View;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButtonBackground:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v9, v4}, Lcom/google/android/talk/TalkApp;->getStatusColorId(I)I

    move-result v9

    invoke-virtual {v7, v9}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_3
    const/4 v1, 0x0

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    iget-object v9, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdateDeleteMagic:Landroid/graphics/Bitmap;

    if-eq v7, v9, :cond_4

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_5

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    :cond_4
    :goto_1
    if-eqz v1, :cond_6

    const/4 v7, 0x1

    :goto_2
    iput-boolean v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mHaveAvatar:Z

    iget-boolean v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mHaveAvatar:Z

    if-eqz v7, :cond_7

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    :goto_3
    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAvatarView:Landroid/widget/ImageView;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v9}, Lcom/google/android/gtalkservice/Presence;->getStatus()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v7}, Lcom/google/android/gtalkservice/Presence;->getDefaultStatusList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    iget-object v9, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v9}, Lcom/google/android/gtalkservice/Presence;->getDndStatusList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int v2, v7, v9

    if-lez v2, :cond_8

    move v3, v8

    :goto_4
    iget-boolean v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mEnabled:Z

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusHistoryButton:Landroid/widget/Button;

    invoke-virtual {v7, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    iget v9, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mColumnAvatarData:I

    invoke-static {v7, v9}, Lcom/google/android/talk/DatabaseUtils;->getAvatarFromCursor(Landroid/database/Cursor;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    :cond_6
    move v7, v8

    goto :goto_2

    :cond_7
    iget-object v7, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v7}, Lcom/google/android/talk/TalkApp;->getGenericAvatar()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_3

    :cond_8
    const/16 v3, 0x8

    goto :goto_4
.end method

.method private checkRunnable()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mOnActivityResultRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mOnActivityResultRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mOnActivityResultRunnable:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private clearStatusLists()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/Presence;->clearStatusLists()V

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-interface {v1, v2}, Lcom/google/android/gtalkservice/IImSession;->setPresence(Lcom/google/android/gtalkservice/Presence;)V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->initializeFromImSession()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "talk"

    const-string v2, "RemoteException failure"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private disableUI()V
    .locals 7

    iget-object v5, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    monitor-enter v5

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mEnabled:Z

    if-nez v4, :cond_0

    monitor-exit v5

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPaused:Z

    if-eqz v4, :cond_2

    :cond_1
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mViewsToDisableWhenOffline:[Landroid/view/View;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    if-eqz v3, :cond_3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mOfflineWarning:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusHistoryButton:Landroid/widget/Button;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mEnabled:Z

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private discardAvatar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdateDeleteMagic:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->bindViews()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->savePendingAvatarChanges()V

    return-void
.end method

.method private enableUI()V
    .locals 7

    const/4 v6, 0x1

    iget-boolean v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mEnabled:Z

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mViewsToDisableWhenOffline:[Landroid/view/View;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    if-eqz v3, :cond_1

    invoke-virtual {v3, v6}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mOfflineWarning:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusHistoryButton:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    iput-boolean v6, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mEnabled:Z

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->initialize()V

    goto :goto_0
.end method

.method private getCapabilities()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSettings:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSettings:Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;

    invoke-virtual {v0}, Lcom/google/android/gsf/TalkContract$AccountSettings$QueryMap;->getCapabilities()I

    move-result v0

    goto :goto_0
.end method

.method public static getCropImageIntent(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 5

    const/16 v4, 0x60

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.camera.action.CROP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "image/*"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "crop"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method public static getPhotoPickIntent()Landroid/content/Intent;
    .locals 5

    const/16 v4, 0x60

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "crop"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method public static getTakePickIntent(Ljava/io/File;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "output"

    invoke-static {p0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method private handlePresenceChoice(Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;)V
    .locals 4
    .param p1    # Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    const/4 v0, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p1, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->mRunnable:Ljava/lang/Runnable;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->mRunnable:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_1
    iget-object v1, p1, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->mShow:Lcom/google/android/gtalkservice/Presence$Show;

    iget-boolean v2, p1, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->mInvisible:Z

    invoke-virtual {v0, v2}, Lcom/google/android/gtalkservice/Presence;->setInvisible(Z)Z

    sget-object v2, Lcom/google/android/gtalkservice/Presence$Show;->NONE:Lcom/google/android/gtalkservice/Presence$Show;

    if-ne v1, v2, :cond_2

    sget-object v1, Lcom/google/android/gtalkservice/Presence$Show;->AVAILABLE:Lcom/google/android/gtalkservice/Presence$Show;

    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gtalkservice/Presence;->setAvailable(Z)V

    iget-boolean v2, p1, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->mCustomPresence:Z

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->mStatus:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gtalkservice/Presence;->setStatus(Lcom/google/android/gtalkservice/Presence$Show;Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getCapabilities()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gtalkservice/Presence;->setCapabilities(I)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v2, v0}, Lcom/google/android/gtalkservice/IImSession;->setPresence(Lcom/google/android/gtalkservice/Presence;)V

    iget-boolean v2, p1, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->mCustomPresence:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->mStatus:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->savePendingAvatarChanges()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gtalkservice/Presence;->setStatus(Lcom/google/android/gtalkservice/Presence$Show;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method private initViews()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f10000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAvatarView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f10000f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f10008c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButtonBackground:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f100087

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f100089

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusHistoryButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f100088

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusEditButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f10008a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mOfflineWarning:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v2, 0x7f10001c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mUsername:Landroid/widget/TextView;

    const/4 v1, 0x5

    new-array v1, v1, [Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAvatarView:Landroid/widget/ImageView;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButton:Landroid/widget/Button;

    aput-object v2, v1, v5

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusEditButton:Landroid/view/View;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusHistoryButton:Landroid/widget/Button;

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mViewsToDisableWhenOffline:[Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mStandardStatusButton:Landroid/widget/Button;

    invoke-direct {p0, v1, v4}, Lcom/google/android/talk/fragments/SetStatusFragment;->setupPresencePopdown(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusHistoryButton:Landroid/widget/Button;

    invoke-direct {p0, v1, v5}, Lcom/google/android/talk/fragments/SetStatusFragment;->setupPresencePopdown(Landroid/view/View;Z)V

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$9;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/SetStatusFragment$9;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusEditButton:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusEditButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    instance-of v1, v1, Landroid/widget/EditText;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAvatarView:Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/talk/fragments/SetStatusFragment$10;

    invoke-direct {v2, p0}, Lcom/google/android/talk/fragments/SetStatusFragment$10;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private initialize()V
    .locals 5

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/talk/fragments/SetStatusFragment$1;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    iget-wide v3, v3, Lcom/google/android/talk/TalkApp$AccountInfo;->accountId:J

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/talk/TalkApp;->addImSessionAvailableCallback(Landroid/os/Handler;JLcom/google/android/talk/SessionAvailableRunnable;)V

    return-void
.end method

.method private initializeFromImSession()V
    .locals 5

    const/4 v3, 0x0

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v1}, Lcom/google/android/gtalkservice/IImSession;->getPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    invoke-virtual {v1}, Lcom/google/android/talk/SelfStatusCursor;->close()V

    iput-object v3, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    :cond_0
    new-instance v1, Lcom/google/android/talk/SelfStatusCursor;

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/talk/SelfStatusCursor;-><init>(Landroid/app/Activity;Lcom/google/android/gtalkservice/Presence;Lcom/google/android/talk/TalkApp$AccountInfo;)V

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    const-string v2, "avatars_data"

    invoke-virtual {v1, v2}, Lcom/google/android/talk/SelfStatusCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mColumnAvatarData:I

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mSelfCursor:Lcom/google/android/talk/SelfStatusCursor;

    new-instance v2, Lcom/google/android/talk/fragments/SetStatusFragment$11;

    invoke-direct {v2, p0}, Lcom/google/android/talk/fragments/SetStatusFragment$11;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/talk/SelfStatusCursor;->setOnAvatarRunnable(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "talk"

    const-string v2, "unable to get presence"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mController:Lcom/google/android/talk/fragments/SetStatusFragment$Controller;

    invoke-interface {v1}, Lcom/google/android/talk/fragments/SetStatusFragment$Controller;->finish()V

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 4

    const-string v0, "talk"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SetStatusFragment."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private makeAdapter(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/widget/ArrayAdapter;
    .locals 8
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;",
            ">;)",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;",
            ">;"
        }
    .end annotation

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$4;

    const v3, 0x7f04003c

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p2

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/talk/fragments/SetStatusFragment$4;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Landroid/content/Context;ILjava/util/List;Ljava/util/ArrayList;Landroid/view/LayoutInflater;Landroid/content/Context;)V

    return-object v0
.end method

.method private makeCustomStatusItems(Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .locals 17
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    move-object/from16 v16, v0

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gtalkservice/Presence;->getDefaultStatusList()Ljava/util/List;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    new-instance v2, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    sget-object v5, Lcom/google/android/gtalkservice/Presence$Show;->AVAILABLE:Lcom/google/android/gtalkservice/Presence$Show;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    const/4 v7, 0x5

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Lcom/google/android/talk/TalkApp;->getStatusIcon(II)I

    move-result v7

    const/4 v8, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Ljava/lang/String;Lcom/google/android/gtalkservice/Presence$Show;ZIZ)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gtalkservice/Presence;->getDndStatusList()Ljava/util/List;

    move-result-object v13

    if-eqz v13, :cond_1

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    new-instance v2, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    sget-object v5, Lcom/google/android/gtalkservice/Presence$Show;->DND:Lcom/google/android/gtalkservice/Presence$Show;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Lcom/google/android/talk/TalkApp;->getStatusIcon(II)I

    move-result v7

    const/4 v8, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Ljava/lang/String;Lcom/google/android/gtalkservice/Presence$Show;ZIZ)V

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v5, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0c0026

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/gtalkservice/Presence$Show;->NONE:Lcom/google/android/gtalkservice/Presence$Show;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v6, p0

    invoke-direct/range {v5 .. v11}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Ljava/lang/String;Lcom/google/android/gtalkservice/Presence$Show;ZIZ)V

    new-instance v2, Lcom/google/android/talk/fragments/SetStatusFragment$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/talk/fragments/SetStatusFragment$6;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;)V

    invoke-virtual {v5, v2}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->setClickRunnable(Ljava/lang/Runnable;)V

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14}, Lcom/google/android/talk/fragments/SetStatusFragment;->makeAdapter(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/widget/ArrayAdapter;

    move-result-object v2

    return-object v2
.end method

.method private makeDefaultStatusItems(Landroid/content/Context;)Landroid/widget/ArrayAdapter;
    .locals 14
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;",
            ">;"
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v4, 0x0

    iget-object v13, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0c0001

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gtalkservice/Presence$Show;->AVAILABLE:Lcom/google/android/gtalkservice/Presence$Show;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    const/4 v5, 0x5

    invoke-virtual {v1, v5, v4}, Lcom/google/android/talk/TalkApp;->getStatusIcon(II)I

    move-result v5

    move-object v1, p0

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Ljava/lang/String;Lcom/google/android/gtalkservice/Presence$Show;ZIZ)V

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0c0002

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gtalkservice/Presence$Show;->DND:Lcom/google/android/gtalkservice/Presence$Show;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    const/4 v5, 0x4

    invoke-virtual {v1, v5, v4}, Lcom/google/android/talk/TalkApp;->getStatusIcon(II)I

    move-result v5

    move-object v1, p0

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Ljava/lang/String;Lcom/google/android/gtalkservice/Presence$Show;ZIZ)V

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0c0005

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/gtalkservice/Presence$Show;->AVAILABLE:Lcom/google/android/gtalkservice/Presence$Show;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    invoke-virtual {v1, v9, v4}, Lcom/google/android/talk/TalkApp;->getStatusIcon(II)I

    move-result v10

    move-object v6, p0

    move v11, v4

    invoke-direct/range {v5 .. v11}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Ljava/lang/String;Lcom/google/android/gtalkservice/Presence$Show;ZIZ)V

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0c0008

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gtalkservice/Presence$Show;->NONE:Lcom/google/android/gtalkservice/Presence$Show;

    move-object v1, p0

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Ljava/lang/String;Lcom/google/android/gtalkservice/Presence$Show;ZIZ)V

    new-instance v1, Lcom/google/android/talk/fragments/SetStatusFragment$5;

    invoke-direct {v1, p0}, Lcom/google/android/talk/fragments/SetStatusFragment$5;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/talk/fragments/SetStatusFragment$StatusItem;->setClickRunnable(Ljava/lang/Runnable;)V

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1, v12}, Lcom/google/android/talk/fragments/SetStatusFragment;->makeAdapter(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    return-object v1
.end method

.method private registerForIntentBroadcast()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.GTALK_AVATAR_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private registerListeners()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mListenersRegistered:Z

    if-nez v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->addConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRosterListener:Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRosterListener:Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRosterListener:Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->addRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mListenersRegistered:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private savePendingAvatarChanges()V
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdateDeleteMagic:Landroid/graphics/Bitmap;

    if-ne v3, v4, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    move-object v0, v2

    :goto_1
    if-eqz v1, :cond_0

    iput-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    :cond_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/google/android/talk/fragments/SetStatusFragment$7;

    invoke-direct {v3, p0, v0}, Lcom/google/android/talk/fragments/SetStatusFragment$7;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Landroid/graphics/Bitmap;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    :goto_2
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingAvatarUpdate:Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->bindViews()V

    goto :goto_2
.end method

.method private saveStatusMessageIfChanged()V
    .locals 6

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v4}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v4}, Lcom/google/android/gtalkservice/Presence;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v4}, Lcom/google/android/gtalkservice/Presence;->getShow()Lcom/google/android/gtalkservice/Presence$Show;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    sget-object v4, Lcom/google/android/gtalkservice/Presence$Show;->NONE:Lcom/google/android/gtalkservice/Presence$Show;

    if-eq v1, v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v5, v4}, Lcom/google/android/gtalkservice/Presence;->setAvailable(Z)V

    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-virtual {v4, v1, v2}, Lcom/google/android/gtalkservice/Presence;->setStatus(Lcom/google/android/gtalkservice/Presence$Show;Ljava/lang/String;)V

    :try_start_0
    iget-object v4, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v5, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPendingPresenceUpdate:Lcom/google/android/gtalkservice/Presence;

    invoke-interface {v4, v5}, Lcom/google/android/gtalkservice/IImSession;->setPresence(Lcom/google/android/gtalkservice/Presence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->savePendingAvatarChanges()V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v4, "talk"

    const-string v5, "RemoteException failure"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method private setupPresencePopdown(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$8;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/talk/fragments/SetStatusFragment$8;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;ZLandroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private unregisterForIntentBroadcast()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private unregisterListeners()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->removeConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    invoke-virtual {v0}, Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;->clearRefs()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mConnectionStateListener:Lcom/google/android/talk/fragments/SetStatusFragment$ConnectionStateListener;

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRosterListener:Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRosterListener:Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

    invoke-interface {v0, v1}, Lcom/google/android/gtalkservice/IImSession;->removeRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRosterListener:Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

    invoke-virtual {v0}, Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;->clearRefs()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRosterListener:Lcom/google/android/talk/fragments/SetStatusFragment$RosterListener;

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mListenersRegistered:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private uploadAvatar()Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mImSession:Lcom/google/android/gtalkservice/IImSession;

    invoke-interface {v1}, Lcom/google/android/gtalkservice/IImSession;->uploadAvatarFromDb()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "talk"

    const-string v2, "RemoteException failure"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private waitForAsyncReadSettingsToComplete()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAsyncReadSettingsTask:Lcom/google/android/talk/fragments/SetStatusFragment$ReadSettingsTask;

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/SetStatusFragment;->waitForAsyncTaskToComplete(Landroid/os/AsyncTask;)V

    return-void
.end method

.method private waitForAsyncTaskToComplete(Landroid/os/AsyncTask;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/AsyncTask",
            "<***>;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/os/AsyncTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Talk"

    const-string v2, "SetStatusFragment.waitForAsyncTaskToComplete get error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private waitForAsyncTasksToComplete()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->waitForAsyncReadSettingsToComplete()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$3;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/google/android/talk/fragments/SetStatusFragment$3;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;IILandroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mOnActivityResultRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->checkRunnable()V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-static {}, Lcom/google/android/talk/TalkApp;->debugLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "onAttach"

    invoke-direct {p0, v1}, Lcom/google/android/talk/fragments/SetStatusFragment;->log(Ljava/lang/String;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    check-cast v0, Lcom/google/android/talk/fragments/SetStatusFragment$Host;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/SetStatusFragment$Host;->getSetStatusController()Lcom/google/android/talk/fragments/SetStatusFragment$Controller;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mController:Lcom/google/android/talk/fragments/SetStatusFragment$Controller;

    iput-object p1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->checkRunnable()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    const v3, 0x7f100085

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b002c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/talk/TalkApp;->getApplication(Landroid/app/Activity;)Lcom/google/android/talk/TalkApp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    new-instance v0, Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;

    invoke-direct {v0, p0, p1}, Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;-><init>(Lcom/google/android/talk/fragments/SetStatusFragment;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAvatarPicker:Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCreated:Z

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/talk/ActivityUtils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f04003a

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->initViews()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    return-object v0

    :cond_0
    const v0, 0x7f04003b

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mRoot:Landroid/view/View;

    goto :goto_0
.end method

.method public onHiddenChanged(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->saveStatusMessageIfChanged()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mPaused:Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAvatarPicker:Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;

    invoke-virtual {v0, p1}, Lcom/google/android/talk/fragments/SetStatusFragment$AvatarPicker;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;

    invoke-interface {v0}, Lcom/google/android/talk/fragments/BuddyListFragment$BuddyListHost;->getAccountInfo()Lcom/google/android/talk/TalkApp$AccountInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->asyncLoadSettings()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->initialize()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->registerForIntentBroadcast()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->checkRunnable()V

    return-void
.end method

.method public onStop()V
    .locals 3

    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->saveStatusMessageIfChanged()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->unregisterListeners()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->unregisterForIntentBroadcast()V

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mApp:Lcom/google/android/talk/TalkApp;

    iget-object v2, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/google/android/talk/TalkApp;->removeImSessionAvailableCallback(Landroid/os/Handler;)V

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCustomStatusText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method public switchAccounts(Lcom/google/android/talk/TalkApp$AccountInfo;)V
    .locals 1
    .param p1    # Lcom/google/android/talk/TalkApp$AccountInfo;

    iget-boolean v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mCreated:Z

    if-nez v0, :cond_1

    const-string v0, "switchAccounts: fragment\'s onCreate not called yet"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/SetStatusFragment;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->unregisterListeners()V

    iget-object v0, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "switchAccounts: mActivity is NULL"

    invoke-direct {p0, v0}, Lcom/google/android/talk/fragments/SetStatusFragment;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->waitForAsyncTasksToComplete()V

    iput-object p1, p0, Lcom/google/android/talk/fragments/SetStatusFragment;->mAccountInfo:Lcom/google/android/talk/TalkApp$AccountInfo;

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->asyncLoadSettings()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->initialize()V

    invoke-direct {p0}, Lcom/google/android/talk/fragments/SetStatusFragment;->initViews()V

    goto :goto_0
.end method
