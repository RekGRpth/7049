.class public Lcom/google/android/talk/videochat/CameraTracker;
.super Ljava/lang/Object;
.source "CameraTracker.java"


# instance fields
.field private mCameraIdList:[I

.field private mCameraSizeOverrides:[Lcom/google/android/videochat/Size;

.field private mNumAvailableCameras:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/CameraTracker;->inspectCameras(Landroid/content/Context;)V

    return-void
.end method

.method private inspectCameras(Landroid/content/Context;)V
    .locals 23
    .param p1    # Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string v21, "gtalk_vc_force_camera_ids"

    invoke-static/range {v20 .. v21}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string v21, "gtalk_vc_force_camera_dimensions"

    invoke-static/range {v20 .. v21}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v4, :cond_4

    const-string v20, ","

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    array-length v0, v14

    move/from16 v20, v0

    if-nez v20, :cond_2

    const-string v20, "talk"

    const-string v21, "Camera override empty, disabling"

    invoke-static/range {v20 .. v21}, Lcom/google/android/talk/TalkApp;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    move/from16 v20, v0

    if-eqz v20, :cond_1

    if-eqz v6, :cond_1

    const-string v20, ","

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v0, v8

    move/from16 v20, v0

    and-int/lit8 v20, v20, 0x1

    if-eqz v20, :cond_8

    const-string v20, "talk"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Non-multiple of 2 length in gtalk_vc_force_camera_dimensions "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/google/android/talk/TalkApp;->LOGE(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    array-length v7, v14

    new-array v0, v7, [I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/talk/videochat/CameraTracker;->mCameraIdList:[I

    move-object v3, v14

    array-length v0, v3

    move/from16 v16, v0

    const/4 v12, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v12, v0, :cond_0

    aget-object v15, v3, v12

    :try_start_0
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    const/16 v20, -0x1

    move/from16 v0, v20

    if-eq v13, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraIdList:[I

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    move/from16 v21, v0

    aput v13, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :catch_0
    move-exception v9

    const-string v20, "talk"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "bad id "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " in "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/google/android/talk/TalkApp;->LOGE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    new-instance v5, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v5}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v7

    new-array v0, v7, [I

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/talk/videochat/CameraTracker;->mCameraIdList:[I

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    const/4 v10, 0x0

    :goto_2
    const/16 v20, 0x3

    move/from16 v0, v20

    if-ge v10, v0, :cond_0

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v7, :cond_7

    invoke-static {v11, v5}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    const/16 v17, 0x0

    packed-switch v10, :pswitch_data_0

    iget v0, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    iget v0, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    move/from16 v20, v0

    if-eqz v20, :cond_5

    const/16 v17, 0x1

    :cond_5
    :goto_4
    if-eqz v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraIdList:[I

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    move/from16 v21, v0

    aput v11, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :pswitch_0
    iget v0, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    const/16 v17, 0x1

    goto :goto_4

    :pswitch_1
    iget v0, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    move/from16 v20, v0

    if-nez v20, :cond_5

    const/16 v17, 0x1

    goto :goto_4

    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    move/from16 v20, v0

    array-length v0, v8

    move/from16 v21, v0

    div-int/lit8 v21, v21, 0x2

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->min(II)I

    move-result v19

    if-lez v19, :cond_1

    move/from16 v0, v19

    new-array v0, v0, [Lcom/google/android/videochat/Size;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/talk/videochat/CameraTracker;->mCameraSizeOverrides:[Lcom/google/android/videochat/Size;

    const/4 v11, 0x0

    :goto_5
    move/from16 v0, v19

    if-ge v11, v0, :cond_1

    const/16 v18, 0x0

    :try_start_1
    new-instance v18, Lcom/google/android/videochat/Size;

    mul-int/lit8 v20, v11, 0x2

    aget-object v20, v8, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    mul-int/lit8 v21, v11, 0x2

    add-int/lit8 v21, v21, 0x1

    aget-object v21, v8, v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/videochat/Size;-><init>(II)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_6
    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/videochat/Size;->width:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_9

    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/videochat/Size;->height:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_a

    :cond_9
    const/16 v18, 0x0

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraSizeOverrides:[Lcom/google/android/videochat/Size;

    move-object/from16 v20, v0

    aput-object v18, v20, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    :catch_1
    move-exception v9

    const/16 v18, 0x0

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public firstCamera()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraIdList:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getNumAvailableCameras()I
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    return v0
.end method

.method public getOverrideSizeForCamera(I)Lcom/google/android/videochat/Size;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraSizeOverrides:[Lcom/google/android/videochat/Size;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraSizeOverrides:[Lcom/google/android/videochat/Size;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraSizeOverrides:[Lcom/google/android/videochat/Size;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public nextCamera(I)I
    .locals 4
    .param p1    # I

    const/4 v2, -0x1

    const/4 v0, -0x1

    const/4 v1, 0x0

    :goto_0
    iget v3, p0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraIdList:[I

    aget v3, v3, v1

    if-ne v3, p1, :cond_1

    move v0, v1

    :cond_0
    if-ne v0, v2, :cond_2

    :goto_1
    return v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget v2, p0, Lcom/google/android/talk/videochat/CameraTracker;->mNumAvailableCameras:I

    if-ne v0, v2, :cond_3

    const/4 v0, 0x0

    :cond_3
    iget-object v2, p0, Lcom/google/android/talk/videochat/CameraTracker;->mCameraIdList:[I

    aget v2, v2, v0

    goto :goto_1
.end method
