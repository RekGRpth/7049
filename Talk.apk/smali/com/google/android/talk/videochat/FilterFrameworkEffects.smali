.class public Lcom/google/android/talk/videochat/FilterFrameworkEffects;
.super Ljava/lang/Object;
.source "FilterFrameworkEffects.java"


# instance fields
.field private mActiveBuffer:[I

.field private mActiveFbo:[I

.field private mActiveProgram:[I

.field private mEffect:Landroid/media/effect/Effect;

.field private mEffectContext:Landroid/media/effect/EffectContext;

.field private mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;


# direct methods
.method public constructor <init>(Lcom/google/android/talk/videochat/FilterSetupInputParameters;)V
    .locals 2
    .param p1    # Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    iput-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mActiveFbo:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mActiveProgram:[I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mActiveBuffer:[I

    invoke-direct {p0}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->setupEnvironment()V

    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->activateEffect(Lcom/google/android/talk/videochat/FilterSetupInputParameters;)V

    return-void
.end method

.method private activateEffect(Lcom/google/android/talk/videochat/FilterSetupInputParameters;)V
    .locals 3
    .param p1    # Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectId:I

    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget v1, v1, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectId:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectId:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    iput-object p1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget v0, p1, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectId:I

    iget-object v1, p1, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectParam:Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->setupEffect(ILjava/lang/Object;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    const-string v2, "currentEffect"

    iget-object v0, p1, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectParam:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "Talk:ffw"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting funny face effect: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p1, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectParam:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget-object v1, p1, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectParam:Ljava/lang/Object;

    iput-object v1, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectParam:Ljava/lang/Object;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public static isEffectAvailable(I)Z
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const-string v0, "com.google.android.media.effect.effects.VCOEffect"

    invoke-static {v0}, Landroid/media/effect/EffectFactory;->isEffectSupported(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    const-string v0, "com.google.android.media.effect.effects.GoofyFaceEffect"

    invoke-static {v0}, Landroid/media/effect/EffectFactory;->isEffectSupported(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    const-string v0, "android.media.effect.effects.BackDropperEffect"

    invoke-static {v0}, Landroid/media/effect/EffectFactory;->isEffectSupported(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private setupEffect(ILjava/lang/Object;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->isEffectAvailable(I)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Talk:ffw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Effect not available: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/talk/TalkApp;->LOGW(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->releaseEffect()V

    packed-switch p1, :pswitch_data_0

    :goto_1
    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    if-nez v1, :cond_0

    const-string v1, "Talk:ffw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not initialize the effect "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/talk/TalkApp;->LOGE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffectContext:Landroid/media/effect/EffectContext;

    invoke-virtual {v1}, Landroid/media/effect/EffectContext;->getFactory()Landroid/media/effect/EffectFactory;

    move-result-object v1

    const-string v2, "com.google.android.media.effect.effects.GoofyFaceEffect"

    invoke-virtual {v1, v2}, Landroid/media/effect/EffectFactory;->createEffect(Ljava/lang/String;)Landroid/media/effect/Effect;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    const-string v2, "currentEffect"

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffectContext:Landroid/media/effect/EffectContext;

    invoke-virtual {v1}, Landroid/media/effect/EffectContext;->getFactory()Landroid/media/effect/EffectFactory;

    move-result-object v1

    const-string v2, "com.google.android.media.effect.effects.VCOEffect"

    invoke-virtual {v1, v2}, Landroid/media/effect/EffectFactory;->createEffect(Ljava/lang/String;)Landroid/media/effect/Effect;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    const-string v2, "maxOutputSize"

    const/16 v3, 0x140

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_2
    move-object v0, p2

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffectContext:Landroid/media/effect/EffectContext;

    invoke-virtual {v1}, Landroid/media/effect/EffectContext;->getFactory()Landroid/media/effect/EffectFactory;

    move-result-object v1

    const-string v2, "android.media.effect.effects.BackDropperEffect"

    invoke-virtual {v1, v2}, Landroid/media/effect/EffectFactory;->createEffect(Ljava/lang/String;)Landroid/media/effect/Effect;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    const-string v2, "source"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    iget-object v2, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget-object v2, v2, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectCallback:Landroid/media/effect/EffectUpdateListener;

    invoke-virtual {v1, v2}, Landroid/media/effect/Effect;->setUpdateListener(Landroid/media/effect/EffectUpdateListener;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private setupEnvironment()V
    .locals 1

    invoke-static {}, Landroid/media/effect/EffectContext;->createWithCurrentGlContext()Landroid/media/effect/EffectContext;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffectContext:Landroid/media/effect/EffectContext;

    return-void
.end method


# virtual methods
.method public release()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->releaseEffect()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffectContext:Landroid/media/effect/EffectContext;

    invoke-virtual {v0}, Landroid/media/effect/EffectContext;->release()V

    return-void
.end method

.method public releaseEffect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    invoke-virtual {v0}, Landroid/media/effect/Effect;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    :cond_0
    return-void
.end method

.method public renderFrame()Z
    .locals 6

    :try_start_0
    iget-object v1, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    iget-object v2, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget v2, v2, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->inputTextureName:I

    iget-object v3, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget v3, v3, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->inputTextureWidth:I

    iget-object v4, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget v4, v4, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->inputTextureHeight:I

    iget-object v5, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget v5, v5, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->outputTextureName:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/media/effect/Effect;->apply(IIII)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    const-string v1, "Talk:ffw"

    const-string v2, "Error running effect"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    throw v0
.end method

.method public setInputTextureName(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iput p1, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->inputTextureName:I

    return-void
.end method

.method public setTimestamp(J)V
    .locals 3
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    invoke-virtual {v0}, Landroid/media/effect/Effect;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.media.effect.effects.VCOEffect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mEffect:Landroid/media/effect/Effect;

    const-string v1, "timestamp"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public switchEffect(Lcom/google/android/talk/videochat/FilterSetupInputParameters;)I
    .locals 1
    .param p1    # Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->activateEffect(Lcom/google/android/talk/videochat/FilterSetupInputParameters;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->mSetupParams:Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    iget v0, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->outputTextureName:I

    return v0
.end method
