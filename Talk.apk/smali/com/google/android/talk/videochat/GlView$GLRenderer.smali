.class Lcom/google/android/talk/videochat/GlView$GLRenderer;
.super Ljava/lang/Object;
.source "GlView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/talk/videochat/GlView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GLRenderer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/videochat/GlView$GLRenderer$SelfRendererCallback;,
        Lcom/google/android/talk/videochat/GlView$GLRenderer$RemoteRendererCallback;,
        Lcom/google/android/talk/videochat/GlView$GLRenderer$EffectCallback;
    }
.end annotation


# instance fields
.field private mCameraIsOpened:Z

.field private mCameraTextureName:I

.field mCount:I

.field private mCurrentEffectId:I

.field private mCurrentEffectParam:Ljava/lang/Object;

.field private mDeviceOrientation:I

.field private final mEffectCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$EffectCallback;

.field private mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

.field private mFilterFrameworkActive:Z

.field private mFilterFrameworkOutputTextureName:I

.field private mHaveInitializedRemoteTexture:Z

.field private mIsInitialRemoteFrame:Z

.field private mIsInitialSelfFrame:Z

.field mLastFrameFrozen:Z

.field private final mOwningView:Lcom/google/android/talk/videochat/GlView;

.field private final mRemoteBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

.field private final mRemoteCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$RemoteRendererCallback;

.field private final mRemoteFrameData:Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

.field private final mRemoteRenderer:Lcom/google/android/videochat/RemoteRenderer;

.field private final mRemoteScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

.field private final mSelfBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

.field private final mSelfCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$SelfRendererCallback;

.field private final mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

.field private final mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

.field private mViewRect:Landroid/graphics/Rect;

.field private final mWindowManager:Landroid/view/WindowManager;

.field final synthetic this$0:Lcom/google/android/talk/videochat/GlView;


# direct methods
.method public constructor <init>(Lcom/google/android/talk/videochat/GlView;Lcom/google/android/talk/videochat/GlView;Lcom/google/android/videochat/CameraSpecification;)V
    .locals 2
    .param p2    # Lcom/google/android/talk/videochat/GlView;
    .param p3    # Lcom/google/android/videochat/CameraSpecification;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mIsInitialSelfFrame:Z

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mIsInitialRemoteFrame:Z

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mLastFrameFrozen:Z

    iput v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCount:I

    const-string v0, "Talk:GlView"

    const-string v1, "GlView.GLRenderer constructor"

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/talk/videochat/GlView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mWindowManager:Landroid/view/WindowManager;

    new-instance v0, Lcom/google/android/talk/videochat/GlView$GLRenderer$RemoteRendererCallback;

    invoke-direct {v0, p0}, Lcom/google/android/talk/videochat/GlView$GLRenderer$RemoteRendererCallback;-><init>(Lcom/google/android/talk/videochat/GlView$GLRenderer;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$RemoteRendererCallback;

    new-instance v0, Lcom/google/android/talk/videochat/GlView$GLRenderer$SelfRendererCallback;

    invoke-direct {v0, p0}, Lcom/google/android/talk/videochat/GlView$GLRenderer$SelfRendererCallback;-><init>(Lcom/google/android/talk/videochat/GlView$GLRenderer;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$SelfRendererCallback;

    iput-object p2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mOwningView:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mRenderer:Lcom/google/android/videochat/RendererManager;
    invoke-static {p1}, Lcom/google/android/talk/videochat/GlView;->access$200(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/videochat/RendererManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$RemoteRendererCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/RendererManager;->createRemoteRenderer(Lcom/google/android/videochat/Renderer$RendererThreadCallback;)Lcom/google/android/videochat/RemoteRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteRenderer:Lcom/google/android/videochat/RemoteRenderer;

    new-instance v0, Lcom/google/android/talk/videochat/TextureRenderer;

    const-string v1, "remote"

    invoke-direct {v0, v1}, Lcom/google/android/talk/videochat/TextureRenderer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    new-instance v0, Lcom/google/android/talk/videochat/TextureRenderer;

    const-string v1, "self"

    invoke-direct {v0, v1}, Lcom/google/android/talk/videochat/TextureRenderer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    new-instance v0, Lcom/google/android/talk/videochat/BorderRenderer;

    const-string v1, "self"

    invoke-direct {v0, v1}, Lcom/google/android/talk/videochat/BorderRenderer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

    new-instance v0, Lcom/google/android/talk/videochat/BorderRenderer;

    const-string v1, "remote"

    invoke-direct {v0, v1}, Lcom/google/android/talk/videochat/BorderRenderer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

    new-instance v0, Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

    invoke-direct {v0}, Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteFrameData:Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

    new-instance v0, Lcom/google/android/talk/videochat/GlView$GLRenderer$EffectCallback;

    invoke-direct {v0, p0}, Lcom/google/android/talk/videochat/GlView$GLRenderer$EffectCallback;-><init>(Lcom/google/android/talk/videochat/GlView$GLRenderer;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mEffectCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$EffectCallback;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mRenderer:Lcom/google/android/videochat/RendererManager;
    invoke-static {p1}, Lcom/google/android/talk/videochat/GlView;->access$200(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/videochat/RendererManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$SelfRendererCallback;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/videochat/RendererManager;->createSelfRenderer(Lcom/google/android/videochat/Renderer$SelfRendererThreadCallback;Lcom/google/android/videochat/CameraSpecification;)Lcom/google/android/videochat/SelfRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mDeviceOrientation:I

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/talk/videochat/GlView$GLRenderer;)I
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;

    iget v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCurrentEffectId:I

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/talk/videochat/GlView$GLRenderer;)Lcom/google/android/talk/videochat/GlView;
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mOwningView:Lcom/google/android/talk/videochat/GlView;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/android/talk/videochat/GlView$GLRenderer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mIsInitialRemoteFrame:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/android/talk/videochat/GlView$GLRenderer;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mIsInitialSelfFrame:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/google/android/talk/videochat/GlView$GLRenderer;)I
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;

    iget v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraTextureName:I

    return v0
.end method

.method static synthetic access$1602(Lcom/google/android/talk/videochat/GlView$GLRenderer;I)I
    .locals 0
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;
    .param p1    # I

    iput p1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraTextureName:I

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/talk/videochat/GlView$GLRenderer;)Lcom/google/android/talk/videochat/TextureRenderer;
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/talk/videochat/GlView$GLRenderer;)Lcom/google/android/talk/videochat/FilterFrameworkEffects;
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/talk/videochat/GlView$GLRenderer;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkActive:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/talk/videochat/GlView$GLRenderer;)I
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;

    iget v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkOutputTextureName:I

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/talk/videochat/GlView$GLRenderer;)Lcom/google/android/videochat/SelfRenderer;
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/GlView$GLRenderer;

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    return-object v0
.end method

.method private drawRemote(Landroid/graphics/Rect;F)V
    .locals 10
    .param p1    # Landroid/graphics/Rect;
    .param p2    # F

    const/4 v6, 0x0

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mHaveInitializedRemoteTexture:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mRemoteStopWatch:Lcom/google/android/videochat/util/GLStopWatch;
    invoke-static {v0}, Lcom/google/android/talk/videochat/GlView;->access$1000(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/videochat/util/GLStopWatch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videochat/util/GLStopWatch;->start()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    iget-object v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v1}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/talk/videochat/VideoAnimator;->getRemoteFrameXClip()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v2}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/talk/videochat/VideoAnimator;->getRemoteFrameYClip()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/talk/videochat/TextureRenderer;->setSourceTextureClip(FF)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/talk/videochat/TextureRenderer;->drawFrame(Landroid/graphics/Rect;F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000

    const/4 v5, 0x1

    const/4 v8, 0x3

    const v9, 0x3e99999a

    move-object v1, p1

    move v3, p2

    move v7, v6

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/talk/videochat/BorderRenderer;->drawFrame(Landroid/graphics/Rect;ZFFIFFIF)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mRemoteStopWatch:Lcom/google/android/videochat/util/GLStopWatch;
    invoke-static {v0}, Lcom/google/android/talk/videochat/GlView;->access$1000(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/videochat/util/GLStopWatch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videochat/util/GLStopWatch;->stop()V

    :cond_0
    return-void
.end method

.method private drawSelf(Landroid/graphics/Rect;F)V
    .locals 10
    .param p1    # Landroid/graphics/Rect;
    .param p2    # F

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mSelfStopWatch:Lcom/google/android/videochat/util/GLStopWatch;
    invoke-static {v0}, Lcom/google/android/talk/videochat/GlView;->access$700(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/videochat/util/GLStopWatch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videochat/util/GLStopWatch;->start()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    iget-object v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v1}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/talk/videochat/VideoAnimator;->getSelfFrameXClip()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mSelfBaseXClip:F
    invoke-static {v2}, Lcom/google/android/talk/videochat/GlView;->access$800(Lcom/google/android/talk/videochat/GlView;)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v2}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/talk/videochat/VideoAnimator;->getSelfFrameYClip()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mSelfBaseYClip:F
    invoke-static {v3}, Lcom/google/android/talk/videochat/GlView;->access$900(Lcom/google/android/talk/videochat/GlView;)F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/talk/videochat/TextureRenderer;->setSourceTextureClip(FF)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/talk/videochat/TextureRenderer;->drawFrame(Landroid/graphics/Rect;F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000

    const/4 v5, 0x1

    const/4 v8, 0x3

    const v9, 0x3e99999a

    move-object v1, p1

    move v3, p2

    move v7, v6

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/talk/videochat/BorderRenderer;->drawFrame(Landroid/graphics/Rect;ZFFIFFIF)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mSelfStopWatch:Lcom/google/android/videochat/util/GLStopWatch;
    invoke-static {v0}, Lcom/google/android/talk/videochat/GlView;->access$700(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/videochat/util/GLStopWatch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videochat/util/GLStopWatch;->stop()V

    return-void
.end method

.method private initializeFilterFramework(ILjava/lang/Object;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v3, "Talk:GlView"

    const-string v4, "initializeFilterFramework"

    invoke-static {v3, v4}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    new-array v2, v6, [I

    invoke-static {v6, v2, v5}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    aget v3, v2, v5

    iput v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkOutputTextureName:I

    new-instance v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;

    invoke-direct {v0}, Lcom/google/android/talk/videochat/FilterSetupInputParameters;-><init>()V

    iget v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraTextureName:I

    iput v3, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->inputTextureName:I

    iget v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkOutputTextureName:I

    iput v3, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->outputTextureName:I

    iget-object v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v3}, Lcom/google/android/videochat/SelfRenderer;->getCameraBufferSize()Lcom/google/android/videochat/Size;

    move-result-object v1

    iget v3, v1, Lcom/google/android/videochat/Size;->width:I

    iput v3, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->inputTextureWidth:I

    iget v3, v1, Lcom/google/android/videochat/Size;->height:I

    iput v3, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->inputTextureHeight:I

    iput p1, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectId:I

    iput-object p2, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectParam:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mEffectCallback:Lcom/google/android/talk/videochat/GlView$GLRenderer$EffectCallback;

    iput-object v3, v0, Lcom/google/android/talk/videochat/FilterSetupInputParameters;->effectCallback:Landroid/media/effect/EffectUpdateListener;

    iget-object v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    if-nez v3, :cond_0

    new-instance v3, Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    invoke-direct {v3, v0}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;-><init>(Lcom/google/android/talk/videochat/FilterSetupInputParameters;)V

    iput-object v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    :goto_0
    const-string v3, "Talk:GlView"

    const-string v4, "initializeFilterFramework exit"

    invoke-static {v3, v4}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    invoke-virtual {v3, v0}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->switchEffect(Lcom/google/android/talk/videochat/FilterSetupInputParameters;)I

    move-result v3

    iput v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkOutputTextureName:I

    goto :goto_0
.end method

.method private stopFilterEffect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->releaseEffect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    :cond_0
    return-void
.end method


# virtual methods
.method public destroyRenderer()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteRenderer:Lcom/google/android/videochat/RemoteRenderer;

    invoke-virtual {v0}, Lcom/google/android/videochat/RemoteRenderer;->release()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/videochat/SelfRenderer;->release()V

    return-void
.end method

.method public getActiveEffect()I
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCurrentEffectId:I

    return v0
.end method

.method public getActiveEffectParam()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCurrentEffectParam:Ljava/lang/Object;

    return-object v0
.end method

.method public getRemoteRenderer()Lcom/google/android/videochat/RemoteRenderer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteRenderer:Lcom/google/android/videochat/RemoteRenderer;

    return-object v0
.end method

.method public getSelfRenderer()Lcom/google/android/videochat/SelfRenderer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    return-object v0
.end method

.method onCameraOpened(Z)V
    .locals 2
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraIsOpened:Z

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/talk/videochat/TextureRenderer;->setFlipSourceTexture(Z)V

    iget v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCurrentEffectId:I

    iget-object v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCurrentEffectParam:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->setActiveEffect(ILjava/lang/Object;)V

    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 13
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    const/4 v12, 0x1

    const/4 v11, 0x0

    iget-boolean v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mIsInitialSelfFrame:Z

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # invokes: Lcom/google/android/talk/videochat/GlView;->onInitialSelfFrame()V
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$300(Lcom/google/android/talk/videochat/GlView;)V

    iput-boolean v11, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mIsInitialSelfFrame:Z

    :cond_0
    iget-boolean v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mIsInitialRemoteFrame:Z

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # invokes: Lcom/google/android/talk/videochat/GlView;->onInitialRemoteFrame()V
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$400(Lcom/google/android/talk/videochat/GlView;)V

    iput-boolean v11, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mIsInitialRemoteFrame:Z

    :cond_1
    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getRotation()I

    move-result v8

    # invokes: Lcom/google/android/talk/videochat/GlView;->surfaceRotationEnumToDegress(I)I
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$500(I)I

    move-result v0

    iget v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mDeviceOrientation:I

    if-eq v0, v8, :cond_2

    iput v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mDeviceOrientation:I

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    iget v9, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mDeviceOrientation:I

    invoke-virtual {v8, v9}, Lcom/google/android/videochat/SelfRenderer;->setDeviceOrientation(I)V

    :cond_2
    const/16 v8, 0xbe2

    invoke-static {v8}, Landroid/opengl/GLES20;->glDisable(I)V

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteRenderer:Lcom/google/android/videochat/RemoteRenderer;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteFrameData:Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

    invoke-virtual {v8, v9, v10}, Lcom/google/android/videochat/RemoteRenderer;->drawTexture(Lcom/google/android/videochat/Renderer$DrawInputParams;Lcom/google/android/videochat/Renderer$DrawOutputParams;)V

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteFrameData:Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

    iget-boolean v8, v8, Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;->updatedTexture:Z

    if-eqz v8, :cond_3

    iput-boolean v12, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mHaveInitializedRemoteTexture:Z

    :cond_3
    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteFrameData:Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

    iget-boolean v8, v8, Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;->frameSizeChanged:Z

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteFrameData:Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

    iget v9, v9, Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;->frameWidth:I

    int-to-float v9, v9

    iget-object v10, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteFrameData:Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;

    iget v10, v10, Lcom/google/android/videochat/RemoteRenderer$RendererFrameOutputData;->frameHeight:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameAspectRatio(F)V

    :cond_4
    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/talk/videochat/VideoAnimator;->getFreezeFrame()Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v8}, Lcom/google/android/videochat/SelfRenderer;->renderSelfFrame()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-boolean v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkActive:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v8}, Lcom/google/android/videochat/SelfRenderer;->getMostRecentCameraFrameTime()J

    move-result-wide v6

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    invoke-virtual {v8, v6, v7}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->setTimestamp(J)V

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    invoke-virtual {v8}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->renderFrame()Z

    :cond_5
    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v8}, Lcom/google/android/videochat/SelfRenderer;->encodeFrame()V

    :cond_6
    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/talk/videochat/VideoAnimator;->getRemoteFrameRect()Landroid/graphics/Rect;

    move-result-object v3

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/talk/videochat/VideoAnimator;->getRemoteFrameAlpha()F

    move-result v2

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/talk/videochat/VideoAnimator;->getSelfFrameRect()Landroid/graphics/Rect;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/talk/videochat/VideoAnimator;->getSelfFrameAlpha()F

    move-result v4

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/talk/videochat/VideoAnimator;->getFreezeFrame()Z

    move-result v8

    if-eqz v8, :cond_a

    iget-boolean v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mLastFrameFrozen:Z

    if-nez v8, :cond_7

    iput v11, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCount:I

    :cond_7
    iget v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCount:I

    iput-boolean v12, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mLastFrameFrozen:Z

    :goto_0
    iget-boolean v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mHaveInitializedRemoteTexture:Z

    if-eqz v8, :cond_8

    float-to-double v8, v2

    const-wide/high16 v10, 0x3ff0000000000000L

    cmpl-double v8, v8, v10

    if-nez v8, :cond_8

    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mViewRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v8}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    :cond_8
    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    iget-object v9, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v9}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/talk/videochat/VideoAnimator;->getBackgroundColor()I

    move-result v9

    # invokes: Lcom/google/android/talk/videochat/GlView;->drawBgColor(I)V
    invoke-static {v8, v9}, Lcom/google/android/talk/videochat/GlView;->access$600(Lcom/google/android/talk/videochat/GlView;I)V

    :cond_9
    iget-object v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v8}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/talk/videochat/VideoAnimator;->getFlipZOrder()Z

    move-result v8

    if-nez v8, :cond_c

    invoke-direct {p0, v3, v2}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->drawRemote(Landroid/graphics/Rect;F)V

    invoke-direct {p0, v5, v4}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->drawSelf(Landroid/graphics/Rect;F)V

    :goto_1
    return-void

    :cond_a
    iget-boolean v8, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mLastFrameFrozen:Z

    if-eqz v8, :cond_b

    const-string v8, "Talk:GlView"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget v10, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCount:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " frames were drawn during the last animation."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    iput-boolean v11, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mLastFrameFrozen:Z

    goto :goto_0

    :cond_c
    invoke-direct {p0, v5, v4}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->drawSelf(Landroid/graphics/Rect;F)V

    invoke-direct {p0, v3, v2}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->drawRemote(Landroid/graphics/Rect;F)V

    goto :goto_1
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 4
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Talk:GlView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GlView.GLRenderer.onSurfaceChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mViewRect:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/talk/videochat/TextureRenderer;->setViewSize(II)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/talk/videochat/TextureRenderer;->setViewSize(II)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/talk/videochat/BorderRenderer;->setViewSize(II)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/talk/videochat/BorderRenderer;->setViewSize(II)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mVideoAnimator:Lcom/google/android/talk/videochat/VideoAnimator;
    invoke-static {v0}, Lcom/google/android/talk/videochat/GlView;->access$100(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/VideoAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/talk/videochat/VideoAnimator;->setViewportSize(II)V

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 3
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v2, 0x0

    const-string v0, "Talk:GlView"

    const-string v1, "GlView.GLRenderer.onSurfaceCreated"

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraIsOpened:Z

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteRenderer:Lcom/google/android/videochat/RemoteRenderer;

    invoke-virtual {v0}, Lcom/google/android/videochat/RemoteRenderer;->initializeGLContext()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/TextureRenderer;->initGLContext()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    iget-object v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteRenderer:Lcom/google/android/videochat/RemoteRenderer;

    invoke-virtual {v1}, Lcom/google/android/videochat/RemoteRenderer;->getOutputTextureName()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/talk/videochat/TextureRenderer;->setInputTextureName(I)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/videochat/SelfRenderer;->initializeGLContext()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/TextureRenderer;->initGLContext()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/BorderRenderer;->initGLContext()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mRemoteBorderRenderer:Lcom/google/android/talk/videochat/BorderRenderer;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/BorderRenderer;->initGLContext()V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/videochat/SelfRenderer;->getOutputTextureName()I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraTextureName:I

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    iget v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraTextureName:I

    invoke-virtual {v0, v1}, Lcom/google/android/talk/videochat/TextureRenderer;->setInputTextureName(I)V

    iput-boolean v2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkActive:Z

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/FilterFrameworkEffects;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFramework:Lcom/google/android/talk/videochat/FilterFrameworkEffects;

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    iget-object v0, v0, Lcom/google/android/talk/videochat/GlView;->mCallback:Lcom/google/android/talk/videochat/GlView$GlViewInitializedCallback;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    iget-object v0, v0, Lcom/google/android/talk/videochat/GlView;->mCallback:Lcom/google/android/talk/videochat/GlView$GlViewInitializedCallback;

    invoke-interface {v0}, Lcom/google/android/talk/videochat/GlView$GlViewInitializedCallback;->complete()V

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mHaveInitializedRemoteTexture:Z

    return-void
.end method

.method setActiveEffect(ILjava/lang/Object;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/talk/TalkApp;->verboseLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Talk:GlView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setActiveEffect "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cameraOpened "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraIsOpened:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput p1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCurrentEffectId:I

    iput-object p2, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCurrentEffectParam:Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraIsOpened:Z

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-static {p1}, Lcom/google/android/talk/videochat/EffectsController;->effectCanClip(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/SelfRenderer;->setUseMaxSizeForCameraBuffer(Z)V

    const-string v0, "Talk:GlView"

    const-string v1, "Unlocking AE/AWB"

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->this$0:Lcom/google/android/talk/videochat/GlView;

    # getter for: Lcom/google/android/talk/videochat/GlView;->mGLRenderer:Lcom/google/android/talk/videochat/GlView$GLRenderer;
    invoke-static {v0}, Lcom/google/android/talk/videochat/GlView;->access$000(Lcom/google/android/talk/videochat/GlView;)Lcom/google/android/talk/videochat/GlView$GLRenderer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->getSelfRenderer()Lcom/google/android/videochat/SelfRenderer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videochat/SelfRenderer;->getCamera()Lcom/google/android/videochat/CameraInterface;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/videochat/CameraInterface;->setPreview3ALocks(Z)V

    if-ne p1, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-static {}, Lcom/google/android/videochat/VideoSpecification;->getOutgoingVideoWithEffectsSpec()Lcom/google/android/videochat/VideoSpecification;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/SelfRenderer;->setMaxEncodeVideoSpec(Lcom/google/android/videochat/VideoSpecification;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    iget v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraTextureName:I

    invoke-virtual {v0, v1}, Lcom/google/android/talk/videochat/TextureRenderer;->setInputTextureName(I)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/SelfRenderer;->setImageStabilizationLevel(I)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/videochat/SelfRenderer;->resetEncodeTexture()V

    invoke-direct {p0}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->stopFilterEffect()V

    iput-boolean v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkActive:Z

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-static {}, Lcom/google/android/videochat/VideoSpecification;->getOutgoingVideoWithEffectsSpec()Lcom/google/android/videochat/VideoSpecification;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/SelfRenderer;->setMaxEncodeVideoSpec(Lcom/google/android/videochat/VideoSpecification;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->initializeFilterFramework(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v0, v3}, Lcom/google/android/videochat/SelfRenderer;->setImageStabilizationLevel(I)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    iget v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkOutputTextureName:I

    invoke-virtual {v0, v1}, Lcom/google/android/talk/videochat/TextureRenderer;->setInputTextureName(I)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    iget v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkOutputTextureName:I

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/SelfRenderer;->overrideEncodeTexture(I)V

    iput-boolean v4, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkActive:Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-static {}, Lcom/google/android/videochat/VideoSpecification;->getOutgoingVideoNoEffectsSpec()Lcom/google/android/videochat/VideoSpecification;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/SelfRenderer;->setMaxEncodeVideoSpec(Lcom/google/android/videochat/VideoSpecification;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v0, v3}, Lcom/google/android/videochat/SelfRenderer;->setImageStabilizationLevel(I)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfScreenRenderer:Lcom/google/android/talk/videochat/TextureRenderer;

    iget v1, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mCameraTextureName:I

    invoke-virtual {v0, v1}, Lcom/google/android/talk/videochat/TextureRenderer;->setInputTextureName(I)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mSelfRenderer:Lcom/google/android/videochat/SelfRenderer;

    invoke-virtual {v0}, Lcom/google/android/videochat/SelfRenderer;->resetEncodeTexture()V

    invoke-direct {p0}, Lcom/google/android/talk/videochat/GlView$GLRenderer;->stopFilterEffect()V

    iput-boolean v3, p0, Lcom/google/android/talk/videochat/GlView$GLRenderer;->mFilterFrameworkActive:Z

    goto/16 :goto_0
.end method
