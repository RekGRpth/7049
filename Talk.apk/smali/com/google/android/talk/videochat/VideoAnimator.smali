.class public Lcom/google/android/talk/videochat/VideoAnimator;
.super Ljava/lang/Object;
.source "VideoAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/videochat/VideoAnimator$RectSize;,
        Lcom/google/android/talk/videochat/VideoAnimator$Position;,
        Lcom/google/android/talk/videochat/VideoAnimator$RgbEvaluator;,
        Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;,
        Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;,
        Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;
    }
.end annotation


# static fields
.field private static final BACKGROUND_FINAL_COLOR:I = -0x1000000

.field private static CLIP_THRESHOLD:F = 0.0f

.field private static final FADE_SELF_FRAME_DURATION:I = 0x1f4

.field private static final FADE_TO_BLACK_DURATION:I = 0x1f4

.field private static final SELF_FRAME_VANITY_FINAL_RELATIVE_SIZE:F = 0.3f

.field private static final SELF_FRAME_VANITY_INITIAL_RELATIVE_SIZE:F = 0.27f

.field private static final STATE_REMOTE_FRAME_CORNER:I = 0x1

.field private static final STATE_REMOTE_FRAME_FULLSCREEN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "talk:VideoAnimator"


# instance fields
.field private mBackgroundColor:I

.field private mBackgroundFadeAnimator:Landroid/animation/ObjectAnimator;

.field private mBackgroundImageAlpha:F

.field private mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

.field private mCornerFrameMarginBottom:I

.field private mCornerFrameMarginLeftRight:I

.field private mCornerFrameWidth:I

.field private mDecelerateQuintInterp:Landroid/view/animation/DecelerateInterpolator;

.field private mFadeInDuration:I

.field private mFlipZOrder:Z

.field private mFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

.field private mFreezeFrame:Z

.field private mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

.field private mHandler:Landroid/os/Handler;

.field private mLeftCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

.field private mLeftCornerFrameWidthLandscape:I

.field private mLeftCornerFrameWidthPortrait:I

.field private mLeftCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

.field private mNeedToAnimateFramesToStandardPosition:Z

.field private mNeedToAnimateFramesToSwappedPosition:Z

.field private mNeedToAnimateToCorner:Z

.field private mNeedToAnimateToVanity:Z

.field private mNeedToPlaceFramesInStandardPosition:Z

.field private mNeedToPlaceFramesInSwappedPosition:Z

.field private mNeedToPlaceInCorner:Z

.field private mNeedToPlaceInVanity:Z

.field private mNeedToPlaceRemoteInFullscreen:Z

.field private mRemoteFrameAlpha:F

.field private mRemoteFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

.field private mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

.field private mRemoteFrameRawAspectRatio:F

.field private mRemoteFrameRect:Landroid/graphics/Rect;

.field private mRemoteFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

.field private mRemoteFrameState:I

.field private mRemoteFrameX:I

.field private mRemoteFrameXClip:F

.field private mRemoteFrameY:I

.field private mRemoteFrameYClip:F

.field private mReverseFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

.field private mReverseSwapFramesAnimation:Landroid/animation/AnimatorSet;

.field private mRightCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

.field private mRightCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

.field private mSelfFrameAlpha:F

.field private mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

.field private mSelfFrameFadeOutAnimator:Landroid/animation/ObjectAnimator;

.field private mSelfFrameFlyInAnimator:Landroid/animation/AnimatorSet;

.field private mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

.field private mSelfFrameMoveToCornerAnimator:Landroid/animation/AnimatorSet;

.field private mSelfFrameRawAspectRatio:F

.field private mSelfFrameRect:Landroid/graphics/Rect;

.field private mSelfFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

.field private mSelfFrameToCornerAnimDuration:I

.field private mSelfFrameX:I

.field private mSelfFrameXClip:F

.field private mSelfFrameY:I

.field private mSelfFrameYClip:F

.field private mSelfFramesAvailable:Z

.field private mSwapFramesAnimDuration:I

.field private mSwapFramesAnimation:Landroid/animation/AnimatorSet;

.field private mUnhideSelfFrameIfAvailable:Z

.field private mVanityFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

.field private mViewportHeight:I

.field private mViewportWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x3dcccccd

    sput v0, Lcom/google/android/talk/videochat/VideoAnimator;->CLIP_THRESHOLD:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v1, 0x0

    const-wide/16 v6, 0x1f4

    const/high16 v5, 0x3fc00000

    const/4 v4, 0x2

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameAlpha:F

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v3, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v3, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRect:Landroid/graphics/Rect;

    iput v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameX:I

    iput v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameY:I

    iput v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameX:I

    iput v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameY:I

    iput v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameAlpha:F

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40200000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mDecelerateQuintInterp:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameToCornerAnimDuration:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimDuration:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFadeInDuration:I

    const-string v0, "remoteFrameAlpha"

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundColor:I

    :goto_0
    const-string v0, "backgroundColor"

    new-array v1, v4, [I

    iget v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundColor:I

    aput v2, v1, v3

    const/4 v2, 0x1

    const/high16 v3, -0x1000000

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundFadeAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundFadeAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/talk/videochat/VideoAnimator$RgbEvaluator;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/talk/videochat/VideoAnimator$RgbEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundFadeAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundFadeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameMarginBottom:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameMarginLeftRight:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameWidth:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameWidthLandscape:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameWidthPortrait:I

    const-string v0, "selfFrameAlpha"

    new-array v1, v4, [F

    fill-array-data v1, :array_1

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-string v0, "selfFrameAlpha"

    new-array v1, v4, [F

    fill-array-data v1, :array_2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeOutAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeOutAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeOutAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeOutAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/talk/videochat/VideoAnimator$1;

    invoke-direct {v1, p0}, Lcom/google/android/talk/videochat/VideoAnimator$1;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-string v0, "flipZOrder"

    new-array v1, v4, [I

    fill-array-data v1, :array_3

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const-string v0, "flipZOrder"

    new-array v1, v4, [I

    fill-array-data v1, :array_4

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const-string v0, "freezeFrame"

    new-array v1, v4, [I

    fill-array-data v1, :array_5

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    return-void

    :cond_0
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundColor:I

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_2
    .array-data 4
        0x3f800000
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_4
    .array-data 4
        0x1
        0x0
    .end array-data

    :array_5
    .array-data 4
        0x1
        0x0
    .end array-data
.end method

.method static synthetic access$102(Lcom/google/android/talk/videochat/VideoAnimator;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/talk/videochat/VideoAnimator;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mUnhideSelfFrameIfAvailable:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/talk/videochat/VideoAnimator;)Z
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/VideoAnimator;

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFramesAvailable:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/talk/videochat/VideoAnimator;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/VideoAnimator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method public static calculateClippingParams(IIF)Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;
    .locals 10
    .param p0    # I
    .param p1    # I
    .param p2    # F

    const/high16 v9, 0x3f800000

    new-instance v1, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    invoke-direct {v1}, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;-><init>()V

    int-to-float v7, p0

    int-to-float v8, p1

    div-float v2, v7, v8

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    cmpl-float v7, v2, p2

    if-lez v7, :cond_1

    div-float v7, v9, p2

    div-float v8, v9, v2

    sub-float/2addr v7, v8

    mul-float v0, v7, p2

    sget v7, Lcom/google/android/talk/videochat/VideoAnimator;->CLIP_THRESHOLD:F

    cmpl-float v7, v0, v7

    if-lez v7, :cond_0

    int-to-float v7, p1

    mul-float/2addr v7, p2

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    sub-int v4, p0, v7

    :goto_0
    iput v3, v1, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mXClipPercentage:F

    iput v5, v1, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mYClipPercentage:F

    new-instance v7, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    sub-int v8, p0, v4

    sub-int v9, p1, v6

    invoke-direct {v7, v8, v9}, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;-><init>(II)V

    iput-object v7, v1, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    return-object v1

    :cond_0
    move v5, v0

    goto :goto_0

    :cond_1
    sub-float v7, p2, v2

    div-float v0, v7, p2

    sget v7, Lcom/google/android/talk/videochat/VideoAnimator;->CLIP_THRESHOLD:F

    cmpl-float v7, v0, v7

    if-lez v7, :cond_2

    int-to-float v7, p0

    div-float/2addr v7, p2

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    sub-int v6, p1, v7

    goto :goto_0

    :cond_2
    move v3, v0

    goto :goto_0
.end method

.method private fullySpecified()Z
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRawAspectRatio:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initializeFullScreenValues()V
    .locals 4

    const/high16 v3, 0x40000000

    new-instance v0, Lcom/google/android/talk/videochat/VideoAnimator$Position;

    iget v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$Position;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    return-void
.end method

.method private initializeLeftCornerValues()V
    .locals 8

    const/high16 v7, 0x40000000

    iget v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRawAspectRatio:F

    const/high16 v5, 0x3f800000

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameWidthLandscape:I

    :goto_0
    int-to-float v4, v0

    div-float/2addr v4, v7

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameMarginLeftRight:I

    add-int v2, v4, v1

    iget v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    iget v5, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameMarginBottom:I

    sub-int/2addr v4, v5

    int-to-float v5, v0

    iget v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRawAspectRatio:F

    div-float/2addr v5, v6

    div-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-int v3, v4, v5

    new-instance v4, Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-direct {v4, v2, v3}, Lcom/google/android/talk/videochat/VideoAnimator$Position;-><init>(II)V

    iput-object v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    new-instance v4, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    int-to-float v5, v0

    iget v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRawAspectRatio:F

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-direct {v4, v0, v5}, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;-><init>(II)V

    iput-object v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameWidthPortrait:I

    goto :goto_0
.end method

.method private initializeRightCornerValues()V
    .locals 7

    const/high16 v6, 0x40000000

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameWidth:I

    int-to-float v3, v3

    div-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    iget v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameMarginLeftRight:I

    sub-int/2addr v3, v4

    sub-int v1, v3, v0

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    iget v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameMarginBottom:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    div-float/2addr v4, v5

    div-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int v2, v3, v4

    new-instance v3, Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-direct {v3, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$Position;-><init>(II)V

    iput-object v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    new-instance v3, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    iget v4, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameWidth:I

    iget v5, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCornerFrameWidth:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;-><init>(II)V

    iput-object v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    return-void
.end method

.method private initializeStartupAnimators()V
    .locals 14

    const v8, 0x3e8a3d71

    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    new-instance v1, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-direct {v1}, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;-><init>()V

    new-instance v0, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-direct {v0}, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;-><init>()V

    iget v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    iget v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    int-to-float v6, v6

    mul-float/2addr v6, v8

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v1, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->h:I

    iget v6, v1, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->h:I

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v1, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->w:I

    const v6, 0x3e99999a

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v0, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->h:I

    iget v6, v0, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->h:I

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v0, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->w:I

    :goto_0
    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mVanityFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    const-string v6, "selfFrameSize"

    new-instance v7, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;

    invoke-direct {v7, p0, v13}, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v1, v8, v10

    aput-object v0, v8, v11

    invoke-static {p0, v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v7, 0x40200000

    invoke-direct {v6, v7}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v2, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-string v6, "selfFramePosition"

    new-instance v7, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;

    invoke-direct {v7, p0, v13}, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    new-array v8, v12, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    aput-object v9, v8, v11

    invoke-static {p0, v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v5

    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFlyInAnimator:Landroid/animation/AnimatorSet;

    iget-object v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFlyInAnimator:Landroid/animation/AnimatorSet;

    const/4 v7, 0x3

    new-array v7, v7, [Landroid/animation/Animator;

    iget-object v8, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    aput-object v8, v7, v10

    aput-object v2, v7, v11

    aput-object v5, v7, v12

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFlyInAnimator:Landroid/animation/AnimatorSet;

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFadeInDuration:I

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    const-string v6, "selfFramePosition"

    new-instance v7, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;

    invoke-direct {v7, p0, v13}, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    new-array v8, v12, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    aput-object v9, v8, v11

    invoke-static {p0, v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-string v6, "selfFrameSize"

    new-instance v7, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;

    invoke-direct {v7, p0, v13}, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    new-array v8, v12, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mVanityFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    aput-object v9, v8, v10

    iget-object v9, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    aput-object v9, v8, v11

    invoke-static {p0, v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v3

    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameMoveToCornerAnimator:Landroid/animation/AnimatorSet;

    iget-object v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameMoveToCornerAnimator:Landroid/animation/AnimatorSet;

    new-array v7, v12, [Landroid/animation/Animator;

    aput-object v4, v7, v10

    aput-object v3, v7, v11

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameMoveToCornerAnimator:Landroid/animation/AnimatorSet;

    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameMoveToCornerAnimator:Landroid/animation/AnimatorSet;

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameToCornerAnimDuration:I

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    return-void

    :cond_0
    iget v6, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    int-to-float v6, v6

    mul-float/2addr v6, v8

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v1, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->w:I

    iget v6, v1, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->w:I

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v1, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->h:I

    const v6, 0x3e99999a

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v0, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->w:I

    iget v6, v0, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->w:I

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v0, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->h:I

    goto/16 :goto_0
.end method

.method private initializeSwapAnimators()V
    .locals 25

    const-string v20, "selfFramePosition"

    new-instance v21, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v19

    const-string v20, "selfFrameSize"

    new-instance v21, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v18

    const-string v20, "selfFrameXClip"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aput v23, v21, v22

    const/16 v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mXClipPercentage:F

    move/from16 v23, v0

    aput v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v16

    const-string v20, "selfFrameYClip"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aput v23, v21, v22

    const/16 v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mYClipPercentage:F

    move/from16 v23, v0

    aput v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v17

    const-string v20, "remoteFramePosition"

    new-instance v21, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v7

    const-string v20, "remoteFrameSize"

    new-instance v21, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-string v20, "remoteFrameXClip"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mXClipPercentage:F

    move/from16 v23, v0

    aput v23, v21, v22

    const/16 v22, 0x1

    const/16 v23, 0x0

    aput v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-string v20, "remoteFrameYClip"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mYClipPercentage:F

    move/from16 v23, v0

    aput v23, v21, v22

    const/16 v22, 0x1

    const/16 v23, 0x0

    aput v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    new-instance v20, Landroid/animation/AnimatorSet;

    invoke-direct/range {v20 .. v20}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v19, v21, v22

    const/16 v22, 0x1

    aput-object v18, v21, v22

    const/16 v22, 0x2

    aput-object v16, v21, v22

    const/16 v22, 0x3

    aput-object v17, v21, v22

    const/16 v22, 0x4

    aput-object v7, v21, v22

    const/16 v22, 0x5

    aput-object v6, v21, v22

    const/16 v22, 0x6

    aput-object v4, v21, v22

    const/16 v22, 0x7

    aput-object v5, v21, v22

    invoke-virtual/range {v20 .. v21}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mDecelerateQuintInterp:Landroid/view/animation/DecelerateInterpolator;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimDuration:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v21, v0

    invoke-virtual/range {v20 .. v22}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    const-string v20, "selfFramePosition"

    new-instance v21, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v15

    const-string v20, "selfFrameSize"

    new-instance v21, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v14

    const-string v20, "selfFrameXClip"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mXClipPercentage:F

    move/from16 v23, v0

    aput v23, v21, v22

    const/16 v22, 0x1

    const/16 v23, 0x0

    aput v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    const-string v20, "selfFrameYClip"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mYClipPercentage:F

    move/from16 v23, v0

    aput v23, v21, v22

    const/16 v22, 0x1

    const/16 v23, 0x0

    aput v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    const-string v20, "remoteFramePosition"

    new-instance v21, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$PositionEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v11

    const-string v20, "remoteFrameSize"

    new-instance v21, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator$RectSizeEvaluator;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;Lcom/google/android/talk/videochat/VideoAnimator$1;)V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v10

    const-string v20, "remoteFrameXClip"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aput v23, v21, v22

    const/16 v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mXClipPercentage:F

    move/from16 v23, v0

    aput v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    const-string v20, "remoteFrameYClip"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aput v23, v21, v22

    const/16 v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mYClipPercentage:F

    move/from16 v23, v0

    aput v23, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    new-instance v20, Landroid/animation/AnimatorSet;

    invoke-direct/range {v20 .. v20}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseSwapFramesAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseSwapFramesAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v15, v21, v22

    const/16 v22, 0x1

    aput-object v14, v21, v22

    const/16 v22, 0x2

    aput-object v12, v21, v22

    const/16 v22, 0x3

    aput-object v13, v21, v22

    const/16 v22, 0x4

    aput-object v11, v21, v22

    const/16 v22, 0x5

    aput-object v10, v21, v22

    const/16 v22, 0x6

    aput-object v8, v21, v22

    const/16 v22, 0x7

    aput-object v9, v21, v22

    invoke-virtual/range {v20 .. v21}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseSwapFramesAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mDecelerateQuintInterp:Landroid/view/animation/DecelerateInterpolator;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseSwapFramesAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimDuration:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v21, v0

    invoke-virtual/range {v20 .. v22}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    return-void
.end method

.method private onFullySpecified()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->initializeLeftCornerValues()V

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->initializeSwapAnimators()V

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceRemoteInFullscreen:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->placeRemoteFrameInFullscreen()V

    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameState:I

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    invoke-virtual {p0, v3}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameXClip(F)V

    invoke-virtual {p0, v3}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameYClip(F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    :goto_1
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateFramesToSwappedPosition:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/android/talk/videochat/VideoAnimator;->placeFramesInSwappedPositions(Z)V

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateFramesToSwappedPosition:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceFramesInSwappedPosition:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/google/android/talk/videochat/VideoAnimator;->placeFramesInSwappedPositions(Z)V

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceFramesInSwappedPosition:Z

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateFramesToStandardPosition:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0, v2}, Lcom/google/android/talk/videochat/VideoAnimator;->placeFramesInStandardPositions(Z)V

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateFramesToStandardPosition:Z

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceFramesInStandardPosition:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/talk/videochat/VideoAnimator;->placeFramesInStandardPositions(Z)V

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceFramesInSwappedPosition:Z

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mXClipPercentage:F

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameXClip(F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mYClipPercentage:F

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameYClip(F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    goto :goto_1
.end method

.method private onStartupValuesSpecified()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    iget v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    iget v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    invoke-static {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator;->calculateClippingParams(IIF)Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->initializeRightCornerValues()V

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->initializeFullScreenValues()V

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->initializeStartupAnimators()V

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateToVanity:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, v4}, Lcom/google/android/talk/videochat/VideoAnimator;->placeSelfFrameInVanity(Z)V

    iput-boolean v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateToVanity:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceInVanity:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/talk/videochat/VideoAnimator;->placeSelfFrameInVanity(Z)V

    iput-boolean v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceInVanity:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateToCorner:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0, v4}, Lcom/google/android/talk/videochat/VideoAnimator;->placeSelfFrameInCorner(Z)V

    iput-boolean v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateToCorner:Z

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceInCorner:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/talk/videochat/VideoAnimator;->placeSelfFrameInCorner(Z)V

    iput-boolean v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceInCorner:Z

    goto :goto_0
.end method

.method private varargs startAnimationsOnMainThread([Landroid/animation/Animator;)V
    .locals 2
    .param p1    # [Landroid/animation/Animator;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    const-string v0, "talk:VideoAnimator"

    const-string v1, "Can\'t run animation. mHandler hasn\'t been set."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/talk/videochat/VideoAnimator$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/talk/videochat/VideoAnimator$2;-><init>(Lcom/google/android/talk/videochat/VideoAnimator;[Landroid/animation/Animator;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private startupValuesSpecified()Z
    .locals 2

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private unprotectedPlaceFramesInStandardPositions(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseSwapFramesAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->getDuration()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseSwapFramesAnimation:Landroid/animation/AnimatorSet;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mReverseFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v5

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    :goto_0
    iput v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameState:I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    invoke-virtual {p0, v1}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFrameXClip(F)V

    invoke-virtual {p0, v1}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFrameYClip(F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mXClipPercentage:F

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameXClip(F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mYClipPercentage:F

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameYClip(F)V

    new-array v0, v5, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    invoke-virtual {p0, v3}, Lcom/google/android/talk/videochat/VideoAnimator;->setFlipZOrder(I)V

    goto :goto_0
.end method

.method private unprotectedPlaceFramesInSwappedPositions(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->getDuration()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSwapFramesAnimation:Landroid/animation/AnimatorSet;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFlipZOrderAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v5

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    :goto_0
    iput v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameState:I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mXClipPercentage:F

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFrameXClip(F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mYClipPercentage:F

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFrameYClip(F)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mLeftCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    invoke-virtual {p0, v1}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameXClip(F)V

    invoke-virtual {p0, v1}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameYClip(F)V

    new-array v0, v5, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    invoke-virtual {p0, v3}, Lcom/google/android/talk/videochat/VideoAnimator;->setFlipZOrder(I)V

    goto :goto_0
.end method

.method private unprotectedPlaceRemoteFrameInFullscreen()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    iget-object v0, v0, Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;->mClippedSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setRemoteFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/animation/Animator;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    return-void
.end method

.method private unprotectedPlaceSelfFrameInCorner(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameMoveToCornerAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->getDuration()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameMoveToCornerAnimator:Landroid/animation/AnimatorSet;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRightCornerPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    new-array v0, v4, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    goto :goto_0
.end method

.method private unprotectedPlaceSelfFrameInVanity(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFlyInAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->getDuration()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFlyInAnimator:Landroid/animation/AnimatorSet;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrameAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mCenterScreenPosition:Lcom/google/android/talk/videochat/VideoAnimator$Position;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mVanityFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setSelfFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V

    new-array v0, v4, [Landroid/animation/Animator;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->startAnimationsOnMainThread([Landroid/animation/Animator;)V

    goto :goto_0
.end method

.method private updateRemoteFrameRect()V
    .locals 4

    const/high16 v3, 0x40000000

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameX:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameY:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    iget v2, v2, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->w:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    iget v2, v2, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->h:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameX:I

    sub-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameY:I

    sub-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameX:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameY:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private updateSelfFrameRect()V
    .locals 4

    const/high16 v3, 0x40000000

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    iget v2, v2, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->w:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    iget v2, v2, Lcom/google/android/talk/videochat/VideoAnimator$RectSize;->h:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameX:I

    sub-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameY:I

    sub-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameX:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRect:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameY:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method


# virtual methods
.method public getBackgroundColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundColor:I

    return v0
.end method

.method public getBackgroundImageAlpha()F
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundImageAlpha:F

    return v0
.end method

.method public getFlipZOrder()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFlipZOrder:Z

    return v0
.end method

.method public getFreezeFrame()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrame:Z

    return v0
.end method

.method public getRemoteFrameAlpha()F
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameAlpha:F

    return v0
.end method

.method public getRemoteFrameRect()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getRemoteFrameXClip()F
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameXClip:F

    return v0
.end method

.method public getRemoteFrameYClip()F
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameYClip:F

    return v0
.end method

.method public getSelfFrameAlpha()F
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameAlpha:F

    return v0
.end method

.method public getSelfFrameRect()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getSelfFrameXClip()F
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameXClip:F

    return v0
.end method

.method public getSelfFrameYClip()F
    .locals 1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameYClip:F

    return v0
.end method

.method public placeFramesInStandardPositions(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->fullySpecified()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateFramesToStandardPosition:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceFramesInStandardPosition:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/VideoAnimator;->unprotectedPlaceFramesInStandardPositions(Z)V

    goto :goto_0
.end method

.method public placeFramesInSwappedPositions(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->fullySpecified()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateFramesToSwappedPosition:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceFramesInSwappedPosition:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/VideoAnimator;->unprotectedPlaceFramesInSwappedPositions(Z)V

    goto :goto_0
.end method

.method public placeRemoteFrameInFullscreen()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->fullySpecified()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceRemoteInFullscreen:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->unprotectedPlaceRemoteFrameInFullscreen()V

    goto :goto_0
.end method

.method public placeSelfFrameInCorner(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->startupValuesSpecified()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateToCorner:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceInCorner:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/VideoAnimator;->unprotectedPlaceSelfFrameInCorner(Z)V

    goto :goto_0
.end method

.method public placeSelfFrameInVanity(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->startupValuesSpecified()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToAnimateToVanity:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mNeedToPlaceInVanity:Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/VideoAnimator;->unprotectedPlaceSelfFrameInVanity(Z)V

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundColor:I

    return-void
.end method

.method public setBackgroundImageAlpha(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundImageAlpha:F

    return-void
.end method

.method public setBackgroundToBlack(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mBackgroundFadeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :goto_0
    return-void

    :cond_0
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoAnimator;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public setFlipZOrder(I)V
    .locals 1
    .param p1    # I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFlipZOrder:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFreezeFrame(I)V
    .locals 1
    .param p1    # I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mFreezeFrame:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRemoteFrameAlpha(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameAlpha:F

    return-void
.end method

.method public setRemoteFrameAspectRatio(F)V
    .locals 3
    .param p1    # F

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRawAspectRatio:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRawAspectRatio:F

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    iget v1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    iget v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameRawAspectRatio:F

    invoke-static {v0, v1, v2}, Lcom/google/android/talk/videochat/VideoAnimator;->calculateClippingParams(IIF)Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameFullscreenClippingParams:Lcom/google/android/talk/videochat/VideoAnimator$ClippingParams;

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->fullySpecified()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->onFullySpecified()V

    goto :goto_0
.end method

.method public setRemoteFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V
    .locals 1
    .param p1    # Lcom/google/android/talk/videochat/VideoAnimator$Position;

    iget v0, p1, Lcom/google/android/talk/videochat/VideoAnimator$Position;->x:I

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameX:I

    iget v0, p1, Lcom/google/android/talk/videochat/VideoAnimator$Position;->y:I

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameY:I

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->updateRemoteFrameRect()V

    return-void
.end method

.method public setRemoteFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V
    .locals 0
    .param p1    # Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    iput-object p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->updateRemoteFrameRect()V

    return-void
.end method

.method public setRemoteFrameXClip(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameXClip:F

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->updateRemoteFrameRect()V

    return-void
.end method

.method public setRemoteFrameYClip(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mRemoteFrameYClip:F

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->updateRemoteFrameRect()V

    return-void
.end method

.method public setSelfFrameAlpha(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameAlpha:F

    return-void
.end method

.method public setSelfFrameAspectRatio(F)V
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameRawAspectRatio:F

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->startupValuesSpecified()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->onStartupValuesSpecified()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->fullySpecified()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->onFullySpecified()V

    goto :goto_0
.end method

.method public setSelfFramePosition(Lcom/google/android/talk/videochat/VideoAnimator$Position;)V
    .locals 1
    .param p1    # Lcom/google/android/talk/videochat/VideoAnimator$Position;

    iget v0, p1, Lcom/google/android/talk/videochat/VideoAnimator$Position;->x:I

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameX:I

    iget v0, p1, Lcom/google/android/talk/videochat/VideoAnimator$Position;->y:I

    iput v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameY:I

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->updateSelfFrameRect()V

    return-void
.end method

.method public setSelfFrameSize(Lcom/google/android/talk/videochat/VideoAnimator$RectSize;)V
    .locals 0
    .param p1    # Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    iput-object p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameSize:Lcom/google/android/talk/videochat/VideoAnimator$RectSize;

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->updateSelfFrameRect()V

    return-void
.end method

.method public setSelfFrameXClip(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameXClip:F

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->updateSelfFrameRect()V

    return-void
.end method

.method public setSelfFrameYClip(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameYClip:F

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->updateSelfFrameRect()V

    return-void
.end method

.method public setSelfFramesAvailable()V
    .locals 3

    const-string v0, "talk:VideoAnimator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSelfFramesAvailable "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mUnhideSelfFrameIfAvailable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/talk/TalkApp;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFramesAvailable:Z

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mUnhideSelfFrameIfAvailable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeInAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mUnhideSelfFrameIfAvailable:Z

    :cond_0
    return-void
.end method

.method public setSelfFramesUnavailable()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFramesAvailable:Z

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mSelfFrameFadeOutAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method public setViewportSize(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportWidth:I

    iput p2, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mViewportHeight:I

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->startupValuesSpecified()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->onStartupValuesSpecified()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->fullySpecified()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/talk/videochat/VideoAnimator;->onFullySpecified()V

    goto :goto_0
.end method

.method public supplyMainThreadHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/talk/videochat/VideoAnimator;->mHandler:Landroid/os/Handler;

    return-void
.end method
