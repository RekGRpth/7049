.class public Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;
.super Landroid/app/Service;
.source "VideoChatOutputProcessingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;
    }
.end annotation


# instance fields
.field private volatile mBackgroundHandler:Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;

.field private volatile mBackgroundLooper:Landroid/os/Looper;

.field protected mGTalkService:Lcom/google/android/gtalkservice/IGTalkService;

.field private mServiceBindingRequested:Z

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceTaskBuffer:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceTaskBuffer:Ljava/util/Queue;

    new-instance v0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$1;

    invoke-direct {v0, p0}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$1;-><init>(Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->handleMissedCallIntent(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->handleEndCauseIntent(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->handleCallInProgressIntent(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;)Ljava/util/Queue;
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceTaskBuffer:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;)Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;
    .locals 1
    .param p0    # Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mBackgroundHandler:Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;

    return-object v0
.end method

.method private handleCallInProgressIntent(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;

    const-string v8, "remote_bare_jid"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "local_bare_jid"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/google/android/talk/DatabaseUtils;->getAccountIdForUsername(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v0

    :try_start_0
    iget-object v8, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mGTalkService:Lcom/google/android/gtalkservice/IGTalkService;

    invoke-interface {v8, v0, v1}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v5, v7}, Lcom/google/android/gtalkservice/IImSession;->getChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-interface {v5, v7}, Lcom/google/android/gtalkservice/IImSession;->createChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Lcom/google/android/gtalkservice/IChatSession;->ensureNonZeroLastMessageDate()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v4

    const-string v8, "Talk:VideoChatOutputProcessingService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Caught remote exception trying to ensureNonZeroLastMessageDate: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleEndCauseIntent(Landroid/content/Intent;)V
    .locals 14
    .param p1    # Landroid/content/Intent;

    const-string v11, "local_bare_jid"

    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v11, "remote_bare_jid"

    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "is_video"

    const/4 v12, 0x1

    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string v11, "end_cause"

    const/4 v12, -0x1

    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/google/android/talk/DatabaseUtils;->getAccountIdForUsername(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v0

    :try_start_0
    iget-object v11, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mGTalkService:Lcom/google/android/gtalkservice/IGTalkService;

    invoke-interface {v11, v0, v1}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6, v10}, Lcom/google/android/gtalkservice/IImSession;->getChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v2

    if-nez v2, :cond_0

    :cond_0
    if-eqz v2, :cond_1

    invoke-static {v3, v10, v0, v1}, Lcom/google/android/talk/DatabaseUtils;->getNicknameForContact(Landroid/content/ContentResolver;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v2, v9, v7, v5}, Lcom/google/android/gtalkservice/IChatSession;->reportEndCause(Ljava/lang/String;ZI)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v11, "Talk:VideoChatOutputProcessingService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "no ImSession for account id: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/talk/TalkApp;->Logwtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v11, "Talk:VideoChatOutputProcessingService"

    const-string v12, "Problem inserting end cause message into chat history"

    invoke-static {v11, v12, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private handleMissedCallIntent(Landroid/content/Intent;)V
    .locals 14
    .param p1    # Landroid/content/Intent;

    const-string v11, "local_bare_jid"

    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v11, "remote_bare_jid"

    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "is_video"

    const/4 v12, 0x1

    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v11, "no_wifi"

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/google/android/talk/DatabaseUtils;->getAccountIdForUsername(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v0

    :try_start_0
    iget-object v11, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mGTalkService:Lcom/google/android/gtalkservice/IGTalkService;

    invoke-interface {v11, v0, v1}, Lcom/google/android/gtalkservice/IGTalkService;->getImSessionForAccountId(J)Lcom/google/android/gtalkservice/IImSession;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v5, v10}, Lcom/google/android/gtalkservice/IImSession;->getChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-interface {v5, v10}, Lcom/google/android/gtalkservice/IImSession;->createChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v2

    :cond_0
    invoke-static {v3, v10, v0, v1}, Lcom/google/android/talk/DatabaseUtils;->getNicknameForContact(Landroid/content/ContentResolver;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v10, v8, v6, v9}, Lcom/google/android/gtalkservice/IChatSession;->reportMissedCall(Ljava/lang/String;Ljava/lang/String;ZZ)V

    :goto_0
    return-void

    :cond_1
    const-string v11, "Talk:VideoChatOutputProcessingService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "no ImSession for account id: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/talk/TalkApp;->Logwtf(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v11, "Talk:VideoChatOutputProcessingService"

    const-string v12, "Problem inserting missed call message into chat history"

    invoke-static {v11, v12, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private postGTalkServiceTask(Landroid/content/Intent;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mBackgroundHandler:Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;

    invoke-virtual {v0}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput p2, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mGTalkService:Lcom/google/android/gtalkservice/IGTalkService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mBackgroundHandler:Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;

    invoke-virtual {v1, v0}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceBindingRequested:Z

    if-nez v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gtalkservice/IGTalkService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gsf"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceBindingRequested:Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceTaskBuffer:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Landroid/os/HandlerThread;

    const-class v1, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mBackgroundLooper:Landroid/os/Looper;

    new-instance v0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;

    iget-object v1, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mBackgroundLooper:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;-><init>(Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mBackgroundHandler:Lcom/google/android/talk/videochat/VideoChatOutputProcessingService$BackgroundHandler;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceBindingRequested:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.videochat.ACTION_MISSED_CALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, p3}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->postGTalkServiceTask(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    const/4 v1, 0x3

    return v1

    :cond_1
    const-string v1, "com.google.android.videochat.ACTION_END_CAUSE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1, p3}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->postGTalkServiceTask(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    const-string v1, "com.google.android.videochat.ACTION_CALL_IN_PROGRESS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/google/android/talk/videochat/VideoChatOutputProcessingService;->postGTalkServiceTask(Landroid/content/Intent;I)V

    goto :goto_0
.end method
