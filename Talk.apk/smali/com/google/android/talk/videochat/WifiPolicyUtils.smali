.class public Lcom/google/android/talk/videochat/WifiPolicyUtils;
.super Ljava/lang/Object;
.source "WifiPolicyUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static showAlertIfNoWifi(Landroid/content/Context;Z)Z
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/talk/videochat/WifiPolicyUtils;->showAlertIfNoWifi(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)Z

    move-result v0

    return v0
.end method

.method public static showAlertIfNoWifi(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Landroid/content/DialogInterface$OnCancelListener;

    const/4 v2, 0x1

    invoke-static {p0}, Lcom/google/android/talk/videochat/WifiPolicyUtils;->wifiConnected(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz p1, :cond_1

    const v1, 0x7f0c00e2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0c00e4

    new-instance v3, Lcom/google/android/talk/videochat/WifiPolicyUtils$1;

    invoke-direct {v3, p0}, Lcom/google/android/talk/videochat/WifiPolicyUtils$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0c00e5

    new-instance v3, Lcom/google/android/talk/videochat/WifiPolicyUtils$2;

    invoke-direct {v3}, Lcom/google/android/talk/videochat/WifiPolicyUtils$2;-><init>()V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    if-eqz p2, :cond_0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move v1, v2

    :goto_1
    return v1

    :cond_1
    const v1, 0x7f0c00e3

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static wifiConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0    # Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSupplicantState()Landroid/net/wifi/SupplicantState;

    move-result-object v2

    sget-object v3, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    invoke-virtual {v2, v3}, Landroid/net/wifi/SupplicantState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static wifiRequiredForVideoChat(Landroid/content/ContentResolver;)Z
    .locals 2
    .param p0    # Landroid/content/ContentResolver;

    const-string v0, "gtalk_vc_wifi_only"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gsf/Gservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
