.class Lcom/google/android/videochat/CallAudioHelper;
.super Ljava/lang/Object;
.source "CallAudioHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videochat/CallAudioHelper$4;
    }
.end annotation


# instance fields
.field private mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

.field private mAudioDevices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videochat/CallSession$AudioDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBluetoothReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private final mInitializationLock:Ljava/lang/Object;

.field private mIsInitialized:Z

.field private mOnAudioStateChangedListener:Ljava/lang/Runnable;

.field private mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

.field private mSavedMuteState:Z

.field private mSavedSpeakerphoneState:Z

.field private mWiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

.field private onBluetoothTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;
    .param p3    # Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mIsInitialized:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mInitializationLock:Ljava/lang/Object;

    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    new-instance v0, Lcom/google/android/videochat/CallAudioHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/CallAudioHelper$1;-><init>(Lcom/google/android/videochat/CallAudioHelper;)V

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->onBluetoothTimeoutRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/videochat/CallAudioHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/CallAudioHelper$2;-><init>(Lcom/google/android/videochat/CallAudioHelper;)V

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/videochat/CallAudioHelper$3;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/CallAudioHelper$3;-><init>(Lcom/google/android/videochat/CallAudioHelper;)V

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mWiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/google/android/videochat/CallAudioHelper;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/videochat/CallAudioHelper;->mHandler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/google/android/videochat/CallAudioHelper;->mOnAudioStateChangedListener:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->hasEarpiece()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    sget-object v1, Lcom/google/android/videochat/CallSession$AudioDevice;->EARPIECE:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    sget-object v1, Lcom/google/android/videochat/CallSession$AudioDevice;->SPEAKERPHONE:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videochat/CallAudioHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->onBluetoothTimeout()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videochat/CallAudioHelper;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videochat/CallAudioHelper;)Lcom/google/android/videochat/CallState$AudioDeviceState;
    .locals 1
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/videochat/CallAudioHelper;Lcom/google/android/videochat/CallState$AudioDeviceState;)Lcom/google/android/videochat/CallState$AudioDeviceState;
    .locals 0
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;
    .param p1    # Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object p1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/videochat/CallAudioHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->cancelBluetoothTimer()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videochat/CallAudioHelper;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videochat/CallAudioHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->reportUpdate()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/videochat/CallAudioHelper;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videochat/CallAudioHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->usePendingAudioDeviceState()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/videochat/CallAudioHelper;)Z
    .locals 1
    .param p0    # Lcom/google/android/videochat/CallAudioHelper;

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->hasEarpiece()Z

    move-result v0

    return v0
.end method

.method private cancelBluetoothTimer()V
    .locals 2

    const-string v0, "Canceling bluetooth timer"

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->onBluetoothTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private hasEarpiece()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x3

    const-string v1, "vclib:CallAudioHelper"

    invoke-static {v0, v1, p1}, Lcom/google/android/videochat/util/LogUtil;->LOG(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x6

    const-string v1, "vclib:CallAudioHelper"

    invoke-static {v0, v1, p1}, Lcom/google/android/videochat/util/LogUtil;->LOG(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onBluetoothTimeout()V
    .locals 2

    const-string v0, "Starting or stopping Bluetooth timed out"

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->cancelBluetoothTimer()V

    sget-object v0, Lcom/google/android/videochat/CallAudioHelper$4;->$SwitchMap$com$google$android$videochat$CallState$AudioDeviceState:[I

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-virtual {v1}, Lcom/google/android/videochat/CallState$AudioDeviceState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "We thought BT had timed out, but it\'s actually on; updating state."

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->loge(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    :goto_1
    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->reportUpdate()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->usePendingAudioDeviceState()V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "We thought BT had timed out, but it\'s actually off; updating state."

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->loge(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->usePendingAudioDeviceState()V

    :goto_2
    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->reportUpdate()V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private registerForBluetoothIntentBroadcast()V
    .locals 3

    const-string v1, "registerForBluetoothIntentBroadcast"

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.media.SCO_AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private registerForWiredHeadsetIntentBroadcast()V
    .locals 3

    const-string v1, "registerForWiredHeadsetIntentBroadcast"

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mWiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private reportUpdate()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportUpdate: state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", devices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mOnAudioStateChangedListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mOnAudioStateChangedListener:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method private setSpeakerphoneOn(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSpeakerphoneOn("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), wasOn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0
.end method

.method private startBluetoothTimer()V
    .locals 4

    const-string v0, "Starting bluetooth timer"

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->onBluetoothTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private turnOffBluetooth()Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "turnOffBluetooth"

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    sget-object v2, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    sget-object v2, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "turnOffBluetooth: state is already "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cannot turn off"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "turnOffBluetooth: SCO is already off, we were out of sync"

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->loge(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->usePendingAudioDeviceState()V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->reportUpdate()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->cancelBluetoothTimer()V

    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_TURNING_OFF:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->startBluetoothTimer()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private turnOnBluetooth()Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "turnOnBluetooth"

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    sget-object v2, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    sget-object v2, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    if-ne v1, v2, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "turnOnBluetooth: state is already "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cannot turn on"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "turnOnBluetooth: SCO is already on, we were out of sync"

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->loge(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->cancelBluetoothTimer()V

    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->BLUETOOTH_TURNING_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->startBluetoothTimer()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private unregisterForBluetoothIntentBroadcast()V
    .locals 2

    const-string v0, "unregisterForBluetoothIntentBroadcast"

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private unregisterForWiredHeadsetIntentBroadcast()V
    .locals 2

    const-string v0, "unregisterForWiredHeadsetIntentBroadcast"

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mWiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private usePendingAudioDeviceState()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    sget-object v2, Lcom/google/android/videochat/CallState$AudioDeviceState;->WIRED_HEADSET_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    sget-object v2, Lcom/google/android/videochat/CallSession$AudioDevice;->WIRED_HEADSET:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "usePendingAudioDeviceState: there\'s no pending state or it was WH, but has beenunplugged; defaulting to speakerphone. Pending state was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->loge(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    sget-object v1, Lcom/google/android/videochat/CallState$AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->setSpeakerphoneOn(Z)V

    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "usePendingAudioDeviceState: using "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v3, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    sget-object v2, Lcom/google/android/videochat/CallState$AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    if-ne v1, v2, :cond_2

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->setSpeakerphoneOn(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public deinitAudio()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mInitializationLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mIsInitialized:Z

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "deinitAudio: turn off BT SCO"

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->cancelBluetoothTimer()V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->unregisterForWiredHeadsetIntentBroadcast()V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->unregisterForBluetoothIntentBroadcast()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deinitAudio: set mute back to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mSavedMuteState:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", speakerphone back to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mSavedSpeakerphoneState:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mSavedMuteState:Z

    invoke-virtual {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->setMute(Z)V

    iget-boolean v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mSavedSpeakerphoneState:Z

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->setSpeakerphoneOn(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mIsInitialized:Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAudioDeviceState()Lcom/google/android/videochat/CallState$AudioDeviceState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    return-object v0
.end method

.method public getAudioDevices()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videochat/CallSession$AudioDevice;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public initAudio()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/videochat/CallAudioHelper;->mInitializationLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mIsInitialized:Z

    if-eqz v1, :cond_0

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videochat/CallAudioHelper;->isMute()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mSavedMuteState:Z

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mSavedSpeakerphoneState:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Saved mute = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/videochat/CallAudioHelper;->mSavedMuteState:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", speakerphone = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/videochat/CallAudioHelper;->mSavedSpeakerphoneState:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    const-string v1, "initAudio: turning speakerphone on by default"

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->setSpeakerphoneOn(Z)V

    sget-object v1, Lcom/google/android/videochat/CallState$AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->registerForBluetoothIntentBroadcast()V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->registerForWiredHeadsetIntentBroadcast()V

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v3

    if-ne v1, v3, :cond_1

    const-string v1, "BT device was connected at start of call, turning SCO on..."

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDevices:Ljava/util/Set;

    sget-object v3, Lcom/google/android/videochat/CallSession$AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/android/videochat/CallSession$AudioDevice;->BLUETOOTH_HEADSET:Lcom/google/android/videochat/CallSession$AudioDevice;

    invoke-virtual {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->setAudioDevice(Lcom/google/android/videochat/CallSession$AudioDevice;)V

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mIsInitialized:Z

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isMute()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    return v0
.end method

.method public setAudioDevice(Lcom/google/android/videochat/CallSession$AudioDevice;)V
    .locals 3
    .param p1    # Lcom/google/android/videochat/CallSession$AudioDevice;

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/videochat/CallAudioHelper$4;->$SwitchMap$com$google$android$videochat$CallSession$AudioDevice:[I

    invoke-virtual {p1}, Lcom/google/android/videochat/CallSession$AudioDevice;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->reportUpdate()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->turnOnBluetooth()Z

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->turnOffBluetooth()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videochat/CallAudioHelper;->setSpeakerphoneOn(Z)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->SPEAKERPHONE_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->turnOffBluetooth()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->WIRED_HEADSET_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    :goto_2
    invoke-direct {p0, v2}, Lcom/google/android/videochat/CallAudioHelper;->setSpeakerphoneOn(Z)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->WIRED_HEADSET_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    goto :goto_2

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->turnOffBluetooth()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->EARPIECE_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mPendingAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    :goto_3
    invoke-direct {p0, v2}, Lcom/google/android/videochat/CallAudioHelper;->setSpeakerphoneOn(Z)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/videochat/CallState$AudioDeviceState;->EARPIECE_ON:Lcom/google/android/videochat/CallState$AudioDeviceState;

    iput-object v0, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioDeviceState:Lcom/google/android/videochat/CallState$AudioDeviceState;

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setMute(Z)V
    .locals 3
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/videochat/CallAudioHelper;->isMute()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMute: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wasMute="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/videochat/CallAudioHelper;->log(Ljava/lang/String;)V

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/videochat/CallAudioHelper;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    invoke-direct {p0}, Lcom/google/android/videochat/CallAudioHelper;->reportUpdate()V

    goto :goto_0
.end method
