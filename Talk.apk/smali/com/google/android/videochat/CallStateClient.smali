.class public Lcom/google/android/videochat/CallStateClient;
.super Ljava/lang/Object;
.source "CallStateClient.java"


# instance fields
.field private mCallStateListener:Lcom/google/android/videochat/CallStateListener;

.field protected mContext:Landroid/content/Context;

.field private mIsListening:Z

.field private mSoftConnection:Landroid/content/ServiceConnection;

.field private mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/videochat/CallStateClient$1;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/CallStateClient$1;-><init>(Lcom/google/android/videochat/CallStateClient;)V

    iput-object v0, p0, Lcom/google/android/videochat/CallStateClient;->mCallStateListener:Lcom/google/android/videochat/CallStateListener;

    new-instance v0, Lcom/google/android/videochat/CallStateClient$2;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/CallStateClient$2;-><init>(Lcom/google/android/videochat/CallStateClient;)V

    iput-object v0, p0, Lcom/google/android/videochat/CallStateClient;->mSoftConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/google/android/videochat/CallStateClient;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videochat/CallStateClient;Lcom/google/android/videochat/VideoChatService$SoftBinder;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/CallStateClient;
    .param p1    # Lcom/google/android/videochat/VideoChatService$SoftBinder;

    invoke-direct {p0, p1}, Lcom/google/android/videochat/CallStateClient;->onSoftVideoChatServiceBound(Lcom/google/android/videochat/VideoChatService$SoftBinder;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/videochat/CallStateClient;Lcom/google/android/videochat/VideoChatService$SoftBinder;)Lcom/google/android/videochat/VideoChatService$SoftBinder;
    .locals 0
    .param p0    # Lcom/google/android/videochat/CallStateClient;
    .param p1    # Lcom/google/android/videochat/VideoChatService$SoftBinder;

    iput-object p1, p0, Lcom/google/android/videochat/CallStateClient;->mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    return-object p1
.end method

.method private bindVideoChatService()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.talk.SOFT_BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CallStateClient;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/videochat/VideoChatService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/videochat/CallStateClient;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videochat/CallStateClient;->mSoftConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method private onSoftVideoChatServiceBound(Lcom/google/android/videochat/VideoChatService$SoftBinder;)V
    .locals 2
    .param p1    # Lcom/google/android/videochat/VideoChatService$SoftBinder;

    iput-object p1, p0, Lcom/google/android/videochat/CallStateClient;->mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    iget-boolean v0, p0, Lcom/google/android/videochat/CallStateClient;->mIsListening:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/CallStateClient;->mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    iget-object v1, p0, Lcom/google/android/videochat/CallStateClient;->mCallStateListener:Lcom/google/android/videochat/CallStateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/VideoChatService$SoftBinder;->addRemoteCallStateListener(Lcom/google/android/videochat/CallStateListener;)V

    iget-object v0, p0, Lcom/google/android/videochat/CallStateClient;->mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/VideoChatService$SoftBinder;->requestCallStateUpdate(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCallStateUpdate(Ljava/lang/String;Lcom/google/android/videochat/CallState;ZLjava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/videochat/CallState;
    .param p3    # Z
    .param p4    # Ljava/lang/Object;

    return-void
.end method

.method public onChatRoomUpdate(Ljava/util/Set;Ljava/util/Set;ZLjava/lang/Object;)V
    .locals 0
    .param p3    # Z
    .param p4    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videochat/NamedSource;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/videochat/NamedSource;",
            ">;Z",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onLoudestSpeakerUpdate(IZLjava/lang/Object;)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .param p3    # Ljava/lang/Object;

    return-void
.end method

.method public requestUpdate()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videochat/CallStateClient;->requestUpdate(Ljava/lang/Object;)V

    return-void
.end method

.method public requestUpdate(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videochat/CallStateClient;->mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/CallStateClient;->mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    invoke-virtual {v0, p1}, Lcom/google/android/videochat/VideoChatService$SoftBinder;->requestCallStateUpdate(Ljava/lang/Object;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startListening()V
    .locals 2

    const-string v0, "vclib:CallStateClient"

    const-string v1, "startListening"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videochat/CallStateClient;->mIsListening:Z

    invoke-direct {p0}, Lcom/google/android/videochat/CallStateClient;->bindVideoChatService()V

    return-void
.end method

.method public stopListening()V
    .locals 3

    const-string v1, "vclib:CallStateClient"

    const-string v2, "stopListening"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videochat/CallStateClient;->mIsListening:Z

    iget-object v1, p0, Lcom/google/android/videochat/CallStateClient;->mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videochat/CallStateClient;->mSoftServiceBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    iget-object v2, p0, Lcom/google/android/videochat/CallStateClient;->mCallStateListener:Lcom/google/android/videochat/CallStateListener;

    invoke-virtual {v1, v2}, Lcom/google/android/videochat/VideoChatService$SoftBinder;->removeRemoteCallStateListener(Lcom/google/android/videochat/CallStateListener;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videochat/CallStateClient;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videochat/CallStateClient;->mSoftConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "vclib:CallStateClient"

    const-string v2, "Problem unbinding service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
