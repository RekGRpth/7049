.class Lcom/google/android/videochat/CameraManager;
.super Ljava/lang/Object;
.source "CameraManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videochat/CameraManager$CameraManagerCallback;,
        Lcom/google/android/videochat/CameraManager$FrameOutputParameters;,
        Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;
    }
.end annotation


# static fields
.field private static final CAMERA_STATE_CLOSED:I = 0x0

.field private static final CAMERA_STATE_NATIVE_WANTS:I = 0x1

.field private static final CAMERA_STATE_RUNNING:I = 0x2

.field private static final CAMERA_STATE_SUSPENDED:I = 0x3

.field private static final DEBUG:Z = false

.field private static final DEFAULT_FRAME_RATE:I = 0xf

.field private static final DELAY_BEFORE_FIRST_DIM_FRAME_PUSH_MS:I = 0x3e8

.field private static final DELAY_BETWEEN_DIM_FRAME_PUSH_MS:I = 0x3e8

.field private static final FRAME_PUSHER_MAX_WAIT_MS:I = 0x3e8

.field private static final FRAME_PUSHER_PUSH_FRAME_MSG:I = 0x64

.field private static final FRAME_PUSHER_QUIT_MSG:I = 0x65

.field private static final TAG:Ljava/lang/String; = "vclib:CameraManager"

.field private static final TEST_STABILIZATION:Ljava/lang/String;

.field private static volatile sInstance:Lcom/google/android/videochat/CameraManager;

.field private static final sInstanceLock:Ljava/lang/Object;


# instance fields
.field private mCallback:Lcom/google/android/videochat/CameraManager$CameraManagerCallback;

.field private mCamera:Landroid/hardware/Camera;

.field private final mCameraLock:Ljava/lang/Object;

.field private mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

.field private mCameraState:I

.field private mCurrentCameraIndex:I

.field private mCurrentCameraLock:Ljava/lang/Object;

.field private mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

.field private mDefaultCamera:Lcom/google/android/videochat/CameraSpecification;

.field private mDisableCamera:Z

.field private mFinalOutputParams:Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

.field private mFramePusher:Landroid/os/Handler;

.field private final mFramePusherLock:Ljava/lang/Object;

.field private mFramePusherPending:Z

.field private final mFramePusherRunnable:Ljava/lang/Runnable;

.field private mHaveReadCameraIds:Z

.field private mLastReportedParameters:Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

.field private mLastSourceTimeNs:J

.field private mLastTranslatedTimeNs:J

.field private mLastWallClockTimeNs:J

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNumAvailableCameras:I

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/videochat/CameraManager;->TEST_STABILIZATION:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/videochat/CameraManager;->sInstanceLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videochat/CameraManager;->mHaveReadCameraIds:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherLock:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/videochat/CameraManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/CameraManager$1;-><init>(Lcom/google/android/videochat/CameraManager;)V

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherRunnable:Ljava/lang/Runnable;

    const-string v0, "Talk.disableCamera"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "vclib:CameraManager"

    const-string v1, "Disabling camera"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videochat/CameraManager;->mDisableCamera:Z

    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videochat/CameraManager;->mLastWallClockTimeNs:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/videochat/CameraManager;->mLastTranslatedTimeNs:J

    invoke-static {}, Lcom/google/android/videochat/util/LogUtil;->refreshTalkLogLevel()I

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->readCameraIds()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videochat/CameraManager;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/videochat/CameraManager;

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/videochat/CameraManager;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/videochat/CameraManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherPending:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/videochat/CameraManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/videochat/CameraManager;

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusher:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/videochat/CameraManager;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0    # Lcom/google/android/videochat/CameraManager;
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/videochat/CameraManager;->mFramePusher:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/videochat/CameraManager;J)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/CameraManager;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/videochat/CameraManager;->nativePushPausedFrame(J)V

    return-void
.end method

.method public static acquireCameraNative()Z
    .locals 1

    invoke-static {}, Lcom/google/android/videochat/CameraManager;->getInstance()Lcom/google/android/videochat/CameraManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/google/android/videochat/CameraManager;->openCameraForNative()Z

    move-result v0

    return v0
.end method

.method private chooseDefaultCamera()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->readCameraIds()V

    iget v1, p0, Lcom/google/android/videochat/CameraManager;->mNumAvailableCameras:I

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mDefaultCamera:Lcom/google/android/videochat/CameraSpecification;

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/google/android/videochat/CameraManager;->mNumAvailableCameras:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraId:I

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mDefaultCamera:Lcom/google/android/videochat/CameraSpecification;

    iget v2, v2, Lcom/google/android/videochat/CameraSpecification;->cameraId:I

    if-ne v1, v2, :cond_2

    iput v0, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraIndex:I

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mDefaultCamera:Lcom/google/android/videochat/CameraSpecification;

    iget-object v1, v1, Lcom/google/android/videochat/CameraSpecification;->previewSize:Lcom/google/android/videochat/Size;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mDefaultCamera:Lcom/google/android/videochat/CameraSpecification;

    iget-object v2, v2, Lcom/google/android/videochat/CameraSpecification;->previewSize:Lcom/google/android/videochat/Size;

    iput-object v2, v1, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/videochat/Size;

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraIndex:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_3
    iget v1, p0, Lcom/google/android/videochat/CameraManager;->mNumAvailableCameras:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraIsFrontFacing:Z

    if-eqz v1, :cond_4

    iput v0, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraIndex:I

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private static convertIntegerListToIntArray(Ljava/util/List;)[I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    new-array v3, v5, [I

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v3, v0

    move v0, v1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private createLastFramePusher()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherRunnable:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    const-string v1, "frame push"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherPending:Z

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private destroyLastFramePusher()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusher:Landroid/os/Handler;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherPending:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusherLock:Ljava/lang/Object;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusher:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusher:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusher:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFramePusher:Landroid/os/Handler;

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private ensureCameraChosen()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->chooseDefaultCamera()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private frameTimeBasisChanged()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/videochat/CameraManager;->mLastSourceTimeNs:J

    return-void
.end method

.method public static getInstance()Lcom/google/android/videochat/CameraManager;
    .locals 2

    sget-object v0, Lcom/google/android/videochat/CameraManager;->sInstance:Lcom/google/android/videochat/CameraManager;

    if-nez v0, :cond_1

    sget-object v1, Lcom/google/android/videochat/CameraManager;->sInstanceLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/videochat/CameraManager;->sInstance:Lcom/google/android/videochat/CameraManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/videochat/CameraManager;

    invoke-direct {v0}, Lcom/google/android/videochat/CameraManager;-><init>()V

    sput-object v0, Lcom/google/android/videochat/CameraManager;->sInstance:Lcom/google/android/videochat/CameraManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/google/android/videochat/CameraManager;->sInstance:Lcom/google/android/videochat/CameraManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getSupportedFrameRates()[I
    .locals 6

    iget-object v3, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    if-nez v2, :cond_0

    const-string v2, "vclib:CameraManager"

    const-string v4, "no camera, so returning null from getSupportedFrameRates"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    monitor-exit v3

    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFrameRates()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v2, "vclib:CameraManager"

    const-string v4, "Camera getSupportedPreviewFrameRates returned null"

    invoke-static {v2, v4}, Lcom/google/android/videochat/util/LogUtil;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v5, 0xf

    aput v5, v2, v4

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v2, "vclib:CameraManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Camera getSupportedFrameRates "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/videochat/util/LogUtil;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/videochat/CameraManager;->convertIntegerListToIntArray(Ljava/util/List;)[I

    move-result-object v2

    goto :goto_0
.end method

.method public static getSupportedFrameRatesNative()[I
    .locals 1

    invoke-static {}, Lcom/google/android/videochat/CameraManager;->getInstance()Lcom/google/android/videochat/CameraManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/google/android/videochat/CameraManager;->getSupportedFrameRates()[I

    move-result-object v0

    return-object v0
.end method

.method private final native nativePushPausedFrame(J)V
.end method

.method private openCameraForNative()Z
    .locals 5

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/videochat/CameraManager;->mDisableCamera:Z

    if-nez v2, :cond_0

    const-string v2, "vclib:CameraManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "openCameraForNative state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/videochat/util/LogUtil;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    monitor-exit v1

    :goto_1
    return v0

    :pswitch_1
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_2
    :try_start_1
    const-string v2, "vclib:CameraManager"

    const-string v3, "got openCameraForNative in state NATIVE_WANTS"

    invoke-static {v2, v3}, Lcom/google/android/videochat/util/LogUtil;->Logwtf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private openCameraIfNeeded()V
    .locals 7

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    if-nez v4, :cond_3

    iget-boolean v4, p0, Lcom/google/android/videochat/CameraManager;->mDisableCamera:Z

    if-nez v4, :cond_3

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->ensureCameraChosen()V

    const-string v4, "vclib:CameraManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Opening Camera "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget v6, v6, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget v4, v4, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraId:I

    invoke-static {v4}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    const-string v4, "video-stabilization-supported"

    invoke-virtual {v0, v4}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "true"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "video-stabilization"

    const-string v5, "false"

    invoke-virtual {v0, v4, v5}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-boolean v4, v4, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->haveReadCameraParameters:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoExposureLockSupported()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isAutoWhiteBalanceLockSupported()Z

    move-result v5

    if-eqz v5, :cond_4

    :goto_0
    iput-boolean v2, v4, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraSupports3ALocks:Z

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->haveReadCameraParameters:Z

    :cond_1
    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-boolean v2, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCallback:Lcom/google/android/videochat/CameraManager$CameraManagerCallback;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/videochat/CameraManager$CameraManagerCallback;->getCaptureSizeForCamera(Ljava/util/List;)Lcom/google/android/videochat/Size;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/videochat/Size;

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    :cond_2
    const-string v2, "vclib:CameraManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting camera preview size to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-object v5, v5, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/videochat/Size;

    invoke-virtual {v5}, Lcom/google/android/videochat/Size;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/videochat/util/LogUtil;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-object v2, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/videochat/Size;

    iget v2, v2, Lcom/google/android/videochat/Size;->width:I

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-object v4, v4, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/videochat/Size;

    iget v4, v4, Lcom/google/android/videochat/Size;->height:I

    invoke-virtual {v0, v2, v4}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mFinalOutputParams:Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mFinalOutputParams:Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

    iget v2, v2, Lcom/google/android/videochat/CameraManager$FrameOutputParameters;->frameRate:I

    invoke-virtual {v0, v2}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCallback:Lcom/google/android/videochat/CameraManager$CameraManagerCallback;

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-object v4, v4, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/videochat/Size;

    iget-object v5, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget v5, v5, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->orientation:I

    iget-object v6, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-boolean v6, v6, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraIsFrontFacing:Z

    invoke-interface {v2, v4, v5, v6}, Lcom/google/android/videochat/CameraManager$CameraManagerCallback;->onCameraOpen(Lcom/google/android/videochat/Size;IZ)V

    :cond_3
    monitor-exit v3

    return-void

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    :cond_5
    const/16 v2, 0xf

    invoke-virtual {v0, v2}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private readCameraIds()V
    .locals 6

    const/4 v3, 0x1

    iget-boolean v2, p0, Lcom/google/android/videochat/CameraManager;->mHaveReadCameraIds:Z

    if-nez v2, :cond_2

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    iput v2, p0, Lcom/google/android/videochat/CameraManager;->mNumAvailableCameras:I

    iget v2, p0, Lcom/google/android/videochat/CameraManager;->mNumAvailableCameras:I

    new-array v2, v2, [Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iput-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/videochat/CameraManager;->mNumAvailableCameras:I

    if-ge v1, v2, :cond_1

    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    new-instance v4, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;-><init>(Lcom/google/android/videochat/CameraManager$1;)V

    aput-object v4, v2, v1

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    aget-object v2, v2, v1

    iput v1, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraId:I

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    aget-object v4, v2, v1

    iget v2, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v2, v3, :cond_0

    move v2, v3

    :goto_1
    iput-boolean v2, v4, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraIsFrontFacing:Z

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    aget-object v2, v2, v1

    iget v4, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iput v4, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->orientation:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/videochat/CameraManager;->mHaveReadCameraIds:Z

    :cond_2
    return-void
.end method

.method private releaseCamera()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "vclib:CameraManager"

    const-string v2, "releaseCamera"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private releaseCameraForNative()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videochat/CameraManager;->mDisableCamera:Z

    if-nez v0, :cond_0

    const-string v0, "vclib:CameraManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "releaseCameraForNative state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/videochat/util/LogUtil;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->releaseCamera()V

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    const-string v0, "vclib:CameraManager"

    const-string v2, "releaseForNative called while in NATIVE_WANTS but camera didn\'t actually release"

    invoke-static {v0, v2}, Lcom/google/android/videochat/util/LogUtil;->Logwtf(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static releaseCameraNative()V
    .locals 1

    invoke-static {}, Lcom/google/android/videochat/CameraManager;->getInstance()Lcom/google/android/videochat/CameraManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/google/android/videochat/CameraManager;->releaseCameraForNative()V

    return-void
.end method

.method private setCaptureParameters(IIII)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "vclib:CameraManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Camera setCaptureParameters state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videochat/util/LogUtil;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

    new-instance v2, Lcom/google/android/videochat/Size;

    invoke-direct {v2, p1, p2}, Lcom/google/android/videochat/Size;-><init>(II)V

    invoke-direct {v0, v2, p4}, Lcom/google/android/videochat/CameraManager$FrameOutputParameters;-><init>(Lcom/google/android/videochat/Size;I)V

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFinalOutputParams:Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCallback:Lcom/google/android/videochat/CameraManager$CameraManagerCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCallback:Lcom/google/android/videochat/CameraManager$CameraManagerCallback;

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mFinalOutputParams:Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

    invoke-interface {v0, v2}, Lcom/google/android/videochat/CameraManager$CameraManagerCallback;->onFrameOutputSet(Lcom/google/android/videochat/CameraManager$FrameOutputParameters;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setCaptureParametersNative(IIII)V
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Lcom/google/android/videochat/CameraManager;->getInstance()Lcom/google/android/videochat/CameraManager;

    move-result-object v0

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/videochat/CameraManager;->setCaptureParameters(IIII)V

    return-void
.end method

.method private useCameraInternal(ILcom/google/android/videochat/Size;)V
    .locals 6
    .param p1    # I
    .param p2    # Lcom/google/android/videochat/Size;

    iget-object v3, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    const-string v2, "vclib:CameraManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchCamera state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->releaseCamera()V

    iput p1, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraIndex:I

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget v5, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraIndex:I

    aget-object v2, v2, v5

    iput-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iput-object p2, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSize:Lcom/google/android/videochat/Size;

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->previewSizeIsValid:Z

    :cond_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_1
    return-void

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :pswitch_0
    :try_start_5
    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->openCameraIfNeeded()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    sget-object v2, Lcom/google/android/videochat/CameraManager;->TEST_STABILIZATION:Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    if-eqz v0, :cond_2

    new-instance v2, Ljava/io/IOException;

    const-string v4, "not supported"

    invoke-direct {v2, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_0
    move-exception v1

    :try_start_7
    const-string v2, "vclib:CameraManager"

    const-string v4, "setPreviewTexture failed"

    invoke-static {v2, v4}, Lcom/google/android/videochat/util/LogUtil;->LOGE(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    :cond_1
    :try_start_8
    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v2, v4}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_2
    :try_start_9
    const-string v2, "vclib:CameraManager"

    const-string v4, "switchCamera calling startPreview"

    invoke-static {v2, v4}, Lcom/google/android/videochat/util/LogUtil;->LOGV(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public arePreview3ALocksSupported()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    if-nez v2, :cond_0

    const-string v2, "vclib:CameraManager"

    const-string v3, "Calling arePreview3ALocksSupported without a current camera!"

    invoke-static {v2, v3}, Lcom/google/android/videochat/util/LogUtil;->LOGE(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-boolean v2, v2, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->haveReadCameraParameters:Z

    if-nez v2, :cond_1

    const-string v2, "vclib:CameraManager"

    const-string v3, "Calling arePreview3ALocksSupported before reading camera parameters!"

    invoke-static {v2, v3}, Lcom/google/android/videochat/util/LogUtil;->LOGE(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-boolean v0, v0, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraSupports3ALocks:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getCurrentCameraId()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget v1, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraIndex:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraId:I

    return v0
.end method

.method public getCurrentFrameOutputParameters()Lcom/google/android/videochat/CameraManager$FrameOutputParameters;
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFinalOutputParams:Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

    return-object v0
.end method

.method public reset()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videochat/CameraManager;->mDisableCamera:Z

    if-nez v0, :cond_0

    const-string v0, "vclib:CameraManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reset state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/videochat/util/LogUtil;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->destroyLastFramePusher()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->releaseCamera()V

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->chooseDefaultCamera()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mFinalOutputParams:Lcom/google/android/videochat/CameraManager$FrameOutputParameters;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setApplicationCallback(Lcom/google/android/videochat/CameraManager$CameraManagerCallback;)V
    .locals 2
    .param p1    # Lcom/google/android/videochat/CameraManager$CameraManagerCallback;

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/google/android/videochat/CameraManager;->mCallback:Lcom/google/android/videochat/CameraManager$CameraManagerCallback;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDefaultCamera(Lcom/google/android/videochat/CameraSpecification;)V
    .locals 0
    .param p1    # Lcom/google/android/videochat/CameraSpecification;

    iput-object p1, p0, Lcom/google/android/videochat/CameraManager;->mDefaultCamera:Lcom/google/android/videochat/CameraSpecification;

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->ensureCameraChosen()V

    return-void
.end method

.method public setPreview3ALocks(Z)V
    .locals 4
    .param p1    # Z

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    const-string v1, "vclib:CameraManager"

    const-string v3, "Can\'t lock AE/AWB when camera not running!"

    invoke-static {v1, v3}, Lcom/google/android/videochat/util/LogUtil;->LOGE(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCurrentCameraSettings:Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    iget-boolean v1, v1, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraSupports3ALocks:Z

    if-nez v1, :cond_1

    const-string v1, "vclib:CameraManager"

    const-string v3, "Current camera does not support AE/AWB locks."

    invoke-static {v1, v3}, Lcom/google/android/videochat/util/LogUtil;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setAutoExposureLock(Z)V

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setAutoWhiteBalanceLock(Z)V

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public setPreviewSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    const-string v3, "vclib:CameraManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setPreviewSurfaceTexture "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videochat/util/LogUtil;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/videochat/CameraManager;->mDisableCamera:Z

    if-eqz v3, :cond_0

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->frameTimeBasisChanged()V

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/videochat/CameraManager;->suspendCamera()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videochat/CameraManager;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/google/android/videochat/CameraManager;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget v3, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    packed-switch v3, :pswitch_data_0

    :goto_1
    sget-object v3, Lcom/google/android/videochat/CameraManager;->TEST_STABILIZATION:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/videochat/CameraManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v3, :cond_2

    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v3, p0, Lcom/google/android/videochat/CameraManager;->mMediaPlayer:Landroid/media/MediaPlayer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :try_start_2
    sget-object v3, Lcom/google/android/videochat/CameraManager;->TEST_STABILIZATION:Ljava/lang/String;

    if-eqz v3, :cond_5

    const-class v3, Landroid/media/MediaPlayer;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/graphics/SurfaceTexture;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    const-string v6, "setTexture"

    invoke-virtual {v3, v6, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_2
    if-eqz v0, :cond_3

    :try_start_4
    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    :try_start_5
    sget-object v0, Lcom/google/android/videochat/CameraManager;->TEST_STABILIZATION:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v0, :cond_4

    :try_start_6
    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    sget-object v1, Lcom/google/android/videochat/CameraManager;->TEST_STABILIZATION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_3
    :try_start_7
    new-instance v0, Lcom/google/android/videochat/CameraManager$1Latch;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/CameraManager$1Latch;-><init>(Lcom/google/android/videochat/CameraManager;)V

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/google/android/videochat/CameraManager$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/videochat/CameraManager$2;-><init>(Lcom/google/android/videochat/CameraManager;Lcom/google/android/videochat/CameraManager$1Latch;)V

    invoke-virtual {v1, v3}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    invoke-virtual {v0}, Lcom/google/android/videochat/CameraManager$1Latch;->awaitSet()V

    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->destroyLastFramePusher()V

    monitor-exit v2

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->openCameraIfNeeded()V

    goto :goto_1

    :pswitch_1
    iget-object v3, p0, Lcom/google/android/videochat/CameraManager;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->stopPreview()V

    goto :goto_1

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_2

    :catch_2
    move-exception v0

    move v0, v1

    goto :goto_2

    :catch_3
    move-exception v0

    const-string v0, "vclib:CameraManager"

    const-string v1, "setPreviewTexture failed"

    invoke-static {v0, v1}, Lcom/google/android/videochat/util/LogUtil;->LOGE(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v2

    goto/16 :goto_0

    :catch_4
    move-exception v0

    const-string v1, "vclib:CameraManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public suspendCamera()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "vclib:CameraManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "suspendCamera state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/videochat/util/LogUtil;->LOGD(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/videochat/CameraManager;->mCameraState:I

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->releaseCamera()V

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->frameTimeBasisChanged()V

    invoke-direct {p0}, Lcom/google/android/videochat/CameraManager;->createLastFramePusher()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public translateFrameTime(J)J
    .locals 8
    .param p1    # J

    iget-wide v0, p0, Lcom/google/android/videochat/CameraManager;->mLastTranslatedTimeNs:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/videochat/CameraManager;->mLastSourceTimeNs:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    iget-wide v4, p0, Lcom/google/android/videochat/CameraManager;->mLastSourceTimeNs:J

    sub-long v4, p1, v4

    add-long/2addr v0, v4

    :goto_0
    iput-wide v0, p0, Lcom/google/android/videochat/CameraManager;->mLastTranslatedTimeNs:J

    iput-wide v2, p0, Lcom/google/android/videochat/CameraManager;->mLastWallClockTimeNs:J

    return-wide v0

    :cond_0
    iget-wide v4, p0, Lcom/google/android/videochat/CameraManager;->mLastWallClockTimeNs:J

    sub-long v4, v2, v4

    add-long/2addr v0, v4

    goto :goto_0
.end method

.method public useCamera(Lcom/google/android/videochat/CameraSpecification;)V
    .locals 6
    .param p1    # Lcom/google/android/videochat/CameraSpecification;

    iget-object v3, p0, Lcom/google/android/videochat/CameraManager;->mCameraLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget v2, p0, Lcom/google/android/videochat/CameraManager;->mNumAvailableCameras:I

    if-ge v0, v2, :cond_0

    iget v2, p1, Lcom/google/android/videochat/CameraSpecification;->cameraId:I

    iget-object v4, p0, Lcom/google/android/videochat/CameraManager;->mCameraSettings:[Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/google/android/videochat/CameraManager$CameraSpecificSettings;->cameraId:I

    if-ne v2, v4, :cond_1

    move v1, v0

    :cond_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    const-string v2, "vclib:CameraManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t switch to camera "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/google/android/videochat/CameraSpecification;->cameraId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Invalid id."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v3

    :goto_1
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/google/android/videochat/CameraSpecification;->previewSize:Lcom/google/android/videochat/Size;

    invoke-direct {p0, v1, v2}, Lcom/google/android/videochat/CameraManager;->useCameraInternal(ILcom/google/android/videochat/Size;)V

    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
