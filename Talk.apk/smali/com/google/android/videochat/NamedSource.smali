.class public Lcom/google/android/videochat/NamedSource;
.super Ljava/lang/Object;
.source "NamedSource.java"


# instance fields
.field public final name:Ljava/lang/String;

.field public final nick:Ljava/lang/String;

.field public final removed:Z

.field public final ssrc:I

.field public final ssrcSet:Z

.field public final usage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Z
    .param p6    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/videochat/NamedSource;->nick:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/videochat/NamedSource;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videochat/NamedSource;->usage:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/videochat/NamedSource;->ssrc:I

    iput-boolean p5, p0, Lcom/google/android/videochat/NamedSource;->ssrcSet:Z

    iput-boolean p6, p0, Lcom/google/android/videochat/NamedSource;->removed:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/videochat/NamedSource;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/videochat/NamedSource;

    iget v0, p1, Lcom/google/android/videochat/NamedSource;->ssrc:I

    iget v1, p0, Lcom/google/android/videochat/NamedSource;->ssrc:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/videochat/NamedSource;->ssrc:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/videochat/NamedSource;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videochat/NamedSource;->nick:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videochat/NamedSource;->ssrc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
