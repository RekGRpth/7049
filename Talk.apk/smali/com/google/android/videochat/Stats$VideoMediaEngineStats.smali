.class public Lcom/google/android/videochat/Stats$VideoMediaEngineStats;
.super Lcom/google/android/videochat/Stats;
.source "Stats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videochat/Stats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VideoMediaEngineStats"
.end annotation


# instance fields
.field public receivedFrameHeight:I

.field public receivedFrameWidth:I

.field public receivedFramerate:F

.field public sentFrameHeight:I

.field public sentFrameWidth:I

.field public sentFramerate:F


# direct methods
.method public constructor <init>(FIIFII)V
    .locals 1
    .param p1    # F
    .param p2    # I
    .param p3    # I
    .param p4    # F
    .param p5    # I
    .param p6    # I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videochat/Stats;-><init>(I)V

    iput p1, p0, Lcom/google/android/videochat/Stats$VideoMediaEngineStats;->receivedFramerate:F

    iput p2, p0, Lcom/google/android/videochat/Stats$VideoMediaEngineStats;->receivedFrameWidth:I

    iput p3, p0, Lcom/google/android/videochat/Stats$VideoMediaEngineStats;->receivedFrameHeight:I

    iput p4, p0, Lcom/google/android/videochat/Stats$VideoMediaEngineStats;->sentFramerate:F

    iput p5, p0, Lcom/google/android/videochat/Stats$VideoMediaEngineStats;->sentFrameWidth:I

    iput p6, p0, Lcom/google/android/videochat/Stats$VideoMediaEngineStats;->sentFrameHeight:I

    return-void
.end method
