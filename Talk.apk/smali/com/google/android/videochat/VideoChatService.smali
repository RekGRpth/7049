.class public Lcom/google/android/videochat/VideoChatService;
.super Landroid/app/Service;
.source "VideoChatService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videochat/VideoChatService$StopServiceHandler;,
        Lcom/google/android/videochat/VideoChatService$HardBinder;,
        Lcom/google/android/videochat/VideoChatService$SoftBinder;
    }
.end annotation


# instance fields
.field private mCachedCallSession:Lcom/google/android/videochat/CallSession;

.field private final mCachedCallSessionLock:Ljava/lang/Object;

.field private mCachedStanzaInjector:Lcom/google/android/videochat/StanzaInjector;

.field private final mCachedStanzaInjectorLock:Ljava/lang/Object;

.field private mCallManager:Lcom/google/android/videochat/CallManager;

.field private final mHardBinder:Lcom/google/android/videochat/VideoChatService$HardBinder;

.field private mHardBound:Z

.field private final mKeepAliveRequests:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNetworkConnectionManager:Lcom/google/android/videochat/NetworkConnectionManager;

.field private mNotifyNewJingleInfoReceiver:Landroid/content/BroadcastReceiver;

.field private mOngoingNotificationFactory:Lcom/google/android/videochat/OngoingNotificationFactory;

.field private mOutputReceiverComponent:Landroid/content/ComponentName;

.field private final mSoftBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

.field private mStopServiceHandler:Lcom/google/android/videochat/VideoChatService$StopServiceHandler;

.field private mStopped:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCachedCallSessionLock:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCachedStanzaInjectorLock:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/videochat/VideoChatService$1;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/VideoChatService$1;-><init>(Lcom/google/android/videochat/VideoChatService;)V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mNotifyNewJingleInfoReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/google/android/videochat/VideoChatService$SoftBinder;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/VideoChatService$SoftBinder;-><init>(Lcom/google/android/videochat/VideoChatService;)V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mSoftBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    new-instance v0, Lcom/google/android/videochat/VideoChatService$HardBinder;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/VideoChatService$HardBinder;-><init>(Lcom/google/android/videochat/VideoChatService;)V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mHardBinder:Lcom/google/android/videochat/VideoChatService$HardBinder;

    new-instance v0, Lcom/google/android/videochat/VideoChatService$StopServiceHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/videochat/VideoChatService$StopServiceHandler;-><init>(Lcom/google/android/videochat/VideoChatService;Lcom/google/android/videochat/VideoChatService$1;)V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mStopServiceHandler:Lcom/google/android/videochat/VideoChatService$StopServiceHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videochat/VideoChatService;)Lcom/google/android/videochat/CallManager;
    .locals 1
    .param p0    # Lcom/google/android/videochat/VideoChatService;

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCallManager:Lcom/google/android/videochat/CallManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videochat/VideoChatService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/VideoChatService;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->onCallConnected(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videochat/VideoChatService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/videochat/VideoChatService;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->onCallEnding(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videochat/VideoChatService;)Z
    .locals 1
    .param p0    # Lcom/google/android/videochat/VideoChatService;

    iget-boolean v0, p0, Lcom/google/android/videochat/VideoChatService;->mStopped:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/videochat/VideoChatService;)Lcom/google/android/videochat/CallSession;
    .locals 1
    .param p0    # Lcom/google/android/videochat/VideoChatService;

    invoke-direct {p0}, Lcom/google/android/videochat/VideoChatService;->getCallSession()Lcom/google/android/videochat/CallSession;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videochat/VideoChatService;)Z
    .locals 1
    .param p0    # Lcom/google/android/videochat/VideoChatService;

    invoke-direct {p0}, Lcom/google/android/videochat/VideoChatService;->stopServiceIfSafe()Z

    move-result v0

    return v0
.end method

.method private createSessionKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCallSession()Lcom/google/android/videochat/CallSession;
    .locals 3

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mCachedCallSessionLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCachedCallSession:Lcom/google/android/videochat/CallSession;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/videochat/CallSession;

    iget-object v2, p0, Lcom/google/android/videochat/VideoChatService;->mCallManager:Lcom/google/android/videochat/CallManager;

    invoke-direct {v0, p0, v2}, Lcom/google/android/videochat/CallSession;-><init>(Lcom/google/android/videochat/VideoChatService;Lcom/google/android/videochat/CallManager;)V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCachedCallSession:Lcom/google/android/videochat/CallSession;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCachedCallSession:Lcom/google/android/videochat/CallSession;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getStanzaInjector()Lcom/google/android/videochat/StanzaInjector;
    .locals 3

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mCachedStanzaInjectorLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCachedStanzaInjector:Lcom/google/android/videochat/StanzaInjector;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/videochat/StanzaInjector;

    iget-object v2, p0, Lcom/google/android/videochat/VideoChatService;->mCallManager:Lcom/google/android/videochat/CallManager;

    invoke-direct {v0, p0, v2}, Lcom/google/android/videochat/StanzaInjector;-><init>(Lcom/google/android/videochat/VideoChatService;Lcom/google/android/videochat/CallManager;)V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCachedStanzaInjector:Lcom/google/android/videochat/StanzaInjector;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCachedStanzaInjector:Lcom/google/android/videochat/StanzaInjector;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private initCallManager()Lcom/google/android/videochat/CallManager;
    .locals 2

    invoke-static {p0}, Lcom/google/android/videochat/CallManager;->getInstance(Lcom/google/android/videochat/VideoChatService;)Lcom/google/android/videochat/CallManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/videochat/VideoChatService$2;

    invoke-direct {v1, p0}, Lcom/google/android/videochat/VideoChatService$2;-><init>(Lcom/google/android/videochat/VideoChatService;)V

    invoke-virtual {v0, v1}, Lcom/google/android/videochat/CallManager;->setCallBoundaryCallback(Lcom/google/android/videochat/CallManager$CallBoundaryCallback;)V

    return-object v0
.end method

.method private onCallConnected(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->offerUpgradeToFullJidKeepAliveRequest(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onCallEnding(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->removeKeepAliveRequest(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private stopServiceIfSafe()Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/videochat/VideoChatService;->mStopped:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videochat/VideoChatService;->safeToStop()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "vclib:VideoChatService"

    const-string v3, "Stopping VideoChatService..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/videochat/VideoChatService;->stopSelf()V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videochat/VideoChatService;->mStopped:Z

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method addKeepAliveRequest(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->createSessionKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/android/videochat/util/LogUtil;->getTalkLogLevel()I

    move-result v1

    const/4 v3, 0x2

    if-gt v1, v3, :cond_1

    const-string v1, "vclib:VideoChatService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "add keep-alive for remoteJid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  localBareJid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    :cond_1
    const-string v1, "vclib:VideoChatService"

    const-string v3, "add keep-alive"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCallManager:Lcom/google/android/videochat/CallManager;

    invoke-virtual {v0, p2}, Lcom/google/android/videochat/CallManager;->dump(Ljava/io/PrintWriter;)V

    return-void
.end method

.method public getNetworkConnectionManager()Lcom/google/android/videochat/NetworkConnectionManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mNetworkConnectionManager:Lcom/google/android/videochat/NetworkConnectionManager;

    return-object v0
.end method

.method public getNumKeepAliveRequests()I
    .locals 2

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getOutputReceiverComponent()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mOutputReceiverComponent:Landroid/content/ComponentName;

    return-object v0
.end method

.method public isBound()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/videochat/VideoChatService;->mHardBound:Z

    return v0
.end method

.method public isStopped()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/videochat/VideoChatService;->mStopped:Z

    return v0
.end method

.method offerUpgradeToFullJidKeepAliveRequest(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videochat/util/Util;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/google/android/videochat/VideoChatService;->createSessionKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->createSessionKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->createSessionKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videochat/VideoChatService;->mStopped:Z

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.talk.SOFT_BIND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mSoftBinder:Lcom/google/android/videochat/VideoChatService$SoftBinder;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.talk.HARD_BIND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/videochat/VideoChatService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/videochat/VideoChatService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videochat/VideoChatService;->mHardBound:Z

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mHardBinder:Lcom/google/android/videochat/VideoChatService$HardBinder;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-direct {p0}, Lcom/google/android/videochat/VideoChatService;->initCallManager()Lcom/google/android/videochat/CallManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mCallManager:Lcom/google/android/videochat/CallManager;

    new-instance v1, Lcom/google/android/videochat/NetworkConnectionManager;

    invoke-direct {v1, p0}, Lcom/google/android/videochat/NetworkConnectionManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mNetworkConnectionManager:Lcom/google/android/videochat/NetworkConnectionManager;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.videochat.NOTIFY_JINGLE_INFO"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mNotifyNewJingleInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/videochat/VideoChatService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "vclib:VideoChatService"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v2, p0, Lcom/google/android/videochat/VideoChatService;->mCachedCallSession:Lcom/google/android/videochat/CallSession;

    iput-object v2, p0, Lcom/google/android/videochat/VideoChatService;->mCachedStanzaInjector:Lcom/google/android/videochat/StanzaInjector;

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mCallManager:Lcom/google/android/videochat/CallManager;

    invoke-virtual {v0}, Lcom/google/android/videochat/CallManager;->release()V

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mNetworkConnectionManager:Lcom/google/android/videochat/NetworkConnectionManager;

    invoke-virtual {v0}, Lcom/google/android/videochat/NetworkConnectionManager;->stopUsingMobileHipri()V

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mNotifyNewJingleInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/videochat/VideoChatService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method onNewCallStarting(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->addKeepAliveRequest(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/videochat/VideoChatService;->mStopped:Z

    const/4 v4, 0x2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "com.google.android.videochat.ACTION_INCOMING_STANZA"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "com.google.android.videochat.ACTION_INCOMING_STANZA_RESPONSE"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    :cond_2
    const-string v6, "output_receiver"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    const-string v6, "stanza"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v3}, Lcom/google/android/videochat/VideoChatService;->setOutputReceiverComponent(Landroid/content/ComponentName;)V

    const-string v6, "com.google.android.videochat.ACTION_INCOMING_STANZA"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "local_bare_jid"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/videochat/VideoChatService;->getStanzaInjector()Lcom/google/android/videochat/StanzaInjector;

    move-result-object v6

    invoke-virtual {v6, v5, v1}, Lcom/google/android/videochat/StanzaInjector;->injectStanza(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v6, "original_stanza"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/videochat/VideoChatService;->getStanzaInjector()Lcom/google/android/videochat/StanzaInjector;

    move-result-object v6

    invoke-virtual {v6, v2, v5}, Lcom/google/android/videochat/StanzaInjector;->injectResponseStanza(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.talk.SOFT_BIND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.talk.HARD_BIND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/videochat/VideoChatService;->mHardBound:Z

    invoke-direct {p0}, Lcom/google/android/videochat/VideoChatService;->stopServiceIfSafe()Z

    goto :goto_0
.end method

.method postOngoingNotification(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-static {p1}, Lcom/google/android/videochat/util/Util;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mOngoingNotificationFactory:Lcom/google/android/videochat/OngoingNotificationFactory;

    new-instance v2, Lcom/google/android/videochat/VideoChatService$3;

    invoke-direct {v2, p0}, Lcom/google/android/videochat/VideoChatService$3;-><init>(Lcom/google/android/videochat/VideoChatService;)V

    invoke-interface {v1, v2, v0, p2, p3}, Lcom/google/android/videochat/OngoingNotificationFactory;->requestOngoingNotification(Lcom/google/android/videochat/OngoingNotificationFactory$OngoingNotificationReadyCallback;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method postStopServiceIfSafe()V
    .locals 4

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mStopServiceHandler:Lcom/google/android/videochat/VideoChatService$StopServiceHandler;

    invoke-virtual {v1, v2}, Lcom/google/android/videochat/VideoChatService$StopServiceHandler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mStopServiceHandler:Lcom/google/android/videochat/VideoChatService$StopServiceHandler;

    invoke-virtual {v1, v2}, Lcom/google/android/videochat/VideoChatService$StopServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mStopServiceHandler:Lcom/google/android/videochat/VideoChatService$StopServiceHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/videochat/VideoChatService$StopServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public removeKeepAliveRequest(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x2

    invoke-direct {p0, p1, p2}, Lcom/google/android/videochat/VideoChatService;->createSessionKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/android/videochat/util/LogUtil;->getTalkLogLevel()I

    move-result v3

    if-gt v3, v5, :cond_1

    const-string v3, "vclib:VideoChatService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remove keep-alive for remoteJid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  localBareJid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/videochat/VideoChatService;->stopServiceIfSafe()Z

    return-void

    :cond_1
    :try_start_1
    const-string v3, "vclib:VideoChatService"

    const-string v5, "remove keep-alive"

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_2
    :try_start_2
    invoke-static {p1}, Lcom/google/android/videochat/util/Util;->parseBareAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/google/android/videochat/VideoChatService;->createSessionKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/android/videochat/util/LogUtil;->getTalkLogLevel()I

    move-result v3

    if-gt v3, v5, :cond_3

    const-string v3, "vclib:VideoChatService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remove keep-alive for remoteJid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  localBareJid: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v3, "vclib:VideoChatService"

    const-string v5, "remove keep-alive"

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method removeOngoingNotification()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videochat/VideoChatService;->stopForeground(Z)V

    return-void
.end method

.method public safeToStop()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videochat/VideoChatService;->mKeepAliveRequests:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videochat/VideoChatService;->mHardBound:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setOngoingNotificationFactory(Lcom/google/android/videochat/OngoingNotificationFactory;)V
    .locals 0
    .param p1    # Lcom/google/android/videochat/OngoingNotificationFactory;

    iput-object p1, p0, Lcom/google/android/videochat/VideoChatService;->mOngoingNotificationFactory:Lcom/google/android/videochat/OngoingNotificationFactory;

    return-void
.end method

.method setOutputReceiverComponent(Landroid/content/ComponentName;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;

    iput-object p1, p0, Lcom/google/android/videochat/VideoChatService;->mOutputReceiverComponent:Landroid/content/ComponentName;

    return-void
.end method
