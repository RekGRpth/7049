.class public Lcom/google/android/videochat/VideoChatServiceBinder;
.super Ljava/lang/Object;
.source "VideoChatServiceBinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;
    }
.end annotation


# instance fields
.field private mBindRequested:Z

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mOutgoingStanzaReceiverComponent:Landroid/content/ComponentName;

.field private mServiceBoundCallback:Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/ComponentName;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/ComponentName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mBindRequested:Z

    new-instance v0, Lcom/google/android/videochat/VideoChatServiceBinder$1;

    invoke-direct {v0, p0}, Lcom/google/android/videochat/VideoChatServiceBinder$1;-><init>(Lcom/google/android/videochat/VideoChatServiceBinder;)V

    iput-object v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mOutgoingStanzaReceiverComponent:Landroid/content/ComponentName;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videochat/VideoChatServiceBinder;)Landroid/content/ComponentName;
    .locals 1
    .param p0    # Lcom/google/android/videochat/VideoChatServiceBinder;

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mOutgoingStanzaReceiverComponent:Landroid/content/ComponentName;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videochat/VideoChatServiceBinder;)Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;
    .locals 1
    .param p0    # Lcom/google/android/videochat/VideoChatServiceBinder;

    iget-object v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mServiceBoundCallback:Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/videochat/VideoChatServiceBinder;Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;)Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;
    .locals 0
    .param p0    # Lcom/google/android/videochat/VideoChatServiceBinder;
    .param p1    # Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;

    iput-object p1, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mServiceBoundCallback:Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;

    return-object p1
.end method


# virtual methods
.method public bind(Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mBindRequested:Z

    if-eqz v0, :cond_0

    const-string v0, "vclib:VideoChatServiceBinder"

    const-string v1, "bind already called; ignoring repeated call"

    invoke-static {v0, v1}, Lcom/google/android/videochat/util/LogUtil;->LOGW(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mServiceBoundCallback:Lcom/google/android/videochat/VideoChatServiceBinder$ServiceBoundCallback;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.talk.HARD_BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/videochat/VideoChatService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mBindRequested:Z

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unbind()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mBindRequested:Z

    if-nez v0, :cond_0

    const-string v0, "vclib:VideoChatServiceBinder"

    const-string v1, "service not bound; ignoring unbind call"

    invoke-static {v0, v1}, Lcom/google/android/videochat/util/LogUtil;->LOGW(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videochat/VideoChatServiceBinder;->mBindRequested:Z

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
