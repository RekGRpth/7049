.class Lcom/android/providers/telephony/TelephonyProvider$1;
.super Ljava/lang/Object;
.source "TelephonyProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/providers/telephony/TelephonyProvider;->initDatabaseIfNeeded(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/telephony/TelephonyProvider;

.field final synthetic val$db:Landroid/database/sqlite/SQLiteDatabase;

.field final synthetic val$initTable:Ljava/lang/String;

.field final synthetic val$sp:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Lcom/android/providers/telephony/TelephonyProvider;Landroid/content/SharedPreferences;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->this$0:Lcom/android/providers/telephony/TelephonyProvider;

    iput-object p2, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->val$sp:Landroid/content/SharedPreferences;

    iput-object p3, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->val$db:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p4, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->val$initTable:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->val$sp:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->this$0:Lcom/android/providers/telephony/TelephonyProvider;

    invoke-static {v1}, Lcom/android/providers/telephony/TelephonyProvider;->access$000(Lcom/android/providers/telephony/TelephonyProvider;)Lcom/android/providers/telephony/TelephonyProvider$DatabaseHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->val$db:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v3, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->val$initTable:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/providers/telephony/TelephonyProvider$DatabaseHelper;->initDatabase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    const-string v1, "load_slot1_apn"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v1, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->this$0:Lcom/android/providers/telephony/TelephonyProvider;

    invoke-static {v1}, Lcom/android/providers/telephony/TelephonyProvider;->access$100(Lcom/android/providers/telephony/TelephonyProvider;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/android/providers/telephony/TelephonyProvider$1;->this$0:Lcom/android/providers/telephony/TelephonyProvider;

    invoke-static {v1}, Lcom/android/providers/telephony/TelephonyProvider;->access$100(Lcom/android/providers/telephony/TelephonyProvider;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    const-string v1, "TelephonyProvider"

    const-string v3, " notify exception "

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
