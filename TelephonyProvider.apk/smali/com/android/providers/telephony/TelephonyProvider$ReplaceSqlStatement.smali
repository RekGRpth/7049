.class interface abstract Lcom/android/providers/telephony/TelephonyProvider$ReplaceSqlStatement;
.super Ljava/lang/Object;
.source "TelephonyProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/telephony/TelephonyProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "ReplaceSqlStatement"
.end annotation


# static fields
.field public static final APN:I = 0x5

.field public static final AUTH_TYPE:I = 0xe

.field public static final BEARER:I = 0x16

.field public static final CARRIER_ENABLED:I = 0x15

.field public static final CARRIER_GEMINI_INSERT_SQL:Ljava/lang/String; = "INSERT INTO carriers_gemini (name, numeric, mcc, mnc, apn, user, server, password, proxy, port, mmsproxy, mmsport, mmsc, authtype, type, current, sourcetype, csdnum, protocol, roaming_protocol, carrier_enabled, bearer)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

.field public static final CARRIER_INSERT_SQL:Ljava/lang/String; = "INSERT INTO carriers (name, numeric, mcc, mnc, apn, user, server, password, proxy, port, mmsproxy, mmsport, mmsc, authtype, type, current, sourcetype, csdnum, protocol, roaming_protocol, carrier_enabled, bearer)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

.field public static final CARRIER_MVNO_GEMINI_INSERT_SQL:Ljava/lang/String; = "INSERT INTO carriers_gemini (name, numeric, mcc, mnc, apn, user, server, password, proxy, port, mmsproxy, mmsport, mmsc, authtype, type, current, sourcetype, csdnum, protocol, roaming_protocol, carrier_enabled, bearer, spn, imsi)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

.field public static final CARRIER_MVNO_INSERT_SQL:Ljava/lang/String; = "INSERT INTO carriers (name, numeric, mcc, mnc, apn, user, server, password, proxy, port, mmsproxy, mmsport, mmsc, authtype, type, current, sourcetype, csdnum, protocol, roaming_protocol, carrier_enabled, bearer, spn, imsi)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

.field public static final CARRIER_MVNO_OMACP_GEMINI_INSERT_SQL:Ljava/lang/String; = "INSERT INTO carriers_gemini (name, numeric, mcc, mnc, apn, user, server, password, proxy, port, mmsproxy, mmsport, mmsc, authtype, type, current, sourcetype, csdnum, protocol, roaming_protocol, omacpid, napid, proxyid, carrier_enabled, bearer, spn, imsi)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

.field public static final CARRIER_MVNO_OMACP_INSERT_SQL:Ljava/lang/String; = "INSERT INTO carriers (name, numeric, mcc, mnc, apn, user, server, password, proxy, port, mmsproxy, mmsport, mmsc, authtype, type, current, sourcetype, csdnum, protocol, roaming_protocol, omacpid, napid, proxyid, carrier_enabled, bearer, spn, imsi)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

.field public static final CARRIER_OMACP_GEMINI_INSERT_SQL:Ljava/lang/String; = "INSERT INTO carriers_gemini (name, numeric, mcc, mnc, apn, user, server, password, proxy, port, mmsproxy, mmsport, mmsc, authtype, type, current, sourcetype, csdnum, protocol, roaming_protocol, omacpid, napid, proxyid, carrier_enabled, bearer)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

.field public static final CARRIER_OMACP_INSERT_SQL:Ljava/lang/String; = "INSERT INTO carriers (name, numeric, mcc, mnc, apn, user, server, password, proxy, port, mmsproxy, mmsport, mmsc, authtype, type, current, sourcetype, csdnum, protocol, roaming_protocol, omacpid, napid, proxyid, carrier_enabled, bearer)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

.field public static final CSD_NUM:I = 0x12

.field public static final CURRENT:I = 0x10

.field public static final IMSI:I = 0x18

.field public static final MCC:I = 0x3

.field public static final MMSC:I = 0xd

.field public static final MMSPORT:I = 0xc

.field public static final MMSPROXY:I = 0xb

.field public static final MNC:I = 0x4

.field public static final NAME:I = 0x1

.field public static final NUMERIC:I = 0x2

.field public static final OMACP_BEARER:I = 0x19

.field public static final OMACP_CARRIER_ENABLED:I = 0x18

.field public static final OMACP_IMSI:I = 0x1b

.field public static final OMACP_NAPID:I = 0x16

.field public static final OMACP_OMACPID:I = 0x15

.field public static final OMACP_PROXYID:I = 0x17

.field public static final OMACP_SPN:I = 0x1a

.field public static final PASSWORD:I = 0x8

.field public static final PORT:I = 0xa

.field public static final PROTOCOL:I = 0x13

.field public static final PROXY:I = 0x9

.field public static final ROAMING_PROTOCOL:I = 0x14

.field public static final SERVER:I = 0x7

.field public static final SOURCE_TYPE:I = 0x11

.field public static final SPN:I = 0x17

.field public static final TYPE:I = 0xf

.field public static final USER:I = 0x6
