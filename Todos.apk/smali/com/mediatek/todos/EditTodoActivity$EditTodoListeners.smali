.class Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;
.super Ljava/lang/Object;
.source "EditTodoActivity.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/EditTodoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTodoListeners"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/todos/EditTodoActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/todos/EditTodoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/todos/EditTodoActivity;Lcom/mediatek/todos/EditTodoActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/todos/EditTodoActivity;
    .param p2    # Lcom/mediatek/todos/EditTodoActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;-><init>(Lcom/mediatek/todos/EditTodoActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v0, "EditTodoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ingore the click. View = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-virtual {v0}, Lcom/mediatek/todos/EditTodoActivity;->onClickNewTodo()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-virtual {v0}, Lcom/mediatek/todos/EditTodoActivity;->onClickEditText()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-virtual {v0}, Lcom/mediatek/todos/EditTodoActivity;->onClickSetDueDate()V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-virtual {v0}, Lcom/mediatek/todos/EditTodoActivity;->onClickRemoveDueDate()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0a0000 -> :sswitch_1
        0x7f0a0001 -> :sswitch_1
        0x7f0a0002 -> :sswitch_2
        0x7f0a0005 -> :sswitch_3
        0x7f0a0010 -> :sswitch_0
    .end sparse-switch
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 11
    .param p1    # Landroid/widget/DatePicker;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/16 v1, 0x7f4

    if-gt p2, v1, :cond_0

    const/16 v1, 0x7b2

    if-ge p2, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    const v2, 0x7f070007

    invoke-static {v1, v2}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    const-string v1, "EditTodoActivity"

    const-string v2, "year < 1970 or year > 2037.Cannot be set."

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    const/16 v1, 0x3b

    const/16 v2, 0x3b

    const/16 v3, 0x17

    move v4, p4

    move v5, p3

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v9

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    invoke-static {v0, v7}, Lcom/mediatek/todos/Utils;->dateCompare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v1

    if-gez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    const v2, 0x7f070006

    invoke-static {v1, v2}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    const-string v1, "EditTodoActivity"

    const-string v2, "Date can\'t before today"

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 v7, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    const/4 v2, 0x1

    invoke-static {v1, v9, v10, v2}, Lcom/mediatek/todos/Utils;->getDateText(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v8

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-static {v1}, Lcom/mediatek/todos/EditTodoActivity;->access$100(Lcom/mediatek/todos/EditTodoActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-static {v1}, Lcom/mediatek/todos/EditTodoActivity;->access$200(Lcom/mediatek/todos/EditTodoActivity;)Lcom/mediatek/todos/TodoInfo;

    move-result-object v1

    invoke-virtual {v1, v9, v10}, Lcom/mediatek/todos/TodoInfo;->setDueDay(J)V

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-static {v1}, Lcom/mediatek/todos/EditTodoActivity;->access$300(Lcom/mediatek/todos/EditTodoActivity;)Landroid/widget/ImageButton;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-static {v1}, Lcom/mediatek/todos/EditTodoActivity;->access$200(Lcom/mediatek/todos/EditTodoActivity;)Lcom/mediatek/todos/TodoInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/todos/TodoInfo;->isExpire()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-static {v1}, Lcom/mediatek/todos/EditTodoActivity;->access$400(Lcom/mediatek/todos/EditTodoActivity;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02000a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;->this$0:Lcom/mediatek/todos/EditTodoActivity;

    invoke-static {v1}, Lcom/mediatek/todos/EditTodoActivity;->access$400(Lcom/mediatek/todos/EditTodoActivity;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02001f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
