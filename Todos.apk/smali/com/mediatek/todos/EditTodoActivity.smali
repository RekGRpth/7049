.class public Lcom/mediatek/todos/EditTodoActivity;
.super Landroid/app/Activity;
.source "EditTodoActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/mediatek/todos/QueryListener;
.implements Lcom/mediatek/todos/TimeChangeReceiver$TimeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;
    }
.end annotation


# static fields
.field private static final DIALOG_BACK_MAIN_PAGE:I = 0x2

.field private static final DIALOG_CANCEL_EDIT:I = 0x3

.field private static final DIALOG_DELETE_ITEMS:I = 0x1

.field private static final MAX_DAY:I = 0x1f

.field private static final MAX_MONTH:I = 0xb

.field private static final MAX_YEAR:I = 0x7f4

.field private static final MIN_YEAR:I = 0x7b2

.field private static final STATE_ADD_NEW:I = 0x0

.field private static final STATE_EDIT_TODO:I = 0x2

.field private static final STATE_NULL:I = -0x1

.field private static final STATE_SHOW_DETAILS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "EditTodoActivity"

.field private static final TOAST_LEFT_OFFSET:I = 0x28


# instance fields
.field private mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

.field private mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

.field private mDataOriginal:Lcom/mediatek/todos/TodoInfo;

.field private mDateIcon:Landroid/widget/ImageView;

.field private mDateText:Landroid/widget/TextView;

.field private mImgBtnDueDateRemove:Landroid/widget/ImageButton;

.field private mImgBtnNewTodo:Landroid/widget/ImageButton;

.field private mListeners:Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

.field private mMaxTime:J

.field private mOperatorCode:I

.field private mSetDueDate:Landroid/widget/LinearLayout;

.field private mState:I

.field private mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

.field private mTodoDescription:Lcom/mediatek/todos/TodoEditText;

.field private mTodoTitle:Lcom/mediatek/todos/TodoEditText;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

    invoke-direct {v0, p0, v2}, Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;-><init>(Lcom/mediatek/todos/EditTodoActivity;Lcom/mediatek/todos/EditTodoActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mListeners:Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    new-instance v0, Lcom/mediatek/todos/TodoInfo;

    invoke-direct {v0}, Lcom/mediatek/todos/TodoInfo;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    new-instance v0, Lcom/mediatek/todos/TodoInfo;

    invoke-direct {v0}, Lcom/mediatek/todos/TodoInfo;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mMaxTime:J

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/todos/EditTodoActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/mediatek/todos/EditTodoActivity;

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/todos/EditTodoActivity;)Lcom/mediatek/todos/TodoInfo;
    .locals 1
    .param p0    # Lcom/mediatek/todos/EditTodoActivity;

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/todos/EditTodoActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/mediatek/todos/EditTodoActivity;

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnDueDateRemove:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/todos/EditTodoActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/mediatek/todos/EditTodoActivity;

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/todos/EditTodoActivity;I)I
    .locals 0
    .param p0    # Lcom/mediatek/todos/EditTodoActivity;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/todos/EditTodoActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/todos/EditTodoActivity;

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->checkAndSaveQuit()V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/todos/EditTodoActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/todos/EditTodoActivity;

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    return-void
.end method

.method private checkAndSaveQuit()V
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->hideIMEFromWindow()V

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    packed-switch v2, :pswitch_data_0

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    :goto_0
    return-void

    :pswitch_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v2, v1}, Lcom/mediatek/todos/TodoInfo;->setTitle(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v2, v0}, Lcom/mediatek/todos/TodoInfo;->setDescription(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p0, v2}, Lcom/mediatek/todos/EditTodoActivity;->startInsert(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0

    :pswitch_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070022

    invoke-static {v2, v3}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    const/4 v2, 0x4

    iput v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p0, v2}, Lcom/mediatek/todos/EditTodoActivity;->startDelete(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v2, v1}, Lcom/mediatek/todos/TodoInfo;->setTitle(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v2, v0}, Lcom/mediatek/todos/TodoInfo;->setDescription(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    iget-object v3, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v2, v3}, Lcom/mediatek/todos/TodoInfo;->equals(Lcom/mediatek/todos/TodoInfo;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p0, v2}, Lcom/mediatek/todos/EditTodoActivity;->startUpdate(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p0, v2}, Lcom/mediatek/todos/EditTodoActivity;->startDelete(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private clearData()V
    .locals 1

    new-instance v0, Lcom/mediatek/todos/TodoInfo;

    invoke-direct {v0}, Lcom/mediatek/todos/TodoInfo;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    new-instance v0, Lcom/mediatek/todos/TodoInfo;

    invoke-direct {v0}, Lcom/mediatek/todos/TodoInfo;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    return-void
.end method

.method private configureActionBar()V
    .locals 5

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030005

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const v2, 0x7f0a0010

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnNewTodo:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnNewTodo:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnNewTodo:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/mediatek/todos/EditTodoActivity;->mListeners:Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private doLongClickNewTodo()V
    .locals 8

    const/4 v7, 0x0

    const v5, 0x7f0a000f

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v5

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070016

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    const/4 v5, 0x2

    new-array v1, v5, [I

    iget-object v5, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnNewTodo:Landroid/widget/ImageButton;

    invoke-virtual {v5, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    const/16 v5, 0x35

    aget v6, v1, v7

    int-to-float v6, v6

    sub-float v6, v2, v6

    const/high16 v7, 0x42200000

    sub-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v7

    invoke-virtual {v3, v5, v6, v7}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private getExpireCode(Lcom/mediatek/todos/TodoInfo;)I
    .locals 4
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->isExpire()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0xb

    goto :goto_0
.end method

.method private hideIMEFromWindow()V
    .locals 3

    const-string v1, "EditTodoActivity"

    const-string v2, "call InputMethodManager.hideSoftInputFromWindow()"

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-nez v0, :cond_0

    const-string v1, "EditTodoActivity"

    const-string v2, "get system service IMM failed"

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method private initViews()V
    .locals 4

    const/high16 v2, 0x7f0a0000

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/mediatek/todos/TodoEditText;

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    iget-object v3, p0, Lcom/mediatek/todos/EditTodoActivity;->mListeners:Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v2, v1}, Lcom/mediatek/todos/TodoEditText;->setMaxLength(I)V

    const v2, 0x7f0a0001

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/mediatek/todos/TodoEditText;

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    iget-object v3, p0, Lcom/mediatek/todos/EditTodoActivity;->mListeners:Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v2, v0}, Lcom/mediatek/todos/TodoEditText;->setMaxLength(I)V

    const v2, 0x7f0a0002

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mSetDueDate:Landroid/widget/LinearLayout;

    const v2, 0x7f0a0003

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    const v2, 0x7f0a0004

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateText:Landroid/widget/TextView;

    const v2, 0x7f0a0005

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnDueDateRemove:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mSetDueDate:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/mediatek/todos/EditTodoActivity;->mListeners:Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnDueDateRemove:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/mediatek/todos/EditTodoActivity;->mListeners:Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private isDone()Z
    .locals 2

    const-string v0, "2"

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private quit(I)V
    .locals 4
    .param p1    # I

    const-string v1, "EditTodoActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "quit() operatorCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    const-string v1, "data_passed"

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    const-string v1, "data_passed"

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    const-string v1, "data_passed"

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private quiteWithOutSave()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/todos/EditTodoActivity;->quit(I)V

    return-void
.end method

.method private showDatePicker()V
    .locals 10

    const-wide/16 v8, 0x0

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v1

    cmp-long v1, v8, v1

    if-nez v1, :cond_0

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    :goto_0
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mListeners:Lcom/mediatek/todos/EditTodoActivity$EditTodoListeners;

    iget v3, v7, Landroid/text/format/Time;->year:I

    iget v4, v7, Landroid/text/format/Time;->month:I

    iget v5, v7, Landroid/text/format/Time;->monthDay:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v6

    invoke-virtual {v6, v8, v9}, Landroid/widget/DatePicker;->setMinDate(J)V

    iget-wide v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mMaxTime:J

    invoke-virtual {v6, v1, v2}, Landroid/widget/DatePicker;->setMaxDate(J)V

    iget v1, v7, Landroid/text/format/Time;->year:I

    iget v2, v7, Landroid/text/format/Time;->month:I

    iget v3, v7, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v6, v1, v2, v3}, Landroid/widget/DatePicker;->updateDate(III)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v1

    invoke-virtual {v7, v1, v2}, Landroid/text/format/Time;->set(J)V

    goto :goto_0
.end method


# virtual methods
.method public changeToState(I)V
    .locals 12
    .param p1    # I

    const/4 v11, 0x2

    const/4 v10, -0x1

    const v9, 0x7f02001f

    const/4 v6, 0x0

    const/4 v5, 0x1

    iput p1, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    iput v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    iget v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v7, :pswitch_data_0

    const-string v5, "EditTodoActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "changeToState : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", no this state."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void

    :pswitch_0
    const/4 v3, 0x0

    iget v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    if-nez v7, :cond_0

    const/4 v7, 0x3

    iput v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    const v3, 0x7f070016

    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/app/ActionBar;->setTitle(I)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v5}, Landroid/view/View;->setClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    iget-object v8, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v8}, Lcom/mediatek/todos/TodoInfo;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    const v8, 0x7f07001b

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setHint(I)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v5}, Landroid/view/View;->setClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    iget-object v8, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v8}, Lcom/mediatek/todos/TodoInfo;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    const v8, 0x7f07001c

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setHint(I)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mSetDueDate:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5}, Landroid/view/View;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mSetDueDate:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5}, Landroid/view/View;->setClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-direct {p0, v7}, Lcom/mediatek/todos/EditTodoActivity;->getExpireCode(Lcom/mediatek/todos/TodoInfo;)I

    move-result v2

    if-ne v2, v10, :cond_1

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnDueDateRemove:Landroid/widget/ImageButton;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v7}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v7

    invoke-static {p0, v7, v8, v5}, Lcom/mediatek/todos/Utils;->getDateText(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_0
    iput v11, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    const v3, 0x7f070017

    goto/16 :goto_1

    :cond_1
    const/16 v7, 0xb

    if-ne v2, v7, :cond_2

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnDueDateRemove:Landroid/widget/ImageButton;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    const v8, 0x7f02000a

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnDueDateRemove:Landroid/widget/ImageButton;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->isDone()Z

    move-result v7

    if-nez v7, :cond_4

    move v4, v5

    :goto_3
    if-eqz v4, :cond_5

    const v0, 0x7f070018

    :goto_4
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/ActionBar;->setTitle(I)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v4}, Landroid/view/View;->setClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v6}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7}, Landroid/view/View;->clearFocus()V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    iget-object v8, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v8}, Lcom/mediatek/todos/TodoInfo;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v4}, Landroid/view/View;->setClickable(Z)V

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v7, v6}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v6}, Landroid/view/View;->clearFocus()V

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v7}, Lcom/mediatek/todos/TodoInfo;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mSetDueDate:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mSetDueDate:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v4}, Landroid/view/View;->setClickable(Z)V

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnDueDateRemove:Landroid/widget/ImageButton;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v4, :cond_7

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-direct {p0, v6}, Lcom/mediatek/todos/EditTodoActivity;->getExpireCode(Lcom/mediatek/todos/TodoInfo;)I

    move-result v1

    if-eq v1, v10, :cond_3

    const/16 v6, 0xb

    if-ne v1, v6, :cond_6

    :cond_3
    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_5
    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v7}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v7

    invoke-static {p0, v7, v8, v5}, Lcom/mediatek/todos/Utils;->getDateText(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    move v4, v6

    goto/16 :goto_3

    :cond_5
    const v0, 0x7f070019

    goto/16 :goto_4

    :cond_6
    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    const v7, 0x7f02000a

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_5

    :cond_7
    iget-object v5, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    const v6, 0x7f020007

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v6}, Lcom/mediatek/todos/TodoInfo;->getCompleteTime()J

    move-result-wide v6

    invoke-static {p0, v6, v7, v11}, Lcom/mediatek/todos/Utils;->getDateText(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->checkAndSaveQuit()V

    return-void
.end method

.method public onClickBtnCancel()V
    .locals 5

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->hideIMEFromWindow()V

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v4, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    goto :goto_0

    :cond_0
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4}, Lcom/mediatek/todos/TodoInfo;->getTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4}, Lcom/mediatek/todos/TodoInfo;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4}, Lcom/mediatek/todos/TodoInfo;->clear()V

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/mediatek/todos/EditTodoActivity;->changeToState(I)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onClickChangeTodoState()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1, v2}, Lcom/mediatek/todos/TodoInfo;->copy(Lcom/mediatek/todos/TodoInfo;)V

    const-string v1, "2"

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "1"

    :goto_0
    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1, v0}, Lcom/mediatek/todos/TodoInfo;->updateStatus(Ljava/lang/String;)V

    const/4 v1, 0x2

    iput v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->checkAndSaveQuit()V

    return-void

    :cond_0
    const-string v0, "2"

    goto :goto_0
.end method

.method public onClickEditText()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "2"

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0, v1}, Lcom/mediatek/todos/TodoInfo;->copy(Lcom/mediatek/todos/TodoInfo;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/mediatek/todos/EditTodoActivity;->changeToState(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public onClickNewTodo()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/todos/EditTodoActivity;->changeToState(I)V

    return-void
.end method

.method public onClickRemoveDueDate()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/todos/TodoInfo;->setDueDay(J)V

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateText:Landroid/widget/TextView;

    const v1, 0x7f070005

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    const v1, 0x7f02001f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnDueDateRemove:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public onClickSetDueDate()V
    .locals 2

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->hideIMEFromWindow()V

    iget v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0, v1}, Lcom/mediatek/todos/TodoInfo;->copy(Lcom/mediatek/todos/TodoInfo;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/mediatek/todos/EditTodoActivity;->changeToState(I)V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->showDatePicker()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->initViews()V

    invoke-static {p0}, Lcom/mediatek/todos/TimeChangeReceiver;->registTimeChangeReceiver(Landroid/content/Context;)Lcom/mediatek/todos/TimeChangeReceiver;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    invoke-virtual {v1, p0}, Lcom/mediatek/todos/TimeChangeReceiver;->addDateChangeListener(Lcom/mediatek/todos/TimeChangeReceiver$TimeChangeListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/todos/TodoAsyncQuery;->getInstatnce(Landroid/content/Context;)Lcom/mediatek/todos/TodoAsyncQuery;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    const/16 v1, 0x1f

    const/16 v2, 0xb

    const/16 v3, 0x7f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/format/Time;->set(III)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mMaxTime:J

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->configureActionBar()V

    const-string v1, "EditTodoActivity"

    const-string v2, "onCreate() finshed."

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .param p1    # I

    const/4 v7, 0x0

    const v6, 0x7f07000d

    const v5, 0x104000a

    const v4, 0x1010355

    const/4 v3, -0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v1, "EditTodoActivity"

    const-string v2, "onCreateDialog,the id is not match!"

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f07000c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07000e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {p0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/mediatek/todos/EditTodoActivity$1;

    invoke-direct {v2, p0}, Lcom/mediatek/todos/EditTodoActivity$1;-><init>(Lcom/mediatek/todos/EditTodoActivity;)V

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    move-object v1, v0

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070013

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070014

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {p0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/mediatek/todos/EditTodoActivity$2;

    invoke-direct {v2, p0}, Lcom/mediatek/todos/EditTodoActivity$2;-><init>(Lcom/mediatek/todos/EditTodoActivity;)V

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    move-object v1, v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070013

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070014

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {p0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/mediatek/todos/EditTodoActivity$3;

    invoke-direct {v2, p0}, Lcom/mediatek/todos/EditTodoActivity$3;-><init>(Lcom/mediatek/todos/EditTodoActivity;)V

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    move-object v1, v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :pswitch_0
    const v1, 0x7f090001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    :pswitch_1
    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onDateChange()V
    .locals 5

    const v4, 0x7f02001f

    const v3, 0x7f02000a

    const-string v0, "EditTodoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDateChnge isExpire:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodoInfo;->isExpire()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "2"

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodoInfo;->isExpire()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v0, "2"

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodoInfo;->isExpire()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDeleteComplete(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "EditTodoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDeleteComplete(). ReQuery the insert TodoInfo. result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-gtz p2, :cond_0

    const v0, 0x7f070012

    invoke-static {p0, v0}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/mediatek/todos/EditTodoActivity;->quit(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    invoke-virtual {v0}, Lcom/mediatek/todos/TimeChangeReceiver;->clearChangeListener()V

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mTimeChangeReceiver:Lcom/mediatek/todos/TimeChangeReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onInsertComplete(ILandroid/net/Uri;)V
    .locals 8
    .param p1    # I
    .param p2    # Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v0, "EditTodoActivity"

    const-string v1, "onInsertComplete(). reQuery the insert TodoInfo."

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    move v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->doLongClickNewTodo()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0010
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v0, "EditTodoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected options item selected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v3

    :sswitch_0
    invoke-virtual {p0}, Lcom/mediatek/todos/EditTodoActivity;->onClickBtnCancel()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->checkAndSaveQuit()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/mediatek/todos/EditTodoActivity;->onClickChangeTodoState()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v3}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f0a0012 -> :sswitch_2
        0x7f0a0013 -> :sswitch_3
        0x7f0a0014 -> :sswitch_0
        0x7f0a0015 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 6

    const-string v4, "EditTodoActivity"

    const-string v5, "onPause() save data."

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v4, :pswitch_data_0

    const-string v4, "EditTodoActivity"

    const-string v5, "cann\'t switch mState..."

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    :pswitch_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void

    :pswitch_1
    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v3}, Lcom/mediatek/todos/TodoInfo;->setTitle(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v0}, Lcom/mediatek/todos/TodoInfo;->setDescription(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v2}, Lcom/mediatek/todos/TodoInfo;->setTitle(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v1}, Lcom/mediatek/todos/TodoInfo;->setDescription(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    iget v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnNewTodo:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mImgBtnNewTodo:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const v1, 0x7f0a0012

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->isDone()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f020023

    :goto_1
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->isDone()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f070020

    :goto_2
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :cond_0
    const v1, 0x7f020024

    goto :goto_1

    :cond_1
    const v1, 0x7f070021

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onQueryComplete(ILandroid/database/Cursor;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/database/Cursor;

    const-string v0, "EditTodoActivity"

    const-string v1, "onQueryComplete()."

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const v0, 0x7f070012

    invoke-static {p0, v0}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    const-string v0, "EditTodoActivity"

    const-string v1, "onQueryComplete,cursor is empty!"

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Lcom/mediatek/todos/TodoInfo;->makeTodoInfoFromCursor(Landroid/database/Cursor;)Lcom/mediatek/todos/TodoInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/mediatek/todos/EditTodoActivity;->quit(I)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v4, "state"

    const/4 v5, -0x1

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    iget v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v4, :pswitch_data_0

    const-string v4, "EditTodoActivity"

    const-string v5, "cann\'t switch mState..."

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v4, "EditTodoActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onRestoreInstanceState()-mState="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const-string v4, "modify"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/mediatek/todos/TodoInfo;

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v2}, Lcom/mediatek/todos/TodoInfo;->copy(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0

    :pswitch_1
    const-string v4, "original"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/mediatek/todos/TodoInfo;

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v0}, Lcom/mediatek/todos/TodoInfo;->copy(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0

    :pswitch_2
    const-string v4, "original"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    const-string v4, "modify"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TodoInfo;

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v3}, Lcom/mediatek/todos/TodoInfo;->copy(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v1}, Lcom/mediatek/todos/TodoInfo;->copy(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v2, "EditTodoActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onResume mState="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "data_passed"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/todos/TodoInfo;

    iget-object v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v2, v0}, Lcom/mediatek/todos/TodoInfo;->copy(Lcom/mediatek/todos/TodoInfo;)V

    const/4 v2, 0x1

    iput v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    :cond_0
    :goto_0
    iget v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    invoke-virtual {p0, v2}, Lcom/mediatek/todos/EditTodoActivity;->changeToState(I)V

    const-string v2, "EditTodoActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onResume() finshed. mState="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const-string v4, "EditTodoActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onSaveInstanceState()-mState="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "state"

    iget v5, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v4, :pswitch_data_0

    const-string v4, "EditTodoActivity"

    const-string v5, "cann\'t switch mState..."

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void

    :pswitch_0
    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v3}, Lcom/mediatek/todos/TodoInfo;->setTitle(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v0}, Lcom/mediatek/todos/TodoInfo;->setDescription(Ljava/lang/String;)V

    const-string v4, "modify"

    iget-object v5, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    :pswitch_1
    const-string v4, "original"

    iget-object v5, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoTitle:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mTodoDescription:Lcom/mediatek/todos/TodoEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v2}, Lcom/mediatek/todos/TodoInfo;->setTitle(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v4, v1}, Lcom/mediatek/todos/TodoInfo;->setDescription(Ljava/lang/String;)V

    const-string v4, "original"

    iget-object v5, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v4, "modify"

    iget-object v5, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTimePick()V
    .locals 4

    const v3, 0x7f02001f

    const v2, 0x7f02000a

    iget v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "2"

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataAfterModify:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodoInfo;->isExpire()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v0, "2"

    iget-object v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDataOriginal:Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v0}, Lcom/mediatek/todos/TodoInfo;->isExpire()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mDateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onUpdateComplete(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "EditTodoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateComplete(). result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-gtz p2, :cond_0

    const-string v0, "EditTodoActivity"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f070012

    invoke-static {p0, v0}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    invoke-direct {p0}, Lcom/mediatek/todos/EditTodoActivity;->quiteWithOutSave()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/todos/EditTodoActivity;->quit(I)V

    goto :goto_0
.end method

.method public startDelete(Lcom/mediatek/todos/TodoInfo;)V
    .locals 2
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const-string v0, "EditTodoActivity"

    const-string v1, "startDelete()."

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    iget v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/todos/Utils;->writeAdapterDataToDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V

    return-void
.end method

.method public startInsert(Lcom/mediatek/todos/TodoInfo;)V
    .locals 2
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const-string v0, "EditTodoActivity"

    const-string v1, "startInsert()."

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    iget v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/todos/Utils;->writeAdapterDataToDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V

    return-void
.end method

.method public startQuery(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public startUpdate(Lcom/mediatek/todos/TodoInfo;)V
    .locals 2
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const-string v0, "EditTodoActivity"

    const-string v1, "startUpdate()"

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/EditTodoActivity;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    iget v1, p0, Lcom/mediatek/todos/EditTodoActivity;->mOperatorCode:I

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/todos/Utils;->writeAdapterDataToDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V

    return-void
.end method
