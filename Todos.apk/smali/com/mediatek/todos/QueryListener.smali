.class public interface abstract Lcom/mediatek/todos/QueryListener;
.super Ljava/lang/Object;
.source "QueryListener.java"


# virtual methods
.method public abstract onDeleteComplete(II)V
.end method

.method public abstract onInsertComplete(ILandroid/net/Uri;)V
.end method

.method public abstract onQueryComplete(ILandroid/database/Cursor;)V
.end method

.method public abstract onUpdateComplete(II)V
.end method

.method public abstract startDelete(Lcom/mediatek/todos/TodoInfo;)V
.end method

.method public abstract startInsert(Lcom/mediatek/todos/TodoInfo;)V
.end method

.method public abstract startQuery(Ljava/lang/String;)V
.end method

.method public abstract startUpdate(Lcom/mediatek/todos/TodoInfo;)V
.end method
