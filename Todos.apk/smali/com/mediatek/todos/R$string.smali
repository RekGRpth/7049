.class public final Lcom/mediatek/todos/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_new_todo:I = 0x7f070016

.field public static final app_header:I = 0x7f070001

.field public static final app_name:I = 0x7f070000

.field public static final back_message:I = 0x7f070014

.field public static final back_title:I = 0x7f070013

.field public static final btn_cancel:I = 0x7f070009

.field public static final btn_done:I = 0x7f070008

.field public static final cancel:I = 0x7f07000d

.field public static final delete:I = 0x7f07000c

.field public static final delete_item:I = 0x7f07000e

.field public static final delete_selected_items:I = 0x7f07000f

.field public static final deselect_all:I = 0x7f07001f

.field public static final done_detail:I = 0x7f070019

.field public static final done_time_format:I = 0x7f070010

.field public static final dones_empty_info:I = 0x7f07000b

.field public static final dones_title:I = 0x7f070003

.field public static final due_day_time_format:I = 0x7f070011

.field public static final edit_todo:I = 0x7f070017

.field public static final hint_details:I = 0x7f07001c

.field public static final hint_title:I = 0x7f07001b

.field public static final mark_done:I = 0x7f070020

.field public static final mark_todo:I = 0x7f070021

.field public static final no_expire_date:I = 0x7f070005

.field public static final ok:I = 0x7f070015

.field public static final operator_failed:I = 0x7f070012

.field public static final prompt_date_before_today:I = 0x7f070006

.field public static final select_all:I = 0x7f07001e

.field public static final separate:I = 0x7f07001d

.field public static final text_full:I = 0x7f07001a

.field public static final todo_detail:I = 0x7f070018

.field public static final todo_details_title:I = 0x7f070004

.field public static final todo_discarded:I = 0x7f070022

.field public static final todos_empty_info:I = 0x7f07000a

.field public static final todos_title:I = 0x7f070002

.field public static final valid_date_range:I = 0x7f070007


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
