.class final Lcom/mediatek/todos/TodoEditText$1;
.super Landroid/text/InputFilter$LengthFilter;
.source "TodoEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/todos/TodoEditText;->registerLengthFilter(Landroid/content/Context;Landroid/widget/EditText;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$maxLength:I


# direct methods
.method constructor <init>(IILandroid/content/Context;)V
    .locals 0
    .param p1    # I

    iput p2, p0, Lcom/mediatek/todos/TodoEditText$1;->val$maxLength:I

    iput-object p3, p0, Lcom/mediatek/todos/TodoEditText$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/text/Spanned;
    .param p5    # I
    .param p6    # I

    invoke-interface {p4, p5, p6}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/todos/TodoEditText;->access$000(Ljava/lang/String;)I

    move-result v4

    invoke-static {v1}, Lcom/mediatek/todos/TodoEditText;->access$000(Ljava/lang/String;)I

    move-result v5

    sub-int v0, v4, v5

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/todos/TodoEditText;->access$000(Ljava/lang/String;)I

    move-result v3

    add-int v4, v0, v3

    iget v5, p0, Lcom/mediatek/todos/TodoEditText$1;->val$maxLength:I

    if-le v4, v5, :cond_0

    iget-object v4, p0, Lcom/mediatek/todos/TodoEditText$1;->val$context:Landroid/content/Context;

    const v5, 0x7f07001a

    invoke-static {v4, v5}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    iget v4, p0, Lcom/mediatek/todos/TodoEditText$1;->val$maxLength:I

    if-lt v0, v4, :cond_1

    const-string p1, ""

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/todos/TodoEditText$1;->val$maxLength:I

    sub-int/2addr v5, v0

    invoke-static {v4, v5}, Lcom/mediatek/todos/TodoEditText;->access$100(Ljava/lang/String;I)I

    move-result v2

    invoke-interface {p1, p2, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method
