.class public Lcom/mediatek/todos/TodoEditText;
.super Landroid/widget/EditText;
.source "TodoEditText.java"


# static fields
.field private static final LINE_UNDER_SPACING:I = 0x5

.field private static final TAG:Ljava/lang/String; = "TodoEditText"


# instance fields
.field private mDrawLines:Z

.field private mLineBottomPadding:I

.field private mLineSpaceHeight:I

.field private mMinLines:I

.field private mPaint:Landroid/graphics/Paint;

.field private mRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/mediatek/todos/TodoEditText;->mDrawLines:Z

    iput-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mRect:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mPaint:Landroid/graphics/Paint;

    const/4 v0, 0x4

    iput v0, p0, Lcom/mediatek/todos/TodoEditText;->mMinLines:I

    iput v1, p0, Lcom/mediatek/todos/TodoEditText;->mLineSpaceHeight:I

    iput v1, p0, Lcom/mediatek/todos/TodoEditText;->mLineBottomPadding:I

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/mediatek/todos/TodoEditText;->getCharacterNum(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Ljava/lang/String;I)I
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # I

    invoke-static {p0, p1}, Lcom/mediatek/todos/TodoEditText;->getKeepLength(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private drawTextLine(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    move-result v7

    const/4 v6, 0x0

    const/4 v9, 0x0

    :goto_0
    if-ge v9, v7, :cond_0

    iget-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v9, v0}, Landroid/widget/TextView;->getLineBounds(ILandroid/graphics/Rect;)I

    move-result v0

    add-int/lit8 v6, v0, 0x5

    iget-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    int-to-float v2, v6

    iget-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/mediatek/todos/TodoEditText;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    iget v0, p0, Lcom/mediatek/todos/TodoEditText;->mMinLines:I

    if-ge v7, v0, :cond_1

    iget v0, p0, Lcom/mediatek/todos/TodoEditText;->mLineSpaceHeight:I

    add-int/2addr v6, v0

    iget v0, p0, Lcom/mediatek/todos/TodoEditText;->mLineBottomPadding:I

    add-int/2addr v0, v6

    if-gt v0, v8, :cond_1

    iget-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    int-to-float v2, v6

    iget-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/mediatek/todos/TodoEditText;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private static getCharacterNum(Ljava/lang/String;)I
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p0}, Lcom/mediatek/todos/TodoEditText;->getChineseNum(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private static getChineseNum(Ljava/lang/String;)I
    .locals 5
    .param p0    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget-char v3, v0, v1

    int-to-byte v3, v3

    int-to-char v3, v3

    aget-char v4, v0, v1

    if-eq v3, v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method private static getKeepLength(Ljava/lang/String;I)I
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # I

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    aget-char v3, v0, v1

    int-to-byte v3, v3

    int-to-char v3, v3

    aget-char v4, v0, v1

    if-eq v3, v4, :cond_3

    add-int/lit8 v3, v2, 0x2

    if-lt v3, p1, :cond_1

    add-int/lit8 v3, v2, 0x2

    if-ne v3, p1, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    :goto_1
    return v1

    :cond_1
    add-int/lit8 v2, v2, 0x2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    if-lt v2, p1, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static registerLengthFilter(Landroid/content/Context;Landroid/widget/EditText;I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/widget/EditText;
    .param p2    # I

    const/4 v1, 0x1

    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Lcom/mediatek/todos/TodoEditText$1;

    invoke-direct {v2, p2, p2, p0}, Lcom/mediatek/todos/TodoEditText$1;-><init>(IILandroid/content/Context;)V

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/mediatek/todos/TodoEditText;->mDrawLines:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/todos/TodoEditText;->drawTextLine(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v1, "TodoEditText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cann\'t switch action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    :pswitch_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/EditTodoActivity;

    invoke-virtual {v1}, Lcom/mediatek/todos/EditTodoActivity;->onClickEditText()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setDrawLines(Z)V
    .locals 3
    .param p1    # Z

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/mediatek/todos/TodoEditText;->mDrawLines:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/mediatek/todos/TodoEditText;->mDrawLines:Z

    iget-boolean v0, p0, Lcom/mediatek/todos/TodoEditText;->mDrawLines:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/mediatek/todos/TodoEditText;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/todos/TodoEditText;->mMinLines:I

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/mediatek/todos/TodoEditText;->mLineSpaceHeight:I

    iget v0, p0, Lcom/mediatek/todos/TodoEditText;->mLineSpaceHeight:I

    shr-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/mediatek/todos/TodoEditText;->mLineBottomPadding:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v1, p0, Lcom/mediatek/todos/TodoEditText;->mRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/mediatek/todos/TodoEditText;->mPaint:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method public setMaxLength(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/mediatek/todos/TodoEditText;->registerLengthFilter(Landroid/content/Context;Landroid/widget/EditText;I)V

    return-void
.end method
