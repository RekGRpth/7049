.class public Lcom/mediatek/todos/TodoInfo;
.super Ljava/lang/Object;
.source "TodoInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final STATUS_DONE:Ljava/lang/String; = "2"

.field public static final STATUS_TODO:Ljava/lang/String; = "1"

.field private static final TAG:Ljava/lang/String; = "TodoInfo"


# instance fields
.field private mCompleteTime:Ljava/lang/String;

.field private mCreateTime:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mDueDate:Ljava/lang/String;

.field private mId:Ljava/lang/String;

.field private mStatus:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    const-string v0, "1"

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    const-string v0, "0"

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mCreateTime:Ljava/lang/String;

    const-string v0, "0"

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    return-void
.end method

.method public static makeTodoInfoFromCursor(Landroid/database/Cursor;)Lcom/mediatek/todos/TodoInfo;
    .locals 9
    .param p0    # Landroid/database/Cursor;

    new-instance v7, Lcom/mediatek/todos/TodoInfo;

    invoke-direct {v7}, Lcom/mediatek/todos/TodoInfo;-><init>()V

    const-string v8, "_id"

    invoke-static {v8, p0}, Lcom/mediatek/todos/Utils;->getColumnByName(Ljava/lang/String;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    const-string v8, "title"

    invoke-static {v8, p0}, Lcom/mediatek/todos/Utils;->getColumnByName(Ljava/lang/String;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "description"

    invoke-static {v8, p0}, Lcom/mediatek/todos/Utils;->getColumnByName(Ljava/lang/String;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    const-string v8, "status"

    invoke-static {v8, p0}, Lcom/mediatek/todos/Utils;->getColumnByName(Ljava/lang/String;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "dtend"

    invoke-static {v8, p0}, Lcom/mediatek/todos/Utils;->getColumnByName(Ljava/lang/String;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "create_time"

    invoke-static {v8, p0}, Lcom/mediatek/todos/Utils;->getColumnByName(Ljava/lang/String;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "complete_time"

    invoke-static {v8, p0}, Lcom/mediatek/todos/Utils;->getColumnByName(Ljava/lang/String;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v4, v7, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    if-nez v6, :cond_0

    const-string v6, ""

    :cond_0
    iput-object v6, v7, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v2, ""

    :cond_1
    iput-object v2, v7, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    iput-object v5, v7, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    iput-object v3, v7, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    iput-object v1, v7, Lcom/mediatek/todos/TodoInfo;->mCreateTime:Ljava/lang/String;

    iput-object v0, v7, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    return-object v7
.end method


# virtual methods
.method public clear()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    const-string v0, "1"

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    const-string v0, "0"

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mCreateTime:Ljava/lang/String;

    const-string v0, "0"

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    return-void
.end method

.method public copy(Lcom/mediatek/todos/TodoInfo;)V
    .locals 1
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    iget-object v0, p1, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    iget-object v0, p1, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    iget-object v0, p1, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    iget-object v0, p1, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    iget-object v0, p1, Lcom/mediatek/todos/TodoInfo;->mCreateTime:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mCreateTime:Ljava/lang/String;

    iget-object v0, p1, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public equals(Lcom/mediatek/todos/TodoInfo;)Z
    .locals 3
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    iget-object v2, p1, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    iget-object v2, p1, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    iget-object v2, p1, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    iget-object v2, p1, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    iget-object v2, p1, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method getCompleteTime()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method getCreateTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mCreateTime:Ljava/lang/String;

    return-object v0
.end method

.method getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method getDueDate()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    return-object v0
.end method

.method getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isComplete()Z
    .locals 5

    const/4 v0, 0x0

    const-string v1, "1"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/todos/TodoInfo;->getCompleteTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gtz v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isExpire()Z
    .locals 5

    const/4 v0, 0x0

    const-string v1, "2"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gtz v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public makeContentValues()Landroid/content/ContentValues;
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "_id"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "title"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "complete_time"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "create_time"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mCreateTime:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "description"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "dtend"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "status"

    iget-object v2, p0, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method setCompleteDay(J)V
    .locals 1
    .param p1    # J

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    return-void
.end method

.method setCompleteTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/todos/TodoInfo;->mCompleteTime:Ljava/lang/String;

    return-void
.end method

.method setCreateTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/todos/TodoInfo;->mCreateTime:Ljava/lang/String;

    return-void
.end method

.method setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/todos/TodoInfo;->mDescription:Ljava/lang/String;

    return-void
.end method

.method setDueDate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    return-void
.end method

.method setDueDay(J)V
    .locals 1
    .param p1    # J

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/todos/TodoInfo;->mDueDate:Ljava/lang/String;

    return-void
.end method

.method setStatus(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/todos/TodoInfo;->mStatus:Ljava/lang/String;

    return-void
.end method

.method setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/todos/TodoInfo;->mTitle:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "------ TodoInfo--------\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "title = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/todos/TodoInfo;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "description = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mediatek/todos/TodoInfo;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateStatus(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    const-string v1, "2"

    invoke-virtual {p0, v1}, Lcom/mediatek/todos/TodoInfo;->setStatus(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/todos/TodoInfo;->setCompleteDay(J)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "1"

    invoke-virtual {p0, v1}, Lcom/mediatek/todos/TodoInfo;->setStatus(Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/mediatek/todos/TodoInfo;->setCompleteDay(J)V

    goto :goto_0
.end method
