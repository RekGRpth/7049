.class Lcom/mediatek/todos/TodosActivity$AdapterViewListener;
.super Ljava/lang/Object;
.source "TodosActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/todos/TodosActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AdapterViewListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/todos/TodosActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/todos/TodosActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v9

    invoke-virtual {v9, p3}, Lcom/mediatek/todos/TodosListAdapter;->getItemViewType(I)I

    move-result v8

    const-string v9, "TodosActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onItemClick viewType ="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " position="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch v8, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v10

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/todos/TodosListAdapter;->isTodosExpand()Z

    move-result v9

    if-nez v9, :cond_0

    const/4 v9, 0x1

    :goto_1
    invoke-virtual {v10, v9}, Lcom/mediatek/todos/TodosListAdapter;->setTodosExpand(Z)V

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    goto :goto_1

    :pswitch_1
    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/todos/TodosListAdapter;->isEditing()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$200(Lcom/mediatek/todos/TodosActivity;)V

    :cond_1
    new-instance v4, Landroid/content/Intent;

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    const-class v10, Lcom/mediatek/todos/EditTodoActivity;

    invoke-direct {v4, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    const/4 v10, 0x1

    invoke-virtual {v9, v4, v10}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_2
    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v10

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/todos/TodosListAdapter;->isDonesExPand()Z

    move-result v9

    if-nez v9, :cond_2

    const/4 v9, 0x1

    :goto_2
    invoke-virtual {v10, v9}, Lcom/mediatek/todos/TodosListAdapter;->setDonesExpand(Z)V

    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    goto :goto_2

    :pswitch_3
    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/todos/TodosListAdapter;->isEditing()Z

    move-result v9

    if-eqz v9, :cond_9

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_5

    const/4 v7, 0x1

    :goto_3
    const/4 v9, 0x4

    if-ne v8, v9, :cond_6

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_6

    const/4 v1, 0x1

    :goto_4
    if-nez v7, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_4

    instance-of v9, v6, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;

    if-eqz v9, :cond_4

    move-object v2, v6

    check-cast v2, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;

    iget-object v9, v2, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v9}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    iget-object v10, v2, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoCheckBox:Landroid/widget/CheckBox;

    if-nez v0, :cond_7

    const/4 v9, 0x1

    :goto_5
    invoke-virtual {v10, v9}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v9}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v10

    if-nez v0, :cond_8

    const/4 v9, 0x1

    :goto_6
    invoke-virtual {v10, p3, v9}, Lcom/mediatek/todos/TodosListAdapter;->selectItem(IZ)V

    :cond_4
    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v9}, Lcom/mediatek/todos/TodosActivity;->updateBottomBarWidgetState()V

    goto/16 :goto_0

    :cond_5
    const/4 v7, 0x0

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    goto :goto_4

    :cond_7
    const/4 v9, 0x0

    goto :goto_5

    :cond_8
    const/4 v9, 0x0

    goto :goto_6

    :cond_9
    new-instance v5, Landroid/content/Intent;

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    const-class v10, Lcom/mediatek/todos/EditTodoActivity;

    invoke-direct {v5, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v9

    invoke-interface {v9, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    const-string v9, "data_passed"

    invoke-virtual {v5, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v9, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    const/4 v10, 0x2

    invoke-virtual {v9, v5, v10}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v2}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/mediatek/todos/TodosListAdapter;->getItemViewType(I)I

    move-result v1

    const/4 v0, 0x0

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const-string v2, "TodosActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cann\'t switch viewType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v2, "TodosActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onItemLongClick viewType ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " editType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v2}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosListAdapter;->getEditType()I

    move-result v2

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v2}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mediatek/todos/TodosListAdapter;->setEditingType(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosActivity;->changeToEditMode()V

    :cond_0
    if-ne v0, v5, :cond_3

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v2}, Lcom/mediatek/todos/TodosActivity;->access$300(Lcom/mediatek/todos/TodosActivity;)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f020024

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v2}, Lcom/mediatek/todos/TodosActivity;->access$300(Lcom/mediatek/todos/TodosActivity;)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f070021

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v2}, Lcom/mediatek/todos/TodosActivity;->access$100(Lcom/mediatek/todos/TodosActivity;)Lcom/mediatek/todos/TodosListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3, v5}, Lcom/mediatek/todos/TodosListAdapter;->selectItem(IZ)V

    :cond_2
    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosActivity;->updateBottomBarWidgetState()V

    return v5

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v2}, Lcom/mediatek/todos/TodosActivity;->access$300(Lcom/mediatek/todos/TodosActivity;)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f020023

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/mediatek/todos/TodosActivity$AdapterViewListener;->this$0:Lcom/mediatek/todos/TodosActivity;

    invoke-static {v2}, Lcom/mediatek/todos/TodosActivity;->access$300(Lcom/mediatek/todos/TodosActivity;)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f070020

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
