.class public Lcom/mediatek/todos/TodosListAdapter;
.super Landroid/widget/BaseAdapter;
.source "TodosListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/mediatek/todos/QueryListener;
.implements Lcom/mediatek/todos/TimeChangeReceiver$TimeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/todos/TodosListAdapter$ViewHolder;,
        Lcom/mediatek/todos/TodosListAdapter$FooterHolder;,
        Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;
    }
.end annotation


# static fields
.field public static final EDIT_DONES:I = 0x2

.field public static final EDIT_NULL:I = 0x0

.field public static final EDIT_TODOS:I = 0x1

.field private static final EMPTY_TEXT:Ljava/lang/String; = ""

.field private static final TAG:Ljava/lang/String; = "TodosListAdapter"

.field public static final TYPE_DONES_FOOTER:I = 0x5

.field public static final TYPE_DONES_HEADER:I = 0x3

.field public static final TYPE_DONES_ITEM:I = 0x4

.field public static final TYPE_TODOS_FOOTER:I = 0x2

.field public static final TYPE_TODOS_HEADER:I = 0x0

.field public static final TYPE_TODOS_ITEM:I = 0x1


# instance fields
.field private mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

.field private mContext:Landroid/content/Context;

.field private mDoneComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/mediatek/todos/TodoInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDonesCheckStates:Landroid/util/SparseBooleanArray;

.field private mDonesDataSource:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/todos/TodoInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDonesExpand:Z

.field private mDonesFooterView:Landroid/view/View;

.field private mDonesHeaderView:Landroid/view/View;

.field private mEditType:I

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsDataDirty:Z

.field private mIsLoadingData:Z

.field private mTodoComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/mediatek/todos/TodoInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTodosCheckStates:Landroid/util/SparseBooleanArray;

.field private mTodosDataSource:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/todos/TodoInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTodosExpand:Z

.field private mTodosFooterView:Landroid/view/View;

.field private mTodosHeaderView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/mediatek/todos/TodosListAdapter$1;

    invoke-direct {v0, p0}, Lcom/mediatek/todos/TodosListAdapter$1;-><init>(Lcom/mediatek/todos/TodosListAdapter;)V

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodoComparator:Ljava/util/Comparator;

    new-instance v0, Lcom/mediatek/todos/TodosListAdapter$2;

    invoke-direct {v0, p0}, Lcom/mediatek/todos/TodosListAdapter$2;-><init>(Lcom/mediatek/todos/TodosListAdapter;)V

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mDoneComparator:Ljava/util/Comparator;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    iput-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    iput-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosHeaderView:Landroid/view/View;

    iput-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosFooterView:Landroid/view/View;

    iput-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesHeaderView:Landroid/view/View;

    iput-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesFooterView:Landroid/view/View;

    iput v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    iput-boolean v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    iput-boolean v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    iput-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsLoadingData:Z

    iput-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsDataDirty:Z

    iput-object p1, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/todos/TodoAsyncQuery;->getInstatnce(Landroid/content/Context;)Lcom/mediatek/todos/TodoAsyncQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0, v1}, Lcom/mediatek/todos/TodosListAdapter;->startQuery(Ljava/lang/String;)V

    return-void
.end method

.method private getIndexByPosition(Ljava/lang/String;I)I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x0

    const-string v1, "1"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, p2, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, p2, -0x3

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v1, :cond_2

    add-int/lit8 v1, p2, -0x2

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v0, v1, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v0, p2, -0x2

    goto :goto_0
.end method

.method private getInfoText(Lcom/mediatek/todos/TodoInfo;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getItemView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;

    if-nez v8, :cond_4

    :cond_0
    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f030004

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;

    invoke-direct {v4}, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;-><init>()V

    const v8, 0x7f0a000a

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoText:Landroid/widget/TextView;

    const v8, 0x7f0a000b

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoState:Landroid/widget/ImageView;

    const v8, 0x7f0a000c

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoDueDate:Landroid/widget/TextView;

    const v8, 0x7f0a000e

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    iput-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoCheckBox:Landroid/widget/CheckBox;

    const v8, 0x7f0a000d

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mChangeInfoState:Landroid/widget/ImageView;

    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    const/4 v6, 0x0

    const v7, 0x7f02000a

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const v0, 0x7f02000c

    iget-boolean v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v8, :cond_1

    const-string v8, "1"

    invoke-direct {p0, v8, p1}, Lcom/mediatek/todos/TodosListAdapter;->getIndexByPosition(Ljava/lang/String;I)I

    move-result v5

    if-ltz v5, :cond_1

    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v5, v8, :cond_1

    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/todos/TodoInfo;

    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v9

    const/4 v11, 0x1

    invoke-static {v8, v9, v10, v11}, Lcom/mediatek/todos/Utils;->getDateText(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v8, v5}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    invoke-virtual {v6}, Lcom/mediatek/todos/TodoInfo;->isExpire()Z

    move-result v8

    if-eqz v8, :cond_5

    const v7, 0x7f02000a

    :goto_1
    iget v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    const/4 v1, 0x1

    :cond_1
    :goto_2
    if-nez v6, :cond_2

    iget-boolean v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    if-eqz v8, :cond_2

    const-string v8, "2"

    invoke-direct {p0, v8, p1}, Lcom/mediatek/todos/TodosListAdapter;->getIndexByPosition(Ljava/lang/String;I)I

    move-result v5

    if-ltz v5, :cond_2

    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v5, v8, :cond_2

    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mediatek/todos/TodoInfo;

    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Lcom/mediatek/todos/TodoInfo;->getCompleteTime()J

    move-result-wide v9

    const/4 v11, 0x2

    invoke-static {v8, v9, v10, v11}, Lcom/mediatek/todos/Utils;->getDateText(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v8, v5}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    iget v8, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_7

    const/4 v1, 0x1

    :goto_3
    const v7, 0x7f020007

    const v0, 0x7f020016

    :cond_2
    if-nez v6, :cond_3

    const-string v8, "TodosListAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "-------info is null ---------"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "TodosListAdapter"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "position = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " To-do size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " dones size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoText:Landroid/widget/TextView;

    invoke-direct {p0, v6}, Lcom/mediatek/todos/TodosListAdapter;->getInfoText(Lcom/mediatek/todos/TodoInfo;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoState:Landroid/widget/ImageView;

    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoDueDate:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v1, :cond_8

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mChangeInfoState:Landroid/widget/ImageView;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoCheckBox:Landroid/widget/CheckBox;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v8, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :goto_4
    return-object p2

    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;

    goto/16 :goto_0

    :cond_5
    const v7, 0x7f02001f

    goto/16 :goto_1

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_8
    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mTodoInfoCheckBox:Landroid/widget/CheckBox;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mChangeInfoState:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mChangeInfoState:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mChangeInfoState:Landroid/widget/ImageView;

    invoke-virtual {v8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v8, v4, Lcom/mediatek/todos/TodosListAdapter$ViewHolder;->mChangeInfoState:Landroid/widget/ImageView;

    invoke-virtual {v8, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_4
.end method

.method private reallyAddItem(Ljava/lang/String;Lcom/mediatek/todos/TodoInfo;)I
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/todos/TodoInfo;

    const/4 v3, 0x0

    const/4 v2, -0x1

    const-string v5, "2"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p2}, Lcom/mediatek/todos/TodoInfo;->getCompleteTime()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getCompleteTime()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-lez v5, :cond_1

    move v2, v1

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v5, v2, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_0
    :goto_1
    return v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v4, :cond_0

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p2}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-ltz v5, :cond_3

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_4

    :cond_3
    move v2, v1

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v5, v2, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private updateCheckBoxValue(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    :cond_0
    :goto_0
    if-nez v1, :cond_3

    :cond_1
    return-void

    :cond_2
    if-ne p1, v3, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x2

    :goto_1
    if-lt v0, p2, :cond_1

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_4

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->delete(I)V

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private updateDonesFooterView()V
    .locals 5

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesFooterView:Landroid/view/View;

    if-nez v2, :cond_0

    new-instance v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;

    invoke-direct {v0}, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030002

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesFooterView:Landroid/view/View;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesFooterView:Landroid/view/View;

    const v3, 0x7f0a0007

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;->mFooterHelper:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesFooterView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;->mFooterHelper:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;->mFooterHelper:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesFooterView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;

    goto :goto_0
.end method

.method private updateDonesHeaderView()V
    .locals 5

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesHeaderView:Landroid/view/View;

    if-nez v2, :cond_0

    new-instance v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;

    invoke-direct {v0}, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030003

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesHeaderView:Landroid/view/View;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesHeaderView:Landroid/view/View;

    const v3, 0x7f0a0008

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mHeaderTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesHeaderView:Landroid/view/View;

    const v3, 0x7f0a0009

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mBtnExpand:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesHeaderView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mHeaderTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mBtnExpand:Landroid/widget/ImageView;

    const v3, 0x7f020009

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesHeaderView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mBtnExpand:Landroid/widget/ImageView;

    const v3, 0x7f020008

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method private updateHeaderNumberText()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/mediatek/todos/TodosActivity;

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/mediatek/todos/TodosActivity;->updateHeaderNumberText(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/todos/TodosActivity;->updateHeaderNumberText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateTodosFooterView()V
    .locals 5

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosFooterView:Landroid/view/View;

    if-nez v2, :cond_0

    new-instance v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;

    invoke-direct {v0}, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030002

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosFooterView:Landroid/view/View;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosFooterView:Landroid/view/View;

    const v3, 0x7f0a0007

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;->mFooterHelper:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosFooterView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;->mFooterHelper:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosFooterView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/todos/TodosListAdapter$FooterHolder;

    goto :goto_0
.end method

.method private updateTodosHeaderView()V
    .locals 5

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosHeaderView:Landroid/view/View;

    if-nez v2, :cond_0

    new-instance v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;

    invoke-direct {v0}, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030003

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosHeaderView:Landroid/view/View;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosHeaderView:Landroid/view/View;

    const v3, 0x7f0a0008

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mHeaderTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosHeaderView:Landroid/view/View;

    const v3, 0x7f0a0009

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mBtnExpand:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosHeaderView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mHeaderTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mBtnExpand:Landroid/widget/ImageView;

    const v3, 0x7f020009

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosHeaderView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lcom/mediatek/todos/TodosListAdapter$HeaderHolder;->mBtnExpand:Landroid/widget/ImageView;

    const v3, 0x7f020008

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method


# virtual methods
.method public addItem(Lcom/mediatek/todos/TodoInfo;)V
    .locals 6
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsLoadingData:Z

    if-eqz v1, :cond_1

    :cond_0
    const-string v1, "TodosListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addItem() failed. mFinishLoadingItems="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsLoadingData:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Or Ill-legeal TodoInfo."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    const-string v1, "2"

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getCompleteTime()J

    move-result-wide v1

    cmp-long v1, v1, v4

    if-eqz v1, :cond_2

    const-string v1, "2"

    invoke-direct {p0, v1, p1}, Lcom/mediatek/todos/TodosListAdapter;->reallyAddItem(Ljava/lang/String;Lcom/mediatek/todos/TodoInfo;)I

    move-result v0

    :cond_2
    if-ne v0, v3, :cond_3

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :cond_3
    iget v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    invoke-direct {p0, v1, v0}, Lcom/mediatek/todos/TodosListAdapter;->updateCheckBoxValue(II)V

    :goto_1
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateHeaderNumberText()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v1

    cmp-long v1, v1, v4

    if-eqz v1, :cond_5

    const-string v1, "1"

    invoke-direct {p0, v1, p1}, Lcom/mediatek/todos/TodosListAdapter;->reallyAddItem(Ljava/lang/String;Lcom/mediatek/todos/TodoInfo;)I

    move-result v0

    :cond_5
    if-ne v0, v3, :cond_6

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :cond_6
    iget v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    invoke-direct {p0, v1, v0}, Lcom/mediatek/todos/TodosListAdapter;->updateCheckBoxValue(II)V

    goto :goto_1
.end method

.method public allSelectAction(Z)V
    .locals 5
    .param p1    # Z

    if-nez p1, :cond_1

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->clear()V

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->clear()V

    :cond_0
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void

    :cond_1
    iget v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v2, p1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v2, p1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public deleteSelectedItems()V
    .locals 5

    iget v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    :goto_0
    if-ltz v2, :cond_1

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p0, v3}, Lcom/mediatek/todos/TodosListAdapter;->startDelete(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->clear()V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateHeaderNumberText()V

    return-void

    :cond_3
    iget v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    :goto_2
    if-ltz v2, :cond_5

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {p0, v3}, Lcom/mediatek/todos/TodosListAdapter;->startDelete(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_4
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->clear()V

    goto :goto_1
.end method

.method public getCount()I
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_2
    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_2

    :cond_2
    iget-boolean v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    goto :goto_1
.end method

.method public getDonesDataSource()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/todos/TodoInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getEditType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    return v0
.end method

.method public getItem(I)Lcom/mediatek/todos/TodoInfo;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v2, :cond_2

    add-int/lit8 v0, p1, -0x1

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TodoInfo;

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    add-int/lit8 v0, p1, -0x3

    :goto_1
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/todos/TodoInfo;

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v2, :cond_4

    add-int/lit8 v2, p1, -0x2

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int v0, v2, v3

    goto :goto_1

    :cond_4
    add-int/lit8 v0, p1, -0x2

    goto :goto_1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/mediatek/todos/TodosListAdapter;->getItem(I)Lcom/mediatek/todos/TodoInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemPosition(Lcom/mediatek/todos/TodoInfo;)I
    .locals 5
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const/4 v1, 0x0

    const-string v2, "1"

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit8 v1, v0, 0x1

    :cond_0
    :goto_0
    const-string v2, "TodosListAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getItemPosition() position="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    return v1

    :cond_1
    const-string v2, "2"

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-boolean v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, 0x2

    :goto_1
    if-ltz v0, :cond_0

    add-int/2addr v1, v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    goto :goto_1
.end method

.method public getItemViewType(I)I
    .locals 4
    .param p1    # I

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-boolean v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    if-nez p1, :cond_1

    const/4 v2, 0x0

    :goto_1
    return v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    if-nez v0, :cond_2

    const/4 v2, 0x2

    goto :goto_1

    :cond_2
    if-gt p1, v1, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    if-nez v0, :cond_4

    const/4 v3, 0x2

    if-eq p1, v3, :cond_5

    :cond_4
    if-eqz v0, :cond_6

    add-int/lit8 v3, v1, 0x1

    if-ne p1, v3, :cond_6

    :cond_5
    const/4 v2, 0x3

    goto :goto_1

    :cond_6
    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v2, 0x5

    goto :goto_1

    :cond_7
    const/4 v2, 0x4

    goto :goto_1
.end method

.method public getSeletedDonesNumber()I
    .locals 5

    const/4 v0, 0x0

    iget v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public getSeletedTodosNumber()I
    .locals 5

    const/4 v0, 0x0

    iget v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v3, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public getTodosDataSource()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/todos/TodoInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/mediatek/todos/TodosListAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/todos/TodosListAdapter;->getItemView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateTodosHeaderView()V

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosHeaderView:Landroid/view/View;

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateTodosFooterView()V

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosFooterView:Landroid/view/View;

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateDonesHeaderView()V

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesHeaderView:Landroid/view/View;

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateDonesFooterView()V

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesFooterView:Landroid/view/View;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public isDonesExPand()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    return v0
.end method

.method public isEditing()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTodosExpand()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "TodosListAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TodosListAdapter Click view tag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_1

    instance-of v2, v1, Lcom/mediatek/todos/TodoInfo;

    if-eqz v2, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/mediatek/todos/TodoInfo;

    const-string v2, "2"

    invoke-virtual {v0}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "1"

    invoke-virtual {v0, v2}, Lcom/mediatek/todos/TodoInfo;->updateStatus(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/mediatek/todos/TodosListAdapter;->startUpdate(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/mediatek/todos/TodosListAdapter;->addItem(Lcom/mediatek/todos/TodoInfo;)V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/mediatek/todos/TodosActivity;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/mediatek/todos/TodosActivity;

    invoke-virtual {v2}, Lcom/mediatek/todos/TodosActivity;->updateBottomBarWidgetState()V

    :cond_1
    return-void

    :cond_2
    const-string v2, "1"

    invoke-virtual {v0}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "2"

    invoke-virtual {v0, v2}, Lcom/mediatek/todos/TodoInfo;->updateStatus(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/mediatek/todos/TodosListAdapter;->startUpdate(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/mediatek/todos/TodosListAdapter;->addItem(Lcom/mediatek/todos/TodoInfo;)V

    goto :goto_0
.end method

.method public onDateChange()V
    .locals 0

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onDeleteComplete(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    if-gtz p2, :cond_0

    const-string v0, "TodosListAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDeleteComplete() failed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f070012

    invoke-static {v0, v1}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsDataDirty:Z

    :cond_0
    return-void
.end method

.method public onInsertComplete(ILandroid/net/Uri;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/net/Uri;

    const-string v0, "TodosListAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onInsertComplete() uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onQueryComplete(ILandroid/database/Cursor;)V
    .locals 4
    .param p1    # I
    .param p2    # Landroid/database/Cursor;

    const/4 v3, 0x1

    const-string v1, "TodosListAdapter"

    const-string v2, "onQueryComplete."

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-static {p2}, Lcom/mediatek/todos/TodoInfo;->makeTodoInfoFromCursor(Landroid/database/Cursor;)Lcom/mediatek/todos/TodoInfo;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v3, :cond_2

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodoComparator:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_2
    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v3, :cond_3

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mDoneComparator:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateHeaderNumberText()V

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsLoadingData:Z

    return-void

    :cond_4
    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onTimePick()V
    .locals 0

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onUpdateComplete(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    if-gtz p2, :cond_0

    const-string v0, "TodosListAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateComplete() result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f070012

    invoke-static {v0, v1}, Lcom/mediatek/todos/Utils;->prompt(Landroid/content/Context;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsDataDirty:Z

    :cond_0
    return-void
.end method

.method public refreshData()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsDataDirty:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/todos/TodosListAdapter;->startQuery(Ljava/lang/String;)V

    return-void
.end method

.method public removeItem(Lcom/mediatek/todos/TodoInfo;)V
    .locals 7
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "TodosListAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeItem remove a To-do item Pos="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateHeaderNumberText()V

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v0, :cond_3

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "TodosListAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeItem remove a Done item Pos="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateHeaderNumberText()V

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    const-string v4, "TodosListAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeItem failed. not find TodoInfo :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public selectItem(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    iget-boolean v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v1, :cond_1

    add-int/lit8 v0, p1, -0x1

    const-string v1, "TodosListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectItem To-do : index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v0, p1, -0x3

    :goto_1
    const-string v1, "TodosListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectItem done : index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eqz v1, :cond_3

    add-int/lit8 v1, p1, -0x2

    iget-object v2, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v0, v1, v2

    goto :goto_1

    :cond_3
    add-int/lit8 v0, p1, -0x2

    goto :goto_1
.end method

.method public setDonesExpand(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesExpand:Z

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setEditingType(I)Z
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0

    :cond_3
    iget v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    if-eq v1, p1, :cond_2

    iput p1, p0, Lcom/mediatek/todos/TodosListAdapter;->mEditType:I

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    iget-object v1, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->clear()V

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setTodosExpand(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosExpand:Z

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public startDelete(Lcom/mediatek/todos/TodoInfo;)V
    .locals 3
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const-string v0, "TodosListAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startDelete(). info = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/todos/Utils;->writeAdapterDataToDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V

    return-void
.end method

.method public startInsert(Lcom/mediatek/todos/TodoInfo;)V
    .locals 2
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const-string v0, "TodosListAdapter"

    const-string v1, "startInsert()."

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    const/4 v1, 0x3

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/todos/Utils;->writeAdapterDataToDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V

    return-void
.end method

.method public startQuery(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    const-string v0, "TodosListAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startQuery query from DB. selection : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsLoadingData:Z

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    const/4 v1, 0x0

    sget-object v3, Lcom/mediatek/todos/TodoAsyncQuery;->TODO_URI:Landroid/net/Uri;

    move-object v2, p0

    move-object v5, p1

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public startUpdate(Lcom/mediatek/todos/TodoInfo;)V
    .locals 2
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const-string v0, "TodosListAdapter"

    const-string v1, "startUpdate() : "

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/todos/TodosListAdapter;->mAsyncQuery:Lcom/mediatek/todos/TodoAsyncQuery;

    const/4 v1, 0x2

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/todos/Utils;->writeAdapterDataToDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V

    return-void
.end method

.method public updateItemData(Lcom/mediatek/todos/TodoInfo;)V
    .locals 9
    .param p1    # Lcom/mediatek/todos/TodoInfo;

    const/4 v6, 0x0

    const/4 v8, 0x1

    iget-boolean v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mIsLoadingData:Z

    if-eqz v4, :cond_0

    const-string v4, "TodosListAdapter"

    const-string v5, "updateItemData failed for Adapter is loading data from DB."

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_5

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    if-le v1, v8, :cond_2

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodoComparator:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_1
    :goto_2
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateHeaderNumberText()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v6, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v8, :cond_1

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mDoneComparator:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_2

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v0, :cond_a

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getStatus()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v3}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getDueDate()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_7

    if-le v0, v8, :cond_7

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mDoneComparator:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_6
    :goto_4
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateHeaderNumberText()V

    goto/16 :goto_0

    :cond_7
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_8
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v6, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v8, :cond_6

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodoComparator:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_4

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_a
    const-string v4, "TodosListAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateItemData failed. not find TodoInfo :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public updateSelectedStatus(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v4, "1"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    :goto_0
    if-ltz v2, :cond_3

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    const-string v4, "2"

    invoke-virtual {v3, v4}, Lcom/mediatek/todos/TodoInfo;->updateStatus(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/mediatek/todos/TodosListAdapter;->startUpdate(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v5, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    const-string v4, "2"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    :goto_1
    if-ltz v2, :cond_3

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/todos/TodoInfo;

    const-string v4, "1"

    invoke-virtual {v3, v4}, Lcom/mediatek/todos/TodoInfo;->updateStatus(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/mediatek/todos/TodosListAdapter;->startUpdate(Lcom/mediatek/todos/TodoInfo;)V

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4, v5, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v6, :cond_4

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosDataSource:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodoComparator:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_4
    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v6, :cond_5

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesDataSource:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/mediatek/todos/TodosListAdapter;->mDoneComparator:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_5
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mTodosCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->clear()V

    iget-object v4, p0, Lcom/mediatek/todos/TodosListAdapter;->mDonesCheckStates:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->clear()V

    invoke-direct {p0}, Lcom/mediatek/todos/TodosListAdapter;->updateHeaderNumberText()V

    return-void
.end method
