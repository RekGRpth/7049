.class public Lcom/mediatek/todos/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final DATE_EXPIRED:I = 0x0

.field public static final DATE_NOT_EXPIRE:I = 0xb

.field public static final DATE_NO_EXPIRE:I = -0x1

.field public static final DATE_TYPE_COMPLETE:I = 0x2

.field public static final DATE_TYPE_DUE:I = 0x1

.field public static final DUE_DATE_HOUR:I = 0x17

.field public static final DUE_DATE_MINUTE:I = 0x3b

.field public static final DUE_DATE_SECOND:I = 0x3b

.field public static final EDIT_RESULT_NOT_SAVED:I = 0x1

.field public static final EDIT_RESULT_SAVED:I = 0x0

.field public static final KEY_EDIT_RESULT:Ljava/lang/String; = "key_edit_result"

.field public static final KEY_EDIT_TODO:Ljava/lang/String; = "key_edit_todo"

.field public static final KEY_PASSED_DATA:Ljava/lang/String; = "data_passed"

.field public static final OPERATOR_DELETE:I = 0x4

.field public static final OPERATOR_INSERT:I = 0x3

.field public static final OPERATOR_NONE:I = 0x0

.field public static final OPERATOR_QUERY:I = 0x1

.field public static final OPERATOR_UPDATE:I = 0x2

.field public static final REQUEST_EDIT:I = 0x0

.field public static final RESULT_EDIT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Utils"

.field private static sToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/todos/Utils;->sToast:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dateCompare(Landroid/text/format/Time;Landroid/text/format/Time;)I
    .locals 4
    .param p0    # Landroid/text/format/Time;
    .param p1    # Landroid/text/format/Time;

    const/4 v1, 0x1

    const/4 v0, -0x1

    iget v2, p0, Landroid/text/format/Time;->year:I

    iget v3, p1, Landroid/text/format/Time;->year:I

    if-ge v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Landroid/text/format/Time;->year:I

    iget v3, p1, Landroid/text/format/Time;->year:I

    if-le v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Landroid/text/format/Time;->yearDay:I

    iget v3, p1, Landroid/text/format/Time;->yearDay:I

    if-lt v2, v3, :cond_0

    iget v0, p0, Landroid/text/format/Time;->yearDay:I

    iget v2, p1, Landroid/text/format/Time;->yearDay:I

    if-le v0, v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getColumnByName(Ljava/lang/String;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/database/Cursor;

    invoke-interface {p1, p0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const-string v2, "Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Column Name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "is not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v2, "_id"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDateText(Landroid/content/Context;JI)Ljava/lang/String;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # I

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    if-ne p3, v3, :cond_2

    cmp-long v2, p1, v4

    if-gtz v2, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    if-ne p3, v3, :cond_3

    cmp-long v3, p1, v4

    if-lez v3, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string v3, "Utils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "format date failed.No this type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/todos/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static prompt(Landroid/content/Context;I)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I

    sget-object v0, Lcom/mediatek/todos/Utils;->sToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/mediatek/todos/Utils;->sToast:Landroid/widget/Toast;

    :goto_0
    sget-object v0, Lcom/mediatek/todos/Utils;->sToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    sget-object v0, Lcom/mediatek/todos/Utils;->sToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method private static syncWithDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V
    .locals 13
    .param p0    # Lcom/mediatek/todos/QueryListener;
    .param p1    # Lcom/mediatek/todos/TodoInfo;
    .param p2    # Landroid/content/AsyncQueryHandler;
    .param p3    # I

    const-string v12, "_id"

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->makeContentValues()Landroid/content/ContentValues;

    move-result-object v4

    packed-switch p3, :pswitch_data_0

    :pswitch_0
    const-string v0, "Utils"

    const-string v1, "Unexpected token of pending db operation"

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 p2, 0x0

    return-void

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x0

    sget-object v3, Lcom/mediatek/todos/TodoAsyncQuery;->TODO_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    move-object v0, p2

    move-object v2, p0

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    sget-object v1, Lcom/mediatek/todos/TodoAsyncQuery;->TODO_URI:Landroid/net/Uri;

    invoke-virtual {p2, v0, p0, v1, v4}, Landroid/content/AsyncQueryHandler;->startInsert(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mediatek/todos/TodoInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    sget-object v9, Lcom/mediatek/todos/TodoAsyncQuery;->TODO_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    move-object v6, p2

    move-object v8, p0

    move-object v10, v5

    invoke-virtual/range {v6 .. v11}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    const-string v0, "Utils"

    const-string v1, "Unexpected token NONE"

    invoke-static {v0, v1}, Lcom/mediatek/todos/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static writeAdapterDataToDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V
    .locals 0
    .param p0    # Lcom/mediatek/todos/QueryListener;
    .param p1    # Lcom/mediatek/todos/TodoInfo;
    .param p2    # Landroid/content/AsyncQueryHandler;
    .param p3    # I

    invoke-static {p0, p1, p2, p3}, Lcom/mediatek/todos/Utils;->syncWithDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V

    return-void
.end method

.method public static writeAdapterDataToDB(Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V
    .locals 1
    .param p0    # Lcom/mediatek/todos/TodoInfo;
    .param p1    # Landroid/content/AsyncQueryHandler;
    .param p2    # I

    const/4 v0, 0x0

    invoke-static {v0, p0, p1, p2}, Lcom/mediatek/todos/Utils;->syncWithDB(Lcom/mediatek/todos/QueryListener;Lcom/mediatek/todos/TodoInfo;Landroid/content/AsyncQueryHandler;I)V

    return-void
.end method
