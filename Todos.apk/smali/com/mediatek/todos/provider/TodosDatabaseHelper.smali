.class public Lcom/mediatek/todos/provider/TodosDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "TodosDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/todos/provider/TodosDatabaseHelper$TodoColumn;,
        Lcom/mediatek/todos/provider/TodosDatabaseHelper$Tables;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "todos.db"

.field static final DATABASE_VERSION:I = 0x134

.field private static final DAY_IN_SECONDS:I = 0x15180

.field private static final LOGD:Z = false

.field private static final TAG:Ljava/lang/String; = "TodosDatabaseHelper"

.field private static sSingleton:Lcom/mediatek/todos/provider/TodosDatabaseHelper;


# instance fields
.field private mTodosInserter:Landroid/database/DatabaseUtils$InsertHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/todos/provider/TodosDatabaseHelper;->sSingleton:Lcom/mediatek/todos/provider/TodosDatabaseHelper;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const-string v0, "todos.db"

    const/4 v1, 0x0

    const/16 v2, 0x134

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method private bootstrapDB(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "TodosDatabaseHelper"

    const-string v1, "Bootstrapping database"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/mediatek/todos/provider/TodosDatabaseHelper;->createTodoTable(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method private createTodoTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const-string v0, "CREATE TABLE Todos (_id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT,description TEXT,priority INTEGER,status INTEGER NOT NULL DEFAULT 0,dtend INTEGER NOT NULL DEFAULT 0,create_time INTEGER,complete_time INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/mediatek/todos/provider/TodosDatabaseHelper;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/mediatek/todos/provider/TodosDatabaseHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/todos/provider/TodosDatabaseHelper;->sSingleton:Lcom/mediatek/todos/provider/TodosDatabaseHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/todos/provider/TodosDatabaseHelper;

    invoke-direct {v0, p0}, Lcom/mediatek/todos/provider/TodosDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/todos/provider/TodosDatabaseHelper;->sSingleton:Lcom/mediatek/todos/provider/TodosDatabaseHelper;

    :cond_0
    sget-object v0, Lcom/mediatek/todos/provider/TodosDatabaseHelper;->sSingleton:Lcom/mediatek/todos/provider/TodosDatabaseHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, p1}, Lcom/mediatek/todos/provider/TodosDatabaseHelper;->bootstrapDB(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Todos"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/todos/provider/TodosDatabaseHelper;->mTodosInserter:Landroid/database/DatabaseUtils$InsertHelper;

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public todosInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1    # Landroid/content/ContentValues;

    iget-object v0, p0, Lcom/mediatek/todos/provider/TodosDatabaseHelper;->mTodosInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method
