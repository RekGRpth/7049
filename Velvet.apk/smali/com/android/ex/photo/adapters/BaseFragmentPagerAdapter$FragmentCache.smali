.class Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter$FragmentCache;
.super Landroid/util/LruCache;
.source "BaseFragmentPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FragmentCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Landroid/support/v4/app/Fragment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter;


# direct methods
.method public constructor <init>(Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter$FragmentCache;->this$0:Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Z
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;
    .param p4    # Ljava/lang/Object;

    check-cast p2, Ljava/lang/String;

    check-cast p3, Landroid/support/v4/app/Fragment;

    check-cast p4, Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter$FragmentCache;->entryRemoved(ZLjava/lang/String;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/support/v4/app/Fragment;
    .param p4    # Landroid/support/v4/app/Fragment;

    if-nez p1, :cond_0

    if-eqz p4, :cond_1

    if-eq p3, p4, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter$FragmentCache;->this$0:Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter;

    # getter for: Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter;->mCurTransaction:Landroid/support/v4/app/FragmentTransaction;
    invoke-static {v0}, Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter;->access$000(Lcom/android/ex/photo/adapters/BaseFragmentPagerAdapter;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :cond_1
    return-void
.end method
