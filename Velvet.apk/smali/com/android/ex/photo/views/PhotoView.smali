.class public Lcom/android/ex/photo/views/PhotoView;
.super Landroid/view/View;
.source "PhotoView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/ex/photo/views/PhotoView$RotateRunnable;,
        Lcom/android/ex/photo/views/PhotoView$SnapRunnable;,
        Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;,
        Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;
    }
.end annotation


# static fields
.field private static sCropDimPaint:Landroid/graphics/Paint;

.field private static sCropPaint:Landroid/graphics/Paint;

.field private static sCropSize:I

.field private static sInitialized:Z

.field private static sVideoImage:Landroid/graphics/Bitmap;

.field private static sVideoNotReadyImage:Landroid/graphics/Bitmap;


# instance fields
.field private mAllowCrop:Z

.field private mCropRect:Landroid/graphics/Rect;

.field private mCropSize:I

.field private mDoubleTapDebounce:Z

.field private mDoubleTapToZoomEnabled:Z

.field private mDrawMatrix:Landroid/graphics/Matrix;

.field private mDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mExternalClickListener:Landroid/view/View$OnClickListener;

.field private mFixedHeight:I

.field private mFullScreen:Z

.field private mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field private mHaveLayout:Z

.field private mIsDoubleTouch:Z

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMaxInitialScaleFactor:F

.field private mMaxScale:F

.field private mMinScale:F

.field private mOriginalMatrix:Landroid/graphics/Matrix;

.field private mRotateRunnable:Lcom/android/ex/photo/views/PhotoView$RotateRunnable;

.field private mRotation:F

.field private mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleRunnable:Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;

.field private mSnapRunnable:Lcom/android/ex/photo/views/PhotoView$SnapRunnable;

.field private mTempDst:Landroid/graphics/RectF;

.field private mTempSrc:Landroid/graphics/RectF;

.field private mTransformsEnabled:Z

.field private mTranslateRect:Landroid/graphics/RectF;

.field private mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

.field private mValues:[F

.field private mVideoBlob:[B

.field private mVideoReady:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/ex/photo/views/PhotoView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/ex/photo/views/PhotoView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/ex/photo/views/PhotoView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->initialize()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/ex/photo/views/PhotoView;FFF)V
    .locals 0
    .param p0    # Lcom/android/ex/photo/views/PhotoView;
    .param p1    # F
    .param p2    # F
    .param p3    # F

    invoke-direct {p0, p1, p2, p3}, Lcom/android/ex/photo/views/PhotoView;->scale(FFF)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/ex/photo/views/PhotoView;FF)Z
    .locals 1
    .param p0    # Lcom/android/ex/photo/views/PhotoView;
    .param p1    # F
    .param p2    # F

    invoke-direct {p0, p1, p2}, Lcom/android/ex/photo/views/PhotoView;->translate(FF)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/android/ex/photo/views/PhotoView;)V
    .locals 0
    .param p0    # Lcom/android/ex/photo/views/PhotoView;

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->snap()V

    return-void
.end method

.method static synthetic access$400(Lcom/android/ex/photo/views/PhotoView;FZ)V
    .locals 0
    .param p0    # Lcom/android/ex/photo/views/PhotoView;
    .param p1    # F
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/ex/photo/views/PhotoView;->rotate(FZ)V

    return-void
.end method

.method private configureBounds(Z)V
    .locals 7
    .param p1    # Z

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/android/ex/photo/views/PhotoView;->mHaveLayout:Z

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v3

    if-ltz v1, :cond_2

    if-ne v4, v1, :cond_7

    :cond_2
    if-ltz v0, :cond_3

    if-ne v3, v0, :cond_7

    :cond_3
    const/4 v2, 0x1

    :goto_1
    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6, v5, v5, v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    if-nez p1, :cond_4

    iget v5, p0, Lcom/android/ex/photo/views/PhotoView;->mMinScale:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_5

    iget-boolean v5, p0, Lcom/android/ex/photo/views/PhotoView;->mHaveLayout:Z

    if-eqz v5, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->generateMatrix()V

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->generateScale()V

    :cond_5
    if-nez v2, :cond_6

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_6
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    goto :goto_0

    :cond_7
    move v2, v5

    goto :goto_1

    :cond_8
    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iput-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    goto :goto_0
.end method

.method private generateMatrix()V
    .locals 13

    const/high16 v12, 0x40000000

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    iget-boolean v6, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v6, :cond_2

    sget v5, Lcom/android/ex/photo/views/PhotoView;->sCropSize:I

    :goto_0
    iget-boolean v6, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v6, :cond_3

    sget v4, Lcom/android/ex/photo/views/PhotoView;->sCropSize:I

    :goto_1
    if-ltz v1, :cond_0

    if-ne v5, v1, :cond_4

    :cond_0
    if-ltz v0, :cond_1

    if-ne v4, v0, :cond_4

    :cond_1
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_5

    iget-boolean v6, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6}, Landroid/graphics/Matrix;->reset()V

    :goto_3
    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v5

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v4

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    int-to-float v7, v1

    int-to-float v8, v0

    invoke-virtual {v6, v9, v9, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    iget-boolean v6, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    :goto_4
    new-instance v3, Landroid/graphics/RectF;

    div-int/lit8 v6, v5, 0x2

    int-to-float v6, v6

    int-to-float v7, v1

    iget v8, p0, Lcom/android/ex/photo/views/PhotoView;->mMaxInitialScaleFactor:F

    mul-float/2addr v7, v8

    div-float/2addr v7, v12

    sub-float/2addr v6, v7

    div-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    int-to-float v8, v0

    iget v9, p0, Lcom/android/ex/photo/views/PhotoView;->mMaxInitialScaleFactor:F

    mul-float/2addr v8, v9

    div-float/2addr v8, v12

    sub-float/2addr v7, v8

    div-int/lit8 v8, v5, 0x2

    int-to-float v8, v8

    int-to-float v9, v1

    iget v10, p0, Lcom/android/ex/photo/views/PhotoView;->mMaxInitialScaleFactor:F

    mul-float/2addr v9, v10

    div-float/2addr v9, v12

    add-float/2addr v8, v9

    div-int/lit8 v9, v4, 0x2

    int-to-float v9, v9

    int-to-float v10, v0

    iget v11, p0, Lcom/android/ex/photo/views/PhotoView;->mMaxInitialScaleFactor:F

    mul-float/2addr v10, v11

    div-float/2addr v10, v12

    add-float/2addr v9, v10

    invoke-direct {v3, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    invoke-virtual {v6, v3}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    sget-object v8, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v6, v7, v3, v8}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto :goto_3

    :cond_6
    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    int-to-float v7, v5

    int-to-float v8, v4

    invoke-virtual {v6, v9, v9, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_4

    :cond_7
    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/android/ex/photo/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    sget-object v9, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v6, v7, v8, v9}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto :goto_3
.end method

.method private generateScale()V
    .locals 6

    const/high16 v5, 0x41000000

    iget-object v4, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v4, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    iget-boolean v4, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->getCropSize()I

    move-result v3

    :goto_0
    iget-boolean v4, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->getCropSize()I

    move-result v2

    :goto_1
    if-ge v1, v3, :cond_2

    if-ge v0, v2, :cond_2

    iget-boolean v4, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-nez v4, :cond_2

    const/high16 v4, 0x3f800000

    iput v4, p0, Lcom/android/ex/photo/views/PhotoView;->mMinScale:F

    :goto_2
    iget v4, p0, Lcom/android/ex/photo/views/PhotoView;->mMinScale:F

    mul-float/2addr v4, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p0, Lcom/android/ex/photo/views/PhotoView;->mMaxScale:F

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v2

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->getScale()F

    move-result v4

    iput v4, p0, Lcom/android/ex/photo/views/PhotoView;->mMinScale:F

    goto :goto_2
.end method

.method private getCropSize()I
    .locals 1

    iget v0, p0, Lcom/android/ex/photo/views/PhotoView;->mCropSize:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/ex/photo/views/PhotoView;->mCropSize:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/android/ex/photo/views/PhotoView;->sCropSize:I

    goto :goto_0
.end method

.method private getScale()F
    .locals 2

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private initialize()V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-boolean v2, Lcom/android/ex/photo/views/PhotoView;->sInitialized:Z

    if-nez v2, :cond_0

    sput-boolean v4, Lcom/android/ex/photo/views/PhotoView;->sInitialized:Z

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/ex/photo/R$dimen;->photo_crop_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/android/ex/photo/views/PhotoView;->sCropSize:I

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sput-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    sget-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    sget v3, Lcom/android/ex/photo/R$color;->photo_crop_dim_color:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sput-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    sget-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    sget v3, Lcom/android/ex/photo/R$color;->photo_crop_highlight_color:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v2, Lcom/android/ex/photo/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    sget v3, Lcom/android/ex/photo/R$dimen;->photo_crop_stroke_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_0
    new-instance v2, Landroid/support/v4/view/GestureDetectorCompat;

    const/4 v3, 0x0

    invoke-direct {v2, v0, p0, v3}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    new-instance v2, Landroid/view/ScaleGestureDetector;

    invoke-direct {v2, v0, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    new-instance v2, Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;

    invoke-direct {v2, p0}, Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;-><init>(Lcom/android/ex/photo/views/PhotoView;)V

    iput-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleRunnable:Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;

    new-instance v2, Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    invoke-direct {v2, p0}, Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;-><init>(Lcom/android/ex/photo/views/PhotoView;)V

    iput-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    new-instance v2, Lcom/android/ex/photo/views/PhotoView$SnapRunnable;

    invoke-direct {v2, p0}, Lcom/android/ex/photo/views/PhotoView$SnapRunnable;-><init>(Lcom/android/ex/photo/views/PhotoView;)V

    iput-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mSnapRunnable:Lcom/android/ex/photo/views/PhotoView$SnapRunnable;

    new-instance v2, Lcom/android/ex/photo/views/PhotoView$RotateRunnable;

    invoke-direct {v2, p0}, Lcom/android/ex/photo/views/PhotoView$RotateRunnable;-><init>(Lcom/android/ex/photo/views/PhotoView;)V

    iput-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mRotateRunnable:Lcom/android/ex/photo/views/PhotoView$RotateRunnable;

    return-void
.end method

.method private rotate(FZ)V
    .locals 3
    .param p1    # F
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mRotateRunnable:Lcom/android/ex/photo/views/PhotoView$RotateRunnable;

    invoke-virtual {v0, p1}, Lcom/android/ex/photo/views/PhotoView$RotateRunnable;->start(F)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/ex/photo/views/PhotoView;->mRotation:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/android/ex/photo/views/PhotoView;->mRotation:F

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->invalidate()V

    goto :goto_0
.end method

.method private scale(FFF)V
    .locals 6
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/android/ex/photo/views/PhotoView;->mRotation:F

    neg-float v3, v3

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    iget v2, p0, Lcom/android/ex/photo/views/PhotoView;->mMinScale:F

    invoke-static {p1, v2}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iget v2, p0, Lcom/android/ex/photo/views/PhotoView;->mMaxScale:F

    invoke-static {p1, v2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->getScale()F

    move-result v0

    div-float v1, p1, v0

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->snap()V

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/android/ex/photo/views/PhotoView;->mRotation:F

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->invalidate()V

    return-void
.end method

.method private snap()V
    .locals 15

    const/high16 v14, 0x41a00000

    const/high16 v13, 0x40000000

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v12, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v11, v12}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v12, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v11, v12}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-boolean v11, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    int-to-float v3, v11

    :goto_0
    iget-boolean v11, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    int-to-float v4, v11

    :goto_1
    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v1, v11, Landroid/graphics/RectF;->left:F

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v11, Landroid/graphics/RectF;->right:F

    sub-float v11, v6, v1

    sub-float v12, v4, v3

    cmpg-float v11, v11, v12

    if-gez v11, :cond_3

    sub-float v11, v4, v3

    add-float v12, v6, v1

    sub-float/2addr v11, v12

    div-float/2addr v11, v13

    add-float v8, v3, v11

    :goto_2
    iget-boolean v11, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v11, :cond_6

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v5, v10

    :goto_3
    iget-boolean v10, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v10

    :goto_4
    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v10, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    sub-float v10, v0, v7

    sub-float v11, v2, v5

    cmpg-float v10, v10, v11

    if-gez v10, :cond_8

    sub-float v10, v2, v5

    add-float v11, v0, v7

    sub-float/2addr v10, v11

    div-float/2addr v10, v13

    add-float v9, v5, v10

    :goto_5
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v14

    if-gtz v10, :cond_0

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v14

    if-lez v10, :cond_b

    :cond_0
    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mSnapRunnable:Lcom/android/ex/photo/views/PhotoView$SnapRunnable;

    invoke-virtual {v10, v8, v9}, Lcom/android/ex/photo/views/PhotoView$SnapRunnable;->start(FF)Z

    :goto_6
    return-void

    :cond_1
    move v3, v10

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v11

    int-to-float v4, v11

    goto :goto_1

    :cond_3
    cmpl-float v11, v1, v3

    if-lez v11, :cond_4

    sub-float v8, v3, v1

    goto :goto_2

    :cond_4
    cmpg-float v11, v6, v4

    if-gez v11, :cond_5

    sub-float v8, v4, v6

    goto :goto_2

    :cond_5
    const/4 v8, 0x0

    goto :goto_2

    :cond_6
    move v5, v10

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_4

    :cond_8
    cmpl-float v10, v7, v5

    if-lez v10, :cond_9

    sub-float v9, v5, v7

    goto :goto_5

    :cond_9
    cmpg-float v10, v0, v2

    if-gez v10, :cond_a

    sub-float v9, v2, v0

    goto :goto_5

    :cond_a
    const/4 v9, 0x0

    goto :goto_5

    :cond_b
    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->invalidate()V

    goto :goto_6
.end method

.method private translate(FF)Z
    .locals 12
    .param p1    # F
    .param p2    # F

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v10, v11}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-boolean v10, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v3, v10

    :goto_0
    iget-boolean v10, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    int-to-float v4, v10

    :goto_1
    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v1, v10, Landroid/graphics/RectF;->left:F

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v10, Landroid/graphics/RectF;->right:F

    iget-boolean v10, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    sub-float v10, v3, v10

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    sub-float v11, v4, v11

    invoke-static {v11, p1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v8

    :goto_2
    iget-boolean v10, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v5, v10

    :goto_3
    iget-boolean v10, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v10

    :goto_4
    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v10, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    iget-boolean v10, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    sub-float v10, v5, v10

    iget-object v11, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    sub-float v11, v2, v11

    invoke-static {v11, p2}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v9

    :goto_5
    iget-object v10, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->invalidate()V

    cmpl-float v10, v8, p1

    if-nez v10, :cond_8

    cmpl-float v10, v9, p2

    if-nez v10, :cond_8

    const/4 v10, 0x1

    :goto_6
    return v10

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v10

    int-to-float v4, v10

    goto :goto_1

    :cond_2
    sub-float v10, v6, v1

    sub-float v11, v4, v3

    cmpg-float v10, v10, v11

    if-gez v10, :cond_3

    sub-float v10, v4, v3

    add-float v11, v6, v1

    sub-float/2addr v10, v11

    const/high16 v11, 0x40000000

    div-float/2addr v10, v11

    add-float v8, v3, v10

    goto :goto_2

    :cond_3
    sub-float v10, v4, v6

    sub-float v11, v3, v1

    invoke-static {v11, p1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v8

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_4

    :cond_6
    sub-float v10, v0, v7

    sub-float v11, v2, v5

    cmpg-float v10, v10, v11

    if-gez v10, :cond_7

    sub-float v10, v2, v5

    add-float v11, v0, v7

    sub-float/2addr v10, v11

    const/high16 v11, 0x40000000

    div-float/2addr v10, v11

    add-float v9, v5, v10

    goto :goto_5

    :cond_7
    sub-float v10, v2, v0

    sub-float v11, v5, v7

    invoke-static {v11, p2}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v9

    goto :goto_5

    :cond_8
    const/4 v10, 0x0

    goto :goto_6
.end method


# virtual methods
.method public bindPhoto(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-ne p1, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_4

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v2, v3, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/ex/photo/views/PhotoView;->mMinScale:F

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    :cond_2
    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v2, :cond_3

    if-eqz p1, :cond_3

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    :cond_3
    invoke-direct {p0, v0}, Lcom/android/ex/photo/views/PhotoView;->configureBounds(Z)V

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->invalidate()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public clear()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    iput-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    iput-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleRunnable:Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;->stop()V

    iput-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleRunnable:Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->stop()V

    iput-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mSnapRunnable:Lcom/android/ex/photo/views/PhotoView$SnapRunnable;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView$SnapRunnable;->stop()V

    iput-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mSnapRunnable:Lcom/android/ex/photo/views/PhotoView$SnapRunnable;

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mRotateRunnable:Lcom/android/ex/photo/views/PhotoView$RotateRunnable;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView$RotateRunnable;->stop()V

    iput-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mRotateRunnable:Lcom/android/ex/photo/views/PhotoView$RotateRunnable;

    invoke-virtual {p0, v1}, Lcom/android/ex/photo/views/PhotoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public enableImageTransforms(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->resetTransformations()V

    :cond_0
    return-void
.end method

.method public interceptMoveLeft(FF)Z
    .locals 7
    .param p1    # F
    .param p2    # F

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    # getter for: Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->mRunning:Z
    invoke-static {v5}, Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->access$000(Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v5

    int-to-float v2, v5

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    const/4 v6, 0x2

    aget v1, v5, v6

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    sub-float v0, v5, v6

    iget-boolean v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v5, :cond_0

    cmpg-float v5, v0, v2

    if-lez v5, :cond_0

    const/4 v5, 0x0

    cmpl-float v5, v1, v5

    if-eqz v5, :cond_0

    add-float v3, v0, v1

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_3

    move v3, v4

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_0
.end method

.method public interceptMoveRight(FF)Z
    .locals 7
    .param p1    # F
    .param p2    # F

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    # getter for: Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->mRunning:Z
    invoke-static {v5}, Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->access$000(Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v5

    int-to-float v2, v5

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mValues:[F

    const/4 v6, 0x2

    aget v1, v5, v6

    iget-object v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    sub-float v0, v5, v6

    iget-boolean v5, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v5, :cond_0

    cmpg-float v5, v0, v2

    if-lez v5, :cond_0

    const/4 v5, 0x0

    cmpl-float v5, v1, v5

    if-nez v5, :cond_3

    move v3, v4

    goto :goto_0

    :cond_3
    add-float v5, v0, v1

    cmpl-float v5, v2, v5

    if-gez v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method public isPhotoBound()Z
    .locals 1

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDoubleTapDebounce:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->getScale()F

    move-result v0

    const/high16 v2, 0x3fc00000

    mul-float v1, v0, v2

    iget v2, p0, Lcom/android/ex/photo/views/PhotoView;->mMinScale:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, p0, Lcom/android/ex/photo/views/PhotoView;->mMaxScale:F

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleRunnable:Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;->start(FFFF)Z

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDoubleTapDebounce:Z

    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->stop()V

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mSnapRunnable:Lcom/android/ex/photo/views/PhotoView$SnapRunnable;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView$SnapRunnable;->stop()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v9

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_0
    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mVideoBlob:[B

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mVideoReady:Z

    if-eqz v0, :cond_5

    sget-object v10, Lcom/android/ex/photo/views/PhotoView;->sVideoImage:Landroid/graphics/Bitmap;

    :goto_0
    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v6, v0, 0x2

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v0

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v7, v0, 0x2

    int-to-float v0, v6

    int-to-float v2, v7

    const/4 v3, 0x0

    invoke-virtual {p1, v10, v0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    :cond_2
    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/android/ex/photo/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_3
    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->restoreToCount(I)V

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/android/ex/photo/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_4
    return-void

    :cond_5
    sget-object v10, Lcom/android/ex/photo/views/PhotoView;->sVideoNotReadyImage:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    invoke-virtual {v0, p3, p4}, Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->start(FF)Z

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/ex/photo/views/PhotoView;->mHaveLayout:Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getHeight()I

    move-result v4

    iget-boolean v6, p0, Lcom/android/ex/photo/views/PhotoView;->mAllowCrop:Z

    if-eqz v6, :cond_0

    sget v6, Lcom/android/ex/photo/views/PhotoView;->sCropSize:I

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    iput v6, p0, Lcom/android/ex/photo/views/PhotoView;->mCropSize:I

    iget v6, p0, Lcom/android/ex/photo/views/PhotoView;->mCropSize:I

    sub-int v6, v5, v6

    div-int/lit8 v1, v6, 0x2

    iget v6, p0, Lcom/android/ex/photo/views/PhotoView;->mCropSize:I

    sub-int v6, v4, v6

    div-int/lit8 v3, v6, 0x2

    iget v6, p0, Lcom/android/ex/photo/views/PhotoView;->mCropSize:I

    add-int v2, v1, v6

    iget v6, p0, Lcom/android/ex/photo/views/PhotoView;->mCropSize:I

    add-int v0, v3, v6

    iget-object v6, p0, Lcom/android/ex/photo/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v1, v3, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/ex/photo/views/PhotoView;->configureBounds(Z)V

    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/android/ex/photo/views/PhotoView;->mFixedHeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/ex/photo/views/PhotoView;->mFixedHeight:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/android/ex/photo/views/PhotoView;->mFixedHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/android/ex/photo/views/PhotoView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1    # Landroid/view/ScaleGestureDetector;

    iget-boolean v2, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/ex/photo/views/PhotoView;->mIsDoubleTouch:Z

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->getScale()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    mul-float v1, v0, v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/android/ex/photo/views/PhotoView;->scale(FFF)V

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleRunnable:Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/android/ex/photo/views/PhotoView$ScaleRunnable;->stop()V

    iput-boolean v1, p0, Lcom/android/ex/photo/views/PhotoView;->mIsDoubleTouch:Z

    :cond_0
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 1
    .param p1    # Landroid/view/ScaleGestureDetector;

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mIsDoubleTouch:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mDoubleTapDebounce:Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->resetTransformations()V

    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    neg-float v0, p3

    neg-float v1, p4

    invoke-direct {p0, v0, v1}, Lcom/android/ex/photo/views/PhotoView;->translate(FF)Z

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mIsDoubleTouch:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mIsDoubleTouch:Z

    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v1, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mTranslateRunnable:Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;

    # getter for: Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->mRunning:Z
    invoke-static {v1}, Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;->access$000(Lcom/android/ex/photo/views/PhotoView$TranslateRunnable;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/ex/photo/views/PhotoView;->snap()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public resetTransformations()V
    .locals 2

    iget-object v0, p0, Lcom/android/ex/photo/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/ex/photo/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->invalidate()V

    return-void
.end method

.method public setFullScreen(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    iget-boolean v0, p0, Lcom/android/ex/photo/views/PhotoView;->mFullScreen:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/android/ex/photo/views/PhotoView;->mFullScreen:Z

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->requestLayout()V

    invoke-virtual {p0}, Lcom/android/ex/photo/views/PhotoView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setMaxInitialScale(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/android/ex/photo/views/PhotoView;->mMaxInitialScaleFactor:F

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/ex/photo/views/PhotoView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method
