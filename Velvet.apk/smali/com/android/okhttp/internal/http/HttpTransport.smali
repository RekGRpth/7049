.class public final Lcom/android/okhttp/internal/http/HttpTransport;
.super Ljava/lang/Object;
.source "HttpTransport.java"

# interfaces
.implements Lcom/android/okhttp/internal/http/Transport;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/okhttp/internal/http/HttpTransport$1;,
        Lcom/android/okhttp/internal/http/HttpTransport$ChunkedInputStream;,
        Lcom/android/okhttp/internal/http/HttpTransport$FixedLengthInputStream;,
        Lcom/android/okhttp/internal/http/HttpTransport$ChunkedOutputStream;,
        Lcom/android/okhttp/internal/http/HttpTransport$FixedLengthOutputStream;
    }
.end annotation


# instance fields
.field private final httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

.field private requestOut:Ljava/io/OutputStream;

.field private final socketIn:Ljava/io/InputStream;

.field private final socketOut:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/HttpEngine;Ljava/io/OutputStream;Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Lcom/android/okhttp/internal/http/HttpEngine;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iput-object p2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketOut:Ljava/io/OutputStream;

    iput-object p2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->requestOut:Ljava/io/OutputStream;

    iput-object p3, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketIn:Ljava/io/InputStream;

    return-void
.end method

.method static synthetic access$200(Lcom/android/okhttp/internal/http/HttpEngine;Ljava/io/InputStream;)Z
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/http/HttpEngine;
    .param p1    # Ljava/io/InputStream;

    invoke-static {p0, p1}, Lcom/android/okhttp/internal/http/HttpTransport;->discardStream(Lcom/android/okhttp/internal/http/HttpEngine;Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/android/okhttp/internal/http/HttpTransport;)Lcom/android/okhttp/internal/http/HttpEngine;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/http/HttpTransport;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/okhttp/internal/http/HttpTransport;)Ljava/io/InputStream;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/http/HttpTransport;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketIn:Ljava/io/InputStream;

    return-object v0
.end method

.method private static discardStream(Lcom/android/okhttp/internal/http/HttpEngine;Ljava/io/InputStream;)Z
    .locals 6
    .param p0    # Lcom/android/okhttp/internal/http/HttpEngine;
    .param p1    # Ljava/io/InputStream;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I

    move-result v3

    const/16 v5, 0x64

    invoke-virtual {v2, v5}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {p1}, Lcom/android/okhttp/internal/Util;->skipAll(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v5, 0x1

    :try_start_2
    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    move v4, v5

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public createRequestBody()Ljava/io/OutputStream;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, -0x1

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/RequestHeaders;->isChunked()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getChunkLength()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/RequestHeaders;->setChunked()V

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_2

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getChunkLength()I

    move-result v0

    if-ne v0, v5, :cond_1

    const/16 v0, 0x400

    :cond_1
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpTransport;->writeRequestHeaders()V

    new-instance v4, Lcom/android/okhttp/internal/http/HttpTransport$ChunkedOutputStream;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpTransport;->requestOut:Ljava/io/OutputStream;

    invoke-direct {v4, v5, v0, v6}, Lcom/android/okhttp/internal/http/HttpTransport$ChunkedOutputStream;-><init>(Ljava/io/OutputStream;ILcom/android/okhttp/internal/http/HttpTransport$1;)V

    :goto_0
    return-object v4

    :cond_2
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getFixedContentLength()I

    move-result v3

    if-eq v3, v5, :cond_3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v4, v3}, Lcom/android/okhttp/internal/http/RequestHeaders;->setContentLength(I)V

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpTransport;->writeRequestHeaders()V

    new-instance v4, Lcom/android/okhttp/internal/http/HttpTransport$FixedLengthOutputStream;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpTransport;->requestOut:Ljava/io/OutputStream;

    invoke-direct {v4, v5, v3, v6}, Lcom/android/okhttp/internal/http/HttpTransport$FixedLengthOutputStream;-><init>(Ljava/io/OutputStream;ILcom/android/okhttp/internal/http/HttpTransport$1;)V

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/RequestHeaders;->getContentLength()I

    move-result v2

    if-eq v2, v5, :cond_4

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpTransport;->writeRequestHeaders()V

    new-instance v4, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    invoke-direct {v4, v2}, Lcom/android/okhttp/internal/http/RetryableOutputStream;-><init>(I)V

    goto :goto_0

    :cond_4
    new-instance v4, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    invoke-direct {v4}, Lcom/android/okhttp/internal/http/RetryableOutputStream;-><init>()V

    goto :goto_0
.end method

.method public flushRequest()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->requestOut:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketOut:Ljava/io/OutputStream;

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->requestOut:Ljava/io/OutputStream;

    return-void
.end method

.method public getTransferStream(Ljava/net/CacheRequest;)Ljava/io/InputStream;
    .locals 4
    .param p1    # Ljava/net/CacheRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponseBody()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/okhttp/internal/http/HttpTransport$FixedLengthInputStream;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketIn:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/android/okhttp/internal/http/HttpTransport$FixedLengthInputStream;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lcom/android/okhttp/internal/http/HttpEngine;I)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;->isChunked()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/okhttp/internal/http/HttpTransport$ChunkedInputStream;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketIn:Ljava/io/InputStream;

    invoke-direct {v0, v1, p1, p0}, Lcom/android/okhttp/internal/http/HttpTransport$ChunkedInputStream;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lcom/android/okhttp/internal/http/HttpTransport;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getContentLength()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    new-instance v0, Lcom/android/okhttp/internal/http/HttpTransport$FixedLengthInputStream;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketIn:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v3, v3, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getContentLength()I

    move-result v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/android/okhttp/internal/http/HttpTransport$FixedLengthInputStream;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lcom/android/okhttp/internal/http/HttpEngine;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/okhttp/internal/http/UnknownLengthHttpInputStream;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketIn:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-direct {v0, v1, p1, v2}, Lcom/android/okhttp/internal/http/UnknownLengthHttpInputStream;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lcom/android/okhttp/internal/http/HttpEngine;)V

    goto :goto_0
.end method

.method public makeReusable(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z
    .locals 2
    .param p1    # Z
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/io/InputStream;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p2, :cond_2

    check-cast p2, Lcom/android/okhttp/internal/http/AbstractHttpOutputStream;

    iget-boolean v1, p2, Lcom/android/okhttp/internal/http/AbstractHttpOutputStream;->closed:Z

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->hasConnectionClose()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->responseHeaders:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->hasConnectionClose()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    instance-of v1, p3, Lcom/android/okhttp/internal/http/UnknownLengthHttpInputStream;

    if-nez v1, :cond_0

    if-eqz p3, :cond_4

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-static {v0, p3}, Lcom/android/okhttp/internal/http/HttpTransport;->discardStream(Lcom/android/okhttp/internal/http/HttpEngine;Ljava/io/InputStream;)Z

    move-result v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public readResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketIn:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/android/okhttp/internal/http/RawHeaders;->fromBytes(Ljava/io/InputStream;)Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RawHeaders;->getHttpMinorVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/Connection;->setHttpMinorVersion(I)V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v1, v0}, Lcom/android/okhttp/internal/http/HttpEngine;->receiveHeaders(Lcom/android/okhttp/internal/http/RawHeaders;)V

    new-instance v1, Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v2, v2, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-direct {v1, v2, v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;-><init>(Ljava/net/URI;Lcom/android/okhttp/internal/http/RawHeaders;)V

    return-object v1
.end method

.method public writeRequestBody(Lcom/android/okhttp/internal/http/RetryableOutputStream;)V
    .locals 1
    .param p1    # Lcom/android/okhttp/internal/http/RetryableOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->requestOut:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lcom/android/okhttp/internal/http/RetryableOutputStream;->writeToSocket(Ljava/io/OutputStream;)V

    return-void
.end method

.method public writeRequestHeaders()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/HttpEngine;->writingRequestHeaders()V

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v3, v3, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/RequestHeaders;->getContentLength()I

    move-result v1

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v3, v3, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/RequestHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/RawHeaders;->toBytes()[B

    move-result-object v0

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    array-length v3, v0

    add-int/2addr v3, v1

    const v4, 0x8000

    if-gt v3, v4, :cond_0

    new-instance v3, Ljava/io/BufferedOutputStream;

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpTransport;->socketOut:Ljava/io/OutputStream;

    array-length v5, v0

    add-int/2addr v5, v1

    invoke-direct {v3, v4, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/HttpTransport;->requestOut:Ljava/io/OutputStream;

    :cond_0
    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpTransport;->requestOut:Ljava/io/OutputStream;

    invoke-virtual {v3, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method
