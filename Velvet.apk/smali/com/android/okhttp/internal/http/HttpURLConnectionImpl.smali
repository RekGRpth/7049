.class public Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;
.super Ljava/net/HttpURLConnection;
.source "HttpURLConnectionImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;
    }
.end annotation


# instance fields
.field final connectionPool:Lcom/android/okhttp/ConnectionPool;

.field final cookieHandler:Ljava/net/CookieHandler;

.field private final followProtocolRedirects:Z

.field hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

.field protected httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

.field protected httpEngineFailure:Ljava/io/IOException;

.field final proxySelector:Ljava/net/ProxySelector;

.field private final rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

.field private redirectionCount:I

.field final requestedProxy:Ljava/net/Proxy;

.field final responseCache:Ljava/net/ResponseCache;

.field sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lcom/android/okhttp/OkHttpClient;)V
    .locals 1
    .param p1    # Ljava/net/URL;
    .param p2    # Lcom/android/okhttp/OkHttpClient;

    invoke-direct {p0, p1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    new-instance v0, Lcom/android/okhttp/internal/http/RawHeaders;

    invoke-direct {v0}, Lcom/android/okhttp/internal/http/RawHeaders;-><init>()V

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    invoke-virtual {p2}, Lcom/android/okhttp/OkHttpClient;->getFollowProtocolRedirects()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->followProtocolRedirects:Z

    invoke-virtual {p2}, Lcom/android/okhttp/OkHttpClient;->getProxy()Ljava/net/Proxy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->requestedProxy:Ljava/net/Proxy;

    invoke-virtual {p2}, Lcom/android/okhttp/OkHttpClient;->getProxySelector()Ljava/net/ProxySelector;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->proxySelector:Ljava/net/ProxySelector;

    invoke-virtual {p2}, Lcom/android/okhttp/OkHttpClient;->getCookieHandler()Ljava/net/CookieHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->cookieHandler:Ljava/net/CookieHandler;

    invoke-virtual {p2}, Lcom/android/okhttp/OkHttpClient;->getResponseCache()Ljava/net/ResponseCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->responseCache:Ljava/net/ResponseCache;

    invoke-virtual {p2}, Lcom/android/okhttp/OkHttpClient;->getConnectionPool()Lcom/android/okhttp/ConnectionPool;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connectionPool:Lcom/android/okhttp/ConnectionPool;

    invoke-virtual {p2}, Lcom/android/okhttp/OkHttpClient;->getSslSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {p2}, Lcom/android/okhttp/OkHttpClient;->getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    return-void
.end method

.method private execute(Z)Z
    .locals 6
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    :try_start_0
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/HttpEngine;->sendRequest()V

    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/HttpEngine;->readResponse()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v3

    :catch_0
    move-exception v0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v2, v4, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v2, v4, v0}, Lcom/android/okhttp/internal/http/RouteSelector;->connectFailed(Lcom/android/okhttp/Connection;Ljava/io/IOException;)V

    :cond_1
    if-nez v2, :cond_2

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-nez v4, :cond_2

    throw v0

    :cond_2
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequestBody()Ljava/io/OutputStream;

    move-result-object v1

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/RouteSelector;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_3
    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->isRecoverable(Ljava/io/IOException;)Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz v1, :cond_4

    instance-of v4, v1, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    if-eqz v4, :cond_5

    :cond_4
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v4, v3}, Lcom/android/okhttp/internal/http/HttpEngine;->release(Z)V

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    const/4 v5, 0x0

    check-cast v1, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    invoke-direct {p0, v3, v4, v5, v1}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->newHttpEngine(Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iput-object v2, v3, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    const/4 v3, 0x0

    goto :goto_0

    :cond_5
    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngineFailure:Ljava/io/IOException;

    throw v0
.end method

.method private getResponse()Lcom/android/okhttp/internal/http/HttpEngine;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->initHttpEngine()V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponse()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    :goto_0
    return-object v0

    :cond_0
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->execute(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->processResponseHeaders()Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    move-result-object v2

    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->NONE:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    if-ne v2, v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->automaticallyReleaseConnectionToPool()V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequestBody()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponseCode()I

    move-result v3

    const/16 v4, 0x12c

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12d

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12e

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12f

    if-ne v3, v4, :cond_3

    :cond_2
    const-string v1, "GET"

    const/4 v0, 0x0

    :cond_3
    if-eqz v0, :cond_4

    instance-of v3, v0, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    if-nez v3, :cond_4

    new-instance v0, Ljava/net/HttpRetryException;

    const-string v1, "Cannot retry streamed HTTP body"

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseCode()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/net/HttpRetryException;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_4
    sget-object v3, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->DIFFERENT_CONNECTION:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpEngine;->automaticallyReleaseConnectionToPool()V

    :cond_5
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/HttpEngine;->release(Z)V

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v3

    check-cast v0, Lcom/android/okhttp/internal/http/RetryableOutputStream;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->newHttpEngine(Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    goto :goto_1
.end method

.method private initHttpEngine()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngineFailure:Ljava/io/IOException;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngineFailure:Ljava/io/IOException;

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connected:Z

    :try_start_0
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->doOutput:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "POST"

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->newHttpEngine(Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngineFailure:Ljava/io/IOException;

    throw v0

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support writing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private isRecoverable(Ljava/io/IOException;)Z
    .locals 5
    .param p1    # Ljava/io/IOException;

    const/4 v2, 0x1

    const/4 v3, 0x0

    instance-of v4, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    instance-of v4, v4, Ljava/security/cert/CertificateException;

    if-eqz v4, :cond_0

    move v1, v2

    :goto_0
    instance-of v0, p1, Ljava/net/ProtocolException;

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    :goto_1
    return v2

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method private newHttpEngine(Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)Lcom/android/okhttp/internal/http/HttpEngine;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/okhttp/internal/http/HttpEngine;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/okhttp/internal/http/HttpEngine;-><init>(Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;-><init>(Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private processResponseHeaders()Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v0, v0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->getProxy()Ljava/net/Proxy;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponseCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->NONE:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->requestedProxy:Ljava/net/Proxy;

    goto :goto_0

    :sswitch_0
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v1, v2, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponseCode()I

    move-result v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/android/okhttp/internal/http/HttpAuthenticator;->processAuthHeader(ILcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/internal/http/RawHeaders;Ljava/net/Proxy;Ljava/net/URL;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->SAME_CONNECTION:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->NONE:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getInstanceFollowRedirects()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->NONE:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->redirectionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->redirectionCount:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_4

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Too many redirects: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->redirectionCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const-string v0, "Location"

    invoke-virtual {p0, v0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->NONE:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1, v0}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->NONE:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->followProtocolRedirects:Z

    if-nez v0, :cond_7

    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->NONE:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v1}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URL;)I

    move-result v0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-static {v1}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URL;)I

    move-result v1

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_2
    if-eqz v3, :cond_9

    if-eqz v0, :cond_9

    if-eqz v2, :cond_9

    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->SAME_CONNECTION:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    :cond_9
    sget-object v0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;->DIFFERENT_CONNECTION:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x12e -> :sswitch_2
        0x12f -> :sswitch_2
        0x191 -> :sswitch_1
        0x197 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    invoke-virtual {v0, p1, p2}, Lcom/android/okhttp/internal/http/RawHeaders;->add(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final connect()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->initHttpEngine()V

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->execute(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method public final disconnect()V
    .locals 2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponse()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseBody()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/HttpEngine;->release(Z)V

    :cond_1
    return-void
.end method

.method final getChunkLength()I
    .locals 1

    iget v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->chunkLength:I

    return v0
.end method

.method public final getErrorStream()Ljava/io/InputStream;
    .locals 5

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponse()Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponseBody()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseCode()I

    move-result v3

    const/16 v4, 0x190

    if-lt v3, v4, :cond_0

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseBody()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_0
    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method final getFixedContentLength()I
    .locals 1

    iget v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->fixedContentLength:I

    return v0
.end method

.method public final getHeaderField(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    :try_start_0
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponse()Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/okhttp/internal/http/RawHeaders;->getValue(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponse()Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v1

    if-nez p1, :cond_0

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/RawHeaders;->getStatusLine()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v1, p1}, Lcom/android/okhttp/internal/http/RawHeaders;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final getHeaderFieldKey(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    :try_start_0
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponse()Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/okhttp/internal/http/RawHeaders;->getFieldName(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getHeaderFields()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponse()Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/RawHeaders;->toMultimap(Z)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getHttpConnectionToCache()Ljava/net/HttpURLConnection;
    .locals 0

    return-object p0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->doInput:Z

    if-nez v2, :cond_0

    new-instance v2, Ljava/net/ProtocolException;

    const-string v3, "This protocol does not support input"

    invoke-direct {v2, v3}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponse()Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponseCode()I

    move-result v2

    const/16 v3, 0x190

    if-lt v2, v3, :cond_1

    new-instance v2, Ljava/io/FileNotFoundException;

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v3}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseBody()Ljava/io/InputStream;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v2, Ljava/net/ProtocolException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No response body exists; responseCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponseCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    return-object v1
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connect()V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequestBody()Ljava/io/OutputStream;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "method does not support a request body: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponse()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "cannot write request body after response has been read"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public final getPermission()Ljava/security/Permission;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URL;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->usingProxy()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->requestedProxy:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    :cond_0
    new-instance v2, Ljava/net/SocketPermission;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "connect, resolve"

    invoke-direct {v2, v0, v1}, Ljava/net/SocketPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public final getRequestProperties()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot access request header fields after connection is set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/RawHeaders;->toMultimap(Z)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    invoke-virtual {v0, p1}, Lcom/android/okhttp/internal/http/RawHeaders;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getResponseCode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponse()Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseCode()I

    move-result v0

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getResponse()Lcom/android/okhttp/internal/http/HttpEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/ResponseHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RawHeaders;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->rawRequestHeaders:Lcom/android/okhttp/internal/http/RawHeaders;

    invoke-virtual {v0, p1, p2}, Lcom/android/okhttp/internal/http/RawHeaders;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final usingProxy()Z
    .locals 2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->requestedProxy:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->requestedProxy:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
