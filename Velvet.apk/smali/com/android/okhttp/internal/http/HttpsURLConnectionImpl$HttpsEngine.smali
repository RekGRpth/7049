.class public final Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;
.super Lcom/android/okhttp/internal/http/HttpEngine;
.source "HttpsURLConnectionImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HttpsEngine"
.end annotation


# instance fields
.field private sslSocket:Ljavax/net/ssl/SSLSocket;


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)V
    .locals 1
    .param p1    # Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/okhttp/internal/http/RawHeaders;
    .param p4    # Lcom/android/okhttp/Connection;
    .param p5    # Lcom/android/okhttp/internal/http/RetryableOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Lcom/android/okhttp/internal/http/HttpEngine;-><init>(Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;Ljava/lang/String;Lcom/android/okhttp/internal/http/RawHeaders;Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RetryableOutputStream;)V

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    :goto_0
    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;->sslSocket:Ljavax/net/ssl/SSLSocket;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;)Ljavax/net/ssl/SSLSocket;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;->sslSocket:Ljavax/net/ssl/SSLSocket;

    return-object v0
.end method


# virtual methods
.method protected acceptCacheResponseType(Ljava/net/CacheResponse;)Z
    .locals 1
    .param p1    # Ljava/net/CacheResponse;

    instance-of v0, p1, Ljava/net/SecureCacheResponse;

    return v0
.end method

.method protected connected(Lcom/android/okhttp/Connection;)V
    .locals 1
    .param p1    # Lcom/android/okhttp/Connection;

    invoke-virtual {p1}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;->sslSocket:Ljavax/net/ssl/SSLSocket;

    return-void
.end method

.method protected getTunnelConfig()Lcom/android/okhttp/TunnelRequest;
    .locals 6

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/RequestHeaders;->getUserAgent()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v0

    new-instance v2, Lcom/android/okhttp/TunnelRequest;

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URL;)I

    move-result v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpsURLConnectionImpl$HttpsEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v5}, Lcom/android/okhttp/internal/http/RequestHeaders;->getProxyAuthorization()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v1, v5}, Lcom/android/okhttp/TunnelRequest;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method protected includeAuthorityInRequestLine()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
