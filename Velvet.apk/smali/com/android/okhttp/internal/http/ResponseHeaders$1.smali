.class Lcom/android/okhttp/internal/http/ResponseHeaders$1;
.super Ljava/lang/Object;
.source "ResponseHeaders.java"

# interfaces
.implements Lcom/android/okhttp/internal/http/HeaderParser$CacheControlHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/okhttp/internal/http/ResponseHeaders;-><init>(Ljava/net/URI;Lcom/android/okhttp/internal/http/RawHeaders;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/okhttp/internal/http/ResponseHeaders;


# direct methods
.method constructor <init>(Lcom/android/okhttp/internal/http/ResponseHeaders;)V
    .locals 0

    iput-object p1, p0, Lcom/android/okhttp/internal/http/ResponseHeaders$1;->this$0:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handle(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    const-string v0, "no-cache"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/ResponseHeaders$1;->this$0:Lcom/android/okhttp/internal/http/ResponseHeaders;

    # setter for: Lcom/android/okhttp/internal/http/ResponseHeaders;->noCache:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->access$002(Lcom/android/okhttp/internal/http/ResponseHeaders;Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "no-store"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/ResponseHeaders$1;->this$0:Lcom/android/okhttp/internal/http/ResponseHeaders;

    # setter for: Lcom/android/okhttp/internal/http/ResponseHeaders;->noStore:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->access$102(Lcom/android/okhttp/internal/http/ResponseHeaders;Z)Z

    goto :goto_0

    :cond_2
    const-string v0, "max-age"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/okhttp/internal/http/ResponseHeaders$1;->this$0:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-static {p2}, Lcom/android/okhttp/internal/http/HeaderParser;->parseSeconds(Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/android/okhttp/internal/http/ResponseHeaders;->maxAgeSeconds:I
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->access$202(Lcom/android/okhttp/internal/http/ResponseHeaders;I)I

    goto :goto_0

    :cond_3
    const-string v0, "s-maxage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/okhttp/internal/http/ResponseHeaders$1;->this$0:Lcom/android/okhttp/internal/http/ResponseHeaders;

    invoke-static {p2}, Lcom/android/okhttp/internal/http/HeaderParser;->parseSeconds(Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/android/okhttp/internal/http/ResponseHeaders;->sMaxAgeSeconds:I
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->access$302(Lcom/android/okhttp/internal/http/ResponseHeaders;I)I

    goto :goto_0

    :cond_4
    const-string v0, "public"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/okhttp/internal/http/ResponseHeaders$1;->this$0:Lcom/android/okhttp/internal/http/ResponseHeaders;

    # setter for: Lcom/android/okhttp/internal/http/ResponseHeaders;->isPublic:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->access$402(Lcom/android/okhttp/internal/http/ResponseHeaders;Z)Z

    goto :goto_0

    :cond_5
    const-string v0, "must-revalidate"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/ResponseHeaders$1;->this$0:Lcom/android/okhttp/internal/http/ResponseHeaders;

    # setter for: Lcom/android/okhttp/internal/http/ResponseHeaders;->mustRevalidate:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;->access$502(Lcom/android/okhttp/internal/http/ResponseHeaders;Z)Z

    goto :goto_0
.end method
