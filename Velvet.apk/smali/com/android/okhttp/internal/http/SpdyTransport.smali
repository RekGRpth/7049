.class public final Lcom/android/okhttp/internal/http/SpdyTransport;
.super Ljava/lang/Object;
.source "SpdyTransport.java"

# interfaces
.implements Lcom/android/okhttp/internal/http/Transport;


# instance fields
.field private final httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

.field private final spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

.field private stream:Lcom/android/okhttp/internal/spdy/SpdyStream;


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/HttpEngine;Lcom/android/okhttp/internal/spdy/SpdyConnection;)V
    .locals 0
    .param p1    # Lcom/android/okhttp/internal/http/HttpEngine;
    .param p2    # Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iput-object p2, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    return-void
.end method


# virtual methods
.method public createRequestBody()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/SpdyTransport;->writeRequestHeaders()V

    iget-object v0, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public flushRequest()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyStream;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    return-void
.end method

.method public getTransferStream(Ljava/net/CacheRequest;)Ljava/io/InputStream;
    .locals 3
    .param p1    # Ljava/net/CacheRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/android/okhttp/internal/http/UnknownLengthHttpInputStream;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/spdy/SpdyStream;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-direct {v0, v1, p1, v2}, Lcom/android/okhttp/internal/http/UnknownLengthHttpInputStream;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lcom/android/okhttp/internal/http/HttpEngine;)V

    return-object v0
.end method

.method public makeReusable(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z
    .locals 3
    .param p1    # Z
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/io/InputStream;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/spdy/SpdyStream;->closeLater(I)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readResponseHeaders()Lcom/android/okhttp/internal/http/ResponseHeaders;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/spdy/SpdyStream;->getResponseHeaders()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/android/okhttp/internal/http/RawHeaders;->fromNameValueBlock(Ljava/util/List;)Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/RawHeaders;->computeResponseStatusLineFromSpdyHeaders()V

    iget-object v2, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v2, v1}, Lcom/android/okhttp/internal/http/HttpEngine;->receiveHeaders(Lcom/android/okhttp/internal/http/RawHeaders;)V

    new-instance v2, Lcom/android/okhttp/internal/http/ResponseHeaders;

    iget-object v3, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v3, v3, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-direct {v2, v3, v1}, Lcom/android/okhttp/internal/http/ResponseHeaders;-><init>(Ljava/net/URI;Lcom/android/okhttp/internal/http/RawHeaders;)V

    return-object v2
.end method

.method public writeRequestBody(Lcom/android/okhttp/internal/http/RetryableOutputStream;)V
    .locals 1
    .param p1    # Lcom/android/okhttp/internal/http/RetryableOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public writeRequestHeaders()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->writingRequestHeaders()V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->requestHeaders:Lcom/android/okhttp/internal/http/RequestHeaders;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/RequestHeaders;->getHeaders()Lcom/android/okhttp/internal/http/RawHeaders;

    move-result-object v0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v1}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const-string v3, "HTTP/1.1"

    :goto_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v8

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->method:Ljava/lang/String;

    invoke-static {v8}, Lcom/android/okhttp/internal/http/HttpEngine;->requestPath(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8}, Lcom/android/okhttp/internal/http/HttpEngine;->getOriginAddress(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/HttpEngine;->uri:Ljava/net/URI;

    invoke-virtual {v5}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/android/okhttp/internal/http/RawHeaders;->addSpdyRequestHeaders(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->hasRequestBody()Z

    move-result v6

    const/4 v7, 0x1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RawHeaders;->toNameValueBlock()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2, v6, v7}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->newStream(Ljava/util/List;ZZ)Lcom/android/okhttp/internal/spdy/SpdyStream;

    move-result-object v1

    iput-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->stream:Lcom/android/okhttp/internal/spdy/SpdyStream;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/SpdyTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v2, v2, Lcom/android/okhttp/internal/http/HttpEngine;->policy:Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpURLConnectionImpl;->getReadTimeout()I

    move-result v2

    int-to-long v4, v2

    invoke-virtual {v1, v4, v5}, Lcom/android/okhttp/internal/spdy/SpdyStream;->setReadTimeout(J)V

    goto :goto_0

    :cond_1
    const-string v3, "HTTP/1.0"

    goto :goto_1
.end method
