.class public Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;
.super Ljava/lang/Object;
.source "SpdyConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/spdy/SpdyConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public client:Z

.field private handler:Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;

.field private hostName:Ljava/lang/String;

.field private in:Ljava/io/InputStream;

.field private out:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/io/InputStream;
    .param p4    # Ljava/io/OutputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;->REFUSE_INCOMING_STREAMS:Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;

    iput-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->handler:Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;

    iput-object p1, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->hostName:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->client:Z

    iput-object p3, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->in:Ljava/io/InputStream;

    iput-object p4, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->out:Ljava/io/OutputStream;

    return-void
.end method

.method static synthetic access$000(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->handler:Lcom/android/okhttp/internal/spdy/IncomingStreamHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)Ljava/io/InputStream;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->in:Ljava/io/InputStream;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)Ljava/io/OutputStream;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->out:Ljava/io/OutputStream;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->hostName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/android/okhttp/internal/spdy/SpdyConnection;
    .locals 2

    new-instance v0, Lcom/android/okhttp/internal/spdy/SpdyConnection;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/okhttp/internal/spdy/SpdyConnection;-><init>(Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;Lcom/android/okhttp/internal/spdy/SpdyConnection$1;)V

    return-object v0
.end method
