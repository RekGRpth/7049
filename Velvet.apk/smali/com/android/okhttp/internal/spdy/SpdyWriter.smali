.class final Lcom/android/okhttp/internal/spdy/SpdyWriter;
.super Ljava/lang/Object;
.source "SpdyWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final nameValueBlockBuffer:Ljava/io/ByteArrayOutputStream;

.field private final nameValueBlockOut:Ljava/io/DataOutputStream;

.field final out:Ljava/io/DataOutputStream;


# direct methods
.method constructor <init>(Ljava/io/OutputStream;)V
    .locals 5
    .param p1    # Ljava/io/OutputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    new-instance v0, Ljava/util/zip/Deflater;

    invoke-direct {v0}, Ljava/util/zip/Deflater;-><init>()V

    sget-object v1, Lcom/android/okhttp/internal/spdy/SpdyReader;->DICTIONARY:[B

    invoke-virtual {v0, v1}, Ljava/util/zip/Deflater;->setDictionary([B)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockBuffer:Ljava/io/ByteArrayOutputStream;

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Lcom/android/okhttp/internal/Platform;->newDeflaterOutputStream(Ljava/io/OutputStream;Ljava/util/zip/Deflater;Z)Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockOut:Ljava/io/DataOutputStream;

    return-void
.end method

.method private writeNameValueBlockToBuffer(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    div-int/lit8 v1, v3, 0x2

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockOut:Ljava/io/DataOutputStream;

    invoke-virtual {v3, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockOut:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockOut:Ljava/io/DataOutputStream;

    const-string v4, "UTF-8"

    invoke-virtual {v2, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->write([B)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockOut:Ljava/io/DataOutputStream;

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->flush()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockOut:Ljava/io/DataOutputStream;

    invoke-static {v0, v1}, Lcom/android/okhttp/internal/Util;->closeAll(Ljava/io/Closeable;Ljava/io/Closeable;)V

    return-void
.end method

.method public declared-synchronized goAway(III)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    const/4 v1, 0x7

    const/16 v0, 0x8

    :try_start_0
    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    const v3, -0x7ffcfff9

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    and-int/lit16 v3, p1, 0xff

    shl-int/lit8 v3, v3, 0x18

    or-int/lit8 v3, v3, 0x8

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized ping(II)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    const/4 v1, 0x6

    const/4 v0, 0x4

    :try_start_0
    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    const v3, -0x7ffcfffa

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    and-int/lit16 v3, p1, 0xff

    shl-int/lit8 v3, v3, 0x18

    or-int/lit8 v3, v3, 0x4

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v2, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized rstStream(II)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    const/4 v2, 0x3

    const/16 v1, 0x8

    :try_start_0
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    const v4, -0x7ffcfffd

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    const v4, 0x7fffffff

    and-int/2addr v4, p1

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v3, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized synStream(IIIIILjava/util/List;)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const v6, 0x7fffffff

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p6}, Lcom/android/okhttp/internal/spdy/SpdyWriter;->writeNameValueBlockToBuffer(Ljava/util/List;)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v3

    add-int/lit8 v0, v3, 0xa

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    const v4, -0x7ffcffff

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    and-int/lit16 v4, p1, 0xff

    shl-int/lit8 v4, v4, 0x18

    const v5, 0xffffff

    and-int/2addr v5, v0

    or-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    and-int v4, p2, v6

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    and-int v4, p3, v6

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    and-int/lit8 v4, p4, 0x7

    shl-int/lit8 v4, v4, 0xd

    or-int/lit8 v4, v4, 0x0

    and-int/lit16 v5, p5, 0xff

    or-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->nameValueBlockBuffer:Ljava/io/ByteArrayOutputStream;

    iget-object v4, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized windowUpdate(II)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    const/16 v2, 0x9

    const/4 v0, 0x0

    const/16 v1, 0x8

    :try_start_0
    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    const v4, -0x7ffcfff7

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v3, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v3, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/android/okhttp/internal/spdy/SpdyWriter;->out:Ljava/io/DataOutputStream;

    invoke-virtual {v3}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
