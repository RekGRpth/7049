.class public abstract Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "AbstractPlaceEntryAdapter.java"


# instance fields
.field protected final mAlternateTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

.field protected final mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field protected final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

.field protected final mEntryLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field protected final mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

.field protected final mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

.field protected final mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

.field protected final mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;


# direct methods
.method protected constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;
    .param p6    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p7    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p7}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNearbyPlaceEntry()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "entry was expected to have frequent_place_entry or nearby_place_entry: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mEntryLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteCount()I

    move-result v2

    if-lez v2, :cond_3

    new-instance v1, Lcom/google/android/apps/sidekick/TravelReport$Builder;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-direct {v1, p3, v2}, Lcom/google/android/apps/sidekick/TravelReport$Builder;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRoute(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/TravelReport$Builder;->setRegularCommute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/android/apps/sidekick/TravelReport$Builder;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/TravelReport$Builder;->build()Lcom/google/android/apps/sidekick/TravelReport;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteCount()I

    move-result v2

    if-le v2, v5, :cond_2

    new-instance v0, Lcom/google/android/apps/sidekick/TravelReport$Builder;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-direct {v0, p3, v2}, Lcom/google/android/apps/sidekick/TravelReport$Builder;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v2, v5}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRoute(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/sidekick/TravelReport$Builder;->setRegularCommute(Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)Lcom/google/android/apps/sidekick/TravelReport$Builder;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/TravelReport$Builder;->build()Lcom/google/android/apps/sidekick/TravelReport;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mAlternateTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    :goto_1
    iput-object p5, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNearbyPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    goto :goto_0

    :cond_2
    iput-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mAlternateTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    goto :goto_1

    :cond_3
    iput-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    iput-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mAlternateTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    goto :goto_1
.end method

.method private static getEtaStringFromTravelReport(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;Z)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/sidekick/TravelReport;
    .param p2    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/TravelReport;->getTotalEtaMinutes()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p0, v1, p2}, Lcom/google/android/apps/sidekick/TimeUtilities;->getEtaString(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getNavigationButtonText(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/TravelReport;
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/TravelReport;->getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, p2, v3}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getEtaStringFromTravelReport(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, p3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1, p2, v3}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getEtaStringFromTravelReport(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, p4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static populateSampleCard(Landroid/view/View;)V
    .locals 11
    .param p0    # Landroid/view/View;

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f100031

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v4, 0x7f0d00f4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090038

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const v6, 0x7f0d01cb

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    const v6, 0x7f0d00ea

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f100027

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    const v4, 0x7f0d00f5

    new-array v5, v10, [Ljava/lang/Object;

    const v6, 0x7f0d0106

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const v6, 0x7f0d01cc

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f100028

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/StaticMapView;

    invoke-virtual {v1, v8}, Lcom/google/android/apps/sidekick/StaticMapView;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/StaticMapView;->showSampleRoute()V

    return-void
.end method

.method private populateView(Landroid/content/Context;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->updateTitle(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->updateContextMessage(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->updateTravelTime(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->updateMapOrImage(Landroid/content/Context;Landroid/view/View;)V

    return-object p2
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 3

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;

    invoke-direct {v2, p0}, Lcom/google/android/apps/sidekick/feedback/PlaceActionAdapter;-><init>(Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;)V

    return-object v0
.end method

.method public getConfirmationLabel(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormattedFullTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    if-eqz v0, :cond_0

    const v0, 0x7f0d00f4

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficColorForHtml(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getLongEtaString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public final getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    return-object v0
.end method

.method public final getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    return-object v0
.end method

.method public final getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLongEtaString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getEtaStringFromTravelReport(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRouteDescription(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/TravelReport;->getRouteDescriptionWithTraffic(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getShortEtaString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getEtaStringFromTravelReport(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 4

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/notifications/TrafficNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V

    return-object v0
.end method

.method protected getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-static {p1, v0}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTrafficColor(Landroid/content/Context;)I
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficColor(Landroid/content/Context;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getTravelReport()Lcom/google/android/apps/sidekick/TravelReport;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f040006

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->populateView(Landroid/content/Context;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f10005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRouteCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v0, v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRoute(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelMode()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v2, v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getRoute(I)Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v2

    invoke-direct {v0, p1, p0, v1, v2}, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/geo/sidekick/Sidekick$Location;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->run()V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->logDetailsInteraction(Landroid/content/Context;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->run()V

    goto :goto_0
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method protected mightShowNavigateButtonFor(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/TravelReport;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/TravelReport;->getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasShowNavigation()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/TravelReport;->getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getShowNavigation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-interface {v0, p2}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->checkNavigationAvailability(Lcom/google/android/apps/sidekick/TravelReport;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->updateActionButtons(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/TravelReport;->getTravelMode()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected shouldShowNavigation()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected shouldShowRoute()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected showMap(Landroid/content/Context;Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->shouldShowRoute()Z

    move-result v0

    const v2, 0x7f100028

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/StaticMapView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/StaticMapView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mEntryLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mStaticMapCache:Lcom/google/android/apps/sidekick/inject/StaticMapCache;

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/google/android/apps/sidekick/StaticMapView;->setLocations(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;ZLcom/google/android/apps/sidekick/inject/StaticMapCache;)V

    return-void
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected updateActionButtons(Landroid/app/Activity;Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mightShowNavigateButtonFor(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;)Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f10002c

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v4, 0x7f0d00db

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(I)V

    new-instance v4, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$1;

    invoke-direct {v4, p0, v3}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->shouldShowNavigation()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mAlternateTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mightShowNavigateButtonFor(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;)Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x7f10002d

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mAlternateTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    const v5, 0x7f0d010c

    const v6, 0x7f0d010b

    invoke-direct {p0, p1, v4, v5, v6}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getNavigationButtonText(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v4, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;

    invoke-direct {v4, p0, v3}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->shouldShowNavigation()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->mFrequentPlaceEntry:Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPhoneNumber()Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f100030

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v4, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$3;

    invoke-direct {v4, p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;Landroid/app/Activity;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-void
.end method

.method protected updateContextMessage(Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    return-void
.end method

.method protected updateMapOrImage(Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->showMap(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method protected updateTitle(Landroid/content/Context;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const v1, 0x7f100031

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getFormattedFullTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected updateTravelTime(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const v2, 0x7f100027

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getRouteDescription(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
