.class public Lcom/google/android/apps/sidekick/AlarmUtils;
.super Ljava/lang/Object;
.source "AlarmUtils.java"


# static fields
.field static final INTERVAL_TO_JITTER_DIVISOR:I = 0xf

.field static final MIN_JITTER_MILLIS:I = 0x7530

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mPreferencesController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private final mRandom:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/AlarmUtils;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/AlarmUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/GsaPreferenceController;Ljava/util/Random;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p4    # Ljava/util/Random;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mPreferencesController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mRandom:Ljava/util/Random;

    return-void
.end method

.method private static calculateMaxJitter(J)I
    .locals 2
    .param p0    # J

    const-wide/16 v0, 0xf

    div-long v0, p0, v0

    long-to-int v0, v0

    const/16 v1, 0x7530

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private getRandomPositiveJitterMillis(J)I
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mRandom:Ljava/util/Random;

    invoke-static {p1, p2}, Lcom/google/android/apps/sidekick/AlarmUtils;->calculateMaxJitter(J)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    return v0
.end method

.method private getRandomSignedJitterMillis(J)I
    .locals 3
    .param p1    # J

    invoke-static {p1, p2}, Lcom/google/android/apps/sidekick/AlarmUtils;->calculateMaxJitter(J)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mRandom:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    return v1
.end method

.method static getStartTimeKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AlarmStartTimeMillis_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private putLongSetting(Ljava/lang/String;J)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mPreferencesController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public cancel(Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/app/PendingIntent;
    .param p2    # Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/sidekick/AlarmUtils;->getStartTimeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mPreferencesController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/AlarmUtils;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v1, p1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public scheduleRepeatingAlarm(JLandroid/app/PendingIntent;Ljava/lang/String;Z)V
    .locals 19
    .param p1    # J
    .param p3    # Landroid/app/PendingIntent;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/AlarmUtils;->mPreferencesController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v9

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/sidekick/AlarmUtils;->getStartTimeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/AlarmUtils;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v10

    if-nez p5, :cond_0

    const-wide/16 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v9, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v17, 0x0

    cmp-long v2, v2, v17

    if-lez v2, :cond_0

    const-wide/16 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v9, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    cmp-long v2, v14, v10

    if-gez v2, :cond_2

    sub-long v2, v10, v14

    div-long v12, v2, p1

    const-wide/16 v2, 0x1

    add-long/2addr v2, v12

    mul-long v2, v2, p1

    add-long v4, v14, v2

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/sidekick/AlarmUtils;->putLongSetting(Ljava/lang/String;J)V

    :cond_0
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-nez v2, :cond_1

    add-long v2, v10, p1

    invoke-direct/range {p0 .. p2}, Lcom/google/android/apps/sidekick/AlarmUtils;->getRandomPositiveJitterMillis(J)I

    move-result v8

    int-to-long v0, v8

    move-wide/from16 v17, v0

    add-long v4, v2, v17

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/sidekick/AlarmUtils;->putLongSetting(Ljava/lang/String;J)V

    :cond_1
    invoke-direct/range {p0 .. p2}, Lcom/google/android/apps/sidekick/AlarmUtils;->getRandomSignedJitterMillis(J)I

    move-result v2

    int-to-long v2, v2

    add-long v6, p1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/AlarmUtils;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    const/4 v3, 0x2

    move-object/from16 v8, p3

    invoke-interface/range {v2 .. v8}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->setRepeating(IJJLandroid/app/PendingIntent;)V

    return-void

    :cond_2
    add-long v2, v10, p1

    cmp-long v2, v14, v2

    if-gez v2, :cond_0

    move-wide v4, v14

    goto :goto_0
.end method
