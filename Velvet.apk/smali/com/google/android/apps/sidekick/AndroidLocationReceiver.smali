.class public Lcom/google/android/apps/sidekick/AndroidLocationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AndroidLocationReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static createPendingIntent(Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)Landroid/app/PendingIntent;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.android_location_broadcast"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/apps/sidekick/AndroidLocationReceiver;

    invoke-interface {p0, v1}, Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;->createComponentName(Ljava/lang/Class;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-interface {p0, v2, v0, v2}, Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;->getBroadcast(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v2, "com.google.android.apps.sidekick.android_location_broadcast"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v1

    const-string v2, "location"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v2, v0, Landroid/location/Location;

    if-eqz v2, :cond_0

    instance-of v2, v1, Lcom/google/android/apps/sidekick/LocationOracleImpl;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/apps/sidekick/LocationOracleImpl;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->postAndroidLocation(Landroid/location/Location;)V

    :cond_0
    return-void
.end method
