.class Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;
.super Ljava/lang/Object;
.source "AsyncFileStorageImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WriteData"
.end annotation


# instance fields
.field private final mData:[B

.field private final mEncrypted:Z

.field private final mFilename:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;[BZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mFilename:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mData:[B

    iput-boolean p3, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mEncrypted:Z

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mEncrypted:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mFilename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;)[B
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;->mData:[B

    return-object v0
.end method
