.class public Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;
.super Ljava/lang/Object;
.source "AsyncFileStorageImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;,
        Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;,
        Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;,
        Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

.field private final mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/FileBytesReader;Lcom/google/android/apps/sidekick/FileBytesWriter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/FileBytesReader;
    .param p2    # Lcom/google/android/apps/sidekick/FileBytesWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;)Lcom/google/android/apps/sidekick/FileBytesReader;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;)Lcom/google/android/apps/sidekick/FileBytesWriter;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    return-object v0
.end method

.method private declared-synchronized getHandler()Landroid/os/Handler;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->TAG:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$AsyncFileStorageHandler;-><init>(Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mHandler:Landroid/os/Handler;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private readFromFileInternal(Ljava/lang/String;Lcom/google/common/base/Function;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$ReadData;-><init>(Ljava/lang/String;Lcom/google/common/base/Function;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method private updateFileInternal(Ljava/lang/String;Lcom/google/common/base/Function;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B[B>;Z)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$UpdateData;-><init>(Ljava/lang/String;Lcom/google/common/base/Function;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method private writeToFileInternal(Ljava/lang/String;[BZ)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [B
    .param p3    # Z

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl$WriteData;-><init>(Ljava/lang/String;[BZ)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method


# virtual methods
.method public deleteFile(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public readFromEncryptedFile(Ljava/lang/String;Lcom/google/common/base/Function;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->readFromFileInternal(Ljava/lang/String;Lcom/google/common/base/Function;Z)V

    return-void
.end method

.method public readFromFile(Ljava/lang/String;Lcom/google/common/base/Function;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->readFromFileInternal(Ljava/lang/String;Lcom/google/common/base/Function;Z)V

    return-void
.end method

.method public updateEncryptedFile(Ljava/lang/String;Lcom/google/common/base/Function;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B[B>;)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->updateFileInternal(Ljava/lang/String;Lcom/google/common/base/Function;Z)V

    return-void
.end method

.method public writeToEncryptedFile(Ljava/lang/String;[B)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->writeToFileInternal(Ljava/lang/String;[BZ)V

    return-void
.end method

.method public writeToFile(Ljava/lang/String;[B)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/AsyncFileStorageImpl;->writeToFileInternal(Ljava/lang/String;[BZ)V

    return-void
.end method
