.class Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;
.super Ljava/lang/Object;
.source "BaseEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerBackOfCardMenuListener(Landroid/app/Activity;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/BaseEntryAdapter;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/BaseEntryAdapter;->mTgPresenter:Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/BaseEntryAdapter;)Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/sidekick/TgPresenterAccessor;->toggleBackOfCard(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_INFO_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapter(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
