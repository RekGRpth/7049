.class Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;
.super Ljava/lang/Object;
.source "BaseEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

.field final synthetic val$card:Landroid/view/ViewGroup;

.field final synthetic val$feedbackPrompt:Landroid/view/View;

.field final synthetic val$likeAction:Lcom/google/geo/sidekick/Sidekick$Action;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/BaseEntryAdapter;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Action;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;->val$card:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;->val$likeAction:Lcom/google/geo/sidekick/Sidekick$Action;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;->val$feedbackPrompt:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;->val$card:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;->val$likeAction:Lcom/google/geo/sidekick/Sidekick$Action;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/BaseEntryAdapter$3;->val$feedbackPrompt:Landroid/view/View;

    # invokes: Lcom/google/android/apps/sidekick/BaseEntryAdapter;->recordFeedback(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Landroid/view/View;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/BaseEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Landroid/view/View;)V

    return-void
.end method
