.class Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;
.super Ljava/lang/Object;
.source "BirthdayCardEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->mBirthdayCardEntry:Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getSenderFocusId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->mBirthdayCardEntry:Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getRecipientFocusId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->mBirthdayCardEntry:Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$BirthdayCardEntry;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->val$context:Landroid/content/Context;

    const v5, 0x7f0d0275

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/GooglePlusIntents;->getBirthdayIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/BirthdayCardEntryAdapter;

    const-string v3, "WISH_HAPPY_BIRTHDAY"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
