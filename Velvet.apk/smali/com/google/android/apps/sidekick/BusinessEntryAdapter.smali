.class public Lcom/google/android/apps/sidekick/BusinessEntryAdapter;
.super Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;
.source "BusinessEntryAdapter.java"


# instance fields
.field private final mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 8
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p5    # Lcom/google/android/searchcommon/util/IntentUtils;
    .param p6    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;
    .param p7    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p8    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$BusinessData;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-void
.end method

.method private isGmailReservation()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v0

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method


# virtual methods
.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->isGmailReservation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d020d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->isGmailReservation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d017a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->isGmailReservation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d017b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v2

    const/16 v3, 0x9

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getTypeList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const v2, 0x7f0d02cc

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-static {p1, v2}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0d02ca

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->isGmailReservation()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f0d02c3

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    const v2, 0x7f0d02cb

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const-string v0, "GogglesPlace"

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->isGmailReservation()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GmailRestaurantReservation"

    goto :goto_0

    :cond_1
    const-string v0, "NearbyPlace"

    goto :goto_0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    const-class v0, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->isGmailReservation()Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v0, Lcom/google/android/searchcommon/preferences/cards/GmailRestaurantsCardSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, ":android:show_fragment"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v1
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v7, 0x0

    const v5, 0x7f040008

    invoke-virtual {p2, v5, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f100031

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-static {p1, v2, v5}, Lcom/google/android/apps/sidekick/PlaceDataHelper;->populateBusinessData(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$BusinessData;)V

    const v5, 0x7f1001ba

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-static {p1, v1, v5}, Lcom/google/android/apps/sidekick/PlaceDataHelper;->populatePlaceReview(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$BusinessData;)V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f100035

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v5, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;

    invoke-direct {v5, p0, p1}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/BusinessEntryAdapter;Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const/16 v5, 0x16

    invoke-virtual {p0, v5}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v4

    if-eqz v4, :cond_1

    const v5, 0x7f100036

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Action;->getDisplayMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$2;

    invoke-direct {v5, p0, p1}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/BusinessEntryAdapter;Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-object v2
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCid()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;-><init>(Landroid/content/Context;JLcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->run()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->logDetailsInteraction(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->shouldDisplay()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/BusinessEntryAdapter;->mBusinessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
