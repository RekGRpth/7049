.class Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;
.super Ljava/lang/Object;
.source "CalendarEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->createTimeToLeaveCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

.field final synthetic val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getPathfinderWaypointList()Ljava/util/List;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/android/apps/sidekick/DirectionsLauncher;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->val$route:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v4

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->start(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Ljava/util/List;I)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "CARD_BUTTON_PRESS"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    const-string v4, "NAVIGATE"

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
