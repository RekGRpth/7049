.class Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;
.super Ljava/lang/Object;
.source "CalendarEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->addEmailButton(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEventId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->createEmailAttendeesIntent(J)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "CARD_BUTTON_PRESS"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    const-string v4, "EMAIL_GUESTS"

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
