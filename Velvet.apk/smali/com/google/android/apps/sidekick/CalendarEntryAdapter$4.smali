.class Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;
.super Ljava/lang/Object;
.source "CalendarEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/MapsLauncher;->start(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_MENU_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/CalendarEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    const-string v3, "VIEW_IN_MAPS"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
