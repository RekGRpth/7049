.class Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;
.super Landroid/graphics/drawable/InsetDrawable;
.source "CardTableLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/CardTableLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlignedDrawable"
.end annotation


# instance fields
.field private mLeftPadding:I

.field private mRightPadding:I

.field private mTmpRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mTmpRect:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;I)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->setLeftPadding(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;I)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->setRightPadding(I)Z

    move-result v0

    return v0
.end method

.method private setLeftPadding(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mLeftPadding:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput p1, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mLeftPadding:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setRightPadding(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mRightPadding:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput p1, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mRightPadding:I

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 3
    .param p1    # Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mTmpRect:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mLeftPadding:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mTmpRect:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mRightPadding:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->mTmpRect:Landroid/graphics/Rect;

    invoke-super {p0, v0}, Landroid/graphics/drawable/InsetDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    return-void
.end method
