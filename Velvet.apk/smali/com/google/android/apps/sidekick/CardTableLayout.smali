.class public Lcom/google/android/apps/sidekick/CardTableLayout;
.super Landroid/widget/TableLayout;
.source "CardTableLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;,
        Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;

.field private mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

.field private mDividerBackground:I

.field private mDividerColumn:I

.field private mDividerPadding:I

.field private mIsExpanded:Z

.field private mOriginalDivider:Landroid/graphics/drawable/Drawable;

.field private final mTmpRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v3, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerColumn:I

    iput v2, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerBackground:I

    sget-object v1, Lcom/google/android/googlequicksearchbox/R$styleable;->CardTableLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerColumn:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/sidekick/CardTableLayout;->setDividerBackgroundColor(I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mTmpRect:Landroid/graphics/Rect;

    invoke-super {p0}, Landroid/widget/TableLayout;->getDividerPadding()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0}, Landroid/widget/TableLayout;->getDividerPadding()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/CardTableLayout;->setDividerPadding(I)V

    invoke-super {p0, v2}, Landroid/widget/TableLayout;->setDividerPadding(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mOriginalDivider:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->getDividerDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-super {p0}, Landroid/widget/TableLayout;->getDividerDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/CardTableLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method private alignDivider(Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;)Z
    .locals 6
    .param p1    # Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    const/4 v3, 0x0

    iget v1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerPadding:I

    iget v2, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerPadding:I

    iget v4, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerColumn:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerColumn:I

    invoke-direct {p0, v4}, Lcom/google/android/apps/sidekick/CardTableLayout;->getFirstCellInColumn(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {v4, v3, v3, v5, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/sidekick/CardTableLayout;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mTmpRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mTmpRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    add-int/2addr v2, v4

    :cond_0
    # invokes: Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->setLeftPadding(I)Z
    invoke-static {p1, v1}, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->access$000(Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;I)Z

    move-result v4

    if-nez v4, :cond_1

    # invokes: Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->setRightPadding(I)Z
    invoke-static {p1, v2}, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;->access$100(Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;I)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v3, 0x1

    :cond_2
    return v3
.end method

.method private createAlignedDivider(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # I

    new-instance v0, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    invoke-direct {v0, p1}, Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/CardTableLayout;->alignDivider(Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;)Z

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private getFirstCellInColumn(I)Landroid/view/View;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/CardTableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Landroid/widget/TableRow;

    if-eqz v3, :cond_0

    move-object v2, v0

    check-cast v2, Landroid/widget/TableRow;

    invoke-virtual {v2}, Landroid/widget/TableRow;->getChildCount()I

    move-result v3

    if-ge p1, v3, :cond_0

    invoke-virtual {v2, p1}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private setDividerBackgroundColor(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerBackground:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mOriginalDivider:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mOriginalDivider:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/sidekick/CardTableLayout;->createAlignedDivider(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/TableLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iput p1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerBackground:I

    goto :goto_0
.end method

.method private updateExpandedViews()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mIsExpanded:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->removeAllViews()V

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mIsExpanded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAdapter:Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAdapter:Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;

    invoke-interface {v0, p0}, Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;->expand(Lcom/google/android/apps/sidekick/CardTableLayout;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/TableLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/CardTableLayout;->alignDivider(Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->invalidate()V

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "parent_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/TableLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v1, "expanded"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mIsExpanded:Z

    iget-boolean v1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mIsExpanded:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->updateExpandedViews()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "parent_state"

    invoke-super {p0}, Landroid/widget/TableLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "expanded"

    iget-boolean v2, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mIsExpanded:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public setAdapter(Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAdapter:Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mIsExpanded:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->updateExpandedViews()V

    :cond_0
    return-void
.end method

.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mOriginalDivider:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mOriginalDivider:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerBackground:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/CardTableLayout;->createAlignedDivider(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    invoke-super {p0, v0}, Landroid/widget/TableLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setDividerPadding(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerPadding:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mDividerPadding:I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mAlignedDivider:Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/CardTableLayout;->alignDivider(Lcom/google/android/apps/sidekick/CardTableLayout$AlignedDrawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->invalidate()V

    goto :goto_0
.end method

.method public setExpanded(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mIsExpanded:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/CardTableLayout;->mIsExpanded:Z

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/CardTableLayout;->updateExpandedViews()V

    goto :goto_0
.end method
