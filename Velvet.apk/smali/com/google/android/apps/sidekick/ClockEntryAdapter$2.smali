.class Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;
.super Ljava/lang/Object;
.source "ClockEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/ClockEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/ClockEntryAdapter;

.field final synthetic val$clock:Lcom/google/android/searchcommon/util/Clock;

.field final synthetic val$timeTickListener:Lcom/google/android/searchcommon/util/Clock$TimeTickListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/ClockEntryAdapter;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/util/Clock$TimeTickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/ClockEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;->val$clock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;->val$timeTickListener:Lcom/google/android/searchcommon/util/Clock$TimeTickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;->val$clock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;->val$timeTickListener:Lcom/google/android/searchcommon/util/Clock$TimeTickListener;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Clock;->registerTimeTickListener(Lcom/google/android/searchcommon/util/Clock$TimeTickListener;)V

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;->val$clock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;->val$timeTickListener:Lcom/google/android/searchcommon/util/Clock$TimeTickListener;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Clock;->unregisterTimeTickListener(Lcom/google/android/searchcommon/util/Clock$TimeTickListener;)V

    return-void
.end method
