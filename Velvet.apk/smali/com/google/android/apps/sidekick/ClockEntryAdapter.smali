.class public Lcom/google/android/apps/sidekick/ClockEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "ClockEntryAdapter.java"


# instance fields
.field private final mClockEntry:Lcom/google/geo/sidekick/Sidekick$ClockEntry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getClockEntry()Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->mClockEntry:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/ClockEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$ClockEntry;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/ClockEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->mClockEntry:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/ClockEntryAdapter;Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/ClockEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->getFormattedText(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getFormattedText(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)Ljava/lang/String;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {p2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1, v8}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    const/4 v6, 0x3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->mClockEntry:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/geo/sidekick/Sidekick$ClockEntry;->getTimeZone(I)Lcom/google/geo/sidekick/Sidekick$ClockEntry$TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$ClockEntry$TimeZone;->getZoneName()Ljava/lang/String;

    move-result-object v7

    move-object v0, p1

    move-wide v4, v2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d020e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d01a9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d01ae

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02e0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/TravelCardsSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v1

    const v4, 0x7f040058

    const/4 v5, 0x0

    invoke-virtual {p2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->mClockEntry:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$ClockEntry;->getTimeZoneCount()I

    move-result v4

    if-lez v4, :cond_0

    const v4, 0x7f100130

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->getFormattedText(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v4, 0x7f100131

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/ClockEntryAdapter;->mClockEntry:Lcom/google/geo/sidekick/Sidekick$ClockEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$ClockEntry;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;

    invoke-direct {v2, p0, v3, v0, v1}, Lcom/google/android/apps/sidekick/ClockEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/ClockEntryAdapter;Landroid/view/View;Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V

    new-instance v4, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;

    invoke-direct {v4, p0, v1, v2}, Lcom/google/android/apps/sidekick/ClockEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/ClockEntryAdapter;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/util/Clock$TimeTickListener;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-object v3
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
