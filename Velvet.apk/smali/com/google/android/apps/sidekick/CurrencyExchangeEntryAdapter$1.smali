.class Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;
.super Ljava/lang/Object;
.source "CurrencyExchangeEntryAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->updateWhenChanged(Landroid/widget/EditText;Landroid/widget/EditText;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

.field final synthetic val$factor:F

.field final synthetic val$target:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;FLandroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;

    iput p2, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;->val$factor:F

    iput-object p3, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;->val$target:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .param p1    # Landroid/text/Editable;

    :try_start_0
    iget v1, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;->val$factor:F

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    mul-float v0, v1, v2

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;->val$target:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;->val$target:Landroid/widget/EditText;

    # getter for: Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->NUMBER_FORMAT:Ljava/text/NumberFormat;
    invoke-static {}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->access$000()Ljava/text/NumberFormat;

    move-result-object v2

    float-to-double v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
