.class public Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "CurrencyExchangeEntryAdapter.java"


# static fields
.field private static final NUMBER_FORMAT:Ljava/text/NumberFormat;


# instance fields
.field private final mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->NUMBER_FORMAT:Ljava/text/NumberFormat;

    return-void
.end method

.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCurrencyExchangeEntry()Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    return-void
.end method

.method static synthetic access$000()Ljava/text/NumberFormat;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->NUMBER_FORMAT:Ljava/text/NumberFormat;

    return-object v0
.end method

.method private getTargetAmount(F)Ljava/lang/String;
    .locals 3
    .param p1    # F

    sget-object v0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->NUMBER_FORMAT:Ljava/text/NumberFormat;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalToHomeRate()F

    move-result v1

    mul-float/2addr v1, p1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected addFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/View;

    const v0, 0x7f100084

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    invoke-virtual {v0, p2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 3

    new-instance v0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;)V

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;)V

    return-object v1
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d020e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d01aa

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d01ab

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02df

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/TravelCardsSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getSourceAmount(F)Ljava/lang/String;
    .locals 3
    .param p1    # F

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    float-to-double v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v4, 0x7f040027

    const/4 v5, 0x0

    invoke-virtual {p2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f100086

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalCurrency()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getDefaultLocalExchangeAmount()I

    move-result v4

    int-to-float v0, v4

    const v4, 0x7f100085

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->getSourceAmount(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const v4, 0x7f100088

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getHomeCurrency()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f100087

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->getTargetAmount(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const v4, 0x7f100089

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalToHomeRate()F

    move-result v4

    invoke-virtual {p0, v1, v2, v4}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->updateWhenChanged(Landroid/widget/EditText;Landroid/widget/EditText;F)V

    const/high16 v4, 0x3f800000

    iget-object v5, p0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->mCurrencyEntry:Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CurrencyExchangeEntry;->getLocalToHomeRate()F

    move-result v5

    div-float/2addr v4, v5

    invoke-virtual {p0, v2, v1, v4}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->updateWhenChanged(Landroid/widget/EditText;Landroid/widget/EditText;F)V

    return-object v3
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method showDisclaimer(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v2, 0x7f0d0056

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    const-string v2, "CURRENCY_EXCHANGE_DISCLAIMER"

    invoke-virtual {p0, p1, v1, v0, v2}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method updateWhenChanged(Landroid/widget/EditText;Landroid/widget/EditText;F)V
    .locals 1
    .param p1    # Landroid/widget/EditText;
    .param p2    # Landroid/widget/EditText;
    .param p3    # F

    new-instance v0, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;

    invoke-direct {v0, p0, p3, p2}, Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/CurrencyExchangeEntryAdapter;FLandroid/widget/EditText;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method
