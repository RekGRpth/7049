.class public Lcom/google/android/apps/sidekick/DismissTrail;
.super Landroid/widget/LinearLayout;
.source "DismissTrail.java"


# instance fields
.field private mEntryType:I

.field private mHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

.field private mRemoveAfterDelay:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mRemoveAfterDelay:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mRemoveAfterDelay:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mRemoveAfterDelay:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/DismissTrail;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/DismissTrail;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/DismissTrail;->removeMyself()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/DismissTrail;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/DismissTrail;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/DismissTrail;->cancelFader()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/DismissTrail;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/DismissTrail;

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mRemoveAfterDelay:Z

    return v0
.end method

.method private cancelFader()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mRemoveAfterDelay:Z

    return-void
.end method

.method private removeMyself()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/DismissTrail;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/DismissTrail;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeGridItem(Landroid/view/View;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    new-instance v0, Lcom/google/android/apps/sidekick/DismissTrail$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/DismissTrail$4;-><init>(Lcom/google/android/apps/sidekick/DismissTrail;)V

    const-wide/16 v1, 0x1b58

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/sidekick/DismissTrail;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    iget v1, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mEntryType:I

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->recordDismissedEntryType(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/DismissTrail;->cancelFader()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f100055

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/DismissTrail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/sidekick/DismissTrail$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/sidekick/DismissTrail$1;-><init>(Lcom/google/android/apps/sidekick/DismissTrail;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/apps/sidekick/DismissTrail$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/DismissTrail$2;-><init>(Lcom/google/android/apps/sidekick/DismissTrail;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/DismissTrail;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method public setCardDismissalHandler(Lcom/google/android/velvet/presenter/CardDismissalHandler;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/CardDismissalHandler;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    return-void
.end method

.method public setEntryAdapter(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v7, 0x0

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/sidekick/DismissTrail;->mEntryType:I

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/DismissTrail;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const v6, 0x7f100052

    invoke-virtual {p0, v6}, Lcom/google/android/apps/sidekick/DismissTrail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/DismissTrail;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const v6, 0x7f100053

    invoke-virtual {p0, v6}, Lcom/google/android/apps/sidekick/DismissTrail;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/DismissTrail;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_2

    const v6, 0x7f100054

    invoke-virtual {p0, v6}, Lcom/google/android/apps/sidekick/DismissTrail;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    new-instance v6, Lcom/google/android/apps/sidekick/DismissTrail$3;

    invoke-direct {v6, p0, v5}, Lcom/google/android/apps/sidekick/DismissTrail$3;-><init>(Lcom/google/android/apps/sidekick/DismissTrail;Landroid/content/Intent;)V

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    :cond_2
    return-void
.end method
