.class public Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;
.super Ljava/lang/Object;
.source "EntryInvalidatorImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/EntryInvalidator;


# static fields
.field static final MIN_TIME_BETWEEN_REFRESH_MILLIS:J = 0x7530L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field private final mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

.field private final mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Landroid/content/SharedPreferences;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
    .param p3    # Landroid/content/SharedPreferences;
    .param p4    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p5    # Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mPrefs:Landroid/content/SharedPreferences;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    return-void
.end method

.method private invalidateIfHasRecentNotification()Z
    .locals 8

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->getLastNotificationTime()J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getLastRefreshTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    sub-long v4, v0, v2

    const-wide/16 v6, 0x7530

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private invalidateIfLocaleChanged()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->isDataForLocale(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private invalidateIfLocationServicesStateChanged()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "location_service_disabled"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v2, "network"

    invoke-interface {v1, v2}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private invalidateIfLocationVeryFar()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->hasLocationChangedSignificantlySinceRefresh()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public invalidateIfNecessary()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->invalidateIfLocationServicesStateChanged()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->invalidateIfLocationVeryFar()Z

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->invalidateIfLocaleChanged()Z

    move-result v0

    :cond_1
    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryInvalidatorImpl;->invalidateIfHasRecentNotification()Z

    move-result v0

    :cond_2
    return-void
.end method
