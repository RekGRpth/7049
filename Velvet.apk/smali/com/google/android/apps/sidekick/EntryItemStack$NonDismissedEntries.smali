.class Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;
.super Ljava/lang/Object;
.source "EntryItemStack.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/EntryItemStack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NonDismissedEntries"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/EntryItemStack$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemStack$1;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->isDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;->apply(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Z

    move-result v0

    return v0
.end method
