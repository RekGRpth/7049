.class public Lcom/google/android/apps/sidekick/EntryItemStack;
.super Ljava/lang/Object;
.source "EntryItemStack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/EntryItemStack$1;,
        Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;
    }
.end annotation


# static fields
.field private static final NON_DISMISSED:Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;


# instance fields
.field private final mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;-><init>(Lcom/google/android/apps/sidekick/EntryItemStack$1;)V

    sput-object v0, Lcom/google/android/apps/sidekick/EntryItemStack;->NON_DISMISSED:Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemStack;->mEntries:Ljava/util/List;

    return-void
.end method

.method public varargs constructor <init>([Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 1
    .param p1    # [Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemStack;->mEntries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public containsRealEntries()Z
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryItemStack;->mEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getIsExample()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemStack;->mEntries:Ljava/util/List;

    return-object v0
.end method

.method public getEntriesToShow()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemAdapter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemStack;->mEntries:Ljava/util/List;

    sget-object v1, Lcom/google/android/apps/sidekick/EntryItemStack;->NON_DISMISSED:Lcom/google/android/apps/sidekick/EntryItemStack$NonDismissedEntries;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public getEntryType()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemStack;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryItemStack;->mEntries:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    goto :goto_0
.end method
