.class Lcom/google/android/apps/sidekick/EntryProviderImpl$2;
.super Ljava/lang/Object;
.source "EntryProviderImpl.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/EntryProviderImpl;->doInitializeFromStorage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<[B",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/EntryProviderImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->apply([B)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public apply([B)Ljava/lang/Void;
    .locals 12

    const/4 v8, 0x0

    const/4 v7, 0x1

    if-eqz p1, :cond_5

    :try_start_0
    new-instance v3, Lcom/google/android/apps/sidekick/EntryProviderData;

    invoke-direct {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;-><init>()V

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/EntryProviderData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$100(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLastRefreshMillis()J
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    sub-long v9, v0, v4

    const-wide/16 v0, 0x0

    cmp-long v0, v9, v0

    if-ltz v0, :cond_5

    const-wide/32 v0, 0x36ee80

    cmp-long v0, v9, v0

    if-lez v0, :cond_2

    move v0, v7

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$200(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->invalidate()V

    :cond_1
    :goto_1
    return-object v8

    :cond_2
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->hasLocale()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLocale()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/util/LocaleUtils;->parseJavaLocale(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v6

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$200(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->getIncludesMoreCards()Z

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v1

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/EntryProviderData;->getLastRefreshMillis()J

    move-result-wide v3

    # invokes: Lcom/google/android/apps/sidekick/EntryProviderImpl;->updateFromEntryResponseInternal(Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Location;JZLjava/util/Locale;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$300(Lcom/google/android/apps/sidekick/EntryProviderImpl;Lcom/google/geo/sidekick/Sidekick$EntryResponse;Lcom/google/geo/sidekick/Sidekick$Location;JZLjava/util/Locale;)V
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->getStalenessTimeoutMs()J

    move-result-wide v1

    cmp-long v1, v9, v1

    if-lez v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAppContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$400(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/sidekick/EntriesRefreshIntentService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mAppContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$400(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_3
    :try_start_3
    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "File storage contained invalid data"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$200(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->invalidate()V

    goto :goto_1

    :catchall_0
    move-exception v0

    move v1, v7

    :goto_4
    iget-object v2, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/EntryProviderImpl;->mFileStoreReadComplete:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->access$200(Lcom/google/android/apps/sidekick/EntryProviderImpl;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EntryProviderImpl$2;->this$0:Lcom/google/android/apps/sidekick/EntryProviderImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/EntryProviderImpl;->invalidate()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto :goto_4

    :catch_1
    move-exception v0

    move v0, v7

    goto :goto_3

    :cond_4
    move-object v6, v8

    goto :goto_2

    :cond_5
    move v0, v7

    goto/16 :goto_0
.end method
