.class public interface abstract Lcom/google/android/apps/sidekick/EntryProviderObserver;
.super Ljava/lang/Object;
.source "EntryProviderObserver.java"


# virtual methods
.method public abstract onCardListEntriesRefreshed()V
.end method

.method public abstract onEntriesAdded(Lcom/google/geo/sidekick/Sidekick$Interest;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Interest;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onInvalidated()V
.end method

.method public abstract onRefreshed()V
.end method
