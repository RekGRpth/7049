.class public Lcom/google/android/apps/sidekick/EntryTreeConverter;
.super Ljava/lang/Object;
.source "EntryTreeConverter.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/geo/sidekick/Sidekick$EntryResponse;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/sidekick/EntryItemStack;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

.field private final mRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/EntryItemFactory;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EntryTreeConverter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EntryTreeConverter;->mRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/EntryTreeConverter;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    return-void
.end method

.method private addAdaptersForEntries(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Ljava/util/List;)V
    .locals 9
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->hasGroupEntry()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryTreeConverter;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    invoke-interface {v6, p1}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->createForGroup(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->shouldDisplay()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Lcom/google/android/apps/sidekick/EntryItemStack;

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-direct {v6, v7}, Lcom/google/android/apps/sidekick/EntryItemStack;-><init>([Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    invoke-interface {p2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v6

    if-lez v6, :cond_4

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryTreeConverter;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/EntryTreeConverter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v8, p0, Lcom/google/android/apps/sidekick/EntryTreeConverter;->mRefreshLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-interface {v6, v2, v7, v8}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->shouldDisplay()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-direct {v6, v1}, Lcom/google/android/apps/sidekick/EntryItemStack;-><init>(Ljava/util/List;)V

    invoke-interface {p2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildCount()I

    move-result v6

    if-lez v6, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildCount()I

    move-result v6

    invoke-static {v6}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/EntryTreeConverter;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    invoke-interface {v6, v5}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->createForGroup(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->shouldDisplay()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-direct {v6, v1}, Lcom/google/android/apps/sidekick/EntryItemStack;-><init>(Ljava/util/List;)V

    invoke-interface {p2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/EntryTreeConverter;->apply(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public apply(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)Ljava/util/List;
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$EntryResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/sidekick/EntryTreeConverter;->addAdaptersForEntries(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :cond_1
    return-object v0
.end method
