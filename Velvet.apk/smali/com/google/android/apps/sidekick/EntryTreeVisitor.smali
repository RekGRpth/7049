.class public abstract Lcom/google/android/apps/sidekick/EntryTreeVisitor;
.super Ljava/lang/Object;
.source "EntryTreeVisitor.java"


# instance fields
.field private mVisited:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->mVisited:Z

    return-void
.end method

.method private visitInternal(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getChildList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->process(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->visitInternal(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    new-instance v2, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v2, v1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->shouldRemove(Lcom/google/android/apps/sidekick/ProtoKey;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->process(Lcom/google/android/apps/sidekick/ProtoKey;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_1
.end method


# virtual methods
.method public isVisited()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->mVisited:Z

    return v0
.end method

.method protected process(Lcom/google/android/apps/sidekick/ProtoKey;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 0
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method protected process(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    return-void
.end method

.method protected shouldRemove(Lcom/google/android/apps/sidekick/ProtoKey;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public final visit(Lcom/google/geo/sidekick/Sidekick$EntryResponse;)V
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->mVisited:Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->visit(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    goto :goto_0
.end method

.method public final visit(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->mVisited:Z

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->process(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;->visitInternal(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    return-void
.end method
