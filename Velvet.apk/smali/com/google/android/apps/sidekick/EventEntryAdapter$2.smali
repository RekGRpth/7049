.class Lcom/google/android/apps/sidekick/EventEntryAdapter$2;
.super Ljava/lang/Object;
.source "EventEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/EventEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/EventEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/EventEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/EventEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/EventEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/EventEntryAdapter$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EventEntryAdapter$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/EventEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/EventEntryAdapter;

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/MapsLauncher;->start(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/EventEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/EventEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/EventEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/EventEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/EventEntryAdapter;

    const-string v3, "EVENT_VIEW_LOCATION"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
