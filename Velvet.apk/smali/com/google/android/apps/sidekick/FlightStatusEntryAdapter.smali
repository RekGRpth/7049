.class public Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "FlightStatusEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$2;,
        Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;,
        Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;
    }
.end annotation


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

.field private final mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/util/Clock;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p5    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p6    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p5, p6}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    return-object v0
.end method

.method private addFlightSegment(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;Lcom/google/android/velvet/cards/FlightCard$Builder;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;
    .locals 7
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;
    .param p3    # Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;
    .param p4    # Lcom/google/android/velvet/cards/FlightCard$Builder;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v5

    invoke-virtual {p3, v5}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->setStatus(I)Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureAirport()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTime()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->hasLocation()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v4

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->departure()Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setAirportName(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setAirportCode(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;->SCHEDULED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;

    invoke-static {v3, v6}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;)Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setScheduled(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;->ACTUAL:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;

    invoke-static {v3, v6}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;)Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setActual(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTerminal()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setTerminal(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureGate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setGate(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalAirport()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTime()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalAirport()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v1

    const-string v4, ""

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->hasLocation()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v4

    :cond_1
    invoke-virtual {p3}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->arrival()Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setAirportName(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setAirportCode(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;->SCHEDULED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;

    invoke-static {v1, v6}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;)Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setScheduled(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;->ACTUAL:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;

    invoke-static {v1, v6}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;)Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setActual(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTerminal()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setTerminal(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalGate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->setGate(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    :goto_1
    if-nez p1, :cond_2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->hasRoute()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->hasLocation()Z

    move-result v5

    if-eqz v5, :cond_5

    move-object p1, v2

    :cond_2
    :goto_2
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getGmailReferenceList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p4, v5}, Lcom/google/android/velvet/cards/FlightCard$Builder;->setGmailReferenceList(Ljava/util/List;)Lcom/google/android/velvet/cards/FlightCard$Builder;

    return-object p1

    :cond_3
    const-string v5, "FlightStatusEntryAdapter"

    const-string v6, "Missing departure info"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v5, "FlightStatusEntryAdapter"

    const-string v6, "Missing arrival info"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->hasRoute()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->hasLocation()Z

    move-result v5

    if-eqz v5, :cond_2

    move-object p1, v0

    goto :goto_2
.end method

.method public static createSampleEntry(Landroid/content/Context;Z)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    new-instance v9, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    const v10, 0x7f0d01d3

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$Location;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    new-instance v9, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;-><init>()V

    invoke-virtual {v9, v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v9

    const v10, 0x7f0d01d4

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->setCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v0

    new-instance v9, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    const v10, 0x7f0d01d1

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$Location;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    new-instance v9, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;-><init>()V

    invoke-virtual {v9, v4}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v9

    const v10, 0x7f0d01d2

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->setCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v3

    new-instance v9, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;-><init>()V

    const-wide/32 v10, 0x4f849e24

    invoke-virtual {v9, v10, v11}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->setActualTimeSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v9

    const-wide/32 v10, 0x4f849e24

    invoke-virtual {v9, v10, v11}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->setScheduledTimeSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v5

    new-instance v9, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;-><init>()V

    const-wide/32 v10, 0x4f85b3e0

    invoke-virtual {v9, v10, v11}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->setActualTimeSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v9

    const-wide/32 v10, 0x4f85b3e0

    invoke-virtual {v9, v10, v11}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->setScheduledTimeSecondsSinceEpoch(J)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v2

    new-instance v9, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;-><init>()V

    invoke-virtual {v9, v3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDepartureAirport(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setArrivalAirport(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setStatusCode(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const v10, 0x7f0d01d0

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setStatus(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const v10, 0x7f0d01cf

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setAirlineCode(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const v10, 0x7f0d01cd

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setAirlineName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDepartureTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    invoke-virtual {v9, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setArrivalTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const v10, 0x7f0d01ce

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setFlightNumber(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const v10, 0x7f0d01d5

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDepartureTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const v10, 0x7f0d01d6

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setDepartureGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const v10, 0x7f0d01d7

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setArrivalTerminal(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v9

    const v10, 0x7f0d01d8

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->setArrivalGate(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v6

    if-eqz p1, :cond_0

    new-instance v8, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    invoke-direct {v8}, Lcom/google/geo/sidekick/Sidekick$GmailReference;-><init>()V

    const-string v9, "bogus_id"

    invoke-virtual {v8, v9}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->setEmailIdentifier(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const-string v9, "http://gmail.com"

    invoke-virtual {v8, v9}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->setEmailUrl(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailReference;

    const v9, 0x7f0d028b

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->setSenderEmailAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GmailReference;

    invoke-virtual {v6, v8}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->addGmailReference(Lcom/google/geo/sidekick/Sidekick$GmailReference;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    :cond_0
    new-instance v9, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;-><init>()V

    invoke-virtual {v9, v6}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->addFlight(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v7

    new-instance v9, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v9}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    invoke-virtual {v9, v7}, Lcom/google/geo/sidekick/Sidekick$Entry;->setFlightStatusEntry(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v9

    return-object v9
.end method

.method private static getTime(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;)Ljava/util/Calendar;
    .locals 8
    .param p0    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;
    .param p1    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasTimeZoneId()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getTimeZoneId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/velvet/cards/FlightCard;->fixTimeZone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    const-wide/16 v1, 0x0

    sget-object v5, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$2;->$SwitchMap$com$google$android$apps$sidekick$FlightStatusEntryAdapter$TimeType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$TimeType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasActualTimeSecondsSinceEpoch()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getActualTimeSecondsSinceEpoch()J

    move-result-wide v1

    :cond_2
    :goto_2
    const/4 v0, 0x0

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-lez v5, :cond_0

    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, v3}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    new-instance v5, Ljava/util/Date;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v1

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    goto :goto_0

    :cond_3
    const-string v4, "UTC"

    goto :goto_1

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasScheduledTimeSecondsSinceEpoch()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getScheduledTimeSecondsSinceEpoch()J

    move-result-wide v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private getTimeToUseInMs(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)J
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    const-wide/16 v2, 0x3e8

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasActualTimeSecondsSinceEpoch()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getActualTimeSecondsSinceEpoch()J

    move-result-wide v0

    mul-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getScheduledTimeSecondsSinceEpoch()J

    move-result-wide v0

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method public static populateSampleCard(Lcom/google/android/velvet/cards/FlightCard;Landroid/view/LayoutInflater;Lcom/google/android/searchcommon/util/Clock;Z)V
    .locals 8
    .param p0    # Lcom/google/android/velvet/cards/FlightCard;
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Z

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/FlightCard;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, p3}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->createSampleEntry(Landroid/content/Context;Z)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/util/Clock;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {v0, v7, p0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->populateView(Landroid/content/Context;Lcom/google/android/velvet/cards/FlightCard;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method formatNotificationTime(Landroid/content/Context;ILcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Ljava/lang/String;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getTimeZoneId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {p0, p3}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getTimeToUseInMs(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 7

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-virtual {v2, v6}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlight(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const v3, 0x7f0d02eb

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineCode()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;I[Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v2
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d020b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0168

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0169

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormattedTime(Landroid/content/Context;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    const/4 v0, 0x0

    if-nez p2, :cond_0

    :goto_0
    :pswitch_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$2;->$SwitchMap$com$google$android$apps$sidekick$FlightStatusEntryAdapter$RelevantFlight$Status:[I

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getStatus()Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0d0123

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->formatNotificationTime(Landroid/content/Context;ILcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0d0124

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->formatNotificationTime(Landroid/content/Context;ILcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0d0125

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->formatNotificationTime(Landroid/content/Context;ILcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlightList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getGmailReferenceCount()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_2

    const v3, 0x7f0d02c0

    :goto_0
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_2
    const v3, 0x7f0d02bc

    goto :goto_0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 5

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlightCount()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlightList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getGmailReferenceCount()I

    move-result v3

    if-lez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Gmail"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public getRelevantFlight()Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;
    .locals 14

    const/4 v11, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v2, 0x0

    iget-object v12, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v12}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    iget-object v12, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlightList()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v12

    const/4 v13, 0x5

    if-ne v12, v13, :cond_2

    move-object v2, v7

    :cond_1
    :goto_1
    if-eqz v2, :cond_5

    new-instance v12, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    sget-object v13, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->CANCELLED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    invoke-direct {v12, v2, v13}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;-><init>(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;)V

    :goto_2
    return-object v12

    :cond_2
    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDepartureTime()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasArrivalTime()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getTimeToUseInMs(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)J

    move-result-wide v5

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getTimeToUseInMs(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;)J

    move-result-wide v0

    cmp-long v12, v5, v3

    if-lez v12, :cond_3

    move-object v11, v7

    goto :goto_1

    :cond_3
    cmp-long v12, v5, v3

    if-gez v12, :cond_4

    cmp-long v12, v0, v3

    if-lez v12, :cond_4

    move-object v9, v7

    goto :goto_1

    :cond_4
    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasStatusCode()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v12

    const/4 v13, 0x3

    if-ne v12, v13, :cond_0

    move-object v10, v7

    goto :goto_0

    :cond_5
    if-eqz v11, :cond_6

    new-instance v12, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    sget-object v13, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->NOT_DEPARTED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    invoke-direct {v12, v11, v13}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;-><init>(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;)V

    goto :goto_2

    :cond_6
    if-eqz v9, :cond_7

    new-instance v12, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    sget-object v13, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->IN_AIR:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    invoke-direct {v12, v9, v13}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;-><init>(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;)V

    goto :goto_2

    :cond_7
    if-eqz v10, :cond_8

    new-instance v12, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    sget-object v13, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->LAST_LANDED:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    invoke-direct {v12, v10, v13}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;-><init>(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;)V

    goto :goto_2

    :cond_8
    const/4 v12, 0x0

    goto :goto_2
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getSpecificEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasType()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->getType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    new-instance v2, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/apps/sidekick/notifications/FlightTimeToLeaveForNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;)V

    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getRelevantFlight()Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    move-result-object v3

    invoke-direct {v2, v0, v3, p0}, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Lcom/google/android/velvet/cards/FlightCard;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/cards/FlightCard;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->populateView(Landroid/content/Context;Lcom/google/android/velvet/cards/FlightCard;)V

    return-object v0
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f1000cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-virtual {v0, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlight(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->hasDetailsUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlight(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDetailsUrl()Ljava/lang/String;

    move-result-object v1

    const-string v2, "FLIGHT_STATUS_DETAILS"

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public populateView(Landroid/content/Context;Lcom/google/android/velvet/cards/FlightCard;)V
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/velvet/cards/FlightCard;

    iget-object v11, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlight(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v3

    new-instance v1, Lcom/google/android/velvet/cards/FlightCard$Builder;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v11, v12}, Lcom/google/android/velvet/cards/FlightCard$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/google/android/velvet/cards/FlightCard$Builder;->setFlightStatusEntryAdapter(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)Lcom/google/android/velvet/cards/FlightCard$Builder;

    const/4 v7, 0x0

    iget-object v11, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlightList()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/FlightCard$Builder;->addSegment()Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;

    move-result-object v9

    invoke-direct {p0, v7, v4, v9, v1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->addFlightSegment(Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;Lcom/google/android/velvet/cards/FlightCard$Builder;)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;

    move-result-object v7

    goto :goto_0

    :cond_0
    if-eqz v7, :cond_1

    iget-object v11, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    const/4 v12, 0x0

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Lcom/google/android/apps/sidekick/DirectionsLauncher;->checkNavigationAvailability(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v6

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v10

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v8

    const/4 v11, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v8, v11}, Lcom/google/android/apps/sidekick/TimeUtilities;->getEtaString(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Z)Ljava/lang/String;

    move-result-object v2

    const v11, 0x7f0d012b

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Airport;->getCode()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v2, v12, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/google/android/velvet/cards/FlightCard$Builder;->setNavigateAction(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$Builder;

    new-instance v11, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;

    invoke-direct {v11, p0, v6, v10}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Location;I)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/google/android/velvet/cards/FlightCard;->setOnNavigateListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/cards/FlightCard$Builder;->update(Lcom/google/android/velvet/cards/FlightCard;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlightCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
