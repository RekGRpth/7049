.class Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;
.super Ljava/lang/Object;
.source "GenericCardEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->handleSpecialIntents(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;)Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;)Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2$1;-><init>(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->setHistoryEnabled(Landroid/accounts/Account;ZLcom/google/android/searchcommon/util/Consumer;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/sidekick/TgPresenterAccessor;->dismissEntry(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    return-void
.end method
