.class Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;
.super Ljava/lang/Object;
.source "GenericCardEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->handleGenericIntent(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;I)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

.field final synthetic val$buttonLogLabel:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$viewAction:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->val$viewAction:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->val$buttonLogLabel:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->val$viewAction:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    # invokes: Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->createIntent(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Landroid/content/Intent;
    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->access$400(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->val$context:Landroid/content/Context;

    const v3, 0x7f0d039c

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeStartActivityWithMessage(Landroid/content/Context;Landroid/content/Intent;I)Z

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "CARD_BUTTON_PRESS"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->this$0:Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;->val$buttonLogLabel:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
