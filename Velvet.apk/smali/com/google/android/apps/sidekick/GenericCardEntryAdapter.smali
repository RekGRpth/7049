.class public Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "GenericCardEntryAdapter.java"


# instance fields
.field private final mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

.field private final mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

.field private final mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/searchcommon/MarinerOptInSettings;Landroid/net/wifi/WifiManager;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/history/SearchHistoryHelper;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/searchcommon/MarinerOptInSettings;
    .param p3    # Landroid/net/wifi/WifiManager;
    .param p4    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p5    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p6    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .param p7    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p4, p7}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getGenericCardEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mWifiManager:Landroid/net/wifi/WifiManager;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;)Landroid/net/wifi/WifiManager;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;)Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;)Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mSearchHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;)Lcom/google/android/searchcommon/MarinerOptInSettings;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->createIntent(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createButton(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)V
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/view/LayoutInflater;
    .param p4    # Landroid/view/ViewGroup;
    .param p5    # I

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasLabel()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasUri()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasAction()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getLatitudeOptInAction()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    const v3, 0x7f040010

    const/4 v4, 0x0

    invoke-virtual {p3, v3, p4, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f10004c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->handleSpecialIntents(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;)Landroid/view/View$OnClickListener;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->handleGenericIntent(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;I)Landroid/view/View$OnClickListener;

    move-result-object v2

    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private createIntent(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Landroid/content/Intent;
    .locals 7
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasUri()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    :goto_0
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getExtraList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasStringValue()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getStringValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasLongValue()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getLongValue()J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->hasBoolValue()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction$Extra;->getBoolValue()Z

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    :cond_4
    return-object v2
.end method

.method private getLatitudeOptInOnClick(Landroid/content/Context;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;Landroid/content/Context;)V

    return-object v0
.end method

.method private handleGenericIntent(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;I)Landroid/view/View$OnClickListener;
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
    .param p2    # Landroid/content/Context;
    .param p3    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Button"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;Ljava/lang/String;)V

    return-object v1
.end method

.method private hasWifiChangePermission(Landroid/content/Context;)Z
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.permission.CHANGE_WIFI_STATE"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasReason()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getReason()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v2, 0x7f040043

    const/4 v4, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f1000f5

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasText()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f1000f6

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageUrl()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f1000f7

    invoke-virtual {v9, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/velvet/ui/WebImageView;

    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v12, v15}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageWidth()Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageHeight()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageWidth()Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageWidth()I

    move-result v16

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->hasImageHeight()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getImageHeight()I

    move-result v10

    :goto_1
    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    move/from16 v0, v16

    invoke-direct {v13, v0, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v12, v13}, Lcom/google/android/velvet/ui/WebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getViewActionCount()I

    move-result v2

    if-lez v2, :cond_6

    move-object v6, v9

    check-cast v6, Landroid/view/ViewGroup;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->mGenericCardEntry:Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getViewActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    add-int/lit8 v8, v7, 0x1

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->createButton(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)V

    move v7, v8

    goto :goto_2

    :cond_4
    const/16 v16, -0x2

    goto :goto_0

    :cond_5
    const/4 v10, -0x2

    goto :goto_1

    :cond_6
    return-object v9
.end method

.method handleSpecialIntents(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;Landroid/content/Context;)Landroid/view/View$OnClickListener;
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;
    .param p2    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasAction()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getAction()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->hasUri()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getUri()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->getLatitudeOptInAction()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->getLatitudeOptInOnClick(Landroid/content/Context;)Landroid/view/View$OnClickListener;

    move-result-object v2

    :cond_1
    :goto_2
    return-object v2

    :cond_2
    move-object v0, v2

    goto :goto_0

    :cond_3
    move-object v1, v2

    goto :goto_1

    :cond_4
    const-string v3, "android.settings.WIFI_SETTINGS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-direct {p0, p2}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;->hasWifiChangePermission(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v2, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;)V

    goto :goto_2

    :cond_5
    const-string v3, "https://www.google.com/history/settings"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v2, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/sidekick/GenericCardEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/GenericCardEntryAdapter;Landroid/content/Context;)V

    goto :goto_2
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
