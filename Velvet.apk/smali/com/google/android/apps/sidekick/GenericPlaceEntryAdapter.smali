.class public Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;
.super Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;
.source "GenericPlaceEntryAdapter.java"


# instance fields
.field private mIsExample:Z


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Lcom/google/android/apps/sidekick/inject/StaticMapCache;
    .param p6    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p7    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/inject/StaticMapCache;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->mIsExample:Z

    return-void
.end method

.method private configurePlaceConfirmationBanner(Landroid/app/Activity;Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    const/16 v5, 0x11

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->placeConfirmationRequested()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    const v4, 0x7f10002a

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    invoke-virtual {v4}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    const v4, 0x7f100056

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v4, 0x7f10005e

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v4, 0x7f100060

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRenameOrEditAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v0

    if-eq v0, v5, :cond_1

    const/16 v4, 0x12

    if-ne v0, v4, :cond_3

    :cond_1
    if-ne v0, v5, :cond_2

    const v4, 0x7f0d01e3

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0d01e5

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(I)V

    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->confirmExistingHomeWorkOnClick(Landroid/app/Activity;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f0d01e9

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->editHomeWorkOnClick(Landroid/app/Activity;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    const v4, 0x7f0d01e7

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0d01e8

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    :cond_3
    const v4, 0x7f0d01e2

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f0d01e6

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->deletePlaceOnClick(Landroid/content/Context;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f0d01e4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->renamePlaceOnClick(Landroid/app/Activity;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method private confirmExistingHomeWorkOnClick(Landroid/app/Activity;)Landroid/view/View$OnClickListener;
    .locals 4
    .param p1    # Landroid/app/Activity;

    new-instance v0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRenameOrEditAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;-><init>(Landroid/app/Activity;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Z)V

    new-instance v1, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;Lcom/google/android/apps/sidekick/actions/EntryAction;)V

    return-object v1
.end method

.method private deletePlaceOnClick(Landroid/content/Context;)Landroid/view/View$OnClickListener;
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getDeleteAction(Landroid/content/Context;)Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;Lcom/google/android/apps/sidekick/actions/EntryAction;)V

    return-object v1
.end method

.method private editHomeWorkOnClick(Landroid/app/Activity;)Landroid/view/View$OnClickListener;
    .locals 4
    .param p1    # Landroid/app/Activity;

    new-instance v0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRenameOrEditAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryAction;-><init>(Landroid/app/Activity;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Z)V

    new-instance v1, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;Lcom/google/android/apps/sidekick/actions/EntryAction;)V

    return-object v1
.end method

.method private final getRenameOrEditAction()Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v3

    const/16 v4, 0x11

    if-eq v3, v4, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v3

    const/16 v4, 0x12

    if-ne v3, v4, :cond_0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private renamePlaceOnClick(Landroid/app/Activity;)Landroid/view/View$OnClickListener;
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRenameOrDeleteAction(Landroid/app/Activity;)Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$4;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;)V

    return-object v1
.end method


# virtual methods
.method public examplify()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->mIsExample:Z

    return-void
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 3

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;

    invoke-direct {v2, p0}, Lcom/google/android/apps/sidekick/feedback/GenericPlaceActionAdapter;-><init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;)V

    return-object v0
.end method

.method public final getDeleteAction(Landroid/content/Context;)Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;
    .locals 6
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v4

    const/16 v5, 0xe

    if-ne v4, v5, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getConfirmationLabel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;

    invoke-direct {v4, p1, v0, v1}, Lcom/google/android/apps/sidekick/actions/DeleteEntryAction;-><init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final getEditHomeOrWorkAction()Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    const/16 v3, 0x11

    if-eq v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0208

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0190

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d01a3

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRenameOrEditAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getIsCommuteDestination()Z

    move-result v1

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasSourceType()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    :cond_0
    :goto_0
    return-object v2

    :pswitch_0
    if-eqz v1, :cond_0

    const v2, 0x7f0d02d8

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    if-eqz v1, :cond_0

    const v2, 0x7f0d02d7

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :pswitch_2
    const v2, 0x7f0d02d9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :pswitch_3
    const v2, 0x7f0d02da

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->placeConfirmationRequested()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "Unconfirmed"

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRenameOrEditAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getSourceType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const-string v2, "SearchHistoryPlace"

    :goto_1
    return-object v2

    :cond_0
    const-string v1, ""

    goto :goto_0

    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Home"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Work"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "FrequentPlace"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getRenameOrDeleteAction(Landroid/app/Activity;)Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;
    .locals 10
    .param p1    # Landroid/app/Activity;

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRenameOrEditAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v8

    const/16 v9, 0xe

    if-ne v8, v9, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getConfirmationLabel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;

    invoke-direct {v7, p1, v2, v6, v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceAction;-><init>(Landroid/app/Activity;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Action;)V

    goto :goto_0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->placeConfirmationRequested()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEditHomeOrWorkAction()Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "<font color=\"#%1$h\">%2$s</font>"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0xffffff

    and-int/2addr v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const v5, 0x7f0d0233

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public handlePlaceEdit(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0, v3, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->updateTitle(Landroid/content/Context;Landroid/view/View;)V

    const v3, 0x7f10002b

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0, v3, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->showMap(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->shouldShowNavigation()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->mTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->mightShowNavigateButtonFor(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;)Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f10002c

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->mAlternateTravelReport:Lcom/google/android/apps/sidekick/TravelReport;

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->mightShowNavigateButtonFor(Landroid/content/Context;Lcom/google/android/apps/sidekick/TravelReport;)Z

    move-result v3

    if-eqz v3, :cond_2

    const v3, 0x7f10002d

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->placeConfirmationRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ViewInMapsAction;->run()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->logDetailsInteraction(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->launchDetails(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public placeConfirmationRequested()Z
    .locals 5

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->mIsExample:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v3

    const/16 v4, 0x10

    if-ne v3, v4, :cond_2

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/AbstractPlaceEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->configurePlaceConfirmationBanner(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected shouldShowNavigation()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->placeConfirmationRequested()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected shouldShowRoute()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->placeConfirmationRequested()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
