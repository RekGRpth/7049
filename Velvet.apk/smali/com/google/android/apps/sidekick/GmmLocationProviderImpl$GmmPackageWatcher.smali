.class public Lcom/google/android/apps/sidekick/GmmLocationProviderImpl$GmmPackageWatcher;
.super Landroid/content/BroadcastReceiver;
.source "GmmLocationProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/GmmLocationProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GmmPackageWatcher"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "package:com.google.android.apps.maps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    # invokes: Lcom/google/android/apps/sidekick/GmmLocationProviderImpl;->gmmLocationProviderFromContext(Landroid/content/Context;)Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;
    invoke-static {p1}, Lcom/google/android/apps/sidekick/GmmLocationProviderImpl;->access$000(Landroid/content/Context;)Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;->onGmmPackageChanged(Landroid/content/Intent;)V

    goto :goto_0
.end method
