.class public Lcom/google/android/apps/sidekick/GooglePlusIntents;
.super Ljava/lang/Object;
.source "GooglePlusIntents.java"


# static fields
.field static final PLUS_ONE_PACKAGE:Ljava/lang/String; = "com.google.android.apps.plus"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static canSendBirthdayIntent(Landroid/content/Context;Lcom/google/android/searchcommon/util/IntentUtils;)Z
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/searchcommon/util/IntentUtils;

    invoke-static {}, Lcom/google/android/apps/sidekick/GooglePlusIntents;->createBirthdayIntentWithNoExtras()Landroid/content/Intent;

    move-result-object v0

    invoke-interface {p1, p0, v0}, Lcom/google/android/searchcommon/util/IntentUtils;->isIntentHandled(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private static createBirthdayIntentWithNoExtras()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus.GOOGLE_BIRTHDAY_POST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static getBirthdayIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/apps/sidekick/GooglePlusIntents;->createBirthdayIntentWithNoExtras()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.SENDER_ID"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus.RECIPIENT_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus.RECIPIENT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.plus.BIRTHDAY_YEAR"

    const/16 v2, 0x7dc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method
