.class Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;
.super Ljava/lang/Object;
.source "GoogleServiceWebviewClickListener.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Landroid/net/Uri;)Z
    .locals 4

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->access$000(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "AUTH_TOKEN_FAIL_FOR_WEBVIEW"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logInternalAction(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    const v1, 0x7f0d024a

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->makeToast(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->access$100(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;II)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->access$000(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;

    invoke-direct {v0, v1, p1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "webview_title"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->access$400(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "enable_javascript"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mEnableJavascript:Z
    invoke-static {v3}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->access$300(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "webview_url_prefixes"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mWebviewUrlPrefixes:[Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->access$200(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->this$0:Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    # getter for: Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->access$000(Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener$1;->consume(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
