.class public final enum Lcom/google/android/apps/sidekick/IconSet;
.super Ljava/lang/Enum;
.source "IconSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/sidekick/IconSet;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/sidekick/IconSet;

.field public static final enum HOME_NOTIFICATION:Lcom/google/android/apps/sidekick/IconSet;


# instance fields
.field private final mLongDelay:I

.field private final mMediumDelay:I

.field private final mNoDelay:I

.field private final mUnknown:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/sidekick/IconSet;

    const-string v1, "HOME_NOTIFICATION"

    const v3, 0x7f02017d

    const v4, 0x7f02017e

    const v5, 0x7f02017c

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/IconSet;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lcom/google/android/apps/sidekick/IconSet;->HOME_NOTIFICATION:Lcom/google/android/apps/sidekick/IconSet;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/sidekick/IconSet;

    sget-object v1, Lcom/google/android/apps/sidekick/IconSet;->HOME_NOTIFICATION:Lcom/google/android/apps/sidekick/IconSet;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/sidekick/IconSet;->$VALUES:[Lcom/google/android/apps/sidekick/IconSet;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/sidekick/IconSet;->mNoDelay:I

    iput p4, p0, Lcom/google/android/apps/sidekick/IconSet;->mMediumDelay:I

    iput p5, p0, Lcom/google/android/apps/sidekick/IconSet;->mLongDelay:I

    iput p6, p0, Lcom/google/android/apps/sidekick/IconSet;->mUnknown:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/sidekick/IconSet;
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/IconSet;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/IconSet;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/sidekick/IconSet;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/IconSet;->$VALUES:[Lcom/google/android/apps/sidekick/IconSet;

    invoke-virtual {v0}, [Lcom/google/android/apps/sidekick/IconSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/sidekick/IconSet;

    return-object v0
.end method


# virtual methods
.method public getIcon(I)I
    .locals 1
    .param p1    # I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/sidekick/IconSet;->mUnknown:I

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    iget v0, p0, Lcom/google/android/apps/sidekick/IconSet;->mUnknown:I

    goto :goto_0

    :pswitch_0
    iget v0, p0, Lcom/google/android/apps/sidekick/IconSet;->mNoDelay:I

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/google/android/apps/sidekick/IconSet;->mMediumDelay:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/google/android/apps/sidekick/IconSet;->mLongDelay:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
