.class Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;
.super Ljava/lang/Object;
.source "LocalAttractionsListEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

.field final synthetic val$card:Landroid/view/View;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$layoutInflater:Landroid/view/LayoutInflater;

.field final synthetic val$showMoreButton:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;Landroid/widget/Button;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$showMoreButton:Landroid/widget/Button;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$layoutInflater:Landroid/view/LayoutInflater;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$card:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$showMoreButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$layoutInflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$card:Landroid/view/View;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->mAttractionList:Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;
    invoke-static {v5}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$AttractionListEntry;->getAttractionCount()I

    move-result v5

    # invokes: Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->addAttractions(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter$1;->val$card:Landroid/view/View;

    # invokes: Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->hideBottomDivider(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/LocalAttractionsListEntryAdapter;Landroid/view/View;)V

    return-void
.end method
