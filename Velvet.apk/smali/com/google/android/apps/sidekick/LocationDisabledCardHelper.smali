.class public Lcom/google/android/apps/sidekick/LocationDisabledCardHelper;
.super Ljava/lang/Object;
.source "LocationDisabledCardHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/LocationDisabledCardHelper;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/LocationDisabledCardHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method createDisabledLocationCard(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;
    .param p4    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p5    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v5, "network"

    invoke-interface {p3, v5}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v4, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;-><init>()V

    const-string v5, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->setAction(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    const v5, 0x7f0d03a7

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->setLabel(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    :goto_0
    new-instance v2, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;-><init>()V

    const v5, 0x7f0d01fe

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setText(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v5

    const v6, 0x7f0d01fd

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setTitle(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->addViewAction(Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v5

    const-string v6, "NlpDisabled"

    invoke-virtual {v5, v6}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->setCardType(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    :goto_1
    return-object v2

    :cond_0
    invoke-interface {p4}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p5}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccountName()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.google.android.gsf.action.SET_USE_LOCATION_FOR_SERVICES"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    const-string v5, "account"

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    const-string v5, "disable"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v3}, Lcom/google/android/apps/sidekick/ProtoUtils;->getViewActionFromIntent(Landroid/content/Intent;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    move-result-object v4

    const v5, 0x7f0d029b

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;->setLabel(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$GenericCardEntry$ViewAction;

    goto :goto_0

    :cond_2
    const-string v5, "location_service_disabled"

    invoke-interface {p2, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v5, "location_service_disabled"

    invoke-interface {v1, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
