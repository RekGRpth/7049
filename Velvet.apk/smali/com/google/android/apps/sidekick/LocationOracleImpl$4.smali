.class Lcom/google/android/apps/sidekick/LocationOracleImpl$4;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;->stop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->stopListening()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STOPPED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mSettingsObserver:Lcom/google/android/searchcommon/google/LocationSettings$Observer;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/google/LocationSettings$Observer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/google/LocationSettings;->removeUseLocationObserver(Lcom/google/android/searchcommon/google/LocationSettings$Observer;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mCheckSettingTask:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1800(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STARTED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    goto :goto_0
.end method
