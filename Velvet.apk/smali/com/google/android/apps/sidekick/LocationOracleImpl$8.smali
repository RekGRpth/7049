.class Lcom/google/android/apps/sidekick/LocationOracleImpl$8;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;->pushTestLocations(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

.field final synthetic val$locations:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->val$locations:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->val$locations:Ljava/util/List;

    # setter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugLocations:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3402(Lcom/google/android/apps/sidekick/LocationOracleImpl;Ljava/util/List;)Ljava/util/List;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->updateGeofencers(Landroid/location/Location;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3500(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->broadcastIfMoved(Landroid/location/Location;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3600(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
