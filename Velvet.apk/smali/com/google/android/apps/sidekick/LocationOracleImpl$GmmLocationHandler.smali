.class Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;
.super Landroid/os/Handler;
.source "LocationOracleImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/LocationOracleImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GmmLocationHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1000(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithAndroidProviders()V
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # getter for: Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocation:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$3200(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;->this$0:Lcom/google/android/apps/sidekick/LocationOracleImpl;

    # invokes: Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->access$2800(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
