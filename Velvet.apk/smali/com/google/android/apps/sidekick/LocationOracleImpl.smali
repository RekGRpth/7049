.class public Lcom/google/android/apps/sidekick/LocationOracleImpl;
.super Ljava/lang/Object;
.source "LocationOracleImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/LocationOracle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;,
        Lcom/google/android/apps/sidekick/LocationOracleImpl$ClockListener;,
        Lcom/google/android/apps/sidekick/LocationOracleImpl$GmmLocationHandler;,
        Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;,
        Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;,
        Lcom/google/android/apps/sidekick/LocationOracleImpl$SettingsObserver;,
        Lcom/google/android/apps/sidekick/LocationOracleImpl$State;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBgHandler:Landroid/os/Handler;

.field private mBgThread:Landroid/os/HandlerThread;

.field private final mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

.field private final mCheckSettingTask:Ljava/lang/Runnable;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mClockListener:Lcom/google/android/searchcommon/util/Clock$TimeResetListener;

.field private final mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

.field private mDebugLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mDebugStationaryTimeSecs:Ljava/lang/Integer;

.field private final mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

.field private final mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

.field private mFirstStationaryLocationFromGMM:Landroid/location/Location;

.field private final mGeofencers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;",
            ">;"
        }
    .end annotation
.end field

.field private mGmmLocationHandler:Landroid/os/Handler;

.field private final mGmmLocationProvider:Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

.field private final mGsaPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private mLastAndroidLocationReceivedMillis:J

.field private mLastGeofenceLocation:Landroid/location/Location;

.field private final mLastGmmLocation:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mLastGmmLocationReceivedMillis:J

.field private mLastLocationsFileHash:I

.field private mLastSignificantLocationChange:Landroid/location/Location;

.field private final mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

.field private final mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

.field private final mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field private final mLocationStorage:Lcom/google/android/apps/sidekick/LocationStorage;

.field private final mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

.field private final mLock:Ljava/lang/Object;

.field private final mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

.field private final mPurgeLocationsTask:Ljava/lang/Runnable;

.field private final mRunningLocks:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettingsObserver:Lcom/google/android/searchcommon/google/LocationSettings$Observer;

.field private final mState:Lcom/google/android/searchcommon/util/StateMachine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/StateMachine",
            "<",
            "Lcom/google/android/apps/sidekick/LocationOracleImpl$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/FileBytesReader;Lcom/google/android/apps/sidekick/FileBytesWriter;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/searchcommon/debug/DebugFeatures;Lcom/google/android/searchcommon/GsaPreferenceController;Landroid/support/v4/content/LocalBroadcastManager;Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;
    .param p2    # Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Lcom/google/android/apps/sidekick/FileBytesReader;
    .param p5    # Lcom/google/android/apps/sidekick/FileBytesWriter;
    .param p6    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p7    # Lcom/google/android/searchcommon/debug/DebugFeatures;
    .param p8    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p9    # Landroid/support/v4/content/LocalBroadcastManager;
    .param p10    # Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;
    .param p11    # Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocation:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    new-instance v0, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$PurgeLocationsTask;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mPurgeLocationsTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$CheckSettingTask;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mCheckSettingTask:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGeofencers:Ljava/util/Set;

    sget-object v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STOPPED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->newBuilder(Ljava/lang/String;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STOPPED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STARTED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STARTED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STARTED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STOPPED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STARTED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STOPPED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setSingleThreadOnly(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->setStrictMode(Z)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->build()Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFirstStationaryLocationFromGMM:Landroid/location/Location;

    new-instance v0, Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-direct {v0, p3}, Lcom/google/android/apps/sidekick/LocationQueue;-><init>(Lcom/google/android/searchcommon/util/Clock;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGmmLocationProvider:Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    new-instance v0, Lcom/google/android/apps/sidekick/LocationOracleImpl$SettingsObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$SettingsObserver;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mSettingsObserver:Lcom/google/android/searchcommon/google/LocationSettings$Observer;

    new-instance v0, Lcom/google/android/apps/sidekick/LocationOracleImpl$ClockListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$ClockListener;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Lcom/google/android/apps/sidekick/LocationOracleImpl$1;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClockListener:Lcom/google/android/searchcommon/util/Clock$TimeResetListener;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    iput-object p8, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGsaPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object p9, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    new-instance v0, Lcom/google/android/apps/sidekick/LocationStorage;

    invoke-direct {v0, p10}, Lcom/google/android/apps/sidekick/LocationStorage;-><init>(Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationStorage:Lcom/google/android/apps/sidekick/LocationStorage;

    iput-object p11, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGmmLocationHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->startInternal()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/StateMachine;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->stopListening()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/google/LocationSettings$Observer;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mSettingsObserver:Lcom/google/android/searchcommon/google/LocationSettings$Observer;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/google/LocationSettings;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mCheckSettingTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBgHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->startListening()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Landroid/location/Location;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getNetworkLocation()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;Landroid/location/Location;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p1    # Landroid/location/Location;
    .param p2    # Landroid/location/Location;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/apps/sidekick/LocationQueue;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithAndroidProviders()V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithGmmProvider()V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p1    # Landroid/location/Location;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/apps/sidekick/LocationOracleImpl;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocationReceivedMillis:J

    return-wide v0
.end method

.method static synthetic access$2902(Lcom/google/android/apps/sidekick/LocationOracleImpl;J)J
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocationReceivedMillis:J

    return-wide p1
.end method

.method static synthetic access$3000(Lcom/google/android/apps/sidekick/LocationOracleImpl;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastAndroidLocationReceivedMillis:J

    return-wide v0
.end method

.method static synthetic access$3100(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocation:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mPurgeLocationsTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/google/android/apps/sidekick/LocationOracleImpl;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugLocations:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p1    # Landroid/location/Location;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->updateGeofencers(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;
    .param p1    # Landroid/location/Location;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->broadcastIfMoved(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/sidekick/LocationOracleImpl;)Ljava/util/WeakHashMap;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mRunningLocks:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->startLocked()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->stop()V

    return-void
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static androidLocationFromProto(Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;)Landroid/location/Location;
    .locals 4
    .param p0    # Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    new-instance v1, Landroid/location/Location;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getLat()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getLng()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getTimestampMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setTime(J)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasAccuracyMeters()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getAccuracyMeters()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/location/Location;->setAccuracy(F)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTravelState()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasNetworkLocationType()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTravelState()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "travelState"

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getTravelState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasNetworkLocationType()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "networkLocationType"

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getNetworkLocationType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    :cond_4
    return-object v1
.end method

.method private static androidLocationHashCode(Landroid/location/Location;)I
    .locals 8
    .param p0    # Landroid/location/Location;

    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    :goto_0
    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v4, "travelState"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "networkLocationType"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_0
    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object v0, v4, v5

    const/4 v5, 0x5

    aput-object v3, v4, v5

    const/4 v5, 0x6

    aput-object v2, v4, v5

    invoke-static {v4}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v4

    return v4

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static androidLocationToProto(Landroid/location/Location;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 7
    .param p0    # Landroid/location/Location;

    new-instance v5, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    invoke-direct {v5}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-virtual {v5, v4}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setProvider(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    move-result-object v4

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setLat(D)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    move-result-object v4

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setLng(D)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    move-result-object v4

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setTimestampMillis(J)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    move-result-object v2

    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setAccuracyMeters(F)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    :cond_0
    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v4, "travelState"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "networkLocationType"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setTravelState(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v2, v1}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setNetworkLocationType(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    :cond_3
    return-object v2

    :cond_4
    const-string v4, ""

    goto :goto_0
.end method

.method private broadcastIfMoved(Landroid/location/Location;)V
    .locals 4
    .param p1    # Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastSignificantLocationChange:Landroid/location/Location;

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->locationChangedSignificantly(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationStorage:Lcom/google/android/apps/sidekick/LocationStorage;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    const-string v3, "lastloc"

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/apps/sidekick/LocationStorage;->saveCurrentLocation(Landroid/location/Location;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastSignificantLocationChange:Landroid/location/Location;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.LOCATION_CHANGED_SIGNIFICANTLY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "location"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    :cond_0
    return-void
.end method

.method private broadcastTravelState()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.sidekick.location_travel_state_action"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->phoneIsStationary()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "stationary"

    :goto_0
    const-string v2, "travelState"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    invoke-virtual {v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    return-void

    :cond_0
    const-string v1, "moving"

    goto :goto_0
.end method

.method private getBgHandler()Landroid/os/Handler;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThread:Landroid/os/HandlerThread;

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    const-string v3, "Interrupted while waiting for thread start"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v1
.end method

.method private getNetworkLocation()Landroid/location/Location;
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocationReceivedMillis:J

    sub-long v0, v2, v4

    const-wide/32 v2, 0xd0bd8

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocation:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v3, "network"

    invoke-interface {v2, v3}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v3, "network"

    invoke-interface {v2, v3}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGsaPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method private initializeFromStorage()V
    .locals 15

    iget-object v11, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFileReader:Lcom/google/android/apps/sidekick/FileBytesReader;

    const-string v12, "loracle"

    const/high16 v13, 0x80000

    invoke-virtual {v11, v12, v13}, Lcom/google/android/apps/sidekick/FileBytesReader;->readEncryptedFileBytes(Ljava/lang/String;I)[B

    move-result-object v1

    const/4 v10, 0x1

    if-eqz v1, :cond_2

    :try_start_0
    new-instance v3, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;

    invoke-direct {v3}, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;-><init>()V

    invoke-virtual {v3, v1}, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;->getLocationCount()I

    move-result v11

    invoke-static {v11}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;->getLocationList()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    invoke-static {v5}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->androidLocationFromProto(Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;)Landroid/location/Location;

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-virtual {v11, v6}, Lcom/google/android/apps/sidekick/LocationQueue;->addLocation(Landroid/location/Location;)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_1
    sget-object v11, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    const-string v12, "File storage contained invalid data"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v10, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getNetworkLocation()Landroid/location/Location;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v13, "gps"

    invoke-interface {v12, v13}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_2
    invoke-static {v9}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->locationListHashCode(Ljava/util/List;)I

    move-result v11

    iput v11, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastLocationsFileHash:I

    iget-object v11, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-virtual {v11}, Lcom/google/android/apps/sidekick/LocationQueue;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v11, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v11}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v11

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J
    :try_end_2
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v13

    sub-long v7, v11, v13

    const-wide/32 v11, 0x45948

    cmp-long v11, v7, v11

    if-lez v11, :cond_3

    const/4 v10, 0x1

    :cond_2
    :goto_2
    if-eqz v10, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getNetworkLocation()Landroid/location/Location;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v13, "gps"

    invoke-interface {v12, v13}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V

    goto :goto_1

    :cond_3
    const/4 v10, 0x0

    goto :goto_2

    :catchall_0
    move-exception v11

    if-eqz v10, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getNetworkLocation()Landroid/location/Location;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v14, "gps"

    invoke-interface {v13, v14}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v13

    invoke-direct {p0, v12, v13}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V

    :cond_4
    throw v11
.end method

.method private locationChangedSignificantly(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 4
    .param p1    # Landroid/location/Location;
    .param p2    # Landroid/location/Location;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    if-nez p2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_0

    invoke-static {p1, p2}, Lcom/google/android/apps/sidekick/LocationUtilities;->distanceBetween(Landroid/location/Location;Landroid/location/Location;)F

    move-result v2

    const v3, 0x461c4000

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static locationListHashCode(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;)I"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [I

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    invoke-static {v2}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->androidLocationHashCode(Landroid/location/Location;)I

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v2

    return v2
.end method

.method private queueLocation(Landroid/location/Location;)V
    .locals 4
    .param p1    # Landroid/location/Location;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const-string v0, "gmm_network"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocationReceivedMillis:J

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->unregisterFromAndroidProviders()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->updateStationaryStatus(Landroid/location/Location;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v1, "gps"

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "network"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastAndroidLocationReceivedMillis:J

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastAndroidLocationReceivedMillis:J

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGmmLocationReceivedMillis:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xd0bd8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v1, "gps"

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V

    goto :goto_0

    :cond_2
    const-string v0, "gps"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getNetworkLocation()Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->queueLocation(Landroid/location/Location;Landroid/location/Location;)V

    goto :goto_0
.end method

.method private queueLocation(Landroid/location/Location;Landroid/location/Location;)V
    .locals 2
    .param p1    # Landroid/location/Location;
    .param p2    # Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/LocationQueue;->addLocation(Landroid/location/Location;)V

    :cond_0
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/sidekick/LocationQueue;->addLocation(Landroid/location/Location;)V

    :cond_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->writeQueuedLocationsToStorage()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->updateGeofencers(Landroid/location/Location;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->broadcastIfMoved(Landroid/location/Location;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;->reset()V

    :cond_3
    return-void
.end method

.method private registerWithAndroidProviders()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/AndroidLocationReceiver;->createPendingIntent(Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)Landroid/app/PendingIntent;

    move-result-object v5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v1, "network"

    const-wide/32 v2, 0x45948

    const/4 v4, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    const-string v1, "passive"

    const-wide/32 v2, 0x45948

    const/4 v4, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v6

    sget-object v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    const-string v1, "No network provider on device"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v6

    sget-object v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    const-string v1, "No passive provider on device"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private registerWithGmmProvider()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGmmLocationProvider:Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGmmLocationHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x45948

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;->setHandler(Landroid/os/Handler;J)V

    return-void
.end method

.method private startInternal()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STOPPED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STARTED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mSettingsObserver:Lcom/google/android/searchcommon/google/LocationSettings$Observer;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/google/LocationSettings;->addUseLocationObserver(Lcom/google/android/searchcommon/google/LocationSettings$Observer;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->startListening()V

    :cond_0
    return-void
.end method

.method private startListening()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithAndroidProviders()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->registerWithGmmProvider()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    const-wide/32 v2, 0x8b290

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClockListener:Lcom/google/android/searchcommon/util/Clock$TimeResetListener;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Clock;->registerTimeResetListener(Lcom/google/android/searchcommon/util/Clock$TimeResetListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->initializeFromStorage()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationStorage:Lcom/google/android/apps/sidekick/LocationStorage;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    const-string v2, "lastgeofenceloc"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/LocationStorage;->readCurrentLocation(Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGeofenceLocation:Landroid/location/Location;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationStorage:Lcom/google/android/apps/sidekick/LocationStorage;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    const-string v2, "lastloc"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/LocationStorage;->readCurrentLocation(Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastSignificantLocationChange:Landroid/location/Location;

    return-void
.end method

.method private startLocked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkHoldsLock(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/sidekick/LocationOracleImpl$2;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBgHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl$3;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private stop()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBgHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl$4;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private stopListening()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-direct {p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->updateGeofencers(Landroid/location/Location;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->broadcastIfMoved(Landroid/location/Location;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->STARTED:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->unregisterFromAndroidProviders()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGmmLocationProvider:Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;->stopUpdates()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationWatchDog:Lcom/google/android/apps/sidekick/LocationOracleImpl$LocationWatchDog;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClockListener:Lcom/google/android/searchcommon/util/Clock$TimeResetListener;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Clock;->unregisterTimeResetListener(Lcom/google/android/searchcommon/util/Clock$TimeResetListener;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mPurgeLocationsTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugLocations:Ljava/util/List;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/LocationQueue;->clearLocations()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    const-string v1, "loracle"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/FileBytesWriter;->deleteFile(Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private unregisterFromAndroidProviders()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationManager:Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    invoke-static {v1}, Lcom/google/android/apps/sidekick/AndroidLocationReceiver;->createPendingIntent(Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;->removeUpdates(Landroid/app/PendingIntent;)V

    return-void
.end method

.method private updateGeofencers(Landroid/location/Location;)V
    .locals 5
    .param p1    # Landroid/location/Location;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mState:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v3, Lcom/google/android/apps/sidekick/LocationOracleImpl$State;->LISTENING:Lcom/google/android/apps/sidekick/LocationOracleImpl$State;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/util/StateMachine;->checkIn(Ljava/lang/Enum;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGeofenceLocation:Landroid/location/Location;

    if-nez v2, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGeofenceLocation:Landroid/location/Location;

    if-eqz v2, :cond_2

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGeofenceLocation:Landroid/location/Location;

    invoke-static {v2, p1}, Lcom/google/android/apps/sidekick/LocationUtilities;->areLocationsEqual(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGeofencers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGeofenceLocation:Landroid/location/Location;

    invoke-interface {v0, v2, p1}, Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;->onLocationChanged(Landroid/location/Location;Landroid/location/Location;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_3
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationStorage:Lcom/google/android/apps/sidekick/LocationStorage;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v3

    const-string v4, "lastgeofenceloc"

    invoke-virtual {v2, p1, v3, v4}, Lcom/google/android/apps/sidekick/LocationStorage;->saveCurrentLocation(Landroid/location/Location;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastGeofenceLocation:Landroid/location/Location;

    goto :goto_0
.end method

.method private updateStationaryStatus(Landroid/location/Location;)V
    .locals 3
    .param p1    # Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "travelState"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    const-string v1, "stationary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFirstStationaryLocationFromGMM:Landroid/location/Location;

    if-nez v1, :cond_1

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFirstStationaryLocationFromGMM:Landroid/location/Location;

    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->broadcastTravelState()V

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v1, "moving"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFirstStationaryLocationFromGMM:Landroid/location/Location;

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFirstStationaryLocationFromGMM:Landroid/location/Location;

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_2
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private writeQueuedLocationsToStorage()V
    .locals 11

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mBgThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v7}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/LocationQueue;->getRawLocations()Ljava/util/List;

    move-result-object v7

    invoke-static {v7}, Lcom/google/common/collect/Lists;->reverse(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->locationListHashCode(Ljava/util/List;)I

    move-result v5

    iget v7, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastLocationsFileHash:I

    if-ne v5, v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/location/Location;

    invoke-static {v4}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->androidLocationToProto(Landroid/location/Location;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v1, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;-><init>()V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;->addLocation(Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;)Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;

    goto :goto_2

    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFileWriter:Lcom/google/android/apps/sidekick/FileBytesWriter;

    const-string v8, "loracle"

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/LocationOracleStore$LocationOracleData;->toByteArray()[B

    move-result-object v9

    const/high16 v10, 0x80000

    invoke-virtual {v7, v8, v9, v10}, Lcom/google/android/apps/sidekick/FileBytesWriter;->writeEncryptedFileBytes(Ljava/lang/String;[BI)Z

    move-result v7

    if-eqz v7, :cond_0

    iput v5, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLastLocationsFileHash:I

    goto :goto_0
.end method


# virtual methods
.method public addLightweightGeofencer(Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mGeofencers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public blockingUpdateBestLocation()Landroid/location/Location;
    .locals 1

    new-instance v0, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl$5;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->postAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    return-object v0
.end method

.method clearTestStationaryTimeSecs()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugStationaryTimeSecs:Ljava/lang/Integer;

    return-void
.end method

.method public getBestLocation()Landroid/location/Location;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBestLocations()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    const-string v2, "Best location was null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBestLocations()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugLocations:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugLocations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugLocations:Ljava/util/List;

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLocationQueue:Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/LocationQueue;->getBestLocations()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public hasLocation()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBestLocations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newRunningLock(Ljava/lang/String;)Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$1;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Ljava/lang/String;)V

    return-object v0
.end method

.method public phoneIsStationary()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugStationaryTimeSecs:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFirstStationaryLocationFromGMM:Landroid/location/Location;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method postAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBgHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    const-string v3, "Unexpected interruption"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v2, Lcom/google/android/apps/sidekick/LocationOracleImpl;->TAG:Ljava/lang/String;

    const-string v3, "Unexpected exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method postAndroidLocation(Landroid/location/Location;)V
    .locals 2
    .param p1    # Landroid/location/Location;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBgHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$7;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$7;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Landroid/location/Location;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method pushTestLocations(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBgHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl$8;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public requestRecentLocation(J)V
    .locals 5
    .param p1    # J

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-gtz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->getBgHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/apps/sidekick/LocationOracleImpl$6;-><init>(Lcom/google/android/apps/sidekick/LocationOracleImpl;J)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method setTestStationaryTimeSecs(I)V
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugStationaryTimeSecs:Ljava/lang/Integer;

    return-void
.end method

.method public stationaryTimeSeconds()I
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugFeatures:Lcom/google/android/searchcommon/debug/DebugFeatures;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugStationaryTimeSecs:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mDebugStationaryTimeSecs:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFirstStationaryLocationFromGMM:Landroid/location/Location;

    if-eqz v0, :cond_1

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/LocationOracleImpl;->mFirstStationaryLocationFromGMM:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    long-to-int v0, v2

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0
.end method
