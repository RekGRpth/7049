.class public final Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "LocationOracleStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/LocationOracleStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AndroidLocationProto"
.end annotation


# instance fields
.field private accuracyMeters_:F

.field private cachedSize:I

.field private hasAccuracyMeters:Z

.field private hasLat:Z

.field private hasLng:Z

.field private hasNetworkLocationType:Z

.field private hasProvider:Z

.field private hasTimestampMillis:Z

.field private hasTravelState:Z

.field private lat_:D

.field private lng_:D

.field private networkLocationType_:Ljava/lang/String;

.field private provider_:Ljava/lang/String;

.field private timestampMillis_:J

.field private travelState_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->provider_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->lat_:D

    iput-wide v1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->lng_:D

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->accuracyMeters_:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->timestampMillis_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->travelState_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->networkLocationType_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAccuracyMeters()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->accuracyMeters_:F

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->cachedSize:I

    return v0
.end method

.method public getLat()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->lat_:D

    return-wide v0
.end method

.method public getLng()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->lng_:D

    return-wide v0
.end method

.method public getNetworkLocationType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->networkLocationType_:Ljava/lang/String;

    return-object v0
.end method

.method public getProvider()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->provider_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasProvider()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasLat()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getLat()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasLng()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getLng()D

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasAccuracyMeters()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getAccuracyMeters()F

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTimestampMillis()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getTimestampMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTravelState()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getTravelState()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasNetworkLocationType()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getNetworkLocationType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->cachedSize:I

    return v0
.end method

.method public getTimestampMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->timestampMillis_:J

    return-wide v0
.end method

.method public getTravelState()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->travelState_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAccuracyMeters()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasAccuracyMeters:Z

    return v0
.end method

.method public hasLat()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasLat:Z

    return v0
.end method

.method public hasLng()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasLng:Z

    return v0
.end method

.method public hasNetworkLocationType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasNetworkLocationType:Z

    return v0
.end method

.method public hasProvider()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasProvider:Z

    return v0
.end method

.method public hasTimestampMillis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTimestampMillis:Z

    return v0
.end method

.method public hasTravelState()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTravelState:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setProvider(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setLat(D)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setLng(D)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFloat()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setAccuracyMeters(F)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setTimestampMillis(J)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setTravelState(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->setNetworkLocationType(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;

    move-result-object v0

    return-object v0
.end method

.method public setAccuracyMeters(F)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasAccuracyMeters:Z

    iput p1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->accuracyMeters_:F

    return-object p0
.end method

.method public setLat(D)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasLat:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->lat_:D

    return-object p0
.end method

.method public setLng(D)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasLng:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->lng_:D

    return-object p0
.end method

.method public setNetworkLocationType(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasNetworkLocationType:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->networkLocationType_:Ljava/lang/String;

    return-object p0
.end method

.method public setProvider(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasProvider:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->provider_:Ljava/lang/String;

    return-object p0
.end method

.method public setTimestampMillis(J)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTimestampMillis:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->timestampMillis_:J

    return-object p0
.end method

.method public setTravelState(Ljava/lang/String;)Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTravelState:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->travelState_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasProvider()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasLat()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getLat()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasLng()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getLng()D

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasAccuracyMeters()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getAccuracyMeters()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFloat(IF)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTimestampMillis()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getTimestampMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasTravelState()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getTravelState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->hasNetworkLocationType()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/LocationOracleStore$AndroidLocationProto;->getNetworkLocationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    return-void
.end method
