.class Lcom/google/android/apps/sidekick/LocationQueue;
.super Ljava/lang/Object;
.source "LocationQueue.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mLocations:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/LocationQueue;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/LocationQueue;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/util/Clock;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method


# virtual methods
.method declared-synchronized addLocation(Landroid/location/Location;)V
    .locals 13
    .param p1    # Landroid/location/Location;

    const-wide/16 v11, 0x3e8

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    const-wide/16 v9, 0x1f4

    add-long/2addr v7, v9

    const-wide/16 v9, 0x3e8

    div-long v5, v7, v9

    mul-long v7, v5, v11

    invoke-virtual {p1, v7, v8}, Landroid/location/Location;->setTime(J)V

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v7}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v7

    const-wide/32 v9, 0x124f80

    sub-long v2, v7, v9

    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/Location;

    invoke-virtual {v7}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    cmp-long v7, v7, v2

    if-gez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    :cond_3
    const/4 v4, 0x1

    const/4 v1, 0x0

    :goto_2
    :try_start_2
    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-ge v1, v7, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-nez v7, :cond_6

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v8

    cmpg-float v7, v7, v8

    if-gez v7, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7, v1, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    :cond_4
    const/4 v4, 0x0

    :cond_5
    :goto_3
    if-eqz v4, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    :cond_6
    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-gez v7, :cond_7

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7, v1, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v4, 0x0

    goto :goto_3

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method declared-synchronized clearLocations()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getBestLocation()Landroid/location/Location;
    .locals 2

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/LocationQueue;->getBestLocationsInIntervals(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method declared-synchronized getBestLocations()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/sidekick/LocationQueue;->getBestLocationsInIntervals(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    if-eqz v1, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_1
    monitor-exit p0

    return-object v2
.end method

.method declared-synchronized getBestLocationsInIntervals(I)Ljava/util/List;
    .locals 13
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    const-wide/32 v11, 0x493e0

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v7}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v5

    const/4 v0, 0x0

    const/4 v7, 0x3

    if-le p1, v7, :cond_0

    const/4 p1, 0x3

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/Location;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-lt v7, p1, :cond_6

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-ge v7, p1, :cond_3

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :cond_3
    const/4 v4, 0x1

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/Location;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_4

    const/4 v4, 0x0

    :cond_5
    monitor-exit p0

    return-object v1

    :cond_6
    :try_start_1
    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    cmp-long v7, v7, v5

    if-gtz v7, :cond_1

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    sub-long v9, v5, v11

    cmp-long v7, v7, v9

    if-gez v7, :cond_8

    if-eqz v0, :cond_7

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :cond_7
    sub-long/2addr v5, v11

    const/4 v0, 0x0

    :cond_8
    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    const/high16 v8, 0x42c80000

    cmpg-float v7, v7, v8

    if-gez v7, :cond_a

    if-eqz v3, :cond_9

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :cond_9
    sub-long/2addr v5, v11

    const/4 v0, 0x0

    goto :goto_0

    :cond_a
    if-nez v0, :cond_b

    move-object v0, v3

    goto :goto_0

    :cond_b
    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1

    move-object v0, v3

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7
.end method

.method declared-synchronized getRawLocations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/LocationQueue;->mLocations:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
