.class Lcom/google/android/apps/sidekick/LocationStorage;
.super Ljava/lang/Object;
.source "LocationStorage.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mSignedCipherHelper:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/LocationStorage;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/LocationStorage;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/LocationStorage;->mSignedCipherHelper:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    return-void
.end method


# virtual methods
.method bytesToLocation([B)Landroid/location/Location;
    .locals 5
    .param p1    # [B

    array-length v3, p1

    const/16 v4, 0x14

    if-eq v3, v4, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v2, 0x0

    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Landroid/location/Location;->setLongitude(D)V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v3

    invoke-virtual {v1, v3}, Landroid/location/Location;->setAccuracy(F)V

    goto :goto_0
.end method

.method decryptLocation([B)Landroid/location/Location;
    .locals 2
    .param p1    # [B

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationStorage;->mSignedCipherHelper:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    invoke-interface {v1, p1}, Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;->decryptBytes([B)[B

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/LocationStorage;->bytesToLocation([B)Landroid/location/Location;

    move-result-object v1

    goto :goto_0
.end method

.method encryptLocation(Landroid/location/Location;)[B
    .locals 2
    .param p1    # Landroid/location/Location;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/LocationStorage;->locationToBytes(Landroid/location/Location;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/LocationStorage;->mSignedCipherHelper:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    invoke-interface {v1, v0}, Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;->encryptBytes([B)[B

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method locationToBytes(Landroid/location/Location;)[B
    .locals 4
    .param p1    # Landroid/location/Location;

    const/16 v2, 0x14

    new-array v1, v2, [B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    return-object v1
.end method

.method readCurrentLocation(Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;)Landroid/location/Location;
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {p1, p2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBytes(Ljava/lang/String;[B)[B

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/LocationStorage;->decryptLocation([B)Landroid/location/Location;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v3, Lcom/google/android/apps/sidekick/LocationStorage;->TAG:Ljava/lang/String;

    const-string v4, "Clearing bad lastloc from prefs"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v3, "lastloc"

    invoke-interface {v0, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    :cond_0
    return-object v2
.end method

.method saveCurrentLocation(Landroid/location/Location;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/location/Location;
    .param p2    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .param p3    # Ljava/lang/String;

    invoke-interface {p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    if-nez p1, :cond_0

    invoke-interface {v1, p3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    :goto_0
    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/LocationStorage;->encryptLocation(Landroid/location/Location;)[B

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v2, Lcom/google/android/apps/sidekick/LocationStorage;->TAG:Ljava/lang/String;

    const-string v3, "error writing sidekick location (crypto fail)"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    invoke-interface {v1, p3, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0
.end method
