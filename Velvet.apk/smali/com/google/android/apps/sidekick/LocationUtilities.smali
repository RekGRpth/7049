.class public Lcom/google/android/apps/sidekick/LocationUtilities;
.super Ljava/lang/Object;
.source "LocationUtilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 3
    .param p0    # Landroid/location/Location;

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$Location;->setLat(D)Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/geo/sidekick/Sidekick$Location;->setLng(D)Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public static areLocationsEqual(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 4
    .param p0    # Landroid/location/Location;
    .param p1    # Landroid/location/Location;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static distanceBetween(DDDD)F
    .locals 9
    .param p0    # D
    .param p2    # D
    .param p4    # D
    .param p6    # D

    const/4 v0, 0x1

    new-array v8, v0, [F

    move-wide v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move-wide v6, p6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method static distanceBetween(Landroid/location/Location;Landroid/location/Location;)F
    .locals 9
    .param p0    # Landroid/location/Location;
    .param p1    # Landroid/location/Location;

    const/4 v0, 0x1

    new-array v8, v0, [F

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public static getLocalDistanceUnits()Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;
    .locals 2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    sget-object v1, Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;->MILES:Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;

    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;->KILOMETERS:Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;

    goto :goto_0
.end method

.method public static locationToTimestampedLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;
    .locals 8
    .param p0    # Landroid/location/Location;

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;-><init>()V

    new-instance v4, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/geo/sidekick/Sidekick$Location;->setLat(D)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/geo/sidekick/Sidekick$Location;->setLng(D)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    move-result-object v3

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    move-result-object v3

    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setProvider(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    move-result-object v2

    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "networkLocationType"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, v1}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setNetworkLocationType(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    :cond_0
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;->setAccuracyMeters(I)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    :cond_1
    return-object v2
.end method

.method public static locationsToTimestampedLocations(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    if-nez v1, :cond_0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcom/google/android/apps/sidekick/LocationUtilities;->locationToTimestampedLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$TimestampedLocation;

    move-result-object v3

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method public static sidekickLocationToAndroidLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Landroid/location/Location;
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Location;

    new-instance v0, Landroid/location/Location;

    const-string v1, "unknown"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    return-object v0
.end method

.method public static toLocalDistanceUnits(I)D
    .locals 6
    .param p0    # I

    invoke-static {}, Lcom/google/android/apps/sidekick/LocationUtilities;->getLocalDistanceUnits()Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;->KILOMETERS:Lcom/google/android/apps/sidekick/LocationUtilities$DistanceUnit;

    if-ne v2, v3, :cond_0

    const-wide v0, 0x408f400000000000L

    :goto_0
    const-wide/high16 v2, 0x3ff0000000000000L

    div-double/2addr v2, v0

    int-to-double v4, p0

    mul-double/2addr v2, v4

    return-wide v2

    :cond_0
    const-wide v0, 0x409925604189374cL

    goto :goto_0
.end method
