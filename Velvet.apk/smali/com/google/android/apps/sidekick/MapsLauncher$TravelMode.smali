.class public final enum Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;
.super Ljava/lang/Enum;
.source "MapsLauncher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/MapsLauncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TravelMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

.field public static final enum BIKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

.field public static final enum DRIVING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

.field public static final enum TRANSIT:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

.field public static final enum WALKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;


# instance fields
.field private final mTemplate:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    const-string v1, "DRIVING"

    const-string v2, "http://maps.google.com/maps/?saddr=%s,%s&daddr=%s,%s&layer=t&dirflg=d"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->DRIVING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    new-instance v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    const-string v1, "WALKING"

    const-string v2, "http://maps.google.com/maps/?saddr=%s,%s&daddr=%s,%s&dirflg=w"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->WALKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    new-instance v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    const-string v1, "TRANSIT"

    const-string v2, "http://maps.google.com/maps/?saddr=%s,%s&daddr=%s,%s&dirflg=r"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->TRANSIT:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    new-instance v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    const-string v1, "BIKING"

    const-string v2, "http://maps.google.com/maps/?saddr=%s,%s&daddr=%s,%s&dirflg=b"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->BIKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    sget-object v1, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->DRIVING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->WALKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->TRANSIT:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->BIKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->$VALUES:[Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->mTemplate:Ljava/lang/String;

    return-void
.end method

.method public static fromSidekickProtoTravelMode(I)Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;
    .locals 2
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown travel type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->DRIVING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->TRANSIT:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->WALKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->BIKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->$VALUES:[Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    invoke-virtual {v0}, [Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    return-object v0
.end method


# virtual methods
.method public getTemplate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->mTemplate:Ljava/lang/String;

    return-object v0
.end method
