.class public Lcom/google/android/apps/sidekick/MapsLauncher;
.super Ljava/lang/Object;
.source "MapsLauncher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/MapsLauncher;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/MapsLauncher;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static buildMapsUri(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    invoke-interface {p0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-static {p1, v0, p2}, Lcom/google/android/apps/sidekick/MapsLauncher;->buildMapsUrlString(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method

.method static buildMapsUrlForLocationAndQuery(Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p1    # Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "http://maps.google.com/maps/?q=%s,%s(%s)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static buildMapsUrlString(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;)Ljava/lang/String;
    .locals 8
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-nez p0, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/sidekick/MapsLauncher;->buildMapsUrlForLocationAndQuery(Lcom/google/geo/sidekick/Sidekick$Location;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "http://maps.google.com/maps/?q=%s,%s"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->getTemplate()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static buildWalkingDirectionsUri(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;

    sget-object v0, Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;->WALKING:Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/sidekick/MapsLauncher;->buildMapsUrlString(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static start(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v0, 0x0

    invoke-static {p0, p1, v0, v0}, Lcom/google/android/apps/sidekick/MapsLauncher;->start(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;)V

    return-void
.end method

.method public static start(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;

    invoke-static {p1, p2, p3}, Lcom/google/android/apps/sidekick/MapsLauncher;->buildMapsUri(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/MapsLauncher$TravelMode;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/sidekick/MapsLauncher;->startUsingUri(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    return-void
.end method

.method public static startUsingUri(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    if-nez p1, :cond_0

    sget-object v0, Lcom/google/android/apps/sidekick/MapsLauncher;->TAG:Ljava/lang/String;

    const-string v1, "uri was null when try to launch navigation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f0d039a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    const v1, 0x7f0d0398

    invoke-interface {p2, p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeViewUriWithMessage(Landroid/content/Context;Landroid/net/Uri;ZI)Z

    goto :goto_0
.end method
