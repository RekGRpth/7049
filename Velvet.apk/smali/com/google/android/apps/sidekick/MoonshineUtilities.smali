.class public Lcom/google/android/apps/sidekick/MoonshineUtilities;
.super Ljava/lang/Object;
.source "MoonshineUtilities.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEffectiveGmailReferenceAndSetText(Landroid/content/Context;Landroid/widget/Button;Ljava/util/List;)Lcom/google/geo/sidekick/Sidekick$GmailReference;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/widget/Button;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/Button;",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;)",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;"
        }
    .end annotation

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$GmailReference;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->getEmailUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->getSenderEmailAddress()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    const v1, 0x7f0d0245

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->getSenderEmailAddress()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
