.class Lcom/google/android/apps/sidekick/MovieRow$2;
.super Ljava/lang/Object;
.source "MovieRow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/MovieRow;->populateView(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/MovieRow;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/MovieRow;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/MovieRow$2;->this$0:Lcom/google/android/apps/sidekick/MovieRow;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/MovieRow$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/MovieRow$2;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/MovieRow$2;->this$0:Lcom/google/android/apps/sidekick/MovieRow;

    # getter for: Lcom/google/android/apps/sidekick/MovieRow;->mEntryItemAdapter:Lcom/google/android/apps/sidekick/BaseEntryAdapter;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/MovieRow;->access$000(Lcom/google/android/apps/sidekick/MovieRow;)Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/MovieRow$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/MovieRow$2;->this$0:Lcom/google/android/apps/sidekick/MovieRow;

    # getter for: Lcom/google/android/apps/sidekick/MovieRow;->mEntryItemAdapter:Lcom/google/android/apps/sidekick/BaseEntryAdapter;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/MovieRow;->access$000(Lcom/google/android/apps/sidekick/MovieRow;)Lcom/google/android/apps/sidekick/BaseEntryAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/MovieRow$2;->val$url:Ljava/lang/String;

    const-string v4, "MOVIE_RATING_LINK"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
