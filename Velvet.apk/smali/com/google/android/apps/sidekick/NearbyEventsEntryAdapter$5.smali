.class Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$5;
.super Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;
.source "NearbyEventsEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V
    .locals 0
    .param p2    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;

    invoke-direct {p0, p2}, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    return-void
.end method


# virtual methods
.method public getQuestionCount(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;

    # invokes: Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->isCardExpanded(Landroid/view/View;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->getQuestionCount(Landroid/view/View;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEventEntry()Lcom/google/geo/sidekick/Sidekick$EventEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EventEntry;->getTitle()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;

    # invokes: Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->isCardExpanded(Landroid/view/View;)Z
    invoke-static {v0, p2}, Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/NearbyEventsEntryAdapter;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d02ea

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
