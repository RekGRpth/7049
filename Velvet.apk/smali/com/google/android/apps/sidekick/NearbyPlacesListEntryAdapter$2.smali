.class Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;
.super Ljava/lang/Object;
.source "NearbyPlacesListEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->addPlaceViewFromEntry(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

.field final synthetic val$businessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$placeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$BusinessData;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->val$businessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->val$placeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->val$businessData:Lcom/google/geo/sidekick/Sidekick$BusinessData;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCid()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;
    invoke-static {v4}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;-><init>(Landroid/content/Context;JLcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->run()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    const-string v3, "DETAILS"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->val$placeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const/16 v1, 0x14

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v6

    if-eqz v6, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;)Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$2;->val$placeEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method
