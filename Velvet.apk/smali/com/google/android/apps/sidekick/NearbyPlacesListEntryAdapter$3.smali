.class Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$3;
.super Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;
.source "NearbyPlacesListEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V
    .locals 0
    .param p2    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    invoke-direct {p0, p2}, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V

    return-void
.end method


# virtual methods
.method public getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;

    # invokes: Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->getPlaceData(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    invoke-static {v2, v1}, Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/NearbyPlacesListEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    if-nez v0, :cond_0

    const v2, 0x7f0d00e9

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const v0, 0x7f0d02ea

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
