.class public Lcom/google/android/apps/sidekick/NotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NotificationReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

.field private mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

.field private mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/NotificationReceiver;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/NotificationReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private autoDismiss(Landroid/content/Context;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)Z
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v7}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v4

    const v7, 0x7f0d0054

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0d0057

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const v7, 0x7f0d0059

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$Action;-><init>()V

    const/4 v7, 0x2

    invoke-virtual {v0, v7}, Lcom/google/geo/sidekick/Sidekick$Action;->setType(I)Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getEntries()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$Entry;

    new-instance v1, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-direct {v1, v7, p1, v2, v0}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    new-array v7, v6, [Ljava/lang/Void;

    invoke-virtual {v1, v7}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    const/4 v6, 0x1

    :cond_1
    return v6
.end method

.method private initializeDependencies(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    if-nez v2, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNowNotificationManager()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    if-nez v2, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    if-nez v2, :cond_3

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    if-nez v2, :cond_4

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    :cond_4
    return-void
.end method

.method private logNotificationAction(Landroid/content/Context;Landroid/content/Intent;Ljava/util/Collection;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    const-string v2, "notificationLogActionKey"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "notificationLoggingNameKey"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v3, "NOTIFICATION_ACTION_PRESS"

    invoke-virtual {v2, v3, v1, p3, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryNotificationWithLabel(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;)V

    return-void
.end method

.method private showTrafficNotification(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    const-string v7, "notification_entry"

    invoke-static {p2, v7}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    const-string v7, "location_key"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/LocationUtilities;->androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    invoke-interface {v7, v1, v4, v4}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_0

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/sidekick/NotificationReceiver;->autoDismiss(Landroid/content/Context;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v3, v6

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v7, v3, v6}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->createNotification(Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationType()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    sget-object v7, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->TRAFFIC_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-interface {v6, v7}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->showNotification(Landroid/app/Notification;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->isActiveNotification()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v6, v1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->sendDeliverActiveNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    sget-object v7, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-interface {v6, v7}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    goto :goto_2
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/NotificationReceiver;->initializeDependencies(Landroid/content/Context;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v5, "com.google.android.apps.sidekick.NOTIFICATION_ENTRY_ACTION"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/NotificationReceiver;->showTrafficNotification(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v5, "com.google.android.apps.sidekick.NOTIFICATION_DISMISS_ACTION"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "notificationEntriesKey"

    invoke-static {p2, v5}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    sget-object v5, Lcom/google/android/apps/sidekick/NotificationReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Received notification dismiss without entries!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v5, "notificationIdKey"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    if-nez v4, :cond_5

    sget-object v5, Lcom/google/android/apps/sidekick/NotificationReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Received notification dismiss without notification type!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v5, v3, v4}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->dismissNotification(Ljava/util/Collection;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    const-string v5, "notificationDismissCallback"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    goto :goto_0

    :cond_6
    const-string v5, "com.google.android.apps.sidekick.NOTIFICATION_CALLBACK_ACTION"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "notificationEntriesKey"

    invoke-static {p2, v5}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntriesFromIntent(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_7
    sget-object v5, Lcom/google/android/apps/sidekick/NotificationReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Received notification action press without entries!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_8
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v5, "notificationIdKey"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    if-nez v4, :cond_9

    sget-object v5, Lcom/google/android/apps/sidekick/NotificationReceiver;->TAG:Ljava/lang/String;

    const-string v6, "Received notification action press without notification type!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    iget-object v5, p0, Lcom/google/android/apps/sidekick/NotificationReceiver;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    invoke-interface {v5, v4}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    invoke-direct {p0, p1, p2, v3}, Lcom/google/android/apps/sidekick/NotificationReceiver;->logNotificationAction(Landroid/content/Context;Landroid/content/Intent;Ljava/util/Collection;)V

    const-string v5, "callback_type"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "notification_callback"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    if-eqz v1, :cond_a

    if-nez v2, :cond_b

    :cond_a
    sget-object v5, Lcom/google/android/apps/sidekick/NotificationReceiver;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received notification action press with unknown callback: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    const-string v5, "activity"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    const-string v5, "broadcast"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_d
    const-string v5, "service"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
