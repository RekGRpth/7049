.class Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;
.super Ljava/lang/Object;
.source "PackageTrackingEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getPackageStatusUpdatesEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasOrderId()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getOrderId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    invoke-static {v4}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    # invokes: Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getShopperIntent(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;
    invoke-static {v3, v4, v2}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->access$300(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-interface {v3, v4, v1}, Lcom/google/android/searchcommon/util/IntentUtils;->isIntentHandled(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openUrlForButton(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openUrlForButton(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openUrlForButton(Landroid/content/Context;)V

    goto :goto_0
.end method
