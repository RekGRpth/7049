.class public Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "PackageTrackingEntryAdapter.java"


# instance fields
.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p4    # Lcom/google/android/searchcommon/util/IntentUtils;
    .param p5    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getPackageTrackingEntry()Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;
    .param p1    # Landroid/accounts/Account;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getShopperIntent(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-object v0
.end method

.method private colorSubstring(Landroid/content/Context;Ljava/lang/String;III)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v2, 0x11

    invoke-virtual {v1, v0, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v1
.end method

.method private estimatedArrivalDateText(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->isToday()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0305

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getEstimatedDeliverySecs()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getEstimatedDeliveryDateColor(Landroid/content/Context;)I
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->isToday()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f09003a

    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    :cond_0
    const v0, 0x7f090039

    goto :goto_0
.end method

.method private getItemFromText(ILandroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110005

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getShopperIntent(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1    # Landroid/accounts/Account;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.shopper.intent.action.ORDER_DETAILS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.apps.shopper"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "order_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_for_order"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private getStatus(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d023e

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatus()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStatusUpdateColor()Ljava/lang/Integer;
    .locals 3

    const v2, 0x7f09003a

    const v1, 0x7f090038

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatusCode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatusCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    const v0, 0x7f090030

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getStatusUpdateText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 12
    .param p1    # Landroid/content/Context;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatus()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getLastUpdateTimeSecs()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v6, v0, v8

    invoke-static {p1, v6, v7}, Lcom/google/android/apps/sidekick/TimeUtilities;->getElapsedString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0d0306

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getStatusUpdateColor()Ljava/lang/Integer;

    move-result-object v11

    if-nez v11, :cond_1

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatus()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->colorSubstring(Landroid/content/Context;Ljava/lang/String;III)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private getTimeString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # J

    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p2

    const/16 v2, 0x12

    invoke-static {p1, v0, v1, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isToday()Z
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getEstimatedDeliverySecs()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    return v2
.end method

.method private openCardDetailsPage(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasSecondaryPageUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrlRequiresGaiaLogin()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PACKAGE_TRACKING_EMAIL_WEB_VIEW"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openInWebView(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->logDetailsInteraction(Landroid/content/Context;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrl()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PACKAGE_TRACKING_EMAIL_BROWSER"

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private openInWebView(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v0, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v8, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->ALL_URL_PREFIXES:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v10

    move-object v1, p1

    move-object v2, p2

    move-object v5, p0

    move-object v6, p3

    move-object v9, v7

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    invoke-virtual {v0, v7}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;->onClick(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d023b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0239

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d023a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02c2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/PackageTrackingCardSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getStatus(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0d0238

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTitleColor(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasStatusCode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getStatusCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 34
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v3, 0x7f040087

    const/4 v4, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    const v3, 0x7f1001a3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/TextView;

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getStatusUpdateText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getItemsCount()I

    move-result v18

    if-lez v18, :cond_1

    const v3, 0x7f1001a4

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getItems(I)Lcom/google/geo/sidekick/Sidekick$PackageItem;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/google/geo/sidekick/Sidekick$PackageItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c008a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    move/from16 v31, v0

    const/16 v3, 0xdc

    move/from16 v0, v31

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v31

    invoke-virtual/range {v29 .. v29}, Lcom/google/geo/sidekick/Sidekick$PackageItem;->hasPhoto()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual/range {v29 .. v29}, Lcom/google/geo/sidekick/Sidekick$PackageItem;->getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrlType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    const v3, 0x7f1001a5

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual/range {v29 .. v29}, Lcom/google/geo/sidekick/Sidekick$PackageItem;->getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v3

    move/from16 v0, v31

    move/from16 v1, v31

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/sidekick/FifeImageUrlUtil;->setImageUrlCenterCrop(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    const/4 v3, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasFromAddress()Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f1001a7

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getItemFromText(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v3, 0x7f1001a8

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getFromAddress()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasEstimatedDeliverySecs()Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f1001a9

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    const v3, 0x7f0d0304

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v3, 0x7f1001aa

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->estimatedArrivalDateText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getEstimatedDeliveryDateColor(Landroid/content/Context;)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasTrackingUrl()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const v3, 0x7f1001ab

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Landroid/widget/Button;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getSecondaryPageUrlTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v3, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;Landroid/content/Context;)V

    invoke-virtual {v15, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v3, 0x7f10002f

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getGmailReferenceList()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/sidekick/MoonshineUtilities;->getEffectiveGmailReferenceAndSetText(Landroid/content/Context;Landroid/widget/Button;Ljava/util/List;)Lcom/google/geo/sidekick/Sidekick$GmailReference;

    move-result-object v23

    if-eqz v23, :cond_3

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v3, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    invoke-virtual/range {v23 .. v23}, Lcom/google/geo/sidekick/Sidekick$GmailReference;->getEmailUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const-string v9, "PACKAGE_TRACKING_EMAIL"

    const-string v10, "mail"

    sget-object v11, Lcom/google/android/apps/sidekick/GoogleServiceWebviewWrapper;->GMAIL_URL_PREFIXES:[Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v13

    move-object/from16 v4, p1

    move-object/from16 v8, p0

    invoke-direct/range {v3 .. v13}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-object v16

    :cond_4
    const v3, 0x7f1001a6

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasFromAddress()Z

    move-result v3

    if-eqz v3, :cond_5

    const v3, 0x7f1001a7

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getItemFromText(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getFromAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasEstimatedDeliverySecs()Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f1001a9

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    const v3, 0x7f0d0304

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    new-instance v30, Ljava/lang/StringBuilder;

    const v3, 0x7f0d0304

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->estimatedArrivalDateText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    const v8, 0x7f090039

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->colorSubstring(Landroid/content/Context;Ljava/lang/String;III)Ljava/lang/CharSequence;

    move-result-object v17

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected getViewToFocusForDetails(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    const v0, 0x7f1001a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public launchDetails(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getPackageStatusUpdatesEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->hasOrderId()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getOrderId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-direct {p0, v3, v2}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getShopperIntent(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    invoke-interface {v3, p1, v1}, Lcom/google/android/searchcommon/util/IntentUtils;->isIntentHandled(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openCardDetailsPage(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openCardDetailsPage(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openCardDetailsPage(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method openUrlForButton(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingButtonUrlRequiresGaiaLogin()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PACKAGE_TRACKING_EMAIL_WEB_VIEW"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openInWebView(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->mPackageTrackingEntry:Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$PackageTrackingEntry;->getTrackingUrl()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PACKAGE_TRACKING_EMAIL_BROWSER"

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/apps/sidekick/PackageTrackingEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
