.class Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;
.super Ljava/lang/Object;
.source "PhotoSpotThumbnailGrid.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->displayThumbnails(Ljava/util/List;Lcom/google/geo/sidekick/Sidekick$Location;Landroid/view/LayoutInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

.field final synthetic val$currentIndex:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    iput p2, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;->val$currentIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->getContext()Landroid/content/Context;

    move-result-object v1

    # invokes: Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->setupGalleryImages(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->access$100(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;Landroid/content/Context;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "photos_uri"

    const-string v2, "content://com.google.android.velvet.gallery.ImageProvider/images"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "projection"

    # getter for: Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->PHOTO_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->access$200()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_index"

    iget v2, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;->val$currentIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "max_scale"

    const/high16 v2, 0x40800000

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;->this$0:Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
