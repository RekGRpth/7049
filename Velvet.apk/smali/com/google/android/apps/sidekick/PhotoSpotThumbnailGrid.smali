.class public Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;
.super Landroid/widget/TableLayout;
.source "PhotoSpotThumbnailGrid.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;
    }
.end annotation


# static fields
.field private static final PHOTO_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mImageListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ui/WebImageView$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mInvalidIndices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPhotoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;",
            ">;"
        }
    .end annotation
.end field

.field private mSourceLocation:Lcom/google/geo/sidekick/Sidekick$Location;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contentUri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "thumbnailUri"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "contentType"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "loadingIndicator"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "domain"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "source"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "nav_uri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->PHOTO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;Landroid/content/Context;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->setupGalleryImages(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->PHOTO_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mImageListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mInvalidIndices:Ljava/util/List;

    return-object v0
.end method

.method private getPhotoCount()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xc

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private setupGalleryImages(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getImageMetadataController()Lcom/google/android/velvet/gallery/ImageMetadataController;

    move-result-object v1

    sget-object v2, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->buildVelvetImages()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/velvet/gallery/ImageMetadataController;->setQueryWithImages(Lcom/google/android/velvet/Query;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method buildVelvetImages()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->getPhotoCount()I

    move-result v5

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;

    new-instance v2, Lcom/google/android/velvet/gallery/VelvetImage;

    invoke-direct {v2}, Lcom/google/android/velvet/gallery/VelvetImage;-><init>()V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setId(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setName(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;->getThumbnail()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setThumbnailUri(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;->getMediumSizedPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setUri(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getWidth()I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setWidth(I)V

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getHeight()I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setHeight(I)V

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasPhotoAttribution()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getPhotoAttribution()Lcom/google/geo/sidekick/Sidekick$Attribution;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Attribution;->hasTitle()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getPhotoAttribution()Lcom/google/geo/sidekick/Sidekick$Attribution;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Attribution;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setDomain(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasInfoUrl()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getInfoUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setSourceUri(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;->hasLocation()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mSourceLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mSourceLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/sidekick/MapsLauncher;->buildWalkingDirectionsUri(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/gallery/VelvetImage;->setNavigationUri(Ljava/lang/String;)V

    :cond_2
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_3
    return-object v1
.end method

.method public displayThumbnails(Ljava/util/List;Lcom/google/geo/sidekick/Sidekick$Location;Landroid/view/LayoutInflater;)V
    .locals 10
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p3    # Landroid/view/LayoutInflater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;",
            ">;",
            "Lcom/google/geo/sidekick/Sidekick$Location;",
            "Landroid/view/LayoutInflater;",
            ")V"
        }
    .end annotation

    const v9, 0x7f04008b

    const/4 v8, 0x0

    invoke-static {p1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mSourceLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mImageListeners:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mInvalidIndices:Ljava/util/List;

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->getPhotoCount()I

    move-result v6

    if-ge v1, v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;

    rem-int/lit8 v6, v1, 0x4

    if-nez v6, :cond_0

    new-instance v5, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v5}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->addView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p3, v9, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/CrossfadingWebImageView;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;->getThumbnail()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setImageUrl(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->isLoadedFromCache()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mInvalidIndices:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    move v0, v1

    new-instance v6, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;

    invoke-direct {v6, p0, v0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;-><init>(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;I)V

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance v3, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;

    const/4 v6, 0x0

    invoke-direct {v3, p0, v1, v6}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$ImageLoadingListener;-><init>(Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;ILcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid$1;)V

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/ui/CrossfadingWebImageView;->setOnDownloadListener(Lcom/google/android/velvet/ui/WebImageView$Listener;)V

    iget-object v6, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mImageListeners:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    :goto_2
    rem-int/lit8 v6, v1, 0x4

    if-eqz v6, :cond_4

    invoke-virtual {p3, v9, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/CrossfadingWebImageView;

    invoke-virtual {v5, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->maybeClearInvalidImages()V

    return-void
.end method

.method getPhotoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GeoLocatedPhoto;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    return-object v0
.end method

.method maybeClearInvalidImages()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mImageListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mInvalidIndices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_1
    if-ltz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mInvalidIndices:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->removeAllViews()V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mPhotoList:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mSourceLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->displayThumbnails(Ljava/util/List;Lcom/google/geo/sidekick/Sidekick$Location;Landroid/view/LayoutInflater;)V

    goto :goto_0
.end method

.method setImageListeners(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ui/WebImageView$Listener;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mImageListeners:Ljava/util/List;

    return-void
.end method

.method setInvalidIndices(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PhotoSpotThumbnailGrid;->mInvalidIndices:Ljava/util/List;

    return-void
.end method
