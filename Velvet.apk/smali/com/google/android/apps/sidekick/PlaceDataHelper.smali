.class public Lcom/google/android/apps/sidekick/PlaceDataHelper;
.super Ljava/lang/Object;
.source "PlaceDataHelper.java"


# static fields
.field private static final MAX_STAR_RATING:Ljava/lang/Double;

.field private static final MIN_STAR_RATING:Ljava/lang/Double;

.field private static final ONE_DP_FORMATTER:Ljava/text/DecimalFormat;

.field private static final STAR_RATINGS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.0"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/sidekick/PlaceDataHelper;->ONE_DP_FORMATTER:Ljava/text/DecimalFormat;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const v2, 0x7f020162

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f020163

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f020164

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f020165

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f020166

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const v2, 0x7f020167

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f020168

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f020169

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x7f02016a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/PlaceDataHelper;->STAR_RATINGS:Ljava/util/List;

    const-wide/high16 v0, 0x3ff0000000000000L

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/PlaceDataHelper;->MIN_STAR_RATING:Ljava/lang/Double;

    const-wide/high16 v0, 0x4014000000000000L

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/PlaceDataHelper;->MAX_STAR_RATING:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getClippedStarRating(I)Ljava/lang/Double;
    .locals 8
    .param p0    # I

    sget-object v0, Lcom/google/android/apps/sidekick/PlaceDataHelper;->MIN_STAR_RATING:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    sget-object v2, Lcom/google/android/apps/sidekick/PlaceDataHelper;->MAX_STAR_RATING:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    int-to-double v4, p0

    const-wide v6, 0x408f400000000000L

    div-double/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method static getStarImageResource(Ljava/lang/Double;)I
    .locals 8
    .param p0    # Ljava/lang/Double;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v1, v4

    add-int/lit8 v0, v1, -0x2

    if-ltz v0, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljunit/framework/Assert;->assertTrue(Z)V

    sget-object v1, Lcom/google/android/apps/sidekick/PlaceDataHelper;->STAR_RATINGS:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    :goto_1
    invoke-static {v2}, Ljunit/framework/Assert;->assertTrue(Z)V

    sget-object v1, Lcom/google/android/apps/sidekick/PlaceDataHelper;->STAR_RATINGS:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public static populateBusinessData(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$BusinessData;)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$BusinessData;

    const/4 v10, 0x0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHoursToday()Z

    move-result v8

    if-eqz v8, :cond_0

    const v8, 0x7f100032

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenHoursToday()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getKnownForTermCount()I

    move-result v8

    if-lez v8, :cond_1

    const-string v8, "\u22c5"

    invoke-static {v8}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v8

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getKnownForTermList()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    const v8, 0x7f100033

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCoverPhoto()Z

    move-result v8

    if-eqz v8, :cond_3

    const v8, 0x7f100034

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v5, v10}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrlType()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_2

    invoke-static {p0}, Lcom/google/android/velvet/util/LayoutUtils;->getCardWidth(Landroid/content/Context;)I

    move-result v8

    div-int/lit8 v7, v8, 0x2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0025

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v1, v8

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v1, v8}, Lcom/google/android/apps/sidekick/FifeImageUrlUtil;->setImageUrlSmartCrop(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    :cond_2
    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    :cond_3
    return-void
.end method

.method public static populatePlaceReview(Landroid/content/Context;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$BusinessData;)V
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$BusinessData;

    const v12, 0x7f0d0237

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumRatingStarsE3()Z

    move-result v7

    if-eqz v7, :cond_2

    const v7, 0x7f1001bd

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getNumRatingStarsE3()I

    move-result v7

    invoke-static {v7}, Lcom/google/android/apps/sidekick/PlaceDataHelper;->getClippedStarRating(I)Ljava/lang/Double;

    move-result-object v4

    sget-object v7, Lcom/google/android/apps/sidekick/PlaceDataHelper;->ONE_DP_FORMATTER:Ljava/text/DecimalFormat;

    invoke-virtual {v7, v4}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setVisibility(I)V

    const v7, 0x7f1001be

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {v4}, Lcom/google/android/apps/sidekick/PlaceDataHelper;->getStarImageResource(Ljava/lang/Double;)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    const v7, 0x7f1001bf

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumReviews()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPriceLevel()Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "\u22c5"

    invoke-static {v7}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getNumReviews()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {p0, v12, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getPriceLevel()Ljava/lang/String;

    move-result-object v9

    new-array v10, v11, [Ljava/lang/Object;

    invoke-virtual {v7, v8, v9, v10}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasReviewScore()Z

    move-result v7

    if-eqz v7, :cond_3

    const v7, 0x7f1001bb

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getReviewScore()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c009b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int v1, v7, v8

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v7

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v9

    invoke-virtual {p1, v7, v1, v8, v9}, Landroid/view/View;->setPadding(IIII)V

    :cond_3
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getZagatRated()Z

    move-result v7

    if-eqz v7, :cond_0

    const v7, 0x7f1001bc

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasNumReviews()Z

    move-result v7

    if-eqz v7, :cond_5

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getNumReviews()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-virtual {p0, v12, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasPriceLevel()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getPriceLevel()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method
