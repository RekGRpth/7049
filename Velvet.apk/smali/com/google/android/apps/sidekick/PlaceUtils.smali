.class public Lcom/google/android/apps/sidekick/PlaceUtils;
.super Ljava/lang/Object;
.source "PlaceUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPlaceDataFromEntry(Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v1

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    const/4 v5, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v3

    :goto_2
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasDisplayName()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_4

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    :goto_3
    if-eqz v4, :cond_5

    move-object v0, v4

    :cond_0
    :goto_4
    if-eqz v0, :cond_7

    :goto_5
    return-object v0

    :cond_1
    move-object v1, v5

    goto :goto_0

    :cond_2
    move-object v2, v5

    goto :goto_1

    :cond_3
    move-object v3, v5

    goto :goto_2

    :cond_4
    move-object v4, v5

    goto :goto_3

    :cond_5
    if-eqz v2, :cond_6

    move-object v0, v2

    goto :goto_4

    :cond_6
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasLat()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasLng()Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x7f0d00f9

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_7
    const v5, 0x7f0d00e9

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method
