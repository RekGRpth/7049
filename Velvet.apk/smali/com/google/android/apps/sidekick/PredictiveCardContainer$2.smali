.class Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;
.super Ljava/lang/Object;
.source "PredictiveCardContainer.java"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/PredictiveCardContainer;->hideSettingsView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

.field final synthetic val$parentTransition:Landroid/animation/LayoutTransition;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/PredictiveCardContainer;Landroid/animation/LayoutTransition;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->val$parentTransition:Landroid/animation/LayoutTransition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 4
    .param p1    # Landroid/animation/LayoutTransition;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/View;
    .param p4    # I

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    # getter for: Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->access$000(Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->removeView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    # getter for: Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->access$100(Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->removeView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    # setter for: Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;
    invoke-static {v1, v3}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->access$002(Lcom/google/android/apps/sidekick/PredictiveCardContainer;Landroid/view/View;)Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    # setter for: Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;
    invoke-static {v1, v3}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->access$102(Lcom/google/android/apps/sidekick/PredictiveCardContainer;Landroid/view/View;)Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->this$0:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    # getter for: Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->access$200(Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f10005c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;->val$parentTransition:Landroid/animation/LayoutTransition;

    invoke-virtual {v1, p0}, Landroid/animation/LayoutTransition;->removeTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    return-void
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0
    .param p1    # Landroid/animation/LayoutTransition;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/View;
    .param p4    # I

    return-void
.end method
