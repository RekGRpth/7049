.class public Lcom/google/android/apps/sidekick/PredictiveCardContainer;
.super Landroid/view/ViewGroup;
.source "PredictiveCardContainer.java"

# interfaces
.implements Lcom/google/android/velvet/tg/SuggestionGridLayout$CardWrapper;
.implements Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;


# instance fields
.field private mCardUnderlapPx:I

.field private mCardView:Landroid/view/View;

.field private mExpanded:Z

.field private mForcedCardHeight:I

.field private mMenuButton:Landroid/view/View;

.field private mMenuButtonPosition:Landroid/graphics/Rect;

.field private mPendingSettingsViewState:Landroid/os/Bundle;

.field private mSettingsView:Landroid/view/View;

.field private mTmpRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mTmpRect:Landroid/graphics/Rect;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mTmpRect:Landroid/graphics/Rect;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mTmpRect:Landroid/graphics/Rect;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/sidekick/PredictiveCardContainer;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/PredictiveCardContainer;
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/sidekick/PredictiveCardContainer;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/PredictiveCardContainer;
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    return-object v0
.end method

.method private createMenuButtonOverlay(Lcom/google/android/velvet/presenter/TgPresenter;)Landroid/view/View;
    .locals 7
    .param p1    # Lcom/google/android/velvet/presenter/TgPresenter;

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    const v4, 0x7f10005c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v3, 0x7f040019

    invoke-virtual {v0, v3, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    new-instance v3, Lcom/google/android/apps/sidekick/PredictiveCardContainer$1;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer$1;-><init>(Lcom/google/android/apps/sidekick/PredictiveCardContainer;Lcom/google/android/velvet/presenter/TgPresenter;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getCardViewBackgroundBottomPadding()I
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mTmpRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    return v1
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const v0, 0x7f100017

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->setId(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardUnderlapPx:I

    return-void
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/view/View;
    .param p3    # J

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    if-ne p2, v3, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/4 v1, 0x1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getSettingsViewTop()I

    move-result v3

    iput v3, v0, Landroid/graphics/Rect;->top:I

    const/4 v3, 0x0

    iput v3, v0, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iput v3, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v2

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    return v2
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    const v1, 0x7f100012

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    goto :goto_0
.end method

.method public getCardView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    return-object v0
.end method

.method public getSettingsViewHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getSettingsViewTop()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getCardViewBackgroundBottomPadding()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardUnderlapPx:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public hideSettingsView()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mExpanded:Z

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer$2;-><init>(Lcom/google/android/apps/sidekick/PredictiveCardContainer;Landroid/animation/LayoutTransition;)V

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->removeView(Landroid/view/View;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    goto :goto_0
.end method

.method public isDismissableViewAtPosition(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    check-cast v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$DismissableChildContainer;->isDismissableViewAtPosition(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mExpanded:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/view/View;->layout(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getSettingsViewTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v5, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    const v3, 0x7f10005c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    iput v5, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    iput v5, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1    # I
    .param p2    # I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    if-nez v13, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mExpanded:Z

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    if-nez v13, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v13, v0, v1}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v14

    const/high16 v15, 0x40000000

    invoke-static {v14, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButtonPosition:Landroid/graphics/Rect;

    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v15

    const/high16 v16, 0x40000000

    invoke-static/range {v15 .. v16}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Landroid/view/View;->measure(II)V

    :cond_3
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v11

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mForcedCardHeight:I

    if-lez v13, :cond_5

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mForcedCardHeight:I

    const/high16 v14, 0x40000000

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    move/from16 v0, p1

    invoke-virtual {v13, v0, v3}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    const/4 v10, 0x0

    sparse-switch v11, :sswitch_data_0

    :goto_2
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    const/high16 v14, 0x40000000

    invoke-static {v4, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    :cond_4
    const/4 v5, 0x0

    sparse-switch v6, :sswitch_data_1

    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->setMeasuredDimension(II)V

    goto/16 :goto_0

    :cond_5
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_1

    :sswitch_0
    move v10, v12

    goto :goto_2

    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getPaddingLeft()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getPaddingRight()I

    move-result v14

    add-int v8, v13, v14

    add-int v10, v4, v8

    goto :goto_2

    :sswitch_2
    move v5, v7

    goto :goto_3

    :sswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getPaddingTop()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getPaddingBottom()I

    move-result v14

    add-int v8, v13, v14

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getCardViewBackgroundBottomPadding()I

    move-result v13

    sub-int v13, v2, v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardUnderlapPx:I

    sub-int/2addr v13, v14

    add-int/2addr v13, v9

    add-int v5, v13, v8

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x0 -> :sswitch_3
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4
    .param p1    # Landroid/os/Parcelable;

    move-object v2, p1

    check-cast v2, Landroid/os/Bundle;

    const-string v3, "parent_state"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-super {p0, v3}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v3, "card_state"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    const-string v3, "settings_state"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;->restoreViewState(Landroid/os/Bundle;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mPendingSettingsViewState:Landroid/os/Bundle;

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "parent_state"

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    if-eqz v4, :cond_0

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    const-string v4, "card_state"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;->saveViewState()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v4, "settings_state"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    return-object v3
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getSettingsViewTop()I

    move-result v3

    sub-int v4, p2, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int v0, v2, v1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setTop(I)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    add-int v4, v2, v1

    invoke-virtual {v3, v4}, Landroid/view/View;->setBottom(I)V

    :cond_0
    return-void
.end method

.method public setCardView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mCardView:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setForcedCardHeight(I)V
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mExpanded:Z

    if-nez v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mForcedCardHeight:I

    :cond_0
    return-void
.end method

.method public showSettingsView(Lcom/google/android/velvet/presenter/TgPresenter;Landroid/view/View;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p2    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mSettingsView:Landroid/view/View;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mExpanded:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->createMenuButtonOverlay(Lcom/google/android/velvet/presenter/TgPresenter;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-object v0, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mMenuButton:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->addView(Landroid/view/View;)V

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, p2, v1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->addView(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mPendingSettingsViewState:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mPendingSettingsViewState:Landroid/os/Bundle;

    invoke-interface {v1, v2}, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;->restoreViewState(Landroid/os/Bundle;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->mPendingSettingsViewState:Landroid/os/Bundle;

    goto :goto_0
.end method
