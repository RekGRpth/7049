.class public Lcom/google/android/apps/sidekick/ProtoKey;
.super Ljava/lang/Object;
.source "ProtoKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Lcom/google/protobuf/micro/MessageMicro;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final EMPTY_BYTE_ARRAY:[B


# instance fields
.field private volatile mBytes:[B

.field private volatile mHashCode:Ljava/lang/Integer;

.field private final mProto:Lcom/google/protobuf/micro/MessageMicro;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/sidekick/ProtoKey;->EMPTY_BYTE_ARRAY:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mProto:Lcom/google/protobuf/micro/MessageMicro;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/apps/sidekick/ProtoKey;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/sidekick/ProtoKey;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mProto:Lcom/google/protobuf/micro/MessageMicro;

    iget-object v4, v0, Lcom/google/android/apps/sidekick/ProtoKey;->mProto:Lcom/google/protobuf/micro/MessageMicro;

    if-eq v3, v4, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mProto:Lcom/google/protobuf/micro/MessageMicro;

    if-nez v1, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mProto:Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/apps/sidekick/ProtoKey;->mProto:Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v1, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ProtoKey;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/ProtoKey;->getBytes()[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    goto :goto_0
.end method

.method public getBytes()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mBytes:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mProto:Lcom/google/protobuf/micro/MessageMicro;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/sidekick/ProtoKey;->EMPTY_BYTE_ARRAY:[B

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mBytes:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mBytes:[B

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mProto:Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v0}, Lcom/google/protobuf/micro/MessageMicro;->toByteArray()[B

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mHashCode:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ProtoKey;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mHashCode:Ljava/lang/Integer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/ProtoKey;->mHashCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
