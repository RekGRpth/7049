.class Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;
.super Ljava/lang/Object;
.source "RealEstateEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/RealEstateEntryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TablePopulator"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDataRow:Landroid/widget/TableRow;

.field private mHeaderRow:Landroid/widget/TableRow;

.field private mIndex:I

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mTableLayout:Landroid/widget/TableLayout;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/widget/TableLayout;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/widget/TableLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mIndex:I

    iput-object p1, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mLayoutInflater:Landroid/view/LayoutInflater;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mTableLayout:Landroid/widget/TableLayout;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/widget/TableLayout;Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$1;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/widget/TableLayout;
    .param p4    # Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/widget/TableLayout;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->addEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private addEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 7
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mHeaderRow:Landroid/widget/TableRow;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mDataRow:Landroid/widget/TableRow;

    if-nez v2, :cond_1

    :cond_0
    new-instance v2, Landroid/widget/TableRow;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mHeaderRow:Landroid/widget/TableRow;

    new-instance v2, Landroid/widget/TableRow;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mDataRow:Landroid/widget/TableRow;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mTableLayout:Landroid/widget/TableLayout;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mHeaderRow:Landroid/widget/TableRow;

    invoke-virtual {v2, v3}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mTableLayout:Landroid/widget/TableLayout;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mDataRow:Landroid/widget/TableRow;

    invoke-virtual {v2, v3}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f0400c8

    iget-object v4, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mHeaderRow:Landroid/widget/TableRow;

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mHeaderRow:Landroid/widget/TableRow;

    invoke-virtual {v2, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f0400c7

    iget-object v4, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mDataRow:Landroid/widget/TableRow;

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mDataRow:Landroid/widget/TableRow;

    invoke-virtual {v2, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    iget v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mIndex:I

    const/4 v3, 0x2

    if-lt v2, v3, :cond_2

    iput v5, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mIndex:I

    iput-object v6, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mHeaderRow:Landroid/widget/TableRow;

    iput-object v6, p0, Lcom/google/android/apps/sidekick/RealEstateEntryAdapter$TablePopulator;->mDataRow:Landroid/widget/TableRow;

    :cond_2
    return-void
.end method
