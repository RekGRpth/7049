.class public Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;
.super Landroid/app/DialogFragment;
.source "RemindersListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/RemindersListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MarkReminderCompleteDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static newInstance(Landroid/app/Fragment;I)Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;
    .locals 3
    .param p0    # Landroid/app/Fragment;
    .param p1    # I

    new-instance v1, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "position_key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;->setArguments(Landroid/os/Bundle;)V

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;->setTargetFragment(Landroid/app/Fragment;I)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "position_key"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0d0301

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x1040013

    new-instance v3, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog$1;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog$1;-><init>(Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;I)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x1040009

    new-instance v3, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog$2;-><init>(Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method
