.class public Lcom/google/android/apps/sidekick/RemindersListFragment;
.super Landroid/app/ListFragment;
.source "RemindersListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/RemindersListFragment$1;,
        Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;,
        Lcom/google/android/apps/sidekick/RemindersListFragment$ReminderListAdapter;,
        Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;
    }
.end annotation


# instance fields
.field private mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mRefreshManager:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private mWorkFragment:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/RemindersListFragment;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/RemindersListFragment;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/RemindersListFragment;->setReminders(Ljava/util/List;)V

    return-void
.end method

.method private setReminders(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/sidekick/RemindersListFragment$ReminderListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/apps/sidekick/RemindersListFragment$ReminderListAdapter;-><init>(Landroid/view/LayoutInflater;Ljava/util/List;Lcom/google/android/searchcommon/util/Clock;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private showDetails(I)V
    .locals 3
    .param p1    # I

    invoke-static {p0, p1}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;->newInstance(Landroid/app/Fragment;I)Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "mark_reminder_done_dialog_tag"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/RemindersListFragment$MarkReminderCompleteDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "fetch_reminders_retained"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    iput-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mWorkFragment:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mWorkFragment:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mWorkFragment:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mWorkFragment:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mWorkFragment:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    const-string v3, "fetch_reminders_retained"

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0d02ff

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/RemindersListFragment;->setEmptyText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mWorkFragment:Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;

    # invokes: Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->updateTargetReminders()V
    invoke-static {v1}, Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;->access$000(Lcom/google/android/apps/sidekick/RemindersListFragment$RetainedFragment;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mRefreshManager:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/RemindersListFragment;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->addOrFixHelpMenuItem(Landroid/content/Context;Landroid/view/Menu;)V

    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    invoke-direct {p0, p3}, Lcom/google/android/apps/sidekick/RemindersListFragment;->showDetails(I)V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidateWithDelayedRefresh()V

    return-void
.end method

.method protected removeReminder(I)V
    .locals 9
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    invoke-interface {v6, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/RemindersListFragment$ReminderListAdapter;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mRefreshManager:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-static {v6, v7}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->newBuilder(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/sidekick/RemindersListFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v7}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->setTimestamp(J)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->build()Lcom/google/android/apps/sidekick/actions/DismissEntryTask;

    move-result-object v4

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Void;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/RemindersListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v2}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getDeleteNotificationIntent(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/RemindersListFragment$ReminderListAdapter;->removeItem(I)V

    return-void
.end method
