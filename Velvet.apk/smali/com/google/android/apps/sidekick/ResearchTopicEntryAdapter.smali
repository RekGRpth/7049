.class public Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "ResearchTopicEntryAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final URL_PREFIXES_STAY_IN_WEBVIEW:[Ljava/lang/String;


# instance fields
.field private final mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->TAG:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "www.google.com/now/topics"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "www.google.com/plus/history/tasks"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->URL_PREFIXES_STAY_IN_WEBVIEW:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .param p2    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p3    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p4    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->expandCard(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method private expandCard(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 30
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchTopicEntry()Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    move-result-object v28

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->initImageStripAndAddMembers(Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;Landroid/content/Context;Landroid/view/View;)V

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual/range {v20 .. v20}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchPageEntry()Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    move-result-object v25

    const v4, 0x7f0400a9

    const/4 v5, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v26

    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;->hasTitle()Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f100203

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;->getTitle()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;->hasDescription()Z

    move-result v4

    if-eqz v4, :cond_1

    const v4, 0x7f100204

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;->getDescription()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->getLinkUri(Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;)Landroid/net/Uri;

    move-result-object v23

    if-eqz v23, :cond_2

    const v4, 0x7f100205

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;->hasLandingPageDomain()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;->getLandingPageDomain()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    new-instance v4, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    move-object/from16 v3, v23

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    invoke-virtual/range {v28 .. v28}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->hasExploreMoreUrl()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual/range {v28 .. v28}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->hasExploreMoreAuthForService()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual/range {v28 .. v28}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getExploreMoreAuthForService()Ljava/lang/String;

    move-result-object v14

    :goto_2
    const v4, 0x7f100209

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/Button;

    const/16 v4, 0x14

    invoke-static {v6, v4}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v18

    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual/range {v28 .. v28}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->hasExploreMoreTitle()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual/range {v28 .. v28}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getExploreMoreTitle()Ljava/lang/String;

    move-result-object v10

    :goto_3
    new-instance v29, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;

    invoke-virtual/range {v28 .. v28}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getExploreMoreUrl()Ljava/lang/String;

    move-result-object v12

    const/4 v11, 0x1

    const-string v13, "RESEARCH_TOPIC_EXPLORE_MORE"

    sget-object v15, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->URL_PREFIXES_STAY_IN_WEBVIEW:[Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-static/range {v18 .. v18}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static/range {p1 .. p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v9

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v17

    move-object/from16 v7, v29

    move-object/from16 v8, p1

    move-object v9, v12

    move-object/from16 v12, p0

    move-object/from16 v16, v4

    invoke-direct/range {v7 .. v17}, Lcom/google/android/apps/sidekick/GoogleServiceWebviewClickListener;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->mEntryTreeNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v4}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->addListCardTags(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/List;)V

    return-void

    :cond_6
    const/4 v14, 0x0

    goto :goto_2

    :cond_7
    invoke-virtual/range {v28 .. v28}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getTopic()Ljava/lang/String;

    move-result-object v10

    goto :goto_3
.end method

.method private static getLinkUri(Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$ResearchPageEntry;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->TAG:Ljava/lang/String;

    const-string v2, "URL failed to parse"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initImageStripAndAddMembers(Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;Landroid/content/Context;Landroid/view/View;)V
    .locals 11
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/view/View;

    const v10, 0x7f100207

    const/4 v9, 0x0

    invoke-virtual {p3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getImageList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/geo/sidekick/Sidekick$Photo;

    const/4 v7, 0x4

    if-lt v3, v7, :cond_3

    :cond_1
    const/4 v7, 0x1

    if-le v3, v7, :cond_2

    invoke-virtual {p3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrlType()I

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasUrlType()Z

    move-result v7

    if-nez v7, :cond_0

    :cond_4
    new-instance v6, Lcom/google/android/velvet/ui/WebImageView;

    invoke-direct {v6, p2}, Lcom/google/android/velvet/ui/WebImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    sget-object v7, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v6, v7}, Lcom/google/android/velvet/ui/WebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/high16 v8, 0x3f800000

    invoke-direct {v4, v9, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v6, v4}, Lcom/google/android/velvet/ui/WebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasPhotoAttribution()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Photo;->getPhotoAttribution()Lcom/google/geo/sidekick/Sidekick$Attribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Attribution;->hasUrl()Z

    move-result v7

    if-eqz v7, :cond_5

    new-instance v7, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$4;

    invoke-direct {v7, p0, p2, v0}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Attribution;)V

    invoke-virtual {v6, v7}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchTopicEntry()Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getTopic()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const v3, 0x7f0d02f0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;I[Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v2
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0248

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0246

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0247

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02ce

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchTopicEntry()Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getTopic()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Entry;->getResearchTopicEntry()Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;

    move-result-object v3

    const v5, 0x7f0400aa

    const/4 v6, 0x0

    invoke-virtual {p2, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v5, 0x7f100031

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getTopic()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f100206

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getActionHeader()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f100208

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/CardTableLayout;

    new-instance v5, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$1;

    invoke-direct {v5, p0, p1, v2}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v1, v5}, Lcom/google/android/apps/sidekick/CardTableLayout;->setAdapter(Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;)V

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$ResearchTopicEntry;->getCollapsed()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-direct {p0, p1, v2, v1, p2}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;->expandCard(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    :goto_0
    return-object v2

    :cond_0
    new-instance v5, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$2;

    invoke-direct {v5, p0, v1}, Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/ResearchTopicEntryAdapter;Lcom/google/android/apps/sidekick/CardTableLayout;)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
