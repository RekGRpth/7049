.class public Lcom/google/android/apps/sidekick/SessionManagerImpl;
.super Ljava/lang/Object;
.source "SessionManagerImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/SessionManager;


# static fields
.field private static final MAX_EXPIRATION_JITTER_SECONDS:I

.field private static final SESSION_DURATION_SECONDS:J

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

.field private final mCipher:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mGetAndSetLock:Ljava/lang/Object;

.field private final mJitterGenerator:Ljava/security/SecureRandom;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/apps/sidekick/SessionManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->TAG:Ljava/lang/String;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x7

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->SESSION_DURATION_SECONDS:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->MAX_EXPIRATION_JITTER_SECONDS:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mGetAndSetLock:Ljava/lang/Object;

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mJitterGenerator:Ljava/security/SecureRandom;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCipher:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    return-void
.end method

.method private decryptSessionKey([B)Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;
    .locals 14
    .param p1    # [B

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCipher:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    invoke-interface {v0, p1}, Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;->decryptBytes([B)[B

    move-result-object v13

    if-nez v13, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v13}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v7, Ljava/io/DataInputStream;

    invoke-direct {v7, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v11

    invoke-virtual {v7}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v9

    invoke-virtual {v7}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    invoke-virtual {v7}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    new-instance v0, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    new-instance v1, Ljava/util/UUID;

    invoke-direct {v1, v11, v12, v9, v10}, Ljava/util/UUID;-><init>(JJ)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;-><init>(Ljava/util/UUID;JJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-exception v8

    const/4 v0, 0x0

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v0
.end method

.method private encryptSessionKey(Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;)[B
    .locals 5
    .param p1    # Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    iget-object v3, p1, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->key:Ljava/util/UUID;

    invoke-virtual {v3}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-object v3, p1, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->key:Ljava/util/UUID;

    invoke-virtual {v3}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-wide v3, p1, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->expirationSeconds:J

    invoke-virtual {v1, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-wide v3, p1, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->jitteredExpirationSeconds:J

    invoke-virtual {v1, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCipher:Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;->encryptBytes([B)[B

    move-result-object v3

    :goto_0
    return-object v3

    :catch_0
    move-exception v2

    const/4 v3, 0x0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v3
.end method

.method private readSessionkeyFromPreferences(Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;)Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    const/4 v1, 0x0

    const-string v2, "session_key"

    const/4 v3, 0x0

    invoke-interface {p1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBytes(Ljava/lang/String;[B)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/SessionManagerImpl;->decryptSessionKey([B)Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v2, Lcom/google/android/apps/sidekick/SessionManagerImpl;->TAG:Ljava/lang/String;

    const-string v3, "Clearing bad session_key from prefs"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v2

    const-string v3, "session_key"

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    :cond_0
    return-object v1
.end method

.method private writeSessionKeyToPreferences(Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;
    .param p2    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/SessionManagerImpl;->encryptSessionKey(Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;)[B

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/sidekick/SessionManagerImpl;->TAG:Ljava/lang/String;

    const-string v2, "error writing session key (crypto fail)"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-interface {p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    const-string v2, "session_key"

    invoke-interface {v1, v2, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    goto :goto_0
.end method


# virtual methods
.method public getSessionKey()Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;
    .locals 11

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v8, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    iget-object v8, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mGetAndSetLock:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/SessionManagerImpl;->readSessionkeyFromPreferences(Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;)Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    iget-wide v9, v0, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;->jitteredExpirationSeconds:J

    cmp-long v0, v9, v6

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    monitor-exit v8

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    sget-wide v9, Lcom/google/android/apps/sidekick/SessionManagerImpl;->SESSION_DURATION_SECONDS:J

    add-long v2, v6, v9

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mJitterGenerator:Ljava/security/SecureRandom;

    sget v9, Lcom/google/android/apps/sidekick/SessionManagerImpl;->MAX_EXPIRATION_JITTER_SECONDS:I

    invoke-virtual {v0, v9}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    int-to-long v9, v0

    add-long v4, v2, v9

    new-instance v0, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;-><init>(Ljava/util/UUID;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    iget-object v9, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v9}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v9

    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/sidekick/SessionManagerImpl;->writeSessionKeyToPreferences(Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SessionManagerImpl;->mCachedSessionKey:Lcom/google/android/apps/sidekick/inject/SessionManager$SessionKey;

    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
