.class public Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;
.super Ljava/lang/Object;
.source "SignedCipherHelperImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/SignedCipherHelper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mIsInitialized:Z

.field private mKeyPair:Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

.field private final mLock:Ljava/lang/Object;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private mSecureRandom:Ljava/security/SecureRandom;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/GsaPreferenceController;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    return-void
.end method

.method private static generateHmacBytes([BLjavax/crypto/spec/SecretKeySpec;)[B
    .locals 3
    .param p0    # [B
    .param p1    # Ljavax/crypto/spec/SecretKeySpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    const-string v2, "HmacSHA1"

    invoke-static {v2}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    const/4 v2, 0x3

    invoke-static {p0, v2}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljavax/crypto/Mac;->update([B)V

    invoke-virtual {v1}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v2

    return-object v2
.end method

.method private generateKeyPairLocked(Landroid/content/SharedPreferences;)Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;
    .locals 10
    .param p1    # Landroid/content/SharedPreferences;

    const/4 v8, 0x3

    :try_start_0
    const-string v7, "AES"

    invoke-static {v7}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    const/16 v7, 0x100

    invoke-virtual {v3, v7}, Ljavax/crypto/KeyGenerator;->init(I)V

    invoke-virtual {v3}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v5

    invoke-virtual {v3}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v1

    invoke-interface {v5}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v7

    invoke-static {v7, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v7

    invoke-static {v7, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v7, "winston"

    invoke-interface {v4, v7, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v7, "wolf"

    invoke-interface {v4, v7, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    new-instance v7, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    invoke-interface {v5}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v8

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;-><init>([B[B)V

    :goto_0
    return-object v7

    :catch_0
    move-exception v0

    sget-object v7, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v8, "Cannot create KeyGenerator for AES"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    goto :goto_0
.end method

.method private maybeInitKeysLocked()V
    .locals 6

    iget-boolean v4, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mIsInitialized:Z

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->readKeyPairFromPrefsLocked(Landroid/content/SharedPreferences;)Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-direct {p0, v2}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->generateKeyPairLocked(Landroid/content/SharedPreferences;)Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    move-result-object v1

    :cond_1
    :try_start_0
    const-string v4, "SHA1PRNG"

    invoke-static {v4}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mKeyPair:Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    iput-object v3, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mSecureRandom:Ljava/security/SecureRandom;

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mIsInitialized:Z

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v4, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v5, "Cannot create SecureRandom for SHA1PRNG"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    const/4 v3, 0x0

    goto :goto_1
.end method

.method private readKeyPairFromPrefsLocked(Landroid/content/SharedPreferences;)Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;
    .locals 8
    .param p1    # Landroid/content/SharedPreferences;

    const/4 v6, 0x0

    const-string v5, "winston"

    invoke-interface {p1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "wolf"

    invoke-interface {p1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v3, :cond_0

    if-nez v1, :cond_1

    :cond_0
    move-object v5, v6

    :goto_0
    return-object v5

    :cond_1
    const/4 v5, 0x3

    :try_start_0
    invoke-static {v3, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    const/4 v5, 0x3

    invoke-static {v1, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    invoke-direct {v5, v4, v2}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;-><init>([B[B)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v7, "winston"

    invoke-interface {v5, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v7, "wolf"

    invoke-interface {v5, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    sget-object v5, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v7, "Failed to read keys successfully; clearing old ones"

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    goto :goto_0
.end method


# virtual methods
.method public decryptBytes([B)[B
    .locals 14
    .param p1    # [B

    const/4 v10, 0x0

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v11, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mLock:Ljava/lang/Object;

    monitor-enter v11

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->maybeInitKeysLocked()V

    iget-object v12, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mKeyPair:Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    if-nez v12, :cond_0

    sget-object v12, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v13, "No key pair"

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v11

    :goto_0
    return-object v10

    :cond_0
    iget-object v9, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mKeyPair:Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    array-length v11, p1

    add-int/lit8 v11, v11, -0x10

    add-int/lit8 v3, v11, -0x14

    if-gez v3, :cond_1

    sget-object v11, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v12, "Failed to decrypt: bad data"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v6

    sget-object v11, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v12, "Failed to decrypt"

    invoke-static {v11, v12, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v10

    :try_start_2
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    :cond_1
    :try_start_3
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const/16 v11, 0x10

    new-array v8, v11, [B

    const/16 v11, 0x14

    new-array v5, v11, [B

    new-array v2, v3, [B

    invoke-virtual {v0, v8}, Ljava/io/ByteArrayInputStream;->read([B)I

    move-result v11

    array-length v12, v8

    if-ne v11, v12, :cond_2

    invoke-virtual {v0, v5}, Ljava/io/ByteArrayInputStream;->read([B)I

    move-result v11

    array-length v12, v5

    if-ne v11, v12, :cond_2

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayInputStream;->read([B)I

    move-result v11

    array-length v12, v2

    if-eq v11, v12, :cond_3

    :cond_2
    sget-object v11, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v12, "Could not parse encrypted data"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v6

    sget-object v11, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v12, "Failed to decrypt"

    invoke-static {v11, v12, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    :try_start_4
    # getter for: Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->mHmacKey:Ljavax/crypto/spec/SecretKeySpec;
    invoke-static {v9}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->access$100(Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;)Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v11

    invoke-static {v2, v11}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->generateHmacBytes([BLjavax/crypto/spec/SecretKeySpec;)[B

    move-result-object v4

    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v11

    if-nez v11, :cond_4

    sget-object v11, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v12, "Signature mismatch"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    new-instance v7, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v7, v8}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    const-string v11, "AES/CBC/PKCS5Padding"

    invoke-static {v11}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    const/4 v11, 0x2

    # getter for: Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->mSecretKey:Ljavax/crypto/spec/SecretKeySpec;
    invoke-static {v9}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->access$000(Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;)Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v12

    invoke-virtual {v1, v11, v12, v7}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    invoke-virtual {v1, v2}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v10

    goto/16 :goto_0
.end method

.method public encryptBytes([B)[B
    .locals 13
    .param p1    # [B

    const/4 v9, 0x0

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mLock:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->maybeInitKeysLocked()V

    iget-object v11, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mKeyPair:Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    if-nez v11, :cond_0

    sget-object v11, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v12, "No key pair"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v10

    :goto_0
    return-object v9

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mKeyPair:Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;

    iget-object v8, p0, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->mSecureRandom:Ljava/security/SecureRandom;

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v10, 0x10

    :try_start_1
    new-array v6, v10, [B

    invoke-virtual {v8, v6}, Ljava/security/SecureRandom;->nextBytes([B)V

    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, v6}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    const-string v10, "AES/CBC/PKCS5Padding"

    invoke-static {v10}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    const/4 v10, 0x1

    # getter for: Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->mSecretKey:Ljavax/crypto/spec/SecretKeySpec;
    invoke-static {v7}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->access$000(Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;)Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v11

    invoke-virtual {v1, v10, v11, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    # getter for: Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->mHmacKey:Ljavax/crypto/spec/SecretKeySpec;
    invoke-static {v7}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;->access$100(Lcom/google/android/apps/sidekick/SignedCipherHelperImpl$KeyPair;)Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v10

    invoke-static {v2, v10}, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->generateHmacBytes([BLjavax/crypto/spec/SecretKeySpec;)[B

    move-result-object v3

    const/16 v10, 0x14

    array-length v11, v3

    if-eq v10, v11, :cond_1

    new-instance v10, Ljava/lang/IllegalStateException;

    const-string v11, "hmac size unexpected"

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :catch_0
    move-exception v4

    sget-object v10, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v11, "Failed to encrypt"

    invoke-static {v10, v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v9

    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v9

    :cond_1
    :try_start_3
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v0, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v9

    goto :goto_0

    :catch_1
    move-exception v4

    sget-object v10, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v11, "Failed to encrypt"

    invoke-static {v10, v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v4

    sget-object v10, Lcom/google/android/apps/sidekick/SignedCipherHelperImpl;->TAG:Ljava/lang/String;

    const-string v11, "Failed to encrypt"

    invoke-static {v10, v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
