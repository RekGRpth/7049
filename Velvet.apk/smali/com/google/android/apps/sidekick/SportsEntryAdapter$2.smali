.class Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;
.super Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;
.source "SportsEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

.field final synthetic val$team1IsInterest:Z

.field final synthetic val$team2IsInterest:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/SportsEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;ZZ)V
    .locals 0
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    iput-boolean p3, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;->val$team1IsInterest:Z

    iput-boolean p4, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;->val$team2IsInterest:Z

    invoke-direct {p0, p2}, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    return-void
.end method


# virtual methods
.method public getNoRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    new-instance v0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2$2;-><init>(Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;Landroid/content/Context;I)V

    return-object v0
.end method

.method public getQuestionCount(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;->val$team1IsInterest:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;->val$team2IsInterest:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;->val$team1IsInterest:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/SportsEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/SportsEntryAdapter;->mSportEntry:Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->access$200(Lcom/google/android/apps/sidekick/SportsEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const v0, 0x7f0d02f2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getYesRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    new-instance v0, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$2$1;-><init>(Lcom/google/android/apps/sidekick/SportsEntryAdapter$2;Landroid/content/Context;I)V

    return-object v0
.end method
