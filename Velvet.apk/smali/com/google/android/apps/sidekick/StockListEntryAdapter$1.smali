.class Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;
.super Ljava/lang/Object;
.source "StockListEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/StockListEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getDisclaimerUrl()Ljava/lang/String;

    move-result-object v3

    const-string v4, "STOCK_DISCLAIMER"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
