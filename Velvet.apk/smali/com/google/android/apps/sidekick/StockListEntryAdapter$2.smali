.class Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;
.super Ljava/lang/Object;
.source "StockListEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/StockListEntryAdapter;->multiStockView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

.field final synthetic val$card:Landroid/view/View;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$layoutInflater:Landroid/view/LayoutInflater;

.field final synthetic val$showMoreButton:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Landroid/widget/Button;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$showMoreButton:Landroid/widget/Button;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$layoutInflater:Landroid/view/LayoutInflater;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$card:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$showMoreButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$layoutInflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$card:Landroid/view/View;

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;
    invoke-static {v5}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/StockListEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntryCount()I

    move-result v5

    # invokes: Lcom/google/android/apps/sidekick/StockListEntryAdapter;->addStocks(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->access$100(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$2;->val$context:Landroid/content/Context;

    const-string v2, "SHOW_MORE"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->logDetailsInteractionWithLabel(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
