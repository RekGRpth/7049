.class Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;
.super Ljava/lang/Object;
.source "StockListEntryAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;->getYesRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$index:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;Landroid/content/Context;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;->this$1:Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;->val$context:Landroid/content/Context;

    iput p3, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;->val$index:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;->this$1:Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;

    iget-object v0, v0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;->this$1:Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;

    iget-object v2, v2, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5;->this$0:Lcom/google/android/apps/sidekick/StockListEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/StockListEntryAdapter;->mStocksListEntry:Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/StockListEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/sidekick/StockListEntryAdapter$5$1;->val$index:I

    invoke-virtual {v2, v3}, Lcom/google/geo/sidekick/Sidekick$StockQuoteListEntry;->getStockQuoteEntry(I)Lcom/google/geo/sidekick/Sidekick$StockQuote;

    move-result-object v2

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/sidekick/StockListEntryAdapter;->updateStockInSettings(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;Z)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/StockListEntryAdapter;->access$300(Lcom/google/android/apps/sidekick/StockListEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$StockQuote;Z)V

    return-void
.end method
