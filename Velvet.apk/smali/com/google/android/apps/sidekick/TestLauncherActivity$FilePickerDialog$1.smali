.class Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;
.super Ljava/lang/Object;
.source "TestLauncherActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;

.field final synthetic val$dataType:I

.field final synthetic val$files:[Ljava/io/File;

.field final synthetic val$items:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;[Ljava/lang/String;[Ljava/io/File;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->this$0:Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->val$items:[Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->val$files:[Ljava/io/File;

    iput p4, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->val$dataType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->this$0:Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->val$items:[Ljava/lang/String;

    aget-object v2, v2, p2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->this$0:Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/TestLauncherActivity;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->val$files:[Ljava/io/File;

    aget-object v1, v1, p2

    iget v2, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog$1;->val$dataType:I

    # invokes: Lcom/google/android/apps/sidekick/TestLauncherActivity;->processData(Ljava/io/File;I)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->access$000(Lcom/google/android/apps/sidekick/TestLauncherActivity;Ljava/io/File;I)V

    return-void
.end method
