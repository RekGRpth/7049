.class public Lcom/google/android/apps/sidekick/TestLauncherActivity;
.super Landroid/app/Activity;
.source "TestLauncherActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/TestLauncherActivity$IntentPickerDialog;,
        Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;
    }
.end annotation


# static fields
.field private static final REFRESH_NOTIFICATIONS_ACTION:Ljava/lang/Object;

.field private static final REFRESH_TRAFFIC_ACTION:Ljava/lang/Object;

.field private static final SCHEDULE_NOTIFICATIONS_ACTION:Ljava/lang/Object;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

.field private mStationarySecs:I

.field private mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/TestLauncherActivity;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->TAG:Ljava/lang/String;

    const-string v0, "com.google.android.apps.sidekick.REFRESH_TRAFFIC_ACTION"

    sput-object v0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->REFRESH_TRAFFIC_ACTION:Ljava/lang/Object;

    const-string v0, "com.google.android.apps.sidekick.REFRESH_NOTIFICATIONS_ACTION"

    sput-object v0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->REFRESH_NOTIFICATIONS_ACTION:Ljava/lang/Object;

    const-string v0, "com.google.android.apps.sidekick.SCHEDULE_NOTIFICATIONS_ACTION"

    sput-object v0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->SCHEDULE_NOTIFICATIONS_ACTION:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mStationarySecs:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/TestLauncherActivity;Ljava/io/File;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/TestLauncherActivity;
    .param p1    # Ljava/io/File;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->processData(Ljava/io/File;I)V

    return-void
.end method

.method private addLocationFromString(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->parseLocationLine(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private deleteApplicationFile(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    const-string v0, "File deleted"

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "File does not exist"

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private getEntriesFile()Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "entry_provider"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private getNotificationsFile()Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "notifications_store"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private loadEncodedData(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x3

    invoke-static {p1, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    :try_start_0
    new-instance v3, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    check-cast v2, Lcom/google/android/apps/sidekick/VelvetNetworkClient;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->setDebugResponse(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget-object v4, Lcom/google/android/apps/sidekick/TestLauncherActivity;->TAG:Ljava/lang/String;

    const-string v5, "IO Exception: "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v4, "Invalid protocol buffer.  Check the logs."

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private parseLocationLine(Ljava/lang/String;)Landroid/location/Location;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x3

    const/4 v4, 0x2

    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-ge v2, v4, :cond_1

    sget-object v2, Lcom/google/android/apps/sidekick/TestLauncherActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected parts (lat, long[, time]) in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/location/Location;

    const-string v2, "fake"

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    array-length v2, v1

    if-le v2, v4, :cond_2

    aget-object v2, v1, v4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    aget-object v2, v1, v4

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    :goto_1
    array-length v2, v1

    if-le v2, v5, :cond_0

    aget-object v2, v1, v5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    aget-object v2, v1, v5

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->setStationaryTime(I)V

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    goto :goto_1
.end method

.method private parseLocationsFromFile(Ljava/io/File;)Ljava/util/List;
    .locals 8
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->addLocationFromString(Ljava/lang/String;Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    throw v5
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v0

    sget-object v5, Lcom/google/android/apps/sidekick/TestLauncherActivity;->TAG:Ljava/lang/String;

    const-string v6, "File not found: "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v5, "File not found.  Check the logs."

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :goto_1
    return-object v2

    :cond_0
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v5, Lcom/google/android/apps/sidekick/TestLauncherActivity;->TAG:Ljava/lang/String;

    const-string v6, "IO Exception: "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v5, "IO Exception.  Check the logs."

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private precacheTest()V
    .locals 3

    const-string v1, "Precache test"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->newInstance(I)Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "precache_test"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private precacheTest(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->parseLocationsFromFile(Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/GmmPrecacher;

    invoke-direct {v1}, Lcom/google/android/searchcommon/GmmPrecacher;-><init>()V

    invoke-virtual {v1, p0, v0}, Lcom/google/android/searchcommon/GmmPrecacher;->precache(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method private processData(Ljava/io/File;I)V
    .locals 0
    .param p1    # Ljava/io/File;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->queueData(Ljava/io/File;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->queueLocations(Ljava/io/File;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->precacheTest(Ljava/io/File;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private pushTestLocations(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    check-cast v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->pushTestLocations(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private queueData(Ljava/io/File;)V
    .locals 7
    .param p1    # Ljava/io/File;

    new-instance v4, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v2}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    check-cast v3, Lcom/google/android/apps/sidekick/VelvetNetworkClient;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->setDebugResponse(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    move-object v1, v2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :goto_1
    :try_start_2
    sget-object v5, Lcom/google/android/apps/sidekick/TestLauncherActivity;->TAG:Ljava/lang/String;

    const-string v6, "File not found: "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v5, "File not found.  Check the logs."

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_2
    :try_start_3
    sget-object v5, Lcom/google/android/apps/sidekick/TestLauncherActivity;->TAG:Ljava/lang/String;

    const-string v6, "IO Exception: "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v5, "Invalid protocol buffer.  Check the logs."

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_2
    move-exception v0

    :goto_3
    :try_start_4
    sget-object v5, Lcom/google/android/apps/sidekick/TestLauncherActivity;->TAG:Ljava/lang/String;

    const-string v6, "IO Exception: "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v5, "IO Exception.  Check the logs."

    const/4 v6, 0x0

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v5

    :goto_4
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v5

    :catchall_1
    move-exception v5

    move-object v1, v2

    goto :goto_4

    :catch_3
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private queueLocation(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->addLocationFromString(Ljava/lang/String;Ljava/util/List;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->pushTestLocations(Ljava/util/List;)V

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    invoke-virtual {v4, v2, v3}, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->setUserTimeMillis(J)V

    :cond_0
    return-void
.end method

.method private queueLocations(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->parseLocationsFromFile(Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->pushTestLocations(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public clearData()V
    .locals 3

    const-string v1, "Clearing response data"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    check-cast v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->setDebugResponse(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    return-void
.end method

.method public clearSavedLocation()V
    .locals 4

    const-string v2, "Cleared saved location"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "lastloc"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public clearStickyLocation()V
    .locals 3

    const-string v1, "Cleared sticky location"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    check-cast v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->pushTestLocations(Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/SensorSignalsOracle;->clearUserTimeMillis()V

    return-void
.end method

.method initializeHandlers()V
    .locals 1

    const v0, 0x7f10025a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10025b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10025d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10025e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10025f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10025c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100260

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100261

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100262

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100263

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100264

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public loadData()V
    .locals 3

    const/4 v2, 0x0

    const-string v1, "Loading response data"

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-static {v2}, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->newInstance(I)Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_response"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public loadLocations()V
    .locals 3

    const-string v1, "Loading location data"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->newInstance(I)Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "locations"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/TestLauncherActivity$FilePickerDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->loadData()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->loadLocations()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->clearStickyLocation()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->sendIntent()V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->clearSavedLocation()V

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->setStationaryTime(I)V

    goto :goto_0

    :pswitch_6
    iget v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mStationarySecs:I

    add-int/lit8 v0, v0, 0x3c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->setStationaryTime(I)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->clearData()V

    goto :goto_0

    :pswitch_8
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->precacheTest()V

    goto :goto_0

    :pswitch_9
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getEntriesFile()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->deleteApplicationFile(Ljava/io/File;)V

    goto :goto_0

    :pswitch_a
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getNotificationsFile()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->deleteApplicationFile(Ljava/io/File;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f10025a
        :pswitch_0
        :pswitch_1
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->teamDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0400c9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->setContentView(I)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getSensorSignalsOracle()Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mSensorSignalsOracle:Lcom/google/android/apps/sidekick/SensorSignalsOracle;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->initializeHandlers()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.apps.sidekick.LOAD_LOCATION"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Location "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->queueLocation(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->finish()V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getEntriesFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f100263

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v0, "Not enabled for a non-debug build"

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->finish()V

    goto :goto_1

    :cond_3
    const-string v2, "com.google.android.apps.sidekick.CLEAR_LOCATIONS"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->clearStickyLocation()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->finish()V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/apps/sidekick/TestLauncherActivity;->REFRESH_TRAFFIC_ACTION:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/sidekick/TrafficIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "force_refresh"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->finish()V

    goto :goto_0

    :cond_5
    sget-object v2, Lcom/google/android/apps/sidekick/TestLauncherActivity;->REFRESH_NOTIFICATIONS_ACTION:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.sidekick.notifications.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->finish()V

    goto :goto_0

    :cond_6
    sget-object v2, Lcom/google/android/apps/sidekick/TestLauncherActivity;->SCHEDULE_NOTIFICATIONS_ACTION:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.sidekick.notifications.SCHEDULE_REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    invoke-virtual {v1, v3}, Lcom/google/geo/sidekick/Sidekick$Interest;->addEntryTypeRestrict(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$Interest;->setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v1

    new-instance v2, Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;->setInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    add-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;->setCallbackTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;

    move-result-object v1

    new-instance v2, Lcom/google/geo/sidekick/Sidekick$EntryTree;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$EntryTree;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->addCallbackWithInterest(Lcom/google/geo/sidekick/Sidekick$EntryTree$CallbackWithInterest;)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v1

    const-string v2, "com.google.android.apps.sidekick.notifications.NEXT_REFRESH"

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->finish()V

    goto/16 :goto_0

    :cond_7
    const-string v2, "com.google.android.apps.sidekick.LOAD_RESPONSE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v1, "response"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->loadEncodedData(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->finish()V

    goto/16 :goto_0

    :cond_8
    const-string v2, "com.google.android.apps.sidekick.BAD_CONNECTION"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->setBadConnectionEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->finish()V

    goto/16 :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.ASSIST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public sendIntent()V
    .locals 3

    const-string v1, "Sending Intent"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    new-instance v0, Lcom/google/android/apps/sidekick/TestLauncherActivity$IntentPickerDialog;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/TestLauncherActivity$IntentPickerDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "intent_picker"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/TestLauncherActivity$IntentPickerDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public setBadConnectionEnabled(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_0

    const-string v1, "Simulating bad connections"

    :goto_0
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    check-cast v0, Lcom/google/android/apps/sidekick/VelvetNetworkClient;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/VelvetNetworkClient;->setDebugBadConnection(Z)V

    return-void

    :cond_0
    const-string v1, "Back to real connections"

    goto :goto_0
.end method

.method public setStationaryTime(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mStationarySecs:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting stationary time to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mStationarySecs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seconds"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    check-cast v0, Lcom/google/android/apps/sidekick/LocationOracleImpl;

    if-lez p1, :cond_0

    iget v1, p0, Lcom/google/android/apps/sidekick/TestLauncherActivity;->mStationarySecs:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->setTestStationaryTimeSecs(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/LocationOracleImpl;->clearTestStationaryTimeSecs()V

    goto :goto_0
.end method
