.class public Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;
.super Ljava/lang/Object;
.source "TgPresenterAccessorImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/TgPresenterAccessor;


# instance fields
.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/EntryProvider;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-void
.end method

.method private getQueryState(Landroid/content/Context;)Lcom/google/android/velvet/presenter/QueryState;
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;->getTgPresenter(Landroid/content/Context;)Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/TgPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getTgPresenter(Landroid/content/Context;)Lcom/google/android/velvet/presenter/TgPresenter;
    .locals 3
    .param p1    # Landroid/content/Context;

    instance-of v2, p1, Lcom/google/android/velvet/presenter/VelvetUi;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/velvet/presenter/VelvetUi;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->getBackFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/velvet/presenter/TgPresenter;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/velvet/presenter/TgPresenter;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public dismissEntry(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-static {p2}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->handleDismissedEntries(Ljava/util/Collection;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;->getTgPresenter(Landroid/content/Context;)Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/TgPresenter;->handleEntryDelete(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    :cond_0
    return-void
.end method

.method public startVisualSearch(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;I)Z
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;->getQueryState(Landroid/content/Context;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/velvet/Query;->goggles(I)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startWebSearch(Landroid/content/Context;Ljava/lang/String;Landroid/location/Location;)Z
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/location/Location;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;->getQueryState(Landroid/content/Context;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/google/android/velvet/Query;->fromPredictive(Ljava/lang/String;Landroid/location/Location;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public toggleBackOfCard(Landroid/content/Context;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/TgPresenterAccessorImpl;->getTgPresenter(Landroid/content/Context;)Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/android/velvet/presenter/TgPresenter;->toggleBackOfCard(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    :cond_0
    return-void
.end method
