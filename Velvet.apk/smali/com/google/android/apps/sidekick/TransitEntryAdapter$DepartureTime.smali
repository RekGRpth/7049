.class Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;
.super Ljava/lang/Object;
.source "TransitEntryAdapter.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/TransitEntryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DepartureTime"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;",
        ">;"
    }
.end annotation


# instance fields
.field public final mDepartureTimeMillis:J

.field public final mHeadSign:Ljava/lang/String;

.field public final mHeadSignColor:I

.field public final mHeadSignTextColor:I

.field public final mLine:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mDepartureTimeMillis:J

    iput-object p3, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mLine:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mHeadSign:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mHeadSignColor:I

    iput p6, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mHeadSignTextColor:I

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;)I
    .locals 4
    .param p1    # Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mDepartureTimeMillis:J

    iget-wide v2, p1, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mDepartureTimeMillis:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->compareTo(Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;)I

    move-result v0

    return v0
.end method

.method public getDepartureTimeString(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/TransitEntryAdapter$DepartureTime;->mDepartureTimeMillis:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
