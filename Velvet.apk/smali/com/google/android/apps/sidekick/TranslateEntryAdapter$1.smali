.class Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;
.super Ljava/lang/Object;
.source "TranslateEntryAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->setTranslateAction(Landroid/content/Context;Landroid/view/View;Landroid/widget/EditText;Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$sourceText:Landroid/widget/EditText;

.field final synthetic val$targetText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/TranslateEntryAdapter;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->val$sourceText:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->val$targetText:Landroid/widget/EditText;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->mTranslateEntry:Lcom/google/geo/sidekick/Sidekick$TranslateEntry;
    invoke-static {v5}, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/TranslateEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->getSourceLanguageCode()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->mTranslateEntry:Lcom/google/geo/sidekick/Sidekick$TranslateEntry;
    invoke-static {v5}, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/TranslateEntryAdapter;)Lcom/google/geo/sidekick/Sidekick$TranslateEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$TranslateEntry;->getTargetLanguageCode()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->val$sourceText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->val$targetText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->val$targetText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v3

    move-object v3, v0

    move-object v0, v2

    :cond_0
    # getter for: Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->TRANSLATE_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->access$100()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "sl"

    invoke-virtual {v5, v6, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "tl"

    invoke-virtual {v5, v6, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "q"

    invoke-virtual {v5, v6, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->val$context:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/apps/sidekick/TranslateEntryAdapter$1;->this$0:Lcom/google/android/apps/sidekick/TranslateEntryAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v7

    const-string v8, "TRANSLATE"

    invoke-virtual {v5, v6, v7, v4, v8}, Lcom/google/android/apps/sidekick/TranslateEntryAdapter;->openUrl(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method
