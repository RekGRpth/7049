.class public Lcom/google/android/apps/sidekick/TravelReport;
.super Ljava/lang/Object;
.source "TravelReport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/TravelReport$1;,
        Lcom/google/android/apps/sidekick/TravelReport$Builder;,
        Lcom/google/android/apps/sidekick/TravelReport$Proximity;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAlternateCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

.field private final mDestination:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

.field private final mProximity:Lcom/google/android/apps/sidekick/TravelReport$Proximity;

.field private final mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

.field private final mSource:Lcom/google/geo/sidekick/Sidekick$Location;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/TravelReport;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/TravelReport;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$FrequentPlace;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/sidekick/TravelReport;->mDestination:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/TravelReport;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-static {p1, p2}, Lcom/google/android/apps/sidekick/TravelReport;->calculateProximity(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Lcom/google/android/apps/sidekick/TravelReport$Proximity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mProximity:Lcom/google/android/apps/sidekick/TravelReport$Proximity;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/TravelReport;->mAlternateCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Lcom/google/android/apps/sidekick/TravelReport$1;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$FrequentPlace;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .param p5    # Lcom/google/android/apps/sidekick/TravelReport$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/sidekick/TravelReport;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/TravelReport;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static calculateProximity(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Lcom/google/android/apps/sidekick/TravelReport$Proximity;
    .locals 9
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/sidekick/TravelReport$Proximity;->UNKNOWN:Lcom/google/android/apps/sidekick/TravelReport$Proximity;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/sidekick/LocationUtilities;->distanceBetween(DDDD)F

    move-result v8

    const v0, 0x48435000

    cmpl-float v0, v8, v0

    if-lez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/sidekick/TravelReport$Proximity;->TOO_FAR:Lcom/google/android/apps/sidekick/TravelReport$Proximity;

    goto :goto_0

    :cond_2
    const/high16 v0, 0x447a0000

    cmpg-float v0, v8, v0

    if-gez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/sidekick/TravelReport$Proximity;->AT_PLACE:Lcom/google/android/apps/sidekick/TravelReport$Proximity;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/sidekick/TravelReport$Proximity;->GOOD_DISTANCE:Lcom/google/android/apps/sidekick/TravelReport$Proximity;

    goto :goto_0
.end method

.method static getTrafficStatusAsString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f0d0106

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0d0108

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0d0107

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private getTransitCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TravelReport;->getTravelMode()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    goto :goto_0
.end method


# virtual methods
.method public buildCommuteString(Landroid/content/Context;)Ljava/lang/String;
    .locals 13
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TravelReport;->getTotalEtaMinutes()Ljava/lang/Integer;

    move-result-object v6

    if-nez v6, :cond_0

    const/4 v7, 0x0

    :goto_0
    return-object v7

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TravelReport;->getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v1

    const-string v3, ""

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasHistoricalTrafficDelayInMinutes()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficDelayInMinutes()I

    move-result v7

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getHistoricalTrafficDelayInMinutes()I

    move-result v8

    sub-int v2, v7, v8

    if-nez v2, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f110009

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficDelayInMinutes()I

    move-result v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficDelayInMinutes()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v8, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficDelayInMinutes()I

    move-result v5

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f110006

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v5, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary()Z

    move-result v7

    if-eqz v7, :cond_5

    const v7, 0x7f0d00f8

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    const/4 v9, 0x1

    aput-object v3, v8, v9

    const/4 v9, 0x2

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_3
    if-lez v2, :cond_4

    const v4, 0x7f110007

    :goto_2
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v7, v4, v0, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_4
    const v4, 0x7f110008

    goto :goto_2

    :cond_5
    const v7, 0x7f0d00f7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    const/4 v9, 0x1

    aput-object v3, v8, v9

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0
.end method

.method getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mDestination:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    return-object v0
.end method

.method public getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    return-object v0
.end method

.method public getRouteDescriptionWithTraffic(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 25
    .param p1    # Landroid/content/Context;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/TravelReport;->getTravelMode()I

    move-result v20

    if-nez v20, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/TravelReport;->getRegularCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficStatusAsString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    if-eqz v11, :cond_1

    if-eqz v15, :cond_1

    const v21, 0x7f0d00f5

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v15, v22, v23

    const/16 v23, 0x1

    aput-object v11, v22, v23

    move-object/from16 v0, p1

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v15

    :cond_0
    :goto_0
    return-object v15

    :cond_1
    if-eqz v11, :cond_2

    invoke-static {v11}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v15

    goto :goto_0

    :cond_2
    if-nez v15, :cond_0

    const/4 v15, 0x0

    goto :goto_0

    :cond_3
    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/TravelReport;->getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    move-result-object v16

    if-nez v16, :cond_4

    const/4 v15, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getStationLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v18

    if-eqz v18, :cond_5

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasWalkingTimeMinutes()Z

    move-result v21

    if-nez v21, :cond_6

    :cond_5
    const/4 v15, 0x0

    goto :goto_0

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    const-wide/16 v23, 0x3e8

    div-long v9, v21, v23

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasDepartureTimeSeconds()Z

    move-result v21

    if-eqz v21, :cond_7

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getDepartureTimeSeconds()J

    move-result-wide v21

    cmp-long v21, v9, v21

    if-lez v21, :cond_8

    :cond_7
    const/4 v15, 0x0

    goto :goto_0

    :cond_8
    new-instance v5, Ljava/util/Date;

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getDepartureTimeSeconds()J

    move-result-wide v21

    const-wide/16 v23, 0x3e8

    mul-long v21, v21, v23

    move-wide/from16 v0, v21

    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-static/range {p1 .. p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineName()Ljava/lang/String;

    move-result-object v17

    const v21, 0x7f0d00f6

    const/16 v22, 0x4

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v17, v22, v23

    const/16 v23, 0x1

    aput-object v8, v22, v23

    const/16 v23, 0x2

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getWalkingTimeMinutes()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x3

    invoke-virtual/range {v18 .. v18}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, p1

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    new-instance v12, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v19

    invoke-direct {v12, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const/16 v21, -0x1

    move/from16 v0, v21

    if-le v13, v0, :cond_a

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v21

    add-int v6, v13, v21

    new-instance v4, Landroid/text/style/StyleSpan;

    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-direct {v4, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v4, v13, v6, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineBackgroundColor()Z

    move-result v21

    if-eqz v21, :cond_9

    new-instance v3, Landroid/text/style/BackgroundColorSpan;

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineBackgroundColor()I

    move-result v21

    move/from16 v0, v21

    invoke-direct {v3, v0}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v3, v13, v6, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_9
    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->hasTransitLineForegroundColor()Z

    move-result v21

    if-eqz v21, :cond_a

    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getTransitLineForegroundColor()I

    move-result v21

    move/from16 v0, v21

    invoke-direct {v7, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v7, v13, v6, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_a
    move-object v15, v12

    goto/16 :goto_0

    :cond_b
    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    const v21, 0x7f0d019c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_0

    :cond_c
    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_d

    const v21, 0x7f0d019d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_0

    :cond_d
    const/4 v15, 0x0

    goto/16 :goto_0
.end method

.method getRouteSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasRouteSummary()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getRouteSummary()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method getSource()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mSource:Lcom/google/geo/sidekick/Sidekick$Location;

    return-object v0
.end method

.method public getTotalEtaMinutes()Ljava/lang/Integer;
    .locals 12

    const-wide/16 v6, 0x0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelTimeWithoutDelayInMinutes()Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TravelReport;->getTravelMode()I

    move-result v5

    const/4 v2, 0x0

    if-nez v5, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficDelayInMinutes()I

    move-result v2

    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelTimeWithoutDelayInMinutes()I

    move-result v6

    add-int/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_0

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v0, v8, v10

    iget-object v8, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTransitDetails()Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;->getDepartureTimeSeconds()J

    move-result-wide v8

    sub-long v3, v8, v0

    :goto_2
    cmp-long v6, v3, v6

    if-lez v6, :cond_2

    long-to-double v6, v3

    const-wide/high16 v8, 0x404e000000000000L

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    goto :goto_1

    :cond_4
    move-wide v3, v6

    goto :goto_2
.end method

.method getTrafficColor(Landroid/content/Context;)I
    .locals 3
    .param p1    # Landroid/content/Context;

    const v0, 0x7f09002f

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficStatus()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    return v2

    :pswitch_0
    const v0, 0x7f090038

    goto :goto_0

    :pswitch_1
    const v0, 0x7f09003a

    goto :goto_0

    :pswitch_2
    const v0, 0x7f090039

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method getTrafficColorForHtml(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficColor(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "#%1$h"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0xffffff

    and-int/2addr v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getTrafficStatus()I
    .locals 5

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/TravelReport;->mAlternateCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mAlternateCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    :cond_1
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTrafficStatus()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/android/apps/sidekick/TravelReport;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected commute to have traffic status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTrafficStatus()I

    move-result v1

    goto :goto_0
.end method

.method getTrafficStatusAsString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficStatus()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-static {p1, v0}, Lcom/google/android/apps/sidekick/TravelReport;->getTrafficStatusAsString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/TravelReport;->getTransitCommute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTransitDetails()Lcom/google/geo/sidekick/Sidekick$CommuteSummary$TransitDetails;

    move-result-object v1

    goto :goto_0
.end method

.method public getTravelMode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/TravelReport;->mRegularCommute:Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->hasTravelMode()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CommuteSummary;->getTravelMode()I

    move-result v1

    goto :goto_0
.end method
