.class public Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "VisualSearchListEntryAdapter.java"


# instance fields
.field private final mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mSubtitle:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/searchcommon/util/IntentUtils;
    .param p4    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p5    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mTitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->hasGroupEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchListEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getGroupEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchListEntry()Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$GenericCardEntry;->getText()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mSubtitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mEntries:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->expandCard(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->openUrl(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;)Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Ljava/lang/String;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->recordClickAction(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Ljava/lang/String;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;)Lcom/google/android/searchcommon/util/IntentUtils;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    return-object v0
.end method

.method private addPlace(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 30
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    invoke-virtual/range {p2 .. p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v25

    const v2, 0x7f0400d2

    const/4 v3, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v27

    const v2, 0x7f100193

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {v25 .. v25}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v19

    const/16 v2, 0x14

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v7

    const v2, 0x7f100032

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasOpenHoursToday()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getOpenHoursToday()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v2, 0x7f100278

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/TextView;

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasWebsite()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getWebsite()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const v2, 0x7f100034

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCoverPhoto()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasUrl()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCoverPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrlType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v2, 0x7f0c0073

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0c0074

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/sidekick/FifeImageUrlUtil;->setImageUrlSmartCrop(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v21

    :cond_0
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$Photo;->hasInfoUrl()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {v24 .. v24}, Lcom/google/geo/sidekick/Sidekick$Photo;->getInfoUrl()Ljava/lang/String;

    move-result-object v5

    new-instance v2, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$4;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v6, p2

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$4;-><init>(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_2
    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasWebsite()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getWebsite()Ljava/lang/String;

    move-result-object v16

    new-instance v13, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v17, p2

    move-object/from16 v18, v7

    invoke-direct/range {v13 .. v18}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$6;-><init>(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    move-object/from16 v0, p3

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_3
    const/16 v2, 0x8

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    const/16 v2, 0x8

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->hasCid()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getCid()J

    move-result-wide v11

    new-instance v8, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$5;

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v13, p2

    move-object v14, v7

    invoke-direct/range {v8 .. v14}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$5;-><init>(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;JLcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lcom/google/android/velvet/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_6
    const/4 v2, 0x4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method private expandCard(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4

    const v5, 0x7f10005a

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p3, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasVisualSearchEntry()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    const-string v5, "VisualSearchListEntryAdapter"

    const-string v6, "Unexpected Entry without place data."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->addPlace(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    add-int/lit8 v2, v2, 0x1

    const/16 v5, 0xa

    if-ne v2, v5, :cond_0

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mEntries:Ljava/util/List;

    invoke-static {p2, p3, v5}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->addListCardTags(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/List;)V

    const/16 v5, 0x16

    invoke-virtual {p0, v5}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    if-eqz v3, :cond_4

    const v5, 0x7f100036

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Action;->getDisplayMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;

    invoke-direct {v5, p0, v4}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/widget/Button;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    return-void
.end method

.method private openUrl(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeViewUrl(Landroid/content/Context;Ljava/lang/String;Z)Z

    return-void
.end method

.method private recordClickAction(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Ljava/lang/String;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "CARD_BUTTON_PRESS"

    invoke-virtual {v0, v1, p0, p4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    invoke-direct {v0, p5, p1, p2, p3}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d020d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d017a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d017b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getVisualSearchEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v7

    if-eqz v7, :cond_0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v7

    invoke-static {p1, v7}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getBusinessData()Lcom/google/geo/sidekick/Sidekick$BusinessData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$BusinessData;->getTypeList()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_2
    const/4 v4, 0x1

    :cond_3
    if-nez v4, :cond_4

    if-nez v0, :cond_5

    :cond_4
    const v7, 0x7f0d02cc

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_0
    return-object v7

    :cond_5
    const v7, 0x7f0d02ca

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/searchcommon/preferences/cards/AtPlaceCardSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v5, 0x7f0400d1

    const/4 v6, 0x0

    invoke-virtual {p2, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v5, 0x7f100031

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mTitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f10005a

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mSubtitle:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f100191

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/CardTableLayout;

    new-instance v5, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$1;

    invoke-direct {v5, p0, p1, v0}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v2, v5}, Lcom/google/android/apps/sidekick/CardTableLayout;->setAdapter(Lcom/google/android/apps/sidekick/CardTableLayout$Adapter;)V

    new-instance v1, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$2;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;Lcom/google/android/apps/sidekick/CardTableLayout;)V

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public shouldDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mEntries:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/VisualSearchListEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
