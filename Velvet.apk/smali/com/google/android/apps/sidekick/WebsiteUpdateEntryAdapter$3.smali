.class Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$3;
.super Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;
.source "WebsiteUpdateEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 0
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    invoke-direct {p0, p2}, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    return-void
.end method


# virtual methods
.method public getQuestionCount(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$3;->this$0:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    # getter for: Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->access$000(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWebsiteUpdateEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->getWebsiteTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const v0, 0x7f0d0312

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
