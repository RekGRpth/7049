.class public Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;
.super Lcom/google/android/apps/sidekick/BaseEntryAdapter;
.source "WebsiteUpdateEntryAdapter.java"


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;
    .param p2    # Lcom/google/android/apps/sidekick/TgPresenterAccessor;
    .param p3    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;
    .param p5    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p6    # Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    iput-object p4, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mTitle:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;

    return-object v0
.end method

.method private addUpdateRow(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/google/android/apps/sidekick/CardTableLayout;Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/google/android/apps/sidekick/CardTableLayout;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    const v1, 0x7f0400dd

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100295

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->getUpdateTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p4, v0, p1}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->populateWebsiteUpdateView(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;Landroid/view/View;Landroid/content/Context;)V

    invoke-virtual {p3, v0}, Lcom/google/android/apps/sidekick/CardTableLayout;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;

    invoke-direct {v1, p0, p1, p4}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$1;-><init>(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private createMultipleUpdatesCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    const v4, 0x7f0400dc

    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    const v4, 0x7f100031

    invoke-virtual {v13, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mTitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v4, Landroid/animation/LayoutTransition;

    invoke-direct {v4}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v13, v4}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    const v4, 0x7f100294

    invoke-virtual {v13, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/sidekick/CardTableLayout;

    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/android/apps/sidekick/CardTableLayout;->setExpanded(Z)V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v7

    const/4 v15, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v15, v4, :cond_0

    const/4 v4, 0x6

    if-gt v15, v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v4, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWebsiteUpdateEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->addUpdateRow(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/google/android/apps/sidekick/CardTableLayout;Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)Landroid/view/View;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v7, v0, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;

    move-object/from16 v0, v17

    invoke-static {v13, v0, v4}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->addListCardTags(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/List;)V

    move-object v4, v13

    check-cast v4, Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    const v5, 0x7f100294

    invoke-virtual {v13, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->setDismissableContainer(Landroid/view/ViewGroup;)V

    move-object/from16 v18, v13

    check-cast v18, Lcom/google/android/apps/sidekick/DismissableLinearLayout;

    new-instance v4, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->getTgPresenter()Lcom/google/android/apps/sidekick/TgPresenterAccessor;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    move-object/from16 v6, p0

    move-object/from16 v8, p1

    invoke-direct/range {v4 .. v12}, Lcom/google/android/apps/sidekick/ListEntryDismissHandler;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/util/Map;Landroid/content/Context;Lcom/google/android/apps/sidekick/TgPresenterAccessor;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/searchcommon/util/Clock;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/android/apps/sidekick/DismissableLinearLayout;->setOnDismissListener(Lcom/google/android/apps/sidekick/DismissableLinearLayout$OnDismissListener;)V

    return-object v13
.end method

.method private createSingleWebsiteUpdateCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    const v2, 0x7f0400db

    const/4 v3, 0x0

    invoke-virtual {p2, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f100031

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->getUpdateTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p4, v0, p1}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->populateWebsiteUpdateView(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;Landroid/view/View;Landroid/content/Context;)V

    const v2, 0x7f100293

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$2;

    invoke-direct {v2, p0, p1, p4}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$2;-><init>(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private populateWebsiteUpdateView(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;Landroid/view/View;Landroid/content/Context;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->hasImage()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f1000ed

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->getImage()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    :cond_0
    const v1, 0x7f100292

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->createWebsiteInfoSpan(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;Landroid/content/Context;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public createWebsiteInfoSpan(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;Landroid/content/Context;)Landroid/text/SpannableString;
    .locals 11
    .param p1    # Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;
    .param p2    # Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->getUpdateTimestampSeconds()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    const/high16 v6, 0x80000

    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    move-result-object v9

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "<font color=\"#%h\">%s</font>"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0xffffff

    and-int/2addr v4, v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->getWebsiteTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-instance v8, Landroid/text/SpannableString;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s - %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v10, v2, v3

    const/4 v3, 0x1

    aput-object v9, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v8
.end method

.method public bridge synthetic examplify()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->examplify()V

    return-void
.end method

.method public bridge synthetic findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findAction(I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$3;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter$3;-><init>(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    new-instance v1, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V

    return-object v1
.end method

.method public getEntryTypeBackString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeDisplayName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryTypeSummaryString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getJustification(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0311

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocation()Lcom/google/geo/sidekick/Sidekick$Location;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLoggingName()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWebsiteUpdateEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->createSingleWebsiteUpdateCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;)Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->createMultipleUpdatesCard(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getWebsiteUpdateEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->mEntries:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic isDismissed()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->isDismissed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchDetails(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->launchDetails(Landroid/content/Context;)V

    return-void
.end method

.method public bridge synthetic maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    return-void
.end method

.method public bridge synthetic prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    return-void
.end method

.method public bridge synthetic registerActions(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setDismissed(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->setDismissed(Z)V

    return-void
.end method

.method public bridge synthetic shouldDisplay()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->shouldDisplay()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic supportsDismissTrail(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/BaseEntryAdapter;->supportsDismissTrail(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
