.class public Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;
.super Landroid/app/DialogFragment;
.source "ChangeTransportationModeDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;
    .locals 3
    .param p0    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "travel_mode_setting"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v10, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "travel_mode_setting"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v2

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->newNowConfigurationPreferences(Landroid/content/SharedPreferences;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v3

    const v0, 0x7f0f0031

    const-string v8, "locale_configuration.biking_directions_enabled"

    invoke-virtual {v3, v8, v10}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_0

    const v0, 0x7f0f0033

    :cond_0
    if-ne v6, v10, :cond_1

    const v5, 0x7f0d0198

    :goto_0
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v9, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;

    invoke-direct {v9, p0, v6, v3, v1}, Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment$1;-><init>(Lcom/google/android/apps/sidekick/actions/ChangeTransportationModeDialogFragment;ILcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V

    invoke-virtual {v8, v0, v9}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8

    :cond_1
    const v5, 0x7f0d0199

    goto :goto_0
.end method
