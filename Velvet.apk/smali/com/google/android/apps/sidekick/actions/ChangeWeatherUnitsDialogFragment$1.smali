.class Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;
.super Ljava/lang/Object;
.source "ChangeWeatherUnitsDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;

.field final synthetic val$entryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field final synthetic val$preferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;->val$preferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;->val$entryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;->val$preferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;

    const v2, 0x7f0d005a

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;->val$entryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;->dismiss()V

    return-void
.end method
