.class public Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;
.super Landroid/app/DialogFragment;
.source "ChangeWeatherUnitsDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance()Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->newNowConfigurationPreferences(Landroid/content/SharedPreferences;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v2

    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0f0041

    new-instance v7, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;

    invoke-direct {v7, p0, v2, v0}, Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment$1;-><init>(Lcom/google/android/apps/sidekick/actions/ChangeWeatherUnitsDialogFragment;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f0d01b4

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5
.end method
