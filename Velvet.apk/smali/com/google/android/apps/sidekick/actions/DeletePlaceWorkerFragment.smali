.class public Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;
.super Landroid/app/Fragment;
.source "DeletePlaceWorkerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;
    }
.end annotation


# instance fields
.field private mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

.field private mTask:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->handleResponse(Z)V

    return-void
.end method

.method private getTgPresenter()Lcom/google/android/velvet/presenter/TgPresenter;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/VelvetActivity;->getBackFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/velvet/presenter/TgPresenter;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/velvet/presenter/TgPresenter;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleResponse(Z)V
    .locals 8
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v7, "action_key"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    const-string v7, "entry_key"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->hide(Landroid/app/FragmentManager;)V

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v7

    invoke-virtual {v7, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->findAdapterForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    invoke-interface {v4, v7}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->handleDismissedEntries(Ljava/util/Collection;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getTgPresenter()Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->handleEntryDelete(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    :cond_0
    return-void
.end method

.method public static newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;
    .locals 4
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "entry_key"

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "action_key"

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Action;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    new-instance v1, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "entry_key"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const-string v3, "action_key"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->show(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    new-instance v3, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v4

    invoke-direct {v3, p0, v2, v0, v4}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;-><init>(Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V

    iput-object v3, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;->cancel(Z)Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/DeletePlaceWorkerFragment$SendDeleteActionTask;

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method
