.class public Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;
.super Ljava/lang/Object;
.source "DismissEntryTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/DismissEntryTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mActionsQueryBuilder:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

.field private final mClientRunnablesBuilder:Lcom/google/common/collect/ImmutableList$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList$Builder",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private mInvalidateEntries:Z

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mTimestamp:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mActionsQueryBuilder:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mTimestamp:J

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mClientRunnablesBuilder:Lcom/google/common/collect/ImmutableList$Builder;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-void
.end method


# virtual methods
.method public addClientRunnable(Ljava/lang/Runnable;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mClientRunnablesBuilder:Lcom/google/common/collect/ImmutableList$Builder;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList$Builder;

    return-object p0
.end method

.method public addEntry(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;
    .locals 6
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;-><init>()V

    invoke-virtual {v1, p2}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setAction(Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mTimestamp:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;->setTimestampSeconds(J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mActionsQueryBuilder:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    invoke-virtual {v1, v0}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;->addExecutedUserAction(Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;)Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    return-object p0
.end method

.method public build()Lcom/google/android/apps/sidekick/actions/DismissEntryTask;
    .locals 7

    new-instance v0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mActionsQueryBuilder:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mClientRunnablesBuilder:Lcom/google/common/collect/ImmutableList$Builder;

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList$Builder;->build()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mInvalidateEntries:Z

    iget-object v5, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$ActionsQuery;Ljava/util/List;ZLcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$1;)V

    return-object v0
.end method

.method public setTimestamp(J)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->mTimestamp:J

    return-object p0
.end method
