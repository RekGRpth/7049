.class public Lcom/google/android/apps/sidekick/actions/DismissEntryTask;
.super Landroid/os/AsyncTask;
.source "DismissEntryTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/actions/DismissEntryTask$1;,
        Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/geo/sidekick/Sidekick$ResponsePayload;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActionsQuery:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

.field private final mClientRunnables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mInvalidateEntries:Z

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$ActionsQuery;Ljava/util/List;ZLcom/google/android/apps/sidekick/inject/EntryProvider;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$ActionsQuery;
    .param p4    # Z
    .param p5    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/inject/NetworkClient;",
            "Lcom/google/geo/sidekick/Sidekick$ActionsQuery;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;Z",
            "Lcom/google/android/apps/sidekick/inject/EntryProvider;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mActionsQuery:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mClientRunnables:Ljava/util/List;

    iput-boolean p4, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mInvalidateEntries:Z

    iput-object p5, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$ActionsQuery;Ljava/util/List;ZLcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$ActionsQuery;
    .param p3    # Ljava/util/List;
    .param p4    # Z
    .param p5    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p6    # Lcom/google/android/apps/sidekick/actions/DismissEntryTask$1;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$ActionsQuery;Ljava/util/List;ZLcom/google/android/apps/sidekick/inject/EntryProvider;)V

    return-void
.end method

.method public static newBuilder(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p1    # Lcom/google/android/apps/sidekick/inject/EntryProvider;

    new-instance v0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V

    return-object v0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mClientRunnables:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mActionsQuery:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;->getExecutedUserActionCount()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    new-instance v4, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mActionsQuery:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setActionsQuery(Lcom/google/geo/sidekick/Sidekick$ActionsQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v3, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->TAG:Ljava/lang/String;

    const-string v4, "Error sending dismiss request to the server"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->doInBackground([Ljava/lang/Void;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public hasActions()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mActionsQuery:Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;->getExecutedUserActionCount()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mClientRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mInvalidateEntries:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    return-void
.end method
