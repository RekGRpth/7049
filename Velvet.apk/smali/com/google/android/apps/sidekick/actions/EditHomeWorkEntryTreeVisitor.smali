.class public Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;
.super Lcom/google/android/apps/sidekick/EntryTreeVisitor;
.source "EditHomeWorkEntryTreeVisitor.java"


# instance fields
.field private final mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mTargetEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;-><init>()V

    new-instance v0, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v0, p1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mTargetEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    return-void
.end method


# virtual methods
.method protected process(Lcom/google/android/apps/sidekick/ProtoKey;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 3
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mTargetEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasName()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$Location;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Location;->hasAddress()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Location;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$Location;->setAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    :cond_1
    :goto_0
    const/16 v1, 0x10

    invoke-static {p2, v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->removeAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)V

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/EditHomeWorkEntryTreeVisitor;->mEditedLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    goto :goto_0
.end method
