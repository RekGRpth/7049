.class public abstract Lcom/google/android/apps/sidekick/actions/EntryActionBase;
.super Ljava/lang/Object;
.source "EntryActionBase.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/actions/EntryAction;


# instance fields
.field protected final mAction:Lcom/google/geo/sidekick/Sidekick$Action;

.field private mCallback:Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;

.field protected final mContext:Landroid/content/Context;

.field protected final mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p4    # Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mAction:Lcom/google/geo/sidekick/Sidekick$Action;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mCallback:Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mCallback:Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mCallback:Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;

    invoke-interface {v0, p0}, Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;->onFailure(Lcom/google/android/apps/sidekick/actions/EntryAction;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mContext:Landroid/content/Context;

    const v1, 0x7f0d00e7

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public success(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mCallback:Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/EntryActionBase;->mCallback:Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/sidekick/actions/EntryAction$Callback;->onSuccess(Lcom/google/android/apps/sidekick/actions/EntryAction;Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    :cond_0
    return-void
.end method
