.class public Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;
.super Lcom/google/android/apps/sidekick/actions/RecordActionTask;
.source "InvalidatingRecordActionTask.java"


# instance fields
.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p5    # Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-direct {p0, p1, p3, p4, p5}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p5    # Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .param p6    # Lcom/google/android/searchcommon/util/Clock;

    invoke-static {p4}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    return-void
.end method


# virtual methods
.method protected onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-super {p0, p1}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->onPostExecute(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/InvalidatingRecordActionTask;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$ResponsePayload;)V

    return-void
.end method
