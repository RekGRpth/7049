.class public Lcom/google/android/apps/sidekick/actions/RecordActionTask;
.super Landroid/os/AsyncTask;
.source "RecordActionTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/actions/RecordActionTask$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/geo/sidekick/Sidekick$ResponsePayload;",
        ">;"
    }
.end annotation


# static fields
.field private static final sTag:Ljava/lang/String;


# instance fields
.field private final mActions:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation
.end field

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mNewPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->sTag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-static {p4}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/inject/NetworkClient;",
            "Landroid/content/Context;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/inject/NetworkClient;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;",
            "Lcom/google/geo/sidekick/Sidekick$PlaceData;",
            "Lcom/google/android/searchcommon/util/Clock;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mActions:Ljava/util/Collection;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mNewPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method


# virtual methods
.method protected buildExecutedAction(Lcom/google/geo/sidekick/Sidekick$Action;J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p2    # J

    new-instance v0, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;J)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mNewPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mNewPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->withCustomPlace(Lcom/google/geo/sidekick/Sidekick$PlaceData;)Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;

    move-result-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/actions/ExecutedUserActionBuilder;->build()Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v1

    return-object v1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 3
    .param p1    # [Ljava/lang/Void;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->recordAction(J)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->sTag:Ljava/lang/String;

    const-string v2, "Error sending request to the server"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->doInBackground([Ljava/lang/Void;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method recordAction(J)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;
    .locals 5
    .param p1    # J

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mActions:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->buildExecutedAction(Lcom/google/geo/sidekick/Sidekick$Action;J)Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/geo/sidekick/Sidekick$ActionsQuery;->addExecutedUserAction(Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;)Lcom/google/geo/sidekick/Sidekick$ActionsQuery;

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v4, v1}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setActionsQuery(Lcom/google/geo/sidekick/Sidekick$ActionsQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-interface {v4, v3}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v4

    return-object v4
.end method
