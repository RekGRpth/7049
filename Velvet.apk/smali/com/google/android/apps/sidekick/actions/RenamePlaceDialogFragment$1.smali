.class Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;
.super Ljava/lang/Object;
.source "RenamePlaceDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

.field final synthetic val$editText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;->val$editText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->hideSoftKeyboard(Landroid/view/View;)V

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$PlaceData;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->setDisplayName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    # invokes: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->startEditPlaceTask(Lcom/google/geo/sidekick/Sidekick$PlaceData;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->access$000(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Lcom/google/geo/sidekick/Sidekick$PlaceData;)V

    return-void
.end method
