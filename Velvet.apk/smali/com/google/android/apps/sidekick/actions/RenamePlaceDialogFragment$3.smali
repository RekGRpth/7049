.class Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;
.super Ljava/lang/Object;
.source "RenamePlaceDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

.field final synthetic val$editText:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;->val$editText:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->hideSoftKeyboard(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    # getter for: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->access$100(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    # getter for: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;
    invoke-static {v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->access$200(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "delete_place_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/sidekick/actions/DeletePlaceDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
