.class Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;
.super Ljava/lang/Object;
.source "RenamePlaceDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ListAdapterUnion"
.end annotation


# instance fields
.field private final label:Ljava/lang/String;

.field private final placeData:Lcom/google/geo/sidekick/Sidekick$PlaceData;


# direct methods
.method private constructor <init>(Lcom/google/geo/sidekick/Sidekick$PlaceData;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->placeData:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->label:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .param p2    # Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;-><init>(Lcom/google/geo/sidekick/Sidekick$PlaceData;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;)Lcom/google/geo/sidekick/Sidekick$PlaceData;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->placeData:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->placeData:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->placeData:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->label:Ljava/lang/String;

    goto :goto_0
.end method
