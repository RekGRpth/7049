.class Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "RenamePlaceDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RenameArrayAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    const v0, 0x1090009

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;

    # getter for: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->placeData:Lcom/google/geo/sidekick/Sidekick$PlaceData;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;->access$300(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;)Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v1

    # invokes: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->getItemViewType(Lcom/google/geo/sidekick/Sidekick$PlaceData;)I
    invoke-static {v1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->access$500(Lcom/google/geo/sidekick/Sidekick$PlaceData;)I

    move-result v1

    return v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;->getItemViewType(I)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;->this$0:Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    # invokes: Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->createViewFromResource(ILjava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    invoke-static {v0, v1, p2, p3, v2}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->access$600(ILjava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method
