.class Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;
.super Ljava/lang/Object;
.source "RenamePlaceDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewFactory"
.end annotation


# static fields
.field private static final sResourceMap:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v0, 0x3

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->sResourceMap:[[I

    return-void

    :array_0
    .array-data 4
        0x7f0400a6
        0x7f1001ff
    .end array-data

    :array_1
    .array-data 4
        0x7f0400a7
        0x7f100201
    .end array-data

    :array_2
    .array-data 4
        0x1090009
        0x0
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/geo/sidekick/Sidekick$PlaceData;)I
    .locals 1
    .param p0    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-static {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->getItemViewType(Lcom/google/geo/sidekick/Sidekick$PlaceData;)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(ILjava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1
    .param p0    # I
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->createViewFromResource(ILjava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static createViewFromResource(ILjava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 9
    .param p0    # I
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/view/LayoutInflater;

    const/4 v8, 0x0

    sget-object v6, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->sResourceMap:[[I

    aget-object v6, v6, p0

    aget v2, v6, v8

    sget-object v6, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;->sResourceMap:[[I

    aget-object v6, v6, p0

    const/4 v7, 0x1

    aget v5, v6, v7

    if-nez p2, :cond_1

    invoke-virtual {p4, v2, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    :goto_0
    if-eqz p1, :cond_0

    if-nez v5, :cond_2

    :try_start_0
    move-object v0, v4

    check-cast v0, Landroid/widget/TextView;

    move-object v3, v0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v4

    :cond_1
    move-object v4, p2

    goto :goto_0

    :cond_2
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Expected TextView"

    invoke-direct {v6, v7, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
.end method

.method private static getItemViewType(Lcom/google/geo/sidekick/Sidekick$PlaceData;)I
    .locals 1
    .param p0    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasContactData()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->hasBusinessData()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
