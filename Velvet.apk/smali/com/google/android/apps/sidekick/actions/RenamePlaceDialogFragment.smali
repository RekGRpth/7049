.class public Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;
.super Lcom/google/android/apps/sidekick/actions/BaseEditDialogFragment;
.source "RenamePlaceDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;,
        Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;,
        Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ViewFactory;
    }
.end annotation


# instance fields
.field private mCurrentName:Ljava/lang/String;

.field private mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

.field private mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

.field private mRenameAction:Lcom/google/geo/sidekick/Sidekick$Action;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/actions/BaseEditDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Lcom/google/geo/sidekick/Sidekick$PlaceData;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->startEditPlaceTask(Lcom/google/geo/sidekick/Sidekick$PlaceData;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

    return-object v0
.end method

.method public static newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$Action;)Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;
    .locals 4
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Action;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "entry_key"

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "rename_action_key"

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Action;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    if-eqz p2, :cond_0

    const-string v2, "delete_action_key"

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Action;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_0
    new-instance v1, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private startEditPlaceTask(Lcom/google/geo/sidekick/Sidekick$PlaceData;)V
    .locals 4
    .param p1    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "editPlaceWorkerFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mRenameAction:Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-static {v2, v3, p1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$PlaceData;)Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "editPlaceWorkerFragment"

    invoke-virtual {v2, v1, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method


# virtual methods
.method getAlternatePlaces()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;",
            ">;"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getAlternatePlaceDataCount()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getAlternatePlaceDataList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mCurrentName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$ListAdapterUnion;-><init>(Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method initFrequentPlace()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasPlaceData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getPlaceData()Lcom/google/geo/sidekick/Sidekick$PlaceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mCurrentName:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Location;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mCurrentName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mCurrentName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mCurrentName:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v8, "entry_key"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const-string v8, "rename_action_key"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mRenameAction:Lcom/google/geo/sidekick/Sidekick$Action;

    const-string v8, "delete_action_key"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "delete_action_key"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->initFrequentPlace()V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getAlternatePlaces()Ljava/util/List;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v0, p0, v8, v6}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$RenameArrayAdapter;-><init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f0400a8

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v8, 0x7f100202

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v8, 0x7f100193

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mFrequentPlace:Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    invoke-static {v8, v9}, Lcom/google/android/apps/sidekick/PlaceUtils;->getPlaceName(Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$FrequentPlace;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/16 v8, 0x2000

    invoke-virtual {v5, v8}, Landroid/widget/EditText;->setInputType(I)V

    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0d0210

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const/high16 v9, 0x1040000

    new-instance v10, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$2;

    invoke-direct {v10, p0, v5}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$2;-><init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x104000a

    new-instance v10, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;

    invoke-direct {v10, p0, v5}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$1;-><init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;->mDeleteAction:Lcom/google/geo/sidekick/Sidekick$Action;

    if-eqz v8, :cond_1

    const v8, 0x7f0d0212

    new-instance v9, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;

    invoke-direct {v9, p0, v5}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$3;-><init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v2, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_1
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    new-instance v8, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;

    invoke-direct {v8, p0, v6, v5, v3}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$4;-><init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Ljava/util/List;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v8, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$5;

    invoke-direct {v8, p0, v5}, Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment$5;-><init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v3, v8}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v8

    const/4 v9, 0x5

    invoke-virtual {v8, v9}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v3
.end method
