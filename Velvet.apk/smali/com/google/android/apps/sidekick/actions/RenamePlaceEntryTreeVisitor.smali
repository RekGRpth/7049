.class public Lcom/google/android/apps/sidekick/actions/RenamePlaceEntryTreeVisitor;
.super Lcom/google/android/apps/sidekick/EntryTreeVisitor;
.source "RenamePlaceEntryTreeVisitor.java"


# instance fields
.field private final mRenamedPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

.field private final mTargetEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$PlaceData;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/EntryTreeVisitor;-><init>()V

    new-instance v0, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v0, p1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceEntryTreeVisitor;->mTargetEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceEntryTreeVisitor;->mRenamedPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    return-void
.end method


# virtual methods
.method protected process(Lcom/google/android/apps/sidekick/ProtoKey;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 2
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/sidekick/ProtoKey",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceEntryTreeVisitor;->mTargetEntryKey:Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasFrequentPlaceEntry()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->hasFrequentPlace()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFrequentPlaceEntry()Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlaceEntry;->getFrequentPlace()Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceEntryTreeVisitor;->mRenamedPlace:Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-virtual {v0, v1}, Lcom/google/geo/sidekick/Sidekick$FrequentPlace;->setPlaceData(Lcom/google/geo/sidekick/Sidekick$PlaceData;)Lcom/google/geo/sidekick/Sidekick$FrequentPlace;

    :cond_0
    const/16 v1, 0x10

    invoke-static {p2, v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->removeAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)V

    :cond_1
    return-void
.end method
