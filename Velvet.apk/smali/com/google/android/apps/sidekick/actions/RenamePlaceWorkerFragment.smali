.class public Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;
.super Landroid/app/Fragment;
.source "RenamePlaceWorkerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;
    }
.end annotation


# instance fields
.field private mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

.field private mTask:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->handleResponse(Z)V

    return-void
.end method

.method private getTgPresenter()Lcom/google/android/velvet/presenter/TgPresenter;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/VelvetActivity;->getBackFragmentPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/velvet/presenter/TgPresenter;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/velvet/presenter/TgPresenter;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleResponse(Z)V
    .locals 8
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v6, "entry_key"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    const-string v6, "action_key"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    new-instance v6, Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-direct {v6}, Lcom/google/geo/sidekick/Sidekick$PlaceData;-><init>()V

    const-string v7, "renamed_place_key"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/sidekick/ProtoUtils;->getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->hide(Landroid/app/FragmentManager;)V

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v6

    invoke-virtual {v6, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    if-eqz p1, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v6, v3}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->findAdapterForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    new-instance v7, Lcom/google/android/apps/sidekick/actions/RenamePlaceEntryTreeVisitor;

    invoke-direct {v7, v3, v4}, Lcom/google/android/apps/sidekick/actions/RenamePlaceEntryTreeVisitor;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$PlaceData;)V

    invoke-interface {v6, v7}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->mutateEntries(Lcom/google/android/apps/sidekick/EntryTreeVisitor;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getTgPresenter()Lcom/google/android/velvet/presenter/TgPresenter;

    move-result-object v5

    if-eqz v5, :cond_0

    instance-of v6, v1, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    if-eqz v6, :cond_0

    check-cast v1, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v5, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->handlePlaceEdit(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;)V

    :cond_0
    return-void
.end method

.method public static newInstance(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$PlaceData;)Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;
    .locals 4
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$PlaceData;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "entry_key"

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Entry;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "action_key"

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Action;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "renamed_place_key"

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$PlaceData;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    new-instance v1, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v0, "entry_key"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const-string v0, "action_key"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ProtoUtils;->getActionFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$PlaceData;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$PlaceData;-><init>()V

    const-string v1, "renamed_place_key"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->getFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$PlaceData;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/apps/sidekick/actions/WorkerFragmentSpinnerDialog;->show(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;-><init>(Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;Lcom/google/geo/sidekick/Sidekick$PlaceData;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;->cancel(Z)Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment;->mTask:Lcom/google/android/apps/sidekick/actions/RenamePlaceWorkerFragment$SendRenameActionTask;

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    return-void
.end method
