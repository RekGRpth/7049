.class public Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;
.super Ljava/lang/Object;
.source "ViewPlacePageAction.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/actions/EntryAction;


# instance fields
.field private final mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

.field private final mCid:J

.field private final mContext:Landroid/content/Context;

.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # Lcom/google/android/searchcommon/util/IntentUtils;
    .param p5    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mContext:Landroid/content/Context;

    iput-wide p2, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mCid:J

    iput-object p4, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    return-void
.end method


# virtual methods
.method buildPlacePageUri()Landroid/net/Uri;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://maps.google.com/maps/place?cid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mCid:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method getPlacePageIntent()Landroid/content/Intent;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->buildPlacePageUri()Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "com.google.android.apps.maps"

    const-string v4, "com.google.android.maps.MapsActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mContext:Landroid/content/Context;

    invoke-interface {v2, v3, v1}, Lcom/google/android/searchcommon/util/IntentUtils;->isIntentHandled(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    :cond_0
    return-object v1
.end method

.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/actions/ViewPlacePageAction;->getPlacePageIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeStartActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    return-void
.end method
