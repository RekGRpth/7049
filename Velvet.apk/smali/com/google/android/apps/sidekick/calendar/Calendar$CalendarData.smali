.class public final Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/Calendar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CalendarData"
.end annotation


# instance fields
.field private cachedSize:I

.field private clientActions_:Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

.field private eventData_:Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

.field private hasClientActions:Z

.field private hasEventData:Z

.field private hasServerData:Z

.field private serverData_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->eventData_:Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->serverData_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->clientActions_:Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->clearEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->clearServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->clearClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->cachedSize:I

    return-object p0
.end method

.method public clearClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->clientActions_:Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    return-object p0
.end method

.method public clearEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->eventData_:Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    return-object p0
.end method

.method public clearServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasServerData:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->serverData_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->cachedSize:I

    return v0
.end method

.method public getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->clientActions_:Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    return-object v0
.end method

.method public getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->eventData_:Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasServerData()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->cachedSize:I

    return v0
.end method

.method public getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->serverData_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    return-object v0
.end method

.method public hasClientActions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions:Z

    return v0
.end method

.method public hasEventData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData:Z

    return v0
.end method

.method public hasServerData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasServerData:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setEventData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setServerData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setClientActions(Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v0

    return-object v0
.end method

.method public setClientActions(Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->clientActions_:Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    return-object p0
.end method

.method public setEventData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->eventData_:Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    return-object p0
.end method

.method public setServerData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasServerData:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->serverData_:Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasServerData()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    return-void
.end method
