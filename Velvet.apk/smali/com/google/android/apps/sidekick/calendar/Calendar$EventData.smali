.class public final Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/Calendar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EventData"
.end annotation


# instance fields
.field private accessLevel_:I

.field private cachedSize:I

.field private endTimeSeconds_:J

.field private eventId_:J

.field private hasAccessLevel:Z

.field private hasEndTimeSeconds:Z

.field private hasEventId:Z

.field private hasNumberOfAttendees:Z

.field private hasPotentiallyGeocodableWhereField:Z

.field private hasProviderId:Z

.field private hasSelfAttendeeStatus:Z

.field private hasStartTimeSeconds:Z

.field private hasTitle:Z

.field private hasWhereField:Z

.field private numberOfAttendees_:I

.field private potentiallyGeocodableWhereField_:Z

.field private providerId_:J

.field private selfAttendeeStatus_:I

.field private startTimeSeconds_:J

.field private title_:Ljava/lang/String;

.field private whereField_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v2, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->providerId_:J

    iput-wide v2, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->eventId_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->title_:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->startTimeSeconds_:J

    iput-wide v2, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->endTimeSeconds_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->whereField_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->potentiallyGeocodableWhereField_:Z

    iput v1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->accessLevel_:I

    iput v1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->numberOfAttendees_:I

    iput v1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->selfAttendeeStatus_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAccessLevel()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->accessLevel_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->cachedSize:I

    return v0
.end method

.method public getEndTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->endTimeSeconds_:J

    return-wide v0
.end method

.method public getEventId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->eventId_:J

    return-wide v0
.end method

.method public getNumberOfAttendees()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->numberOfAttendees_:I

    return v0
.end method

.method public getPotentiallyGeocodableWhereField()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->potentiallyGeocodableWhereField_:Z

    return v0
.end method

.method public getProviderId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->providerId_:J

    return-wide v0
.end method

.method public getSelfAttendeeStatus()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->selfAttendeeStatus_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasProviderId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasEventId()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEventId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasTitle()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasStartTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasEndTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEndTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasWhereField()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasAccessLevel()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getAccessLevel()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasNumberOfAttendees()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getNumberOfAttendees()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasPotentiallyGeocodableWhereField()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getPotentiallyGeocodableWhereField()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasSelfAttendeeStatus()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getSelfAttendeeStatus()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->cachedSize:I

    return v0
.end method

.method public getStartTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->startTimeSeconds_:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getWhereField()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->whereField_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAccessLevel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasAccessLevel:Z

    return v0
.end method

.method public hasEndTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasEndTimeSeconds:Z

    return v0
.end method

.method public hasEventId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasEventId:Z

    return v0
.end method

.method public hasNumberOfAttendees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasNumberOfAttendees:Z

    return v0
.end method

.method public hasPotentiallyGeocodableWhereField()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasPotentiallyGeocodableWhereField:Z

    return v0
.end method

.method public hasProviderId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasProviderId:Z

    return v0
.end method

.method public hasSelfAttendeeStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasSelfAttendeeStatus:Z

    return v0
.end method

.method public hasStartTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasStartTimeSeconds:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasTitle:Z

    return v0
.end method

.method public hasWhereField()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasWhereField:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setProviderId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setEventId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setStartTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setEndTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setWhereField(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setAccessLevel(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readUInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setNumberOfAttendees(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setPotentiallyGeocodableWhereField(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setSelfAttendeeStatus(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    return-object v0
.end method

.method public setAccessLevel(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasAccessLevel:Z

    iput p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->accessLevel_:I

    return-object p0
.end method

.method public setEndTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasEndTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->endTimeSeconds_:J

    return-object p0
.end method

.method public setEventId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasEventId:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->eventId_:J

    return-object p0
.end method

.method public setNumberOfAttendees(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasNumberOfAttendees:Z

    iput p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->numberOfAttendees_:I

    return-object p0
.end method

.method public setPotentiallyGeocodableWhereField(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasPotentiallyGeocodableWhereField:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->potentiallyGeocodableWhereField_:Z

    return-object p0
.end method

.method public setProviderId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasProviderId:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->providerId_:J

    return-object p0
.end method

.method public setSelfAttendeeStatus(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasSelfAttendeeStatus:Z

    iput p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->selfAttendeeStatus_:I

    return-object p0
.end method

.method public setStartTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasStartTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->startTimeSeconds_:J

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasTitle:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public setWhereField(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasWhereField:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->whereField_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasProviderId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasEventId()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEventId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasStartTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasEndTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEndTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasWhereField()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasAccessLevel()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getAccessLevel()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasNumberOfAttendees()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getNumberOfAttendees()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeUInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasPotentiallyGeocodableWhereField()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getPotentiallyGeocodableWhereField()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasSelfAttendeeStatus()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getSelfAttendeeStatus()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    return-void
.end method
