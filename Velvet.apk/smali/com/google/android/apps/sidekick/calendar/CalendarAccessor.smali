.class Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;
.super Ljava/lang/Object;
.source "CalendarAccessor.java"


# static fields
.field private static final ATTENDEES_QUERY_PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static final UPCOMING_EVENTS_QUERY_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAppContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-class v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->TAG:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "event_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "end"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "accessLevel"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->UPCOMING_EVENTS_QUERY_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "attendeeName"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->ATTENDEES_QUERY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->mAppContext:Landroid/content/Context;

    return-void
.end method

.method private containsAttendees(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const-string v1, "\\s*,\\s*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->retainAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getAttendeeNames(J)Ljava/util/List;
    .locals 10
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    :try_start_0
    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->ATTENDEES_QUERY_PROJECTION:[Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "(event_id=%d)"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-static {v7}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    return-object v6

    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    throw v1
.end method

.method public static isEmptyOrWhitespace(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    invoke-virtual {v0, p0}, Lcom/google/common/base/CharMatcher;->matchesAllOf(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private upcomingEventsQuery(Landroid/content/ContentResolver;JI)Landroid/database/Cursor;
    .locals 7
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # J
    .param p4    # I

    const/4 v4, 0x0

    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-static {v6, p2, p3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    int-to-long v0, p4

    add-long/2addr v0, p2

    invoke-static {v6, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->UPCOMING_EVENTS_QUERY_PROJECTION:[Ljava/lang/String;

    const-string v3, "(visible=1) AND (calendar_access_level=700) AND (allDay=0) AND (selfAttendeeStatus!=2)"

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getUpcomingEvents(JI)Ljava/util/Collection;
    .locals 20
    .param p1    # J
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->mAppContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    :try_start_0
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, p3

    invoke-direct {v0, v6, v1, v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->upcomingEventsQuery(Landroid/content/ContentResolver;JI)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v15

    if-nez v15, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    invoke-static {v7}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    :goto_0
    return-object v8

    :cond_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v15

    invoke-static {v15}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v8

    :cond_2
    const/4 v15, 0x3

    invoke-interface {v7, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    const-wide/16 v17, 0x3e8

    div-long v11, v15, v17

    const/16 v15, 0x10

    shl-long v15, v11, v15

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    xor-long v9, v15, v17

    new-instance v15, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    invoke-direct {v15}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;-><init>()V

    invoke-virtual {v15, v9, v10}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setProviderId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v15

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setEventId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v15

    invoke-virtual {v15, v11, v12}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setStartTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v15

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    div-long v16, v16, v18

    invoke-virtual/range {v15 .. v17}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setEndTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v15

    const/16 v16, 0x5

    move/from16 v0, v16

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setAccessLevel(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v15

    const/16 v16, 0x6

    move/from16 v0, v16

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setSelfAttendeeStatus(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v5

    const/4 v15, 0x1

    invoke-interface {v7, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v15, 0x2

    invoke-interface {v7, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->isEmptyOrWhitespace(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_4

    invoke-virtual {v5, v13}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    :goto_1
    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEventId()J

    move-result-wide v15

    move-object/from16 v0, p0

    move-wide v1, v15

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->getAttendeeNames(J)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v15

    invoke-virtual {v5, v15}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setNumberOfAttendees(I)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    invoke-static {v14}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->isEmptyOrWhitespace(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_3

    invoke-virtual {v5, v14}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setWhereField(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v4}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->containsAttendees(Ljava/lang/String;Ljava/util/List;)Z

    move-result v15

    if-nez v15, :cond_5

    const/4 v15, 0x1

    :goto_2
    invoke-virtual {v5, v15}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setPotentiallyGeocodableWhereField(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    :cond_3
    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v15

    if-nez v15, :cond_2

    invoke-static {v7}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->mAppContext:Landroid/content/Context;

    const v16, 0x7f0d0165

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v15

    invoke-static {v7}, Lcom/android/common/io/MoreCloseables;->closeQuietly(Landroid/database/Cursor;)V

    throw v15

    :cond_5
    const/4 v15, 0x0

    goto :goto_2
.end method

.method public registerEventsObserver(Landroid/database/ContentObserver;)V
    .locals 3
    .param p1    # Landroid/database/ContentObserver;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public unregisterEventsObserver(Landroid/database/ContentObserver;)V
    .locals 2
    .param p1    # Landroid/database/ContentObserver;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method
