.class public Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;
.super Ljava/lang/Object;
.source "CalendarControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/calendar/CalendarController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sCalendarStarted:Z


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private mCalendarObserver:Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->sCalendarStarted:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method

.method private registerCalendarObserver()V
    .locals 4

    new-instance v1, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V

    iput-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mCalendarObserver:Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mCalendarObserver:Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->registerEventsObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method private unregisterCalendarObserver()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mCalendarObserver:Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mCalendarObserver:Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->unregisterEventsObserver(Landroid/database/ContentObserver;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mCalendarObserver:Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl$CalendarObserver;

    :cond_0
    return-void
.end method


# virtual methods
.method public newCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    .locals 3

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V

    return-object v0
.end method

.method public startCalendar(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    sget-boolean v0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->sCalendarStarted:Z

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->initialize()V

    invoke-static {p1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->registerForSignificantLocationChange(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->registerCalendarObserver()V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->sCalendarStarted:Z

    :cond_0
    return-void
.end method

.method public stopCalendar(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->stopUpdateAlarm(Landroid/content/Context;)V

    sget-boolean v0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->sCalendarStarted:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->unregisterForSignificantLocationChange(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->unregisterCalendarObserver()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/sidekick/calendar/CalendarControllerImpl;->sCalendarStarted:Z

    :cond_0
    return-void
.end method
