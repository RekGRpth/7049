.class public interface abstract Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
.super Ljava/lang/Object;
.source "CalendarDataProvider.java"


# virtual methods
.method public abstract clearAllEventNotifiedMarkers()V
.end method

.method public abstract clearData()V
.end method

.method public abstract getCalendarDataByServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
.end method

.method public abstract getCalendarDataForNotify()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCalendarDataForRefresh()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEarliestNotificationTimeSecs()Ljava/lang/Long;
.end method

.method public abstract getNotifyingCalendarData()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract initialize()V
.end method

.method public abstract markEventAsDismissed(J)Z
.end method

.method public abstract markEventAsNotified(J)Z
.end method

.method public abstract markEventNotificationAsDismissed(J)Z
.end method

.method public abstract updateWithNewEntryTreeFromServer(Lcom/google/geo/sidekick/Sidekick$EntryTree;)Z
.end method

.method public abstract updateWithNewEventData(Ljava/util/Collection;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;",
            ">;)Z"
        }
    .end annotation
.end method
