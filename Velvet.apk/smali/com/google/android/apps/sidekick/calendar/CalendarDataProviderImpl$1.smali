.class Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$1;
.super Landroid/os/AsyncTask;
.source "CalendarDataProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$1;->this$0:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$1;->this$0:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;

    # invokes: Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->readFromDisk()V
    invoke-static {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->access$000(Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;)V

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$1;->this$0:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;

    # getter for: Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->mInitializedLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;->access$100(Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    const/4 v0, 0x0

    return-object v0
.end method
