.class Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;
.super Ljava/lang/Object;
.source "CalendarDataProviderImpl.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NotDismissed"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;-><init>()V

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasClientActions()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsDismissed()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotDismissed;->apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    move-result v0

    return v0
.end method
