.class Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotifyingAt;
.super Ljava/lang/Object;
.source "CalendarDataProviderImpl.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NotifyingAt"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
        ">;"
    }
.end annotation


# instance fields
.field private final mQueryTimeSecs:J


# direct methods
.method constructor <init>(J)V
    .locals 0
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotifyingAt;->mQueryTimeSecs:J

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z
    .locals 4
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotificationPending;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotificationPending;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotificationPending;->apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getNotifyTimeSeconds()J

    move-result-wide v0

    const-wide/16 v2, 0x3c

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotifyingAt;->mQueryTimeSecs:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$NotifyingAt;->apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    move-result v0

    return v0
.end method
