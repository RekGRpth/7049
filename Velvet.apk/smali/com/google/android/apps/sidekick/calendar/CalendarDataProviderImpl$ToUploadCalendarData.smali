.class Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;
.super Ljava/lang/Object;
.source "CalendarDataProviderImpl.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ToUploadCalendarData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
        "Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;-><init>()V

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;->INSTANCE:Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;
    .locals 8
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v3

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasWhereField()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getPotentiallyGeocodableWhereField()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/geo/sidekick/Sidekick$Location;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v2, 0x1

    :cond_0
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLat()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLng()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLat()D

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Lcom/google/geo/sidekick/Sidekick$Location;->setLat(D)Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLng()D

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Lcom/google/geo/sidekick/Sidekick$Location;->setLng(D)Lcom/google/geo/sidekick/Sidekick$Location;

    const/4 v2, 0x1

    :cond_1
    new-instance v4, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;->setHash(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;->setStartTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEndTimeSeconds()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;->setEndTimeSeconds(J)Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getSelfAttendeeStatus()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;->setSelfAttendeeStatus(I)Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    if-eqz v2, :cond_2

    invoke-virtual {v4, v1}, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;->setLocation(Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    :cond_2
    return-object v4
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProviderImpl$ToUploadCalendarData;->apply(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    move-result-object v0

    return-object v0
.end method
