.class Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;
.super Ljava/lang/Object;
.source "CalendarIntentHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$1;,
        Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final sCheckNetworkBackoffTime:Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;


# instance fields
.field private final mAlarmUtils:Lcom/google/android/apps/sidekick/AlarmUtils;

.field private final mAppContext:Landroid/content/Context;

.field private final mCalendarAccessor:Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;

.field private final mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

.field private final mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;-><init>(Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$1;)V

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->sCheckNetworkBackoffTime:Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Lcom/google/android/apps/sidekick/inject/EntryItemFactory;Lcom/google/android/apps/sidekick/AlarmUtils;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p5    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p6    # Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
    .param p7    # Lcom/google/android/apps/sidekick/inject/EntryItemFactory;
    .param p8    # Lcom/google/android/apps/sidekick/AlarmUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarAccessor:Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    iput-object p7, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    iput-object p8, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAlarmUtils:Lcom/google/android/apps/sidekick/AlarmUtils;

    return-void
.end method

.method private buildNotificationRequestPayload(Ljava/lang/Iterable;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;",
            ">;)",
            "Lcom/google/geo/sidekick/Sidekick$RequestPayload;"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return-object v4

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->hasLocation()Z

    move-result v5

    if-nez v5, :cond_1

    sget-object v5, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->TAG:Ljava/lang/String;

    const-string v6, "All locations are stale; not sending request to server"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ClientUserData;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ClientUserData;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;

    invoke-virtual {v1, v0}, Lcom/google/geo/sidekick/Sidekick$ClientUserData;->addCalendarData(Lcom/google/geo/sidekick/Sidekick$UploadCalendarData;)Lcom/google/geo/sidekick/Sidekick$ClientUserData;

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    new-instance v5, Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    invoke-direct {v5}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;-><init>()V

    new-instance v6, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v6}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/google/geo/sidekick/Sidekick$Interest;->setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v6

    const/16 v7, 0xe

    invoke-virtual {v6, v7}, Lcom/google/geo/sidekick/Sidekick$Interest;->addEntryTypeRestrict(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;->addInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/geo/sidekick/Sidekick$EntryQuery;->setClientUserData(Lcom/google/geo/sidekick/Sidekick$ClientUserData;)Lcom/google/geo/sidekick/Sidekick$EntryQuery;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setEntryQuery(Lcom/google/geo/sidekick/Sidekick$EntryQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v4

    goto :goto_0
.end method

.method private executeNotificationRequest()Z
    .locals 14

    const/4 v2, 0x0

    iget-object v8, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->getCalendarDataForNotify()Ljava/lang/Iterable;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->buildNotificationRequestPayload(Ljava/lang/Iterable;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-interface {v8, v6}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasEntryResponse()Z

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    iget-object v8, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v8

    sget-object v10, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->sCheckNetworkBackoffTime:Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;

    invoke-virtual {v10}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;->getRetryAlarmOffsetMillis()J

    move-result-wide v10

    add-long/2addr v8, v10

    invoke-direct {p0, v8, v9}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->setCheckNotificationAlarm(J)V

    sget-object v8, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->TAG:Ljava/lang/String;

    const-string v9, "Failed to get response for notification query from server"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-object v8, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->sCheckNetworkBackoffTime:Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;

    invoke-virtual {v8}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler$BackoffTime;->clearBackoff()V

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getEntryResponse()Lcom/google/geo/sidekick/Sidekick$EntryResponse;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTreeCount()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v3, v2}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasRoot()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v3, v2}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getRoot()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v3, v2}, Lcom/google/geo/sidekick/Sidekick$EntryResponse;->getEntryTree(I)Lcom/google/geo/sidekick/Sidekick$EntryTree;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->hasExpirationTimestampSeconds()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTree;->getExpirationTimestampSeconds()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    iget-object v10, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v10}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v10

    const-wide/32 v12, 0x493e0

    add-long/2addr v10, v12

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->setCheckNotificationAlarm(J)V

    :cond_4
    iget-object v8, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    invoke-interface {v8, v4}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->updateWithNewEntryTreeFromServer(Lcom/google/geo/sidekick/Sidekick$EntryTree;)Z

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->setNextUserNotifyAlarm()V

    goto :goto_0
.end method

.method private presentPendingNotifications()V
    .locals 24

    new-instance v7, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getNextMeetingNotificationType()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {v4}, Lcom/google/android/apps/sidekick/LocationUtilities;->androidLocationToSidekickLocation(Landroid/location/Location;)Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v5

    :goto_0
    const/16 v16, 0x0

    const/4 v14, 0x0

    const/4 v11, 0x0

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->getNotifyingCalendarData()Ljava/lang/Iterable;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v15, v0, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEndTimeSeconds()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v20

    const-wide/16 v22, 0x3e8

    div-long v20, v20, v22

    cmp-long v18, v18, v20

    if-gez v18, :cond_2

    :cond_0
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-object/from16 v18, v0

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v19

    invoke-interface/range {v18 .. v20}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->markEventAsNotified(J)Z

    const/16 v16, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v12

    new-instance v18, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct/range {v18 .. v18}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    const/16 v19, 0xe

    invoke-virtual/range {v18 .. v19}, Lcom/google/geo/sidekick/Sidekick$Entry;->setType(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v18

    new-instance v19, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-direct/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->setHash(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/google/geo/sidekick/Sidekick$Entry;->setCalendarEntry(Lcom/google/geo/sidekick/Sidekick$CalendarEntry;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v18

    new-instance v19, Lcom/google/geo/sidekick/Sidekick$Notification;

    invoke-direct/range {v19 .. v19}, Lcom/google/geo/sidekick/Sidekick$Notification;-><init>()V

    const/16 v20, 0x2

    invoke-virtual/range {v19 .. v20}, Lcom/google/geo/sidekick/Sidekick$Notification;->setType(I)Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/google/geo/sidekick/Sidekick$Entry;->setNotification(Lcom/google/geo/sidekick/Sidekick$Notification;)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mEntryItemFactory:Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1, v5, v5}, Lcom/google/android/apps/sidekick/inject/EntryItemFactory;->create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntryNotification()Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    move-result-object v10

    :goto_3
    if-eqz v10, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v12}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->createDismissPendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v10, v1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->createNotification(Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v14

    if-nez v14, :cond_4

    sget-object v18, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->TAG:Ljava/lang/String;

    const-string v19, "createNotification surprisingly returned null"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_3
    const/4 v10, 0x0

    goto :goto_3

    :cond_4
    invoke-interface {v10}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v11

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEndTimeSeconds()J

    move-result-wide v8

    goto/16 :goto_2

    :cond_5
    if-eqz v14, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v14, v11}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->showNotification(Landroid/app/Notification;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    const-wide/16 v18, 0x3e8

    mul-long v18, v18, v8

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->setClearNotificationAlarm(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->setLastNotificationTime()V

    :cond_6
    if-eqz v16, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->setNextUserNotifyAlarm()V

    :cond_7
    return-void
.end method

.method private setCheckNotificationAlarm(J)V
    .locals 3
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->CHECK_NOTIFICATIONS_ACTION:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v0, v1, v2, p1, p2}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->setAlarmForAction(Landroid/content/Context;Ljava/lang/String;IJ)V

    return-void
.end method

.method private setClearNotificationAlarm(J)V
    .locals 3
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_EXPIRE_ACTION:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, p2}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->setAlarmForAction(Landroid/content/Context;Ljava/lang/String;IJ)V

    return-void
.end method

.method private setNextUserNotifyAlarm()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->getEarliestNotificationTimeSecs()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    :goto_0
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->setUserNotifyAlarm(J)V

    return-void

    :cond_0
    const-wide/16 v1, 0x0

    goto :goto_0
.end method

.method private setUserNotifyAlarm(J)V
    .locals 3
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_ACTION:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, p2}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->setAlarmForAction(Landroid/content/Context;Ljava/lang/String;IJ)V

    return-void
.end method

.method private updateCalendarDataProvider()Z
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarAccessor:Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    const v5, 0x5265c00

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarAccessor;->getUpcomingEvents(JI)Ljava/util/Collection;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    invoke-interface {v2, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->updateWithNewEventData(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return v0
.end method


# virtual methods
.method handle(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_ACTION:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->presentPendingNotifications()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->NOTIFICATION_DISMISS_ACTION:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getHashFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    invoke-interface {v0, v9}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->getCalendarDataByServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasProviderId()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->markEventNotificationAsDismissed(J)Z

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_EXPIRE_ACTION:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mNowNotificationManager:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->CALENDAR_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->INITIALIZE_ACTION:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->UPDATE_CALENDAR_ACTION:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->hasPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v8, 0x1

    :goto_1
    if-eqz v8, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mCalendarDataProvider:Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->clearAllEventNotifiedMarkers()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->setNextUserNotifyAlarm()V

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->updateCalendarDataProvider()Z

    move-result v6

    const/4 v10, 0x0

    if-nez v6, :cond_5

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->CHECK_NOTIFICATIONS_ACTION:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->executeNotificationRequest()Z

    move-result v10

    :cond_6
    if-eqz v8, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAlarmUtils:Lcom/google/android/apps/sidekick/AlarmUtils;

    const-wide/32 v1, 0xdbba0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->mAppContext:Landroid/content/Context;

    sget-object v4, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->UPDATE_CALENDAR_ACTION:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    const-string v4, "Calendar_alarm_UPDATE_CALENDAR_ACTION"

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/sidekick/AlarmUtils;->scheduleRepeatingAlarm(JLandroid/app/PendingIntent;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_7
    move v8, v5

    goto :goto_1
.end method
