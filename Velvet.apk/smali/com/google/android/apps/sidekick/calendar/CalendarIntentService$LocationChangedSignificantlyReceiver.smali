.class Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$LocationChangedSignificantlyReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CalendarIntentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocationChangedSignificantlyReceiver"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$1;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$LocationChangedSignificantlyReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.apps.sidekick.LOCATION_CHANGED_SIGNIFICANTLY"

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->CHECK_NOTIFICATIONS_ACTION:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->sendIntentWithAction(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
