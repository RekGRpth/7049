.class public Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;
.super Landroid/app/IntentService;
.source "CalendarIntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$1;,
        Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$LocationChangedSignificantlyReceiver;,
        Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$CalendarReceiver;
    }
.end annotation


# static fields
.field static final CHECK_NOTIFICATIONS_ACTION:Ljava/lang/String;

.field static final INITIALIZE_ACTION:Ljava/lang/String;

.field static final NOTIFICATION_DISMISS_ACTION:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field static final UPDATE_CALENDAR_ACTION:Ljava/lang/String;

.field static final USER_NOTIFY_ACTION:Ljava/lang/String;

.field static final USER_NOTIFY_EXPIRE_ACTION:Ljava/lang/String;

.field private static final VALID_ACTION_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLocationChangedReceiver:Landroid/content/BroadcastReceiver;


# instance fields
.field private mHandler:Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;

.field private mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-class v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".INIT_CALENDAR_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->INITIALIZE_ACTION:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".UPDATE_CALENDAR_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->UPDATE_CALENDAR_ACTION:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CHECK_NOTIFICATIONS_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->CHECK_NOTIFICATIONS_ACTION:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".USER_NOTIFY_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_ACTION:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".USER_NOTIFY_EXPIRE_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_EXPIRE_ACTION:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".NOTIFICATION_DISMISS_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->NOTIFICATION_DISMISS_ACTION:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->INITIALIZE_ACTION:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->UPDATE_CALENDAR_ACTION:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->CHECK_NOTIFICATIONS_ACTION:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_ACTION:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_EXPIRE_ACTION:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->NOTIFICATION_DISMISS_ACTION:Ljava/lang/String;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->VALID_ACTION_SET:Ljava/util/Set;

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$LocationChangedSignificantlyReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$LocationChangedSignificantlyReceiver;-><init>(Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$1;)V

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->sLocationChangedReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "CalendarIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->setIntentRedelivery(Z)V

    return-void
.end method

.method static createDismissPendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 5

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "calendar_notification"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->NOTIFICATION_DISMISS_ACTION:Ljava/lang/String;

    const-class v4, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-direct {v2, v3, v1, v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x0

    const/high16 v3, 0x40000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method static getHashFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method static getPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$CalendarReceiver;

    invoke-direct {v1, p1, v2, v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method static hasPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const/4 v3, 0x0

    const-class v4, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService$CalendarReceiver;

    invoke-direct {v2, p1, v3, v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x20000000

    invoke-static {v1, v0, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static registerForSignificantLocationChange(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V
    .locals 3
    .param p0    # Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.android.apps.sidekick.LOCATION_CHANGED_SIGNIFICANTLY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocalBroadcastManager()Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->sLocationChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method static sendIntentWithAction(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->VALID_ACTION_SET:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendIntentWithAction ignoring call with unexpected action: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;

    invoke-direct {v1, p1, v2, v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method static setAlarmForAction(Landroid/content/Context;Ljava/lang/String;IJ)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # J

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getAlarmManager()Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-gtz v2, :cond_0

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0, p2, p3, p4, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static startUpdateAlarm(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->INITIALIZE_ACTION:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->sendIntentWithAction(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static stopUpdateAlarm(Landroid/content/Context;)V
    .locals 6
    .param p0    # Landroid/content/Context;

    sget-object v5, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->UPDATE_CALENDAR_ACTION:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->CHECK_NOTIFICATIONS_ACTION:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    sget-object v5, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_ACTION:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->USER_NOTIFY_EXPIRE_ACTION:Ljava/lang/String;

    invoke-static {p0, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getPendingIntentWithAction(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getAlarmManager()Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    invoke-interface {v0, v4}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    invoke-interface {v0, v3}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    invoke-virtual {v2}, Landroid/app/PendingIntent;->cancel()V

    invoke-virtual {v1}, Landroid/app/PendingIntent;->cancel()V

    invoke-virtual {v4}, Landroid/app/PendingIntent;->cancel()V

    invoke-virtual {v3}, Landroid/app/PendingIntent;->cancel()V

    return-void
.end method

.method static unregisterForSignificantLocationChange(Lcom/google/android/apps/sidekick/inject/SidekickInjector;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {p0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocalBroadcastManager()Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->sLocationChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 10

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->maybeRegisterSidekickAlarms()V

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNowNotificationManager()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v7}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mSidekickInjector:Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    invoke-interface {v8}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getAlarmUtils()Lcom/google/android/apps/sidekick/AlarmUtils;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;-><init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;Lcom/google/android/apps/sidekick/inject/EntryItemFactory;Lcom/google/android/apps/sidekick/AlarmUtils;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mHandler:Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    sget-object v3, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->TAG:Ljava/lang/String;

    const-string v4, "onHandleIntent: received unexpected null or empty Intent"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->VALID_ACTION_SET:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onHandleIntent: received Intent with unexpect action: \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v3, "power"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/MarinerOptInSettings;->stopServicesIfUserOptedOut()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarIntentService;->mHandler:Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarIntentHandler;->handle(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v3
.end method
