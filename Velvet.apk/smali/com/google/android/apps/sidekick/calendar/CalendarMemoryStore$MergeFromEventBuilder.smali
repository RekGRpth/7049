.class Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;
.super Ljava/lang/Object;
.source "CalendarMemoryStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MergeFromEventBuilder"
.end annotation


# instance fields
.field private final mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

.field private mChanged:Z

.field private final mNewIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mChanged:Z

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mNewIds:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
    .param p2    # Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;-><init>(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;)V

    return-void
.end method

.method private serverDataHasGeocode(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLat()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->hasLng()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private serverDataHasTravelData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v0

    return-object v0
.end method

.method update(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Z
    .locals 12
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    const/4 v6, 0x0

    const/4 v7, 0x1

    # invokes: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Z
    invoke-static {p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$500(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mNewIds:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->calculateServerHash(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->getByProviderId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    new-instance v8, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-direct {v8}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;-><init>()V

    invoke-virtual {v8, p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setEventData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v8

    new-instance v9, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-direct {v9}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;-><init>()V

    invoke-virtual {v9, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setServerData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->add(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    iput-boolean v7, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mChanged:Z

    :goto_1
    move v6, v7

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->add(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    goto :goto_1

    :cond_3
    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;-><init>()V

    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setEventData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    new-instance v5, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-direct {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;-><init>()V

    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    invoke-virtual {v5, v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->serverDataHasGeocode(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->serverDataHasTravelData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_4
    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearIsGeocodable()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearTravelTimeMinutes()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearNotifyTimeSeconds()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setDataClearedBecauseEventChanged(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    :cond_5
    :goto_4
    invoke-virtual {v1, v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setServerData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    iget-object v6, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-virtual {v6, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->add(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    iput-boolean v7, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mChanged:Z

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->clear()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clear()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_3

    :cond_6
    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v8

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v10

    cmp-long v6, v8, v10

    if-eqz v6, :cond_5

    invoke-direct {p0, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->serverDataHasTravelData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearTravelTimeMinutes()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->clearNotifyTimeSeconds()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setDataClearedBecauseEventChanged(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    goto :goto_4
.end method

.method wasChanged()Z
    .locals 2

    iget-boolean v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mChanged:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    # invokes: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->getProviderIdSet()Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$600(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mNewIds:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mChanged:Z

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;->mChanged:Z

    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
