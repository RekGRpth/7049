.class Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;
.super Ljava/lang/Object;
.source "CalendarMemoryStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MergeFromServerBuilder"
.end annotation


# instance fields
.field private final changedIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

.field private final mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->changedIds:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
    .param p2    # Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;-><init>(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;)V

    return-void
.end method

.method private static doubleE7Eq(DD)Z
    .locals 4
    .param p0    # D
    .param p2    # D

    sub-double v0, p0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x416312d000000000L

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static latLngE7Eq(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;)Z
    .locals 4
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLat()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLat()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->doubleE7Eq(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLng()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLng()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->doubleE7Eq(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private serverDataEquivalent(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    .locals 5
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .param p2    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getDataClearedBecauseEventChanged()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getDataClearedBecauseEventChanged()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable()Z

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasIsGeocodable()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->latLngE7Eq(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes()Z

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getTravelTimeMinutes()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getTravelTimeMinutes()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds()Z

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getNotifyTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p2}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getNotifyTimeSeconds()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->wasChanged()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    :goto_0
    return-object v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    # invokes: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->getProviderIdSet()Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$600(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;)Ljava/util/Set;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->changedIds:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->getByProviderId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->add(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->build()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    move-result-object v3

    goto :goto_0
.end method

.method update(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    .locals 7
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    const/4 v4, 0x0

    # invokes: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    invoke-static {p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$700(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasDataClearedBecauseEventChanged()Z

    move-result v5

    if-eqz v5, :cond_1

    # getter for: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Incoming ServerData has dataClearedBecauseEventChanged; unexpected!"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mOldMemoryStore:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->getByServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds()Z

    move-result v4

    if-eqz v4, :cond_5

    # invokes: Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->latLngEqual(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    invoke-static {p1, v3}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->access$800(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes()Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;-><init>()V

    invoke-static {p1, v0}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getTravelTimeMinutes()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setTravelTimeMinutes(I)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasNotifyTimeSeconds()Z

    move-result v4

    if-nez v4, :cond_4

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;-><init>()V

    invoke-static {p1, v0}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    :cond_3
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getNotifyTimeSeconds()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->setNotifyTimeSeconds(J)Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    :cond_4
    if-eqz v0, :cond_5

    move-object p1, v0

    :cond_5
    invoke-direct {p0, v3, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->serverDataEquivalent(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v1, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-direct {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;-><init>()V

    invoke-static {v2, v1}, Lcom/google/android/speech/utils/ProtoBufUtils;->copyOf(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setServerData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->mBuilder:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;->add(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    iget-object v4, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->changedIds:Ljava/util/Set;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_6
    const/4 v4, 0x1

    goto/16 :goto_0
.end method

.method wasChanged()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;->changedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
