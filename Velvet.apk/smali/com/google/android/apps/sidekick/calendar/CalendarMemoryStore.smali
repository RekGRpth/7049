.class Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
.super Ljava/lang/Object;
.source "CalendarMemoryStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;,
        Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;,
        Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;,
        Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$Builder;
    }
.end annotation


# static fields
.field static final EMPTY:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mDataMapByHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;"
        }
    .end annotation
.end field

.field private final mDataMapByProviderId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->of()Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->of()Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->EMPTY:Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    const-class v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/util/Map;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByProviderId:Ljava/util/Map;

    invoke-static {p2}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByHash:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByProviderId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByHash:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;

    const-string v1, "CalendarMemoryStore maps have inconsistent sizes."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;)V
    .locals 0
    .param p1    # Ljava/util/Map;
    .param p2    # Ljava/util/Map;
    .param p3    # Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-static {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    invoke-static {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->getProviderIdSet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-static {p0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-static {p0, p1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->latLngEqual(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v0

    return v0
.end method

.method static calculateServerHash(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Ljava/lang/String;
    .locals 12
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    :try_start_0
    const-string v0, "SHA1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getProviderId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEventId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEndTimeSeconds()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getWhereField()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getNumberOfAttendees()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getSelfAttendeeStatus()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v10

    const-string v0, "|"

    invoke-static {v0, v10}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v8}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v9

    sget-object v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not find \'SHA1\' message digest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getProviderIdSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByProviderId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasEventData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->hasServerData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->hasProviderId()Z

    move-result v0

    return v0
.end method

.method private static isValidData(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasServerHash()Z

    move-result v0

    return v0
.end method

.method private static latLngEqual(Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;)Z
    .locals 7
    .param p0    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;
    .param p1    # Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getIsGeocodable()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasGeocodedLatLng()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getGeocodedLatLng()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLat()D

    move-result-wide v3

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLat()D

    move-result-wide v5

    cmpl-double v3, v3, v5

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLng()D

    move-result-wide v3

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData$LatLng;->getLng()D

    move-result-wide v5

    cmpl-double v3, v3, v5

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method


# virtual methods
.method getByProviderId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByProviderId:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    return-object v0
.end method

.method getByServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByHash:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    return-object v0
.end method

.method mergeFromEventBuilder()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromEventBuilder;-><init>(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;)V

    return-object v0
.end method

.method mergeFromServerBuilder()Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$MergeFromServerBuilder;-><init>(Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore$1;)V

    return-object v0
.end method

.method setClientAction(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;
    .locals 10
    .param p1    # J
    .param p3    # Ljava/lang/Boolean;
    .param p4    # Ljava/lang/Boolean;
    .param p5    # Ljava/lang/Boolean;

    const/4 v7, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->getByProviderId(J)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getClientActions()Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    move-result-object v3

    if-eqz v3, :cond_4

    if-eqz p4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsDismissed()Z

    move-result v8

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-ne v8, v9, :cond_4

    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotified()Z

    move-result v8

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-ne v8, v9, :cond_4

    :cond_3
    if-eqz p5, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->getIsNotificationDismissed()Z

    move-result v8

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eq v8, v9, :cond_0

    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByProviderId:Ljava/util/Map;

    invoke-static {v7}, Lcom/google/common/collect/Maps;->newHashMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByHash:Ljava/util/Map;

    invoke-static {v7}, Lcom/google/common/collect/Maps;->newHashMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;-><init>()V

    if-eqz v3, :cond_5

    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->toByteArray()[B

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_1
    if-eqz p4, :cond_6

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->setIsDismissed(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    :cond_6
    if-eqz p3, :cond_7

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->setIsNotified(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    :cond_7
    if-eqz p5, :cond_8

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;->setIsNotificationDismissed(Z)Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;

    :cond_8
    new-instance v6, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-direct {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;-><init>()V

    :try_start_1
    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->toByteArray()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    invoke-virtual {v6, v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->setClientActions(Lcom/google/android/apps/sidekick/calendar/Calendar$ClientActions;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v2, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getServerHash()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;

    invoke-direct {v7, v2, v1}, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_0

    :catch_0
    move-exception v5

    sget-object v7, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;

    const-string v8, "Error in merging proto buffer"

    invoke-static {v7, v8, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    move-exception v5

    sget-object v7, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->TAG:Ljava/lang/String;

    const-string v8, "Error in merging proto buffer"

    invoke-static {v7, v8, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByProviderId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/calendar/CalendarMemoryStore;->mDataMapByProviderId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
