.class public interface abstract Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
.super Ljava/lang/Object;
.source "BackOfCardAdapter.java"


# virtual methods
.method public abstract commitFeedback(Landroid/content/Context;)V
.end method

.method public abstract dismissCardAfterCommit()Z
.end method

.method public abstract getChildEntriesToDismissAfterCommit()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEntryAdapter()Lcom/google/android/apps/sidekick/EntryItemAdapter;
.end method

.method public abstract populateBackOfCard(Landroid/app/Activity;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Landroid/view/View;)V
.end method

.method public abstract restoreViewState(Landroid/os/Bundle;)V
.end method

.method public abstract saveViewState()Landroid/os/Bundle;
.end method
