.class public interface abstract Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;
.super Ljava/lang/Object;
.source "BackOfCardQuestionListAdapter.java"


# virtual methods
.method public abstract getChildEntriesToDismiss([Ljava/lang/Boolean;)Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;
.end method

.method public abstract getNoActions(I)Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNoRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
.end method

.method public abstract getQuestionCount(Landroid/view/View;)I
.end method

.method public abstract getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;
.end method

.method public abstract getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;
.end method

.method public abstract getViewEnablement(Landroid/view/View;[Ljava/lang/Boolean;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "[",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getYesActions(I)Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getYesRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
.end method

.method public abstract shouldDismissCard([Ljava/lang/Boolean;)Z
.end method
