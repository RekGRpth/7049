.class public Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;
.super Ljava/lang/Object;
.source "BaseBackOfCardAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActionAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;

.field private mCardView:Landroid/view/View;

.field private final mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

.field private mFeedbackButtons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/apps/sidekick/FeedbackInterestButton;",
            "Lcom/google/android/apps/sidekick/FeedbackInterestButton;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p2    # Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;-><init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p2    # Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;
    .param p3    # Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mActionAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->updateCardEnablement()V

    return-void
.end method

.method private getFeedbackStates()[Ljava/lang/Boolean;
    .locals 4

    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v1, v3, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    aput-object v3, v1, v2

    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    aput-object v3, v1, v2

    goto :goto_1
.end method

.method private setViewEnabled(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-nez p2, :cond_0

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    :goto_0
    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, v3}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->setViewEnabledInternal(Landroid/view/View;ZZ)V

    return-void

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private setViewEnabledInternal(Landroid/view/View;ZZ)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Z
    .param p3    # Z

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    instance-of v2, p1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, p2, v3}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->setViewEnabledInternal(Landroid/view/View;ZZ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-nez p3, :cond_1

    if-eqz p2, :cond_2

    const/high16 v2, 0x3f800000

    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    return-void

    :cond_2
    const v2, 0x3f333333

    goto :goto_1
.end method

.method private updateCardEnablement()V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->getFeedbackStates()[Ljava/lang/Boolean;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mCardView:Landroid/view/View;

    invoke-interface {v4, v5, v0}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getViewEnablement(Landroid/view/View;[Ljava/lang/Boolean;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->setViewEnabled(Landroid/view/View;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private yesNoCheckedListener(Landroid/content/Context;ILcom/google/android/apps/sidekick/FeedbackInterestButton;Z)Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/sidekick/FeedbackInterestButton;
    .param p4    # Z

    new-instance v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;

    invoke-direct {v0, p0, p3, p4}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter$1;-><init>(Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;Lcom/google/android/apps/sidekick/FeedbackInterestButton;Z)V

    return-object v0
.end method


# virtual methods
.method protected addYesNoFeedbackQuestion(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/util/Pair;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/LayoutInflater;",
            "Landroid/view/ViewGroup;",
            "II)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/apps/sidekick/FeedbackInterestButton;",
            "Lcom/google/android/apps/sidekick/FeedbackInterestButton;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    const v4, 0x7f040035

    invoke-virtual {p2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p3, v2, p4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    invoke-interface {v4, p1, p5}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    const v4, 0x7f100059

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f1000b4

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    const v4, 0x7f1000b5

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    const/4 v4, 0x1

    invoke-direct {p0, p1, p5, v1, v4}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->yesNoCheckedListener(Landroid/content/Context;ILcom/google/android/apps/sidekick/FeedbackInterestButton;Z)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-direct {p0, p1, p5, v3, v5}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->yesNoCheckedListener(Landroid/content/Context;ILcom/google/android/apps/sidekick/FeedbackInterestButton;Z)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, v3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4
.end method

.method public commitFeedback(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->getFeedbackStates()[Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x0

    :goto_0
    array-length v7, v3

    if-ge v4, v7, :cond_0

    aget-object v1, v3, v4

    if-nez v1, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v1, v7, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    invoke-interface {v7, v4}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getYesActions(I)Ljava/util/Collection;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    invoke-interface {v7, p1, v4}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getYesRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;

    move-result-object v6

    :cond_4
    :goto_2
    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    invoke-interface {v7, v4}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v5

    new-instance v7, Lcom/google/android/apps/sidekick/actions/RecordActionTask;

    invoke-direct {v7, v5, p1, v2, v0}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;-><init>(Lcom/google/android/apps/sidekick/inject/NetworkClient;Landroid/content/Context;Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;)V

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Void;

    invoke-virtual {v7, v8}, Lcom/google/android/apps/sidekick/actions/RecordActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_5
    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    :cond_6
    sget-object v7, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne v1, v7, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    invoke-interface {v7, v4}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getNoActions(I)Ljava/util/Collection;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    invoke-interface {v7, p1, v4}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getNoRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;

    move-result-object v6

    goto :goto_2
.end method

.method public dismissCardAfterCommit()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->getFeedbackStates()[Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    invoke-interface {v1, v0}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->shouldDismissCard([Ljava/lang/Boolean;)Z

    move-result v1

    goto :goto_0
.end method

.method public getChildEntriesToDismissAfterCommit()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->getFeedbackStates()[Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    invoke-interface {v1, v0}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getChildEntriesToDismiss([Ljava/lang/Boolean;)Ljava/util/Collection;

    move-result-object v1

    goto :goto_0
.end method

.method public getEntryAdapter()Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    return-object v0
.end method

.method public populateBackOfCard(Landroid/app/Activity;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 17
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/LayoutInflater;
    .param p4    # Landroid/view/View;

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mCardView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getJustification(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const v3, 0x7f10004e

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mCardView:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-interface {v3, v0, v4}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;

    move-result-object v15

    const v3, 0x7f10004f

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mCardView:Landroid/view/View;

    invoke-interface {v3, v4}, Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;->getQuestionCount(Landroid/view/View;)I

    move-result v12

    invoke-static {v12}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v7

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v12, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    move-object/from16 v16, v0

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p2

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->addYesNoFeedbackQuestion(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;II)Landroid/util/Pair;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mActionAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mActionAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-interface {v3, v0, v1, v2}, Lcom/google/android/apps/sidekick/feedback/BackOfCardActionAdapter;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mQuestionListAdapter:Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;

    if-eqz v3, :cond_3

    instance-of v3, v9, Landroid/widget/LinearLayout;

    if-eqz v3, :cond_3

    move-object v3, v9

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getShowDividers()I

    move-result v14

    move-object v3, v9

    check-cast v3, Landroid/widget/LinearLayout;

    or-int/lit8 v4, v14, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_4
    return-void
.end method

.method public restoreViewState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "yes_button_states"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v3

    const-string v4, "no_button_states"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v2

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    array-length v4, v3

    array-length v5, v2

    if-ne v4, v5, :cond_2

    array-length v4, v3

    iget-object v5, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_3

    :cond_2
    sget-object v4, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->TAG:Ljava/lang/String;

    const-string v5, "mis-matched view state for back of card, skipping restore"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_1
    array-length v4, v3

    if-ge v1, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    aget-boolean v5, v3, v1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->setChecked(Z)V

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    aget-boolean v5, v2, v1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->setChecked(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public saveViewState()Landroid/os/Bundle;
    .locals 6

    iget-object v5, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_1
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v4, v5, [Z

    iget-object v5, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v2, v5, [Z

    const/4 v1, 0x0

    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/sidekick/feedback/BaseBackOfCardAdapter;->mFeedbackButtons:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->isChecked()Z

    move-result v5

    aput-boolean v5, v4, v1

    iget-object v5, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/apps/sidekick/FeedbackInterestButton;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/FeedbackInterestButton;->isChecked()Z

    move-result v5

    aput-boolean v5, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const-string v5, "yes_button_states"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    const-string v5, "no_button_states"

    invoke-virtual {v3, v5, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    goto :goto_0
.end method
