.class public abstract Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;
.super Ljava/lang/Object;
.source "GroupNodeQuestionListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;


# instance fields
.field private final mNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->mNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    return-void
.end method


# virtual methods
.method public getChildEntriesToDismiss([Ljava/lang/Boolean;)Ljava/util/Collection;
    .locals 4
    .param p1    # [Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    array-length v2, p1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->mNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v3

    if-ne v2, v3, :cond_1

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    aget-object v2, p1, v1

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->mNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v2, v1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->of()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->mNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v0, p1}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    return-object v0
.end method

.method public getNoActions(I)Ljava/util/Collection;
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const/16 v4, 0x1d

    invoke-static {v2, v4}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v4}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v1

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    if-eqz v3, :cond_0

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v4

    return-object v4
.end method

.method public getNoRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getQuestionCount(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->mNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v0

    return v0
.end method

.method public getViewEnablement(Landroid/view/View;[Ljava/lang/Boolean;)Ljava/util/Map;
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # [Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "[",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    array-length v3, p2

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->mNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v4

    if-ne v3, v4, :cond_1

    const v3, 0x7f100016

    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    array-length v4, p2

    if-ne v3, v4, :cond_1

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    array-length v3, p2

    if-ge v1, v3, :cond_2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aget-object v3, p2, v1

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-eq v3, v5, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->of()Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    :cond_2
    return-object v2
.end method

.method public getYesActions(I)Ljava/util/Collection;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    const/16 v2, 0x1c

    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->of()Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    goto :goto_0
.end method

.method public getYesRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public shouldDismissCard([Ljava/lang/Boolean;)Z
    .locals 4
    .param p1    # [Ljava/lang/Boolean;

    array-length v2, p1

    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/GroupNodeQuestionListAdapter;->mNode:Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;->getEntryCount()I

    move-result v3

    if-ne v2, v3, :cond_1

    const/4 v0, 0x1

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    aget-object v2, p1, v1

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-eq v2, v3, :cond_0

    const/4 v0, 0x0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    return v0
.end method
