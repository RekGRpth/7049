.class public abstract Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;
.super Ljava/lang/Object;
.source "IndexedQuestionListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;


# instance fields
.field private final mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-void
.end method


# virtual methods
.method protected createIndexedFeedbackAction(Lcom/google/geo/sidekick/Sidekick$Action;I)Lcom/google/geo/sidekick/Sidekick$Action;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Action;
    .param p2    # I

    :try_start_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Action;->toByteArray()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/geo/sidekick/Sidekick$Action;->parseFrom([B)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/geo/sidekick/Sidekick$Action;->setInterestedItemIndex(I)Lcom/google/geo/sidekick/Sidekick$Action;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChildEntriesToDismiss([Ljava/lang/Boolean;)Ljava/util/Collection;
    .locals 1
    .param p1    # [Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method

.method public getNoActions(I)Ljava/util/Collection;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const/16 v2, 0x1d

    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;->createIndexedFeedbackAction(Lcom/google/geo/sidekick/Sidekick$Action;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->of()Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    goto :goto_0
.end method

.method public getNoRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewEnablement(Landroid/view/View;[Ljava/lang/Boolean;)Ljava/util/Map;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # [Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "[",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;->shouldDismissCard([Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYesActions(I)Ljava/util/Collection;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const/16 v2, 0x1c

    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/sidekick/feedback/IndexedQuestionListAdapter;->createIndexedFeedbackAction(Lcom/google/geo/sidekick/Sidekick$Action;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->of()Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    goto :goto_0
.end method

.method public getYesRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public shouldDismissCard([Ljava/lang/Boolean;)Z
    .locals 3
    .param p1    # [Ljava/lang/Boolean;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-object v1, p1, v0

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
