.class Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;
.super Ljava/lang/Object;
.source "SettingsActionAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->getView(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->access$000(Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;->val$activity:Landroid/app/Activity;

    invoke-interface {v1, v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->access$100(Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;)Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;->val$activity:Landroid/app/Activity;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeStartActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;
    invoke-static {v1}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->access$000(Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "CARD_BACK_BUTTON_PRESS"

    iget-object v3, p0, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter$1;->this$0:Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;

    # getter for: Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->mEntryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;
    invoke-static {v3}, Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;->access$000(Lcom/google/android/apps/sidekick/feedback/SettingsActionAdapter;)Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v3

    const-string v4, "SETTINGS"

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method
