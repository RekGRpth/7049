.class public Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;
.super Ljava/lang/Object;
.source "SingleItemQuestionListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/feedback/BackOfCardQuestionListAdapter;


# instance fields
.field private final mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private final mQuestionArgs:[Ljava/lang/CharSequence;

.field private final mQuestionResourceId:I


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;I)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # I

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v2, v0, v1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;I[Ljava/lang/CharSequence;)V

    return-void
.end method

.method public varargs constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;I[Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # I
    .param p3    # [Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput p2, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mQuestionResourceId:I

    iput-object p3, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mQuestionArgs:[Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public getChildEntriesToDismiss([Ljava/lang/Boolean;)Ljava/util/Collection;
    .locals 1
    .param p1    # [Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->of()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public getEntry(I)Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method

.method public getNoActions(I)Ljava/util/Collection;
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const/16 v5, 0x1d

    invoke-static {v4, v5}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const/16 v5, 0x13

    invoke-static {v4, v5}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v2

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    if-eqz v3, :cond_0

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v2, :cond_2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v4

    return-object v4
.end method

.method public getNoRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getQuestionCount(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    return v0
.end method

.method public getQuestionLabel(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    iget v1, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mQuestionResourceId:I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mQuestionArgs:[Ljava/lang/CharSequence;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTopLevelQuestion(Landroid/content/Context;Landroid/view/View;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/View;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewEnablement(Landroid/view/View;[Ljava/lang/Boolean;)Ljava/util/Map;
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # [Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "[",
            "Ljava/lang/Boolean;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->shouldDismissCard([Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYesActions(I)Ljava/util/Collection;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Action;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/feedback/SingleItemQuestionListAdapter;->mEntry:Lcom/google/geo/sidekick/Sidekick$Entry;

    const/16 v2, 0x1c

    invoke-static {v1, v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->findAction(Lcom/google/geo/sidekick/Sidekick$Entry;I)Lcom/google/geo/sidekick/Sidekick$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->of()Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    goto :goto_0
.end method

.method public getYesRunnable(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public shouldDismissCard([Ljava/lang/Boolean;)Z
    .locals 4
    .param p1    # [Ljava/lang/Boolean;

    const/4 v0, 0x1

    const/4 v1, 0x0

    array-length v2, p1

    if-ne v2, v0, :cond_0

    aget-object v2, p1, v1

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
