.class public Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;
.super Ljava/lang/Object;
.source "ActivityHelperImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/ActivityHelper;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mUiThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/google/UserInteractionLogger;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
    .param p2    # Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    return-void
.end method

.method private showToastOnUiThread(Landroid/content/Context;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl$1;-><init>(Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;Landroid/content/Context;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    return-object v0
.end method

.method public safeStartActivity(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const v0, 0x7f0d039c

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->safeStartActivityWithMessage(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    return v0
.end method

.method public safeStartActivityWithMessage(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;
    .param p3    # I

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no handler for intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->showToastOnUiThread(Landroid/content/Context;I)V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public safeViewUri(Landroid/content/Context;Landroid/net/Uri;Z)Z
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # Z

    const v0, 0x7f0d0370

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->safeViewUriWithMessage(Landroid/content/Context;Landroid/net/Uri;ZI)Z

    move-result v0

    return v0
.end method

.method public safeViewUriWithMessage(Landroid/content/Context;Landroid/net/Uri;ZI)Z
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # Z
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    if-eqz p3, :cond_0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, p1, v0, p4}, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->safeStartActivityWithMessage(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v1

    return v1
.end method

.method public safeViewUrl(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lcom/google/android/apps/sidekick/inject/ActivityHelperImpl;->safeViewUri(Landroid/content/Context;Landroid/net/Uri;Z)Z

    move-result v0

    return v0
.end method
