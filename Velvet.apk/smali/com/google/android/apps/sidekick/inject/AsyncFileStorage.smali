.class public interface abstract Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;
.super Ljava/lang/Object;
.source "AsyncFileStorage.java"


# virtual methods
.method public abstract deleteFile(Ljava/lang/String;)V
.end method

.method public abstract readFromEncryptedFile(Ljava/lang/String;Lcom/google/common/base/Function;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract readFromFile(Ljava/lang/String;Lcom/google/common/base/Function;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateEncryptedFile(Ljava/lang/String;Lcom/google/common/base/Function;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/base/Function",
            "<[B[B>;)V"
        }
    .end annotation
.end method

.method public abstract writeToEncryptedFile(Ljava/lang/String;[B)V
.end method

.method public abstract writeToFile(Ljava/lang/String;[B)V
.end method
