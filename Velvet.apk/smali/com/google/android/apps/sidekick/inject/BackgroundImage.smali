.class public Lcom/google/android/apps/sidekick/inject/BackgroundImage;
.super Ljava/lang/Object;
.source "BackgroundImage.java"


# instance fields
.field private final mDescriptor:Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

.field private final mPhoto:Lcom/google/geo/sidekick/Sidekick$Photo;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Photo;Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Photo;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->mPhoto:Lcom/google/geo/sidekick/Sidekick$Photo;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->mDescriptor:Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    return-void
.end method


# virtual methods
.method public getDescriptor()Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->mDescriptor:Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    return-object v0
.end method

.method public getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->mPhoto:Lcom/google/geo/sidekick/Sidekick$Photo;

    return-object v0
.end method

.method public isDoodle()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->getDescriptor()Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->getDescriptor()Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$BackgroundPhotoDescriptor;->getSource()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
