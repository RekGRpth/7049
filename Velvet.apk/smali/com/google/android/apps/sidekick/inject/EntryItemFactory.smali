.class public interface abstract Lcom/google/android/apps/sidekick/inject/EntryItemFactory;
.super Ljava/lang/Object;
.source "EntryItemFactory.java"


# virtual methods
.method public abstract create(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
.end method

.method public abstract createForGroup(Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;)Lcom/google/android/apps/sidekick/EntryItemAdapter;
.end method
