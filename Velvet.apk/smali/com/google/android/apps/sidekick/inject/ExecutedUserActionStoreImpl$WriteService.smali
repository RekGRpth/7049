.class public Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl$WriteService;
.super Landroid/app/IntentService;
.source "ExecutedUserActionStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WriteService"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "ExecutedUserActionStoreImpl_WriteService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl$WriteService;->setIntentRedelivery(Z)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getExecutedUserActionStore()Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;

    if-nez v2, :cond_2

    # getter for: Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected ExecutedUserActionStore implementation, dropping intent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;

    const-string v2, "write"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, p1}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->handleWriteIntent(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const-string v2, "delete"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStoreImpl;->deleteStore()V

    goto :goto_0
.end method
