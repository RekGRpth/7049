.class public final Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ExecutedUserActions.java"


# instance fields
.field private cachedSize:I

.field private executedUserAction_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->executedUserAction_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addExecutedUserAction(Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;)Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->executedUserAction_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->executedUserAction_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->executedUserAction_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final clear()Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->clearExecutedUserAction()Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->cachedSize:I

    return-object p0
.end method

.method public clearExecutedUserAction()Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->executedUserAction_:Ljava/util/List;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->cachedSize:I

    return v0
.end method

.method public getExecutedUserActionCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->executedUserAction_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getExecutedUserActionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->executedUserAction_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->getExecutedUserActionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    iput v2, p0, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->cachedSize:I

    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->addExecutedUserAction(Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;)Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActions;->getExecutedUserActionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$ExecutedUserAction;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    return-void
.end method
