.class public Lcom/google/android/apps/sidekick/inject/ListenerManager;
.super Ljava/lang/Object;
.source "ListenerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/inject/ListenerManager$1;,
        Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;,
        Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mAction:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDispatcher:Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher",
            "<TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mAction:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mDispatcher:Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    new-instance v0, Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/inject/ListenerManager$Receiver;-><init>(Lcom/google/android/apps/sidekick/inject/ListenerManager;Lcom/google/android/apps/sidekick/inject/ListenerManager$1;)V

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/inject/ListenerManager;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/inject/ListenerManager;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/sidekick/inject/ListenerManager;)Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;
    .locals 1
    .param p0    # Lcom/google/android/apps/sidekick/inject/ListenerManager;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mDispatcher:Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;

    return-object v0
.end method


# virtual methods
.method public getReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public registerListener(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mAction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unRegisterListener(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/inject/ListenerManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
