.class public interface abstract Lcom/google/android/apps/sidekick/inject/LocationOracle;
.super Ljava/lang/Object;
.source "LocationOracle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;,
        Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;
    }
.end annotation


# virtual methods
.method public abstract addLightweightGeofencer(Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;)V
.end method

.method public abstract blockingUpdateBestLocation()Landroid/location/Location;
.end method

.method public abstract getBestLocation()Landroid/location/Location;
.end method

.method public abstract getBestLocations()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasLocation()Z
.end method

.method public abstract newRunningLock(Ljava/lang/String;)Lcom/google/android/apps/sidekick/inject/LocationOracle$RunningLock;
.end method

.method public abstract phoneIsStationary()Z
.end method

.method public abstract requestRecentLocation(J)V
.end method

.method public abstract stationaryTimeSeconds()I
.end method
