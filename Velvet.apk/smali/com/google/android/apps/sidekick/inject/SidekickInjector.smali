.class public interface abstract Lcom/google/android/apps/sidekick/inject/SidekickInjector;
.super Ljava/lang/Object;
.source "SidekickInjector.java"


# virtual methods
.method public abstract getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;
.end method

.method public abstract getAlarmUtils()Lcom/google/android/apps/sidekick/AlarmUtils;
.end method

.method public abstract getAsyncFileStorage()Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;
.end method

.method public abstract getCalendarController()Lcom/google/android/apps/sidekick/calendar/CalendarController;
.end method

.method public abstract getCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
.end method

.method public abstract getEntryInvalidator()Lcom/google/android/apps/sidekick/inject/EntryInvalidator;
.end method

.method public abstract getEntryItemFactory()Lcom/google/android/apps/sidekick/inject/EntryItemFactory;
.end method

.method public abstract getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;
.end method

.method public abstract getExecutedUserActionStore()Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;
.end method

.method public abstract getGmmLocationProvider()Lcom/google/android/apps/sidekick/inject/GmmLocationProvider;
.end method

.method public abstract getLocalBroadcastManager()Landroid/support/v4/content/LocalBroadcastManager;
.end method

.method public abstract getLocationManager()Lcom/google/android/apps/sidekick/inject/LocationManagerInjectable;
.end method

.method public abstract getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;
.end method

.method public abstract getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;
.end method

.method public abstract getNotificationStore()Lcom/google/android/apps/sidekick/notifications/NotificationStore;
.end method

.method public abstract getNowNotificationManager()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
.end method

.method public abstract getSensorSignalsOracle()Lcom/google/android/apps/sidekick/SensorSignalsOracle;
.end method

.method public abstract getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;
.end method
