.class public Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;
.super Ljava/lang/Object;
.source "SystemAlarmManagerInjectable.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAlarmManager:Landroid/app/AlarmManager;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;->mAlarmManager:Landroid/app/AlarmManager;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-void
.end method


# virtual methods
.method public cancel(Landroid/app/PendingIntent;)V
    .locals 1
    .param p1    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public set(IJLandroid/app/PendingIntent;)V
    .locals 1
    .param p1    # I
    .param p2    # J
    .param p4    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public setRepeating(IJJLandroid/app/PendingIntent;)V
    .locals 7
    .param p1    # I
    .param p2    # J
    .param p4    # J
    .param p6    # Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;->mAlarmManager:Landroid/app/AlarmManager;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    return-void
.end method
