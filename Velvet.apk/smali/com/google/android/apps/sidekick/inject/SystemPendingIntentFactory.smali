.class public Lcom/google/android/apps/sidekick/inject/SystemPendingIntentFactory;
.super Ljava/lang/Object;
.source "SystemPendingIntentFactory.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;


# instance fields
.field private final mAppContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/inject/SystemPendingIntentFactory;->mAppContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public createComponentName(Ljava/lang/Class;)Landroid/content/ComponentName;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/ComponentName;"
        }
    .end annotation

    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/inject/SystemPendingIntentFactory;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public getBroadcast(ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/content/Intent;
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemPendingIntentFactory;->mAppContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public getService(ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/content/Intent;
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/sidekick/inject/SystemPendingIntentFactory;->mAppContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
