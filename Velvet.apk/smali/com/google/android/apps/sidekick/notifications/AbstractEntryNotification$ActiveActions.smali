.class Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;
.super Ljava/lang/Object;
.source "AbstractEntryNotification.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActiveActions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/apps/sidekick/notifications/NotificationAction;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$1;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/notifications/NotificationAction;

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/notifications/NotificationAction;->isActive()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/sidekick/notifications/NotificationAction;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;->apply(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)Z

    move-result v0

    return v0
.end method
