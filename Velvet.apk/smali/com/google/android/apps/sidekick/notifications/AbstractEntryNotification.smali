.class public abstract Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;
.super Ljava/lang/Object;
.source "AbstractEntryNotification.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/notifications/EntryNotification;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$1;,
        Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;
    }
.end annotation


# static fields
.field private static final ACTIVE_ACTIONS:Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;


# instance fields
.field private final mEntries:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotificationActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/notifications/NotificationAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;-><init>(Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$1;)V

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->ACTIVE_ACTIONS:Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->mNotificationActions:Ljava/util/List;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->mEntries:Ljava/util/Collection;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addAction(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/notifications/NotificationAction;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->mNotificationActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getActiveActions()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/sidekick/notifications/NotificationAction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->mNotificationActions:Ljava/util/List;

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->ACTIVE_ACTIONS:Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification$ActiveActions;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterables;->filter(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final getEntries()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->mEntries:Ljava/util/Collection;

    return-object v0
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Notification"

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/Util;->removeTrailingSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationContentIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "notificationEntriesKey"

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->getEntries()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->putEntriesInIntent(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v1, "android.intent.action.ASSIST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notification_content://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->addToIntent(Landroid/content/Intent;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationStyle()Landroid/app/Notification$Style;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public isActiveNotification()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->getNotificationType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLowPriorityNotification()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->getNotificationType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
