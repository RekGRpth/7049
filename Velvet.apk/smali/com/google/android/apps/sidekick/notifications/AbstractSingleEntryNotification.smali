.class public abstract Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;
.source "AbstractSingleEntryNotification.java"


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-static {p1}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasType()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    return-void
.end method


# virtual methods
.method getDefaultNotificationText()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasNotificationBarText()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getNotificationBarText()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;->getEntries()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method

.method public getLoggingName()Ljava/lang/String;
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractEntryNotification;->getLoggingName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->getGenericEntryType(Lcom/google/geo/sidekick/Sidekick$Entry;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public getNotificationType()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Notification;->getType()I

    move-result v0

    return v0
.end method
