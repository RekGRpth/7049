.class public Lcom/google/android/apps/sidekick/notifications/CalendarNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "CalendarNotification.java"


# instance fields
.field private final mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

.field private final mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

.field private final mCalendarEntryAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

.field private final mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

.field private final mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p4    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p5    # Lcom/google/android/apps/sidekick/DirectionsLauncher;
    .param p6    # Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    iput-object p6, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarEntryAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getCalendarEntry()Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getHash()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->getCalendarDataByServerHash(Ljava/lang/String;)Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p4, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

    iput-object p5, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->addNavigateAction()V

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->addEmailAction()V

    return-void
.end method

.method private addEmailAction()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/notifications/EmailAttendeesAction;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarEntryAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-direct {v0, v1}, Lcom/google/android/apps/sidekick/notifications/EmailAttendeesAction;-><init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->addAction(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)V

    return-void
.end method

.method private addNavigateAction()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasRoute()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getRoute()Lcom/google/geo/sidekick/Sidekick$CommuteSummary;

    move-result-object v1

    :goto_0
    new-instance v0, Lcom/google/android/apps/sidekick/notifications/NavigateAction;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCurrentLocation:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mDestination:Lcom/google/geo/sidekick/Sidekick$Location;

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mDirectionsLauncher:Lcom/google/android/apps/sidekick/DirectionsLauncher;

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/apps/sidekick/notifications/NavigateAction;-><init>(Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/geo/sidekick/Sidekick$Location;Lcom/google/android/apps/sidekick/DirectionsLauncher;Lcom/google/geo/sidekick/Sidekick$CommuteSummary;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->addAction(Lcom/google/android/apps/sidekick/notifications/NotificationAction;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 9
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->isLowPriorityNotification()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->hasTravelTimeSeconds()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarEntry:Lcom/google/geo/sidekick/Sidekick$CalendarEntry;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$CalendarEntry;->getTravelTimeSeconds()I

    move-result v4

    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getStartTimeSeconds()J

    move-result-wide v5

    int-to-long v7, v4

    sub-long v2, v5, v7

    new-instance v1, Ljava/util/Date;

    const-wide/16 v5, 0x3e8

    mul-long/2addr v5, v2

    invoke-direct {v1, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    const v5, 0x7f0d00fb

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->hasTravelTimeMinutes()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getServerData()Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/calendar/Calendar$ServerData;->getTravelTimeMinutes()I

    move-result v5

    mul-int/lit8 v4, v5, 0x3c

    goto :goto_1
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->isLowPriorityNotification()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->getDefaultNotificationText()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->getNotificationBarText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Notification;->getNotificationBarText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const v1, 0x7f0d00fa

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->mCalendarData:Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->isLowPriorityNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->CALENDAR_TIME_TO_LEAVE_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    goto :goto_0
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->isLowPriorityNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f02016f

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f02011c

    goto :goto_0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/notifications/CalendarNotification;->getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
