.class public Lcom/google/android/apps/sidekick/notifications/EmailAttendeesAction;
.super Ljava/lang/Object;
.source "EmailAttendeesAction.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/notifications/NotificationAction;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/EmailAttendeesAction;->mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    return-void
.end method


# virtual methods
.method public getActionIcon()I
    .locals 1

    const v0, 0x7f0200bf

    return v0
.end method

.method public getActionString(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d02a0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCallbackIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/EmailAttendeesAction;->mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getCalendarData()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getEventId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/EmailAttendeesAction;->mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->createEmailAttendeesIntent(J)Landroid/content/Intent;

    move-result-object v2

    return-object v2
.end method

.method public getCallbackType()Ljava/lang/String;
    .locals 1

    const-string v0, "broadcast"

    return-object v0
.end method

.method public getLogString()Ljava/lang/String;
    .locals 1

    const-string v0, "EMAIL_GUESTS"

    return-object v0
.end method

.method public isActive()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/EmailAttendeesAction;->mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getCalendarData()Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/calendar/Calendar$CalendarData;->getEventData()Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/calendar/Calendar$EventData;->getNumberOfAttendees()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
