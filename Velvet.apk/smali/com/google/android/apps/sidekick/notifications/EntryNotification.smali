.class public interface abstract Lcom/google/android/apps/sidekick/notifications/EntryNotification;
.super Ljava/lang/Object;
.source "EntryNotification.java"


# virtual methods
.method public abstract getActiveActions()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/sidekick/notifications/NotificationAction;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEntries()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLoggingName()Ljava/lang/String;
.end method

.method public abstract getNotificationContentIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
.end method

.method public abstract getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
.end method

.method public abstract getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
.end method

.method public abstract getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
.end method

.method public abstract getNotificationSmallIcon()I
.end method

.method public abstract getNotificationStyle()Landroid/app/Notification$Style;
.end method

.method public abstract getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
.end method

.method public abstract getNotificationType()I
.end method

.method public abstract isActiveNotification()Z
.end method

.method public abstract isLowPriorityNotification()Z
.end method
