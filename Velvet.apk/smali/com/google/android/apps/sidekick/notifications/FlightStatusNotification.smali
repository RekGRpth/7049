.class public Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "FlightStatusNotification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification$1;
    }
.end annotation


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

.field private final mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

.field private final mRelevantFlight:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;
    .param p3    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getFlightStatusEntry()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mRelevantFlight:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    iput-object p3, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    return-void
.end method

.method private flightDelayMinutes()I
    .locals 7

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification$1;->$SwitchMap$com$google$android$apps$sidekick$FlightStatusEntryAdapter$RelevantFlight$Status:[I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mRelevantFlight:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getStatus()Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasScheduledTimeSecondsSinceEpoch()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->hasActualTimeSecondsSinceEpoch()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_1
    return v1

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mRelevantFlight:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getDepartureTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mRelevantFlight:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getArrivalTime()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-wide/16 v1, 0x0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getActualTimeSecondsSinceEpoch()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Time;->getScheduledTimeSecondsSinceEpoch()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x3c

    div-long/2addr v3, v5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    long-to-int v1, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mRelevantFlight:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;->getFormattedTime(Landroid/content/Context;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mRelevantFlight:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mRelevantFlight:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter$RelevantFlight;->getFlight()Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getStatusCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->mFlightStatusEntry:Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;

    invoke-virtual {v3, v6}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry;->getFlight(I)Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;

    move-result-object v0

    const v3, 0x7f0d011c

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :pswitch_0
    const v3, 0x7f0d011d

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/FlightStatusNotification;->flightDelayMinutes()I

    move-result v2

    if-lez v2, :cond_1

    const v3, 0x7f0d011e

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    const v3, 0x7f0d011f

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getAirlineName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$FlightStatusEntry$Flight;->getFlightNumber()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    return-object v0
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    const v0, 0x7f020170

    return v0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method
