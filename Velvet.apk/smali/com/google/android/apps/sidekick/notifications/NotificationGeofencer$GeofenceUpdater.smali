.class Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;
.super Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;
.source "NotificationGeofencer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GeofenceUpdater"
.end annotation


# instance fields
.field private final mNewLocation:Landroid/location/Location;

.field private final mPreviousLocation:Landroid/location/Location;


# direct methods
.method public constructor <init>(Landroid/location/Location;Landroid/location/Location;)V
    .locals 0
    .param p1    # Landroid/location/Location;
    .param p2    # Landroid/location/Location;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->mPreviousLocation:Landroid/location/Location;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->mNewLocation:Landroid/location/Location;

    return-void
.end method

.method private static isPointInGeofence(Lcom/google/geo/sidekick/Sidekick$Location;Landroid/location/Location;)Z
    .locals 12
    .param p0    # Lcom/google/geo/sidekick/Sidekick$Location;
    .param p1    # Landroid/location/Location;

    const/4 v10, 0x1

    const/4 v11, 0x0

    new-array v8, v10, [F

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLat()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getLng()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    aget v9, v8, v11

    float-to-double v0, v9

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getRadiusMeters()D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    invoke-virtual {p0}, Lcom/google/geo/sidekick/Sidekick$Location;->getRadiusMeters()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L

    mul-double/2addr v2, v4

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    move v0, v10

    :goto_0
    return v0

    :cond_0
    move v0, v11

    goto :goto_0
.end method


# virtual methods
.method protected areTriggerConditionsSatisfied(Lcom/google/geo/sidekick/Sidekick$Entry;Z)Z
    .locals 7
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p2    # Z

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->hasNotification()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Notification;->hasTriggerCondition()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->hasLocation()Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getConditionCount()I

    move-result v4

    if-lez v4, :cond_1

    move v4, v5

    :goto_1
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getNotification()Lcom/google/geo/sidekick/Sidekick$Notification;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$Notification;->getTriggerCondition()Lcom/google/geo/sidekick/Sidekick$TriggerCondition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getLocation()Lcom/google/geo/sidekick/Sidekick$Location;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getConditionCount()I

    move-result v4

    if-le v4, v5, :cond_2

    :goto_2
    return p2

    :cond_0
    move v4, v6

    goto :goto_0

    :cond_1
    move v4, v6

    goto :goto_1

    :cond_2
    invoke-virtual {v0, v6}, Lcom/google/geo/sidekick/Sidekick$TriggerCondition;->getCondition(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    # getter for: Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown trigger condition type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v3, p2

    :goto_3
    move p2, v3

    goto :goto_2

    :sswitch_0
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->mNewLocation:Landroid/location/Location;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->mNewLocation:Landroid/location/Location;

    invoke-static {v2, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->isPointInGeofence(Lcom/google/geo/sidekick/Sidekick$Location;Landroid/location/Location;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v3, v5

    :goto_4
    goto :goto_3

    :cond_3
    move v3, v6

    goto :goto_4

    :sswitch_1
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->mNewLocation:Landroid/location/Location;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->mNewLocation:Landroid/location/Location;

    invoke-static {v2, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->isPointInGeofence(Lcom/google/geo/sidekick/Sidekick$Location;Landroid/location/Location;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->mPreviousLocation:Landroid/location/Location;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->mPreviousLocation:Landroid/location/Location;

    invoke-static {v2, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->isPointInGeofence(Lcom/google/geo/sidekick/Sidekick$Location;Landroid/location/Location;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v3, 0x1

    goto :goto_3

    :cond_6
    move v3, p2

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method
