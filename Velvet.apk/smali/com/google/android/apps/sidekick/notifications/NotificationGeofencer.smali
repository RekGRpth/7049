.class public Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;
.super Ljava/lang/Object;
.source "NotificationGeofencer.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/LocationOracle$LightweightGeofencer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/sidekick/notifications/NotificationStore;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;Landroid/location/Location;)V
    .locals 5
    .param p1    # Landroid/location/Location;
    .param p2    # Landroid/location/Location;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->initialize()V

    new-instance v1, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;-><init>(Landroid/location/Location;Landroid/location/Location;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->mNotificationStore:Lcom/google/android/apps/sidekick/notifications/NotificationStore;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/sidekick/notifications/NotificationStore;->updateLocationTriggerConditions(Lcom/google/android/apps/sidekick/notifications/NotificationStore$TriggerConditionsUpdater;)V

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->hasAffectedNotifications()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->getTriggeredNotifications()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer$GeofenceUpdater;->getConcludedNotifications()Ljava/util/Set;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;->getTriggerIntent(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationGeofencer;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method
