.class Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;
.super Ljava/lang/Object;
.source "NotificationRefreshService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BackoffTime"
.end annotation


# instance fields
.field private final mBackoffTimesMins:[I

.field private volatile mLastBackoffIdx:I

.field private final mRandomVarianceSecs:I


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mBackoffTimesMins:[I

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mRandomVarianceSecs:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mLastBackoffIdx:I

    return-void

    :array_0
    .array-data 4
        0x1
        0x3
        0x5
        0xf
        0x1e
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$1;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;-><init>()V

    return-void
.end method


# virtual methods
.method clearBackoff()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mLastBackoffIdx:I

    return-void
.end method

.method getRetryAlarmOffsetMillis()J
    .locals 7

    iget v1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mLastBackoffIdx:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mLastBackoffIdx:I

    iget-object v2, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mBackoffTimesMins:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mBackoffTimesMins:[I

    aget v1, v1, v0

    int-to-long v1, v1

    const-wide/32 v3, 0xea60

    mul-long/2addr v1, v3

    iget v3, p0, Lcom/google/android/apps/sidekick/notifications/NotificationRefreshService$BackoffTime;->mRandomVarianceSecs:I

    int-to-long v3, v3

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    add-long/2addr v1, v3

    return-wide v1
.end method
