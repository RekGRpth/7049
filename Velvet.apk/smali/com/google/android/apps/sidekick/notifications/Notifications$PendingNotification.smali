.class public final Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Notifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/notifications/Notifications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PendingNotification"
.end annotation


# instance fields
.field private cachedSize:I

.field private entry_:Lcom/google/geo/sidekick/Sidekick$Entry;

.field private hasEntry:Z

.field private hasInterest:Z

.field private hasLastTriggerTimeSeconds:Z

.field private hasNotificationDismissed:Z

.field private hasNotified:Z

.field private hasSnoozeTimeSeconds:Z

.field private interest_:Lcom/google/geo/sidekick/Sidekick$Interest;

.field private lastTriggerTimeSeconds_:J

.field private notificationDismissed_:Z

.field private notified_:Z

.field private snoozeTimeSeconds_:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->entry_:Lcom/google/geo/sidekick/Sidekick$Entry;

    iput-object v1, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->interest_:Lcom/google/geo/sidekick/Sidekick$Interest;

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->notified_:Z

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->notificationDismissed_:Z

    iput-wide v2, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->lastTriggerTimeSeconds_:J

    iput-wide v2, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->snoozeTimeSeconds_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->cachedSize:I

    return-void
.end method


# virtual methods
.method public clearLastTriggerTimeSeconds()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasLastTriggerTimeSeconds:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->lastTriggerTimeSeconds_:J

    return-object p0
.end method

.method public clearNotificationDismissed()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotificationDismissed:Z

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->notificationDismissed_:Z

    return-object p0
.end method

.method public clearNotified()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotified:Z

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->notified_:Z

    return-object p0
.end method

.method public clearSnoozeTimeSeconds()Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->snoozeTimeSeconds_:J

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->cachedSize:I

    return v0
.end method

.method public getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->entry_:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object v0
.end method

.method public getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->interest_:Lcom/google/geo/sidekick/Sidekick$Interest;

    return-object v0
.end method

.method public getLastTriggerTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->lastTriggerTimeSeconds_:J

    return-wide v0
.end method

.method public getNotificationDismissed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->notificationDismissed_:Z

    return v0
.end method

.method public getNotified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->notified_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasEntry()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasInterest()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotified()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotified()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotificationDismissed()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotificationDismissed()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasLastTriggerTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getLastTriggerTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getSnoozeTimeSeconds()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->cachedSize:I

    return v0
.end method

.method public getSnoozeTimeSeconds()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->snoozeTimeSeconds_:J

    return-wide v0
.end method

.method public hasEntry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasEntry:Z

    return v0
.end method

.method public hasInterest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasInterest:Z

    return v0
.end method

.method public hasLastTriggerTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasLastTriggerTimeSeconds:Z

    return v0
.end method

.method public hasNotificationDismissed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotificationDismissed:Z

    return v0
.end method

.method public hasNotified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotified:Z

    return v0
.end method

.method public hasSnoozeTimeSeconds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setNotified(Z)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setNotificationDismissed(Z)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setLastTriggerTimeSeconds(J)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->setSnoozeTimeSeconds(J)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;

    move-result-object v0

    return-object v0
.end method

.method public setEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasEntry:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->entry_:Lcom/google/geo/sidekick/Sidekick$Entry;

    return-object p0
.end method

.method public setInterest(Lcom/google/geo/sidekick/Sidekick$Interest;)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasInterest:Z

    iput-object p1, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->interest_:Lcom/google/geo/sidekick/Sidekick$Interest;

    return-object p0
.end method

.method public setLastTriggerTimeSeconds(J)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasLastTriggerTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->lastTriggerTimeSeconds_:J

    return-object p0
.end method

.method public setNotificationDismissed(Z)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotificationDismissed:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->notificationDismissed_:Z

    return-object p0
.end method

.method public setNotified(Z)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotified:Z

    iput-boolean p1, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->notified_:Z

    return-object p0
.end method

.method public setSnoozeTimeSeconds(J)Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds:Z

    iput-wide p1, p0, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->snoozeTimeSeconds_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasEntry()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasInterest()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getInterest()Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotified()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotified()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasNotificationDismissed()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getNotificationDismissed()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasLastTriggerTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getLastTriggerTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->hasSnoozeTimeSeconds()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/sidekick/notifications/Notifications$PendingNotification;->getSnoozeTimeSeconds()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_5
    return-void
.end method
