.class public interface abstract Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;
.super Ljava/lang/Object;
.source "NowNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    }
.end annotation


# virtual methods
.method public abstract cancelAll()V
.end method

.method public abstract cancelNotification(Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V
.end method

.method public abstract createNotification(Lcom/google/android/apps/sidekick/notifications/EntryNotification;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract dismissNotification(Ljava/util/Collection;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;",
            ")V"
        }
    .end annotation
.end method

.method public abstract getLastNotificationTime()J
.end method

.method public abstract sendDeliverActiveNotification(Lcom/google/geo/sidekick/Sidekick$Entry;)V
.end method

.method public abstract setLastNotificationTime()V
.end method

.method public abstract showNotification(Landroid/app/Notification;Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;)V
.end method
