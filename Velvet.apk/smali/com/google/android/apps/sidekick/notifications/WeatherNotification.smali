.class public Lcom/google/android/apps/sidekick/notifications/WeatherNotification;
.super Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;
.source "WeatherNotification.java"


# instance fields
.field private final mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, p1}, Lcom/google/android/apps/sidekick/notifications/AbstractSingleEntryNotification;-><init>(Lcom/google/geo/sidekick/Sidekick$Entry;)V

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWeatherEntry()Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/sidekick/notifications/WeatherNotification;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    return-void
.end method


# virtual methods
.method public getNotificationContentText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNotificationContentTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/notifications/WeatherNotification;->mWeatherEntry:Lcom/google/geo/sidekick/Sidekick$WeatherEntry;

    invoke-virtual {v1, v4}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry;->getWeatherPoint(I)Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasHighTemperature()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasDescription()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0d0137

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getDescription()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->hasHighTemperature()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0d0134

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$WeatherEntry$WeatherPoint;->getHighTemperature()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNotificationId()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;->LOW_PRIORITY_NOTIFICATION:Lcom/google/android/apps/sidekick/notifications/NowNotificationManager$NotificationType;

    return-object v0
.end method

.method public getNotificationSmallIcon()I
    .locals 1

    const v0, 0x7f020180

    return v0
.end method

.method public getNotificationTickerText(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    return-object v0
.end method
