.class public interface abstract Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;
.super Ljava/lang/Object;
.source "MessageVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/sync/MessageVisitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Visitor"
.end annotation


# virtual methods
.method public abstract visitRepeated(Ljava/lang/reflect/Field;Ljava/util/SortedMap;Ljava/util/SortedMap;)Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;)",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;"
        }
    .end annotation
.end method

.method public abstract visitScalar(Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)V
.end method
