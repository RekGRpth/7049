.class public final Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;
.super Ljava/lang/Object;
.source "SidekickConfigurationRepeatedMessageInfo.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$NoInfoForMessageException;,
        Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider;
    }
.end annotation


# static fields
.field private static final INFO_PROVIDERS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;",
            "Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider",
            "<+",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/google/common/collect/Maps;->newConcurrentMap()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;->INFO_PROVIDERS:Ljava/util/Map;

    sget-object v0, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;->INFO_PROVIDERS:Ljava/util/Map;

    const-class v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    new-instance v2, Lcom/google/android/apps/sidekick/sync/SportTeamPlayerInfoProvider;

    invoke-direct {v2}, Lcom/google/android/apps/sidekick/sync/SportTeamPlayerInfoProvider;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;->INFO_PROVIDERS:Ljava/util/Map;

    const-class v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    new-instance v2, Lcom/google/android/apps/sidekick/sync/StockDataInfoProvider;

    invoke-direct {v2}, Lcom/google/android/apps/sidekick/sync/StockDataInfoProvider;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;->INFO_PROVIDERS:Ljava/util/Map;

    const-class v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$News$ExcludedQuery;

    new-instance v2, Lcom/google/android/apps/sidekick/sync/NewsInfoProvider;

    invoke-direct {v2}, Lcom/google/android/apps/sidekick/sync/NewsInfoProvider;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;->INFO_PROVIDERS:Ljava/util/Map;

    const-class v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$TrafficCardSharing$LocationSharingContact;

    new-instance v2, Lcom/google/android/apps/sidekick/sync/StockDataInfoProvider;

    invoke-direct {v2}, Lcom/google/android/apps/sidekick/sync/StockDataInfoProvider;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static KeyGeneratorFor(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(TT;)",
            "Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$NoInfoForMessageException;
        }
    .end annotation

    sget-object v1, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;->INFO_PROVIDERS:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider;

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$NoInfoForMessageException;

    invoke-direct {v1, p0}, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$NoInfoForMessageException;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    throw v1

    :cond_0
    return-object v0
.end method


# virtual methods
.method hasPrimaryKeyGenerator(Lcom/google/protobuf/micro/MessageMicro;)Z
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    sget-object v0, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;->INFO_PROVIDERS:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;->KeyGeneratorFor(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider;->generateKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
