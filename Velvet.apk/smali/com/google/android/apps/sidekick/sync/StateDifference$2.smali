.class final Lcom/google/android/apps/sidekick/sync/StateDifference$2;
.super Ljava/lang/Object;
.source "StateDifference.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/sync/MessageVisitor$Visitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/sidekick/sync/StateDifference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visitRepeated(Ljava/lang/reflect/Field;Ljava/util/SortedMap;Ljava/util/SortedMap;)Ljava/util/SortedMap;
    .locals 6
    .param p1    # Ljava/lang/reflect/Field;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;)",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">;"
        }
    .end annotation

    invoke-interface {p3}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p2}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/common/collect/Sets;->difference(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p3, v2}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public visitScalar(Ljava/lang/reflect/Field;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;)V
    .locals 0
    .param p1    # Ljava/lang/reflect/Field;
    .param p2    # Lcom/google/protobuf/micro/MessageMicro;
    .param p3    # Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {p3, p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->clearFieldValue(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)V

    return-void
.end method
