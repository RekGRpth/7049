.class Lcom/google/android/apps/sidekick/sync/StockDataInfoProvider;
.super Ljava/lang/Object;
.source "StockDataInfoProvider.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo$RepeatedMessageInfoProvider",
        "<",
        "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public generateKeyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getGin()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/sync/StockDataInfoProvider;->generateKeyFor(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
