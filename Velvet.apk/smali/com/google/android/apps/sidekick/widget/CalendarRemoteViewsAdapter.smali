.class public Lcom/google/android/apps/sidekick/widget/CalendarRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "CalendarRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/CalendarEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/CalendarRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/widget/CalendarRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 5
    .param p1    # Landroid/content/Context;

    const v4, 0x7f10029f

    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0400df

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v2, 0x7f10029d

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/CalendarRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v2, 0x7f10029e

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/CalendarRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getFormattedStartTime(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/widget/CalendarRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/CalendarEntryAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/sidekick/CalendarEntryAdapter;->getWhereField()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_0
    return-object v0
.end method
