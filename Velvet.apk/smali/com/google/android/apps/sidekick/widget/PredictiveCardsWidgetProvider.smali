.class public Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "PredictiveCardsWidgetProvider.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private getLayoutInfo(Landroid/content/Context;Landroid/os/Bundle;)Landroid/util/Pair;
    .locals 21
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;",
            "Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;",
            ">;"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0c008c

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0c008d

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0c008e

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    const/high16 v19, 0x40000000

    mul-float v19, v19, v18

    add-float v13, v17, v19

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/16 v19, 0x1

    const-string v20, "appWidgetMinHeight"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    const/16 v19, 0x1

    const-string v20, "appWidgetMaxHeight"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    invoke-static {v7, v13}, Ljava/lang/Math;->max(FF)F

    move-result v19

    div-float v19, v19, v13

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->floor(D)D

    move-result-wide v19

    move-wide/from16 v0, v19

    double-to-int v4, v0

    invoke-static {v5, v13}, Ljava/lang/Math;->max(FF)F

    move-result v19

    div-float v19, v19, v13

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->floor(D)D

    move-result-wide v19

    move-wide/from16 v0, v19

    double-to-int v12, v0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0c009a

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    const/16 v19, 0x1

    const-string v20, "appWidgetMinWidth"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v8

    const/16 v19, 0x1

    const-string v20, "appWidgetMaxWidth"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    int-to-float v0, v9

    move/from16 v19, v0

    cmpg-float v19, v6, v19

    if-gez v19, :cond_2

    const/4 v14, 0x1

    :goto_0
    int-to-float v0, v9

    move/from16 v19, v0

    cmpg-float v19, v8, v19

    if-gez v19, :cond_3

    const/4 v15, 0x1

    :goto_1
    const/high16 v19, 0x40000000

    mul-float v19, v19, v18

    add-float v19, v19, v16

    move/from16 v0, v19

    float-to-int v10, v0

    const/16 v19, 0x1

    move/from16 v0, v19

    if-gt v4, v0, :cond_0

    int-to-float v0, v4

    move/from16 v19, v0

    mul-float v19, v19, v17

    sub-float v19, v7, v19

    int-to-float v0, v10

    move/from16 v20, v0

    cmpl-float v19, v19, v20

    if-lez v19, :cond_4

    :cond_0
    const/4 v3, 0x1

    :goto_2
    const/16 v19, 0x1

    move/from16 v0, v19

    if-gt v12, v0, :cond_1

    int-to-float v0, v12

    move/from16 v19, v0

    mul-float v19, v19, v17

    sub-float v19, v5, v19

    int-to-float v0, v10

    move/from16 v20, v0

    cmpl-float v19, v19, v20

    if-lez v19, :cond_5

    :cond_1
    const/4 v11, 0x1

    :goto_3
    new-instance v19, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v14, v3}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;-><init>(IZZ)V

    new-instance v20, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;

    move-object/from16 v0, v20

    invoke-direct {v0, v12, v15, v11}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;-><init>(IZZ)V

    invoke-static/range {v19 .. v20}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v19

    return-object v19

    :cond_2
    const/4 v14, 0x0

    goto :goto_0

    :cond_3
    const/4 v15, 0x0

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    :cond_5
    const/4 v11, 0x0

    goto :goto_3
.end method


# virtual methods
.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetManager;
    .param p3    # I
    .param p4    # Landroid/os/Bundle;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v6

    new-instance v2, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getImageLoader()Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;-><init>(Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/apps/sidekick/inject/WidgetManager;)V

    new-instance v0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v3

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;-><init>(Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/MarinerOptInSettings;Lcom/google/android/searchcommon/util/Clock;)V

    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;->getLayoutInfo(Landroid/content/Context;Landroid/os/Bundle;)Landroid/util/Pair;

    move-result-object v7

    iget-object v1, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;

    iget-object v3, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;

    invoke-virtual {v0, p1, v1, v3, p3}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->populateView(Landroid/content/Context;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;I)Landroid/widget/RemoteViews;

    move-result-object v8

    invoke-virtual {p2, p3, v8}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "WIDGET_REMOVED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "WIDGET_INSTALLED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "internal_request"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->updateWidget()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetManager;
    .param p3    # [I

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v6

    new-instance v2, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getImageLoader()Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getWidgetManager()Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;-><init>(Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/apps/sidekick/inject/WidgetManager;)V

    new-instance v0, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v3

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;-><init>(Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/MarinerOptInSettings;Lcom/google/android/searchcommon/util/Clock;)V

    const/4 v7, 0x0

    :goto_0
    array-length v1, p3

    if-ge v7, v1, :cond_0

    aget v1, p3, v7

    invoke-virtual {p2, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v9

    invoke-direct {p0, p1, v9}, Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;->getLayoutInfo(Landroid/content/Context;Landroid/os/Bundle;)Landroid/util/Pair;

    move-result-object v8

    iget-object v1, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;

    iget-object v3, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;

    aget v4, p3, v7

    invoke-virtual {v0, p1, v1, v3, v4}, Lcom/google/android/apps/sidekick/widget/WidgetPopulator;->populateView(Landroid/content/Context;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;Lcom/google/android/apps/sidekick/widget/WidgetPopulator$WidgetLayoutInfo;I)Landroid/widget/RemoteViews;

    move-result-object v10

    aget v1, p3, v7

    invoke-virtual {p2, v1, v10}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    array-length v1, p3

    if-lez v1, :cond_1

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v3, "WIDGET_UPDATE"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logInternalActionOncePerDay(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    return-void
.end method
