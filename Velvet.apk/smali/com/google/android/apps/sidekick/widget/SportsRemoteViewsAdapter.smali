.class public Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "SportsRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

.field private final mSportsEntryAdapter:Lcom/google/android/apps/sidekick/SportsEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/SportsEntryAdapter;Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/SportsEntryAdapter;
    .param p2    # Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;->mSportsEntryAdapter:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 18
    .param p1    # Landroid/content/Context;

    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0400de

    invoke-direct {v3, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;->mSportsEntryAdapter:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getSportScoreEntry()Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;

    move-result-object v12

    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v14

    const/4 v1, 0x1

    invoke-virtual {v12, v1}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getSportEntity(I)Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;

    move-result-object v16

    const v1, 0x7f100297

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f10029a

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasScore()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f100298

    invoke-virtual {v14}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f100298

    const/4 v2, 0x0

    invoke-virtual {v3, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_0
    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->hasScore()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f10029b

    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;->getScore()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f10029b

    const/4 v2, 0x0

    invoke-virtual {v3, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_1
    invoke-static {v12, v14}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->fromSportEntry(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;)Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-static {v12, v0}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->fromSportEntry(Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;Lcom/google/geo/sidekick/Sidekick$SportScoreEntry$SportEntity;)Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;

    move-result-object v17

    invoke-virtual {v15}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->getUri()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    const v4, 0x7f100296

    invoke-virtual {v15}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->getClipRect()Landroid/graphics/Rect;

    move-result-object v6

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->loadImageUri(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Landroid/graphics/Rect;)V

    :cond_2
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->getUri()Landroid/net/Uri;

    move-result-object v10

    if-eqz v10, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;->mImageLoader:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    const v9, 0x7f100299

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/sidekick/SportsEntryAdapter$LogoUriWrapper;->getClipRect()Landroid/graphics/Rect;

    move-result-object v11

    move-object/from16 v7, p1

    move-object v8, v3

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->loadImageUri(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Landroid/graphics/Rect;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/sidekick/widget/SportsRemoteViewsAdapter;->mSportsEntryAdapter:Lcom/google/android/apps/sidekick/SportsEntryAdapter;

    invoke-virtual {v12}, Lcom/google/geo/sidekick/Sidekick$SportScoreEntry;->getStatusCode()I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/sidekick/SportsEntryAdapter;->getStatus(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const v1, 0x7f1000d8

    invoke-virtual {v3, v1, v13}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f1000d8

    const/4 v2, 0x0

    invoke-virtual {v3, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_4
    return-object v3
.end method
