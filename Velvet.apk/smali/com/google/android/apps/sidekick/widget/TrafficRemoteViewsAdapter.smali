.class public Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "TrafficRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mPlaceEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;->mPlaceEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 9
    .param p1    # Landroid/content/Context;

    const v8, 0x7f10029d

    const v7, 0x7f10029e

    const/4 v6, 0x0

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0400df

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v3, 0x7f0d00eb

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;->mPlaceEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v5, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;->mPlaceEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getShortEtaString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v7, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;->mPlaceEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getTrafficColor(Landroid/content/Context;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    invoke-virtual {v2, v7, v0}, Landroid/widget/RemoteViews;->setTextColor(II)V

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0090

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v8, v6, v3}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0092

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v7, v6, v3}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    return-object v2
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 4
    .param p1    # Landroid/content/Context;

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0400df

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v2, 0x7f10029d

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;->mPlaceEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getFormattedFullTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/sidekick/widget/TrafficRemoteViewsAdapter;->mPlaceEntryAdapter:Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->getRouteDescription(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const v2, 0x7f10029e

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_0
    return-object v1
.end method
