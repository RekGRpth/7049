.class public Lcom/google/android/apps/sidekick/widget/WebsiteUpdateRemoteViewsAdapter;
.super Ljava/lang/Object;
.source "WebsiteUpdateRemoteViewsAdapter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/widget/EntryRemoteViewsAdapter;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/WebsiteUpdateRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    return-void
.end method


# virtual methods
.method public createNarrowRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/widget/WebsiteUpdateRemoteViewsAdapter;->createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public createRemoteView(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 5
    .param p1    # Landroid/content/Context;

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0400df

    invoke-direct {v1, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/WebsiteUpdateRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->getWebsiteUpdateEntries()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Entry;->getWebsiteUpdateEntry()Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;

    move-result-object v2

    const v3, 0x7f10029d

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;->getUpdateTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v3, 0x7f10029e

    iget-object v4, p0, Lcom/google/android/apps/sidekick/widget/WebsiteUpdateRemoteViewsAdapter;->mAdapter:Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;

    invoke-virtual {v4, v2, p1}, Lcom/google/android/apps/sidekick/WebsiteUpdateEntryAdapter;->createWebsiteInfoSpan(Lcom/google/geo/sidekick/Sidekick$WebsiteUpdateEntry;Landroid/content/Context;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    return-object v1
.end method
