.class Lcom/google/android/apps/sidekick/widget/WidgetImageLoader$1;
.super Ljava/lang/Object;
.source "WidgetImageLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->loadImageUri(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/net/Uri;Landroid/graphics/Rect;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader$1;->this$0:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader$1;->this$0:Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;

    # getter for: Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->mWidgetManager:Lcom/google/android/apps/sidekick/inject/WidgetManager;
    invoke-static {v0}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;->access$000(Lcom/google/android/apps/sidekick/widget/WidgetImageLoader;)Lcom/google/android/apps/sidekick/inject/WidgetManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/WidgetManager;->updateWidget()V

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/sidekick/widget/WidgetImageLoader$1;->consume(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method
