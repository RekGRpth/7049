.class public Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;
.super Ljava/lang/Object;
.source "WidgetManagerImpl.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/inject/WidgetManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$1;,
        Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;
    }
.end annotation


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mLastQueuedUpdate:Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;

    invoke-direct {p0}, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->updateWidgetInternal()V

    return-void
.end method

.method private updateWidgetInternal()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mAppContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mAppContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "appWidgetIds"

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v0, "internal_request"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public getWidgetInstallCount()I
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mAppContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    array-length v0, v0

    goto :goto_0
.end method

.method public updateWidget()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;-><init>(Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$1;)V

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mLastQueuedUpdate:Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mLastQueuedUpdate:Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;->cancel()V

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mLastQueuedUpdate:Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl$DelayedWidgetUpdater;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/apps/sidekick/widget/WidgetManagerImpl;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeOnIdle(Ljava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
