.class public Lcom/google/android/ears/EarsResultParser;
.super Ljava/lang/Object;
.source "EarsResultParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertEarsToPlayMediaAction(Lcom/google/audio/ears/proto/EarsService$EarsResult;ZLjava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 9
    .param p0    # Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getMusicResult()Lcom/google/audio/ears/proto/EarsService$MusicResult;

    move-result-object v1

    new-instance v7, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    invoke-direct {v7}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;-><init>()V

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbum()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->setAlbum(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtist()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->setArtist(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getTrack()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->setSong(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getIsExplicit()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->setIsExplicit(Z)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v2

    new-instance v7, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-direct {v7}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;-><init>()V

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setMediaSource(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getTrack()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setName(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setIsFromSoundSearch(Z)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v7

    invoke-static {v1}, Lcom/google/android/ears/EarsResultParser;->getPlayStoreLink(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setMusicItem(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v4

    invoke-static {v1, p1}, Lcom/google/android/ears/EarsResultParser;->getAlbumArtUrl(Lcom/google/audio/ears/proto/EarsService$MusicResult;Z)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v4, v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemImageUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    :cond_0
    invoke-static {v1}, Lcom/google/android/ears/EarsResultParser;->getGoogleMusicProductOffer(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v3, p2}, Lcom/google/android/ears/EarsResultParser;->getPrice(Lcom/google/audio/ears/proto/EarsService$ProductOffer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v7, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;

    invoke-direct {v7}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;-><init>()V

    invoke-virtual {v7, v5}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;->setPrice(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->addItemPriceTag(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    :cond_1
    invoke-virtual {v3}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->hasPreviewUrl()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v3}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getPreviewUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->setItemPreviewUrl(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    :cond_2
    return-object v4
.end method

.method public static getAlbumArtUrl(Lcom/google/audio/ears/proto/EarsService$MusicResult;Z)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/audio/ears/proto/EarsService$MusicResult;
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasAlbumArtUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbumArtUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getAlbumArtUrl()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasSignedInAlbumArtUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getSignedInAlbumArtUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getSignedInAlbumArtUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getFirstEarsResultWithMusic(Ljava/util/List;)Lcom/google/audio/ears/proto/EarsService$EarsResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$EarsResult;",
            ">;)",
            "Lcom/google/audio/ears/proto/EarsService$EarsResult;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/audio/ears/proto/EarsService$EarsResult;

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasMusicResult()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getMusicResult()Lcom/google/audio/ears/proto/EarsService$MusicResult;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/ears/EarsResultParser;->normalizeMusicResult(Lcom/google/audio/ears/proto/EarsService$MusicResult;)V

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getGoogleMusicArtistUrl(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Ljava/lang/String;
    .locals 3
    .param p0    # Lcom/google/audio/ears/proto/EarsService$MusicResult;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtistId()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtistId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "https://play.google.com/store/music/artist"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtistId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&utm_source=sound_search_gsa"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    const-string v1, "https://play.google.com/store/search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?q="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&c=music"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&utm_source=sound_search_gsa"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getGoogleMusicProductOffer(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .locals 4
    .param p0    # Lcom/google/audio/ears/proto/EarsService$MusicResult;

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getOfferList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getVendor()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getGoogleMusicUrl(Lcom/google/audio/ears/proto/EarsService$ProductOffer;)Ljava/lang/String;
    .locals 3
    .param p0    # Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https://play.google.com/store/music/album"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getParentIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&tid=song-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&utm_source=sound_search_gsa"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getPlayStoreLink(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/audio/ears/proto/EarsService$MusicResult;

    invoke-static {p0}, Lcom/google/android/ears/EarsResultParser;->getGoogleMusicProductOffer(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Lcom/google/audio/ears/proto/EarsService$ProductOffer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/ears/EarsResultParser;->getGoogleMusicUrl(Lcom/google/audio/ears/proto/EarsService$ProductOffer;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Lcom/google/android/ears/EarsResultParser;->getGoogleMusicArtistUrl(Lcom/google/audio/ears/proto/EarsService$MusicResult;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getPrice(Lcom/google/audio/ears/proto/EarsService$ProductOffer;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0    # Lcom/google/audio/ears/proto/EarsService$ProductOffer;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$ProductOffer;->getPricingInfoList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;

    invoke-virtual {v4}, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;->getCountryList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    const-string v5, "%s%.2f"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v1}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v4}, Lcom/google/audio/ears/proto/EarsService$ProductOffer$PricingInfo;->getPriceMicros()J

    move-result-wide v8

    long-to-double v8, v8

    const-wide v10, 0x412e848000000000L

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :goto_0
    return-object v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static getQueryForSearch(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/audio/ears/proto/EarsService$EarsResult;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/ears/EarsResultParser;->getFirstEarsResultWithMusic(Ljava/util/List;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->getMusicResult()Lcom/google/audio/ears/proto/EarsService$MusicResult;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getTrack()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getResultTypeInt(Lcom/google/audio/ears/proto/EarsService$EarsResult;)I
    .locals 1
    .param p0    # Lcom/google/audio/ears/proto/EarsService$EarsResult;

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasMusicResult()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$EarsResult;->hasTvResult()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static normalizeMusicResult(Lcom/google/audio/ears/proto/EarsService$MusicResult;)V
    .locals 1
    .param p0    # Lcom/google/audio/ears/proto/EarsService$MusicResult;

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasTrack()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getTrack()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/ears/EarsResultParser;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setTrack(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    :cond_0
    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->hasArtist()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->getArtist()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/ears/EarsResultParser;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/audio/ears/proto/EarsService$MusicResult;->setArtist(Ljava/lang/String;)Lcom/google/audio/ears/proto/EarsService$MusicResult;

    :cond_1
    return-void
.end method

.method private static normalizeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const/16 v0, 0x28

    const/16 v1, 0x29

    invoke-static {p0, v0, v1}, Lcom/google/android/ears/EarsResultParser;->removeBracketedText(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x5b

    const/16 v1, 0x5d

    invoke-static {p0, v0, v1}, Lcom/google/android/ears/EarsResultParser;->removeBracketedText(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static removeBracketedText(Ljava/lang/String;CC)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # C
    .param p2    # C

    const/4 v6, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v4, -0x1

    if-le v3, v4, :cond_4

    const/4 v2, 0x1

    const/4 v0, 0x0

    add-int/lit8 v1, v3, 0x1

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_3

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, p1, :cond_1

    add-int/lit8 v2, v2, 0x1

    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, p2, :cond_2

    add-int/lit8 v2, v2, -0x1

    :cond_2
    if-nez v2, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x1

    :cond_3
    if-nez v0, :cond_0

    invoke-virtual {p0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_4
    return-object p0

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
