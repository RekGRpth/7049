.class public Lcom/google/android/ears/SoundSearchError;
.super Lcom/google/android/velvet/presenter/SearchError;
.source "SoundSearchError.java"


# instance fields
.field private final mImageId:I

.field private final mMessageId:I


# direct methods
.method public constructor <init>(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/SoundSearchRecognizeException;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchError;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/speech/exception/SoundSearchRecognizeException;->getOriginalException()Lcom/google/android/speech/exception/RecognizeException;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/speech/exception/NoMatchRecognizeException;

    if-eqz v0, :cond_0

    const v0, 0x7f0d039e

    iput v0, p0, Lcom/google/android/ears/SoundSearchError;->mMessageId:I

    const v0, 0x7f02011a

    iput v0, p0, Lcom/google/android/ears/SoundSearchError;->mImageId:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/speech/exception/SoundSearchRecognizeException;->getOriginalException()Lcom/google/android/speech/exception/RecognizeException;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/ErrorUtils;->getErrorMessage(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v0

    iput v0, p0, Lcom/google/android/ears/SoundSearchError;->mMessageId:I

    invoke-virtual {p1}, Lcom/google/android/speech/exception/SoundSearchRecognizeException;->getOriginalException()Lcom/google/android/speech/exception/RecognizeException;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/ErrorUtils;->getErrorImage(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v0

    iput v0, p0, Lcom/google/android/ears/SoundSearchError;->mImageId:I

    goto :goto_0
.end method


# virtual methods
.method public getErrorImageResId()I
    .locals 1

    iget v0, p0, Lcom/google/android/ears/SoundSearchError;->mImageId:I

    return v0
.end method

.method public getErrorMessageResId()I
    .locals 1

    iget v0, p0, Lcom/google/android/ears/SoundSearchError;->mMessageId:I

    return v0
.end method
