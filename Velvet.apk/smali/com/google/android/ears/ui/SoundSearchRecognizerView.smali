.class public Lcom/google/android/ears/ui/SoundSearchRecognizerView;
.super Landroid/widget/RelativeLayout;
.source "SoundSearchRecognizerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/ears/ui/SoundSearchRecognizerView$Listener;
    }
.end annotation


# instance fields
.field private mCaptureAnimation:Lcom/google/android/ears/AudioProgressRenderer;

.field private mListener:Lcom/google/android/ears/ui/SoundSearchRecognizerView$Listener;

.field private mMicButton:Landroid/widget/ImageView;

.field private mStatusText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/ears/ui/SoundSearchRecognizerView;)Lcom/google/android/ears/ui/SoundSearchRecognizerView$Listener;
    .locals 1
    .param p0    # Lcom/google/android/ears/ui/SoundSearchRecognizerView;

    iget-object v0, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mListener:Lcom/google/android/ears/ui/SoundSearchRecognizerView$Listener;

    return-object v0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f10022e

    invoke-virtual {p0, v0}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mStatusText:Landroid/widget/TextView;

    const v0, 0x7f10022f

    invoke-virtual {p0, v0}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ears/AudioProgressRenderer;

    iput-object v0, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mCaptureAnimation:Lcom/google/android/ears/AudioProgressRenderer;

    const v0, 0x7f10022d

    invoke-virtual {p0, v0}, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mMicButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mMicButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/ears/ui/SoundSearchRecognizerView$1;

    invoke-direct {v1, p0}, Lcom/google/android/ears/ui/SoundSearchRecognizerView$1;-><init>(Lcom/google/android/ears/ui/SoundSearchRecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setListener(Lcom/google/android/ears/ui/SoundSearchRecognizerView$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/ears/ui/SoundSearchRecognizerView$Listener;

    iput-object p1, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mListener:Lcom/google/android/ears/ui/SoundSearchRecognizerView$Listener;

    return-void
.end method

.method public setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    iget-object v0, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mCaptureAnimation:Lcom/google/android/ears/AudioProgressRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/ears/AudioProgressRenderer;->setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    return-void
.end method

.method public showListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mCaptureAnimation:Lcom/google/android/ears/AudioProgressRenderer;

    invoke-virtual {v0}, Lcom/google/android/ears/AudioProgressRenderer;->startAnimation()V

    return-void
.end method

.method public showNotListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/ears/ui/SoundSearchRecognizerView;->mCaptureAnimation:Lcom/google/android/ears/AudioProgressRenderer;

    invoke-virtual {v0}, Lcom/google/android/ears/AudioProgressRenderer;->stopAnimation()V

    return-void
.end method
