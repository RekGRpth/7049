.class Lcom/google/android/goggles/GogglesActivity$1;
.super Ljava/lang/Object;
.source "GogglesActivity.java"

# interfaces
.implements Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/GogglesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/GogglesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/GogglesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/GogglesActivity$1;->this$0:Lcom/google/android/goggles/GogglesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraCreated(Lcom/google/android/goggles/camera/CameraManager;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/camera/CameraManager;

    new-instance v0, Lcom/google/android/goggles/camera/BarcodeScanner;

    invoke-direct {v0}, Lcom/google/android/goggles/camera/BarcodeScanner;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/goggles/camera/CameraManager;->addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V

    new-instance v0, Lcom/google/android/goggles/GogglesActivity$1$1;

    const-string v1, ""

    invoke-direct {v0, p0, v1}, Lcom/google/android/goggles/GogglesActivity$1$1;-><init>(Lcom/google/android/goggles/GogglesActivity$1;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/google/android/goggles/camera/CameraManager;->addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V

    return-void
.end method

.method public onCameraOpened(Z)V
    .locals 2
    .param p1    # Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/GogglesActivity$1;->this$0:Lcom/google/android/goggles/GogglesActivity;

    # getter for: Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;
    invoke-static {v0}, Lcom/google/android/goggles/GogglesActivity;->access$100(Lcom/google/android/goggles/GogglesActivity;)Lcom/google/android/goggles/ui/GogglesPlate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/GogglesActivity$1;->this$0:Lcom/google/android/goggles/GogglesActivity;

    # getter for: Lcom/google/android/goggles/GogglesActivity;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;
    invoke-static {v1}, Lcom/google/android/goggles/GogglesActivity;->access$000(Lcom/google/android/goggles/GogglesActivity;)Lcom/google/android/goggles/GogglesSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->isTorchOn()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->setTorch(Z)V

    :cond_0
    return-void
.end method

.method public onCameraReleased()V
    .locals 0

    return-void
.end method

.method public onCaptureButtonClicked()V
    .locals 0

    return-void
.end method

.method public onFlashButtonClicked()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/goggles/GogglesActivity$1;->this$0:Lcom/google/android/goggles/GogglesActivity;

    # getter for: Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;
    invoke-static {v1}, Lcom/google/android/goggles/GogglesActivity;->access$100(Lcom/google/android/goggles/GogglesActivity;)Lcom/google/android/goggles/ui/GogglesPlate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/ui/GogglesPlate;->isTorchOn()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/goggles/GogglesActivity$1;->this$0:Lcom/google/android/goggles/GogglesActivity;

    # getter for: Lcom/google/android/goggles/GogglesActivity;->mGogglesPlate:Lcom/google/android/goggles/ui/GogglesPlate;
    invoke-static {v1}, Lcom/google/android/goggles/GogglesActivity;->access$100(Lcom/google/android/goggles/GogglesActivity;)Lcom/google/android/goggles/ui/GogglesPlate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->setTorch(Z)V

    iget-object v1, p0, Lcom/google/android/goggles/GogglesActivity$1;->this$0:Lcom/google/android/goggles/GogglesActivity;

    # getter for: Lcom/google/android/goggles/GogglesActivity;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;
    invoke-static {v1}, Lcom/google/android/goggles/GogglesActivity;->access$000(Lcom/google/android/goggles/GogglesActivity;)Lcom/google/android/goggles/GogglesSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/GogglesSettings;->setTorch(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
