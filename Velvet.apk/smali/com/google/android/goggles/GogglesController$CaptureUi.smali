.class public interface abstract Lcom/google/android/goggles/GogglesController$CaptureUi;
.super Ljava/lang/Object;
.source "GogglesController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/GogglesController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CaptureUi"
.end annotation


# virtual methods
.method public abstract doFinalFocus(Ljava/lang/Runnable;)V
.end method

.method public abstract isTorchOn()Z
.end method

.method public abstract notifyGogglesQuerySent()V
.end method

.method public abstract notifyGogglesResult()V
.end method

.method public abstract notifyNoGogglesResult()V
.end method

.method public abstract pauseUi()V
.end method

.method public abstract setTorch(Z)V
.end method

.method public abstract startCamera()V
.end method
