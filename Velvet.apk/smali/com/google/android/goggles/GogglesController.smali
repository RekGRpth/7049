.class public Lcom/google/android/goggles/GogglesController;
.super Ljava/lang/Object;
.source "GogglesController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/GogglesController$Callback;,
        Lcom/google/android/goggles/GogglesController$GogglesUi;,
        Lcom/google/android/goggles/GogglesController$ResponseUi;,
        Lcom/google/android/goggles/GogglesController$CaptureUi;
    }
.end annotation


# static fields
.field private static final NO_OP_UI:Lcom/google/android/goggles/GogglesController$GogglesUi;


# instance fields
.field private final mBarcodingEnabled:Z

.field private final mCallback:Lcom/google/android/goggles/GogglesController$Callback;

.field private mCaptureButtonClicked:Z

.field private final mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

.field private final mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

.field private final mMainHandler:Landroid/os/Handler;

.field private final mOneShotQueryManager:Lcom/google/android/goggles/network/OneShotQueryManager;

.field private final mPauseCameraUi:Ljava/lang/Runnable;

.field private final mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;

.field private final mResponseUiCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

.field private final mResultCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

.field private final mResultRanker:Lcom/google/android/goggles/ResultRanker;

.field private final mSceneChangeDelayMillis:I

.field private final mSceneFrameProcessor:Lcom/google/android/goggles/SceneFrameProcessor;

.field private final mSessionTimeoutMillis:I

.field private final mSessionTimeoutRunnable:Ljava/lang/Runnable;

.field private mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/goggles/GogglesController$1;

    invoke-direct {v0}, Lcom/google/android/goggles/GogglesController$1;-><init>()V

    sput-object v0, Lcom/google/android/goggles/GogglesController;->NO_OP_UI:Lcom/google/android/goggles/GogglesController$GogglesUi;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/goggles/GogglesController$Callback;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/voicesearch/VoiceSearchServices;Ljava/util/concurrent/Executor;Lcom/google/android/goggles/GogglesSettings;ZILcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;ILcom/google/android/velvet/presenter/QueryState;)V
    .locals 10
    .param p1    # Lcom/google/android/goggles/GogglesController$Callback;
    .param p2    # Lcom/google/android/velvet/VelvetFactory;
    .param p3    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p4    # Ljava/util/concurrent/Executor;
    .param p5    # Lcom/google/android/goggles/GogglesSettings;
    .param p6    # Z
    .param p7    # I
    .param p8    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p9    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p10    # I
    .param p11    # Lcom/google/android/velvet/presenter/QueryState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/goggles/GogglesController$2;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/GogglesController$2;-><init>(Lcom/google/android/goggles/GogglesController;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mSessionTimeoutRunnable:Ljava/lang/Runnable;

    new-instance v1, Lcom/google/android/goggles/GogglesController$3;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/GogglesController$3;-><init>(Lcom/google/android/goggles/GogglesController;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    new-instance v1, Lcom/google/android/goggles/GogglesController$4;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/GogglesController$4;-><init>(Lcom/google/android/goggles/GogglesController;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mPauseCameraUi:Ljava/lang/Runnable;

    new-instance v1, Lcom/google/android/goggles/GogglesController$5;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/GogglesController$5;-><init>(Lcom/google/android/goggles/GogglesController;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mResultCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    new-instance v1, Lcom/google/android/goggles/GogglesController$6;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/GogglesController$6;-><init>(Lcom/google/android/goggles/GogglesController;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mResponseUiCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    sget-object v1, Lcom/google/android/goggles/GogglesController;->NO_OP_UI:Lcom/google/android/goggles/GogglesController$GogglesUi;

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mMainHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/goggles/GogglesController;->mCallback:Lcom/google/android/goggles/GogglesController$Callback;

    new-instance v1, Lcom/google/android/goggles/ResultRanker;

    iget-object v2, p0, Lcom/google/android/goggles/GogglesController;->mResultCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    invoke-direct {v1, v2}, Lcom/google/android/goggles/ResultRanker;-><init>(Lcom/google/android/goggles/ResultRanker$ResultListener;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    iput-object p5, p0, Lcom/google/android/goggles/GogglesController;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    if-eqz p6, :cond_0

    invoke-static {}, Lcom/google/android/goggles/GogglesUtils;->deviceSupportsBarcoding()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/goggles/GogglesController;->mBarcodingEnabled:Z

    new-instance v1, Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v2, p0, Lcom/google/android/goggles/GogglesController;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    iget-object v8, p0, Lcom/google/android/goggles/GogglesController;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    move-object v3, p4

    move-object v4, p3

    move-object v5, p2

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v9, p11

    invoke-direct/range {v1 .. v9}, Lcom/google/android/goggles/QueryFrameProcessor;-><init>(Lcom/google/android/goggles/QueryFrameProcessor$EventListener;Ljava/util/concurrent/Executor;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;

    new-instance v1, Lcom/google/android/goggles/SceneFrameProcessor;

    iget-object v2, p0, Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;

    invoke-direct {v1, v2}, Lcom/google/android/goggles/SceneFrameProcessor;-><init>(Lcom/google/android/goggles/SceneFrameProcessor$Listener;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mSceneFrameProcessor:Lcom/google/android/goggles/SceneFrameProcessor;

    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/goggles/GogglesController;->mSessionTimeoutMillis:I

    move/from16 v0, p10

    iput v0, p0, Lcom/google/android/goggles/GogglesController;->mSceneChangeDelayMillis:I

    new-instance v1, Lcom/google/android/goggles/network/OneShotQueryManager;

    iget-object v7, p0, Lcom/google/android/goggles/GogglesController;->mResponseUiCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    move-object v2, p3

    move-object v3, p2

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    move-object v6, p5

    move-object/from16 v8, p11

    invoke-direct/range {v1 .. v8}, Lcom/google/android/goggles/network/OneShotQueryManager;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/goggles/GogglesController$Callback;Lcom/google/android/velvet/presenter/QueryState;)V

    iput-object v1, p0, Lcom/google/android/goggles/GogglesController;->mOneShotQueryManager:Lcom/google/android/goggles/network/OneShotQueryManager;

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/goggles/GogglesController;)I
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget v0, p0, Lcom/google/android/goggles/GogglesController;->mSessionTimeoutMillis:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/QueryFrameProcessor;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/goggles/GogglesController;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/GogglesController;

    invoke-direct {p0}, Lcom/google/android/goggles/GogglesController;->closeSession()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/GogglesController$Callback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mCallback:Lcom/google/android/goggles/GogglesController$Callback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/GogglesController$GogglesUi;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/ResultRanker;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/goggles/GogglesController;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget-boolean v0, p0, Lcom/google/android/goggles/GogglesController;->mCaptureButtonClicked:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/goggles/GogglesController;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/GogglesController;

    invoke-direct {p0}, Lcom/google/android/goggles/GogglesController;->stopCapturingAndNotifyResults()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/goggles/GogglesController;)Lcom/google/android/goggles/GogglesController$Callback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mResultCallbackProxy:Lcom/google/android/goggles/GogglesController$Callback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/goggles/GogglesController;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/goggles/GogglesController;)I
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget v0, p0, Lcom/google/android/goggles/GogglesController;->mSceneChangeDelayMillis:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/goggles/GogglesController;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesController;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mPauseCameraUi:Ljava/lang/Runnable;

    return-object v0
.end method

.method private closeSession()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesController;->mSessionTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mSceneFrameProcessor:Lcom/google/android/goggles/SceneFrameProcessor;

    invoke-virtual {v0}, Lcom/google/android/goggles/SceneFrameProcessor;->close()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;

    invoke-virtual {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->close()V

    return-void
.end method

.method private startRecognition()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$GogglesUi;->startCamera()V

    return-void
.end method

.method private stopCapturingAndNotifyResults()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultRanker;->hasSceneResults()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultRanker;->stop()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/QueryFrameProcessor;->requestDoneStreamingImages(I)V

    return-void
.end method


# virtual methods
.method public attachUi(Lcom/google/android/goggles/GogglesController$GogglesUi;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/GogglesController$GogglesUi;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    sget-object v1, Lcom/google/android/goggles/GogglesController;->NO_OP_UI:Lcom/google/android/goggles/GogglesController$GogglesUi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/GogglesController$GogglesUi;

    iput-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelRecognition()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultRanker;->cancelWait()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mOneShotQueryManager:Lcom/google/android/goggles/network/OneShotQueryManager;

    invoke-virtual {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->cancel()V

    invoke-direct {p0}, Lcom/google/android/goggles/GogglesController;->closeSession()V

    return-void
.end method

.method public detachUi(Lcom/google/android/goggles/GogglesController$GogglesUi;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/GogglesController$GogglesUi;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    sget-object v1, Lcom/google/android/goggles/GogglesController;->NO_OP_UI:Lcom/google/android/goggles/GogglesController$GogglesUi;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    sget-object v0, Lcom/google/android/goggles/GogglesController;->NO_OP_UI:Lcom/google/android/goggles/GogglesController$GogglesUi;

    iput-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCameraButtonClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mOneShotQueryManager:Lcom/google/android/goggles/network/OneShotQueryManager;

    invoke-virtual {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->cancel()V

    return-void
.end method

.method public onCameraCreated(Lcom/google/android/goggles/camera/CameraManager;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/camera/CameraManager;

    iget-boolean v0, p0, Lcom/google/android/goggles/GogglesController;->mBarcodingEnabled:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/goggles/camera/BarcodeScanner;

    invoke-direct {v0}, Lcom/google/android/goggles/camera/BarcodeScanner;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/goggles/camera/CameraManager;->addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mSceneFrameProcessor:Lcom/google/android/goggles/SceneFrameProcessor;

    invoke-virtual {p1, v0}, Lcom/google/android/goggles/camera/CameraManager;->addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;

    invoke-virtual {p1, v0}, Lcom/google/android/goggles/camera/CameraManager;->addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V

    return-void
.end method

.method public onCameraOpened(Z)V
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesController;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->isTorchOn()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/goggles/GogglesController$GogglesUi;->setTorch(Z)V

    :cond_1
    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->clearStats()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mQueryFrameProcessor:Lcom/google/android/goggles/QueryFrameProcessor;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesController;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->getServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/QueryFrameProcessor;->open(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mSceneFrameProcessor:Lcom/google/android/goggles/SceneFrameProcessor;

    invoke-virtual {v0}, Lcom/google/android/goggles/SceneFrameProcessor;->open()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultRanker;->reset()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/GogglesController;->mCaptureButtonClicked:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/goggles/ui/DebugView;->sessionStartMillis:J

    iget v0, p0, Lcom/google/android/goggles/GogglesController;->mSessionTimeoutMillis:I

    int-to-long v0, v0

    sput-wide v0, Lcom/google/android/goggles/ui/DebugView;->sessionTimeoutMillis:J

    iget v0, p0, Lcom/google/android/goggles/GogglesController;->mSceneChangeDelayMillis:I

    sput v0, Lcom/google/android/goggles/ui/DebugView;->sceneChangeDelayMillis:I

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mMainHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesController;->mSessionTimeoutRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lcom/google/android/goggles/GogglesController;->mSessionTimeoutMillis:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onCameraReleased()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/goggles/GogglesController;->cancelRecognition()V

    return-void
.end method

.method public onCaptureButtonClicked()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/goggles/GogglesController;->mCaptureButtonClicked:Z

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/TraceTracker;->getLatestSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventRequestResults()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mResultRanker:Lcom/google/android/goggles/ResultRanker;

    invoke-virtual {v0}, Lcom/google/android/goggles/ResultRanker;->hasSceneResults()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    new-instance v1, Lcom/google/android/goggles/GogglesController$7;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/GogglesController$7;-><init>(Lcom/google/android/goggles/GogglesController;)V

    invoke-interface {v0, v1}, Lcom/google/android/goggles/GogglesController$GogglesUi;->doFinalFocus(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/goggles/GogglesController;->stopCapturingAndNotifyResults()V

    goto :goto_0
.end method

.method public onFlashButtonClicked()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    invoke-interface {v1}, Lcom/google/android/goggles/GogglesController$GogglesUi;->isTorchOn()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/goggles/GogglesController;->mUi:Lcom/google/android/goggles/GogglesController$GogglesUi;

    invoke-interface {v1, v0}, Lcom/google/android/goggles/GogglesController$GogglesUi;->setTorch(Z)V

    iget-object v1, p0, Lcom/google/android/goggles/GogglesController;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/GogglesSettings;->setTorch(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTextEdited(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Ljava/lang/CharSequence;

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/TraceTracker;->newSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventTextRefinement()V

    iget-object v0, p0, Lcom/google/android/goggles/GogglesController;->mOneShotQueryManager:Lcom/google/android/goggles/network/OneShotQueryManager;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesController;->mGogglesSettings:Lcom/google/android/goggles/GogglesSettings;

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->getServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/goggles/network/OneShotQueryManager;->query(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void
.end method

.method public pause(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/goggles/GogglesController;->cancelRecognition()V

    return-void
.end method

.method public resume()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/goggles/GogglesController;->startRecognition()V

    return-void
.end method
