.class Lcom/google/android/goggles/GogglesDebugPreferenceController$1;
.super Ljava/lang/Object;
.source "GogglesDebugPreferenceController.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/GogglesDebugPreferenceController;->handleCustomFrontend(Landroid/preference/ListPreference;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/GogglesDebugPreferenceController;

.field final synthetic val$p:Landroid/preference/ListPreference;

.field final synthetic val$view:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/GogglesDebugPreferenceController;Landroid/widget/EditText;Landroid/preference/ListPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;->this$0:Lcom/google/android/goggles/GogglesDebugPreferenceController;

    iput-object p2, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;->val$view:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;->val$p:Landroid/preference/ListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;->val$view:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;->this$0:Lcom/google/android/goggles/GogglesDebugPreferenceController;

    # getter for: Lcom/google/android/goggles/GogglesDebugPreferenceController;->mSettings:Lcom/google/android/goggles/GogglesSettings;
    invoke-static {v1}, Lcom/google/android/goggles/GogglesDebugPreferenceController;->access$000(Lcom/google/android/goggles/GogglesDebugPreferenceController;)Lcom/google/android/goggles/GogglesSettings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/GogglesSettings;->setServerInfo(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;->this$0:Lcom/google/android/goggles/GogglesDebugPreferenceController;

    iget-object v2, p0, Lcom/google/android/goggles/GogglesDebugPreferenceController$1;->val$p:Landroid/preference/ListPreference;

    # invokes: Lcom/google/android/goggles/GogglesDebugPreferenceController;->handleFrontend(Landroid/preference/ListPreference;)V
    invoke-static {v1, v2}, Lcom/google/android/goggles/GogglesDebugPreferenceController;->access$100(Lcom/google/android/goggles/GogglesDebugPreferenceController;Landroid/preference/ListPreference;)V

    return-void
.end method
