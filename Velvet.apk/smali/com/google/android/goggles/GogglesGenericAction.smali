.class public Lcom/google/android/goggles/GogglesGenericAction;
.super Ljava/lang/Object;
.source "GogglesGenericAction.java"


# instance fields
.field public final icon:I

.field public final label:Ljava/lang/String;

.field public final url:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/Uri;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/goggles/GogglesGenericAction;->label:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/goggles/GogglesGenericAction;->url:Landroid/net/Uri;

    iput p3, p0, Lcom/google/android/goggles/GogglesGenericAction;->icon:I

    return-void
.end method
