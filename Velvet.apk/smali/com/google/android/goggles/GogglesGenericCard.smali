.class public Lcom/google/android/goggles/GogglesGenericCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "GogglesGenericCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/GogglesGenericController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;"
    }
.end annotation


# instance fields
.field private final downloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

.field private mActionContainer:Landroid/widget/LinearLayout;

.field private mFifeImageUrl:Ljava/lang/String;

.field private mImageSet:Z

.field private mImageView:Lcom/google/android/velvet/ui/WebImageView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mSessionId:Ljava/lang/String;

.field private mSubtitleView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/goggles/GogglesGenericCard$1;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/GogglesGenericCard$1;-><init>(Lcom/google/android/goggles/GogglesGenericCard;)V

    iput-object v0, p0, Lcom/google/android/goggles/GogglesGenericCard;->downloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/GogglesGenericCard;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesGenericCard;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesGenericCard;->mFifeImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/goggles/GogglesGenericCard;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/goggles/GogglesGenericCard;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesGenericCard;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method private appendFifeThumbnailSizes(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s=w%d-h%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public hideImage()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/goggles/GogglesGenericCard;->mFifeImageUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesGenericCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    return-void
.end method

.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    iput-object p2, p0, Lcom/google/android/goggles/GogglesGenericCard;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040047

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1000fd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    const v1, 0x7f1000fe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mTitleView:Landroid/widget/TextView;

    const v1, 0x7f1000ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mSubtitleView:Landroid/widget/TextView;

    const v1, 0x7f100100

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mActionContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    iget-object v2, p0, Lcom/google/android/goggles/GogglesGenericCard;->downloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/WebImageView;->setOnDownloadListener(Lcom/google/android/velvet/ui/WebImageView$Listener;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->onLayout(ZIIII)V

    iget-boolean v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mImageSet:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mFifeImageUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mImageSet:Z

    iget-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mFifeImageUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/goggles/GogglesGenericCard;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/google/android/goggles/GogglesGenericCard;->appendFifeThumbnailSizes(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public setActions(Ljava/util/List;Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;)V
    .locals 10
    .param p2    # Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/goggles/GogglesGenericAction;",
            ">;",
            "Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;",
            ")V"
        }
    .end annotation

    new-instance v6, Lcom/google/android/goggles/GogglesGenericCard$2;

    invoke-direct {v6, p0, p2}, Lcom/google/android/goggles/GogglesGenericCard$2;-><init>(Lcom/google/android/goggles/GogglesGenericCard;Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;)V

    iget-object v7, p0, Lcom/google/android/goggles/GogglesGenericCard;->mActionContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/goggles/GogglesGenericAction;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/GogglesGenericAction;

    iget-object v7, p0, Lcom/google/android/goggles/GogglesGenericCard;->mInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f040048

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v7, 0x7f100102

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v7, v0, Lcom/google/android/goggles/GogglesGenericAction;->label:Ljava/lang/String;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, v0, Lcom/google/android/goggles/GogglesGenericAction;->url:Landroid/net/Uri;

    invoke-virtual {v3, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v7, 0x7f100101

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v7, v0, Lcom/google/android/goggles/GogglesGenericAction;->icon:I

    packed-switch v7, :pswitch_data_0

    const v7, 0x7f020089

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    if-ne v5, v0, :cond_0

    const v7, 0x7f100066

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/goggles/GogglesGenericCard;->mActionContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :pswitch_0
    const v7, 0x7f02003a

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setFifeImageUrl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mFifeImageUrl:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/GogglesGenericCard;->mImageSet:Z

    return-void
.end method

.method public setSessionId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/goggles/GogglesGenericCard;->mSessionId:Ljava/lang/String;

    return-void
.end method

.method public setSubtitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesGenericCard;->mSubtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/GogglesGenericCard;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
