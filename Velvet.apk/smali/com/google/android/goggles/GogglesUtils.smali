.class public Lcom/google/android/goggles/GogglesUtils;
.super Ljava/lang/Object;
.source "GogglesUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deviceSupportsBarcoding()Z
    .locals 2

    const/4 v0, 0x1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static deviceSupportsGoggles(Lcom/google/android/velvet/VelvetApplication;)Z
    .locals 6
    .param p0    # Lcom/google/android/velvet/VelvetApplication;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/goggles/GogglesSettings;->shouldShowDebugView()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const-string v4, "en"

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/goggles/GogglesUtils;->getLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getDeviceCapabilityManager()Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->hasRearFacingCamera()Z

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->isGogglesEnabled()Z

    move-result v0

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    if-nez v0, :cond_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static getLanguage(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
