.class Lcom/google/android/goggles/QueryFrameProcessor$1;
.super Ljava/lang/Object;
.source "QueryFrameProcessor.java"

# interfaces
.implements Lcom/google/android/goggles/network/QueryManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/QueryFrameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/QueryFrameProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/QueryFrameProcessor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    const/4 v2, -0x1

    # setter for: Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I
    invoke-static {v0, v2}, Lcom/google/android/goggles/QueryFrameProcessor;->access$202(Lcom/google/android/goggles/QueryFrameProcessor;I)I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mFakeResponseList:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$000(Lcom/google/android/goggles/QueryFrameProcessor;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onConnected()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onConnectionError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onConnectionError()V

    return-void
.end method

.method public onDisconnected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onDisconnected()V

    return-void
.end method

.method public onError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onError()V

    return-void
.end method

.method public onNewSession(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onNewSession(Ljava/lang/String;)V

    return-void
.end method

.method public onQuerySent(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onQuerySent(I)V

    iget-object v1, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mQueryAllowed:I
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$200(Lcom/google/android/goggles/QueryFrameProcessor;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/goggles/QueryFrameProcessor;->checkAndClear(I)Z
    invoke-static {v0, v2}, Lcom/google/android/goggles/QueryFrameProcessor;->access$300(Lcom/google/android/goggles/QueryFrameProcessor;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    invoke-static {v0}, Lcom/google/android/goggles/QueryFrameProcessor;->access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onImageFetchingDone()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    const/4 v2, 0x2

    # invokes: Lcom/google/android/goggles/QueryFrameProcessor;->checkAndClear(I)Z
    invoke-static {v0, v2}, Lcom/google/android/goggles/QueryFrameProcessor;->access$300(Lcom/google/android/goggles/QueryFrameProcessor;I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onReconnected()V
    .locals 0

    return-void
.end method

.method public onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V
    .locals 3
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mFakeResponseList:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/goggles/QueryFrameProcessor;->access$000(Lcom/google/android/goggles/QueryFrameProcessor;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->getHighestSequenceNumberReceived()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->setHighestSequenceNumberMatched(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {p1, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;->addResults(Lcom/google/bionics/goggles/api2/GogglesProtos$Result;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/goggles/QueryFrameProcessor$1;->this$0:Lcom/google/android/goggles/QueryFrameProcessor;

    # getter for: Lcom/google/android/goggles/QueryFrameProcessor;->mEventListener:Lcom/google/android/goggles/QueryFrameProcessor$EventListener;
    invoke-static {v2}, Lcom/google/android/goggles/QueryFrameProcessor;->access$100(Lcom/google/android/goggles/QueryFrameProcessor;)Lcom/google/android/goggles/QueryFrameProcessor$EventListener;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/goggles/QueryFrameProcessor$EventListener;->onResponse(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamResponse;)V

    return-void
.end method
