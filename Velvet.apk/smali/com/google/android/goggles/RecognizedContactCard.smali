.class public Lcom/google/android/goggles/RecognizedContactCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "RecognizedContactCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/RecognizedContactController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/RecognizedContactController$Ui;"
    }
.end annotation


# static fields
.field private static final ADDRESS_LISTENER:Landroid/view/View$OnClickListener;

.field private static final CALL_LISTENER:Landroid/view/View$OnClickListener;

.field private static final EMAIL_LISTENER:Landroid/view/View$OnClickListener;

.field private static final SMS_LISTENER:Landroid/view/View$OnClickListener;

.field private static final VIEW_URL_LISTENER:Landroid/view/View$OnClickListener;


# instance fields
.field private mActionContainer:Landroid/view/ViewGroup;

.field private mCompanyView:Landroid/widget/TextView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mListDataContainer:Landroid/view/ViewGroup;

.field private mNameView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/goggles/RecognizedContactCard$1;

    invoke-direct {v0}, Lcom/google/android/goggles/RecognizedContactCard$1;-><init>()V

    sput-object v0, Lcom/google/android/goggles/RecognizedContactCard;->VIEW_URL_LISTENER:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/goggles/RecognizedContactCard$2;

    invoke-direct {v0}, Lcom/google/android/goggles/RecognizedContactCard$2;-><init>()V

    sput-object v0, Lcom/google/android/goggles/RecognizedContactCard;->ADDRESS_LISTENER:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/goggles/RecognizedContactCard$3;

    invoke-direct {v0}, Lcom/google/android/goggles/RecognizedContactCard$3;-><init>()V

    sput-object v0, Lcom/google/android/goggles/RecognizedContactCard;->EMAIL_LISTENER:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/goggles/RecognizedContactCard$4;

    invoke-direct {v0}, Lcom/google/android/goggles/RecognizedContactCard$4;-><init>()V

    sput-object v0, Lcom/google/android/goggles/RecognizedContactCard;->CALL_LISTENER:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/goggles/RecognizedContactCard$5;

    invoke-direct {v0}, Lcom/google/android/goggles/RecognizedContactCard$5;-><init>()V

    sput-object v0, Lcom/google/android/goggles/RecognizedContactCard;->SMS_LISTENER:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private createAction(IILandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/google/android/goggles/RecognizedContactCard;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0400a2

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f100101

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    const v3, 0x7f100102

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(I)V

    return-object v2
.end method

.method private createListDataView(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;ILandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/view/View$OnClickListener;
    .param p4    # I
    .param p5    # Landroid/view/View$OnClickListener;

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/goggles/RecognizedContactCard;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0400a3

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v3, 0x7f10009b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p4, :cond_0

    const v3, 0x7f1001fa

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f1001e2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object v2
.end method

.method private populateActions()V
    .locals 6

    new-instance v3, Lcom/google/android/goggles/RecognizedContactCard$6;

    invoke-direct {v3, p0}, Lcom/google/android/goggles/RecognizedContactCard$6;-><init>(Lcom/google/android/goggles/RecognizedContactCard;)V

    new-instance v1, Lcom/google/android/goggles/RecognizedContactCard$7;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/RecognizedContactCard$7;-><init>(Lcom/google/android/goggles/RecognizedContactCard;)V

    const v4, 0x7f020048

    const v5, 0x7f0d0046

    invoke-direct {p0, v4, v5, v3}, Lcom/google/android/goggles/RecognizedContactCard;->createAction(IILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    const v4, 0x7f020034

    const v5, 0x7f0d0048

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/goggles/RecognizedContactCard;->createAction(IILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/goggles/RecognizedContactCard;->mActionContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/goggles/RecognizedContactCard;->mActionContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f0400a1

    const/4 v2, 0x0

    invoke-virtual {p2, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100061

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard;->mNameView:Landroid/widget/TextView;

    const v1, 0x7f1001f6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard;->mTitleView:Landroid/widget/TextView;

    const v1, 0x7f1001f7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard;->mCompanyView:Landroid/widget/TextView;

    const v1, 0x7f1001f9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard;->mActionContainer:Landroid/view/ViewGroup;

    const v1, 0x7f1001f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard;->mListDataContainer:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/google/android/goggles/RecognizedContactCard;->mInflater:Landroid/view/LayoutInflater;

    invoke-direct {p0}, Lcom/google/android/goggles/RecognizedContactCard;->populateActions()V

    return-object v0
.end method

.method public setCompany(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard;->mCompanyView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/goggles/RecognizedContactCard;->mCompanyView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListData(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/goggles/RecognizedContactCard;->mListDataContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;

    iget-object v6, p0, Lcom/google/android/goggles/RecognizedContactCard;->mListDataContainer:Landroid/view/ViewGroup;

    invoke-virtual {v11}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;->getFull()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PhoneNumber;->getFull()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/goggles/RecognizedContactCard;->CALL_LISTENER:Landroid/view/View$OnClickListener;

    const v4, 0x7f020080

    sget-object v5, Lcom/google/android/goggles/RecognizedContactCard;->SMS_LISTENER:Landroid/view/View$OnClickListener;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/RecognizedContactCard;->createListDataView(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;ILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;

    iget-object v6, p0, Lcom/google/android/goggles/RecognizedContactCard;->mListDataContainer:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$Email;->getAddress()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/goggles/RecognizedContactCard;->EMAIL_LISTENER:Landroid/view/View$OnClickListener;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/RecognizedContactCard;->createListDataView(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;ILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/goggles/RecognizedContactCard;->mListDataContainer:Landroid/view/ViewGroup;

    sget-object v3, Lcom/google/android/goggles/RecognizedContactCard;->VIEW_URL_LISTENER:Landroid/view/View$OnClickListener;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/RecognizedContactCard;->createListDataView(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;ILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_2
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;

    iget-object v0, p0, Lcom/google/android/goggles/RecognizedContactCard;->mListDataContainer:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;->getFull()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/google/bionics/goggles/api2/GogglesStructuredResponseProtos$RecognizedContact$PostalAddress;->getFull()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/goggles/RecognizedContactCard;->ADDRESS_LISTENER:Landroid/view/View$OnClickListener;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/goggles/RecognizedContactCard;->createListDataView(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;ILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_3

    :cond_3
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard;->mNameView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/goggles/RecognizedContactCard;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/goggles/RecognizedContactCard;->mTitleView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/goggles/RecognizedContactCard;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
