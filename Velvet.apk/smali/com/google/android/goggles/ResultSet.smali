.class public Lcom/google/android/goggles/ResultSet;
.super Ljava/lang/Object;
.source "ResultSet.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/goggles/ResultSet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAllResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;"
        }
    .end annotation
.end field

.field private mResultSetNumber:I

.field private mSceneAt:I

.field private mSceneResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/goggles/ResultSet$1;

    invoke-direct {v0}, Lcom/google/android/goggles/ResultSet$1;-><init>()V

    sput-object v0, Lcom/google/android/goggles/ResultSet;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/goggles/ResultSet;->mAllResults:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneResults:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/goggles/ResultSet;->mResultSetNumber:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneAt:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/goggles/ResultSet;->mResultSetNumber:I

    invoke-static {p1}, Lcom/google/android/goggles/ResultSet;->readResultsFromParcel(Landroid/os/Parcel;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ResultSet;->mAllResults:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/goggles/ResultSet;->readResultsFromParcel(Landroid/os/Parcel;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneResults:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneAt:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSessionId:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/goggles/ResultSet$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/google/android/goggles/ResultSet$1;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/ResultSet;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private static readResultsFromParcel(Landroid/os/Parcel;)Ljava/util/ArrayList;
    .locals 7
    .param p0    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    new-instance v4, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-direct {v4}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;-><init>()V

    :try_start_0
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v5, "goggles.ResultSet"

    const-string v6, "Error restoring resultset"

    invoke-static {v5, v6, v1}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    return-object v2
.end method

.method private static writeResultsToParcel(Landroid/os/Parcel;Ljava/util/List;)V
    .locals 3
    .param p0    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/goggles/ResultSet;->mAllResults:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneAt:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/goggles/ResultSet;->mResultSetNumber:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSessionId:Ljava/lang/String;

    return-void
.end method

.method public declared-synchronized copy()Lcom/google/android/goggles/ResultSet;
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/goggles/ResultSet;

    invoke-direct {v0}, Lcom/google/android/goggles/ResultSet;-><init>()V

    iget-object v1, p0, Lcom/google/android/goggles/ResultSet;->mSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ResultSet;->setSessionId(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/goggles/ResultSet;->mResultSetNumber:I

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/goggles/ResultSet;->mAllResults:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget v3, p0, Lcom/google/android/goggles/ResultSet;->mSceneAt:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/goggles/ResultSet;->update(ILjava/util/List;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public get(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mAllResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    return-object v0
.end method

.method public getResultSetNumber()I
    .locals 1

    iget v0, p0, Lcom/google/android/goggles/ResultSet;->mResultSetNumber:I

    return v0
.end method

.method public getSceneResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneResults:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public hasSceneResults()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOutOfScene(I)Z
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/goggles/ResultSet;->get(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getHighestSequenceNumberMatched()I

    move-result v0

    iget v1, p0, Lcom/google/android/goggles/ResultSet;->mSceneAt:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setSessionId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/goggles/ResultSet;->mSessionId:Ljava/lang/String;

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mAllResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method declared-synchronized update(ILjava/util/List;I)V
    .locals 1
    .param p1    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$Result;",
            ">;I)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/goggles/ResultSet;->mResultSetNumber:I

    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ResultSet;->mAllResults:Ljava/util/List;

    iput p3, p0, Lcom/google/android/goggles/ResultSet;->mSceneAt:I

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneResults:Ljava/util/ArrayList;

    invoke-static {v0, p2, p3}, Lcom/google/android/goggles/ResultRanker;->filterResults(Ljava/util/List;Ljava/util/List;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/google/android/goggles/ResultSet;->mResultSetNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mAllResults:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/android/goggles/ResultSet;->writeResultsToParcel(Landroid/os/Parcel;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneResults:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lcom/google/android/goggles/ResultSet;->writeResultsToParcel(Landroid/os/Parcel;Ljava/util/List;)V

    iget v0, p0, Lcom/google/android/goggles/ResultSet;->mSceneAt:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ResultSet;->mSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
