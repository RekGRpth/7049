.class public Lcom/google/android/goggles/SceneFrameProcessor;
.super Lcom/google/android/goggles/camera/FrameProcessor;
.source "SceneFrameProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/SceneFrameProcessor$Listener;
    }
.end annotation


# instance fields
.field private final mAccumulatedMatrix:Landroid/graphics/Matrix;

.field private final mDstPoint:[F

.field private final mInvertedMatrix:Landroid/graphics/Matrix;

.field private final mListener:Lcom/google/android/goggles/SceneFrameProcessor$Listener;

.field private final mSrcPoint:[F

.field private final mTempMatrix:Landroid/graphics/Matrix;

.field private mVisionGyro:Lcom/google/android/goggles/VisionGyro;


# direct methods
.method public constructor <init>(Lcom/google/android/goggles/SceneFrameProcessor$Listener;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/SceneFrameProcessor$Listener;

    const/4 v1, 0x2

    const-string v0, "VisionGyro processor"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FrameProcessor;-><init>(Ljava/lang/String;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mAccumulatedMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mInvertedMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mTempMatrix:Landroid/graphics/Matrix;

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mSrcPoint:[F

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mDstPoint:[F

    iput-object p1, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mListener:Lcom/google/android/goggles/SceneFrameProcessor$Listener;

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mDstPoint:[F

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->visionGyroVector:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mVisionGyro:Lcom/google/android/goggles/VisionGyro;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mVisionGyro:Lcom/google/android/goggles/VisionGyro;

    invoke-virtual {v0}, Lcom/google/android/goggles/VisionGyro;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mVisionGyro:Lcom/google/android/goggles/VisionGyro;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized onBackgroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V
    .locals 6
    .param p1    # Lcom/google/android/goggles/camera/PreviewFrame;

    const/high16 v5, 0x3f000000

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mVisionGyro:Lcom/google/android/goggles/VisionGyro;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mVisionGyro:Lcom/google/android/goggles/VisionGyro;

    iget-object v1, p1, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    iget-object v2, p1, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v2, v2, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v3, p1, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    iget-object v4, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/goggles/VisionGyro;->nextFrame([BIILandroid/graphics/Matrix;)V

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mAccumulatedMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mAccumulatedMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mDstPoint:[F

    iget-object v2, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mSrcPoint:[F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mInvertedMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mDstPoint:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mDstPoint:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p1, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v1, v1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    int-to-float v1, v1

    mul-float/2addr v1, v5

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mDstPoint:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p1, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v1, v1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    int-to-float v1, v1

    mul-float/2addr v1, v5

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/TraceTracker;->getLatestSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/TraceTracker$Session;->addClientEventSceneChangeDetected()V

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mListener:Lcom/google/android/goggles/SceneFrameProcessor$Listener;

    invoke-interface {v0}, Lcom/google/android/goggles/SceneFrameProcessor$Listener;->onNewScene()V

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mAccumulatedMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mInvertedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/goggles/ui/DebugView;->visionGyroThresholdCrossed:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v0, 0x0

    :try_start_2
    sput-boolean v0, Lcom/google/android/goggles/ui/DebugView;->visionGyroThresholdCrossed:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized open()V
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/goggles/VisionGyro;

    invoke-direct {v0}, Lcom/google/android/goggles/VisionGyro;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mVisionGyro:Lcom/google/android/goggles/VisionGyro;

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mAccumulatedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mInvertedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lcom/google/android/goggles/SceneFrameProcessor;->mListener:Lcom/google/android/goggles/SceneFrameProcessor$Listener;

    invoke-interface {v0}, Lcom/google/android/goggles/SceneFrameProcessor$Listener;->onNewScene()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
