.class public Lcom/google/android/goggles/TraceTracker$NoOpSession;
.super Ljava/lang/Object;
.source "TraceTracker.java"

# interfaces
.implements Lcom/google/android/goggles/TraceTracker$Session;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/TraceTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoOpSession"
.end annotation


# instance fields
.field private impression:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    invoke-direct {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/TraceTracker$NoOpSession;->impression:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    return-void
.end method


# virtual methods
.method public addClientEventLocalBarcodeDetected()V
    .locals 0

    return-void
.end method

.method public addClientEventRecvResponse(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public addClientEventSceneChangeDetected()V
    .locals 0

    return-void
.end method

.method public addClientEventSendRequest(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public addClientEventThumbnailGet(Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    return-void
.end method

.method public addImpressionDisambiguation(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$NoOpSession;->impression:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    return-object v0
.end method

.method public addImpressionSingleResult(ILjava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$NoOpSession;->impression:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    return-object v0
.end method

.method public addUserEventDisambigClick(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public addUserEventRequestResults()V
    .locals 0

    return-void
.end method

.method public addUserEventStartSearch(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public addUserEventTextRefinement()V
    .locals 0

    return-void
.end method

.method public build()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public end()V
    .locals 0

    return-void
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker$NoOpSession;->impression:Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    invoke-virtual {v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;->clear()Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    return-void
.end method

.method public setSessionId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method
