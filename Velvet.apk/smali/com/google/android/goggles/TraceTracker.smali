.class public Lcom/google/android/goggles/TraceTracker;
.super Ljava/lang/Object;
.source "TraceTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/TraceTracker$1;,
        Lcom/google/android/goggles/TraceTracker$RealSession;,
        Lcom/google/android/goggles/TraceTracker$NoOpSession;,
        Lcom/google/android/goggles/TraceTracker$Session;
    }
.end annotation


# static fields
.field private static final NO_OP_SESSION:Lcom/google/android/goggles/TraceTracker$NoOpSession;

.field private static sTraceTracker:Lcom/google/android/goggles/TraceTracker;


# instance fields
.field private final mActiveSessions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/goggles/TraceTracker$Session;",
            ">;"
        }
    .end annotation
.end field

.field private final mClosedSessions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/goggles/TraceTracker$Session;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/goggles/TraceTracker$NoOpSession;

    invoke-direct {v0}, Lcom/google/android/goggles/TraceTracker$NoOpSession;-><init>()V

    sput-object v0, Lcom/google/android/goggles/TraceTracker;->NO_OP_SESSION:Lcom/google/android/goggles/TraceTracker$NoOpSession;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/TraceTracker;->mClosedSessions:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/goggles/TraceTracker;Lcom/google/android/goggles/TraceTracker$Session;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/TraceTracker;
    .param p1    # Lcom/google/android/goggles/TraceTracker$Session;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/TraceTracker;->endSession(Lcom/google/android/goggles/TraceTracker$Session;)V

    return-void
.end method

.method private declared-synchronized endSession(Lcom/google/android/goggles/TraceTracker$Session;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/TraceTracker$Session;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/goggles/TraceTracker;->mClosedSessions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static final getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;
    .locals 1

    sget-object v0, Lcom/google/android/goggles/TraceTracker;->sTraceTracker:Lcom/google/android/goggles/TraceTracker;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/goggles/TraceTracker;

    invoke-direct {v0}, Lcom/google/android/goggles/TraceTracker;-><init>()V

    sput-object v0, Lcom/google/android/goggles/TraceTracker;->sTraceTracker:Lcom/google/android/goggles/TraceTracker;

    :cond_0
    sget-object v0, Lcom/google/android/goggles/TraceTracker;->sTraceTracker:Lcom/google/android/goggles/TraceTracker;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized buildAndClear()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/google/android/goggles/TraceTracker;->mClosedSessions:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/goggles/TraceTracker$Session;

    invoke-interface {v3}, Lcom/google/android/goggles/TraceTracker$Session;->build()Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v4, "goggles.TraceTracker"

    const-string v5, "Session is closed but no session id. Ignored."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_0
    :try_start_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/goggles/TraceTracker;->mClosedSessions:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v2
.end method

.method public declared-synchronized closeAllSessions()Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/goggles/TraceTracker;->mClosedSessions:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getLatestSession()Lcom/google/android/goggles/TraceTracker$Session;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/goggles/TraceTracker;->NO_OP_SESSION:Lcom/google/android/goggles/TraceTracker$NoOpSession;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/goggles/TraceTracker$Session;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getSession(Ljava/lang/String;)Lcom/google/android/goggles/TraceTracker$Session;
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/google/android/goggles/TraceTracker;->NO_OP_SESSION:Lcom/google/android/goggles/TraceTracker$NoOpSession;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/goggles/TraceTracker$Session;

    invoke-interface {v1}, Lcom/google/android/goggles/TraceTracker$Session;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/goggles/TraceTracker;->NO_OP_SESSION:Lcom/google/android/goggles/TraceTracker$NoOpSession;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public newSession()Lcom/google/android/goggles/TraceTracker$Session;
    .locals 3

    sget-object v2, Lcom/google/android/goggles/TraceTracker;->NO_OP_SESSION:Lcom/google/android/goggles/TraceTracker$NoOpSession;

    invoke-virtual {v2}, Lcom/google/android/goggles/TraceTracker$NoOpSession;->reset()V

    new-instance v1, Lcom/google/android/goggles/TraceTracker$RealSession;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/goggles/TraceTracker$RealSession;-><init>(Lcom/google/android/goggles/TraceTracker;Lcom/google/android/goggles/TraceTracker$1;)V

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/goggles/TraceTracker;->getLatestSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/goggles/TraceTracker$Session;->end()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/goggles/TraceTracker;->mActiveSessions:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
