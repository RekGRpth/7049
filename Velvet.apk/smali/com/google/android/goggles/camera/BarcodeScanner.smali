.class public Lcom/google/android/goggles/camera/BarcodeScanner;
.super Lcom/google/android/goggles/camera/FrameProcessor;
.source "BarcodeScanner.java"


# instance fields
.field private mDetectedBarcode:Lcom/google/android/goggles/Barcode;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Client barcode detector"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FrameProcessor;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/goggles/Barcode;

    invoke-direct {v0}, Lcom/google/android/goggles/Barcode;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/camera/BarcodeScanner;->mDetectedBarcode:Lcom/google/android/goggles/Barcode;

    iget-object v0, p0, Lcom/google/android/goggles/camera/BarcodeScanner;->mDetectedBarcode:Lcom/google/android/goggles/Barcode;

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->barcode:Lcom/google/android/goggles/Barcode;

    return-void
.end method

.method private static native barhopperDetect([BIILcom/google/android/goggles/Barcode;)Z
.end method


# virtual methods
.method protected declared-synchronized onBackgroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V
    .locals 6
    .param p1    # Lcom/google/android/goggles/camera/PreviewFrame;

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p1, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    iget-object v3, p1, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v4, p1, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v4, v4, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    iget-object v5, p0, Lcom/google/android/goggles/camera/BarcodeScanner;->mDetectedBarcode:Lcom/google/android/goggles/Barcode;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/goggles/camera/BarcodeScanner;->barhopperDetect([BIILcom/google/android/goggles/Barcode;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/goggles/camera/BarcodeScanner;->mDetectedBarcode:Lcom/google/android/goggles/Barcode;

    iput-object v2, p1, Lcom/google/android/goggles/camera/PreviewFrame;->newBarcode:Lcom/google/android/goggles/Barcode;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/goggles/ui/DebugView;->newBarcodeTimestamp:J

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/goggles/TraceTracker;->getLatestSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/goggles/TraceTracker$Session;->addClientEventLocalBarcodeDetected()V

    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    sput-wide v2, Lcom/google/android/goggles/ui/DebugView;->barcodeProcessTimeMs:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, p1, Lcom/google/android/goggles/camera/PreviewFrame;->newBarcode:Lcom/google/android/goggles/Barcode;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
