.class public Lcom/google/android/goggles/camera/CameraManager$Size;
.super Ljava/lang/Object;
.source "CameraManager.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/CameraManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Size"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/goggles/camera/CameraManager$Size;",
        ">;"
    }
.end annotation


# instance fields
.field public final height:I

.field public final width:I


# direct methods
.method constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iput p2, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/goggles/camera/CameraManager$Size;)I
    .locals 4
    .param p1    # Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v2, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget v3, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    mul-int v1, v2, v3

    iget v2, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget v3, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    mul-int v0, v2, v3

    sub-int v2, v1, v0

    return v2
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {p0, p1}, Lcom/google/android/goggles/camera/CameraManager$Size;->compareTo(Lcom/google/android/goggles/camera/CameraManager$Size;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Lcom/google/android/goggles/camera/CameraManager$Size;

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget v4, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    iget v4, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "[%d, %d]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
