.class Lcom/google/android/goggles/camera/CameraService$10;
.super Ljava/lang/Object;
.source "CameraService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraService;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraService;

.field final synthetic val$callback:Landroid/hardware/Camera$AutoFocusMoveCallback;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/CameraService;Landroid/hardware/Camera$AutoFocusMoveCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraService$10;->this$0:Lcom/google/android/goggles/camera/CameraService;

    iput-object p2, p0, Lcom/google/android/goggles/camera/CameraService$10;->val$callback:Landroid/hardware/Camera$AutoFocusMoveCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$10;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraService;->access$800(Lcom/google/android/goggles/camera/CameraService;)Landroid/hardware/Camera;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$10;->this$0:Lcom/google/android/goggles/camera/CameraService;

    const-string v1, "setAutoFocusMoveCallback skipped because camera is null."

    # invokes: Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/CameraService;->access$900(Lcom/google/android/goggles/camera/CameraService;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$10;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraService;->access$800(Lcom/google/android/goggles/camera/CameraService;)Landroid/hardware/Camera;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService$10;->val$callback:Landroid/hardware/Camera$AutoFocusMoveCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    goto :goto_0
.end method
