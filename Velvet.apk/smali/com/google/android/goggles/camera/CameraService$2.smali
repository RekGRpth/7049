.class Lcom/google/android/goggles/camera/CameraService$2;
.super Ljava/lang/Object;
.source "CameraService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraService;->openCamera(Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraService;

.field final synthetic val$callback:Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/CameraService;Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraService$2;->this$0:Lcom/google/android/goggles/camera/CameraService;

    iput-object p2, p0, Lcom/google/android/goggles/camera/CameraService$2;->val$callback:Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$2;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # invokes: Lcom/google/android/goggles/camera/CameraService;->openCameraInBackground()Z
    invoke-static {v2}, Lcom/google/android/goggles/camera/CameraService;->access$000(Lcom/google/android/goggles/camera/CameraService;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$2;->val$callback:Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$2;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mMainHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/goggles/camera/CameraService;->access$100(Lcom/google/android/goggles/camera/CameraService;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/google/android/goggles/camera/CameraService$2$2;

    invoke-direct {v3, p0, v1}, Lcom/google/android/goggles/camera/CameraService$2$2;-><init>(Lcom/google/android/goggles/camera/CameraService$2;Z)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "goggles.CameraService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error opening camera: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$2;->val$callback:Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService$2;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mMainHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/goggles/camera/CameraService;->access$100(Lcom/google/android/goggles/camera/CameraService;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/google/android/goggles/camera/CameraService$2$1;

    invoke-direct {v3, p0}, Lcom/google/android/goggles/camera/CameraService$2$1;-><init>(Lcom/google/android/goggles/camera/CameraService$2;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
