.class Lcom/google/android/goggles/camera/CameraService$5;
.super Ljava/lang/Object;
.source "CameraService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/CameraService;->startPreview(Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/CameraService;

.field final synthetic val$callback:Ljava/lang/Runnable;

.field final synthetic val$preview:Landroid/graphics/SurfaceTexture;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/CameraService;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraService$5;->this$0:Lcom/google/android/goggles/camera/CameraService;

    iput-object p2, p0, Lcom/google/android/goggles/camera/CameraService$5;->val$preview:Landroid/graphics/SurfaceTexture;

    iput-object p3, p0, Lcom/google/android/goggles/camera/CameraService$5;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$5;->this$0:Lcom/google/android/goggles/camera/CameraService;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService$5;->val$preview:Landroid/graphics/SurfaceTexture;

    # invokes: Lcom/google/android/goggles/camera/CameraService;->startPreviewInBackground(Landroid/graphics/SurfaceTexture;)Z
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/CameraService;->access$500(Lcom/google/android/goggles/camera/CameraService;Landroid/graphics/SurfaceTexture;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$5;->val$callback:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService$5;->this$0:Lcom/google/android/goggles/camera/CameraService;

    # getter for: Lcom/google/android/goggles/camera/CameraService;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/goggles/camera/CameraService;->access$100(Lcom/google/android/goggles/camera/CameraService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService$5;->val$callback:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
