.class Lcom/google/android/goggles/camera/CameraService;
.super Ljava/lang/Object;
.source "CameraService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;
    }
.end annotation


# instance fields
.field private mBackgroundThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

.field private volatile mCachedParameters:Landroid/hardware/Camera$Parameters;

.field private volatile mCamera:Landroid/hardware/Camera;

.field private mCameraPreviewing:Z

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mMainHandler:Landroid/os/Handler;

.field private volatile mParameters:Lcom/google/android/goggles/camera/CameraParameters;

.field private mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/goggles/camera/CameraParameters;

    invoke-direct {v0}, Lcom/google/android/goggles/camera/CameraParameters;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    new-instance v0, Lcom/google/android/goggles/camera/CameraService$1;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/camera/CameraService$1;-><init>(Lcom/google/android/goggles/camera/CameraService;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check(Ljava/util/concurrent/Executor;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mBackgroundThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mMainHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/camera/CameraService;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/CameraService;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraService;->openCameraInBackground()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/goggles/camera/CameraService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/CameraService;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/goggles/camera/CameraService;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraService;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraService;->releaseCameraInBackground()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/goggles/camera/CameraService;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/CameraService;

    iget-boolean v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCameraPreviewing:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/goggles/camera/CameraService;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraService;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraService;->updateParametersInBackground()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/goggles/camera/CameraService;Landroid/graphics/SurfaceTexture;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/CameraService;
    .param p1    # Landroid/graphics/SurfaceTexture;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/camera/CameraService;->startPreviewInBackground(Landroid/graphics/SurfaceTexture;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/goggles/camera/CameraService;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraService;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraService;->stopPreviewInBackground()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/goggles/camera/CameraService;Lcom/google/android/goggles/camera/PreviewLooper;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraService;
    .param p1    # Lcom/google/android/goggles/camera/PreviewLooper;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/camera/CameraService;->installPreviewLooperInBackground(Lcom/google/android/goggles/camera/PreviewLooper;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/goggles/camera/CameraService;)Landroid/hardware/Camera;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/CameraService;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/goggles/camera/CameraService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/CameraService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    return-void
.end method

.method private cameraDebugMessage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method private installPreviewLooperInBackground(Lcom/google/android/goggles/camera/PreviewLooper;)V
    .locals 4
    .param p1    # Lcom/google/android/goggles/camera/PreviewLooper;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mBackgroundThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    const-string v0, "installPreviewLooper skipped because camera is null."

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    if-nez v0, :cond_1

    const-string v0, "installPreviewLooperInBackground()"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/goggles/camera/CameraService;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v1, v1, Lcom/google/android/goggles/camera/CameraParameters;->previewSize:Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/PreviewLooper;->setFrameSize(Lcom/google/android/goggles/camera/CameraManager$Size;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/goggles/camera/PreviewLooper;->bindToCameraService(Landroid/hardware/Camera;Lcom/google/android/goggles/camera/CameraParameters;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Can\'t reinstall looper"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private openCameraInBackground()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mBackgroundThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    const-string v1, "openingCamera skipped because camera is already open."

    invoke-direct {p0, v1}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v1, "openCameraInBackground() started."

    invoke-direct {p0, v1}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCachedParameters:Landroid/hardware/Camera$Parameters;

    new-instance v0, Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mCachedParameters:Landroid/hardware/Camera$Parameters;

    invoke-direct {v0, v1}, Lcom/google/android/goggles/camera/CameraParameters;-><init>(Landroid/hardware/Camera$Parameters;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    const-string v0, "openCameraInBackground() finished."

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private releaseCameraInBackground()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mBackgroundThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    const-string v0, "releaseCamera skipped because camera is already null."

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "releaseCameraInBackground() started."

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const-string v0, "releaseCameraInBackground() finished."

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCameraPreviewing:Z

    iput-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mCachedParameters:Landroid/hardware/Camera$Parameters;

    iput-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mPreviewLooper:Lcom/google/android/goggles/camera/PreviewLooper;

    goto :goto_0
.end method

.method private startPreviewInBackground(Landroid/graphics/SurfaceTexture;)Z
    .locals 5
    .param p1    # Landroid/graphics/SurfaceTexture;

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraService;->mBackgroundThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    if-nez v3, :cond_0

    const-string v2, "startPreview skipped because camera is null."

    invoke-direct {p0, v2}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    const-string v3, "startPreviewInBackground()"

    invoke-direct {p0, v3}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    :try_start_0
    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v3, p1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    iget-object v3, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->startPreview()V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/goggles/camera/CameraService;->mCameraPreviewing:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "goggles.CameraService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startPreview failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopPreviewInBackground()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mBackgroundThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    const-string v0, "stopPreview skipped because camera is null."

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "stopPreviewInBackground()"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCameraPreviewing:Z

    goto :goto_0
.end method

.method private updateParametersInBackground()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mBackgroundThread:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    const-string v0, "updateParameters skipped because camera is null."

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mCachedParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraParameters;->writeTo(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mCachedParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mCamera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    iget v1, v1, Lcom/google/android/goggles/camera/CameraParameters;->cameraDisplayOrientation:I

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    goto :goto_0
.end method


# virtual methods
.method autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    .locals 2
    .param p1    # Landroid/hardware/Camera$AutoFocusCallback;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/goggles/camera/CameraService$8;

    invoke-direct {v1, p0, p1}, Lcom/google/android/goggles/camera/CameraService$8;-><init>(Lcom/google/android/goggles/camera/CameraService;Landroid/hardware/Camera$AutoFocusCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method cancelAutoFocus()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/goggles/camera/CameraService$9;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/camera/CameraService$9;-><init>(Lcom/google/android/goggles/camera/CameraService;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method getParameters()Lcom/google/android/goggles/camera/CameraParameters;
    .locals 2

    new-instance v0, Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    invoke-direct {v0, v1}, Lcom/google/android/goggles/camera/CameraParameters;-><init>(Lcom/google/android/goggles/camera/CameraParameters;)V

    return-object v0
.end method

.method installPreviewLooper(Lcom/google/android/goggles/camera/PreviewLooper;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/camera/PreviewLooper;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/goggles/camera/CameraService$7;

    invoke-direct {v1, p0, p1}, Lcom/google/android/goggles/camera/CameraService$7;-><init>(Lcom/google/android/goggles/camera/CameraService;Lcom/google/android/goggles/camera/PreviewLooper;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method openCamera(Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;

    const-string v0, "openCamera()"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/goggles/camera/CameraService$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/goggles/camera/CameraService$2;-><init>(Lcom/google/android/goggles/camera/CameraService;Lcom/google/android/goggles/camera/CameraService$CameraOpenedCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method releaseCamera(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    const-string v0, "releaseCamera()"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/goggles/camera/CameraService$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/goggles/camera/CameraService$3;-><init>(Lcom/google/android/goggles/camera/CameraService;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V
    .locals 2
    .param p1    # Landroid/hardware/Camera$AutoFocusMoveCallback;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/goggles/camera/CameraService$10;

    invoke-direct {v1, p0, p1}, Lcom/google/android/goggles/camera/CameraService$10;-><init>(Lcom/google/android/goggles/camera/CameraService;Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method setParameters(Lcom/google/android/goggles/camera/CameraParameters;)V
    .locals 3
    .param p1    # Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    new-instance v1, Lcom/google/android/goggles/camera/CameraParameters;

    invoke-direct {v1, p1}, Lcom/google/android/goggles/camera/CameraParameters;-><init>(Lcom/google/android/goggles/camera/CameraParameters;)V

    iput-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mParameters:Lcom/google/android/goggles/camera/CameraParameters;

    iget-object v1, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/goggles/camera/CameraService$4;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/goggles/camera/CameraService$4;-><init>(Lcom/google/android/goggles/camera/CameraService;Lcom/google/android/goggles/camera/CameraParameters;Lcom/google/android/goggles/camera/CameraParameters;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method startPreview(Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # Ljava/lang/Runnable;

    const-string v0, "startPreview()"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/goggles/camera/CameraService$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/goggles/camera/CameraService$5;-><init>(Lcom/google/android/goggles/camera/CameraService;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method stopPreview()V
    .locals 2

    const-string v0, "stopPreview()"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/CameraService;->cameraDebugMessage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/CameraService;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/goggles/camera/CameraService$6;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/camera/CameraService$6;-><init>(Lcom/google/android/goggles/camera/CameraService;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
