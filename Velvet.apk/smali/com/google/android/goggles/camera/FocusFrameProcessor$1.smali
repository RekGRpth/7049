.class Lcom/google/android/goggles/camera/FocusFrameProcessor$1;
.super Ljava/lang/Object;
.source "FocusFrameProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/FocusFrameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$1;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$1;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/goggles/camera/FocusFrameProcessor;->onFocusMoving(Z)V
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$000(Lcom/google/android/goggles/camera/FocusFrameProcessor;Z)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$1;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$1;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$100(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/hardware/Camera$AutoFocusCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraService;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    return-void
.end method
