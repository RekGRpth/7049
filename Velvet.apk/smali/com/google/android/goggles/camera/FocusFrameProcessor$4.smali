.class Lcom/google/android/goggles/camera/FocusFrameProcessor$4;
.super Ljava/lang/Object;
.source "FocusFrameProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/FocusFrameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # invokes: Lcom/google/android/goggles/camera/FocusFrameProcessor;->hardContinuousFocusAvailable()Z
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$600(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mForceFakeFocus:Z
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$700(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/camera/CameraService;->setParameters(Lcom/google/android/goggles/camera/CameraParameters;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$500(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$400(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$500(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$400(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;
    invoke-static {v2}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$800(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/hardware/Camera$AutoFocusMoveCallback;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/camera/CameraService;->setAutoFocusMoveCallback(Landroid/hardware/Camera$AutoFocusMoveCallback;)V

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    if-nez v1, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/camera/CameraService;->setParameters(Lcom/google/android/goggles/camera/CameraParameters;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/camera/CameraService;->cancelAutoFocus()V

    goto :goto_0
.end method
