.class Lcom/google/android/goggles/camera/FocusFrameProcessor$5;
.super Ljava/lang/Object;
.source "FocusFrameProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/FocusFrameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$5;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$5;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # invokes: Lcom/google/android/goggles/camera/FocusFrameProcessor;->hardContinuousFocusAvailable()Z
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$600(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$5;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$500(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$5;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$400(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
