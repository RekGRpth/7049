.class Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;
.super Ljava/lang/Object;
.source "FocusFrameProcessor.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;

.field final synthetic val$this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;Lcom/google/android/goggles/camera/FocusFrameProcessor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;->this$1:Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;

    iput-object p2, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;->val$this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 3
    .param p1    # Z
    .param p2    # Landroid/hardware/Camera;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;->this$1:Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->access$1000(Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;->this$1:Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;

    iget-object v1, v1, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/goggles/camera/CameraParameters;->continuousAutofocus:Z

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;->this$1:Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;

    iget-object v1, v1, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/goggles/camera/CameraService;->setParameters(Lcom/google/android/goggles/camera/CameraParameters;)V

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;->this$1:Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;

    iget-object v1, v1, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v1

    new-instance v2, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2$1;

    invoke-direct {v2, p0}, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2$1;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;)V

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/camera/CameraService;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    goto :goto_0
.end method
