.class Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;
.super Ljava/lang/Object;
.source "FocusFrameProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/camera/FocusFrameProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoFocusAndStop"
.end annotation


# instance fields
.field private final mAutoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

.field private final mRunnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;


# direct methods
.method public constructor <init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;Ljava/lang/Runnable;)V
    .locals 1
    .param p2    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$1;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;Lcom/google/android/goggles/camera/FocusFrameProcessor;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->mRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop$2;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;Lcom/google/android/goggles/camera/FocusFrameProcessor;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->mAutoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # invokes: Lcom/google/android/goggles/camera/FocusFrameProcessor;->hardContinuousFocusAvailable()Z
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$600(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mIsFocusing:Z
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$1100(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->mRunnable:Ljava/lang/Runnable;

    # setter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mPendingRunnable:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$302(Lcom/google/android/goggles/camera/FocusFrameProcessor;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->mRunnable:Ljava/lang/Runnable;

    # setter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mPendingRunnable:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$302(Lcom/google/android/goggles/camera/FocusFrameProcessor;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$500(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$400(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$500(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$400(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->this$0:Lcom/google/android/goggles/camera/FocusFrameProcessor;

    # getter for: Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;
    invoke-static {v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;->mAutoFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraService;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    goto :goto_0
.end method
