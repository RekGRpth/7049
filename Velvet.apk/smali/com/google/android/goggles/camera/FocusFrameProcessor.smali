.class Lcom/google/android/goggles/camera/FocusFrameProcessor;
.super Lcom/google/android/goggles/camera/FrameProcessor;
.source "FocusFrameProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;
    }
.end annotation


# instance fields
.field private final mCameraService:Lcom/google/android/goggles/camera/CameraService;

.field private final mFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

.field private final mFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;

.field private final mFocusRunnable:Ljava/lang/Runnable;

.field private final mForceFakeFocus:Z

.field private volatile mIsFocusing:Z

.field private final mMainHandler:Landroid/os/Handler;

.field private mPendingRunnable:Ljava/lang/Runnable;

.field private final mStartRunnable:Ljava/lang/Runnable;

.field private final mStopRunnable:Ljava/lang/Runnable;


# direct methods
.method protected constructor <init>(Lcom/google/android/goggles/camera/CameraService;Z)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/camera/CameraService;
    .param p2    # Z

    const-string v0, "Focus processor"

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FrameProcessor;-><init>(Ljava/lang/String;)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/goggles/camera/FocusFrameProcessor$1;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/camera/FocusFrameProcessor$1;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/camera/FocusFrameProcessor$2;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    new-instance v0, Lcom/google/android/goggles/camera/FocusFrameProcessor$3;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/camera/FocusFrameProcessor$3;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;

    new-instance v0, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/camera/FocusFrameProcessor$4;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mStartRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/goggles/camera/FocusFrameProcessor$5;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/camera/FocusFrameProcessor$5;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mStopRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;

    iput-boolean p2, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mForceFakeFocus:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/camera/FocusFrameProcessor;Z)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->onFocusMoving(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/hardware/Camera$AutoFocusCallback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusCallback:Landroid/hardware/Camera$AutoFocusCallback;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-boolean v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mIsFocusing:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Lcom/google/android/goggles/camera/CameraService;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mPendingRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/goggles/camera/FocusFrameProcessor;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mPendingRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->hardContinuousFocusAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-boolean v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mForceFakeFocus:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Landroid/hardware/Camera$AutoFocusMoveCallback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mFocusMoveCallback:Landroid/hardware/Camera$AutoFocusMoveCallback;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/goggles/camera/FocusFrameProcessor;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/FocusFrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mStopRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private hardContinuousFocusAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mCameraService:Lcom/google/android/goggles/camera/CameraService;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraService;->getParameters()Lcom/google/android/goggles/camera/CameraParameters;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/goggles/camera/CameraParameters;->continuousPictureFocusCapable:Z

    return v0
.end method

.method private isMainThread()Z
    .locals 2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onFocusMoving(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mIsFocusing:Z

    iget-boolean v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mIsFocusing:Z

    sput-boolean v0, Lcom/google/android/goggles/ui/DebugView;->isFocusing:Z

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    return-void
.end method

.method private runInMainThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->isMainThread()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public autoFocusAndStop(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;

    invoke-direct {v0, p0, p1}, Lcom/google/android/goggles/camera/FocusFrameProcessor$AutoFocusAndStop;-><init>(Lcom/google/android/goggles/camera/FocusFrameProcessor;Ljava/lang/Runnable;)V

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->runInMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onForegroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/camera/PreviewFrame;

    iget-boolean v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mIsFocusing:Z

    iput-boolean v0, p1, Lcom/google/android/goggles/camera/PreviewFrame;->isFocusing:Z

    return-void
.end method

.method start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mStartRunnable:Ljava/lang/Runnable;

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->runInMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method stop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/camera/FocusFrameProcessor;->mStopRunnable:Ljava/lang/Runnable;

    invoke-direct {p0, v0}, Lcom/google/android/goggles/camera/FocusFrameProcessor;->runInMainThread(Ljava/lang/Runnable;)V

    return-void
.end method
