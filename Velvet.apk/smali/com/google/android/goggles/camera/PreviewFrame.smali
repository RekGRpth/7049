.class public Lcom/google/android/goggles/camera/PreviewFrame;
.super Ljava/lang/Object;
.source "PreviewFrame.java"


# instance fields
.field public isFocusing:Z

.field private mRect:Landroid/graphics/Rect;

.field private mYuv:Landroid/graphics/YuvImage;

.field public newBarcode:Lcom/google/android/goggles/Barcode;

.field public rotation:I

.field public final size:Lcom/google/android/goggles/camera/CameraManager$Size;

.field public timestamp:J

.field public final yuvData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "goggles_clientvision"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1    # Landroid/graphics/Bitmap;

    const/16 v9, 0x11

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/goggles/camera/CameraManager$Size;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/google/android/goggles/camera/CameraManager$Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v0, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    mul-int/2addr v0, v3

    invoke-static {v9}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v3

    mul-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v0, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    mul-int/2addr v0, v3

    new-array v1, v0, [I

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v6, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v7, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    move-object v0, p1

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v4, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v4, v4, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-static {v1, v0, v3, v4}, Lcom/google/android/goggles/camera/PreviewFrame;->convertARGB8888ToYuv([I[BII)V

    new-instance v3, Landroid/graphics/YuvImage;

    iget-object v4, p0, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v6, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v7, v0, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    const/4 v8, 0x0

    move v5, v9

    invoke-direct/range {v3 .. v8}, Landroid/graphics/YuvImage;-><init>([BIII[I)V

    iput-object v3, p0, Lcom/google/android/goggles/camera/PreviewFrame;->mYuv:Landroid/graphics/YuvImage;

    new-instance v0, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v4, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v4, v4, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-direct {v0, v2, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->mRect:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/goggles/camera/CameraManager$Size;)V
    .locals 7
    .param p1    # Lcom/google/android/goggles/camera/CameraManager$Size;

    const/16 v2, 0x11

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput-object p1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v0, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget v1, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    mul-int/2addr v0, v1

    invoke-static {v2}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v1

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    new-instance v0, Landroid/graphics/YuvImage;

    iget-object v1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    iget v3, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget v4, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/YuvImage;-><init>([BIII[I)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->mYuv:Landroid/graphics/YuvImage;

    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget v2, p1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    invoke-direct {v0, v6, v6, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->mRect:Landroid/graphics/Rect;

    return-void
.end method

.method static native convertARGB8888ToYuv([I[BII)V
.end method

.method static native convertYuvToRotatedARGB8888([B[IIII)V
.end method


# virtual methods
.method public toJpeg(Ljava/io/OutputStream;)Z
    .locals 3
    .param p1    # Ljava/io/OutputStream;

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewFrame;->mYuv:Landroid/graphics/YuvImage;

    iget-object v1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->mRect:Landroid/graphics/Rect;

    const/16 v2, 0x5f

    invoke-virtual {v0, v1, v2, p1}, Landroid/graphics/YuvImage;->compressToJpeg(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z

    move-result v0

    return v0
.end method

.method public toRgbBitmap()Landroid/graphics/Bitmap;
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    invoke-virtual {p0}, Lcom/google/android/goggles/camera/PreviewFrame;->toRgbBitmapInner()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public toRgbBitmapInner()Landroid/graphics/Bitmap;
    .locals 5

    iget-object v1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v1, v1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v2, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v2, v2, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    mul-int/2addr v1, v2

    new-array v0, v1, [I

    iget-object v1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    iget-object v2, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v2, v2, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v3, v3, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    iget v4, p0, Lcom/google/android/goggles/camera/PreviewFrame;->rotation:I

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/goggles/camera/PreviewFrame;->convertYuvToRotatedARGB8888([B[IIII)V

    iget v1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->rotation:I

    const/16 v2, 0x5a

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->rotation:I

    const/16 v2, 0x10e

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v1, v1, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    iget-object v2, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v2, v2, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v1, v1, Lcom/google/android/goggles/camera/CameraManager$Size;->width:I

    iget-object v2, p0, Lcom/google/android/goggles/camera/PreviewFrame;->size:Lcom/google/android/goggles/camera/CameraManager$Size;

    iget v2, v2, Lcom/google/android/goggles/camera/CameraManager$Size;->height:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method
