.class Lcom/google/android/goggles/camera/PreviewLooper;
.super Ljava/lang/Object;
.source "PreviewLooper.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/camera/PreviewLooper$1;,
        Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;
    }
.end annotation


# instance fields
.field private mBackgroundFrameProcessors:Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;

.field private mExecutor:Ljava/util/concurrent/Executor;

.field private mFps:Lcom/google/android/goggles/camera/FpsTracker;

.field private final mFrameProcessors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/goggles/camera/FrameProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mFrameProcessors:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;-><init>(Lcom/google/android/goggles/camera/PreviewLooper;Lcom/google/android/goggles/camera/PreviewLooper$1;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mBackgroundFrameProcessors:Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;

    new-instance v0, Lcom/google/android/goggles/camera/FpsTracker;

    invoke-direct {v0}, Lcom/google/android/goggles/camera/FpsTracker;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mFps:Lcom/google/android/goggles/camera/FpsTracker;

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mFps:Lcom/google/android/goggles/camera/FpsTracker;

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->fpsTracker:Lcom/google/android/goggles/camera/FpsTracker;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/goggles/camera/PreviewLooper;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/PreviewLooper;

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mFrameProcessors:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/goggles/camera/PreviewLooper;)Lcom/google/android/goggles/camera/PreviewFrame;
    .locals 1
    .param p0    # Lcom/google/android/goggles/camera/PreviewLooper;

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    return-object v0
.end method


# virtual methods
.method addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V
    .locals 2
    .param p1    # Lcom/google/android/goggles/camera/FrameProcessor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mExecutor:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t add FrameProcessor after PreviewLooper is bound to CameraService."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mFrameProcessors:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method bindToCameraService(Landroid/hardware/Camera;Lcom/google/android/goggles/camera/CameraParameters;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p1    # Landroid/hardware/Camera;
    .param p2    # Lcom/google/android/goggles/camera/CameraParameters;
    .param p3    # Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    const-string v1, "setFrameSize() must be called first."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, p0}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    iget-object v0, v0, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    iget-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    iget v1, p2, Lcom/google/android/goggles/camera/CameraParameters;->cameraDisplayOrientation:I

    rsub-int v1, v1, 0x168

    rem-int/lit16 v1, v1, 0x168

    iput v1, v0, Lcom/google/android/goggles/camera/PreviewFrame;->rotation:I

    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 6
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    iget-object v3, v3, Lcom/google/android/goggles/camera/PreviewFrame;->yuvData:[B

    if-ne p1, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/goggles/camera/PreviewFrame;->timestamp:J

    invoke-static {}, Lcom/google/android/goggles/camera/CameraManagerTestUtils;->getFakePreviewFrame()Lcom/google/android/goggles/camera/PreviewFrame;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mFrameProcessors:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/goggles/camera/FrameProcessor;

    if-eqz v0, :cond_1

    move-object v3, v0

    :goto_2
    invoke-virtual {v2, v3}, Lcom/google/android/goggles/camera/FrameProcessor;->onForegroundProcessFrame(Lcom/google/android/goggles/camera/PreviewFrame;)V

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    goto :goto_2

    :cond_2
    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mBackgroundFrameProcessors:Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;

    invoke-virtual {v3, p2}, Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;->bindCamera(Landroid/hardware/Camera;)V

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mExecutor:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mBackgroundFrameProcessors:Lcom/google/android/goggles/camera/PreviewLooper$BackgroundFrameProcessors;

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object v3, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mFps:Lcom/google/android/goggles/camera/FpsTracker;

    invoke-virtual {v3}, Lcom/google/android/goggles/camera/FpsTracker;->tick()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->postUpdate()V

    :cond_3
    return-void
.end method

.method setFrameSize(Lcom/google/android/goggles/camera/CameraManager$Size;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/camera/CameraManager$Size;

    new-instance v0, Lcom/google/android/goggles/camera/PreviewFrame;

    invoke-direct {v0, p1}, Lcom/google/android/goggles/camera/PreviewFrame;-><init>(Lcom/google/android/goggles/camera/CameraManager$Size;)V

    iput-object v0, p0, Lcom/google/android/goggles/camera/PreviewLooper;->mPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    return-void
.end method
