.class Lcom/google/android/goggles/network/OneShotQueryManager$2;
.super Ljava/lang/Object;
.source "OneShotQueryManager.java"

# interfaces
.implements Lcom/google/android/goggles/GogglesController$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/network/OneShotQueryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/network/OneShotQueryManager;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/network/OneShotQueryManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/network/OneShotQueryManager$2;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$2;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # invokes: Lcom/google/android/goggles/network/OneShotQueryManager;->closeSession()V
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$500(Lcom/google/android/goggles/network/OneShotQueryManager;)V

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$2;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mCallback:Lcom/google/android/goggles/GogglesController$Callback;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$600(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/GogglesController$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$Callback;->onError()V

    return-void
.end method

.method public onNoResult()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$2;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # invokes: Lcom/google/android/goggles/network/OneShotQueryManager;->closeSession()V
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$500(Lcom/google/android/goggles/network/OneShotQueryManager;)V

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$2;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mCallback:Lcom/google/android/goggles/GogglesController$Callback;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$600(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/GogglesController$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/GogglesController$Callback;->onNoResult()V

    return-void
.end method

.method public onResultReady(Lcom/google/android/goggles/ResultSet;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/ResultSet;

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$2;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # invokes: Lcom/google/android/goggles/network/OneShotQueryManager;->closeSession()V
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$500(Lcom/google/android/goggles/network/OneShotQueryManager;)V

    iget-object v0, p0, Lcom/google/android/goggles/network/OneShotQueryManager$2;->this$0:Lcom/google/android/goggles/network/OneShotQueryManager;

    # getter for: Lcom/google/android/goggles/network/OneShotQueryManager;->mCallback:Lcom/google/android/goggles/GogglesController$Callback;
    invoke-static {v0}, Lcom/google/android/goggles/network/OneShotQueryManager;->access$600(Lcom/google/android/goggles/network/OneShotQueryManager;)Lcom/google/android/goggles/GogglesController$Callback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/goggles/GogglesController$Callback;->onResultReady(Lcom/google/android/goggles/ResultSet;)V

    return-void
.end method
