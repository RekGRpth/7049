.class public interface abstract Lcom/google/android/goggles/network/QueryManager;
.super Ljava/lang/Object;
.source "QueryManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/network/QueryManager$Listener;
    }
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract getNextSequenceNumber()I
.end method

.method public abstract hasOutstandingQuery()Z
.end method

.method public abstract open(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V
.end method

.method public abstract query([BILjava/lang/String;Z)V
.end method
