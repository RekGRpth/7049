.class public Lcom/google/android/goggles/network/QueryManagerImpl;
.super Ljava/lang/Object;
.source "QueryManagerImpl.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/goggles/network/QueryManager;


# instance fields
.field private volatile mAckedSequenceNumber:I

.field private final mCallback:Lcom/google/android/goggles/network/QueryManager$Listener;

.field protected final mEngine:Lcom/google/android/goggles/network/QueryEngine;

.field private final mEngineListener:Lcom/google/android/goggles/network/QueryEngine$Listener;

.field protected mHandler:Landroid/os/Handler;

.field private final mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field private volatile mNextSequenceNumber:I

.field private final mPostConnectedCallback:Ljava/lang/Runnable;

.field private final mPostConnectedRetry:Ljava/lang/Runnable;

.field private final mPostDisconnectedCallback:Ljava/lang/Runnable;

.field private final mPostDisconnectedError:Ljava/lang/Runnable;

.field protected final mRetryPolicy:Lcom/google/android/goggles/network/RetryPolicy;

.field private mServer:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

.field protected final mThread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/goggles/network/QueryManager$Listener;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 6
    .param p1    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p2    # Lcom/google/android/velvet/VelvetFactory;
    .param p3    # Lcom/google/android/apps/sidekick/inject/LocationOracle;
    .param p4    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p5    # Lcom/google/android/goggles/network/QueryManager$Listener;
    .param p6    # Lcom/google/android/goggles/GogglesSettings;
    .param p7    # Lcom/google/android/velvet/presenter/QueryState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/goggles/network/QueryManagerImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/network/QueryManagerImpl$1;-><init>(Lcom/google/android/goggles/network/QueryManagerImpl;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mEngineListener:Lcom/google/android/goggles/network/QueryEngine$Listener;

    new-instance v0, Lcom/google/android/goggles/network/QueryManagerImpl$2;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/network/QueryManagerImpl$2;-><init>(Lcom/google/android/goggles/network/QueryManagerImpl;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mPostConnectedRetry:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/goggles/network/QueryManagerImpl$3;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/network/QueryManagerImpl$3;-><init>(Lcom/google/android/goggles/network/QueryManagerImpl;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mPostConnectedCallback:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/goggles/network/QueryManagerImpl$4;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/network/QueryManagerImpl$4;-><init>(Lcom/google/android/goggles/network/QueryManagerImpl;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mPostDisconnectedCallback:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/goggles/network/QueryManagerImpl$5;

    invoke-direct {v0, p0}, Lcom/google/android/goggles/network/QueryManagerImpl$5;-><init>(Lcom/google/android/goggles/network/QueryManagerImpl;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mPostDisconnectedError:Ljava/lang/Runnable;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mAckedSequenceNumber:I

    iput-object p5, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mCallback:Lcom/google/android/goggles/network/QueryManager$Listener;

    invoke-virtual {p0, p1}, Lcom/google/android/goggles/network/QueryManagerImpl;->createRetryPolicy(Lcom/google/android/voicesearch/VoiceSearchServices;)Lcom/google/android/goggles/network/RetryPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mRetryPolicy:Lcom/google/android/goggles/network/RetryPolicy;

    iput-object p3, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    iput-object p4, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mEngineListener:Lcom/google/android/goggles/network/QueryEngine$Listener;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p6

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/goggles/network/QueryManagerImpl;->createQueryEngine(Lcom/google/android/goggles/network/QueryEngine$Listener;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/goggles/network/QueryEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mEngine:Lcom/google/android/goggles/network/QueryEngine;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "goggles query manager"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mThread:Landroid/os/HandlerThread;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/goggles/network/QueryManagerImpl;I)I
    .locals 0
    .param p0    # Lcom/google/android/goggles/network/QueryManagerImpl;
    .param p1    # I

    iput p1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mAckedSequenceNumber:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/goggles/network/QueryManagerImpl;)Lcom/google/android/goggles/network/QueryManager$Listener;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/QueryManagerImpl;

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mCallback:Lcom/google/android/goggles/network/QueryManager$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/goggles/network/QueryManagerImpl;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/QueryManagerImpl;

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mPostDisconnectedError:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/goggles/network/QueryManagerImpl;Ljava/lang/Runnable;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/network/QueryManagerImpl;
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/network/QueryManagerImpl;->closeInner(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/goggles/network/QueryManagerImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/network/QueryManagerImpl;

    invoke-direct {p0}, Lcom/google/android/goggles/network/QueryManagerImpl;->retry()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/goggles/network/QueryManagerImpl;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/goggles/network/QueryManagerImpl;

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mPostConnectedRetry:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/goggles/network/QueryManagerImpl;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Ljava/lang/Runnable;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/network/QueryManagerImpl;
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;
    .param p2    # Ljava/lang/Runnable;

    invoke-direct {p0, p1, p2}, Lcom/google/android/goggles/network/QueryManagerImpl;->openInner(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Ljava/lang/Runnable;)V

    return-void
.end method

.method private buildQuery([BILjava/lang/String;Z)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;
    .locals 4
    .param p1    # [B
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const/4 v3, 0x1

    new-instance v2, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    invoke-direct {v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;-><init>()V

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;-><init>()V

    invoke-static {p1}, Lcom/google/protobuf/micro/ByteStringMicro;->copyFrom([B)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;->setEncodedImage(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    invoke-virtual {v1, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;->setImageEncoding(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    sparse-switch p2, :sswitch_data_0

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setImage(Lcom/google/bionics/goggles/api2/GogglesProtos$Image;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, p3}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setText(Ljava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    :cond_0
    invoke-virtual {v2, p4}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setImportantPayload(Z)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    return-object v2

    :sswitch_0
    invoke-virtual {v1, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;->setImageRotation(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    goto :goto_0

    :sswitch_1
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;->setImageRotation(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    goto :goto_0

    :sswitch_2
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Image;->setImageRotation(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Image;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method

.method private declared-synchronized clearQueue()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private closeInner(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mNextSequenceNumber:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mAckedSequenceNumber:I

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/network/QueryManagerImpl;->sendMessage(Landroid/os/Message;)V

    return-void
.end method

.method private doClose(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mServer:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mEngine:Lcom/google/android/goggles/network/QueryEngine;

    invoke-virtual {v0}, Lcom/google/android/goggles/network/QueryEngine;->sendLastRequest()V

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mEngine:Lcom/google/android/goggles/network/QueryEngine;

    invoke-virtual {v0}, Lcom/google/android/goggles/network/QueryEngine;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mServer:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    invoke-direct {p0}, Lcom/google/android/goggles/network/QueryManagerImpl;->clearQueue()V

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private doOpen(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;
    .param p2    # Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mServer:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput-object p1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mServer:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mEngine:Lcom/google/android/goggles/network/QueryEngine;

    invoke-virtual {v1, p1}, Lcom/google/android/goggles/network/QueryEngine;->connect(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V

    invoke-interface {p2}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/goggles/network/QueryManagerImpl;->clearQueue()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mServer:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mCallback:Lcom/google/android/goggles/network/QueryManager$Listener;

    invoke-interface {v1}, Lcom/google/android/goggles/network/QueryManager$Listener;->onConnectionError()V

    goto :goto_1
.end method

.method private doQuery(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)V
    .locals 2
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mServer:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/goggles/network/QueryManagerImpl;->setLocationIfAvailable(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)V

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mEngine:Lcom/google/android/goggles/network/QueryEngine;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/network/QueryEngine;->sendQuery(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)V

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mCallback:Lcom/google/android/goggles/network/QueryManager$Listener;

    invoke-virtual {p1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->getSequenceNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/goggles/network/QueryManager$Listener;->onQuerySent(I)V

    goto :goto_0
.end method

.method private maybeStartThreadBlocking()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method private openInner(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Ljava/lang/Runnable;)V
    .locals 4
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;
    .param p2    # Ljava/lang/Runnable;

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mNextSequenceNumber:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mAckedSequenceNumber:I

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v3

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;

    invoke-static {v1, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->sendMessage(Landroid/os/Message;)V

    return-void
.end method

.method private declared-synchronized retry()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mServer:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    new-instance v1, Lcom/google/android/speech/params/RecognizerParamsBuilder;

    iget-object v2, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mEngine:Lcom/google/android/goggles/network/QueryEngine;

    invoke-virtual {v2}, Lcom/google/android/goggles/network/QueryEngine;->getRecognizerParams()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;-><init>(Lcom/google/android/speech/params/RecognizerParams;)V

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    new-instance v1, Lcom/google/android/goggles/network/QueryManagerImpl$6;

    invoke-direct {v1, p0, v0}, Lcom/google/android/goggles/network/QueryManagerImpl$6;-><init>(Lcom/google/android/goggles/network/QueryManagerImpl;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V

    invoke-direct {p0, v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->closeInner(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private setLocationIfAvailable(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)V
    .locals 4
    .param p1    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    iget-object v2, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mLocationOracle:Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v2}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForGoogleApps()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    invoke-direct {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;-><init>()V

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->setLatDegrees(D)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->setLngDegrees(D)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->setLatLngAccuracyMeters(F)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    :cond_0
    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;->setAltitudeMeters(F)Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;

    :cond_1
    invoke-virtual {p1, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setPose(Lcom/google/bionics/goggles/api2/GogglesProtos$Pose;)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    :cond_2
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mRetryPolicy:Lcom/google/android/goggles/network/RetryPolicy;

    invoke-virtual {v0}, Lcom/google/android/goggles/network/RetryPolicy;->markAsUnretryable()V

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mPostDisconnectedCallback:Ljava/lang/Runnable;

    invoke-direct {p0, v0}, Lcom/google/android/goggles/network/QueryManagerImpl;->closeInner(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected createQueryEngine(Lcom/google/android/goggles/network/QueryEngine$Listener;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/goggles/network/QueryEngine;
    .locals 6
    .param p1    # Lcom/google/android/goggles/network/QueryEngine$Listener;
    .param p2    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p3    # Lcom/google/android/velvet/VelvetFactory;
    .param p4    # Lcom/google/android/goggles/GogglesSettings;
    .param p5    # Lcom/google/android/velvet/presenter/QueryState;

    new-instance v0, Lcom/google/android/goggles/network/QueryEngine;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/network/QueryEngine;-><init>(Lcom/google/android/goggles/network/QueryEngine$Listener;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/goggles/GogglesSettings;Lcom/google/android/velvet/presenter/QueryState;)V

    return-object v0
.end method

.method protected createRetryPolicy(Lcom/google/android/voicesearch/VoiceSearchServices;)Lcom/google/android/goggles/network/RetryPolicy;
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/VoiceSearchServices;

    new-instance v0, Lcom/google/android/goggles/network/RetryPolicy;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/goggles/network/RetryPolicy;-><init>(Lcom/google/android/voicesearch/settings/Settings;)V

    return-object v0
.end method

.method public getNextSequenceNumber()I
    .locals 1

    iget v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mNextSequenceNumber:I

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x1

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return v3

    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    aget-object v2, v0, v3

    check-cast v2, Ljava/lang/Runnable;

    invoke-direct {p0, v1, v2}, Lcom/google/android/goggles/network/QueryManagerImpl;->doOpen(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Runnable;

    invoke-direct {p0, v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->doClose(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    invoke-direct {p0, v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->doQuery(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public hasOutstandingQuery()Z
    .locals 2

    iget v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mNextSequenceNumber:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mAckedSequenceNumber:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized open(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;)V
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/goggles/network/QueryManagerImpl;->maybeStartThreadBlocking()V

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mRetryPolicy:Lcom/google/android/goggles/network/RetryPolicy;

    invoke-virtual {v0}, Lcom/google/android/goggles/network/RetryPolicy;->reset()V

    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mPostConnectedCallback:Ljava/lang/Runnable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/goggles/network/QueryManagerImpl;->openInner(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$PairHttpServerInfo;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized query([BILjava/lang/String;Z)V
    .locals 3
    .param p1    # [B
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/goggles/network/QueryManagerImpl;->buildQuery([BILjava/lang/String;Z)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    move-result-object v0

    iget v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mNextSequenceNumber:I

    invoke-virtual {v0, v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;->setSequenceNumber(I)Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesStreamRequest;

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/TraceTracker;->getLatestSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v1

    iget v2, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mNextSequenceNumber:I

    invoke-interface {v1, v2}, Lcom/google/android/goggles/TraceTracker$Session;->addClientEventSendRequest(I)V

    iget v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mNextSequenceNumber:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mNextSequenceNumber:I

    iget-object v1, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/goggles/network/QueryManagerImpl;->sendMessage(Landroid/os/Message;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected declared-synchronized sendMessage(Landroid/os/Message;)V
    .locals 1
    .param p1    # Landroid/os/Message;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/goggles/network/QueryManagerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
