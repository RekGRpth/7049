.class public Lcom/google/android/goggles/ui/DebugView$Frames$Frame;
.super Ljava/lang/Object;
.source "DebugView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/DebugView$Frames;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Frame"
.end annotation


# instance fields
.field public final bitmap:Landroid/graphics/Bitmap;

.field public final sequenceNumber:I


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;I)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/goggles/ui/DebugView$Frames$Frame;->bitmap:Landroid/graphics/Bitmap;

    iput p2, p0, Lcom/google/android/goggles/ui/DebugView$Frames$Frame;->sequenceNumber:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/Bitmap;ILcom/google/android/goggles/ui/DebugView$1;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # Lcom/google/android/goggles/ui/DebugView$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/goggles/ui/DebugView$Frames$Frame;-><init>(Landroid/graphics/Bitmap;I)V

    return-void
.end method
