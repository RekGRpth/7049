.class public Lcom/google/android/goggles/ui/DebugView$QueryStats;
.super Ljava/lang/Object;
.source "DebugView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/DebugView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QueryStats"
.end annotation


# instance fields
.field public volatile highestSeqNumberCompleted:I

.field public volatile highestSeqNumberRecv:I

.field public volatile resultSetNumberRecv:I

.field public volatile sequenceNumberSent:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/goggles/ui/DebugView$QueryStats;->sequenceNumberSent:I

    iput v0, p0, Lcom/google/android/goggles/ui/DebugView$QueryStats;->resultSetNumberRecv:I

    iput v0, p0, Lcom/google/android/goggles/ui/DebugView$QueryStats;->highestSeqNumberRecv:I

    iput v0, p0, Lcom/google/android/goggles/ui/DebugView$QueryStats;->highestSeqNumberCompleted:I

    return-void
.end method
