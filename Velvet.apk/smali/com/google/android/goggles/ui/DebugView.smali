.class public Lcom/google/android/goggles/ui/DebugView;
.super Landroid/view/View;
.source "DebugView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/ui/DebugView$1;,
        Lcom/google/android/goggles/ui/DebugView$Frames;,
        Lcom/google/android/goggles/ui/DebugView$ResultStats;,
        Lcom/google/android/goggles/ui/DebugView$QueryStats;
    }
.end annotation


# static fields
.field public static authErrorCount:I

.field public static barcode:Lcom/google/android/goggles/Barcode;

.field public static barcodeProcessTimeMs:J

.field public static focusMode:Ljava/lang/String;

.field public static fpsTracker:Lcom/google/android/goggles/camera/FpsTracker;

.field public static frames:Lcom/google/android/goggles/ui/DebugView$Frames;

.field private static instance:Lcom/google/android/goggles/ui/DebugView;

.field public static isFocusing:Z

.field public static lastError:Ljava/lang/String;

.field public static maxRetry:I

.field public static newBarcodeTimestamp:J

.field public static queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

.field public static resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

.field public static retryCount:I

.field public static retryTimeout:I

.field public static sceneChangeAt:I

.field public static sceneChangeDelayMillis:I

.field public static sessionIds:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static sessionStartMillis:J

.field public static sessionTimeoutMillis:J

.field public static visionGyroThresholdCrossed:Z

.field public static visionGyroVector:[F


# instance fields
.field private final mFontMetrics:Landroid/graphics/Paint$FontMetricsInt;

.field private final mMargins:Landroid/graphics/Rect;

.field private final mPaint:Landroid/graphics/Paint;

.field private final mTextNormal:I

.field private final mTextSmall:I

.field private mX:I

.field private mY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->sessionIds:Ljava/util/LinkedList;

    new-instance v0, Lcom/google/android/goggles/ui/DebugView$QueryStats;

    invoke-direct {v0}, Lcom/google/android/goggles/ui/DebugView$QueryStats;-><init>()V

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    new-instance v0, Lcom/google/android/goggles/ui/DebugView$ResultStats;

    invoke-direct {v0}, Lcom/google/android/goggles/ui/DebugView$ResultStats;-><init>()V

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/goggles/ui/DebugView;->visionGyroThresholdCrossed:Z

    new-instance v0, Lcom/google/android/goggles/ui/DebugView$Frames;

    invoke-direct {v0}, Lcom/google/android/goggles/ui/DebugView$Frames;-><init>()V

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->frames:Lcom/google/android/goggles/ui/DebugView$Frames;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/high16 v6, 0x40000000

    const/high16 v5, 0x41200000

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v2}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    iput-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mFontMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x41000000

    const/high16 v4, -0x1000000

    invoke-virtual {v2, v3, v6, v6, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-static {v5, p1}, Lcom/google/android/goggles/ui/DebugView;->dipToPix(FLandroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/high16 v2, 0x41400000

    invoke-static {v2, p1}, Lcom/google/android/goggles/ui/DebugView;->dipToPix(FLandroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    invoke-static {v5, p1}, Lcom/google/android/goggles/ui/DebugView;->dipToPix(FLandroid/content/Context;)I

    move-result v0

    invoke-static {v5, p1}, Lcom/google/android/goggles/ui/DebugView;->dipToPix(FLandroid/content/Context;)I

    move-result v1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v0, v0, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    invoke-static {}, Lcom/google/android/goggles/ui/DebugView;->isVisible()Z

    move-result v0

    return v0
.end method

.method public static clearStats()V
    .locals 3

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/goggles/ui/DebugView;->sessionStartMillis:J

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/DebugView$QueryStats;->clear()V

    const/4 v0, -0x1

    sput v0, Lcom/google/android/goggles/ui/DebugView;->sceneChangeAt:I

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/DebugView$ResultStats;->clear()V

    sput v2, Lcom/google/android/goggles/ui/DebugView;->authErrorCount:I

    sput v2, Lcom/google/android/goggles/ui/DebugView;->retryCount:I

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->lastError:Ljava/lang/String;

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->frames:Lcom/google/android/goggles/ui/DebugView$Frames;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/DebugView$Frames;->clear()V

    return-void
.end method

.method public static detachFrom(Landroid/widget/FrameLayout;)V
    .locals 1
    .param p0    # Landroid/widget/FrameLayout;

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->invalidate()V

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    :cond_0
    return-void
.end method

.method private static dipToPix(FLandroid/content/Context;)I
    .locals 3
    .param p0    # F
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, p0

    const/high16 v2, 0x3f000000

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private drawFrames(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    iget-object v9, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    iget v7, v9, Landroid/graphics/Rect;->left:I

    iget-object v9, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    iget v8, v9, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/DebugView;->getWidth()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    sub-int v6, v9, v10

    const/4 v5, 0x0

    sget-object v10, Lcom/google/android/goggles/ui/DebugView;->frames:Lcom/google/android/goggles/ui/DebugView$Frames;

    monitor-enter v10

    :try_start_0
    sget-object v9, Lcom/google/android/goggles/ui/DebugView;->frames:Lcom/google/android/goggles/ui/DebugView$Frames;

    invoke-virtual {v9}, Lcom/google/android/goggles/ui/DebugView$Frames;->size()I

    move-result v9

    if-nez v9, :cond_0

    monitor-exit v10

    :goto_0
    return-void

    :cond_0
    div-int/lit8 v3, v6, 0x6

    iget-object v9, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    const/4 v11, -0x1

    invoke-virtual {v9, v11}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v9, Lcom/google/android/goggles/ui/DebugView;->frames:Lcom/google/android/goggles/ui/DebugView$Frames;

    invoke-virtual {v9}, Lcom/google/android/goggles/ui/DebugView$Frames;->size()I

    move-result v9

    add-int/lit8 v4, v9, -0x1

    :goto_1
    if-ltz v4, :cond_1

    sget-object v9, Lcom/google/android/goggles/ui/DebugView;->frames:Lcom/google/android/goggles/ui/DebugView$Frames;

    invoke-virtual {v9, v4}, Lcom/google/android/goggles/ui/DebugView$Frames;->get(I)Lcom/google/android/goggles/ui/DebugView$Frames$Frame;

    move-result-object v1

    iget-object v9, v1, Lcom/google/android/goggles/ui/DebugView$Frames$Frame;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    mul-int/2addr v9, v3

    iget-object v11, v1, Lcom/google/android/goggles/ui/DebugView$Frames$Frame;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    div-int v2, v9, v11

    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v9, v7, 0x2

    add-int/lit8 v11, v8, 0x2

    add-int v12, v7, v3

    add-int/lit8 v12, v12, -0x2

    add-int v13, v8, v2

    add-int/lit8 v13, v13, -0x2

    invoke-direct {v0, v9, v11, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v9, v1, Lcom/google/android/goggles/ui/DebugView$Frames$Frame;->bitmap:Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p1, v9, v11, v0, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    const/4 v11, 0x0

    invoke-virtual {v9, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    iget v11, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    int-to-float v11, v11

    invoke-virtual {v9, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "#"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v11, v1, Lcom/google/android/goggles/ui/DebugView$Frames$Frame;->sequenceNumber:I

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget v11, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v11, v11, 0x4

    int-to-float v11, v11

    iget v12, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v12, v12, 0x4

    iget v13, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    add-int/2addr v12, v13

    int-to-float v12, v12

    iget-object v13, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v11, v12, v13}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/2addr v7, v3

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    :cond_1
    iget v9, p0, Lcom/google/android/goggles/ui/DebugView;->mY:I

    add-int/lit8 v11, v5, 0x5

    add-int/2addr v9, v11

    iput v9, p0, Lcom/google/android/goggles/ui/DebugView;->mY:I

    monitor-exit v10

    goto/16 :goto_0

    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9
.end method

.method private drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p4}, Landroid/graphics/Paint;->setFlags(I)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    int-to-float v2, p2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mFontMetrics:Landroid/graphics/Paint$FontMetricsInt;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    const/high16 v2, 0x3f000000

    add-float/2addr v1, v2

    float-to-int v0, v1

    iget v1, p0, Lcom/google/android/goggles/ui/DebugView;->mX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mY:I

    iget-object v3, p0, Lcom/google/android/goggles/ui/DebugView;->mFontMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/goggles/ui/DebugView;->mFontMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p5, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v1, p0, Lcom/google/android/goggles/ui/DebugView;->mX:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/goggles/ui/DebugView;->mX:I

    const-string v1, "\n"

    invoke-virtual {p5, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/google/android/goggles/ui/DebugView;->mX:I

    iget v1, p0, Lcom/google/android/goggles/ui/DebugView;->mY:I

    iget-object v2, p0, Lcom/google/android/goggles/ui/DebugView;->mFontMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget-object v3, p0, Lcom/google/android/goggles/ui/DebugView;->mFontMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/goggles/ui/DebugView;->mFontMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/goggles/ui/DebugView;->mY:I

    :cond_0
    return-void
.end method

.method private getMaxWidthAvailable(Landroid/graphics/Canvas;)I
    .locals 2
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/goggles/ui/DebugView;->mX:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private static declared-synchronized isVisible()Z
    .locals 2

    const-class v1, Lcom/google/android/goggles/ui/DebugView;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/DebugView;->getParent()Landroid/view/ViewParent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static maybeAttachTo(Landroid/widget/FrameLayout;)V
    .locals 3
    .param p0    # Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/goggles/GogglesSettings;->shouldShowDebugView()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/goggles/ui/DebugView;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/goggles/ui/DebugView;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public static postUpdate()V
    .locals 1

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->instance:Lcom/google/android/goggles/ui/DebugView;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/DebugView;->postInvalidate()V

    :cond_0
    return-void
.end method

.method public static pushSessionId(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->sessionIds:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->sessionIds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->sessionIds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private truncate(Ljava/lang/String;II)Ljava/lang/String;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    iget-object v7, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v7, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    int-to-float v8, p2

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v7, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    int-to-float v8, p3

    invoke-virtual {v7, p1, v11, v8, v9}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v1, v7, :cond_0

    :goto_0
    return-object p1

    :cond_0
    iget-object v7, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    const-string v8, "..."

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    int-to-float v7, p3

    sub-float/2addr v7, v0

    const/high16 v8, 0x40000000

    div-float v6, v7, v8

    iget-object v7, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, p1, v11, v6, v9}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v2

    invoke-virtual {p1, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v5, v10, v6, v9}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    sub-int/2addr v7, v4

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "..."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iput v0, p0, Lcom/google/android/goggles/ui/DebugView;->mX:I

    iget-object v0, p0, Lcom/google/android/goggles/ui/DebugView;->mMargins:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, p0, Lcom/google/android/goggles/ui/DebugView;->mY:I

    invoke-direct {p0, p1}, Lcom/google/android/goggles/ui/DebugView;->drawFrames(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/DebugView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v6

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Debug frontend: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/google/android/goggles/GogglesSettings;->getCurrentServer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Logs:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/goggles/GogglesSettings;->getLogServerInfo()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v5, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    invoke-direct {p0, p1}, Lcom/google/android/goggles/ui/DebugView;->getMaxWidthAvailable(Landroid/graphics/Canvas;)I

    move-result v6

    invoke-direct {p0, v1, v5, v6}, Lcom/google/android/goggles/ui/DebugView;->truncate(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "\n"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Elapsed:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sget-wide v7, Lcom/google/android/goggles/ui/DebugView;->sessionStartMillis:J

    sub-long/2addr v5, v7

    long-to-float v1, v5

    const/high16 v5, 0x447a0000

    div-float/2addr v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " second(s), max:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-wide v5, Lcom/google/android/goggles/ui/DebugView;->sessionTimeoutMillis:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->sessionIds:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->descendingIterator()Ljava/util/Iterator;

    move-result-object v6

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Session: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    goto :goto_1

    :cond_0
    const-string v0, "NONE"

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "\n"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "Seq sent:"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const v3, -0xffff01

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    iget v1, v1, Lcom/google/android/goggles/ui/DebugView$QueryStats;->sequenceNumberSent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "Hi seq recv:"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/high16 v3, -0x10000

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    iget v1, v1, Lcom/google/android/goggles/ui/DebugView$QueryStats;->highestSeqNumberRecv:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "Hi seq cmpl:"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/16 v3, -0x100

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    iget v1, v1, Lcom/google/android/goggles/ui/DebugView$QueryStats;->highestSeqNumberCompleted:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "result num recv:"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const v3, -0xff0100

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->queryStats:Lcom/google/android/goggles/ui/DebugView$QueryStats;

    iget v1, v1, Lcom/google/android/goggles/ui/DebugView$QueryStats;->resultSetNumberRecv:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "\n"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Scene starts from:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/goggles/ui/DebugView;->sceneChangeAt:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", will delay:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/goggles/ui/DebugView;->sceneChangeDelayMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    iget v1, v1, Lcom/google/android/goggles/ui/DebugView$ResultStats;->sceneResultCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    iget v1, v1, Lcom/google/android/goggles/ui/DebugView$ResultStats;->lastResultCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->resultStats:Lcom/google/android/goggles/ui/DebugView$ResultStats;

    # getter for: Lcom/google/android/goggles/ui/DebugView$ResultStats;->results:Ljava/util/concurrent/CopyOnWriteArrayList;
    invoke-static {v0}, Lcom/google/android/goggles/ui/DebugView$ResultStats;->access$200(Lcom/google/android/goggles/ui/DebugView$ResultStats;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/text/SpannableString;

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-class v2, Ljava/lang/Integer;

    invoke-virtual {v6, v0, v1, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {v6, v0, v1, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Boolean;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-class v2, Ljava/lang/String;

    invoke-virtual {v6, v0, v1, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-eqz v9, :cond_2

    const/16 v1, 0xff

    const/16 v2, 0xc8

    const/16 v3, 0xc8

    const/16 v4, 0xc8

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    :goto_3
    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v4, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    if-eqz v9, :cond_3

    const/4 v4, 0x0

    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    goto/16 :goto_2

    :cond_2
    const/16 v1, 0x96

    const/16 v2, 0xc8

    const/16 v3, 0xc8

    const/16 v4, 0xc8

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    goto :goto_3

    :cond_3
    const/16 v4, 0x10

    goto :goto_4

    :cond_4
    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "\n"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->fpsTracker:Lcom/google/android/goggles/camera/FpsTracker;

    if-eqz v0, :cond_5

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fps:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->fpsTracker:Lcom/google/android/goggles/camera/FpsTracker;

    invoke-virtual {v1}, Lcom/google/android/goggles/camera/FpsTracker;->getFpsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    :cond_5
    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Focus mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->focusMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Focus:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v0, Lcom/google/android/goggles/ui/DebugView;->isFocusing:Z

    if-eqz v0, :cond_8

    const-string v0, "moving"

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextSmall:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "\n"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/high16 v3, -0x10000

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/goggles/ui/DebugView;->authErrorCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, " auth errors "

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const v3, -0xff0100

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/goggles/ui/DebugView;->retryCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " retries, max:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/goggles/ui/DebugView;->maxRetry:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timeout:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/goggles/ui/DebugView;->retryTimeout:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "last error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->lastError:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->visionGyroVector:[F

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    sget-boolean v0, Lcom/google/android/goggles/ui/DebugView;->visionGyroThresholdCrossed:Z

    if-eqz v0, :cond_9

    const/high16 v0, -0x10000

    :goto_6
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    div-int/lit8 v4, v1, 0x2

    if-le v0, v4, :cond_a

    int-to-float v1, v0

    int-to-float v2, v4

    int-to-float v0, v0

    sget-object v3, Lcom/google/android/goggles/ui/DebugView;->visionGyroVector:[F

    const/4 v5, 0x0

    aget v3, v3, v5

    add-float/2addr v3, v0

    int-to-float v0, v4

    sget-object v4, Lcom/google/android/goggles/ui/DebugView;->visionGyroVector:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_6
    :goto_7
    sget-object v0, Lcom/google/android/goggles/ui/DebugView;->barcode:Lcom/google/android/goggles/Barcode;

    if-eqz v0, :cond_7

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v0, "[%2dms] "

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-wide v6, Lcom/google/android/goggles/ui/DebugView;->barcodeProcessTimeMs:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    const-string v0, ""

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->barcode:Lcom/google/android/goggles/Barcode;

    iget-object v1, v1, Lcom/google/android/goggles/Barcode;->info:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "No barcode detected."

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    :cond_7
    :goto_8
    return-void

    :cond_8
    const-string v0, "stopped"

    goto/16 :goto_5

    :cond_9
    const v0, -0xff0100

    goto :goto_6

    :cond_a
    int-to-float v1, v0

    int-to-float v2, v4

    int-to-float v0, v0

    sget-object v3, Lcom/google/android/goggles/ui/DebugView;->visionGyroVector:[F

    const/4 v5, 0x1

    aget v3, v3, v5

    sub-float v3, v0, v3

    int-to-float v0, v4

    sget-object v4, Lcom/google/android/goggles/ui/DebugView;->visionGyroVector:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/goggles/ui/DebugView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_7

    :cond_b
    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/goggles/ui/DebugView;->barcode:Lcom/google/android/goggles/Barcode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/goggles/ui/DebugView;->mTextNormal:I

    const/4 v3, -0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Detected "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sget-wide v7, Lcom/google/android/goggles/ui/DebugView;->newBarcodeTimestamp:J

    sub-long/2addr v5, v7

    long-to-float v1, v5

    const/high16 v5, 0x447a0000

    div-float/2addr v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s ago.\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/goggles/ui/DebugView;->drawMessage(Landroid/graphics/Canvas;IIILjava/lang/String;)V

    goto :goto_8
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/goggles/ui/DebugView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method
