.class Lcom/google/android/goggles/ui/GogglesPlate$7;
.super Ljava/lang/Object;
.source "GogglesPlate.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/GogglesPlate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/ui/GogglesPlate;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/ui/GogglesPlate;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/ui/GogglesPlate$7;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$7;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mCameraButton:Lcom/google/android/goggles/ui/CameraButton;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$1400(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/CameraButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/CameraButton;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$7;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mResponseCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$300(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;->onCameraButtonClicked()V

    return-void
.end method
