.class Lcom/google/android/goggles/ui/GogglesPlate$8;
.super Ljava/lang/Object;
.source "GogglesPlate.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/GogglesPlate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/ui/GogglesPlate;


# direct methods
.method constructor <init>(Lcom/google/android/goggles/ui/GogglesPlate;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/ui/GogglesPlate$8;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$8;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$800(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/camera/CameraManager;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "goggles.GogglesPlate"

    const-string v1, "Flash toggle clicked, but camera is still null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$8;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$900(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;->onFlashButtonClicked()V

    goto :goto_0
.end method
