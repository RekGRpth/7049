.class Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;
.super Lcom/google/android/goggles/camera/CameraManager$Listener;
.source "GogglesPlate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/GogglesPlate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/goggles/ui/GogglesPlate;


# direct methods
.method private constructor <init>(Lcom/google/android/goggles/ui/GogglesPlate;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-direct {p0}, Lcom/google/android/goggles/camera/CameraManager$Listener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/goggles/ui/GogglesPlate;Lcom/google/android/goggles/ui/GogglesPlate$1;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/ui/GogglesPlate;
    .param p2    # Lcom/google/android/goggles/ui/GogglesPlate$1;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;)V

    return-void
.end method


# virtual methods
.method public onCameraError()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v1}, Lcom/google/android/goggles/ui/GogglesPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0042

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public onCameraOpened(Z)V
    .locals 3
    .param p1    # Z

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$800(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/camera/CameraManager;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "goggles.GogglesPlate"

    const-string v1, "onCameraOpened() triggered on null camera."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$900(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;->onCameraOpened(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$800(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/camera/CameraManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraManager;->canEnableTorch()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mFlashButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$1000(Lcom/google/android/goggles/ui/GogglesPlate;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$1100(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->disableFlash()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->completelyVisible:Z
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$1200(Lcom/google/android/goggles/ui/GogglesPlate;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mNextMode:I
    invoke-static {v1}, Lcom/google/android/goggles/ui/GogglesPlate;->access$1300(Lcom/google/android/goggles/ui/GogglesPlate;)I

    move-result v1

    const/4 v2, 0x1

    # invokes: Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->access$400(Lcom/google/android/goggles/ui/GogglesPlate;IZ)V

    goto :goto_0
.end method

.method public onCameraReleased()V
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$900(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;->onCameraReleased()V

    return-void
.end method

.method public onPreviewStarted()V
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;->this$0:Lcom/google/android/goggles/ui/GogglesPlate;

    # getter for: Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;
    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->access$800(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/camera/CameraManager;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "goggles.GogglesPlate"

    const-string v1, "onPreviewStarted() triggered on null camera."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
