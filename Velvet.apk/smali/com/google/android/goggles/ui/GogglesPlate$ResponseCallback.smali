.class public interface abstract Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;
.super Ljava/lang/Object;
.source "GogglesPlate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/GogglesPlate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ResponseCallback"
.end annotation


# virtual methods
.method public abstract onCameraButtonClicked()V
.end method

.method public abstract onClearButtonClicked()V
.end method

.method public abstract onMicrophoneButtonClicked()V
.end method

.method public abstract onTextEdited(Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Z)V
.end method
