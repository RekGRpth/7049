.class public Lcom/google/android/goggles/ui/GogglesPlate;
.super Landroid/widget/FrameLayout;
.source "GogglesPlate.java"

# interfaces
.implements Lcom/google/android/goggles/GogglesController$GogglesUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;,
        Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;,
        Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;
    }
.end annotation


# instance fields
.field private completelyVisible:Z

.field private mCamera:Lcom/google/android/goggles/camera/CameraManager;

.field private mCameraButton:Lcom/google/android/goggles/ui/CameraButton;

.field private mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

.field private mCameraPreview:Landroid/view/TextureView;

.field mCaptureClickListener:Landroid/view/View$OnClickListener;

.field private mCapturingMessage:Landroid/widget/TextView;

.field private mFlashButton:Landroid/widget/ImageButton;

.field mFlashClickHandler:Landroid/view/View$OnClickListener;

.field private mLastPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

.field private mLastPreviewImage:Landroid/graphics/Bitmap;

.field private mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

.field private volatile mMode:I

.field private mNextMode:I

.field private mQueryBar:Lcom/google/android/goggles/ui/QueryBar;

.field private mQueryImage:Landroid/widget/ImageView;

.field private mResponseCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

.field mResponseClickListener:Landroid/view/View$OnClickListener;

.field private mRingView:Lcom/google/android/goggles/ui/RingView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    new-instance v1, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;)V

    iput-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    new-instance v1, Lcom/google/android/goggles/ui/GogglesPlate$6;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/GogglesPlate$6;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;)V

    iput-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCaptureClickListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/google/android/goggles/ui/GogglesPlate$7;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/GogglesPlate$7;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;)V

    iput-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mResponseClickListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/google/android/goggles/ui/GogglesPlate$8;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/GogglesPlate$8;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;)V

    iput-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashClickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    new-instance v1, Lcom/google/android/goggles/ui/GogglesPlate$1;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/GogglesPlate$1;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;)V

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/ui/GogglesPlate;)I
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    return v0
.end method

.method static synthetic access$100(I)Z
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->isCaptureMode(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/goggles/ui/GogglesPlate;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/goggles/ui/GogglesPlate;)Z
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget-boolean v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->completelyVisible:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/goggles/ui/GogglesPlate;)I
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mNextMode:I

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/CameraButton;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraButton:Lcom/google/android/goggles/ui/CameraButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/goggles/ui/GogglesPlate;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-direct {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->maybeShowCameraPreview()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mResponseCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/goggles/ui/GogglesPlate;IZ)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/RingView;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mRingView:Lcom/google/android/goggles/ui/RingView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/goggles/ui/GogglesPlate;Lcom/google/android/goggles/camera/PreviewFrame;)Lcom/google/android/goggles/camera/PreviewFrame;
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;
    .param p1    # Lcom/google/android/goggles/camera/PreviewFrame;

    iput-object p1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/camera/CameraManager;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/goggles/ui/GogglesPlate;)Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/GogglesPlate;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    return-object v0
.end method

.method private static isCaptureMode(I)Z
    .locals 1
    .param p0    # I

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isResponseMode(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x8

    if-eq p0, v0, :cond_0

    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeShowCameraPreview()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    invoke-virtual {v1}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->getCaptureHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method private postUpdateMode(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/goggles/ui/GogglesPlate$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/goggles/ui/GogglesPlate$3;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static shouldRequestSimilarImages(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x8

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateMode(IZ)V
    .locals 4
    .param p1    # I
    .param p2    # Z

    const/4 v3, 0x6

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    if-ne v1, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->transitionElements(IZ)V

    iget v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    invoke-static {v1}, Lcom/google/android/goggles/ui/GogglesPlate;->isCaptureMode(I)Z

    move-result v1

    invoke-static {p1}, Lcom/google/android/goggles/ui/GogglesPlate;->isCaptureMode(I)Z

    move-result v2

    xor-int v0, v1, v2

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraButton:Lcom/google/android/goggles/ui/CameraButton;

    invoke-virtual {v1, p1}, Lcom/google/android/goggles/ui/CameraButton;->updateMode(I)V

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mRingView:Lcom/google/android/goggles/ui/RingView;

    invoke-virtual {v1, p1}, Lcom/google/android/goggles/ui/RingView;->updateMode(I)V

    invoke-static {p1}, Lcom/google/android/goggles/ui/GogglesPlate;->isResponseMode(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewImage:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryBar:Lcom/google/android/goggles/ui/QueryBar;

    invoke-virtual {v1, p1}, Lcom/google/android/goggles/ui/QueryBar;->updateMode(I)V

    invoke-static {p1}, Lcom/google/android/goggles/ui/GogglesPlate;->isCaptureMode(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p0}, Lcom/google/android/goggles/ui/DebugView;->maybeAttachTo(Landroid/widget/FrameLayout;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->requestLayout()V

    :cond_2
    iget v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    if-eq v1, v3, :cond_3

    if-ne p1, v3, :cond_5

    :cond_3
    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCapturingMessage:Landroid/widget/TextView;

    const v2, 0x7f0d0044

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    iput p1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lcom/google/android/goggles/ui/DebugView;->detachFrom(Landroid/widget/FrameLayout;)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCapturingMessage:Landroid/widget/TextView;

    const v2, 0x7f0d0043

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method


# virtual methods
.method public doFinalFocus(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    new-instance v1, Lcom/google/android/goggles/ui/GogglesPlate$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/goggles/ui/GogglesPlate$4;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraManager;->lockFocus(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public enterDirectly()V
    .locals 2

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    return-void
.end method

.method public enterOffline()V
    .locals 2

    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    return-void
.end method

.method public enterViaTextToCaptureMode(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    iput-boolean v2, p0, Lcom/google/android/goggles/ui/GogglesPlate;->completelyVisible:Z

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->setTextEndpointHeight(I)V

    invoke-direct {p0, v2, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mNextMode:I

    return-void
.end method

.method public enterViaTextToResponseMode(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->completelyVisible:Z

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->setTextEndpointHeight(I)V

    invoke-direct {p0, v1, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    iput v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mNextMode:I

    return-void

    :cond_0
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public enterViaVoiceToCaptureMode(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    iput-boolean v2, p0, Lcom/google/android/goggles/ui/GogglesPlate;->completelyVisible:Z

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->setVoiceEndpointHeight(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0, v2}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mNextMode:I

    return-void
.end method

.method public enterViaVoiceToResponseMode(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->completelyVisible:Z

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->setVoiceEndpointHeight(I)V

    invoke-direct {p0, v2, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    if-ne p2, v2, :cond_0

    const/16 v0, 0x8

    :goto_0
    iput v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mNextMode:I

    return-void

    :cond_0
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public exitViaText()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    return-void
.end method

.method public exitViaVoice()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    return-void
.end method

.method public getLastPreviewFrame()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewImage:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/PreviewFrame;->toRgbBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewImage:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewImage:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method getMode()I
    .locals 1

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    return v0
.end method

.method public goToResponseMode(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x7

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/16 v0, 0x8

    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x9

    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public isGogglesCapturing()Z
    .locals 1

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->isCaptureMode(I)Z

    move-result v0

    return v0
.end method

.method public isTorchOn()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraManager;->isTorchOn()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyFadeInDone()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->completelyVisible:Z

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mNextMode:I

    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->isResponseMode(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mNextMode:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    :cond_1
    return-void
.end method

.method public notifyGogglesQuerySent()V
    .locals 2

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->postUpdateMode(I)V

    :cond_0
    return-void
.end method

.method public notifyGogglesResult()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mRingView:Lcom/google/android/goggles/ui/RingView;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/RingView;->sparkle()V

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->postUpdateMode(I)V

    :cond_0
    return-void
.end method

.method public notifyNoGogglesResult()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mRingView:Lcom/google/android/goggles/ui/RingView;

    invoke-virtual {v0}, Lcom/google/android/goggles/ui/RingView;->sparkle()V

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->postUpdateMode(I)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    const v0, 0x7f100104

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraPreview:Landroid/view/TextureView;

    const v0, 0x7f100106

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/ui/RingView;

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mRingView:Lcom/google/android/goggles/ui/RingView;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mRingView:Lcom/google/android/goggles/ui/RingView;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mResponseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/RingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100105

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    const v0, 0x7f100107

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/ui/QueryBar;

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryBar:Lcom/google/android/goggles/ui/QueryBar;

    const v0, 0x7f10010f

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/ui/CameraButton;

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraButton:Lcom/google/android/goggles/ui/CameraButton;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraButton:Lcom/google/android/goggles/ui/CameraButton;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCaptureClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/CameraButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10010d

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashClickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10010e

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCapturingMessage:Landroid/widget/TextView;

    const v0, 0x7f10010b

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/goggles/ui/GogglesPlate$2;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/GogglesPlate$2;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    iget v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->configureVisibilityMatrix(I)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/goggles/ui/GogglesPlate;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->onLayout(ZIIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLayoutManager:Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;

    iget v2, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/ui/GogglesPlateLayoutManager;->determineHeight(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->setMeasuredDimension(II)V

    return-void
.end method

.method public pauseUi()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/GogglesPlate;->setKeepScreenOn(Z)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraManager;->stopPreview()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "goggles.GogglesPlate"

    const-string v1, "Pausing UI, but mCamera is null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setCallbacks(Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;)V
    .locals 1
    .param p1    # Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;
    .param p2    # Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    iput-object p1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    iput-object p2, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mResponseCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryBar:Lcom/google/android/goggles/ui/QueryBar;

    invoke-virtual {v0, p2}, Lcom/google/android/goggles/ui/QueryBar;->setCallback(Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;)V

    return-void
.end method

.method public setQueryImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setQueryText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryBar:Lcom/google/android/goggles/ui/QueryBar;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/ui/QueryBar;->setQueryText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTorch(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    if-nez v0, :cond_0

    const-string v0, "goggles.GogglesPlate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTorch("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), but camera is still null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/android/goggles/camera/CameraManager;->setTorch(Z)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashButton:Landroid/widget/ImageButton;

    const v1, 0x7f02002b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashButton:Landroid/widget/ImageButton;

    const v1, 0x7f02002a

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mFlashButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/GogglesPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public startCamera()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/goggles/camera/CameraManager;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraPreview:Landroid/view/TextureView;

    new-instance v2, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/goggles/ui/GogglesPlate$CameraListener;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;Lcom/google/android/goggles/ui/GogglesPlate$1;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/goggles/camera/CameraManager;-><init>(Landroid/view/TextureView;Lcom/google/android/goggles/camera/CameraManager$Listener;)V

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    iput-object v3, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mLastPreviewFrame:Lcom/google/android/goggles/camera/PreviewFrame;

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    new-instance v1, Lcom/google/android/goggles/ui/GogglesPlate$5;

    const-string v2, "FrameSaver"

    invoke-direct {v1, p0, v2}, Lcom/google/android/goggles/ui/GogglesPlate$5;-><init>(Lcom/google/android/goggles/ui/GogglesPlate;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/camera/CameraManager;->addFrameProcessor(Lcom/google/android/goggles/camera/FrameProcessor;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCameraCallback:Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;

    iget-object v1, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    invoke-interface {v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate$CameraCallback;->onCameraCreated(Lcom/google/android/goggles/camera/CameraManager;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraManager;->openCamera()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraManager;->requestStartPreview()V

    return-void
.end method

.method public startSpinner()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryBar:Lcom/google/android/goggles/ui/QueryBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/QueryBar;->adjustLogo(I)V

    return-void
.end method

.method public stopCamera()V
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/android/goggles/camera/CameraManager;->releaseCamera()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mCamera:Lcom/google/android/goggles/camera/CameraManager;

    goto :goto_0
.end method

.method public stopSpinner()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mQueryBar:Lcom/google/android/goggles/ui/QueryBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/ui/QueryBar;->adjustLogo(I)V

    return-void
.end method

.method public transitionBackToCaptureMode()V
    .locals 2

    iget v0, p0, Lcom/google/android/goggles/ui/GogglesPlate;->mMode:I

    invoke-static {v0}, Lcom/google/android/goggles/ui/GogglesPlate;->isResponseMode(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/goggles/ui/GogglesPlate;->updateMode(IZ)V

    return-void
.end method
