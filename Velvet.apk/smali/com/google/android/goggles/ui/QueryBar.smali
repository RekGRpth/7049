.class Lcom/google/android/goggles/ui/QueryBar;
.super Landroid/widget/RelativeLayout;
.source "QueryBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/ui/QueryBar$EventHandler;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

.field private mClearButton:Landroid/view/View;

.field private mHandler:Lcom/google/android/goggles/ui/QueryBar$EventHandler;

.field private mLogo:Landroid/view/View;

.field private mPair:Landroid/view/View;

.field private mPairVisible:Z

.field private mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

.field private mQueryString:Ljava/lang/CharSequence;

.field private mSpinner:Landroid/view/View;

.field private mTextbox:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/goggles/ui/QueryBar$EventHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/goggles/ui/QueryBar$EventHandler;-><init>(Lcom/google/android/goggles/ui/QueryBar;Lcom/google/android/goggles/ui/QueryBar$1;)V

    iput-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mHandler:Lcom/google/android/goggles/ui/QueryBar$EventHandler;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/goggles/ui/QueryBar;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/QueryBar;

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mPair:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/goggles/ui/QueryBar;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/QueryBar;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/goggles/ui/QueryBar;->mPairVisible:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/goggles/ui/QueryBar;)V
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/QueryBar;

    invoke-direct {p0}, Lcom/google/android/goggles/ui/QueryBar;->updateLogoVisibility()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/goggles/ui/QueryBar;)Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/QueryBar;

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/goggles/ui/QueryBar;)Lcom/google/android/goggles/ui/GogglesPlate;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/QueryBar;

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/goggles/ui/QueryBar;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/QueryBar;

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    return-object v0
.end method

.method private blur()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mPair:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mPair:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mPair:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method private updateLogoVisibility()V
    .locals 6

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mPair:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mPair:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget-boolean v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mPairVisible:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v5, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget-boolean v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mPairVisible:Z

    if-eqz v2, :cond_2

    move v2, v4

    :goto_2
    invoke-virtual {v5, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    iget-object v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget-boolean v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mPairVisible:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_3
    invoke-virtual {v5, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    iget-object v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mClearButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget-boolean v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mPairVisible:Z

    if-eqz v2, :cond_4

    int-to-float v2, v1

    :goto_4
    invoke-virtual {v5, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-boolean v5, p0, Lcom/google/android/goggles/ui/QueryBar;->mPairVisible:Z

    if-eqz v5, :cond_5

    :goto_5
    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void

    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0

    :cond_1
    neg-int v2, v1

    int-to-float v2, v2

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    neg-int v2, v1

    int-to-float v2, v2

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_4

    :cond_5
    move v3, v4

    goto :goto_5
.end method


# virtual methods
.method adjustLogo(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mSpinner:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/goggles/ui/QueryBar;->mLogo:Landroid/view/View;

    iget-boolean v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mPairVisible:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/velvet/ui/util/Animations;->crossFadeViews(Landroid/view/View;Landroid/view/View;Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mLogo:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/goggles/ui/QueryBar;->mSpinner:Landroid/view/View;

    iget-boolean v2, p0, Lcom/google/android/goggles/ui/QueryBar;->mPairVisible:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/velvet/ui/util/Animations;->crossFadeViews(Landroid/view/View;Landroid/view/View;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/goggles/ui/QueryBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/ui/GogglesPlate;

    iput-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mPlate:Lcom/google/android/goggles/ui/GogglesPlate;

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f10020c

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/QueryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mLogo:Landroid/view/View;

    const v0, 0x7f10020b

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/QueryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mSpinner:Landroid/view/View;

    const v0, 0x7f100108

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/QueryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mPair:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mPair:Landroid/view/View;

    new-instance v1, Lcom/google/android/goggles/ui/QueryBar$1;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/QueryBar$1;-><init>(Lcom/google/android/goggles/ui/QueryBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f100109

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/QueryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/goggles/ui/QueryBar;->mHandler:Lcom/google/android/goggles/ui/QueryBar$EventHandler;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/goggles/ui/QueryBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/QueryBar$2;-><init>(Lcom/google/android/goggles/ui/QueryBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f10010a

    invoke-virtual {p0, v0}, Lcom/google/android/goggles/ui/QueryBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mClearButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mClearButton:Landroid/view/View;

    new-instance v1, Lcom/google/android/goggles/ui/QueryBar$3;

    invoke-direct {v1, p0}, Lcom/google/android/goggles/ui/QueryBar$3;-><init>(Lcom/google/android/goggles/ui/QueryBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/goggles/ui/QueryBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setCallback(Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    iput-object p1, p0, Lcom/google/android/goggles/ui/QueryBar;->mCallback:Lcom/google/android/goggles/ui/GogglesPlate$ResponseCallback;

    return-void
.end method

.method setQueryText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/goggles/ui/QueryBar;->mQueryString:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method updateMode(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    const v1, 0x7f0d0040

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/QueryBar;->mTextbox:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/goggles/ui/QueryBar;->mQueryString:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/goggles/ui/QueryBar;->blur()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
