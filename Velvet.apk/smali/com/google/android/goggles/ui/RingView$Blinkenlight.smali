.class Lcom/google/android/goggles/ui/RingView$Blinkenlight;
.super Ljava/lang/Object;
.source "RingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/goggles/ui/RingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Blinkenlight"
.end annotation


# instance fields
.field alpha:F

.field radius:F

.field final synthetic this$0:Lcom/google/android/goggles/ui/RingView;

.field tick:J

.field x:F

.field y:F


# direct methods
.method private constructor <init>(Lcom/google/android/goggles/ui/RingView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->this$0:Lcom/google/android/goggles/ui/RingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/goggles/ui/RingView;Lcom/google/android/goggles/ui/RingView$1;)V
    .locals 0
    .param p1    # Lcom/google/android/goggles/ui/RingView;
    .param p2    # Lcom/google/android/goggles/ui/RingView$1;

    invoke-direct {p0, p1}, Lcom/google/android/goggles/ui/RingView$Blinkenlight;-><init>(Lcom/google/android/goggles/ui/RingView;)V

    return-void
.end method


# virtual methods
.method generate(FFFJ)V
    .locals 11
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # J

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->this$0:Lcom/google/android/goggles/ui/RingView;

    # getter for: Lcom/google/android/goggles/ui/RingView;->mPrng:Ljava/util/Random;
    invoke-static {v3}, Lcom/google/android/goggles/ui/RingView;->access$000(Lcom/google/android/goggles/ui/RingView;)Ljava/util/Random;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    const-wide v5, 0x3fc999999999999aL

    const-wide v7, 0x3fe999999999999aL

    float-to-double v9, p3

    mul-double/2addr v7, v9

    add-double/2addr v5, v7

    mul-double v0, v3, v5

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->this$0:Lcom/google/android/goggles/ui/RingView;

    # getter for: Lcom/google/android/goggles/ui/RingView;->mPrng:Ljava/util/Random;
    invoke-static {v3}, Lcom/google/android/goggles/ui/RingView;->access$000(Lcom/google/android/goggles/ui/RingView;)Ljava/util/Random;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    const v4, 0x40c49375

    mul-float v2, v3, v4

    float-to-double v3, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v3, v0

    double-to-float v3, v3

    add-float/2addr v3, p1

    iput v3, p0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->x:F

    float-to-double v3, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    mul-double/2addr v3, v0

    double-to-float v3, v3

    add-float/2addr v3, p2

    iput v3, p0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->y:F

    iput-wide p4, p0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->tick:J

    return-void
.end method
