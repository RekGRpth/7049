.class public Lcom/google/android/goggles/ui/RingView;
.super Landroid/view/View;
.source "RingView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/goggles/ui/RingView$ModeState;,
        Lcom/google/android/goggles/ui/RingView$Blinkenlight;
    }
.end annotation


# static fields
.field private static final BGCOLOR:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/goggles/ui/RingView;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final FLASH_COLOR:I

.field private static final MODESTATE:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/goggles/ui/RingView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final PRESSED_COLOR:I


# instance fields
.field private mBgColor:I

.field private mBgColorAnimator:Landroid/animation/ObjectAnimator;

.field private mBlinkenlights:[Lcom/google/android/goggles/ui/RingView$Blinkenlight;

.field private mCenterDotRadiusPx:F

.field private mCenterX:I

.field private mCenterY:I

.field private mCornerPainter:Landroid/graphics/Paint;

.field private mCornerPath:Landroid/graphics/Path;

.field private mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

.field private mDotBoundryRadiusPx:F

.field private mDotPainter:Landroid/graphics/Paint;

.field private mFakeCornerRadiusPx:F

.field private mFocusStartTime:J

.field private mModeStateAnimator:Landroid/animation/ObjectAnimator;

.field private mModeStateInterpolator:F

.field private mModeStateMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/goggles/ui/RingView$ModeState;",
            ">;"
        }
    .end annotation
.end field

.field private mPressedDowntime:J

.field private mPreviousModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

.field private mPrng:Ljava/util/Random;

.field private mRingBoundsRect:Landroid/graphics/RectF;

.field private mRingPainter:Landroid/graphics/Paint;

.field private mRingRadiusPx:F

.field private mRingShadowPainter:Landroid/graphics/Paint;

.field private mScaledRingRect:Landroid/graphics/RectF;

.field private volatile mSparklesRemaining:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v1, 0xff

    const/16 v0, 0x4d

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/goggles/ui/RingView;->FLASH_COLOR:I

    const/16 v0, 0xcc

    const/16 v1, 0x33

    const/16 v2, 0xb5

    const/16 v3, 0xe5

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/goggles/ui/RingView;->PRESSED_COLOR:I

    new-instance v0, Lcom/google/android/goggles/ui/RingView$1;

    const-class v1, Ljava/lang/Float;

    const-string v2, "modestate"

    invoke-direct {v0, v1, v2}, Lcom/google/android/goggles/ui/RingView$1;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/goggles/ui/RingView;->MODESTATE:Landroid/util/Property;

    new-instance v0, Lcom/google/android/goggles/ui/RingView$2;

    const-class v1, Ljava/lang/Integer;

    const-string v2, "bgcolor"

    invoke-direct {v0, v1, v2}, Lcom/google/android/goggles/ui/RingView$2;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/goggles/ui/RingView;->BGCOLOR:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/high16 v8, -0x1000000

    const/high16 v7, 0x40800000

    const/high16 v6, 0x40000000

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v3, 0x6

    new-array v3, v3, [Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    iput-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mBlinkenlights:[Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iput-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    iput-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mPrng:Ljava/util/Random;

    sget-object v3, Lcom/google/android/googlequicksearchbox/R$styleable;->RingView:[I

    invoke-virtual {p1, p2, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/high16 v3, 0x43500000

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingRadiusPx:F

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPainter:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPainter:Landroid/graphics/Paint;

    const/4 v4, 0x6

    invoke-virtual {v0, v4, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPainter:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    mul-float/2addr v4, v6

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    const/4 v3, 0x2

    const/high16 v4, 0x41400000

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/google/android/goggles/ui/RingView;->mCenterDotRadiusPx:F

    const/4 v3, 0x4

    const/high16 v4, 0x43400000

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/google/android/goggles/ui/RingView;->mDotBoundryRadiusPx:F

    const/4 v3, 0x5

    invoke-virtual {v0, v3, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/goggles/ui/RingView;->setLayerType(ILandroid/graphics/Paint;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mBlinkenlights:[Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mBlinkenlights:[Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    new-instance v4, Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/google/android/goggles/ui/RingView$Blinkenlight;-><init>(Lcom/google/android/goggles/ui/RingView;Lcom/google/android/goggles/ui/RingView$1;)V

    aput-object v4, v3, v2

    iget-object v3, p0, Lcom/google/android/goggles/ui/RingView;->mBlinkenlights:[Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    aget-object v3, v3, v2

    iput v1, v3, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->radius:F

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/goggles/ui/RingView;->initializeModeMap()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/goggles/ui/RingView;)Ljava/util/Random;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/RingView;

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mPrng:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/goggles/ui/RingView;)F
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/RingView;

    iget v0, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateInterpolator:F

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/goggles/ui/RingView;F)F
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/RingView;
    .param p1    # F

    iput p1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateInterpolator:F

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/goggles/ui/RingView;)I
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/RingView;

    iget v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColor:I

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/goggles/ui/RingView;I)I
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/RingView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/goggles/ui/RingView;->mBgColor:I

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/goggles/ui/RingView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/RingView;
    .param p1    # Landroid/animation/ObjectAnimator;

    iput-object p1, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/goggles/ui/RingView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/google/android/goggles/ui/RingView;

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/goggles/ui/RingView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0
    .param p0    # Lcom/google/android/goggles/ui/RingView;
    .param p1    # Landroid/animation/ObjectAnimator;

    iput-object p1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$600()Landroid/util/Property;
    .locals 1

    sget-object v0, Lcom/google/android/goggles/ui/RingView;->MODESTATE:Landroid/util/Property;

    return-object v0
.end method

.method private calculateDots()V
    .locals 15

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    const/4 v7, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mBlinkenlights:[Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    array-length v1, v1

    if-ge v7, v1, :cond_8

    const-wide/16 v1, 0x96

    int-to-long v13, v7

    mul-long/2addr v1, v13

    add-long v8, v11, v1

    const-wide/16 v1, 0x2ee

    div-long v4, v8, v1

    const-wide/16 v1, 0x2ee

    rem-long v1, v8, v1

    long-to-float v1, v1

    const v2, 0x443b8000

    div-float v10, v1, v2

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    iget v6, v1, Lcom/google/android/goggles/ui/RingView$ModeState;->gogglesPlateMode:I

    :goto_1
    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mBlinkenlights:[Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    aget-object v0, v1, v7

    iget v1, p0, Lcom/google/android/goggles/ui/RingView;->mSparklesRemaining:I

    if-gtz v1, :cond_0

    const/4 v1, 0x6

    if-eq v6, v1, :cond_0

    const/16 v1, 0xa

    if-ne v6, v1, :cond_3

    :cond_0
    iget-wide v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->tick:J

    const-wide/16 v13, 0x1

    sub-long v13, v4, v13

    cmp-long v1, v1, v13

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/android/goggles/ui/RingView;->mCenterX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/goggles/ui/RingView;->mCenterY:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mDotBoundryRadiusPx:F

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->generate(FFFJ)V

    iget v1, p0, Lcom/google/android/goggles/ui/RingView;->mSparklesRemaining:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/goggles/ui/RingView;->mSparklesRemaining:I

    :cond_1
    :goto_2
    iget v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->x:F

    iget v2, p0, Lcom/google/android/goggles/ui/RingView;->mCenterX:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_5

    iget v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->y:F

    iget v2, p0, Lcom/google/android/goggles/ui/RingView;->mCenterY:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_5

    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mPreviousModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    iget v6, v1, Lcom/google/android/goggles/ui/RingView$ModeState;->gogglesPlateMode:I

    goto :goto_1

    :cond_3
    iget-wide v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->tick:J

    cmp-long v1, v1, v4

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/goggles/ui/RingView;->mSparklesRemaining:I

    if-gtz v1, :cond_1

    iget-wide v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->tick:J

    const-wide/16 v13, 0x1

    sub-long v13, v4, v13

    cmp-long v1, v1, v13

    if-nez v1, :cond_1

    :cond_4
    iput-wide v4, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->tick:J

    iget v1, p0, Lcom/google/android/goggles/ui/RingView;->mCenterX:I

    int-to-float v1, v1

    iput v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->x:F

    iget v1, p0, Lcom/google/android/goggles/ui/RingView;->mCenterY:I

    int-to-float v1, v1

    iput v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->y:F

    goto :goto_2

    :cond_5
    const/high16 v1, 0x3e800000

    cmpg-float v1, v10, v1

    if-gez v1, :cond_6

    const/high16 v1, 0x3e800000

    div-float v1, v10, v1

    iput v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->alpha:F

    goto :goto_3

    :cond_6
    const/high16 v1, 0x3f000000

    cmpg-float v1, v10, v1

    if-gez v1, :cond_7

    const/high16 v1, 0x3f800000

    iput v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->alpha:F

    goto :goto_3

    :cond_7
    const/high16 v1, 0x3f800000

    sub-float/2addr v1, v10

    const/high16 v2, 0x3f000000

    div-float/2addr v1, v2

    iput v1, v0, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->alpha:F

    goto :goto_3

    :cond_8
    return-void
.end method

.method private initializeModeMap()V
    .locals 11

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/4 v10, 0x2

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/4 v10, 0x3

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/4 v2, 0x3

    const/high16 v3, 0x3f000000

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/4 v10, 0x4

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/4 v2, 0x4

    const/high16 v3, 0x3f800000

    const/high16 v4, 0x3f800000

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/4 v10, 0x5

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/4 v2, 0x5

    const/high16 v3, 0x3f000000

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000

    const/4 v8, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/4 v10, 0x6

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/4 v2, 0x6

    const/high16 v3, 0x3f800000

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000

    const/4 v8, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/16 v10, 0x8

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/16 v2, 0x8

    const/high16 v3, 0x3f800000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/16 v10, 0x9

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/16 v2, 0x9

    const/high16 v3, 0x3f800000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/4 v10, 0x7

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/4 v2, 0x7

    const/high16 v3, 0x3f800000

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v9, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    const/16 v10, 0xa

    new-instance v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    const/16 v2, 0xa

    const/high16 v3, 0x3f000000

    const/high16 v4, 0x3f800000

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/goggles/ui/RingView$ModeState;-><init>(Lcom/google/android/goggles/ui/RingView;IFFFFFZ)V

    invoke-virtual {v9, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method private interpolate(FF)F
    .locals 2
    .param p1    # F
    .param p2    # F

    sub-float v0, p2, p1

    iget v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateInterpolator:F

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    return v0
.end method

.method private setupCornerPath()V
    .locals 9

    const/high16 v8, -0x3d4c0000

    const/high16 v7, 0x40000000

    const/4 v6, 0x0

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    iget v2, p0, Lcom/google/android/goggles/ui/RingView;->mCenterX:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v1, v2

    iget v2, p0, Lcom/google/android/goggles/ui/RingView;->mCenterY:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v0, v2

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    invoke-virtual {v2, v6, v6}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    mul-float/2addr v4, v7

    iget v5, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    mul-float/2addr v5, v7

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v4, 0x43870000

    invoke-virtual {v2, v3, v4, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    invoke-virtual {v2, v6, v6}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    invoke-virtual {v2, v1, v6}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    mul-float/2addr v4, v7

    sub-float v4, v1, v4

    iget v5, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    mul-float/2addr v5, v7

    invoke-direct {v3, v4, v6, v1, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v3, v6, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    sub-float v3, v1, v3

    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    invoke-virtual {v2, v1, v6}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    sub-float v3, v1, v3

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    mul-float/2addr v4, v7

    sub-float v4, v1, v4

    iget v5, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    mul-float/2addr v5, v7

    sub-float v5, v0, v5

    invoke-direct {v3, v4, v5, v1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v4, 0x42b40000

    invoke-virtual {v2, v3, v4, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    sub-float v3, v0, v3

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    invoke-virtual {v2, v6, v0}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    sub-float v3, v0, v3

    invoke-virtual {v2, v6, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    mul-float/2addr v4, v7

    sub-float v4, v0, v4

    iget v5, p0, Lcom/google/android/goggles/ui/RingView;->mFakeCornerRadiusPx:F

    mul-float/2addr v5, v7

    invoke-direct {v3, v6, v4, v5, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/high16 v4, 0x43340000

    invoke-virtual {v2, v3, v4, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    invoke-virtual {v2, v6, v0}, Landroid/graphics/Path;->lineTo(FF)V

    return-void
.end method


# virtual methods
.method protected dispatchSetPressed(Z)V
    .locals 11
    .param p1    # Z

    const/4 v10, 0x2

    const/4 v9, 0x1

    const-wide/16 v7, 0x12c

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/goggles/ui/RingView;->mPressedDowntime:J

    sget-object v2, Lcom/google/android/goggles/ui/RingView;->BGCOLOR:Landroid/util/Property;

    new-array v3, v10, [I

    aput v6, v3, v6

    sget v4, Lcom/google/android/goggles/ui/RingView;->PRESSED_COLOR:I

    aput v4, v3, v9

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    :goto_0
    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    new-instance v3, Landroid/animation/ArgbEvaluator;

    invoke-direct {v3}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/goggles/ui/RingView;->mPressedDowntime:J

    sub-long v0, v2, v4

    cmp-long v2, v0, v7

    if-lez v2, :cond_2

    sget-object v2, Lcom/google/android/goggles/ui/RingView;->BGCOLOR:Landroid/util/Property;

    new-array v3, v10, [I

    sget v4, Lcom/google/android/goggles/ui/RingView;->PRESSED_COLOR:I

    aput v4, v3, v6

    aput v6, v3, v9

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/goggles/ui/RingView;->BGCOLOR:Landroid/util/Property;

    const/4 v3, 0x3

    new-array v3, v3, [I

    iget v4, p0, Lcom/google/android/goggles/ui/RingView;->mBgColor:I

    aput v4, v3, v6

    sget v4, Lcom/google/android/goggles/ui/RingView;->PRESSED_COLOR:I

    aput v4, v3, v9

    aput v6, v3, v10

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    add-long v3, v0, v7

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 26
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/goggles/ui/RingView;->mPreviousModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mPreviousModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    move-object/from16 v21, v0

    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/goggles/ui/RingView;->mBgColor:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    const-wide/16 v19, 0x0

    move-object/from16 v0, v21

    iget v2, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->rotatingSpeed:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->rotatingSpeed:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/goggles/ui/RingView;->mFocusStartTime:J

    sub-long v19, v2, v6

    :cond_1
    const/high16 v2, 0x43b40000

    move-wide/from16 v0, v19

    long-to-float v3, v0

    mul-float/2addr v2, v3

    const v3, 0x461c4000

    div-float/2addr v2, v3

    neg-float v2, v2

    move-object/from16 v0, v21

    iget v3, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->rotatingSpeed:F

    iget v6, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->rotatingSpeed:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6}, Lcom/google/android/goggles/ui/RingView;->interpolate(FF)F

    move-result v3

    mul-float v4, v2, v3

    const/high16 v2, 0x43340000

    move-object/from16 v0, v21

    iget v3, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->dashFraction:F

    iget v6, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->dashFraction:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6}, Lcom/google/android/goggles/ui/RingView;->interpolate(FF)F

    move-result v3

    mul-float v5, v2, v3

    const/high16 v15, 0x3f800000

    move-object/from16 v0, v21

    iget v2, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->gogglesPlateMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    iget v2, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->gogglesPlateMode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    const-wide/high16 v2, 0x3ff0000000000000L

    const-wide v6, 0x3fc99999a0000000L

    const-wide/high16 v8, 0x3fe0000000000000L

    const-wide/high16 v10, 0x3fe0000000000000L

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/goggles/ui/RingView;->mModeStateInterpolator:F

    move/from16 v22, v0

    const/high16 v23, 0x40000000

    mul-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide v24, 0x400921fb54442d18L

    mul-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    mul-double v10, v10, v22

    sub-double/2addr v8, v10

    mul-double/2addr v6, v8

    add-double/2addr v2, v6

    double-to-float v15, v2

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    const/high16 v3, 0x42000000

    move-object/from16 v0, v21

    iget v6, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->dashVisibility:F

    iget v7, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->dashVisibility:F

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/goggles/ui/RingView;->interpolate(FF)F

    move-result v6

    mul-float/2addr v3, v6

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    const/high16 v3, 0x437f0000

    move-object/from16 v0, v21

    iget v6, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->dashVisibility:F

    iget v7, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->dashVisibility:F

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/goggles/ui/RingView;->interpolate(FF)F

    move-result v6

    mul-float/2addr v3, v6

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v15

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    sub-float/2addr v3, v6

    iput v3, v2, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v6, v15

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    add-float/2addr v3, v6

    iput v3, v2, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v15

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    sub-float/2addr v3, v6

    iput v3, v2, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v6, v15

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    add-float/2addr v3, v6

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    const/high16 v2, 0x43340000

    add-float v8, v4, v2

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/goggles/ui/RingView;->mRingShadowPainter:Landroid/graphics/Paint;

    move-object/from16 v6, p1

    move v9, v5

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    const/high16 v2, 0x43340000

    add-float v8, v4, v2

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/goggles/ui/RingView;->mRingPainter:Landroid/graphics/Paint;

    move-object/from16 v6, p1

    move v9, v5

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    const/high16 v3, 0x43000000

    move-object/from16 v0, v21

    iget v6, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->discVisibility:F

    iget v7, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->discVisibility:F

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/goggles/ui/RingView;->interpolate(FF)F

    move-result v6

    mul-float/2addr v3, v6

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/goggles/ui/RingView;->mScaledRingRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object/from16 v0, v21

    iget v2, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->dotVisibility:F

    iget v3, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->dotVisibility:F

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/goggles/ui/RingView;->interpolate(FF)F

    move-result v16

    const/4 v2, 0x0

    cmpl-float v2, v16, v2

    if-eqz v2, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/google/android/goggles/ui/RingView;->calculateDots()V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/goggles/ui/RingView;->mBlinkenlights:[Lcom/google/android/goggles/ui/RingView$Blinkenlight;

    array-length v0, v12

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_2
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_5

    aget-object v13, v12, v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    const/high16 v3, 0x437f0000

    iget v6, v13, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->alpha:F

    mul-float/2addr v3, v6

    mul-float v3, v3, v16

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget v2, v13, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->x:F

    iget v3, v13, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->y:F

    iget v6, v13, Lcom/google/android/goggles/ui/RingView$Blinkenlight;->radius:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/goggles/ui/RingView;->mPreviousModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    move-object/from16 v21, v0

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    const/high16 v3, 0x437f0000

    move-object/from16 v0, v21

    iget v6, v0, Lcom/google/android/goggles/ui/RingView$ModeState;->dotVisibility:F

    iget v7, v14, Lcom/google/android/goggles/ui/RingView$ModeState;->dotVisibility:F

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/goggles/ui/RingView;->interpolate(FF)F

    move-result v6

    mul-float/2addr v3, v6

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/goggles/ui/RingView;->mCenterX:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/goggles/ui/RingView;->mCenterY:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/goggles/ui/RingView;->mCenterDotRadiusPx:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/goggles/ui/RingView;->mDotPainter:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v6, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    if-nez v2, :cond_6

    invoke-direct/range {p0 .. p0}, Lcom/google/android/goggles/ui/RingView;->setupCornerPath()V

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/goggles/ui/RingView;->mCornerPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/goggles/ui/RingView;->mCornerPainter:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method flash()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    sget-object v0, Lcom/google/android/goggles/ui/RingView;->BGCOLOR:Landroid/util/Property;

    const/4 v1, 0x3

    new-array v1, v1, [I

    aput v4, v1, v4

    const/4 v2, 0x1

    sget v3, Lcom/google/android/goggles/ui/RingView;->FLASH_COLOR:I

    aput v3, v1, v2

    const/4 v2, 0x2

    aput v4, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/animation/ArgbEvaluator;

    invoke-direct {v1}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iput-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    :cond_0
    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iput-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    :cond_1
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/goggles/ui/RingView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    div-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/google/android/goggles/ui/RingView;->mCenterX:I

    div-int/lit8 v0, p2, 0x2

    iput v0, p0, Lcom/google/android/goggles/ui/RingView;->mCenterY:I

    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/goggles/ui/RingView;->mCenterX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/goggles/ui/RingView;->mRingRadiusPx:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/goggles/ui/RingView;->mCenterY:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mRingRadiusPx:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/goggles/ui/RingView;->mCenterX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/goggles/ui/RingView;->mRingRadiusPx:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/goggles/ui/RingView;->mCenterY:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/goggles/ui/RingView;->mRingRadiusPx:F

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mRingBoundsRect:Landroid/graphics/RectF;

    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/16 v0, 0x8

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mBgColorAnimator:Landroid/animation/ObjectAnimator;

    :cond_0
    return-void
.end method

.method sparkle()V
    .locals 1

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/goggles/ui/RingView;->mSparklesRemaining:I

    return-void
.end method

.method public updateMode(I)V
    .locals 5
    .param p1    # I

    const/4 v3, 0x2

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/goggles/ui/RingView$ModeState;

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    iput-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mPreviousModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    iput-object v0, p0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    const/4 v1, 0x5

    if-ne p1, v1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/goggles/ui/RingView;->mFocusStartTime:J

    :cond_1
    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_2
    const/16 v1, 0xa

    if-ne p1, v1, :cond_4

    sget-object v1, Lcom/google/android/goggles/ui/RingView;->MODESTATE:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000

    aput v4, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    invoke-static {p1}, Lcom/google/android/goggles/ui/GogglesPlate;->isResponseMode(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/goggles/ui/RingView;->setClickable(Z)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mPreviousModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/android/goggles/ui/RingView;->ALPHA:Landroid/util/Property;

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    if-nez v1, :cond_6

    sget-object v1, Lcom/google/android/goggles/ui/RingView;->ALPHA:Landroid/util/Property;

    new-array v2, v3, [F

    fill-array-data v2, :array_1

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    goto :goto_1

    :cond_6
    sget-object v1, Lcom/google/android/goggles/ui/RingView;->MODESTATE:Landroid/util/Property;

    new-array v2, v3, [F

    fill-array-data v2, :array_2

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mCurrentModeState:Lcom/google/android/goggles/ui/RingView$ModeState;

    iget-boolean v1, v1, Lcom/google/android/goggles/ui/RingView$ModeState;->animated:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/goggles/ui/RingView;->mModeStateAnimator:Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/google/android/goggles/ui/RingView$3;

    invoke-direct {v2, p0}, Lcom/google/android/goggles/ui/RingView$3;-><init>(Lcom/google/android/goggles/ui/RingView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000
    .end array-data
.end method
