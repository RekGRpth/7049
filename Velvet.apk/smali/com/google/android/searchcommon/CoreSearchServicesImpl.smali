.class public Lcom/google/android/searchcommon/CoreSearchServicesImpl;
.super Ljava/lang/Object;
.source "CoreSearchServicesImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/CoreSearchServices;


# instance fields
.field private final mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

.field private mBeamHelper:Lcom/google/android/velvet/util/BeamHelper;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private final mCookies:Lcom/google/android/velvet/Cookies;

.field private final mCorpora:Lcom/google/android/velvet/Corpora;

.field private final mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private mImageMetadataController:Lcom/google/android/velvet/gallery/ImageMetadataController;

.field private mImageMetadataParser:Lcom/google/android/velvet/gallery/ImageMetadataParser;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

.field private mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

.field private final mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

.field private mPredictiveCardsPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

.field private final mRlzHelper:Lcom/google/android/searchcommon/google/RlzHelper;

.field private final mSearchApp:Lcom/google/android/velvet/VelvetApplication;

.field private final mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

.field private final mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mUriRewriter:Lcom/google/android/searchcommon/google/UriRewriter;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mUserAgentHelper:Lcom/google/android/searchcommon/UserAgentHelper;

.field private mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

.field private mVelvetBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

.field private final mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/velvet/VelvetApplication;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/velvet/VelvetApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createClock(Landroid/content/Context;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createUriRewriter()Lcom/google/android/searchcommon/google/UriRewriter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUriRewriter:Lcom/google/android/searchcommon/google/UriRewriter;

    new-instance v0, Lcom/google/android/searchcommon/UserAgentHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/searchcommon/UserAgentHelper;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUserAgentHelper:Lcom/google/android/searchcommon/UserAgentHelper;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createRlzHelper()Lcom/google/android/searchcommon/google/RlzHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mRlzHelper:Lcom/google/android/searchcommon/google/RlzHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    invoke-static {p1}, Lcom/google/android/velvet/Cookies;->create(Landroid/content/Context;)Lcom/google/android/velvet/Cookies;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mCookies:Lcom/google/android/velvet/Cookies;

    new-instance v0, Lcom/google/android/voicesearch/settings/Settings;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v4, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/settings/Settings;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/SearchConfig;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createCorpora()Lcom/google/android/velvet/Corpora;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mCorpora:Lcom/google/android/velvet/Corpora;

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpora;->init()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    new-instance v0, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/sidekick/inject/SystemAlarmManagerInjectable;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-direct {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v1, "mobilepersonalfeeds"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->requireAuthTokenType(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v0

    const-string v1, "oauth2:https://www.googleapis.com/auth/googlenow"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->requireAuthTokenType(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createDeviceCapabilitiesManager()Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    new-instance v0, Lcom/google/android/apps/sidekick/inject/SystemPendingIntentFactory;

    invoke-direct {v0, p1}, Lcom/google/android/apps/sidekick/inject/SystemPendingIntentFactory;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    new-instance v0, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getCorpora()Lcom/google/android/velvet/Corpora;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUserAgentHelper:Lcom/google/android/searchcommon/UserAgentHelper;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/Corpora;Lcom/google/common/base/Supplier;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    return-void
.end method

.method private createBeamHelper()Lcom/google/android/velvet/util/BeamHelper;
    .locals 3

    new-instance v0, Lcom/google/android/velvet/util/BeamHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVersionCodeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/util/BeamHelper;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    return-object v0
.end method

.method private createImageMetadataController()Lcom/google/android/velvet/gallery/ImageMetadataController;
    .locals 7

    new-instance v0, Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v4

    const-string v5, "imagemetadata"

    invoke-interface {v4, v5}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/AsyncServices;->getScheduledBackgroundExecutorService()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getImageMetadataParser()Lcom/google/android/velvet/gallery/ImageMetadataParser;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/gallery/ImageMetadataController;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/velvet/gallery/ImageMetadataParser;)V

    return-object v0
.end method

.method private createMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;
    .locals 10

    new-instance v0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v5

    const-string v6, "predictivecardssettings"

    invoke-interface {v5, v6}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v7, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v8, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getPredictiveCardsPreferences()Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;-><init>(Lcom/google/android/velvet/VelvetApplication;Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/util/Clock;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;)V

    return-object v0
.end method

.method private createSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .locals 5

    new-instance v0, Lcom/google/android/searchcommon/google/SearchBoxLogging;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v3

    const-string v4, "clicklog"

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/HttpHelper;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Clock;)V

    return-object v0
.end method

.method private getImageMetadataParser()Lcom/google/android/velvet/gallery/ImageMetadataParser;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mImageMetadataParser:Lcom/google/android/velvet/gallery/ImageMetadataParser;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/velvet/gallery/ImageMetadataParser;

    invoke-direct {v0}, Lcom/google/android/velvet/gallery/ImageMetadataParser;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mImageMetadataParser:Lcom/google/android/velvet/gallery/ImageMetadataParser;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mImageMetadataParser:Lcom/google/android/velvet/gallery/ImageMetadataParser;

    return-object v0
.end method


# virtual methods
.method protected createBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;
    .locals 9

    new-instance v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v5

    const-string v6, "background-tasks"

    invoke-interface {v5, v6}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getPendingIntentFactory()Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;-><init>(Lcom/google/android/velvet/VelvetApplication;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Ljava/util/concurrent/Executor;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V

    return-object v0
.end method

.method protected createClock(Landroid/content/Context;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/google/android/searchcommon/util/SystemClockImpl;

    invoke-direct {v0, p1}, Lcom/google/android/searchcommon/util/SystemClockImpl;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected createConfig()Lcom/google/android/searchcommon/SearchConfig;
    .locals 3

    new-instance v0, Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/SearchConfig;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;)V

    return-object v0
.end method

.method protected createCorpora()Lcom/google/android/velvet/Corpora;
    .locals 8

    new-instance v0, Lcom/google/android/velvet/Corpora;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    new-instance v4, Lcom/google/android/searchcommon/util/NetworkBytesLoader;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v5

    const/16 v6, 0x9

    invoke-direct {v4, v5, v6}, Lcom/google/android/searchcommon/util/NetworkBytesLoader;-><init>(Lcom/google/android/searchcommon/util/HttpHelper;I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v6

    const-string v7, "corpora"

    invoke-interface {v6, v7}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/Corpora;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;I)V

    return-object v0
.end method

.method protected createDeviceCapabilitiesManager()Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/DeviceCapabilityManagerImpl;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected createHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;
    .locals 7

    new-instance v0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUriRewriter:Lcom/google/android/searchcommon/google/UriRewriter;

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUserAgentHelper:Lcom/google/android/searchcommon/UserAgentHelper;

    iget-object v4, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v4

    const-string v5, "http"

    invoke-interface {v4, v5}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/HttpHelper$UrlRewriter;Lcom/google/common/base/Supplier;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;Landroid/content/Context;)V

    return-object v0
.end method

.method protected createLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;
    .locals 5

    new-instance v0, Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v3

    const-string v4, "Location-updater"

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/LocationOptIn;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method protected createLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .locals 6

    new-instance v0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v4

    const-string v5, "gaia"

    invoke-interface {v4, v5}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAllAccountNames()[Ljava/lang/String;

    return-object v0
.end method

.method protected createRlzHelper()Lcom/google/android/searchcommon/google/RlzHelper;
    .locals 5

    new-instance v0, Lcom/google/android/searchcommon/google/RlzHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v2

    const-string v3, "rlz"

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/google/RlzHelper;-><init>(Lcom/google/android/velvet/ActivityLifecycleNotifier;Ljava/util/concurrent/Executor;Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;)V

    return-object v0
.end method

.method protected createSearchSettings()Lcom/google/android/searchcommon/SearchSettings;
    .locals 4

    new-instance v0, Lcom/google/android/searchcommon/SearchSettingsImpl;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/SearchSettingsImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/SearchConfig;)V

    return-object v0
.end method

.method protected createSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .locals 13

    new-instance v9, Lcom/google/android/searchcommon/CoreSearchServicesImpl$1;

    invoke-direct {v9, p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl$1;-><init>(Lcom/google/android/searchcommon/CoreSearchServicesImpl;)V

    new-instance v0, Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getPredictiveCardsPreferences()Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getCorpora()Lcom/google/android/velvet/Corpora;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getApp()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getRlzHelper()Lcom/google/android/searchcommon/google/RlzHelper;

    move-result-object v8

    new-instance v10, Lcom/google/android/searchcommon/google/PartnerInfo;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/google/android/searchcommon/google/PartnerInfo;-><init>(Landroid/content/Context;)V

    iget-object v11, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mCookies:Lcom/google/android/velvet/Cookies;

    iget-object v12, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct/range {v0 .. v12}, Lcom/google/android/searchcommon/google/SearchUrlHelper;-><init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/velvet/Corpora;Lcom/google/android/velvet/VelvetApplication;Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/google/RlzHelper;Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/google/PartnerInfo;Lcom/google/android/velvet/Cookies;Lcom/google/android/voicesearch/settings/Settings;)V

    return-object v0
.end method

.method protected createUriRewriter()Lcom/google/android/searchcommon/google/UriRewriter;
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/google/UriRewriter;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/google/UriRewriter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected createUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .locals 4

    new-instance v0, Lcom/google/android/searchcommon/google/UserInteractionLogger;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v3, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/GsaPreferenceController;)V

    return-object v0
.end method

.method protected createWebViewGoogleCookiesLock()Lcom/google/android/searchcommon/util/ForceableLock;
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/util/ForceableLock;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/util/ForceableLock;-><init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    return-object v0
.end method

.method public getAlarmManager()Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    return-object v0
.end method

.method protected getApp()Lcom/google/android/velvet/VelvetApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchApp:Lcom/google/android/velvet/VelvetApplication;

    return-object v0
.end method

.method public declared-synchronized getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mVelvetBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mVelvetBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mVelvetBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getBeamHelper()Lcom/google/android/velvet/util/BeamHelper;
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mBeamHelper:Lcom/google/android/velvet/util/BeamHelper;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createBeamHelper()Lcom/google/android/velvet/util/BeamHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mBeamHelper:Lcom/google/android/velvet/util/BeamHelper;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mBeamHelper:Lcom/google/android/velvet/util/BeamHelper;

    return-object v0
.end method

.method public getClock()Lcom/google/android/searchcommon/util/Clock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method public getConfig()Lcom/google/android/searchcommon/SearchConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getCookies()Lcom/google/android/velvet/Cookies;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mCookies:Lcom/google/android/velvet/Cookies;

    return-object v0
.end method

.method public getCorpora()Lcom/google/android/velvet/Corpora;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mCorpora:Lcom/google/android/velvet/Corpora;

    return-object v0
.end method

.method public getDeviceCapabilityManager()Lcom/google/android/searchcommon/DeviceCapabilityManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    return-object v0
.end method

.method public getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    return-object v0
.end method

.method public declared-synchronized getImageMetadataController()Lcom/google/android/velvet/gallery/ImageMetadataController;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mImageMetadataController:Lcom/google/android/velvet/gallery/ImageMetadataController;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createImageMetadataController()Lcom/google/android/velvet/gallery/ImageMetadataController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mImageMetadataController:Lcom/google/android/velvet/gallery/ImageMetadataController;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mImageMetadataController:Lcom/google/android/velvet/gallery/ImageMetadataController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    return-object v0
.end method

.method public getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    return-object v0
.end method

.method public getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    return-object v0
.end method

.method public declared-synchronized getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    if-nez v0, :cond_0

    new-instance v2, Lcom/google/android/speech/utils/NetworkInformation;

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-direct {v2, v0, v1}, Lcom/google/android/speech/utils/NetworkInformation;-><init>(Landroid/telephony/TelephonyManager;Landroid/net/ConnectivityManager;)V

    iput-object v2, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPendingIntentFactory()Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    return-object v0
.end method

.method public getPinholeParamsBuilder()Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    return-object v0
.end method

.method public getPredictiveCardsPreferences()Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mPredictiveCardsPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    iget-object v1, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mPredictiveCardsPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mPredictiveCardsPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    return-object v0
.end method

.method public getRlzHelper()Lcom/google/android/searchcommon/google/RlzHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mRlzHelper:Lcom/google/android/searchcommon/google/RlzHelper;

    return-object v0
.end method

.method public getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    return-object v0
.end method

.method public getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

    return-object v0
.end method

.method public getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method public getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    return-object v0
.end method

.method public getUriRewriter()Lcom/google/android/searchcommon/google/UriRewriter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUriRewriter:Lcom/google/android/searchcommon/google/UriRewriter;

    return-object v0
.end method

.method public getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUserAgentHelper:Lcom/google/android/searchcommon/UserAgentHelper;

    return-object v0
.end method

.method public declared-synchronized getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getVoiceSettings()Lcom/google/android/voicesearch/settings/Settings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-object v0
.end method

.method public declared-synchronized getWebViewGoogleCookiesLock()Lcom/google/android/searchcommon/util/ForceableLock;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->createWebViewGoogleCookiesLock()Lcom/google/android/searchcommon/util/ForceableLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/CoreSearchServicesImpl;->mWebViewGoogleCookiesLock:Lcom/google/android/searchcommon/util/ForceableLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
