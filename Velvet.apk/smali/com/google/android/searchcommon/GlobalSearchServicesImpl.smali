.class public Lcom/google/android/searchcommon/GlobalSearchServicesImpl;
.super Ljava/lang/Object;
.source "GlobalSearchServicesImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/GlobalSearchServices;


# instance fields
.field protected final mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

.field private mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

.field protected final mContext:Landroid/content/Context;

.field protected final mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field private mGoogleSource:Lcom/google/android/searchcommon/google/WebSuggestSource;

.field private mHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

.field private final mImageLoadersManager:Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;

.field private final mLocationOracleSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/android/apps/sidekick/inject/LocationOracle;",
            ">;"
        }
    .end annotation
.end field

.field private mQueryThreadFactory:Ljava/util/concurrent/ThreadFactory;

.field private mSearchableSources:Lcom/google/android/searchcommon/summons/SearchableSources;

.field private mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

.field private mSourceRanker:Lcom/google/android/searchcommon/summons/SourceRanker;

.field private mSourceTaskExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private mSources:Lcom/google/android/searchcommon/summons/Sources;

.field private mSuggestionFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

.field private mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

.field private mSuggestionsProvider:Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

.field private mWebHistoryRepository:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/AsyncServices;Lcom/google/common/base/Supplier;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/CoreSearchServices;
    .param p3    # Lcom/google/android/searchcommon/AsyncServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/searchcommon/CoreSearchServices;",
            "Lcom/google/android/searchcommon/AsyncServices;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/android/apps/sidekick/inject/LocationOracle;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iput-object p3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    iput-object p4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mLocationOracleSupplier:Lcom/google/common/base/Supplier;

    new-instance v0, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;

    new-instance v1, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/AsyncServices;)V

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;-><init>(Lcom/google/android/searchcommon/imageloader/ImageLoadersFactory;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mImageLoadersManager:Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/GlobalSearchServicesImpl;)Lcom/google/android/searchcommon/summons/Sources;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/GlobalSearchServicesImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/GlobalSearchServicesImpl;)Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/GlobalSearchServicesImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mImageLoadersManager:Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;

    return-object v0
.end method

.method private createSearchHistoryHelper()Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .locals 7

    new-instance v0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    const-string v6, "history-api"

    invoke-interface {v5, v6}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v6}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;-><init>(Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/SearchConfig;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method protected createClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;
    .locals 8

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->checkStartupAtLeast(I)V

    new-instance v6, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->getSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v7

    new-instance v0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->getSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/AsyncServices;->getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;-><init>(Lcom/google/android/searchcommon/summons/Sources;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    invoke-direct {v6, v7, v0}, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;-><init>(Lcom/google/android/searchcommon/summons/Sources;Lcom/google/android/searchcommon/clicklog/ClickLog;)V

    return-object v6
.end method

.method protected createGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;
    .locals 7

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/velvet/VelvetStrictMode;->checkStartupAtLeast(I)V

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    new-instance v1, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mLocationOracleSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/inject/LocationOracle;

    invoke-direct {v1, v4, v5, v2}, Lcom/google/android/searchcommon/google/complete/CompleteServerClient;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/apps/sidekick/inject/LocationOracle;)V

    new-instance v0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;

    move-result-object v6

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;-><init>(Lcom/google/android/searchcommon/google/WebSuggestSource;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/GlobalSearchServices;Landroid/database/DataSetObservable;)V

    new-instance v1, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;

    move-result-object v5

    invoke-direct {v1, v0, v2, v4, v5}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;-><init>(Lcom/google/android/searchcommon/google/WebSuggestSource;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/database/DataSetObservable;)V

    new-instance v0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v4

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;-><init>(Lcom/google/android/searchcommon/google/WebSuggestSource;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/Clock;)V

    return-object v0
.end method

.method protected createQueryThreadFactory()Ljava/util/concurrent/ThreadFactory;
    .locals 4

    const-string v0, "QSB #%d"

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getQueryThreadPriority()I

    move-result v1

    new-instance v2, Lcom/google/common/util/concurrent/ThreadFactoryBuilder;

    invoke-direct {v2}, Lcom/google/common/util/concurrent/ThreadFactoryBuilder;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/common/util/concurrent/ThreadFactoryBuilder;->setNameFormat(Ljava/lang/String;)Lcom/google/common/util/concurrent/ThreadFactoryBuilder;

    move-result-object v2

    new-instance v3, Lcom/google/android/searchcommon/util/PriorityThreadFactory;

    invoke-direct {v3, v1}, Lcom/google/android/searchcommon/util/PriorityThreadFactory;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/common/util/concurrent/ThreadFactoryBuilder;->setThreadFactory(Ljava/util/concurrent/ThreadFactory;)Lcom/google/common/util/concurrent/ThreadFactoryBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/util/concurrent/ThreadFactoryBuilder;->build()Ljava/util/concurrent/ThreadFactory;

    move-result-object v2

    return-object v2
.end method

.method protected createShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    iget-object v1, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;-><init>(Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/searchcommon/SearchConfig;)V

    return-object v0
.end method

.method protected createSourceRanker()Lcom/google/android/searchcommon/summons/SourceRanker;
    .locals 4

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->checkStartupAtLeast(I)V

    new-instance v0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->getSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->getClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;-><init>(Lcom/google/android/searchcommon/summons/Sources;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/clicklog/ClickLog;)V

    return-object v0
.end method

.method protected createSourceTaskExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->getQueryThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/util/PerNameExecutor;

    invoke-static {v0}, Lcom/google/android/searchcommon/util/SingleThreadNamedTaskExecutor;->factory(Ljava/util/concurrent/ThreadFactory;)Lcom/google/android/searchcommon/util/Factory;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/searchcommon/util/PerNameExecutor;-><init>(Lcom/google/android/searchcommon/util/Factory;)V

    return-object v1
.end method

.method protected createSources()Lcom/google/android/searchcommon/summons/Sources;
    .locals 6

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/google/android/velvet/VelvetStrictMode;->checkStartupAtLeast(I)V

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;)V

    new-instance v2, Lcom/google/android/searchcommon/summons/SearchableSources;

    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/android/searchcommon/summons/SearchableSources;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;)V

    iput-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSearchableSources:Lcom/google/android/searchcommon/summons/SearchableSources;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSearchableSources:Lcom/google/android/searchcommon/summons/SearchableSources;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/summons/SearchableSources;->update()V

    iget-object v1, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSearchableSources:Lcom/google/android/searchcommon/summons/SearchableSources;

    new-instance v2, Lcom/google/android/searchcommon/GlobalSearchServicesImpl$1;

    invoke-direct {v2, p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl$1;-><init>(Lcom/google/android/searchcommon/GlobalSearchServicesImpl;)V

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/summons/Sources;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-object v1
.end method

.method protected createSuggestionFormatter()Lcom/google/android/searchcommon/suggest/SuggestionFormatter;
    .locals 3

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->checkStartupAtLeast(I)V

    new-instance v0, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;

    new-instance v1, Lcom/google/android/searchcommon/TextAppearanceFactory;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/searchcommon/TextAppearanceFactory;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;-><init>(Lcom/google/android/searchcommon/TextAppearanceFactory;)V

    return-object v0
.end method

.method protected createSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;
    .locals 8

    new-instance v0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iget-object v1, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    const-string v3, "suggestions_presenter"

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v6

    new-instance v7, Lcom/google/android/searchcommon/GlobalSearchServicesImpl$2;

    invoke-direct {v7, p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl$2;-><init>(Lcom/google/android/searchcommon/GlobalSearchServicesImpl;)V

    move-object v4, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;-><init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;)V

    return-object v0
.end method

.method protected createSuggestionsProvider()Lcom/google/android/searchcommon/suggest/SuggestionsProvider;
    .locals 5

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->checkStartupAtLeast(I)V

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    iget-object v1, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->getShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->getSourceTaskExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;Lcom/google/android/searchcommon/util/NamedTaskExecutor;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    return-object v0
.end method

.method protected createWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;
    .locals 6

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->checkStartupAtLeast(I)V

    new-instance v0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iget-object v1, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mAsyncServices:Lcom/google/android/searchcommon/AsyncServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/AsyncServices;->getPooledBackgroundExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/NamedTaskExecutor;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;)V

    return-object v0
.end method

.method public getClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    return-object v0
.end method

.method public getGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mGoogleSource:Lcom/google/android/searchcommon/google/WebSuggestSource;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mGoogleSource:Lcom/google/android/searchcommon/google/WebSuggestSource;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mGoogleSource:Lcom/google/android/searchcommon/google/WebSuggestSource;

    return-object v0
.end method

.method public getIcingSources()Lcom/google/android/searchcommon/summons/IcingSources;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getIconLoader()Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->getIconLoader(Ljava/lang/String;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    move-result-object v0

    return-object v0
.end method

.method public getIconLoader(Ljava/lang/String;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mImageLoadersManager:Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->getImageLoaderForPackage(Ljava/lang/String;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    move-result-object v0

    return-object v0
.end method

.method protected declared-synchronized getQueryThreadFactory()Ljava/util/concurrent/ThreadFactory;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mQueryThreadFactory:Ljava/util/concurrent/ThreadFactory;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createQueryThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mQueryThreadFactory:Ljava/util/concurrent/ThreadFactory;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mQueryThreadFactory:Ljava/util/concurrent/ThreadFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSearchHistoryHelper()Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createSearchHistoryHelper()Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createShouldQueryStrategy()Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    return-object v0
.end method

.method public getSourceRanker()Lcom/google/android/searchcommon/summons/SourceRanker;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSourceRanker:Lcom/google/android/searchcommon/summons/SourceRanker;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createSourceRanker()Lcom/google/android/searchcommon/summons/SourceRanker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSourceRanker:Lcom/google/android/searchcommon/summons/SourceRanker;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSourceRanker:Lcom/google/android/searchcommon/summons/SourceRanker;

    return-object v0
.end method

.method public getSourceTaskExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSourceTaskExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createSourceTaskExecutor()Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSourceTaskExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSourceTaskExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    return-object v0
.end method

.method public getSources()Lcom/google/android/searchcommon/summons/Sources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    return-object v0
.end method

.method public getSuggestionFormatter()Lcom/google/android/searchcommon/suggest/SuggestionFormatter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createSuggestionFormatter()Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    return-object v0
.end method

.method public getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionsPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    return-object v0
.end method

.method public getSuggestionsProvider()Lcom/google/android/searchcommon/suggest/SuggestionsProvider;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionsProvider:Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createSuggestionsProvider()Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionsProvider:Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSuggestionsProvider:Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

    return-object v0
.end method

.method public declared-synchronized getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mWebHistoryRepository:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->createWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mWebHistoryRepository:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mWebHistoryRepository:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mImageLoadersManager:Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/imageloader/ImageLoadersManager;->clearCache()V

    return-void
.end method

.method public updateSearchableSources()V
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSearchableSources:Lcom/google/android/searchcommon/summons/SearchableSources;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GlobalSearchServicesImpl;->mSearchableSources:Lcom/google/android/searchcommon/summons/SearchableSources;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/summons/SearchableSources;->update()V

    :cond_0
    return-void
.end method
