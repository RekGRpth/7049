.class public Lcom/google/android/searchcommon/GsaPreferenceController;
.super Ljava/lang/Object;
.source "GsaPreferenceController.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLock:Ljava/lang/Object;

.field private mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

.field private mPendingListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

.field private mVersionChecked:Z

.field private mWriteDelayCounter:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mContext:Landroid/content/Context;

    const-string v0, "StartupSettings"

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->openProtoPreferences(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    return-void
.end method

.method private checkVersionLocked()V
    .locals 6

    const/4 v5, 0x6

    iget-boolean v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mVersionChecked:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    const-string v1, "settings_version"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ge v4, v5, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceController;->initMainPrefsLocked()V

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    iget-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    const-string v3, "settings_version"

    invoke-static/range {v0 .. v5}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->upgrade(Landroid/content/Context;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;II)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mVersionChecked:Z

    :cond_1
    return-void
.end method

.method private initMainPrefsLocked()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    if-nez v2, :cond_2

    const-string v2, "SearchSettings"

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/GsaPreferenceController;->openProtoPreferences(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    iget-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mPendingListeners:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mPendingListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v2, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mPendingListeners:Ljava/util/ArrayList;

    :cond_1
    iget v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mWriteDelayCounter:I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->delayWrites()V

    :cond_2
    return-void
.end method

.method private openProtoPreferences(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mContext:Landroid/content/Context;

    const-string v3, "shared_prefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    invoke-direct {v2, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;-><init>(Ljava/io/File;)V

    return-object v2
.end method

.method public static useMainPreferences(Landroid/preference/PreferenceManager;)V
    .locals 1
    .param p0    # Landroid/preference/PreferenceManager;

    const-string v0, "SearchSettings"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceManager;->setSharedPreferencesMode(I)V

    return-void
.end method


# virtual methods
.method public allowWrites()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mWriteDelayCounter:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mWriteDelayCounter:I

    iget v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mWriteDelayCounter:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->allowWrites()V

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->allowWrites()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public delayWrites()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mWriteDelayCounter:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->delayWrites()V

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->delayWrites()V

    :cond_0
    iget v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mWriteDelayCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mWriteDelayCounter:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mVersionChecked:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceController;->checkVersionLocked()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceController;->initMainPrefsLocked()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mVersionChecked:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceController;->checkVersionLocked()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isMainPreferencesName(Ljava/lang/String;I)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v0, "SearchSettings"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mPendingListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mPendingListeners:Ljava/util/ArrayList;

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mPendingListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mPendingListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceController;->mPendingListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
