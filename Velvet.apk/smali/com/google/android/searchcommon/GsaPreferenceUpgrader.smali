.class public Lcom/google/android/searchcommon/GsaPreferenceUpgrader;
.super Ljava/lang/Object;
.source "GsaPreferenceUpgrader.java"

# interfaces
.implements Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;
    }
.end annotation


# static fields
.field private static final OLD_SETTINGS_PROVIDER_URI:Landroid/net/Uri;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

.field private final mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

.field private mStartupEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

.field private final mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

.field private final mVersionKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://com.google.android.voicesearch/prefs"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->OLD_SETTINGS_PROVIDER_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .param p3    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    iput-object p3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    iput-object p4, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mVersionKey:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getStartupEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method private createSettingsEditorSelector(Ljava/util/Map;)Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)",
            "Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$1;-><init>(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;Ljava/util/Map;)V

    return-object v0
.end method

.method private doApplySettings(Ljava/util/Map;Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;)V
    .locals 7
    .param p2    # Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;",
            ")V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {p2, v3}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;->selectEditor(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    instance-of v5, v4, Ljava/lang/Boolean;

    if-eqz v5, :cond_1

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-interface {v0, v3, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0

    :cond_1
    instance-of v5, v4, Ljava/lang/Integer;

    if-eqz v5, :cond_2

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v0, v3, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0

    :cond_2
    instance-of v5, v4, Ljava/lang/Long;

    if-eqz v5, :cond_3

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-interface {v0, v3, v5, v6}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0

    :cond_3
    instance-of v5, v4, Ljava/lang/String;

    if-eqz v5, :cond_4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0

    :cond_4
    instance-of v5, v4, Ljava/util/Set;

    if-eqz v5, :cond_5

    check-cast v4, Ljava/util/Set;

    invoke-interface {v0, v3, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0

    :cond_5
    instance-of v5, v4, Ljava/lang/Float;

    if-eqz v5, :cond_0

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-interface {v0, v3, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putFloat(Ljava/lang/String;F)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0

    :cond_6
    return-void
.end method

.method private doClearCorporaConfigIfAppropriate()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    const-string v2, "web_corpora_config_url"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    const-string v2, "web_corpora_config"

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    :cond_0
    return-void
.end method

.method private doCopyPersonalizedSearchValue()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    const-string v2, "personalized_search"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v2

    const-string v3, "personalized_search_bool"

    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v2, v3, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private doFetchGservicesKeysToSettings()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "gservices_overrides"

    iget-object v2, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/searchcommon/GservicesUpdateTask;->getGservicesOverridesJson(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    return-void
.end method

.method private doStoreSettings(Ljava/util/Map;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;)V
    .locals 1
    .param p2    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;",
            ")V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$2;

    invoke-direct {v0, p0, p2}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader$2;-><init>(Lcom/google/android/searchcommon/GsaPreferenceUpgrader;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doApplySettings(Ljava/util/Map;Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;)V

    return-void
.end method

.method private doUpgrade(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ge p1, p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const/4 v0, 0x6

    if-lt p2, v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->delayWrites()V

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->delayWrites()V

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doUpgradeInternal(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->allowWrites()V

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->allowWrites()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->allowWrites()V

    iget-object v1, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->allowWrites()V

    throw v0
.end method

.method private doUpgradeAlarmSettings()V
    .locals 12

    const-string v8, "AlarmUtils"

    invoke-direct {p0, v8}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getXmlPrefsAndDelete(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string v6, "_StartTimeMillis"

    const-string v5, "AlarmStartTimeMillis_"

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    const-string v8, "_StartTimeMillis"

    invoke-virtual {v3, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    instance-of v8, v7, Ljava/lang/Long;

    if-eqz v8, :cond_2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "AlarmStartTimeMillis_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10

    const-string v11, "_StartTimeMillis"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v8

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-interface {v8, v4, v9, v10}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0
.end method

.method private doUpgradeDefaultSettings()V
    .locals 2

    const-string v1, "com.google.android.googlequicksearchbox_preferences"

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getXmlPrefsAndDelete(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->createSettingsEditorSelector(Ljava/util/Map;)Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doApplySettings(Ljava/util/Map;Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;)V

    goto :goto_0
.end method

.method private doUpgradeInternal(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    if-ge p1, v4, :cond_0

    const-string v3, "StartupSettings"

    invoke-direct {p0, v3}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getXmlPrefsAndDelete(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mVersionKey:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v3, v1, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v0, v4, :cond_0

    if-gt v0, v5, :cond_0

    move p1, v0

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-direct {p0, v2, v3}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doStoreSettings(Ljava/util/Map;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;)V

    :cond_0
    if-ge p1, v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doUpgradeVoiceSearchSettings()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doUpgradeAlarmSettings()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doUpgradePredictiveCardsOptInSettings()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doUpgradeDefaultSettings()V

    :cond_1
    if-ge p1, v5, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doFetchGservicesKeysToSettings()V

    :cond_2
    const/4 v3, 0x3

    if-ge p1, v3, :cond_3

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doUpgradeSearchSettings()V

    :cond_3
    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    :cond_4
    const/4 v3, 0x5

    if-ge p1, v3, :cond_5

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doCopyPersonalizedSearchValue()V

    :cond_5
    const/4 v3, 0x6

    if-ge p1, v3, :cond_6

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doClearCorporaConfigIfAppropriate()V

    :cond_6
    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getStartupEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mVersionKey:Ljava/lang/String;

    invoke-interface {v3, v4, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    iput-object v6, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    iput-object v6, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    :cond_7
    return-void
.end method

.method private doUpgradePredictiveCardsOptInSettings()V
    .locals 2

    const-string v1, "PredictiveCardsOptInSettings"

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getXmlPrefsAndDelete(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->createSettingsEditorSelector(Ljava/util/Map;)Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doApplySettings(Ljava/util/Map;Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;)V

    goto :goto_0
.end method

.method private doUpgradeSearchSettings()V
    .locals 2

    const-string v1, "SearchSettings"

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getXmlPrefsAndDelete(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->createSettingsEditorSelector(Ljava/util/Map;)Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doApplySettings(Ljava/util/Map;Lcom/google/android/searchcommon/GsaPreferenceUpgrader$EditorSelector;)V

    goto :goto_0
.end method

.method private doUpgradeVoiceSearchSettings()V
    .locals 8

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    const-string v1, "settings_upgraded"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    const-string v1, "settings_upgraded"

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->OLD_SETTINGS_PROVIDER_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/google/android/speech/contacts/Cursors;->iterateCursor(Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v0, "Search.GsaPreferenceUpgrader"

    const-string v1, "Error during voice search settings upgrade."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainPrefs:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mMainEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    return-object v0
.end method

.method private getStartupEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mStartupEditor:Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    return-object v0
.end method

.method private getXmlPrefsAndDelete(Ljava/lang/String;)Ljava/util/Map;
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    const-string v4, "shared_prefs"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".bak"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-object v2, v0

    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :goto_0
    return-object v3

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    throw v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static upgrade(Landroid/content/Context;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;II)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .param p2    # Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I

    new-instance v0, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;Ljava/lang/String;)V

    invoke-direct {v0, p4, p5}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->doUpgrade(II)V

    return-void
.end method


# virtual methods
.method public handleCurrentRow(Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "profanityFilter"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-interface {v3, v0, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "actual_language_setting"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    const-string v4, "spoken-language-bcp-47"

    invoke-interface {v3, v4, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    goto :goto_0

    :cond_2
    const-string v3, "pref-voice-personalization-status"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/GsaPreferenceUpgrader;->getMainEditor()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    const-string v4, "personalizedResults"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBoolean(Ljava/lang/String;Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_0
.end method
