.class public Lcom/google/android/searchcommon/GservicesUpdateTask;
.super Ljava/lang/Object;
.source "GservicesUpdateTask.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClientImpl;,
        Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field static final APP_PREFIX:Ljava/lang/String; = "qsb:"

.field static final DEBUG_FEATURES_LEVEL_KEY:Ljava/lang/String; = "debug_level"


# instance fields
.field private final mAppVersion:I

.field private final mClient:Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mVsSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/voicesearch/settings/Settings;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;
    .param p4    # Lcom/google/android/voicesearch/settings/Settings;
    .param p5    # I

    new-instance v1, Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClientImpl;

    invoke-direct {v1, p1}, Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClientImpl;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/GservicesUpdateTask;-><init>(Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/voicesearch/settings/Settings;I)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/voicesearch/settings/Settings;I)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;
    .param p4    # Lcom/google/android/voicesearch/settings/Settings;
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mClient:Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;

    iput-object p2, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p4, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mVsSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput p5, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mAppVersion:I

    return-void
.end method

.method private static getDebugFeaturesLevel(Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v1

    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Search.GservicesUpdateTask"

    const-string v3, "Malformed integer value."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getGservicesOverridesJson(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClientImpl;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClientImpl;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/searchcommon/GservicesUpdateTask;->getOverridesJsonAndSetDebugLevel(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getOverridesJsonAndSetDebugLevel(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;I)Ljava/lang/String;
    .locals 15
    .param p0    # Lcom/google/android/searchcommon/SearchSettings;
    .param p1    # Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;
    .param p2    # I

    const/4 v3, 0x0

    :try_start_0
    new-instance v12, Ljava/io/StringWriter;

    invoke-direct {v12}, Ljava/io/StringWriter;-><init>()V

    new-instance v7, Landroid/util/JsonWriter;

    invoke-direct {v7, v12}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v7}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    const-string v13, "qsb:"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;->getStringsByPrefix(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    const-string v14, "qsb:"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    const-string v13, "debug_level"

    invoke-virtual {v13, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-static {v13}, Lcom/google/android/searchcommon/GservicesUpdateTask;->getDebugFeaturesLevel(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    :cond_1
    const/16 v13, 0x3a

    invoke-virtual {v8, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_2

    const/4 v13, 0x0

    invoke-virtual {v8, v13, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    move/from16 v0, p2

    invoke-static {v13, v0}, Lcom/google/android/searchcommon/GservicesUpdateTask;->matchesVersion(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_0

    add-int/lit8 v13, v2, 0x1

    invoke-virtual {v8, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    :cond_2
    invoke-virtual {v7, v8}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v14

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v14, v13}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v6

    :try_start_1
    const-string v13, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p0, :cond_3

    invoke-interface {p0, v3}, Lcom/google/android/searchcommon/SearchSettings;->setDebugFeaturesLevel(I)V

    :cond_3
    :goto_1
    return-object v13

    :cond_4
    :try_start_2
    invoke-static {}, Lcom/google/android/searchcommon/SearchConfig;->getGservicesExtraOverrideKeys()[Ljava/lang/String;

    move-result-object v1

    array-length v9, v1

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v9, :cond_6

    aget-object v8, v1, v5

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v8, v13}, Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_5

    invoke-virtual {v7, v8}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v13

    invoke-virtual {v13, v11}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_6
    invoke-virtual {v7}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    invoke-virtual {v7}, Landroid/util/JsonWriter;->close()V

    invoke-virtual {v12}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v13

    if-eqz p0, :cond_3

    invoke-interface {p0, v3}, Lcom/google/android/searchcommon/SearchSettings;->setDebugFeaturesLevel(I)V

    goto :goto_1

    :catchall_0
    move-exception v13

    if-eqz p0, :cond_7

    invoke-interface {p0, v3}, Lcom/google/android/searchcommon/SearchSettings;->setDebugFeaturesLevel(I)V

    :cond_7
    throw v13
.end method

.method private static matchesVersion(Ljava/lang/String;I)Z
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # I

    const/4 v4, 0x0

    const/16 v5, 0x2c

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-ltz v0, :cond_1

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :goto_0
    if-lt p1, v2, :cond_0

    if-gt p1, v1, :cond_0

    const/4 v4, 0x1

    :cond_0
    :goto_1
    return v4

    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    move v1, v2

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v5, "Search.GservicesUpdateTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error parsing gservices version spec "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/GservicesUpdateTask;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 4

    const-string v0, "Search.GservicesUpdateTask"

    const-string v1, "Updating Gservices keys"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v1, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v2, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mClient:Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;

    iget v3, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mAppVersion:I

    invoke-static {v1, v2, v3}, Lcom/google/android/searchcommon/GservicesUpdateTask;->getOverridesJsonAndSetDebugLevel(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/GservicesUpdateTask$GservicesClient;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setGservicesOverridesJson(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getDebugFeaturesLevel()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/searchcommon/debug/DebugFeatures;->setDebugLevel(I)V

    iget-object v0, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mVsSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->updateStaticConfiguration()V

    iget-object v0, p0, Lcom/google/android/searchcommon/GservicesUpdateTask;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->clearCachedValues()V

    const/4 v0, 0x0

    return-object v0
.end method
