.class public Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;
.super Ljava/lang/Object;
.source "MarinerOptInSettingsImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/MarinerOptInSettings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInReceiver;,
        Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;
    }
.end annotation


# static fields
.field static final ANY_ACCOUNT_CAN_RUN_THEGOOGLE_PREF:Ljava/lang/String; = "any_account_can_run_the_google"

.field private static final CONFIGURATION_TIMESTAMP_PREFIX:Ljava/lang/String; = "last_configuration_saved_time_"

.field private static final DBG:Z = false

.field public static final FIRST_RUN_SCREENS_PREF:Ljava/lang/String; = "first_run_screens"

.field private static final LOCATION_OPT_IN_ACTION:Ljava/lang/String; = "com.google.android.apps.maps.googlenav.friend.internal.OPT_IN"

.field private static final LOCATION_OPT_IN_START_TIME_IN_MILLIS_PREF:Ljava/lang/String; = "location_opt_in_start_time"

.field private static final LOCATION_OPT_IN_TIMEOUT_MILLIS:J = 0x5265c00L

.field private static final OPTED_IN_VERSION_PREFIX:Ljava/lang/String; = "opted_in_version_"

.field private static final OPTIN_TIMEOUT_MILLIS:J = 0x3a98L

.field private static final RESULT_MESSAGES:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static final USER_CAN_RUN_THEGOOGLE_PREF_PREFIX:Ljava/lang/String; = "user_can_run_the_google_"


# instance fields
.field private final mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mContext:Landroid/content/Context;

.field private mIdentifierIntent:Landroid/app/PendingIntent;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private final mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mServiceHandlers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->TAG:Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Success"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Package or certificate invalid"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Different user signed in to GMM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "No user signed in to GMM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Unsupported version"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Invalid request"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Server error: failed to opt-in"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Cannot log into non-primary account"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->RESULT_MESSAGES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/velvet/VelvetApplication;Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/util/Clock;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/VelvetApplication;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;
    .param p5    # Ljava/util/concurrent/Executor;
    .param p6    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p7    # Lcom/google/android/searchcommon/SearchConfig;
    .param p8    # Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;
    .param p9    # Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mServiceHandlers:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    iput-object p2, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object p4, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p5, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p7, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p8, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    iput-object p9, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)Landroid/app/PendingIntent;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mIdentifierIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mServiceHandlers:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$300()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->RESULT_MESSAGES:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->cancelLatitudeOptInAlarm()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;Landroid/accounts/Account;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;
    .param p1    # Landroid/accounts/Account;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->updateWhetherAccountCanRunTheGoogle(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)Lcom/google/android/velvet/VelvetApplication;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    return-object v0
.end method

.method private cancelLatitudeOptInAlarm()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getLocationReportingOptInIntent()Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v1, v0}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getMainPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    const-string v2, "location_opt_in_start_time"

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method private getAccountOptedInKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "opted_in_version_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getConfigurationTimestampKey(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/accounts/Account;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "last_configuration_saved_time_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getIdentifierIntent()Landroid/app/PendingIntent;
    .locals 4

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "identity"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x40000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private getLoggedInAccount()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private getMainPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method private getOptInPageVersion()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method private getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getStartupPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method private getUserCanRunTheGoogleKey(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/accounts/Account;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "user_can_run_the_google_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hasLocationReportingOptInExpired()Z
    .locals 8

    const-wide/16 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getMainPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v4

    const-string v5, "location_opt_in_start_time"

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v4, v2, v6

    if-lez v4, :cond_0

    sub-long v4, v0, v2

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private isAccountWhitelisted(Landroid/accounts/Account;)Z
    .locals 5
    .param p1    # Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->getDomainWhitelist()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/collect/ImmutableList;->copyOf([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/16 v3, 0x40

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isStartupSetting(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "first_run_screens"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "any_account_can_run_the_google"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "opted_in_version_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "user_can_run_the_google_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "last_configuration_saved_time_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isUserWhitelisted()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getLoggedInAccount()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->isAccountWhitelisted(Landroid/accounts/Account;)Z

    move-result v1

    goto :goto_0
.end method

.method private setAccountCanRunTheGoogle(ILandroid/accounts/Account;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getUserCanRunTheGoogleKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method private timeStampConfigurationSave(Landroid/accounts/Account;)V
    .locals 5
    .param p1    # Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getConfigurationTimestampKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method private updateWhetherAccountCanRunTheGoogle(Landroid/accounts/Account;)V
    .locals 6
    .param p1    # Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v1

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    new-instance v4, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setFetchConfigurationQuery(Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithoutLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchConfigurationResponse()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getFetchConfigurationResponse()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;->hasConfiguration()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getFetchConfigurationResponse()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;->getConfiguration()Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->saveConfiguration(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    const-string v4, "any_account_can_run_the_google"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->userCanRunTheGoogle(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)I

    move-result v3

    invoke-direct {p0, v3, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->setAccountCanRunTheGoogle(ILandroid/accounts/Account;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getUserCanRunTheGoogleKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->TAG:Ljava/lang/String;

    const-string v4, "Failed to fetch default configuration"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x3

    invoke-direct {p0, v3, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->setAccountCanRunTheGoogle(ILandroid/accounts/Account;)V

    goto :goto_0
.end method


# virtual methods
.method public canAccountRunTheGoogle(Landroid/accounts/Account;)I
    .locals 14
    .param p1    # Landroid/accounts/Account;

    const/4 v0, 0x0

    const/4 v7, 0x3

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v8

    const-string v9, "any_account_can_run_the_google"

    invoke-interface {v8, v9, v7}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    move v6, v7

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v8

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getUserCanRunTheGoogleKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getInt(Ljava/lang/String;I)I

    move-result v6

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->isUserWhitelisted()Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    const v9, 0x7f0b0019

    invoke-virtual {v8, v9}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getSavedConfigurationTimestamp(Landroid/accounts/Account;)J

    move-result-wide v8

    int-to-long v10, v1

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long v2, v8, v10

    iget-object v8, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v8, v4, v2

    if-lez v8, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-eqz v6, :cond_3

    if-eq v6, v7, :cond_3

    if-eqz v0, :cond_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v8, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$1;

    invoke-direct {v8, p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$1;-><init>(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;Landroid/accounts/Account;)V

    invoke-interface {v7, v8}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_4
    iget-object v8, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    const v9, 0x7f0b0018

    invoke-virtual {v8, v9}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v1

    goto :goto_1
.end method

.method public disableForAccount(Landroid/accounts/Account;)V
    .locals 4
    .param p1    # Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->stopTgServices()V

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->clearWorkingConfiguration()V

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->optAccountOut(Landroid/accounts/Account;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getCalendarDataProvider()Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/calendar/CalendarDataProvider;->clearData()V

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getExecutedUserActionStore()Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;->postDeleteStore()V

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNowNotificationManager()Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/notifications/NowNotificationManager;->cancelAll()V

    return-void
.end method

.method public domainIsBlockedFromNow(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)Z
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Configuration;
    .param p2    # Landroid/accounts/Account;

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->isAccountWhitelisted(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasDasherConfiguration()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getDasherConfiguration()Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$DasherConfiguration;->getPredictiveUiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fetchAccountConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;
    .locals 5
    .param p1    # Landroid/accounts/Account;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    new-instance v4, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;

    invoke-direct {v4}, Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setFetchConfigurationQuery(Lcom/google/geo/sidekick/Sidekick$FetchConfigurationQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v0

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithoutLocationWithAccount(Lcom/google/geo/sidekick/Sidekick$RequestPayload;Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchConfigurationResponse()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getFetchConfigurationResponse()Lcom/google/geo/sidekick/Sidekick$FetchConfigurationResponse;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method getLocationReportingOptInIntent()Landroid/app/PendingIntent;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public getSavedConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 3
    .param p1    # Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getMasterConfigurationFor(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getConfigurationTimestampKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    :cond_0
    return-object v0
.end method

.method public getSavedConfigurationTimestamp(Landroid/accounts/Account;)J
    .locals 4
    .param p1    # Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getConfigurationTimestampKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public isAccountOptedIn(Landroid/accounts/Account;)Z
    .locals 4
    .param p1    # Landroid/accounts/Account;

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getAccountOptedInKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    const/4 v3, -0x1

    invoke-interface {v2, v0, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getOptInPageVersion()I

    move-result v3

    if-lt v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUserOptedIn()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getLoggedInAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->isAccountOptedIn(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method public localeIsBlockedFromNow(Lcom/google/geo/sidekick/Sidekick$Configuration;)Z
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Configuration;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasLocaleConfiguration()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getLocaleConfiguration()Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;->getEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public optAccountIn(Landroid/accounts/Account;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;

    if-eqz p1, :cond_0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getAccountOptedInKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getOptInPageVersion()I

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public optAccountOut(Landroid/accounts/Account;)V
    .locals 2
    .param p1    # Landroid/accounts/Account;

    if-eqz p1, :cond_0

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getAccountOptedInKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public optIntoLatitude()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getMainPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v7

    const-string v0, "location_opt_in_start_time"

    invoke-interface {v7, v0, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v7}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getLocationReportingOptInIntent()Landroid/app/PendingIntent;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    const/4 v1, 0x0

    const-wide/32 v4, 0x36ee80

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->setRepeating(IJJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/GsaPreferenceController;->registerChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public saveConfiguration(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Configuration;
    .param p2    # Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->setMasterConfigurationFor(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;Z)V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->userCanRunTheGoogle(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getUserCanRunTheGoogleKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->timeStampConfigurationSave(Landroid/accounts/Account;)V

    return-void
.end method

.method public sendLocationReportingOptInIntent()V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->hasLocationReportingOptInExpired()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->cancelLatitudeOptInAlarm()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getIdentifierIntent()Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mIdentifierIntent:Landroid/app/PendingIntent;

    new-instance v2, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;

    invoke-direct {v2, p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;-><init>(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mServiceHandlers:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.maps.googlenav.friend.internal.OPT_IN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v3, "account"

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getLoggedInAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "messenger"

    new-instance v4, Landroid/os/Messenger;

    invoke-direct {v4, v2}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "sender"

    iget-object v4, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mIdentifierIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "version"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, -0x1

    const-wide/16 v4, 0x3a98

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$LocationReportingOptInServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v3, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    const v4, 0x7f0d00e4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public setFirstRunScreensShown()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    const-string v2, "first_run_screens"

    invoke-interface {v1, v2, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putInt(Ljava/lang/String;I)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method public stopServicesIfUserOptedOut()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->isUserOptedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$2;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl$2;-><init>(Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/GsaPreferenceController;->unregisterChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public updateConfigurationForAccount(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;)Z
    .locals 2
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Configuration;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->setMasterConfigurationFor(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;Z)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->timeStampConfigurationSave(Landroid/accounts/Account;)V

    invoke-virtual {p0, p2, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->userCanRunTheGoogle(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateLastConfigurationTimestamp(Landroid/accounts/Account;)V
    .locals 4
    .param p1    # Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getConfigurationTimestampKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putLong(Ljava/lang/String;J)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    return-void
.end method

.method public userCanRunTheGoogle(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)I
    .locals 1
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Configuration;
    .param p2    # Landroid/accounts/Account;

    if-nez p1, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->domainIsBlockedFromNow(Lcom/google/geo/sidekick/Sidekick$Configuration;Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->localeIsBlockedFromNow(Lcom/google/geo/sidekick/Sidekick$Configuration;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public userHasSeenFirstRunScreens()Z
    .locals 5

    iget-object v2, p0, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/MarinerOptInSettingsImpl;->getStartupPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    const-string v3, "first_run_screens"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lt v1, v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
