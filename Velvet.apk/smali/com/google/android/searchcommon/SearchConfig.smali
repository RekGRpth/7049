.class public Lcom/google/android/searchcommon/SearchConfig;
.super Ljava/lang/Object;
.source "SearchConfig.java"


# static fields
.field private static final DBG:Z = false

.field private static final DEVICE_COUNTRY_KEY:Ljava/lang/String; = "device_country"

.field public static final NATIVE_IG_ALWAYS:I = 0x1

.field public static final NATIVE_IG_DISABLED:I = 0x0

.field public static final NATIVE_IG_NON_JESR:I = 0x2

.field private static final NUM_GSERVICES_OVERRIDE_KEYS:I = 0x100

.field private static final QUERY_THREAD_PRIORITY:I = 0x9

.field public static final RLZ_PROVIDER_URI:Landroid/net/Uri;

.field private static final SOUND_SEARCH_GMS_DISABLE_KEY:Ljava/lang/String; = "gms_disable:com.google.android.ears"

.field private static final TAG:Ljava/lang/String; = "Search.SearchConfig"


# instance fields
.field private final mCachedStringMaps:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mCachedStringSets:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mDeviceCountryOverride:Ljava/lang/String;

.field private final mGsResourceKeys:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private volatile mResourceOverrides:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://com.google.android.partnersetup.rlzappprovider/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/SearchConfig;->RLZ_PROVIDER_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringSets:Landroid/util/SparseArray;

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringMaps:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/GsaPreferenceController;

    const/16 v2, 0x100

    const/4 v1, 0x5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/SearchConfig;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/SearchConfig;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringSets:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringMaps:Landroid/util/SparseArray;

    invoke-direct {p0}, Lcom/google/android/searchcommon/SearchConfig;->fillGservicesResourceKeys()V

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-gt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static convertBooleanOverride(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2

    sget-object v0, Lcom/google/android/gsf/Gservices;->TRUE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gsf/Gservices;->FALSE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_1
    const-string v0, "Search.SearchConfig"

    const-string v1, "Invalid gservices boolean"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static convertIntegerOverride(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "Search.SearchConfig"

    const-string v1, "Invalid gservices int"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static convertStringArrayOverride(Ljava/lang/String;Z)[Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/google/android/searchcommon/util/Util;->jsonToStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ","

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private fillGservicesResourceKeys()V
    .locals 2

    const v0, 0x7f0a0005

    const-string v1, "allow_ssl_search"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0018

    const-string v1, "background_task_min_periods"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0025

    const-string v1, "background_tasks_period_mins"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0027

    const-string v1, "background_tasks_max_period_days"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0026

    const-string v1, "background_tasks_period_days_of_disuse_squared_multiple_hours"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0007

    const-string v1, "complete_server_domain_name"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0008

    const-string v1, "complete_server_suggest_path"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0009

    const-string v1, "complete_server_remove_history_path"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0007

    const-string v1, "default_source_uris"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0009

    const-string v1, "default_sources"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f000a

    const-string v1, "google_search_paths"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f000b

    const-string v1, "google_search_logout_redirects"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b001a

    const-string v1, "native_images_from_web_enabled_min_version"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0011

    const-string v1, "image_metadata_client_id"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d000f

    const-string v1, "image_metadata_url_pattern"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b001b

    const-string v1, "image_metadata_chunk_size"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0011

    const-string v1, "trusted_source_uris"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0012

    const-string v1, "trusted_sources"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0015

    const-string v1, "full_size_icon_source_suggest_uris"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0003

    const-string v1, "default_num_visible_suggestion_slots"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0004

    const-string v1, "deleted_query_propagation_delay_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0002

    const-string v1, "show_zero_query_suggestions"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0002

    const-string v1, "concurrent_source_queries"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0005

    const-string v1, "source_timeout_millis"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0009

    const-string v1, "max_concurrent_source_queries"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b001d

    const-string v1, "max_image_card_result_rows"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b001e

    const-string v1, "max_local_history_age_days"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b000a

    const-string v1, "max_promoted_suggestions"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b000c

    const-string v1, "max_promoted_summons_per_source_initial"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b000d

    const-string v1, "max_promoted_summons_per_source_increase"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b000e

    const-string v1, "max_results_per_source"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b000f

    const-string v1, "max_stat_age_hours"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0010

    const-string v1, "max_suggestions_above_summons"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0011

    const-string v1, "min_clicks_for_source_ranking"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0012

    const-string v1, "min_suggestions_above_summons"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0013

    const-string v1, "min_visible_summons"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0014

    const-string v1, "new_concurrent_source_query_delay"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0015

    const-string v1, "publish_result_delay_millis"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0017

    const-string v1, "typing_update_suggestions_delay_millis"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d000a

    const-string v1, "complete_server_client_id"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0005

    const-string v1, "complete_server_extra_params"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d000c

    const-string v1, "search_domain_check_pattern"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d000e

    const-string v1, "search_url_format_with_scheme"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0013

    const-string v1, "manage_history_url_format"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d000b

    const-string v1, "default_search_source_param"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d001c

    const-string v1, "search_domain_override"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d001d

    const-string v1, "search_single_req_domain_override"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d001e

    const-string v1, "service_personalized_search"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d001f

    const-string v1, "majel_domain_override"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0009

    const-string v1, "dedupe_user_query"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0023

    const-string v1, "refresh_search_history_delay"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0024

    const-string v1, "refresh_search_parameters_cookie_refresh_days"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0028

    const-string v1, "remove_from_history_button_delay"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0032

    const-string v1, "http_cache_size"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a000a

    const-string v1, "suggest_look_ahead_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0006

    const-string v1, "suggestion_cache_timeout_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0007

    const-string v1, "suggestion_cache_max_values"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d002b

    const-string v1, "user_agent_param_format"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0010

    const-string v1, "enable_answers"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0035

    const-string v1, "location_expirey_time"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a000b

    const-string v1, "word_by_word_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0013

    const-string v1, "web_corpus_query_param_blacklist"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a000e

    const-string v1, "deploy_the_google"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b002e

    const-string v1, "mariner_staleness_timeout_minutes"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b002f

    const-string v1, "mariner_background_refresh_rate_limit_minutes"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0030

    const-string v1, "mariner_background_refresh_interval_minutes"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f001c

    const-string v1, "clicked_result_destination_params"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0014

    const-string v1, "locales_with_answer_patterns"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a000d

    const-string v1, "enable_ad_click_handling"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0023

    const-string v1, "clicked_ad_url_path"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0022

    const-string v1, "clicked_result_url_path"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f001a

    const-string v1, "click_ad_url_exception_patterns"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f001b

    const-string v1, "click_ad_url_substitutions"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0024

    const-string v1, "default_corpora_config_uri"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0025

    const-string v1, "corpora_config_uri_24_plus"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d001b

    const-string v1, "google_ssl_prewarm_pattern"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d001a

    const-string v1, "google_jesr_base_pattern"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0033

    const-string v1, "hide_web_results_delay_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0034

    const-string v1, "hide_web_results_js_callback_timeout"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0008

    const-string v1, "jesr_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b001c

    const-string v1, "jesr_timeout_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b002c

    const-string v1, "max_initial_web_corpus_selectors"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0020

    const-string v1, "register_gsa_bridge_javascript"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b002b

    const-string v1, "suggestion_view_recycle_bin_size"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0029

    const-string v1, "suggest_num_visible_summons_rows"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b002a

    const-string v1, "summons_rows_per_source"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0021

    const-string v1, "velvetgsabridge_interface_name"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0026

    const-string v1, "web_corpus_query_param"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0027

    const-string v1, "web_mode_query_param"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0016

    const-string v1, "domain_whitelist"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0018

    const-string v1, "saved_configuration_expiry_seconds"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0019

    const-string v1, "saved_whitelisted_configuration_expiry_seconds"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f001d

    const-string v1, "path_override_whitelist"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0037

    const-string v1, "webview_login_load_timeout_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0038

    const-string v1, "webview_login_redirect_timeout_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0039

    const-string v1, "webview_suppress_previous_results_for_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f001e

    const-string v1, "gws_cgi_param_query_equal_whitelist"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f001f

    const-string v1, "gws_cgi_param_changes_for_back_navigation"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0f0020

    const-string v1, "gws_path_whitelist_for_back_navigation"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a000c

    const-string v1, "correction_spans_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0012

    const-string v1, "enable_image_metadata_generic_chunking"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b003a

    const-string v1, "image_fetch_more_threshold"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a000f

    const-string v1, "enable_beam_support"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b003b

    const-string v1, "answer_card_supress_percentage"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0028

    const-string v1, "suppressed_card_query_param"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0029

    const-string v1, "suppressed_card_query_param_value"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b003c

    const-string v1, "native_ig_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0013

    const-string v1, "debug_always_prefetch"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b003d

    const-string v1, "prefetch_ttl_millis"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b003e

    const-string v1, "prefetch_cache_entries"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b003f

    const-string v1, "prefetch_simultaneous_downloads"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0040

    const-string v1, "prefetch_throttle_period_millis"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d002a

    const-string v1, "google_gen_204_pattern"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0041

    const-string v1, "static_map_cache_max_size"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0014

    const-string v1, "enable_goggles"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0015

    const-string v1, "enable_goggles_barcoding"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0042

    const-string v1, "goggles_max_session_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0043

    const-string v1, "goggles_scene_change_delay_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0016

    const-string v1, "enable_time_to_leave_notifications"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0017

    const-string v1, "personalized_search_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d002c

    const-string v1, "device_country"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d002d

    const-string v1, "gms_disable:com.google.android.ears"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0018

    const-string v1, "single_request_architecture_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d002e

    const-string v1, "s3_service_voice_actions_single_request"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d002f

    const-string v1, "s3_server_override_single_request"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0019

    const-string v1, "enable_spdy"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a001a

    const-string v1, "enable_spdy_for_http_helper"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0044

    const-string v1, "intercepted_request_header_timeout_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0045

    const-string v1, "in_app_loading_indicator_delay_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0030

    const-string v1, "history_api_lookup_url_pattern"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0031

    const-string v1, "history_api_change_url_pattern"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0032

    const-string v1, "history_api_client_param"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a0006

    const-string v1, "should_supply_csl_param_for_image_metadata"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0d0010

    const-string v1, "csl_value_for_image_metadata"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a001b

    const-string v1, "action_discovery_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b0036

    const-string v1, "music_detection_timeout_ms"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a001c

    const-string v1, "use_http_for_voice"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a001d

    const-string v1, "test_platform_logging_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0a001e

    const-string v1, "debug_audio_logging_enabled"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    const v0, 0x7f0b002d

    const-string v1, "reminders_feature_enabled_level"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->putResourceKey(ILjava/lang/String;)V

    return-void
.end method

.method private getBoolean(I)Z
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/searchcommon/SearchConfig;->mResourceOverrides:Landroid/util/SparseArray;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    :goto_0
    return v2

    :cond_0
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/searchcommon/SearchConfig;->convertBooleanOverride(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/SearchConfig;->loadOverrides()V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    goto :goto_0
.end method

.method private getFixedDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/wireless/gdata/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/SearchConfig;->loadOverrides()V

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mDeviceCountryOverride:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mDeviceCountryOverride:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getCountryDomainOverride(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method static getGservicesExtraOverrideKeys()[Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "device_country"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "gms_disable:com.google.android.ears"

    aput-object v2, v0, v1

    return-object v0
.end method

.method private getString(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/searchcommon/SearchConfig;->mResourceOverrides:Landroid/util/SparseArray;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/SearchConfig;->loadOverrides()V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getStringArray(IZ)[Ljava/lang/String;
    .locals 3
    .param p1    # I
    .param p2    # Z

    iget-object v1, p0, Lcom/google/android/searchcommon/SearchConfig;->mResourceOverrides:Landroid/util/SparseArray;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, [Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/google/android/searchcommon/SearchConfig;->convertStringArrayOverride(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/SearchConfig;->loadOverrides()V

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private declared-synchronized getStringMap(I)Ljava/util/Map;
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringMaps:Landroid/util/SparseArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    move-object v2, v1

    :goto_0
    monitor-exit p0

    return-object v2

    :cond_0
    const/4 v5, 0x0

    :try_start_1
    invoke-direct {p0, p1, v5}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    array-length v5, v3

    rem-int/lit8 v5, v5, 0x2

    if-nez v5, :cond_1

    const/4 v4, 0x1

    :cond_1
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v0, 0x0

    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_2

    aget-object v4, v3, v0

    add-int/lit8 v5, v0, 0x1

    aget-object v5, v3, v5

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringMaps:Landroid/util/SparseArray;

    invoke-virtual {v4, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v1

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private declared-synchronized getStringSet(I)Ljava/util/Set;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringSets:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    move-object v1, v0

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, p1, v2}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringSets:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static isGmsEnabled(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, ","

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-eqz v2, :cond_0

    aget-object v2, v1, v0

    invoke-static {v2}, Lcom/google/wireless/gdata/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    aget-object v1, v1, v0

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const-string v0, "enabled"

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private declared-synchronized loadOverrides()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mResourceOverrides:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    const-string v1, "gservices_overrides"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/util/Util;->jsonToStringMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_3

    const-string v0, "device_country"

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mDeviceCountryOverride:Ljava/lang/String;

    new-instance v1, Landroid/util/SparseArray;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v1, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mResourceOverrides:Landroid/util/SparseArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public areNativeImagesFromWebEnabled()Z
    .locals 2

    const v1, 0x7f0b001a

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/SearchConfig;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode(Landroid/content/Context;)I

    move-result v1

    if-lt v1, v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public areSearchPlateAndFooterStickyInWebSearch()Z
    .locals 1

    const v0, 0x7f0a0003

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized clearCachedValues()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mResourceOverrides:Landroid/util/SparseArray;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mDeviceCountryOverride:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringSets:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mCachedStringMaps:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cslValueForImageMetadata()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0010

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAdClickHandlingEnabled()Z
    .locals 1

    const v0, 0x7f0a000d

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public getAdClickUrlSubstitutions()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const v3, 0x7f0f001b

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    aget-object v3, v1, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v1, v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public getAllowBackFromUrlWhitelist()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f0020

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAnswersEnabled()Z
    .locals 1

    const v0, 0x7f0a0010

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public getApplicationsSuggestUri()Landroid/net/Uri;
    .locals 1

    const v0, 0x7f0d0012

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundTaskMinPeriods()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f0018

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundTasks()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f0017

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBackgroundTasksPeriodDaysOfDisuseSquaredMultipleMs()J
    .locals 4

    const v0, 0x7f0b0026

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getBackgroundTasksPeriodMs()J
    .locals 4

    const v0, 0x7f0b0025

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method protected declared-synchronized getBytesFromAndroidResource(Landroid/net/Uri;)[B
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.resource"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/searchcommon/util/Util;->loadBytesFromRawResource(Landroid/content/Context;Landroid/net/Uri;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCardSuppressedQueryParam()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0028

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCardSuppressedQueryParamValue()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0029

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCardSuppressionPercentage()I
    .locals 1

    const v0, 0x7f0b003b

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getChangingParamsThatAllowBackNavigation()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const v0, 0x7f0f001f

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getClickedAdUrlPath()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0023

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getClickedResultDestinationParams()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f001c

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getClickedResultUrlPath()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0022

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCommitPublishedResultsDelayMillis()I
    .locals 1

    const v0, 0x7f0b0001

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getCompleteServerClientId()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d000a

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCompleteServerDomainName()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0007

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCompleteServerExtraParams()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f0005

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCompleteServerPath()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0008

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConcurrentSourceQueries()I
    .locals 1

    const v0, 0x7f0b0002

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getCorporaConfigUri()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0025

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCountryDomainOverride(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const v0, 0x7f0f0006

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringMap(I)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultCorpora()[B
    .locals 2

    const v1, 0x7f0d0024

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBytesFromAndroidResource(Landroid/net/Uri;)[B

    move-result-object v1

    return-object v1
.end method

.method public getDefaultNumVisibleSuggestionSlots()I
    .locals 1

    const v0, 0x7f0b0003

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getDeletedQueryPropagationDelayMs()I
    .locals 1

    const v0, 0x7f0b0004

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getDeviceCountry()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d002c

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDomainWhitelist()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f0016

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFixedMajelDomain()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d001f

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getFixedDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFixedSearchDomain()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d001c

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getFixedDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGogglesMaxSessionMs()I
    .locals 1

    const v0, 0x7f0b0042

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getGogglesSceneChangeDelayMs()I
    .locals 1

    const v0, 0x7f0b0043

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getGoogleGen204Pattern()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d002a

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGoogleJesrBasePattern()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d001a

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGooglePathOverrideWhitelist()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f001d

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGoogleSslPrewarmPattern()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d001b

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGwsParamsAffectingQueryEquivalenceForSearch()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const v0, 0x7f0f001e

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getHistoryApiChangeUrlPattern()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0031

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHistoryApiClientId()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0032

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHistoryApiLookupUrlPattern()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0030

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHttpCacheSize()J
    .locals 2

    const v0, 0x7f0b0032

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getHttpConnectTimeout()I
    .locals 1

    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getHttpReadTimeout()I
    .locals 1

    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getImageFetchMoreThreshold()I
    .locals 1

    const v0, 0x7f0b003a

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getImageMetadataChunkSize()I
    .locals 1

    const v0, 0x7f0b001b

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getImageMetadataClientId()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0011

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageMetadataGulpNames()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f0021

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageMetatDataUrlPattern()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d000f

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInAppLoadingIndicatorDelayMs()I
    .locals 1

    const v0, 0x7f0b0045

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/searchcommon/SearchConfig;->mResourceOverrides:Landroid/util/SparseArray;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_0
    return v2

    :cond_0
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/searchcommon/SearchConfig;->convertIntegerOverride(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/SearchConfig;->loadOverrides()V

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/searchcommon/SearchConfig;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    goto :goto_0
.end method

.method public getInterceptedRequestHeaderTimeoutMs()I
    .locals 1

    const v0, 0x7f0b0044

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getJesrTimeoutMs()I
    .locals 1

    const v0, 0x7f0b001c

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLocationExpiryTimeSeconds()I
    .locals 1

    const v0, 0x7f0b0035

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getManageSearchHistoryUrlFormat()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0013

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMarinerBackgroundRefreshIntervalMinutes()I
    .locals 1

    const v0, 0x7f0b0030

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMarinerBackgroundRefreshRateLimitMinutes()I
    .locals 1

    const v0, 0x7f0b002f

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMarinerMaximumStaleDataRefreshDistanceMeters()I
    .locals 1

    const v0, 0x7f0b0031

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMarinerStalenessTimeoutMinutes()I
    .locals 1

    const v0, 0x7f0b002e

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxBackgroundTasksPeriodMs()J
    .locals 4

    const v0, 0x7f0b0027

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getMaxConcurrentSourceQueries()I
    .locals 1

    const v0, 0x7f0b0009

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxImageCardResultRows()I
    .locals 1

    const v0, 0x7f0b001d

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxInitialWebCorpusSelectors()I
    .locals 1

    const v0, 0x7f0b002c

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxLocalHistoryAgeMillis()J
    .locals 4

    const v0, 0x7f0b001e

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getMaxLocalHistoryForFullWebResults()I
    .locals 1

    const v0, 0x7f0b0020

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxLocalHistoryForPartialWebResults()I
    .locals 1

    const v0, 0x7f0b0021

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxLocalHistorySuggestions()I
    .locals 1

    const v0, 0x7f0b001f

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxPromotedSuggestions()I
    .locals 1

    const v0, 0x7f0b000a

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxPromotedSummons()I
    .locals 1

    const v0, 0x7f0b000b

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxPromotedSummonsPerSourceIncrease()I
    .locals 1

    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxPromotedSummonsPerSourceInitial()I
    .locals 1

    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxResultsPerSource()I
    .locals 1

    const v0, 0x7f0b000e

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMaxStatAgeMillis()J
    .locals 4

    const v0, 0x7f0b000f

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getMaxSuggestionsAboveSummons()I
    .locals 1

    const v0, 0x7f0b0010

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMinClicksForSourceRanking()I
    .locals 1

    const v0, 0x7f0b0011

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMinSuggestionsAboveSummons()I
    .locals 1

    const v0, 0x7f0b0012

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMinVisibleSummons()I
    .locals 1

    const v0, 0x7f0b0013

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMusicDetectionTimeoutMs()I
    .locals 1

    const v0, 0x7f0b0036

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getNewConcurrentSourceQueryDelay()I
    .locals 1

    const v0, 0x7f0b0014

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getPartialWebResultsThresholdForLocalHistory()I
    .locals 1

    const v0, 0x7f0b0022

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getPersonalizedSearchService()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d001e

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefetchCacheEntries()I
    .locals 1

    const v0, 0x7f0b003e

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getPrefetchSimultaneousDownloads()I
    .locals 1

    const v0, 0x7f0b003f

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getPrefetchThrottlePeriodMillis()I
    .locals 1

    const v0, 0x7f0b0040

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getPrefetchTtlMillis()I
    .locals 1

    const v0, 0x7f0b003d

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getPublishResultDelayMillis()I
    .locals 1

    const v0, 0x7f0b0015

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getQueryThreadPriority()I
    .locals 1

    const/16 v0, 0x9

    return v0
.end method

.method public getRefreshSearchHistoryDelay()I
    .locals 1

    const v0, 0x7f0b0023

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getRefreshSearchParametersCookieRefreshPeriodMs()J
    .locals 4

    const v0, 0x7f0b0024

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public getRegisterGsaBridgeJavascript()Ljava/lang/String;
    .locals 5

    const v2, 0x7f0d0020

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchConfig;->getVelvetGsaBridgeInterfaceName()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getRemoveFromHistoryButtonDisplayDelay()I
    .locals 1

    const v0, 0x7f0b0028

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getRemoveHistoryPath()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0009

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResultHidingDelay()I
    .locals 1

    const v0, 0x7f0b0033

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getResultHidingJsTimeout()I
    .locals 1

    const v0, 0x7f0b0034

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getResultLastAccessSecondsLeeway()I
    .locals 1

    const v0, 0x7f0b0016

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getRlzAccessPoint()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0014

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRlzProviderUri()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/searchcommon/SearchConfig;->RLZ_PROVIDER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getS3ServerOverrideForSingleRequest()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d002f

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getS3ServiceVoiceActionsSingleRequest()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d002e

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSavedConfigurationExpirySeconds()I
    .locals 1

    const v0, 0x7f0b0018

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getSavedWhitelistedConfigurationExpirySeconds()I
    .locals 1

    const v0, 0x7f0b0019

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getSearchDomainCheckPattern()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d000c

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchSingleReqDomain()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d001d

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchSourceParam()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d000b

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchUrlFormat()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d000e

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSoundSearchEnabled()Z
    .locals 1

    const v0, 0x7f0d002d

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/SearchConfig;->isGmsEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getSourceIconOverride(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const v0, 0x7f0f0010

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringMap(I)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSourceTimeoutMillis()I
    .locals 1

    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getStaticMapCacheMaxSize()I
    .locals 1

    const v0, 0x7f0b0041

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getSuggestNumVisibleSummonsRows()I
    .locals 1

    const v0, 0x7f0b0029

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getSuggestionCacheMaxValues()I
    .locals 1

    const v0, 0x7f0b0007

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getSuggestionCacheTimeout()I
    .locals 1

    const v0, 0x7f0b0006

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getSuggestionViewRecycleBinSize()I
    .locals 1

    const v0, 0x7f0b002b

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getSummonsRowsPerSource()I
    .locals 1

    const v0, 0x7f0b002a

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getTypingUpdateSuggestionsDelayMillis()I
    .locals 1

    const v0, 0x7f0b0017

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getUserAgentPattern()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d002b

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVelvetGsaBridgeInterfaceName()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0021

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebCorpusQueryParam()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0026

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebCorpusQueryParamBlacklist()[Ljava/lang/String;
    .locals 2

    const v0, 0x7f0f0013

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebModeQueryParam()Ljava/lang/String;
    .locals 1

    const v0, 0x7f0d0027

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebViewLoginLoadTimeoutMs()I
    .locals 1

    const v0, 0x7f0b0037

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getWebViewLoginRedirectTimeoutMs()I
    .locals 1

    const v0, 0x7f0b0038

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getWebViewSuppressPreviousResultsForMs()I
    .locals 1

    const v0, 0x7f0b0039

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    return v0
.end method

.method public isActionDiscoveryEnabled()Z
    .locals 1

    const v0, 0x7f0a001b

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isAdClickUrlException(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    const v6, 0x7f0f001a

    invoke-direct {p0, v6, v5}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {p1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v5, 0x1

    :cond_0
    return v5

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isAlwaysPrefetchEnabled()Z
    .locals 1

    const v0, 0x7f0a0013

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isBeamEnabled()Z
    .locals 1

    const v0, 0x7f0a000f

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isCannedAudioEnabled()Z
    .locals 1

    const v0, 0x7f0a0011

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isCorrectionsEnabled()Z
    .locals 1

    const v0, 0x7f0a000c

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isDebugAudioLoggingEnabled()Z
    .locals 1

    const v0, 0x7f0a001e

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isFullSizeIconSource(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const v0, 0x7f0f0015

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isGogglesBarcodingEnabled()Z
    .locals 1

    const v0, 0x7f0a0015

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isGogglesEnabled()Z
    .locals 1

    const v0, 0x7f0a0014

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isGoogleSearchLogoutRedirect(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const v0, 0x7f0f000b

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isGoogleSearchUrlPath(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const v0, 0x7f0f000a

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isImageMetadataGenericChunkingEnabled()Z
    .locals 1

    const v0, 0x7f0a0012

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isJesrEnabled()Z
    .locals 1

    const v0, 0x7f0a0008

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isLocaleWithAnswers(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    const v6, 0x7f0f0014

    invoke-direct {p0, v6, v5}, Lcom/google/android/searchcommon/SearchConfig;->getStringArray(IZ)[Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {p1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public isNativeIgEnabled()Z
    .locals 3

    const/4 v1, 0x1

    const v2, 0x7f0b003c

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/SearchConfig;->getInt(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/SearchConfig;->isJesrEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPersonalizedSearchEnabled()Z
    .locals 1

    const v0, 0x7f0a0017

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isSingleRequestArchitectureEnabled()Z
    .locals 1

    const v0, 0x7f0a0018

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isSourceEnabledByDefault(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    const v1, 0x7f0f0009

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const v1, 0x7f0f0007

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized isSourceEnabledInSuggestMode(Lcom/google/android/searchcommon/summons/ContentProviderSource;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getName()Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getSuggestUri()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f0008

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isSourceIgnoreIcon1(Ljava/lang/String;)Z
    .locals 1

    const v0, 0x7f0f000d

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSourceIgnored(Lcom/google/android/searchcommon/summons/Source;)Z
    .locals 2

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f000e

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSourceShowSingleLine(Ljava/lang/String;)Z
    .locals 1

    const v0, 0x7f0f000f

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSourceTrusted(Lcom/google/android/searchcommon/summons/ContentProviderSource;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/summons/ContentProviderSource;

    const v0, 0x7f0f0012

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0f0011

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getStringSet(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/ContentProviderSource;->getSuggestUri()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSpdyEnabled()Z
    .locals 1

    const v0, 0x7f0a0019

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isSpdyEnabledForHttpHelper()Z
    .locals 1

    const v0, 0x7f0a001a

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isSuggestLookAheadEnabled()Z
    .locals 1

    const v0, 0x7f0a000a

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isTestPlatformLoggingEnabled()Z
    .locals 1

    const v0, 0x7f0a001d

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isTheGoogleDeployed()Z
    .locals 1

    const v0, 0x7f0a000e

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isTimeToLeaveNotificationsEnabled()Z
    .locals 1

    const v0, 0x7f0a0016

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isUseHttpForVoice()Z
    .locals 1

    const v0, 0x7f0a001c

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public isWordByWordEnabled()Z
    .locals 1

    const v0, 0x7f0a000b

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method protected putResourceKey(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/SearchConfig;->mGsResourceKeys:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public shouldAllowSslSearch()Z
    .locals 1

    const v0, 0x7f0a0005

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public shouldCenterResultCardAndMatchPortraitWidthInLandscape()Z
    .locals 1

    const v0, 0x7f0a0004

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public shouldDedupeUserQuery()Z
    .locals 1

    const v0, 0x7f0a0009

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public shouldGroupSimilarSuggestions()Z
    .locals 1

    const v0, 0x7f0a0001

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public shouldSupplyCslParamForImageMetadata()Z
    .locals 1

    const v0, 0x7f0a0006

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public showSuggestionsForZeroQuery()Z
    .locals 1

    const v0, 0x7f0a0002

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/SearchConfig;->getBoolean(I)Z

    move-result v0

    return v0
.end method
