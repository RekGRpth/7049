.class public Lcom/google/android/searchcommon/TextAppearanceFactory;
.super Ljava/lang/Object;
.source "TextAppearanceFactory.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/TextAppearanceFactory;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public createSuggestionQueryTextAppearance()[Ljava/lang/Object;
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    iget-object v3, p0, Lcom/google/android/searchcommon/TextAppearanceFactory;->mContext:Landroid/content/Context;

    const v4, 0x7f0e003d

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    aput-object v2, v0, v1

    return-object v0
.end method

.method public createSuggestionSuggestedTextAppearance()[Ljava/lang/Object;
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    iget-object v3, p0, Lcom/google/android/searchcommon/TextAppearanceFactory;->mContext:Landroid/content/Context;

    const v4, 0x7f0e003e

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    aput-object v2, v0, v1

    return-object v0
.end method
