.class public Lcom/google/android/searchcommon/UserAgentHelper;
.super Ljava/lang/Object;
.source "UserAgentHelper.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private mUserAgent:Ljava/lang/String;

.field private mUserAgentAssumed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-void
.end method

.method private buildUserAgent(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getUserAgentPattern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/velvet/VelvetApplication;->getVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/UserAgentHelper;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/UserAgentHelper;->getUserAgent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getUserAgent()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mUserAgent:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getCachedUserAgentBase()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/searchcommon/WebViewUtils;->getCurrentUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mUserAgentAssumed:Z

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/UserAgentHelper;->buildUserAgent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mUserAgent:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mUserAgent:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized onWebViewCreated(Landroid/webkit/WebView;)V
    .locals 3
    .param p1    # Landroid/webkit/WebView;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mUserAgent:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mUserAgentAssumed:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v2, v0}, Lcom/google/android/searchcommon/SearchSettings;->setCachedUserAgentBase(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/UserAgentHelper;->buildUserAgent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mUserAgent:Ljava/lang/String;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/searchcommon/UserAgentHelper;->mUserAgentAssumed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
