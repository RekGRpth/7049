.class public Lcom/google/android/searchcommon/clicklog/BucketingClickLog;
.super Ljava/lang/Object;
.source "BucketingClickLog.java"

# interfaces
.implements Lcom/google/android/searchcommon/clicklog/ClickLog;


# static fields
.field static final CLICK_COUNT_GRANULARITY_MILLIS:J = 0xa4cb800L


# instance fields
.field private final mClicks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/searchcommon/util/ExpiringSum;",
            ">;"
        }
    .end annotation
.end field

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private mFlushPending:Z

.field private final mFlushSettingsTask:Lcom/google/android/searchcommon/util/NamedTask;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/summons/Sources;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V
    .locals 12
    .param p1    # Lcom/google/android/searchcommon/summons/Sources;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/SearchSettings;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;
    .param p5    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClock:Lcom/google/android/searchcommon/util/Clock;

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClicks:Ljava/util/Map;

    new-instance v1, Lcom/google/android/searchcommon/clicklog/BucketingClickLog$1;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/clicklog/BucketingClickLog$1;-><init>(Lcom/google/android/searchcommon/clicklog/BucketingClickLog;)V

    iput-object v1, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mFlushSettingsTask:Lcom/google/android/searchcommon/util/NamedTask;

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Sources;->getSources()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/searchcommon/summons/Source;

    iget-object v1, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v9}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/SearchSettings;->getSourceClickStats(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v10, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClicks:Ljava/util/Map;

    invoke-interface {v9}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v11

    new-instance v1, Lcom/google/android/searchcommon/util/ExpiringSum;

    iget-object v2, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v3, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->getMaxStatAgeMillis()J

    move-result-wide v3

    const-wide/32 v5, 0xa4cb800

    invoke-direct/range {v1 .. v7}, Lcom/google/android/searchcommon/util/ExpiringSum;-><init>(Lcom/google/android/searchcommon/util/Clock;JJLjava/lang/String;)V

    invoke-interface {v10, v11, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/clicklog/BucketingClickLog;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/clicklog/BucketingClickLog;

    invoke-direct {p0}, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->flushSettings()V

    return-void
.end method

.method private declared-synchronized flushSettings()V
    .locals 5

    monitor-enter p0

    const/4 v3, 0x0

    :try_start_0
    iput-boolean v3, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mFlushPending:Z

    iget-object v3, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClicks:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/searchcommon/util/ExpiringSum;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/util/ExpiringSum;->getJsonIfChanged()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v4, v3, v2}, Lcom/google/android/searchcommon/SearchSettings;->setSourceClickStats(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_1
    monitor-exit p0

    return-void
.end method

.method private scheduleFlushSettings()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mFlushPending:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mFlushPending:Z

    iget-object v0, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mFlushSettingsTask:Lcom/google/android/searchcommon/util/NamedTask;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public clearHistory()V
    .locals 0

    return-void
.end method

.method public getSourceScores(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClicks:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/searchcommon/util/ExpiringSum;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/util/ExpiringSum;->get()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getMinClicksForSourceRanking()I

    move-result v4

    if-lt v0, v4, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {p1, v3}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void
.end method

.method public hasHistory(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void
.end method

.method public reportClick(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionSource()Lcom/google/android/searchcommon/summons/Source;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->reportClickAtTime(Ljava/lang/String;J)V

    :cond_0
    return-void
.end method

.method public declared-synchronized reportClickAtTime(Ljava/lang/String;J)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # J

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClicks:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/ExpiringSum;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/util/ExpiringSum;

    iget-object v1, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iget-object v2, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getMaxStatAgeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb800

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/util/ExpiringSum;-><init>(Lcom/google/android/searchcommon/util/Clock;JJ)V

    iget-object v1, p0, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->mClicks:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/google/android/searchcommon/util/ExpiringSum;->incrementAtTime(J)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/clicklog/BucketingClickLog;->scheduleFlushSettings()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
