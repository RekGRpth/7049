.class Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;
.super Ljava/lang/Object;
.source "DefaultSourcesFirstClickLog.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->getSourceScores(Lcom/google/android/searchcommon/util/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;

.field final synthetic val$consumer:Lcom/google/android/searchcommon/util/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;->this$0:Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;

    iput-object p2, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;->consume(Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public consume(Ljava/util/Map;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;->this$0:Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;

    # getter for: Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->mSources:Lcom/google/android/searchcommon/summons/Sources;
    invoke-static {v3}, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->access$000(Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;)Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/summons/Sources;->getSources()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v2}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;->this$0:Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;

    # invokes: Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->getDefaultSourceScore(Lcom/google/android/searchcommon/summons/Source;)I
    invoke-static {v3, v2}, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->access$100(Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;Lcom/google/android/searchcommon/summons/Source;)I

    move-result v1

    invoke-interface {v2}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-interface {v3, p1}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    move-result v3

    return v3
.end method
