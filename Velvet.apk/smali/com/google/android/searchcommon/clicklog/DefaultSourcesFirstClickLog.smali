.class public Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;
.super Ljava/lang/Object;
.source "DefaultSourcesFirstClickLog.java"

# interfaces
.implements Lcom/google/android/searchcommon/clicklog/ClickLog;


# instance fields
.field private final mSources:Lcom/google/android/searchcommon/summons/Sources;

.field private final mWrapped:Lcom/google/android/searchcommon/clicklog/ClickLog;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/summons/Sources;Lcom/google/android/searchcommon/clicklog/ClickLog;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/summons/Sources;
    .param p2    # Lcom/google/android/searchcommon/clicklog/ClickLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->mWrapped:Lcom/google/android/searchcommon/clicklog/ClickLog;

    iput-object p1, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;)Lcom/google/android/searchcommon/summons/Sources;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;

    iget-object v0, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;Lcom/google/android/searchcommon/summons/Source;)I
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;
    .param p1    # Lcom/google/android/searchcommon/summons/Source;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->getDefaultSourceScore(Lcom/google/android/searchcommon/summons/Source;)I

    move-result v0

    return v0
.end method

.method private getDefaultSourceScore(Lcom/google/android/searchcommon/summons/Source;)I
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->isEnabledByDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public clearHistory()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->mWrapped:Lcom/google/android/searchcommon/clicklog/ClickLog;

    invoke-interface {v0}, Lcom/google/android/searchcommon/clicklog/ClickLog;->clearHistory()V

    return-void
.end method

.method public getSourceScores(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->mWrapped:Lcom/google/android/searchcommon/clicklog/ClickLog;

    new-instance v1, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog$1;-><init>(Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;Lcom/google/android/searchcommon/util/Consumer;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/clicklog/ClickLog;->getSourceScores(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public hasHistory(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->mWrapped:Lcom/google/android/searchcommon/clicklog/ClickLog;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/clicklog/ClickLog;->hasHistory(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public reportClick(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/clicklog/DefaultSourcesFirstClickLog;->mWrapped:Lcom/google/android/searchcommon/clicklog/ClickLog;

    invoke-interface {v0, p1, p2}, Lcom/google/android/searchcommon/clicklog/ClickLog;->reportClick(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;)V

    return-void
.end method
