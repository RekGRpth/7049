.class public abstract Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;
.super Lcom/google/android/searchcommon/google/AbstractGoogleSource;
.source "AbstractGoogleWebSource.java"

# interfaces
.implements Lcom/google/android/searchcommon/google/WebSuggestSource;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/AbstractGoogleSource;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;)V

    return-void
.end method


# virtual methods
.method public canQueryNow()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected doQueryExternal(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-object v2, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    invoke-virtual {v2, p1}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v1

    new-instance v0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;->getSourceName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;->query(Lcom/google/android/velvet/Query;ZLcom/google/android/searchcommon/suggest/MutableSuggestionList;)V

    return-object v0
.end method

.method protected doQueryInternal(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;->query(Lcom/google/android/velvet/Query;ZLcom/google/android/searchcommon/suggest/MutableSuggestionList;)V

    invoke-interface {p2, v0}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void
.end method

.method public isLikelyToReturnZeroQueryResults()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract query(Lcom/google/android/velvet/Query;ZLcom/google/android/searchcommon/suggest/MutableSuggestionList;)V
.end method

.method public queryExternal(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/util/Latency;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/searchcommon/util/Latency;-><init>(Lcom/google/android/searchcommon/util/Clock;)V

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/google/AbstractGoogleWebSource;->doQueryExternal(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/Latency;->getLatency()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->setLatency(I)V

    return-object v1
.end method
