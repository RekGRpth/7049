.class Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper$1;
.super Ljava/lang/Object;
.source "DeletedQueryRemovingSourceWrapper.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Lcom/google/android/searchcommon/suggest/SuggestionList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;

.field final synthetic val$consumer:Lcom/google/android/searchcommon/util/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper$1;->this$0:Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper$1;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper$1;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper$1;->this$0:Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;

    # invokes: Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->filterDeletedQueries(Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {v1, p1}, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->access$000(Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper$1;->consume(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z

    move-result v0

    return v0
.end method
