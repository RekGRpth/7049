.class public Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;
.super Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;
.source "DeletedQueryRemovingSourceWrapper.java"


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mDeletions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/google/WebSuggestSource;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/google/WebSuggestSource;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;-><init>(Lcom/google/android/searchcommon/google/WebSuggestSource;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mDeletions:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->filterDeletedQueries(Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    return-object v0
.end method

.method private filterDeletedQueries(Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->removeExpiredDeletions()V

    iget-object v4, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mDeletions:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    if-nez v4, :cond_0

    move-object v1, p1

    :goto_0
    return-object v1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWebSearchSuggestion()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/searchcommon/suggest/Suggestion;->isHistorySuggestion()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mDeletions:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    instance-of v4, p1, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;

    if-nez v4, :cond_1

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getMutableCopy()Lcom/google/android/searchcommon/suggest/MutableSuggestionList;

    move-result-object p1

    :cond_1
    move-object v4, p1

    check-cast v4, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;

    invoke-interface {v4, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->remove(I)V

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method private removeExpiredDeletions()V
    .locals 7

    iget-object v3, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->getDeletedQueryPropagationDelayMs()I

    move-result v5

    int-to-long v5, v5

    sub-long v0, v3, v5

    iget-object v3, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mDeletions:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v0

    if-gtz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper$1;-><init>(Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;Lcom/google/android/searchcommon/util/Consumer;)V

    invoke-super {p0, p1, v0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public removeFromHistory(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->removeFromHistory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mDeletions:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/DeletedQueryRemovingSourceWrapper;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
