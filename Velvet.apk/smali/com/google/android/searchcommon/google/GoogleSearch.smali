.class public Lcom/google/android/searchcommon/google/GoogleSearch;
.super Landroid/app/Activity;
.source "GoogleSearch.java"


# static fields
.field private static DBG:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/searchcommon/google/GoogleSearch;->DBG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static createLaunchUriIntentFromSearchIntent(Landroid/content/Intent;Landroid/content/Context;ZLandroid/location/Location;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/google/UriRewriter;Ljava/lang/String;)Landroid/content/Intent;
    .locals 20
    .param p0    # Landroid/content/Intent;
    .param p1    # Landroid/content/Context;
    .param p2    # Z
    .param p3    # Landroid/location/Location;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;
    .param p5    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p6    # Lcom/google/android/searchcommon/google/UriRewriter;
    .param p7    # Ljava/lang/String;

    const-string v17, "query"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_0

    const-string v17, "QSB.GoogleSearch"

    const-string v18, "Got search intent with no query."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x0

    :goto_0
    return-object v9

    :cond_0
    const-string v17, "com.android.browser.application_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    :cond_1
    const-string v17, "from_self"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v17, "new_search"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v17, "query_submit_ts"

    invoke-interface/range {p4 .. p4}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    const-string v17, "unknown"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/velvet/util/IntentUtils;->getSourceParam(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchBaseUrl()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setStaticParams()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setSource(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v13}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setSubmissionTime(J)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setOrDisableLocation(ZLandroid/location/Location;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v5

    if-eqz v6, :cond_2

    move-object/from16 v0, p7

    invoke-virtual {v5, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setRlz(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->build()Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p6

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/UriRewriter;->rewrite(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    new-instance v9, Landroid/content/Intent;

    const-string v17, "android.intent.action.VIEW"

    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v9, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v17, "com.android.browser.application_id"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v10, :cond_3

    const-string v17, "create_new_tab"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    sget-boolean v17, Lcom/google/android/searchcommon/google/GoogleSearch;->DBG:Z

    if-eqz v17, :cond_4

    const-string v17, "QSB.GoogleSearch"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Formatting location "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    if-eqz p2, :cond_6

    if-eqz p3, :cond_6

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/searchcommon/util/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/searchcommon/util/Util;->stringMapToBundle(Ljava/util/Map;)Landroid/os/Bundle;

    move-result-object v7

    sget-boolean v17, Lcom/google/android/searchcommon/google/GoogleSearch;->DBG:Z

    if-eqz v17, :cond_5

    const-string v17, "QSB.GoogleSearch"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Headers: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const-string v17, "com.android.browser.headers"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_6
    const/high16 v17, 0x10000000

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method private getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/GoogleSearch;->getSearchApplication()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    return-object v0
.end method

.method private getSearchApplication()Lcom/google/android/velvet/VelvetApplication;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/GoogleSearch;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/VelvetApplication;

    return-object v0
.end method

.method private handleViewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "QSB.GoogleSearch"

    const-string v3, "Got ACTION_VIEW with no data."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v3, "intent_extra_data_key"

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/google/GoogleSearch;->launchIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private handleWebSearchIntent(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/GoogleSearch;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForSearch()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/GoogleSearch;->getSearchApplication()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/LocationOracle;->getBestLocation()Landroid/location/Location;

    move-result-object v3

    :goto_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/GoogleSearch;->getSearchApplication()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v4

    invoke-virtual {v8}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v5

    invoke-virtual {v8}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getUriRewriter()Lcom/google/android/searchcommon/google/UriRewriter;

    move-result-object v6

    invoke-virtual {v8}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getRlzHelper()Lcom/google/android/searchcommon/google/RlzHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/RlzHelper;->getRlzForSearch()Ljava/lang/String;

    move-result-object v7

    move-object v0, p1

    move-object v1, p0

    invoke-static/range {v0 .. v7}, Lcom/google/android/searchcommon/google/GoogleSearch;->createLaunchUriIntentFromSearchIntent(Landroid/content/Intent;Landroid/content/Context;ZLandroid/location/Location;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/google/UriRewriter;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const-string v0, "web_search_pendingintent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/app/PendingIntent;

    if-eqz v10, :cond_0

    invoke-direct {p0, v10, v9}, Lcom/google/android/searchcommon/google/GoogleSearch;->launchPendingIntent(Landroid/app/PendingIntent;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz v9, :cond_1

    invoke-direct {p0, v9}, Lcom/google/android/searchcommon/google/GoogleSearch;->launchIntent(Landroid/content/Intent;)V

    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private launchIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    :try_start_0
    const-string v1, "QSB.GoogleSearch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Launching intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/google/GoogleSearch;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "QSB.GoogleSearch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No activity found to handle: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private launchPendingIntent(Landroid/app/PendingIntent;Landroid/content/Intent;)Z
    .locals 4
    .param p1    # Landroid/app/PendingIntent;
    .param p2    # Landroid/content/Intent;

    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p1, p0, v1, p2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "QSB.GoogleSearch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pending intent cancelled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private logIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x0

    const-string v4, "intent_extra_data_key"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "query"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "from_self"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/GoogleSearch;->getSearchApplication()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/GlobalSearchServices;->getGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;

    move-result-object v4

    if-eqz v1, :cond_0

    :goto_0
    invoke-interface {v4, v0, v2, v3}, Lcom/google/android/searchcommon/google/WebSuggestSource;->logClick(Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    const/high16 v3, 0x10000000

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/GoogleSearch;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "QSB.GoogleSearch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Got intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.SEARCH"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/google/GoogleSearch;->handleWebSearchIntent(Landroid/content/Intent;)V

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/google/GoogleSearch;->logIntent(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/GoogleSearch;->finish()V

    return-void

    :cond_1
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/google/GoogleSearch;->handleViewIntent(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v2, "QSB.GoogleSearch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unhandled intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
