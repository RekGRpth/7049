.class public interface abstract Lcom/google/android/searchcommon/google/GoogleSource;
.super Ljava/lang/Object;
.source "GoogleSource.java"


# virtual methods
.method public abstract canQueryNow()Z
.end method

.method public abstract getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
.end method

.method public abstract getClientNameForLogging()Ljava/lang/String;
.end method

.method public abstract getSourceName()Ljava/lang/String;
.end method

.method public abstract getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract isLikelyToReturnZeroQueryResults()Z
.end method

.method public abstract logClick(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract removeFromHistory(Ljava/lang/String;)Z
.end method
