.class public Lcom/google/android/searchcommon/google/GoogleSuggestionProvider;
.super Landroid/content/ContentProvider;
.source "GoogleSuggestionProvider.java"


# instance fields
.field private mImpl:Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProvider;->mImpl:Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 3

    new-instance v0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/GoogleSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/GoogleSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;-><init>(Landroid/content/Context;Lcom/google/android/velvet/VelvetApplication;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProvider;->mImpl:Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;

    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProvider;->mImpl:Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
