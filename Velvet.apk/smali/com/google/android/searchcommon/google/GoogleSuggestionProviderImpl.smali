.class public Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;
.super Ljava/lang/Object;
.source "GoogleSuggestionProviderImpl.java"


# instance fields
.field private final mApp:Lcom/google/android/velvet/VelvetApplication;

.field private final mContext:Landroid/content/Context;

.field private mSource:Lcom/google/android/searchcommon/google/WebSuggestSource;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/velvet/VelvetApplication;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/velvet/VelvetApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->buildUriMatcher(Landroid/content/Context;)Landroid/content/UriMatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mUriMatcher:Landroid/content/UriMatcher;

    return-void
.end method

.method private static buildUriMatcher(Landroid/content/Context;)Landroid/content/UriMatcher;
    .locals 5
    .param p0    # Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->getAuthority(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    const-string v2, "search_suggest_query"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v2, "search_suggest_query/*"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v2, "search_suggest_shortcut"

    invoke-virtual {v1, v0, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v2, "search_suggest_shortcut/*"

    invoke-virtual {v1, v0, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v1
.end method

.method private emptyIfNull(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p2    # Lcom/google/android/velvet/Query;

    if-nez p1, :cond_0

    new-instance p1, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->getSource()Lcom/google/android/searchcommon/google/WebSuggestSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/WebSuggestSource;->getSourceName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p2}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    :cond_0
    return-object p1
.end method

.method private getApplication()Lcom/google/android/velvet/VelvetApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mApp:Lcom/google/android/velvet/VelvetApplication;

    return-object v0
.end method

.method private static getAuthority(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".google"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getQuery(Landroid/net/Uri;)Lcom/google/android/velvet/Query;
    .locals 2
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    goto :goto_0
.end method

.method private declared-synchronized getSource()Lcom/google/android/searchcommon/google/WebSuggestSource;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mSource:Lcom/google/android/searchcommon/google/WebSuggestSource;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->getApplication()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->awaitReadiness()V

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getGoogleSource()Lcom/google/android/searchcommon/google/WebSuggestSource;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mSource:Lcom/google/android/searchcommon/google/WebSuggestSource;

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mSource:Lcom/google/android/searchcommon/google/WebSuggestSource;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->getQuery(Landroid/net/Uri;)Lcom/google/android/velvet/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->getSource()Lcom/google/android/searchcommon/google/WebSuggestSource;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/google/WebSuggestSource;->queryExternal(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v3

    invoke-direct {p0, v3, v1}, Lcom/google/android/searchcommon/google/GoogleSuggestionProviderImpl;->emptyIfNull(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown URI "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
