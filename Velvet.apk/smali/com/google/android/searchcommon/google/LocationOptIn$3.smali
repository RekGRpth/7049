.class Lcom/google/android/searchcommon/google/LocationOptIn$3;
.super Ljava/lang/Object;
.source "LocationOptIn.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/LocationOptIn;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/LocationOptIn;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$3;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/LocationOptIn$3;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn$3;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$3;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationObserver:Landroid/database/ContentObserver;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$500(Lcom/google/android/searchcommon/google/LocationOptIn;)Landroid/database/ContentObserver;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gsf/UseLocationForServices;->registerUseLocationForServicesObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/LocationOptIn$3;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mGetSystemLocationSetting:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$200(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method
