.class Lcom/google/android/searchcommon/google/LocationOptIn$4;
.super Ljava/lang/Object;
.source "LocationOptIn.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/LocationOptIn;->notifyObserversLocked(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

.field final synthetic val$observers:Ljava/util/Set;

.field final synthetic val$useLocation:Z


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/LocationOptIn;Ljava/util/Set;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$4;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/LocationOptIn$4;->val$observers:Ljava/util/Set;

    iput-boolean p3, p0, Lcom/google/android/searchcommon/google/LocationOptIn$4;->val$useLocation:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/google/LocationOptIn$4;->val$observers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/google/LocationSettings$Observer;

    iget-boolean v2, p0, Lcom/google/android/searchcommon/google/LocationOptIn$4;->val$useLocation:Z

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/google/LocationSettings$Observer;->onUseLocationChanged(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method
