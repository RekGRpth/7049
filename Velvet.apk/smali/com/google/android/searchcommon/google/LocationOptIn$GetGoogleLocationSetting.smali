.class Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;
.super Ljava/lang/Object;
.source "LocationOptIn.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/LocationOptIn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGoogleLocationSetting"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/LocationOptIn;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/google/LocationOptIn;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/google/LocationOptIn;Lcom/google/android/searchcommon/google/LocationOptIn$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/google/LocationOptIn;
    .param p2    # Lcom/google/android/searchcommon/google/LocationOptIn$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;-><init>(Lcom/google/android/searchcommon/google/LocationOptIn;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$1100(Lcom/google/android/searchcommon/google/LocationOptIn;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gsf/UseLocationForServices;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$700(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationEnabled:Z
    invoke-static {v3}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$1200(Lcom/google/android/searchcommon/google/LocationOptIn;)Z

    move-result v3

    # invokes: Lcom/google/android/searchcommon/google/LocationOptIn;->updateSettingsLocked(ZI)V
    invoke-static {v1, v3, v0}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$900(Lcom/google/android/searchcommon/google/LocationOptIn;ZI)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSettingValid:Z
    invoke-static {v1, v3}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$1302(Lcom/google/android/searchcommon/google/LocationOptIn;Z)Z

    iget-object v1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetGoogleLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$700(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
