.class Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;
.super Ljava/lang/Object;
.source "LocationOptIn.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/LocationOptIn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSystemLocationSetting"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/LocationOptIn;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/google/LocationOptIn;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/google/LocationOptIn;Lcom/google/android/searchcommon/google/LocationOptIn$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/google/LocationOptIn;
    .param p2    # Lcom/google/android/searchcommon/google/LocationOptIn$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;-><init>(Lcom/google/android/searchcommon/google/LocationOptIn;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {v4}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$600(Lcom/google/android/searchcommon/google/LocationOptIn;)Landroid/location/LocationManager;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {v4}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$600(Lcom/google/android/searchcommon/google/LocationOptIn;)Landroid/location/LocationManager;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "passive"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$700(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mGoogleLocationSetting:I
    invoke-static {v6}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$800(Lcom/google/android/searchcommon/google/LocationOptIn;)I

    move-result v6

    # invokes: Lcom/google/android/searchcommon/google/LocationOptIn;->updateSettingsLocked(ZI)V
    invoke-static {v4, v0, v6}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$900(Lcom/google/android/searchcommon/google/LocationOptIn;ZI)V

    iget-object v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    const/4 v6, 0x1

    # setter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mSystemLocationSettingValid:Z
    invoke-static {v4, v6}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$1002(Lcom/google/android/searchcommon/google/LocationOptIn;Z)Z

    iget-object v4, p0, Lcom/google/android/searchcommon/google/LocationOptIn$GetSystemLocationSetting;->this$0:Lcom/google/android/searchcommon/google/LocationOptIn;

    # getter for: Lcom/google/android/searchcommon/google/LocationOptIn;->mSettingsLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/google/android/searchcommon/google/LocationOptIn;->access$700(Lcom/google/android/searchcommon/google/LocationOptIn;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
