.class Lcom/google/android/searchcommon/google/PelletChunkHelper;
.super Ljava/lang/Object;
.source "PelletChunkHelper.java"


# instance fields
.field private final mBaseUri:Ljava/lang/String;

.field private final mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

.field private final mParent:Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

.field private final mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/util/InputStreamChunkProducer;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/InputStreamChunkProducer;
    .param p2    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/InputStreamChunkProducer;",
            "Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mParent:Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/Consumer;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mBaseUri:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method bufferAllData(Ljava/io/InputStream;Lcom/google/android/searchcommon/util/Consumer;)Z
    .locals 8
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;
        }
    .end annotation

    const/4 v4, 0x0

    new-instance v3, Lcom/google/android/searchcommon/google/PelletParser;

    invoke-direct {v3, p1}, Lcom/google/android/searchcommon/google/PelletParser;-><init>(Ljava/io/InputStream;)V

    new-instance v2, Lcom/google/android/searchcommon/google/PelletDemultiplexer;

    iget-object v5, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;

    iget-object v7, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mBaseUri:Ljava/lang/String;

    invoke-direct {v2, p2, v5, v6, v7}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;-><init>(Lcom/google/android/searchcommon/util/Consumer;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mParent:Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->throwIOExceptionIfStopped(Ljava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/PelletParser;->read()Lcom/google/android/searchcommon/google/PelletParser$Pellet;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->onEndOfResponse()V

    return v5

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v5, p0, Lcom/google/android/searchcommon/google/PelletChunkHelper;->mParent:Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

    invoke-virtual {v5, v0}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;->throwIOExceptionIfStopped(Ljava/lang/Exception;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v5

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->onEndOfResponse()V

    throw v5

    :cond_1
    :try_start_3
    invoke-virtual {v2, v1}, Lcom/google/android/searchcommon/google/PelletDemultiplexer;->consume(Lcom/google/android/searchcommon/google/PelletParser$Pellet;)Z

    const/high16 v5, 0x400000

    if-le v4, v5, :cond_2

    new-instance v5, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;

    invoke-direct {v5}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;-><init>()V

    throw v5

    :cond_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/InterruptedException;

    invoke-direct {v5}, Ljava/lang/InterruptedException;-><init>()V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method
