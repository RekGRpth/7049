.class public Lcom/google/android/searchcommon/google/PelletChunkProducer;
.super Lcom/google/android/searchcommon/util/InputStreamChunkProducer;
.source "PelletChunkProducer.java"


# instance fields
.field private final mHelper:Lcom/google/android/searchcommon/google/PelletChunkHelper;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/util/concurrent/ExecutorService;
    .param p3    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;-><init>(Ljava/io/InputStream;Ljava/util/concurrent/ExecutorService;)V

    new-instance v0, Lcom/google/android/searchcommon/google/PelletChunkHelper;

    invoke-direct {v0, p0, p3, p4, p5}, Lcom/google/android/searchcommon/google/PelletChunkHelper;-><init>(Lcom/google/android/searchcommon/util/InputStreamChunkProducer;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletChunkProducer;->mHelper:Lcom/google/android/searchcommon/google/PelletChunkHelper;

    return-void
.end method


# virtual methods
.method protected bufferAllData(Ljava/io/InputStream;Lcom/google/android/searchcommon/util/Consumer;)Z
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/google/PelletChunkProducer;->mHelper:Lcom/google/android/searchcommon/google/PelletChunkHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/searchcommon/google/PelletChunkHelper;->bufferAllData(Ljava/io/InputStream;Lcom/google/android/searchcommon/util/Consumer;)Z

    move-result v0

    return v0
.end method
