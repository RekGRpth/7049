.class public Lcom/google/android/searchcommon/google/PelletHttpChunkProducer;
.super Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;
.source "PelletHttpChunkProducer.java"


# instance fields
.field private final mHelper:Lcom/google/android/searchcommon/google/PelletChunkHelper;


# direct methods
.method public constructor <init>(Ljava/net/HttpURLConnection;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLjava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 5
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
    .param p3    # [B
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            "Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;",
            "[B",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;-><init>(Ljava/net/HttpURLConnection;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLjava/util/concurrent/ExecutorService;)V

    new-instance v0, Lcom/google/android/searchcommon/google/PelletChunkHelper;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p5, p6, v1}, Lcom/google/android/searchcommon/google/PelletChunkHelper;-><init>(Lcom/google/android/searchcommon/util/InputStreamChunkProducer;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletHttpChunkProducer;->mHelper:Lcom/google/android/searchcommon/google/PelletChunkHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/PelletHttpChunkProducer;->mHeaderOverrides:Ljava/util/Map;

    const-string v1, "Content-Type"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "text/html; charset=UTF-8"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected bufferAllData(Ljava/io/InputStream;Lcom/google/android/searchcommon/util/Consumer;)Z
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Lcom/google/android/searchcommon/util/InputStreamChunkProducer$SizeExceededException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/google/PelletHttpChunkProducer;->mHelper:Lcom/google/android/searchcommon/google/PelletChunkHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/searchcommon/google/PelletChunkHelper;->bufferAllData(Ljava/io/InputStream;Lcom/google/android/searchcommon/util/Consumer;)Z

    move-result v0

    return v0
.end method
