.class public Lcom/google/android/searchcommon/google/PelletParser;
.super Ljava/lang/Object;
.source "PelletParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/PelletParser$Pellet;
    }
.end annotation


# instance fields
.field private final mIn:Ljava/io/InputStream;

.field private mJsonReader:Landroid/util/JsonReader;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/PelletParser;->mIn:Ljava/io/InputStream;

    return-void
.end method

.method private init()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/JsonReader;

    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/PelletParser;->mIn:Ljava/io/InputStream;

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/util/JsonReader;->setLenient(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public read()Lcom/google/android/searchcommon/google/PelletParser$Pellet;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/PelletParser;->init()V

    :goto_0
    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v9

    if-eqz v9, :cond_0

    sget-object v11, Landroid/util/JsonToken;->END_DOCUMENT:Landroid/util/JsonToken;

    if-ne v9, v11, :cond_1

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    sget-object v11, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    if-ne v9, v11, :cond_2

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->beginObject()V

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v2, -0x1

    :goto_2
    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_9

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v7

    const-string v11, "e"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    const-string v11, "Search.PelletParser"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unexpected JSON token. Expected pellet, but got "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    :cond_3
    const-string v11, "c"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    goto :goto_2

    :cond_4
    const-string v11, "u"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_5
    const-string v11, "d"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_6
    const-string v11, "a"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    :try_start_0
    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->beginObject()V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v5

    :goto_3
    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v8

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v8, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v6

    const-string v11, "Search.PelletParser"

    const-string v12, "Could not parse pellet \'a\' field"

    invoke-static {v11, v12, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    :cond_7
    :try_start_1
    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->endObject()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :cond_8
    const-string v11, "Search.PelletParser"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unknown JSON field: \""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_2

    :cond_9
    iget-object v11, p0, Lcom/google/android/searchcommon/google/PelletParser;->mJsonReader:Landroid/util/JsonReader;

    invoke-virtual {v11}, Landroid/util/JsonReader;->endObject()V

    if-eqz v1, :cond_0

    const/4 v11, -0x1

    if-eq v2, v11, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/google/PelletParser$Pellet;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/google/PelletParser$Pellet;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_1
.end method
