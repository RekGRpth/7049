.class Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "RefreshSearchDomainAndCookiesTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WatchRedirectsWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;
    .param p2    # Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;-><init>(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    const/4 v1, 0x1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->updateWebViewLoginState(ZZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->access$500(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;ZZ)V

    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    # invokes: Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->updateWebViewLoginState(ZZ)V
    invoke-static {v0, v1, v1}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->access$500(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;ZZ)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v0, "Search.RefreshSearchDomainAndCookiesTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to log in: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask$WatchRedirectsWebViewClient;->this$0:Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->updateWebViewLoginState(ZZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;->access$500(Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;ZZ)V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method
