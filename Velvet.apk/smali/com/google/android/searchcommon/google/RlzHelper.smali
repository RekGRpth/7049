.class public Lcom/google/android/searchcommon/google/RlzHelper;
.super Ljava/lang/Object;
.source "RlzHelper.java"

# interfaces
.implements Lcom/google/android/velvet/ActivityLifecycleObserver;


# instance fields
.field private final mBaseUri:Landroid/net/Uri;

.field private final mBgThread:Ljava/util/concurrent/Executor;

.field private mContentObserverRegistered:Z

.field private final mContext:Landroid/content/Context;

.field private final mGetRlzTask:Ljava/lang/Runnable;

.field private final mPeekRlzTask:Ljava/lang/Runnable;

.field private volatile mRlz:Ljava/lang/String;

.field private final mRlzObserver:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ActivityLifecycleNotifier;Ljava/util/concurrent/Executor;Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/ActivityLifecycleNotifier;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/google/RlzHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/google/RlzHelper$1;-><init>(Lcom/google/android/searchcommon/google/RlzHelper;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mPeekRlzTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/google/RlzHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/google/RlzHelper$2;-><init>(Lcom/google/android/searchcommon/google/RlzHelper;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mGetRlzTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/google/RlzHelper$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/google/RlzHelper$3;-><init>(Lcom/google/android/searchcommon/google/RlzHelper;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mRlzObserver:Landroid/database/ContentObserver;

    invoke-interface {p1, p0}, Lcom/google/android/velvet/ActivityLifecycleNotifier;->addActivityLifecycleObserver(Lcom/google/android/velvet/ActivityLifecycleObserver;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mBgThread:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p4}, Lcom/google/android/searchcommon/SearchConfig;->getRlzProviderUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mBaseUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mBgThread:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mPeekRlzTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/google/RlzHelper;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/RlzHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mPeekRlzTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/google/RlzHelper;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/RlzHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mBgThread:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method protected getRlz(Z)V
    .locals 9
    .param p1    # Z

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mBaseUri:Landroid/net/Uri;

    const-string v2, "peek"

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    const/4 v8, 0x0

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_1
    iput-object v8, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mRlz:Ljava/lang/String;

    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mBaseUri:Landroid/net/Uri;

    goto :goto_0

    :catch_0
    move-exception v7

    :try_start_1
    const-string v0, "Velvet.RlzHelper"

    const-string v2, "Could not get RLZ: "

    invoke-static {v0, v2, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getRlzForSearch()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mBgThread:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mGetRlzTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mRlz:Ljava/lang/String;

    return-object v0
.end method

.method public onActivityStart()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mContentObserverRegistered:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mBaseUri:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mRlzObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mContentObserverRegistered:Z

    :cond_0
    return-void
.end method

.method public onActivityStop()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mContentObserverRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mRlzObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/RlzHelper;->mContentObserverRegistered:Z

    :cond_0
    return-void
.end method
