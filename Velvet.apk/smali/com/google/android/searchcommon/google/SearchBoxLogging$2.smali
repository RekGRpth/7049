.class Lcom/google/android/searchcommon/google/SearchBoxLogging$2;
.super Ljava/lang/Object;
.source "SearchBoxLogging.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionsUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/SearchBoxLogging;->captureShownWebSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsUi;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

.field final synthetic val$ui:Lcom/google/android/searchcommon/suggest/SuggestionsUi;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->val$ui:Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V
    .locals 6
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p3    # I
    .param p4    # Z
    .param p5    # Z

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    # getter for: Lcom/google/android/searchcommon/google/SearchBoxLogging;->mStatsLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->access$100(Lcom/google/android/searchcommon/google/SearchBoxLogging;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    # setter for: Lcom/google/android/searchcommon/google/SearchBoxLogging;->mLastShownWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {v0, p2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->access$202(Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    # setter for: Lcom/google/android/searchcommon/google/SearchBoxLogging;->mLastShownWebSuggestionsCount:I
    invoke-static {v0, p3}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->access$302(Lcom/google/android/searchcommon/google/SearchBoxLogging;I)I

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isEmptySuggestQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    # getter for: Lcom/google/android/searchcommon/google/SearchBoxLogging;->mZeroQuerySuggestionsCount:I
    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->access$400(Lcom/google/android/searchcommon/google/SearchBoxLogging;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    # setter for: Lcom/google/android/searchcommon/google/SearchBoxLogging;->mZeroQuerySuggestionsCount:I
    invoke-static {v0, p3}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->access$402(Lcom/google/android/searchcommon/google/SearchBoxLogging;I)I

    :cond_0
    if-eqz p4, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    # getter for: Lcom/google/android/searchcommon/google/SearchBoxLogging;->mSuggestUplStats:Lcom/google/android/searchcommon/google/SuggestUplStats;
    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->access$600(Lcom/google/android/searchcommon/google/SearchBoxLogging;)Lcom/google/android/searchcommon/google/SuggestUplStats;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->this$0:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    # getter for: Lcom/google/android/searchcommon/google/SearchBoxLogging;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->access$500(Lcom/google/android/searchcommon/google/SearchBoxLogging;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v2

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->isFromCache()Z

    move-result v4

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/searchcommon/google/SuggestUplStats;->registerSuggestResponse(JZLcom/google/android/velvet/Query;)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$2;->val$ui:Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/searchcommon/suggest/SuggestionsUi;->showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
