.class public Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;
.super Ljava/lang/Object;
.source "SearchBoxLogging.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/SearchBoxLogging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "StatsBuilder"
.end annotation


# instance fields
.field private final mDenominator:Ljava/lang/String;

.field private mLastNonZeroEntryAt:I

.field private final mStatsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mDenominator:Ljava/lang/String;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mStatsList:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mLastNonZeroEntryAt:I

    return-void
.end method

.method private isEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method addStat(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mStatsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mStatsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mLastNonZeroEntryAt:I

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mStatsList:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method build()Ljava/lang/String;
    .locals 4

    iget v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mLastNonZeroEntryAt:I

    if-nez v2, :cond_0

    const-string v2, ""

    :goto_0
    return-object v2

    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mStatsList:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    :goto_1
    iget v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mLastNonZeroEntryAt:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mDenominator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchBoxLogging$StatsBuilder;->mStatsList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
