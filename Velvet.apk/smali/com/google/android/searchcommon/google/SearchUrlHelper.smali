.class public Lcom/google/android/searchcommon/google/SearchUrlHelper;
.super Ljava/lang/Object;
.source "SearchUrlHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    }
.end annotation


# static fields
.field static final PARAM_INPUT_METHOD:Ljava/lang/String; = "inm"

.field static final PARAM_SPOKEN_LANGUAGE:Ljava/lang/String; = "spknlang"

.field static final PARAM_VALUE_GOGGLES:Ljava/lang/String; = "sbi"

.field static final PARAM_VALUE_VOICE_SEARCH:Ljava/lang/String; = "vs"


# instance fields
.field private final mApp:Lcom/google/android/velvet/VelvetApplication;

.field private mBrowserDimensionsSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mCookies:Lcom/google/android/velvet/Cookies;

.field private final mCorpora:Lcom/google/android/velvet/Corpora;

.field private final mDefaultLocale:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mPartner:Lcom/google/android/searchcommon/google/PartnerInfo;

.field private final mPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

.field private final mRlzHelper:Lcom/google/android/searchcommon/google/RlzHelper;

.field private final mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

.field private final mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mSimpleDateFormat:Ljava/text/SimpleDateFormat;

.field private mSource:Ljava/lang/String;

.field private final mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/velvet/Corpora;Lcom/google/android/velvet/VelvetApplication;Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/google/RlzHelper;Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/google/PartnerInfo;Lcom/google/android/velvet/Cookies;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;
    .param p4    # Lcom/google/android/searchcommon/util/Clock;
    .param p5    # Lcom/google/android/velvet/Corpora;
    .param p6    # Lcom/google/android/velvet/VelvetApplication;
    .param p7    # Lcom/google/android/searchcommon/google/SearchBoxLogging;
    .param p8    # Lcom/google/android/searchcommon/google/RlzHelper;
    .param p10    # Lcom/google/android/searchcommon/google/PartnerInfo;
    .param p11    # Lcom/google/android/velvet/Cookies;
    .param p12    # Lcom/google/android/voicesearch/settings/Settings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/SearchSettings;",
            "Lcom/google/android/searchcommon/SearchConfig;",
            "Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;",
            "Lcom/google/android/searchcommon/util/Clock;",
            "Lcom/google/android/velvet/Corpora;",
            "Lcom/google/android/velvet/VelvetApplication;",
            "Lcom/google/android/searchcommon/google/SearchBoxLogging;",
            "Lcom/google/android/searchcommon/google/RlzHelper;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/google/android/searchcommon/google/PartnerInfo;",
            "Lcom/google/android/velvet/Cookies;",
            "Lcom/google/android/voicesearch/settings/Settings;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/SearchConfig;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    iput-object p6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/Clock;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-static {p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/Corpora;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-static {p8}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/google/RlzHelper;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mRlzHelper:Lcom/google/android/searchcommon/google/RlzHelper;

    invoke-static {p7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    iput-object p9, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mDefaultLocale:Lcom/google/common/base/Supplier;

    iput-object p10, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mPartner:Lcom/google/android/searchcommon/google/PartnerInfo;

    iput-object p11, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-static {p12}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getSpokenLocaleBcp47Supplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, dd MMM yyyy HH:mm:ss\' GMT\'"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSimpleDateFormat:Ljava/text/SimpleDateFormat;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSimpleDateFormat:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/net/Uri;Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/util/Map;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->maybeAddCookieHeader(Landroid/net/Uri;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mBrowserDimensionsSupplier:Lcom/google/common/base/Supplier;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Ljava/text/SimpleDateFormat;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSimpleDateFormat:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/velvet/VelvetApplication;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mApp:Lcom/google/android/velvet/VelvetApplication;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/searchcommon/SearchSettings;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getClientParam()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getHlParameterForSearch()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSafeSearchValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/searchcommon/SearchConfig;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-object v0
.end method

.method public static convertObsoleteGeolocationCodeToNew(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "gb"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "uk"

    goto :goto_0
.end method

.method public static convertObsoleteLanguageCodeToNew(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v0, "iw"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p0, "he"

    goto :goto_0

    :cond_2
    const-string v0, "in"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string p0, "id"

    goto :goto_0

    :cond_3
    const-string v0, "ji"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "yi"

    goto :goto_0
.end method

.method private static endsWithIfPresent(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private formatUrlBase(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->formatUrlBase(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    return-object v0
.end method

.method private formatUrlBase(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    if-eqz p2, :cond_1

    move-object v2, p2

    :goto_1
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, p1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v6}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static getAllQueryParameters(Landroid/net/Uri;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v0, "Search.SearchUrlHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error parsing URL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const-string v0, "Search.SearchUrlHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error parsing URL [fragment]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    return-object v1
.end method

.method private getBeamSearchUri(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x1

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v6}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, p1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getClientParam()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mPartner:Lcom/google/android/searchcommon/google/PartnerInfo;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/PartnerInfo;->getSearchClientId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ms-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mPartner:Lcom/google/android/searchcommon/google/PartnerInfo;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/PartnerInfo;->getClientId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getCurrentMajelDomain()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getFixedMajelDomain()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getSearchDomain()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private getCurrentSearchDomain()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getFixedSearchDomain()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getSearchDomain()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private getDefaultSearchDomain()Ljava/lang/String;
    .locals 1

    const-string v0, ".google.com"

    return-object v0
.end method

.method private static getHlParameter(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/util/Locale;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->stringFromLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->localeFromString(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method private getHlParameterForSearch()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mDefaultLocale:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getSearchDomainLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getHlParameter(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getIntQueryParameter(Landroid/net/Uri;Ljava/lang/String;I)I
    .locals 4

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p3

    :cond_0
    :goto_0
    return p3

    :catch_0
    move-exception v1

    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid integer value \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" in search URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getPathQueryAndFragment(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPersonalizedSearchValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isPersonalizedSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getPersonalizedSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    move-object v0, v3

    goto :goto_0
.end method

.method private getSafeSearchValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getSafeSearch()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSearchUrlBuilder(Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getWebSearchPattern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchUrlBuilder(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    return-object v0
.end method

.method private getSearchUrlBuilder(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSingleRequestSearchDomain()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->formatUrlBase(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    :goto_0
    if-eqz p2, :cond_0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->buildUpFromQuery(Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;Lcom/google/android/velvet/Query;Ljava/lang/String;)V

    :cond_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->formatUrlBase(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    goto :goto_0
.end method

.method private getUrlDestination(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 5
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private isSearchPath(Landroid/net/Uri;)Z
    .locals 2
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->isGoogleSearchUrlPath(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static localeFromString(Ljava/lang/String;)Ljava/util/Locale;
    .locals 6
    .param p0    # Ljava/lang/String;

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v1, "-"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-ne v1, v4, :cond_0

    new-instance v1, Ljava/util/Locale;

    aget-object v2, v0, v3

    invoke-direct {v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    array-length v1, v0

    if-ne v1, v5, :cond_3

    aget-object v1, v0, v4

    const-string v2, "Hant"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    aget-object v1, v0, v4

    const-string v2, "Hans"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    new-instance v1, Ljava/util/Locale;

    aget-object v2, v0, v3

    const-string v3, ""

    aget-object v4, v0, v4

    invoke-direct {v1, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/Locale;

    aget-object v2, v0, v3

    aget-object v3, v0, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    array-length v1, v0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    new-instance v1, Ljava/util/Locale;

    aget-object v2, v0, v3

    aget-object v3, v0, v5

    aget-object v4, v0, v4

    invoke-direct {v1, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported locale format: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private maybeAddCookieHeader(Landroid/net/Uri;Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->maybeAddCookieHeader(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private normalizeDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getDefaultSearchDomain()Ljava/lang/String;

    move-result-object p1

    :cond_0
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "www"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public static safeLogUrl(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/net/Uri;

    if-nez p0, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->isHierarchical()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "auth"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "REDACTED"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static safeLogUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private shouldUseGoogleCom()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->shouldUseGoogleCom()Z

    move-result v0

    return v0
.end method

.method private static stringFromLocale(Ljava/util/Locale;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/util/Locale;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "-"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "-"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method buildUpFromQuery(Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;Lcom/google/android/velvet/Query;Ljava/lang/String;)V
    .locals 11
    .param p1    # Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v8

    invoke-static {v8}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setStaticParams()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSource:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setSource(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v9}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setSubmissionTime(J)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mRlzHelper:Lcom/google/android/searchcommon/google/RlzHelper;

    invoke-virtual {v9}, Lcom/google/android/searchcommon/google/RlzHelper;->getRlzForSearch()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setRlz(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setSpeechCookie()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setNoJesr()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p1, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_0
    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v8}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v8}, Lcom/google/android/searchcommon/SearchConfig;->getDeviceCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setCountryCode(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setTimezone()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSingleRequestSearchDomain()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, ".sandbox.google.com"

    invoke-virtual {v6, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {p1, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setDebugHostParam(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->getQueryParams()Ljava/util/Map;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->getQueryParams()Ljava/util/Map;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->putParams(Ljava/util/Map;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_2
    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v8}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->shouldShowCards()Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-static {}, Lcom/google/android/searchcommon/util/Util;->getLocaleString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/searchcommon/SearchConfig;->isLocaleWithAnswers(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setDoubleRequest()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_3
    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;

    invoke-virtual {v8, p2, p1}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->setLoggingParams(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;)V

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v8

    if-nez v8, :cond_4

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->isQueryTextFromVoice()Z

    move-result v8

    if-eqz v8, :cond_11

    :cond_4
    const-string v8, "vs"

    invoke-virtual {p1, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setInputMethod(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSpokenLanguageSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v8}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    invoke-virtual {p1, v7}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setSpokenLanguage(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_5
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getResultIndex()I

    move-result v8

    if-lez v8, :cond_6

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getResultIndex()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setResultStartIndex(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_6
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getPersonalizedSearchValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p1, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setPersonalizedSearch(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_7
    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v8}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v8

    if-eqz v8, :cond_8

    # invokes: Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setDateHeader()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->access$000(Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_8
    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v8}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/google/LocationSettings;->canUseLocationForSearch()Z

    move-result v8

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getLocationOverride()Landroid/location/Location;

    move-result-object v9

    invoke-virtual {p1, v8, v9}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setOrDisableLocation(ZLandroid/location/Location;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->shouldSuppressAnswers()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setSuppressAnswers()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_9
    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v8}, Lcom/google/android/searchcommon/SearchConfig;->isNativeIgEnabled()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setNativeIg()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_a
    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->isPrefetch()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setPrefetch()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setNoSuggestions()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_b
    invoke-virtual {p0, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->shouldRequestPelletResponse(Lcom/google/android/velvet/Query;)Z

    move-result v8

    if-eqz v8, :cond_13

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setPelletizedResponse()V

    :goto_1
    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v8

    if-eqz v8, :cond_c

    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v8}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/voicesearch/VoiceSearchServices;->getHeadsetAudioRecorderRouter()Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->shouldSetHandsFreeTtsMode()Z

    move-result v8

    if-nez v8, :cond_d

    :cond_c
    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v8}, Lcom/google/android/searchcommon/SearchSettings;->shouldForceHandsFreeDebugFlag()Z

    move-result v8

    if-eqz v8, :cond_e

    :cond_d
    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setHandsFree()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_e
    iget-object v8, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mPreferences:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {v8}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getUnits()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_14

    const/4 v8, 0x1

    :goto_2
    invoke-virtual {p1, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setTemperatureFahrenheit(Z)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getPersistCgiParameters()Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    if-eqz v1, :cond_f

    invoke-virtual {p1, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setPersistCgiParameters(Ljava/util/Map;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_f
    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setBrowserDimensionsIfAvailable()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setIncludeCookieHeader()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    invoke-virtual {p1, p3}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setSpeechRequestId(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_10
    return-void

    :cond_11
    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->isGogglesSearch()Z

    move-result v8

    if-nez v8, :cond_12

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->isQueryTextFromGoggles()Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_12
    const-string v8, "sbi"

    invoke-virtual {p1, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setInputMethod(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setDoubleRequestArchitecture()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    goto :goto_1

    :cond_14
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public cgiParamsEqualForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z
    .locals 7
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getPersistCgiParameters()Lcom/google/common/collect/ImmutableMap;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getPersistCgiParameters()Lcom/google/common/collect/ImmutableMap;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->getGwsParamsAffectingQueryEquivalenceForSearch()Ljava/util/Set;

    move-result-object v3

    if-eq v1, v4, :cond_1

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public equivalentForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/Query;

    invoke-static {p1, p2}, Lcom/google/android/velvet/Query;->equivalentForSearchDisregardingParams(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->cgiParamsEqualForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public formatUrl(Ljava/lang/String;Z)Landroid/net/Uri;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public getAdUriForRedirectHandling(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/UriRequest;
    .locals 5

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getAdClickHandlingEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v3

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchAuthority(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getClickedAdUrlPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->isAdClickUrlException(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Search.SearchUrlHelper"

    const-string v1, "Not handling JS-redirected ad click link"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move-object v0, v3

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getAdClickUrlSubstitutions()Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/searchcommon/util/Util;->asString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_3
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->maybeAddCookieHeader(Landroid/net/Uri;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    new-instance v0, Lcom/google/android/searchcommon/util/UriRequest;

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/util/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public getBeamUrlFromQuery(Lcom/google/android/velvet/Query;)Ljava/lang/String;
    .locals 5
    .param p1    # Lcom/google/android/velvet/Query;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getWebSearchPattern()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getBeamSearchUri(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setWebCorpus(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getResultIndex()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getResultIndex()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setResultStartIndex(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->build()Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getCardAboveSrpLogUri(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 7
    .param p1    # Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%1$s://%2$s/velog/action"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSingleRequestSearchDomain()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setEventId(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setIncludeCookieHeader()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    return-object v1
.end method

.method public getGeoLocation()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getSearchDomainCountryCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->convertObsoleteGeolocationCodeToNew(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getGsaSearchParametersRequest()Lcom/google/android/searchcommon/util/UriRequest;
    .locals 8

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v4, "https"

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v5, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->getSearchDomainCheckPattern()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "https"

    aput-object v7, v6, v2

    if-nez v0, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/google/android/searchcommon/util/UriRequest;

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->maybeAddCookieHeader(Landroid/net/Uri;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/searchcommon/util/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    return-object v2
.end method

.method public getHistoryApiChangeUrl()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getHistoryApiChangeUrlPattern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "https"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHistoryApiLookupUrl(JJIZZ)Landroid/net/Uri;
    .locals 7
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # Z
    .param p7    # Z

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->getHistoryApiLookupUrlPattern()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "https"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "client"

    iget-object v3, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->getHistoryApiClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    const-string v2, "min"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-eqz v2, :cond_1

    const-string v2, "max"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    const/4 v2, -0x1

    if-eq p5, v2, :cond_2

    const-string v2, "num"

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    if-eqz p6, :cond_3

    const-string v2, "titles"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    if-eqz p7, :cond_4

    const-string v2, "thumbnails"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_4
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method public getHlParameter()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mDefaultLocale:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getHlParameter(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getImageMetadataChunkParam(I)I
    .locals 1
    .param p1    # I

    const/16 v0, 0x14

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x64

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    div-int/lit8 v0, p1, 0x64

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getImageMetadataStartParam(I)I
    .locals 2
    .param p1    # I

    const/16 v0, 0x14

    if-ge p1, v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x64

    if-lt p1, v1, :cond_0

    div-int/lit8 v0, p1, 0x64

    mul-int/lit8 v0, v0, 0x64

    goto :goto_0
.end method

.method public getImageMetadataUrl(Lcom/google/android/velvet/Query;I)Landroid/net/Uri;
    .locals 9
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # I

    const/4 v8, 0x0

    invoke-virtual {p0, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getImageMetadataChunkParam(I)I

    move-result v2

    invoke-virtual {p0, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getImageMetadataStartParam(I)I

    move-result v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v5, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->getImageMetatDataUrlPattern()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    invoke-virtual {p0, v8}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getWebCorpusQueryParam()Ljava/lang/String;

    move-result-object v4

    const-string v5, "isch"

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "biw"

    const-string v6, "100"

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "bih"

    const-string v6, "200"

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "ifm"

    const-string v6, "1"

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "ijn"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "start"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v4, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->shouldSupplyCslParamForImageMetadata()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "csl"

    iget-object v5, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->cslValueForImageMetadata()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    const-string v4, "dbla"

    const-string v5, "1"

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v4, "q"

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    return-object v4
.end method

.method public getLocalSearchDomain()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getCurrentSearchDomain()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->normalizeDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLoginDomainUrl()Landroid/net/Uri;
    .locals 2

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getMajelDomain(Z)Ljava/lang/String;
    .locals 2
    .param p1    # Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->shouldUseGoogleCom()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getDefaultSearchDomain()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->normalizeDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getCurrentMajelDomain()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPrefetchGen204Uri(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 10
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v6}, Lcom/google/android/searchcommon/SearchConfig;->getGoogleGen204Pattern()Ljava/lang/String;

    move-result-object v4

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    const/4 v8, 0x1

    invoke-virtual {p0, v9}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v4, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getSuggestionClickLogInfo()Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getPrefetchSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setQueryString(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {v3}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setOriginalQueryString(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {v1, p3}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setEventId(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    return-object v1
.end method

.method public getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;Z)Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;Z)Lcom/google/android/velvet/Query;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0, p2, p3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchAuthority(Landroid/net/Uri;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchPath(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/net/Uri;->isHierarchical()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getWebCorpusQueryParam()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mCorpora:Lcom/google/android/velvet/Corpora;

    const-string v3, "web"

    invoke-static {v0}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/velvet/Corpora;->getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    const-string v2, "output"

    invoke-direct {p0, p2, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "rss"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "atom"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    :goto_2
    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpora;->areWebCorporaLoaded()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mCorpora:Lcom/google/android/velvet/Corpora;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v3}, Lcom/google/android/velvet/Corpora;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/velvet/Corpora;->getSubCorpora(Lcom/google/android/velvet/Corpus;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/Corpus;

    check-cast v0, Lcom/google/android/velvet/WebCorpus;

    invoke-virtual {v0, p2, v2}, Lcom/google/android/velvet/WebCorpus;->matchesUrl(Landroid/net/Uri;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_3
    const-string v2, "q"

    invoke-direct {p0, p2, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v1, "start"

    const/4 v3, 0x0

    invoke-direct {p0, p2, v1, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getIntQueryParameter(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v1

    invoke-static {p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getAllQueryParameters(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {p1, v2, v0, v1, v3}, Lcom/google/android/velvet/Query;->withQueryStringCorpusIndexAndPersistCgiParameters(Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;)Lcom/google/android/velvet/Query;

    move-result-object v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public getResultTargetAndLogUrl(Landroid/net/Uri;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/searchcommon/util/UriRequest;",
            "Lcom/google/android/searchcommon/util/UriRequest;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchAuthority(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getClickedResultUrlPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getClickedResultDestinationParams()[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    invoke-direct {p0, p1, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getUrlDestination(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getVoiceSearchInstallId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "X-Speech-Cookie"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->maybeAddCookieHeader(Landroid/net/Uri;Ljava/util/Map;)Ljava/util/Map;

    new-instance v1, Lcom/google/android/searchcommon/util/UriRequest;

    invoke-direct {v1, p1, v0}, Lcom/google/android/searchcommon/util/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    new-instance v0, Lcom/google/android/searchcommon/util/UriRequest;

    invoke-direct {v0, v3}, Lcom/google/android/searchcommon/util/UriRequest;-><init>(Landroid/net/Uri;)V

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string v0, "Search.SearchUrlHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/url with no destination param!: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v0, Lcom/google/android/searchcommon/util/UriRequest;

    invoke-direct {v0, p1}, Lcom/google/android/searchcommon/util/UriRequest;-><init>(Landroid/net/Uri;)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1
.end method

.method public getSearchBaseUri(ZZ)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 4
    .param p1    # Z
    .param p2    # Z

    const/4 v3, 0x0

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    if-eqz p1, :cond_2

    const-string v2, "https"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchDomainSchemeHttps()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :goto_0
    new-instance v0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/net/Uri;)V

    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->setIncludeCookieHeader()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    :cond_0
    return-object v0

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method public getSearchBaseUrl()Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getSearchUrlFormat()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->formatUrlBase(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getSearchDomain(Z)Ljava/lang/String;
    .locals 2
    .param p1    # Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->shouldUseGoogleCom()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getDefaultSearchDomain()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->normalizeDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getCurrentSearchDomain()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSearchDomainScheme()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->shouldAllowSslSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "http"

    goto :goto_0
.end method

.method public getSearchRequest(Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/util/UriRequest;
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchUrlBuilder(Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;->build()Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v1

    return-object v1
.end method

.method public getSearchUrlBuilder(Landroid/net/Uri;Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/net/Uri;)V

    if-eqz p2, :cond_0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->buildUpFromQuery(Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;Lcom/google/android/velvet/Query;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method getSearchUrlBuilder(Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchUrlBuilder(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/google/SearchUrlHelper$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getSingleRequestBaseUrl()Ljava/lang/String;
    .locals 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v1}, Lcom/google/android/velvet/Corpora;->getWebCorpus()Lcom/google/android/velvet/WebCorpus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/WebCorpus;->getWebSearchPattern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSingleRequestSearchDomain()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSingleRequestSearchDomain()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getDebugSearchDomainOverride()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Using manual override for single request architecture: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getSearchSingleReqDomain()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Search.SearchUrlHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Using gservices override for single request architecture: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSslPrewarmUri()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getGoogleSslPrewarmPattern()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public isDotComAnyway()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getDefaultSearchDomain()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getCurrentSearchDomain()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isSearchAuthority(Landroid/net/Uri;)Z
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchAuthority(Landroid/net/Uri;Z)Z

    move-result v0

    return v0
.end method

.method public isSearchAuthority(Landroid/net/Uri;Z)Z
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    if-nez p2, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchAuthority(Ljava/lang/String;)Z

    move-result v2

    goto :goto_0
.end method

.method public isSearchAuthority(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getDefaultSearchDomain()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getLocalSearchDomain()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchDomainSchemeHttps()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSearchDomainSchemeSecure()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getDebugSearchSchemeOverride()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSecureGoogleUri(Landroid/net/Uri;)Z
    .locals 4
    .param p1    # Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v3, "https"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v3}, Lcom/google/android/searchcommon/SearchSettings;->getDebugSearchSchemeOverride()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getDefaultSearchDomain()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->endsWithIfPresent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getCurrentSearchDomain()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->endsWithIfPresent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public maybeAddCookieHeader(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/Cookies;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez p2, :cond_0

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object p2

    :cond_0
    const-string v1, "Cookie"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object p2
.end method

.method public parseMaybeRelative(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomainScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchDomain(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public setBrowserDimensionsSupplier(Lcom/google/common/base/Supplier;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Landroid/graphics/Point;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mBrowserDimensionsSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSource:Ljava/lang/String;

    return-void
.end method

.method public shouldAllowBackBetween(Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 12
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {p0, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchAuthority(Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getPathQueryAndFragment(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    iget-object v10, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v10}, Lcom/google/android/searchcommon/SearchConfig;->getAllowBackFromUrlWhitelist()[Ljava/lang/String;

    move-result-object v9

    move-object v0, v9

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v7, v0, v3

    invoke-virtual {v6, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v8, 0x1

    :cond_0
    :goto_1
    return v8

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-static {p1, p2}, Lcom/google/android/searchcommon/util/UriDiff;->diff(Landroid/net/Uri;Landroid/net/Uri;)Lcom/google/android/searchcommon/util/UriDiff;

    move-result-object v2

    sget-object v10, Lcom/google/android/searchcommon/util/UriDiff;->SAME:Lcom/google/android/searchcommon/util/UriDiff;

    if-eq v2, v10, :cond_0

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriDiff;->authorityDifferent()Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriDiff;->schemeDifferent()Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriDiff;->pathDifferent()Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriDiff;->queryDiffs()Ljava/util/Set;

    move-result-object v10

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriDiff;->fragmentDiffs()Ljava/util/Set;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/common/collect/Sets;->union(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    iget-object v10, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v10}, Lcom/google/android/searchcommon/SearchConfig;->getChangingParamsThatAllowBackNavigation()Ljava/util/Set;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/google/common/collect/Sets;->difference(Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/collect/Sets$SetView;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/common/collect/Sets$SetView;->isEmpty()Z

    move-result v8

    goto :goto_1
.end method

.method public shouldRequestPelletResponse(Lcom/google/android/velvet/Query;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/SearchUrlHelper;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->shouldShowCards()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
