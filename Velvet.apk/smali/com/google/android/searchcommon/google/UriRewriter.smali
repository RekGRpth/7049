.class public Lcom/google/android/searchcommon/google/UriRewriter;
.super Ljava/lang/Object;
.source "UriRewriter.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/HttpHelper$UrlRewriter;


# instance fields
.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/UriRewriter;->mResolver:Landroid/content/ContentResolver;

    return-void
.end method


# virtual methods
.method public rewrite(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/UriRewriter;->mResolver:Landroid/content/ContentResolver;

    invoke-static {v2}, Lcom/google/android/common/http/UrlRules;->getRules(Landroid/content/ContentResolver;)Lcom/google/android/common/http/UrlRules;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/common/http/UrlRules;->matchRule(Ljava/lang/String;)Lcom/google/android/common/http/UrlRules$Rule;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/common/http/UrlRules$Rule;->apply(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
