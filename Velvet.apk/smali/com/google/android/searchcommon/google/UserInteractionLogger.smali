.class public Lcom/google/android/searchcommon/google/UserInteractionLogger;
.super Ljava/lang/Object;
.source "UserInteractionLogger.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;,
        Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;,
        Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;
    }
.end annotation


# static fields
.field private static final METRIC_ACTION_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final NO_OP_INTERACTION_TIMER:Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mGaInstance:Lcom/google/analytics/tracking/android/GoogleAnalytics;

.field private mLastSessionEndedMillis:J

.field private mLastSessionStartedMillis:J

.field private mLastViewName:Ljava/lang/String;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private final mRandom:Ljava/util/Random;

.field private final mSessionTimeoutHandler:Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;

.field private final mTracker:Lcom/google/analytics/tracking/android/Tracker;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const-string v0, "CARD_BUTTON_PRESS"

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CARD_DETAILS"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "NOTIFICATION_CLICK"

    const/16 v5, 0x17

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "NOTIFICATION_ACTION_PRESS"

    const/16 v7, 0x18

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v8, "WIDGET_PRESS"

    const/16 v9, 0x1b

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->METRIC_ACTION_MAP:Ljava/util/Map;

    new-instance v0, Lcom/google/android/searchcommon/google/UserInteractionLogger$1;

    invoke-direct {v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger$1;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->NO_OP_INTERACTION_TIMER:Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/GsaPreferenceController;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mClock:Lcom/google/android/searchcommon/util/Clock;

    new-instance v0, Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;-><init>(Lcom/google/android/searchcommon/google/UserInteractionLogger;Lcom/google/android/searchcommon/google/UserInteractionLogger$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mSessionTimeoutHandler:Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;

    invoke-static {p1}, Lcom/google/analytics/tracking/android/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/analytics/tracking/android/GoogleAnalytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mGaInstance:Lcom/google/analytics/tracking/android/GoogleAnalytics;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mRandom:Ljava/util/Random;

    invoke-static {}, Lcom/google/analytics/tracking/android/GAServiceManager;->getInstance()Lcom/google/analytics/tracking/android/GAServiceManager;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/analytics/tracking/android/GAServiceManager;->setDispatchPeriod(I)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mGaInstance:Lcom/google/analytics/tracking/android/GoogleAnalytics;

    const-string v1, "UA-25271179-2"

    invoke-virtual {v0, v1}, Lcom/google/analytics/tracking/android/GoogleAnalytics;->getTracker(Ljava/lang/String;)Lcom/google/analytics/tracking/android/Tracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/google/UserInteractionLogger;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->endSession()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/google/UserInteractionLogger;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/UserInteractionLogger;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/google/UserInteractionLogger;)Lcom/google/analytics/tracking/android/Tracker;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/UserInteractionLogger;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;

    return-object v0
.end method

.method private endSession()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mSessionTimeoutHandler:Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->isInSession()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mLastSessionEndedMillis:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mLastViewName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->sendEmptyEvent()V

    :cond_0
    return-void
.end method

.method private isInSession()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mLastSessionEndedMillis:J

    iget-wide v2, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mLastSessionStartedMillis:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendEmptyEvent()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/analytics/tracking/android/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method private startNewSession()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mLastSessionStartedMillis:J

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/analytics/tracking/android/Tracker;->setStartSession(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mLastViewName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->sendEmptyEvent()V

    return-void
.end method


# virtual methods
.method public flushEvents()V
    .locals 1

    invoke-static {}, Lcom/google/analytics/tracking/android/GAServiceManager;->getInstance()Lcom/google/analytics/tracking/android/GAServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/analytics/tracking/android/GAServiceManager;->dispatch()V

    return-void
.end method

.method public logInternalAction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;

    const-string v1, "INTERNAL"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/analytics/tracking/android/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public logInternalActionOncePerDay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v6}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "uil_limiter_internal_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    div-long v0, v6, v8

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v6, v0, v2

    if-lez v6, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logInternalAction(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6, v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public logMetricsAction(Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$Entry;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Entry;

    sget-object v4, Lcom/google/android/searchcommon/google/UserInteractionLogger;->METRIC_ACTION_MAP:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Entry;->getEntryActionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Action;

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$Action;->getType()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getExecutedUserActionStore()Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    move-result-object v4

    const-wide/16 v5, -0x1

    invoke-interface {v4, p2, v0, v5, v6}, Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;->saveAction(Lcom/google/geo/sidekick/Sidekick$Entry;Lcom/google/geo/sidekick/Sidekick$Action;J)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public logTiming(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimerImpl;-><init>(Lcom/google/android/searchcommon/google/UserInteractionLogger;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public logTimingSometimes(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v8}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "uil_limiter_timing_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v8}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v8, v0, v2

    if-lez v8, :cond_0

    const-wide/32 v8, 0x3dcc500

    add-long/2addr v8, v0

    iget-object v10, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mRandom:Ljava/util/Random;

    const v11, 0x2932e00

    invoke-virtual {v10, v11}, Ljava/util/Random;->nextInt(I)I

    move-result v10

    int-to-long v10, v10

    add-long v4, v8, v10

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8, v7, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logTiming(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;

    move-result-object v8

    :goto_0
    return-object v8

    :cond_0
    sget-object v8, Lcom/google/android/searchcommon/google/UserInteractionLogger;->NO_OP_INTERACTION_TIMER:Lcom/google/android/searchcommon/google/UserInteractionLogger$UserInteractionTimer;

    goto :goto_0
.end method

.method public logUiAction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;

    const-string v1, "UI_ACTION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/analytics/tracking/android/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public logUiActionOnEntryAdapter(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V

    return-void
.end method

.method public logUiActionOnEntryAdapterWithLabel(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p3    # Ljava/lang/String;

    if-nez p3, :cond_0

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-interface {p2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logMetricsAction(Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public logUiActionOnEntryNotification(Ljava/lang/String;Lcom/google/android/apps/sidekick/notifications/EntryNotification;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/sidekick/notifications/EntryNotification;

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getLoggingName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/notifications/EntryNotification;->getEntries()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logMetricsAction(Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public logUiActionOnEntryNotificationWithLabel(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logMetricsAction(Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$Entry;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public logUiActionOnStacks(Ljava/lang/String;Ljava/util/List;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/HashMultiset;->create()Lcom/google/common/collect/HashMultiset;

    move-result-object v5

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntriesToShow()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getLoggingName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/common/collect/Multiset;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v5}, Lcom/google/common/collect/Multiset;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/collect/Multiset$Entry;

    invoke-interface {v1}, Lcom/google/common/collect/Multiset$Entry;->getElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/common/collect/Multiset$Entry;->getCount()I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {p0, p1, v6, v7, v8}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionWithValue(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public logUiActionWithValue(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;

    const-string v1, "UI_ACTION"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/analytics/tracking/android/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public logView(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mLastViewName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mLastViewName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mTracker:Lcom/google/analytics/tracking/android/Tracker;

    invoke-virtual {v0, p1}, Lcom/google/analytics/tracking/android/Tracker;->trackView(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSessionStart()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mSessionTimeoutHandler:Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->isInSession()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->startNewSession()V

    :cond_0
    return-void
.end method

.method public onSessionStop()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/searchcommon/google/UserInteractionLogger;->mSessionTimeoutHandler:Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;

    const/4 v1, 0x1

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger$SessionTimeoutHandler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
