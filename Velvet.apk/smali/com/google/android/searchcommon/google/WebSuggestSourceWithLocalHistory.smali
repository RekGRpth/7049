.class public Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;
.super Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;
.source "WebSuggestSourceWithLocalHistory.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;
    }
.end annotation


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private mEnabled:Z

.field private final mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

.field private mHistoryObserver:Landroid/database/DataSetObserver;

.field private final mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private mZeroQuerySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/google/WebSuggestSource;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/GlobalSearchServices;Landroid/database/DataSetObservable;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/google/WebSuggestSource;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Lcom/google/android/searchcommon/SearchSettings;
    .param p5    # Lcom/google/android/searchcommon/GlobalSearchServices;
    .param p6    # Landroid/database/DataSetObservable;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;-><init>(Lcom/google/android/searchcommon/google/WebSuggestSource;)V

    iput-object p5, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p4, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p6, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

    new-instance v0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$1;-><init>(Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mHistoryObserver:Landroid/database/DataSetObserver;

    invoke-interface {p4, p0}, Lcom/google/android/searchcommon/SearchSettings;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->updateEnabled()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->fetchZeroQuerySuggestions()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->setZeroQuerySuggestions(Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    return-void
.end method

.method private fetchZeroQuerySuggestions()V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->setZeroQuerySuggestions(Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getMaxLocalHistorySuggestions()I

    move-result v2

    new-instance v3, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$2;

    invoke-direct {v3, p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$2;-><init>(Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;)V

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->getLocalHistory(Lcom/google/android/velvet/Query;ILcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method private getLatency(Lcom/google/android/searchcommon/suggest/SuggestionList;)I
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getLatency()I

    move-result v0

    goto :goto_0
.end method

.method private declared-synchronized isZeroQueryEmpty()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mZeroQuerySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mZeroQuerySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setZeroQuerySuggestions(Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mZeroQuerySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateEnabled()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mEnabled:Z

    if-eq v0, v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mEnabled:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->fetchZeroQuerySuggestions()V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mHistoryObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mHistoryObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method


# virtual methods
.method addAtMost(ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .param p3    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-interface {p3, v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method blendResults(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p3    # Lcom/google/android/velvet/Query;

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;

    invoke-super {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->getLatency(Lcom/google/android/searchcommon/suggest/SuggestionList;)I

    move-result v1

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->getLatency(Lcom/google/android/searchcommon/suggest/SuggestionList;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setLatency(I)V

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->addAll(Ljava/lang/Iterable;)I

    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    invoke-interface {v0, p2}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->addAll(Ljava/lang/Iterable;)I

    :cond_2
    return-object v0

    :cond_3
    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getPartialWebResultsThresholdForLocalHistory()I

    move-result v2

    if-ge v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getMaxLocalHistoryForPartialWebResults()I

    move-result v1

    invoke-virtual {p0, v1, v0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->addAtMost(ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getMaxLocalHistoryForFullWebResults()I

    move-result v1

    invoke-virtual {p0, v1, v0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->addAtMost(ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    goto :goto_0
.end method

.method public getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mEnabled:Z

    if-eqz v0, :cond_0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mZeroQuerySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    monitor-exit p0

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    goto :goto_0
.end method

.method public getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation

    iget-boolean v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mEnabled:Z

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;

    invoke-super {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getMaxLocalHistorySuggestions()I

    move-result v2

    invoke-interface {v1, p1, v2, v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->getLocalHistory(Lcom/google/android/velvet/Query;ILcom/google/android/searchcommon/util/Consumer;)V

    invoke-super {p0, p1, v0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->await()Z

    iget-object v1, v0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mHistorySuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v2, v0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory$CombinedConsumer;->mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->blendResults(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_0
.end method

.method public isLikelyToReturnZeroQueryResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mEnabled:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->isZeroQueryEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->isLikelyToReturnZeroQueryResults()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public logClick(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    iget-boolean v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->insertLocalHistory(Ljava/lang/String;J)V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->logClick(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->updateEnabled()V

    return-void
.end method

.method public removeFromHistory(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mEnabled:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/WebSuggestSourceWithLocalHistory;->mGss:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getWebHistoryRepository()Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->deleteLocalHistoryItem(Ljava/lang/String;Z)V

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->removeFromHistory(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
