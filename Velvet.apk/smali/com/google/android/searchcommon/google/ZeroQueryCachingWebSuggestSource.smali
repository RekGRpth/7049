.class public Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;
.super Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;
.source "ZeroQueryCachingWebSuggestSource.java"


# instance fields
.field final mAccountsObserver:Landroid/database/DataSetObserver;

.field private final mEmptyResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private final mHistoryObservable:Landroid/database/DataSetObservable;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/google/WebSuggestSource;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/database/DataSetObservable;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/google/WebSuggestSource;
    .param p2    # Lcom/google/android/searchcommon/SearchSettings;
    .param p3    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p4    # Landroid/database/DataSetObservable;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;-><init>(Lcom/google/android/searchcommon/google/WebSuggestSource;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    new-instance v0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource$1;-><init>(Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mAccountsObserver:Landroid/database/DataSetObserver;

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->getSourceName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mEmptyResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->loadSuggestions()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->validateCache(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mAccountsObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iput-object p4, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mHistoryObservable:Landroid/database/DataSetObservable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;Z)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->validateCache(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->cacheResults(Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    return-void
.end method

.method private declared-synchronized cacheResults(Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->isValid(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-static {v1}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->serializeSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionList;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/SearchSettings;->getCachedZeroQueryWebResults()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/SearchSettings;->setCachedZeroQueryWebResults(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mHistoryObservable:Landroid/database/DataSetObservable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mHistoryObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v1}, Landroid/database/DataSetObservable;->notifyChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private clearCache()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setCachedZeroQueryWebResults(Ljava/lang/String;)V

    return-void
.end method

.method private isValid(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->isRequestFailed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getAccount()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getAccount()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadSuggestions()Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 14

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v12}, Lcom/google/android/searchcommon/SearchSettings;->getCachedZeroQueryWebResults()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v12, "source"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v12, "account"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v12, "q"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    sget-object v12, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    invoke-virtual {v12, v7}, Lcom/google/android/velvet/Query;->withQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query;

    move-result-object v10

    new-instance v5, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;

    invoke-direct {v5, v8, v10}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    invoke-interface {v5, v0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setAccount(Ljava/lang/String;)V

    const-string v12, "suggestions"

    invoke-virtual {v4, v12}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v2, v12, :cond_1

    invoke-virtual {v9, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/android/searchcommon/suggest/Suggestion;->builder()Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text1(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v12

    const-string v13, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v12, v13}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentAction(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->suggestionQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isHistory(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v12

    sget-object v13, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_SEARCH_HISTORY:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->logType(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->build()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v12

    invoke-interface {v5, v12}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v12, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v12, v11}, Lcom/google/android/searchcommon/SearchSettings;->setCachedZeroQueryWebResults(Ljava/lang/String;)V

    :cond_0
    move-object v5, v11

    :cond_1
    return-object v5
.end method

.method private declared-synchronized removeQueryFromCache(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->clearCache()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static serializeSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionList;)Ljava/lang/String;
    .locals 6
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "source"

    invoke-interface {p0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v4, "account"

    invoke-interface {p0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getAccount()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v4, "q"

    invoke-interface {p0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v4

    if-ge v1, v4, :cond_0

    invoke-interface {p0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v4, "suggestions"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :goto_1
    return-object v4

    :catch_0
    move-exception v0

    const/4 v4, 0x0

    goto :goto_1
.end method

.method private declared-synchronized validateCache(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->isValid(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->clearCache()V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mEmptyResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isEmptySuggestQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-super {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isEmptySuggestQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, p2

    new-instance p2, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource$2;

    invoke-direct {p2, p0, v0}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource$2;-><init>(Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;Lcom/google/android/searchcommon/util/Consumer;)V

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->getSuggestions(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public declared-synchronized isLikelyToReturnZeroQueryResults()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->mZeroQueryCache:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-super {p0}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->isLikelyToReturnZeroQueryResults()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public removeFromHistory(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/ZeroQueryCachingWebSuggestSource;->removeQueryFromCache(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/google/WebSuggestSourceWrapper;->removeFromHistory(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
