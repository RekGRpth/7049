.class Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;
.super Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback;
.source "LoginHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/gaia/LoginHelper;->updateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$authTokenType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/gaia/LoginHelper;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->val$authTokenType:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->val$account:Landroid/accounts/Account;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback;-><init>(Lcom/google/android/searchcommon/google/gaia/LoginHelper$1;)V

    return-void
.end method


# virtual methods
.method public handleResult(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    # getter for: Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->access$200(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v1

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "authtoken"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->val$account:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->val$authTokenType:Ljava/lang/String;

    # invokes: Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->access$300(Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    # invokes: Lcom/google/android/searchcommon/google/gaia/LoginHelper;->notifyChanged()V
    invoke-static {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->access$400(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)V

    goto :goto_0
.end method

.method public bridge synthetic handleResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;->handleResult(Landroid/os/Bundle;)V

    return-void
.end method
