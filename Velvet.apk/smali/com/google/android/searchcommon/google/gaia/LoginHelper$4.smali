.class Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;
.super Ljava/lang/Object;
.source "LoginHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getGaiaWebLoginLink(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/searchcommon/util/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$consumer:Lcom/google/android/searchcommon/util/Consumer;

.field final synthetic val$continueToUrl:Landroid/net/Uri;

.field final synthetic val$service:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/searchcommon/util/Consumer;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->val$continueToUrl:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->val$service:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    iput-object p5, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->val$continueToUrl:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->val$service:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->blockingGetGaiaWebLoginLink(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->val$consumer:Lcom/google/android/searchcommon/util/Consumer;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->val$account:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;->this$0:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    # getter for: Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->access$600(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Landroid/accounts/Account;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
