.class public Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;
.super Ljava/lang/Object;
.source "LoginHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/google/gaia/LoginHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AuthToken"
.end annotation


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->mAccount:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->mToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->mAccount:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->mToken:Ljava/lang/String;

    return-object v0
.end method
