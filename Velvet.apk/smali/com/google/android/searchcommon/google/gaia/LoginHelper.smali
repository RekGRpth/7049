.class public Lcom/google/android/searchcommon/google/gaia/LoginHelper;
.super Ljava/lang/Object;
.source "LoginHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;,
        Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private mAccounts:[Landroid/accounts/Account;

.field private final mAccountsListener:Landroid/accounts/OnAccountsUpdateListener;

.field private final mAppContext:Landroid/content/Context;

.field private final mAuthTokens:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mDataSetObservable:Landroid/database/DataSetObservable;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchSettings;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-interface {p3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAppContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    new-instance v0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$1;-><init>(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountsListener:Landroid/accounts/OnAccountsUpdateListener;

    iput-object p3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p4, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    if-nez v0, :cond_0

    const-string v0, "Search.LoginHelper"

    const-string v1, "Missing account manager."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountsListener:Landroid/accounts/OnAccountsUpdateListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->refresh()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p1    # Landroid/accounts/Account;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->notifyChanged()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Landroid/accounts/AccountManager;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/google/gaia/LoginHelper;)Landroid/accounts/Account;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method private declared-synchronized findAccountByName(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_2

    :cond_0
    move-object v0, v4

    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v0, v1, v2

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-nez v5, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move-object v0, v4

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method private getSettings()Lcom/google/android/searchcommon/SearchSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method private declared-synchronized handleAccounts([Landroid/accounts/Account;)V
    .locals 4
    .param p1    # [Landroid/accounts/Account;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;

    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setAccount(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/SearchSettings;->setGoogleAccountToUse(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->notifyChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->findAccountByName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->getSignedOut()Z

    move-result v2

    if-nez v2, :cond_3

    if-nez v0, :cond_3

    const/4 v2, 0x0

    aget-object v0, p1, v2

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/SearchSettings;->setGoogleAccountToUse(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setAccount(Landroid/accounts/Account;)V

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->updateAuthTokens(Landroid/accounts/Account;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private invalidateAuthToken(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    if-nez v3, :cond_3

    monitor-exit p0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    iget-object v1, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz p2, :cond_4

    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    const/4 v4, 0x0

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v3, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-direct {p0, v0, p1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->updateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    :try_start_2
    iget-object v3, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private notifyChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void
.end method

.method private declared-synchronized refresh()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->handleAccounts([Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized requireAuthTokenType(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    if-eqz p2, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->refresh()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized setAccount(Landroid/accounts/Account;)V
    .locals 4
    .param p1    # Landroid/accounts/Account;

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private declared-synchronized setAccountToUse(Landroid/accounts/Account;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->disableForAccount(Landroid/accounts/Account;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setAccount(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/SearchSettings;->setSignedOut(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->updateAuthTokens(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/SearchSettings;->setGoogleAccountToUse(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized setToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/accounts/Account;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$2;-><init>(Lcom/google/android/searchcommon/google/gaia/LoginHelper;Ljava/lang/String;Landroid/accounts/Account;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$3;-><init>(Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/searchcommon/google/gaia/LoginHelper$Callback;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private updateAuthTokens(Landroid/accounts/Account;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->updateAuthToken(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public blockingGetAuthTokenForAccount(Ljava/lang/String;Landroid/accounts/Account;)Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/accounts/Account;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkNotMainThread()V

    const/4 v11, 0x0

    const/4 v7, 0x0

    :goto_0
    const/4 v0, 0x3

    if-ge v7, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    const/4 v4, 0x1

    move-object v1, p2

    move-object v2, p1

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v10

    const-wide/16 v0, 0x5

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v10, v0, v1, v2}, Landroid/accounts/AccountManagerFuture;->getResult(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Bundle;

    if-nez v8, :cond_1

    const-string v0, "Search.LoginHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "blockingGetAuthToken: null was returned from getResult() for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", authTokenType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return-object v3

    :cond_1
    const-string v0, "authtoken"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v11

    :goto_2
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1, v11}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catch_0
    move-exception v9

    const-string v0, "Search.LoginHelper"

    const-string v1, "Operation canceled while trying to get auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_1
    move-exception v9

    const-string v0, "Search.LoginHelper"

    const-string v1, "Authenticator exception while trying to get auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_2
    move-exception v9

    const-string v0, "Search.LoginHelper"

    const-string v1, "IOException while trying to get auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    new-instance v3, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v3, v0, v11}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public blockingGetGaiaWebLoginLink(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 8
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    if-nez p2, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "weblogin:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "continue="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&de=1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v3, v7}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    if-nez v2, :cond_1

    :goto_1
    return-object v4

    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "weblogin:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "service="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&continue="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&de=1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v4

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v5, "Search.LoginHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception while getting uber auth token: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/accounts/OperationCanceledException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v5, "Search.LoginHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception while getting uber auth token: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/accounts/AuthenticatorException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v5, "Search.LoginHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception while getting uber auth token: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public declared-synchronized getAccount()Landroid/accounts/Account;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getAllAccountNames()[Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->refresh()V

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    new-array v1, v2, [Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v1

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;

    array-length v2, v2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;

    aget-object v2, v2, v0

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v2, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getAllAccounts()[Landroid/accounts/Account;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->refresh()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccounts:[Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAuthToken(Ljava/lang/String;)Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccountName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAuthTokens:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    new-instance v2, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    invoke-direct {v2, v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getGaiaWebLoginLink(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mAccount:Landroid/accounts/Account;

    iget-object v6, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$4;-><init>(Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/searchcommon/util/Consumer;Landroid/accounts/Account;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public declared-synchronized hasAccount(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->findAccountByName(Ljava/lang/String;)Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public invalidateAuthToken(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->invalidateAuthToken(Ljava/lang/String;Z)V

    return-void
.end method

.method public declared-synchronized logOut()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setSignedOut(Z)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setGoogleAccountToUse(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setAccount(Landroid/accounts/Account;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->notifyChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public declared-synchronized requireAuthTokenType(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->requireAuthTokenType(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAccountToUseByName(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->findAccountByName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setAccountToUse(Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v1, "Search.LoginHelper"

    const-string v2, "setAccountToUseByName: Account name not found."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method
