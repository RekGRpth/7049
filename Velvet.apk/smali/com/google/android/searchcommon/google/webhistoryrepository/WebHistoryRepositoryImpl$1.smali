.class Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;
.super Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$LocalHistoryTask;
.source "WebHistoryRepositoryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->getLocalHistory(Lcom/google/android/velvet/Query;ILcom/google/android/searchcommon/util/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

.field final synthetic val$localHistory:Lcom/google/android/searchcommon/util/Consumer;

.field final synthetic val$maxHistory:I

.field final synthetic val$query:Lcom/google/android/velvet/Query;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;Lcom/google/android/velvet/Query;ILcom/google/android/searchcommon/util/Consumer;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iput-object p2, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->val$query:Lcom/google/android/velvet/Query;

    iput p3, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->val$maxHistory:I

    iput-object p4, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->val$localHistory:Lcom/google/android/searchcommon/util/Consumer;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$LocalHistoryTask;-><init>(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/searchcommon/util/Latency;

    iget-object v2, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    # getter for: Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v2}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->access$100(Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/searchcommon/util/Latency;-><init>(Lcom/google/android/searchcommon/util/Clock;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->this$0:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;

    iget-object v3, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->val$query:Lcom/google/android/velvet/Query;

    iget v4, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->val$maxHistory:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl;->getLocalHistoryForQuery(Lcom/google/android/velvet/Query;I)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/Latency;->getLatency()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->setLatency(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->val$localHistory:Lcom/google/android/searchcommon/util/Consumer;

    invoke-interface {v2, v0}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepositoryImpl$1;->val$localHistory:Lcom/google/android/searchcommon/util/Consumer;

    invoke-interface {v3, v0}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    throw v2
.end method
