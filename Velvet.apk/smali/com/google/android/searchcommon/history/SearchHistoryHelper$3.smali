.class Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;
.super Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;
.source "SearchHistoryHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/history/SearchHistoryHelper;->setHistoryEnabled(Landroid/accounts/Account;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

.field final synthetic val$json:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;->this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iput-object p2, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;->val$json:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;-><init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Lcom/google/android/searchcommon/history/SearchHistoryHelper$1;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic runRequest(Ljava/util/Map;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;->runRequest(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public runRequest(Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;

    iget-object v1, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;->this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    # getter for: Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;
    invoke-static {v1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->access$100(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getHistoryApiChangeUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;->val$json:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;->setContent(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;->this$0:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    # getter for: Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;
    invoke-static {v1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->access$200(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v0, v2}, Lcom/google/android/searchcommon/util/HttpHelper;->post(Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
