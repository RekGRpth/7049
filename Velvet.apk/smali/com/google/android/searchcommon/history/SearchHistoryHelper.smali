.class public Lcom/google/android/searchcommon/history/SearchHistoryHelper;
.super Ljava/lang/Object;
.source "SearchHistoryHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/history/SearchHistoryHelper$AuthRequestWithRetry;,
        Lcom/google/android/searchcommon/history/SearchHistoryHelper$BgTask;
    }
.end annotation


# instance fields
.field private final mBgThread:Ljava/util/concurrent/Executor;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mUiThread:Ljava/util/concurrent/Executor;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/SearchConfig;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p2    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p3    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p4    # Lcom/google/android/searchcommon/SearchConfig;
    .param p5    # Ljava/util/concurrent/Executor;
    .param p6    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p2, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p3, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v1, "oauth2:https://www.googleapis.com/auth/webhistory"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->requireAuthTokenType(Ljava/lang/String;)V

    iput-object p4, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p5, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mBgThread:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mUiThread:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)Lcom/google/android/searchcommon/util/HttpHelper;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->parseJsonResponse(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Landroid/accounts/Account;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->getHistoryEnabled(Landroid/accounts/Account;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Landroid/accounts/Account;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .param p1    # Landroid/accounts/Account;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->setHistoryEnabled(Landroid/accounts/Account;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mUiThread:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mBgThread:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Landroid/accounts/Account;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;
    .param p1    # Landroid/accounts/Account;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->getHeaders(Landroid/accounts/Account;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    return-object v0
.end method

.method private getHeaders(Landroid/accounts/Account;)Ljava/util/Map;
    .locals 5
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v3, "oauth2:https://www.googleapis.com/auth/webhistory"

    invoke-virtual {v2, v3, p1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->blockingGetAuthTokenForAccount(Ljava/lang/String;Landroid/accounts/Account;)Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bearer "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper$AuthToken;->getToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "X-Developer-Key"

    const-string v3, "1016085902054.apps.googleusercontent.com"

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private getHistoryEnabled(Landroid/accounts/Account;)Ljava/lang/Boolean;
    .locals 1
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$1;-><init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;)V

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$1;->run(Landroid/accounts/Account;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method private parseJsonResponse(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "history_recording_enabled"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private setHistoryEnabled(Landroid/accounts/Account;Z)Z
    .locals 5
    .param p1    # Landroid/accounts/Account;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "enable_history_recording"

    invoke-virtual {v1, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v3, "client"

    iget-object v4, p0, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getHistoryApiClientId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;

    invoke-direct {v3, p0, v0}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;-><init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$3;->run(Landroid/accounts/Account;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getHistoryEnabled(Landroid/accounts/Account;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;-><init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Landroid/accounts/Account;Lcom/google/android/searchcommon/util/Consumer;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$2;->execute([Ljava/lang/Object;)V

    return-void
.end method

.method public setHistoryEnabled(Landroid/accounts/Account;ZLcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .param p1    # Landroid/accounts/Account;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Z",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/history/SearchHistoryHelper$4;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$4;-><init>(Lcom/google/android/searchcommon/history/SearchHistoryHelper;Landroid/accounts/Account;ZLcom/google/android/searchcommon/util/Consumer;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper$4;->execute([Ljava/lang/Object;)V

    return-void
.end method
