.class Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry$1;
.super Lcom/google/android/searchcommon/util/NowOrLaterWrapper;
.source "CachingImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;-><init>(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/NowOrLaterWrapper",
        "<",
        "Landroid/graphics/drawable/Drawable$ConstantState;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;

.field final synthetic val$this$0:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;Lcom/google/android/searchcommon/util/NowOrLater;Lcom/google/android/searchcommon/imageloader/CachingImageLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry$1;->this$1:Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;

    iput-object p3, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry$1;->val$this$0:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/util/NowOrLaterWrapper;-><init>(Lcom/google/android/searchcommon/util/NowOrLater;)V

    return-void
.end method


# virtual methods
.method public get(Landroid/graphics/drawable/Drawable$ConstantState;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable$ConstantState;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry$1;->get(Landroid/graphics/drawable/Drawable$ConstantState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
