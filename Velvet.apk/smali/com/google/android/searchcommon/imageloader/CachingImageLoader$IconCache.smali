.class Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;
.super Landroid/support/v4/util/LruCache;
.source "CachingImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IconCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Landroid/net/Uri;",
        "Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;->this$0:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected create(Landroid/net/Uri;)Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;
    .locals 2
    .param p1    # Landroid/net/Uri;

    new-instance v0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;->this$0:Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    invoke-direct {v0, v1, p1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;-><init>(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;Landroid/net/Uri;)V

    return-object v0
.end method

.method protected bridge synthetic create(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;->create(Landroid/net/Uri;)Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;

    move-result-object v0

    return-object v0
.end method

.method protected entryRemoved(ZLandroid/net/Uri;Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;)V
    .locals 0
    .param p1    # Z
    .param p2    # Landroid/net/Uri;
    .param p3    # Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;
    .param p4    # Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;

    return-void
.end method

.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Z
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;
    .param p4    # Ljava/lang/Object;

    check-cast p2, Landroid/net/Uri;

    check-cast p3, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;

    check-cast p4, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;->entryRemoved(ZLandroid/net/Uri;Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;)V

    return-void
.end method
