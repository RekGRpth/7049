.class public Lcom/google/android/searchcommon/imageloader/CachingImageLoader;
.super Ljava/lang/Object;
.source "CachingImageLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/UriLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;,
        Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/UriLoader",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final mIconCache:Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;

.field private final mWrapped:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/UriLoader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    new-instance v0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;

    const/16 v1, 0x32

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;-><init>(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->mIconCache:Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/imageloader/CachingImageLoader;)Lcom/google/android/searchcommon/util/UriLoader;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    return-object v0
.end method


# virtual methods
.method getWrapped()Lcom/google/android/searchcommon/util/UriLoader;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->mWrapped:Lcom/google/android/searchcommon/util/UriLoader;

    return-object v0
.end method

.method public load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/android/searchcommon/util/Util;->isEmpty(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "0"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/android/searchcommon/util/Now;->returnNull()Lcom/google/android/searchcommon/util/Now;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->mIconCache:Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$Entry;->getDrawable()Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v1

    goto :goto_0
.end method

.method public purge()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->mIconCache:Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader$IconCache;->evictAll()V

    return-void
.end method
