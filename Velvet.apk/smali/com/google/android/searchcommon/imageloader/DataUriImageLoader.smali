.class public Lcom/google/android/searchcommon/imageloader/DataUriImageLoader;
.super Lcom/google/android/searchcommon/util/SynchronousLoader;
.source "DataUriImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/SynchronousLoader",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/SynchronousLoader;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/DataUriImageLoader;->mResources:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public loadNow(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1    # Landroid/net/Uri;

    invoke-static {p1}, Lcom/google/android/searchcommon/util/Util;->parseDataUri(Landroid/net/Uri;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/google/android/searchcommon/imageloader/DataUriImageLoader;->mResources:Landroid/content/res/Resources;

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public bridge synthetic loadNow(Landroid/net/Uri;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/imageloader/DataUriImageLoader;->loadNow(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
