.class Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;
.super Lcom/google/android/searchcommon/util/SynchronousLoader;
.source "PackageImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/imageloader/PackageImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncronousImageLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/SynchronousLoader",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;->this$0:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/SynchronousLoader;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;Lcom/google/android/searchcommon/imageloader/PackageImageLoader$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/imageloader/PackageImageLoader;
    .param p2    # Lcom/google/android/searchcommon/imageloader/PackageImageLoader$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;-><init>(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;)V

    return-void
.end method

.method private doLoadNow(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    .locals 13
    .param p1    # Landroid/net/Uri;

    const/4 v11, 0x0

    invoke-static {p1}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->isNullDrawable(Landroid/net/Uri;)Z

    move-result v9

    if-eqz v9, :cond_0

    move-object v9, v11

    :goto_0
    return-object v9

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iget-object v9, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;->this$0:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    # getter for: Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->access$100(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    goto :goto_0

    :catch_0
    move-exception v4

    :try_start_1
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    const-string v9, "android.resource"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;->this$0:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    # getter for: Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->access$100(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;)Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, p1}, Lcom/google/android/searchcommon/util/Util;->getResourceId(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v5

    :try_start_2
    iget-object v9, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v9, Landroid/content/res/Resources;

    iget-object v10, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v9

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_3
    new-instance v9, Ljava/io/FileNotFoundException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Resource does not exist: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v1

    const-string v9, "Search.PackageImageLoader"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to load icon: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, ", "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v9, v11

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    const/4 v8, 0x0

    :try_start_4
    iget-object v9, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;->this$0:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    # getter for: Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->access$100(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {v9, p1}, Landroid/content/ContentResolver;->acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    const-string v9, "r"

    invoke-virtual {v0, p1, v9}, Landroid/content/ContentProviderClient;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v8, v11

    :goto_1
    if-nez v8, :cond_3

    new-instance v9, Ljava/io/FileNotFoundException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to open "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v9

    :try_start_5
    invoke-static {v8}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    # invokes: Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V
    invoke-static {v3}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->access$200(Landroid/content/res/AssetFileDescriptor;)V

    throw v9
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :cond_2
    :try_start_6
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v8

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;->this$0:Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    # getter for: Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->access$100(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v12, 0x0

    invoke-static {v9, v10, v8, v12}, Landroid/graphics/drawable/Drawable;->createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v9

    :try_start_7
    invoke-static {v8}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    # invokes: Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V
    invoke-static {v3}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->access$200(Landroid/content/res/AssetFileDescriptor;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0
.end method


# virtual methods
.method public loadNow(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1    # Landroid/net/Uri;

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;->doLoadNow(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "Search.PackageImageLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load icon "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic loadNow(Landroid/net/Uri;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;->loadNow(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
