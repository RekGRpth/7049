.class public Lcom/google/android/searchcommon/imageloader/PackageImageLoader;
.super Ljava/lang/Object;
.source "PackageImageLoader.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/UriLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/UriLoader",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mContext:Landroid/content/Context;

.field private final mIconLoaderExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private mPackageContext:Landroid/content/Context;

.field private final mPackageName:Ljava/lang/String;

.field private final mSyncLoader:Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p4, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mIconLoaderExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    new-instance v0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;-><init>(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;Lcom/google/android/searchcommon/imageloader/PackageImageLoader$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mSyncLoader:Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/imageloader/PackageImageLoader;

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/res/AssetFileDescriptor;)V
    .locals 0
    .param p0    # Landroid/content/res/AssetFileDescriptor;

    invoke-static {p0}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->closeQuietly(Landroid/content/res/AssetFileDescriptor;)V

    return-void
.end method

.method public static canLoadSynchronously(Landroid/net/Uri;)Z
    .locals 4
    .param p0    # Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-static {p0}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->isNullDrawable(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return v2

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "android.resource"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method private static closeQuietly(Landroid/content/res/AssetFileDescriptor;)V
    .locals 1
    .param p0    # Landroid/content/res/AssetFileDescriptor;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private ensurePackageContext()Z
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageContext:Landroid/content/Context;

    if-nez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageName:Ljava/lang/String;

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageContext:Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "Search.PackageImageLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Application not found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isNullDrawable(Landroid/net/Uri;)Z
    .locals 2
    .param p0    # Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/searchcommon/util/Util;->asString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private maybeOverrideIcon(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/SearchConfig;->getSourceIconOverride(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/util/Util;->parseUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 4
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->ensurePackageContext()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/searchcommon/util/Now;->returnNull()Lcom/google/android/searchcommon/util/Now;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->maybeOverrideIcon(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object p1, v0

    :cond_1
    invoke-static {p1}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->canLoadSynchronously(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mSyncLoader:Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v1

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$1;

    iget-object v2, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mSyncLoader:Lcom/google/android/searchcommon/imageloader/PackageImageLoader$SyncronousImageLoader;

    iget-object v3, p0, Lcom/google/android/searchcommon/imageloader/PackageImageLoader;->mIconLoaderExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-direct {v1, p0, p1, v2, v3}, Lcom/google/android/searchcommon/imageloader/PackageImageLoader$1;-><init>(Lcom/google/android/searchcommon/imageloader/PackageImageLoader;Landroid/net/Uri;Lcom/google/android/searchcommon/util/SynchronousLoader;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    goto :goto_0
.end method
