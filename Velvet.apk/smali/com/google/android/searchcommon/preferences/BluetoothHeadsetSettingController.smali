.class public Lcom/google/android/searchcommon/preferences/BluetoothHeadsetSettingController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "BluetoothHeadsetSettingController.java"


# instance fields
.field private final mFilterPreference:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>()V

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/BluetoothHeadsetSettingController;->mFilterPreference:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/BluetoothHeadsetSettingController;->mFilterPreference:Z

    return v0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 0
    .param p1    # Landroid/preference/Preference;

    return-void
.end method
