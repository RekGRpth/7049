.class public Lcom/google/android/searchcommon/preferences/ClearShortcutsController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "ClearShortcutsController.java"


# instance fields
.field private mClearShortcutsPreference:Lcom/google/android/searchcommon/preferences/OkCancelPreference;

.field private final mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

.field private final mHandler:Landroid/os/Handler;

.field private final mWebHistory:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/clicklog/ClickLog;Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Lcom/google/android/searchcommon/clicklog/ClickLog;
    .param p3    # Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mHandler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mWebHistory:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/preferences/ClearShortcutsController;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/ClearShortcutsController;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->clearShortcuts()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/preferences/ClearShortcutsController;)Lcom/google/android/searchcommon/preferences/OkCancelPreference;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/ClearShortcutsController;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mClearShortcutsPreference:Lcom/google/android/searchcommon/preferences/OkCancelPreference;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/preferences/ClearShortcutsController;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/ClearShortcutsController;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/preferences/ClearShortcutsController;)Lcom/google/android/searchcommon/clicklog/ClickLog;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/ClearShortcutsController;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    return-object v0
.end method

.method private clearShortcuts()V
    .locals 2

    const-string v0, "Search.ClearShortcutsController"

    const-string v1, "Clearing shortcuts..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->broadcastSettingsChanged()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    invoke-interface {v0}, Lcom/google/android/searchcommon/clicklog/ClickLog;->clearHistory()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mWebHistory:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->deleteAllLocalHistory()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mClearShortcutsPreference:Lcom/google/android/searchcommon/preferences/OkCancelPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/OkCancelPreference;->setEnabled(Z)V

    return-void
.end method

.method private updateClearShortcutsPreference()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mWebHistory:Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/searchcommon/preferences/ClearShortcutsController$2;

    invoke-direct {v2, p0}, Lcom/google/android/searchcommon/preferences/ClearShortcutsController$2;-><init>(Lcom/google/android/searchcommon/preferences/ClearShortcutsController;)V

    invoke-static {v1, v2}, Lcom/google/android/searchcommon/util/Consumers;->createAsyncConsumer(Landroid/os/Handler;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/google/webhistoryrepository/WebHistoryRepository;->hasLocalHistory(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method


# virtual methods
.method public handlePreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    check-cast p1, Lcom/google/android/searchcommon/preferences/OkCancelPreference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mClearShortcutsPreference:Lcom/google/android/searchcommon/preferences/OkCancelPreference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->mClearShortcutsPreference:Lcom/google/android/searchcommon/preferences/OkCancelPreference;

    new-instance v1, Lcom/google/android/searchcommon/preferences/ClearShortcutsController$1;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/preferences/ClearShortcutsController$1;-><init>(Lcom/google/android/searchcommon/preferences/ClearShortcutsController;)V

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/OkCancelPreference;->setListener(Lcom/google/android/searchcommon/preferences/OkCancelPreference$Listener;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/ClearShortcutsController;->updateClearShortcutsPreference()V

    return-void
.end method
