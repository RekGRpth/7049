.class Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;
.super Ljava/lang/Object;
.source "DebugVoiceController.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/DebugVoiceController;->showEditExperiment(Landroid/preference/Preference;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/DebugVoiceController;

.field final synthetic val$editText:Landroid/widget/EditText;

.field final synthetic val$p:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/DebugVoiceController;Landroid/widget/EditText;Landroid/preference/Preference;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;->this$0:Lcom/google/android/searchcommon/preferences/DebugVoiceController;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;->val$editText:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;->val$p:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;->this$0:Lcom/google/android/searchcommon/preferences/DebugVoiceController;

    # getter for: Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;
    invoke-static {v1}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->access$000(Lcom/google/android/searchcommon/preferences/DebugVoiceController;)Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getOverrideConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->setId(Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;->this$0:Lcom/google/android/searchcommon/preferences/DebugVoiceController;

    # getter for: Lcom/google/android/searchcommon/preferences/DebugVoiceController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;
    invoke-static {v1}, Lcom/google/android/searchcommon/preferences/DebugVoiceController;->access$000(Lcom/google/android/searchcommon/preferences/DebugVoiceController;)Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/settings/Settings;->setOverrideConfiguration(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;->val$p:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/DebugVoiceController$1;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
