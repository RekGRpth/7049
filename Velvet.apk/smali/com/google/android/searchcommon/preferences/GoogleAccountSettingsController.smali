.class public Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "GoogleAccountSettingsController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final mAccountObserver:Landroid/database/DataSetObserver;

.field private final mActivity:Landroid/app/Activity;

.field protected mClearShortcutsPreference:Landroid/preference/Preference;

.field private final mCloudHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

.field protected mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

.field private mCloudWebHistorySetting:Ljava/lang/Boolean;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field protected mManageLocationHistoryPreference:Landroid/preference/Preference;

.field private mManageSearchHistoryHelper:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

.field protected mManageSearchHistoryPreference:Landroid/preference/Preference;

.field private mMapsDisabled:Z

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field protected mScreen:Landroid/preference/PreferenceScreen;

.field private final mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

.field protected mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/SearchConfig;Landroid/app/Activity;Lcom/google/android/searchcommon/google/SearchUrlHelper;Landroid/database/DataSetObservable;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/searchcommon/util/IntentUtils;Lcom/google/android/searchcommon/history/SearchHistoryHelper;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Landroid/app/Activity;
    .param p5    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p6    # Landroid/database/DataSetObservable;
    .param p7    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p8    # Lcom/google/android/searchcommon/util/IntentUtils;
    .param p9    # Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    new-instance v0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$1;-><init>(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mAccountObserver:Landroid/database/DataSetObserver;

    new-instance v0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$2;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$2;-><init>(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p4, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p5, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p6, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

    iput-object p7, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p8, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iput-object p9, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->updateUi()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudWebHistorySetting:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudWebHistorySetting:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;I)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->showToast(I)V

    return-void
.end method

.method private getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    return-object v0
.end method

.method private hasAccounts()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAllAccounts()[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private haveSelectedAccount()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshCloudHistoryPreference(Landroid/accounts/Account;)V
    .locals 2
    .param p1    # Landroid/accounts/Account;

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudWebHistorySetting:Ljava/lang/Boolean;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    const v1, 0x7f0d033d

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    new-instance v1, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$3;-><init>(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;Landroid/accounts/Account;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->getHistoryEnabled(Landroid/accounts/Account;Lcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_0
.end method

.method private setCloudSearchHistoryPreference(Landroid/preference/Preference;)V
    .locals 1
    .param p1    # Landroid/preference/Preference;

    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private setManageSearchHistoryPreference(Landroid/preference/Preference;)V
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method private setSelectAccountPreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    check-cast p1, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setNetworkClient(Lcom/google/android/apps/sidekick/inject/NetworkClient;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->updateAccounts()V

    return-void
.end method

.method private showToast(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private updateAccounts()V
    .locals 7

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAllAccountNames()[Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccountName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v3, v1

    add-int/lit8 v3, v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    array-length v3, v1

    invoke-static {v1, v6, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d034b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    move-object v1, v2

    :cond_0
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {v3, v1}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {v3, v1}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    array-length v3, v1

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setEnabled(Z)V

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {v3, v0}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setValue(Ljava/lang/String;)V

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->updateUi()V

    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {v3, v6}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateUi()V
    .locals 9

    const v3, 0x7f0d0347

    const v8, 0x7f0d0346

    const v7, 0x7f0d0343

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->hasAccounts()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    const v2, 0x7f0d034a

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setSummary(I)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v8}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v7}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->haveSelectedAccount()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccountName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d034c

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v8}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0348

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v7}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0344

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-boolean v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mMapsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d034e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->isPsuggestAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cloud_search_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->setCloudSearchHistoryPreference(Landroid/preference/Preference;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "manage_search_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->setManageSearchHistoryPreference(Landroid/preference/Preference;)V

    goto :goto_0

    :cond_2
    const-string v1, "manage_location_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->setManageLocationHistoryPreference(Landroid/preference/Preference;)V

    goto :goto_0

    :cond_3
    const-string v1, "google_account"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->setSelectAccountPreference(Landroid/preference/Preference;)V

    goto :goto_0
.end method

.method protected isPsuggestAvailable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreateComplete(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->onCreateComplete(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->isPsuggestAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mAccountObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->isPsuggestAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mAccountObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    if-ne p1, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSearchHistoryChangedObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d034b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d034d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    const-class v3, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->logOut()V

    :goto_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_1
    move v0, v1

    :cond_1
    :goto_2
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0, p2}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->hasAccount(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0, p2}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setAccountToUseByName(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    const-class v3, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "skip_to_end"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudSearchHistoryPreference:Landroid/preference/SwitchPreference;

    if-ne p1, v2, :cond_0

    check-cast p1, Landroid/preference/SwitchPreference;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudWebHistorySetting:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudWebHistorySetting:Ljava/lang/Boolean;

    invoke-virtual {v2, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const v3, 0x7f0d0340

    invoke-direct {p0, v3}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->showToast(I)V

    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mCloudHistoryHelper:Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    new-instance v4, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$4;

    invoke-direct {v4, p0, v2, p1}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController$4;-><init>(Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;ZLandroid/preference/SwitchPreference;)V

    invoke-virtual {v0, v3, v2, v4}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->setHistoryEnabled(Landroid/accounts/Account;ZLcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryHelper:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryHelper:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->start()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->updateSearchHistorySettingVisibility()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->isPsuggestAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->updateAccounts()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->refreshCloudHistoryPreference(Landroid/accounts/Account;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryHelper:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryHelper:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryHelper:Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    :cond_0
    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->onStop()V

    return-void
.end method

.method setManageLocationHistoryPreference(Landroid/preference/Preference;)V
    .locals 4
    .param p1    # Landroid/preference/Preference;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mIntentUtils:Lcom/google/android/searchcommon/util/IntentUtils;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v3}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/searchcommon/util/IntentUtils;->isIntentHandled(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mMapsDisabled:Z

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mMapsDisabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageLocationHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1
.end method

.method public setScreen(Landroid/preference/PreferenceScreen;)V
    .locals 2
    .param p1    # Landroid/preference/PreferenceScreen;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->setScreen(Landroid/preference/PreferenceScreen;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    const-string v1, "clear_shortcuts"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mClearShortcutsPreference:Landroid/preference/Preference;

    :cond_0
    return-void
.end method

.method protected updateSearchHistorySettingVisibility()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mSelectAccountPreference:Lcom/google/android/searchcommon/preferences/SelectAccountPreference;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/SelectAccountPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mClearShortcutsPreference:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mClearShortcutsPreference:Landroid/preference/Preference;

    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mClearShortcutsPreference:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mScreen:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/GoogleAccountSettingsController;->mManageSearchHistoryPreference:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method
