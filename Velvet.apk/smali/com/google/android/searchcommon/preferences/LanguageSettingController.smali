.class public Lcom/google/android/searchcommon/preferences/LanguageSettingController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "LanguageSettingController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/LanguageSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method

.method private updateSummary(Ljava/lang/String;Lcom/google/android/voicesearch/ui/LanguagePreference;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/LanguageSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/LanguageSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->isDefaultSpokenLanguage()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d03d3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public handlePreference(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    move-object v0, p1

    check-cast v0, Lcom/google/android/voicesearch/ui/LanguagePreference;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/LanguageSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/searchcommon/preferences/LanguageSettingController;->updateSummary(Ljava/lang/String;Lcom/google/android/voicesearch/ui/LanguagePreference;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    move-object v1, p1

    check-cast v1, Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/LanguageSettingController;->mVoiceSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {v2, v0}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->updateSpokenLanguage(Lcom/google/android/speech/SpeechSettings;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/preferences/LanguageSettingController;->updateSummary(Ljava/lang/String;Lcom/google/android/voicesearch/ui/LanguagePreference;)V

    const/4 v2, 0x0

    return v2
.end method
