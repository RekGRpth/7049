.class public Lcom/google/android/searchcommon/preferences/LocationSettingsController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "LocationSettingsController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field private final mSupportsGoogleLocationSettings:Z


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Landroid/app/Activity;Lcom/google/android/searchcommon/google/LocationSettings;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Landroid/app/Activity;
    .param p3    # Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->mActivity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/google/LocationSettings;->isGoogleSettingForAllApps()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->mSupportsGoogleLocationSettings:Z

    return-void
.end method


# virtual methods
.method public handlePreference(Landroid/preference/Preference;)V
    .locals 1
    .param p1    # Landroid/preference/Preference;

    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->mSupportsGoogleLocationSettings:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0d0345

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setTitle(I)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0d01df

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setTitle(I)V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;

    iget-boolean v1, p0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->mSupportsGoogleLocationSettings:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->getSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/google/LocationSettings;->getGoogleSettingIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/LocationSettingsController;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x0

    return v1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
