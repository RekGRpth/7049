.class public Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;
.super Landroid/app/ProgressDialog;
.source "ManageSearchHistoryHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field private mCancelled:Z

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p4    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    const/4 v5, 0x1

    invoke-direct {p0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p4, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccountName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0d0353

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->setTitle(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0354

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v5}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->setIndeterminate(Z)V

    invoke-virtual {p0, v5}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->setCancelable(Z)V

    invoke-virtual {p0, p0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0355

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$1;

    invoke-direct {v3, p0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$1;-><init>(Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;)V

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->cancelled()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mCancelled:Z

    return v0
.end method

.method private cancelled()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mCancelled:Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->dismiss()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->cancelled()V

    return-void
.end method

.method public start()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->show()V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getManageSearchHistoryUrlFormat()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->formatUrl(Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    const-string v2, "hist"

    new-instance v3, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper$2;-><init>(Lcom/google/android/searchcommon/preferences/ManageSearchHistoryHelper;Landroid/net/Uri;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getGaiaWebLoginLink(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method
