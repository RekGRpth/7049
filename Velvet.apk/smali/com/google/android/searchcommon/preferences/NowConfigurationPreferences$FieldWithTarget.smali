.class final Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;
.super Ljava/lang/Object;
.source "NowConfigurationPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FieldWithTarget"
.end annotation


# instance fields
.field final mField:Ljava/lang/reflect/Field;

.field final mTarget:Lcom/google/protobuf/micro/MessageMicro;


# direct methods
.method constructor <init>(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)V
    .locals 0
    .param p1    # Lcom/google/protobuf/micro/MessageMicro;
    .param p2    # Ljava/lang/reflect/Field;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mTarget:Lcom/google/protobuf/micro/MessageMicro;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    return-void
.end method


# virtual methods
.method get()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mTarget:Lcom/google/protobuf/micro/MessageMicro;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getFieldValue(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method has()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mTarget:Lcom/google/protobuf/micro/MessageMicro;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    invoke-static {v0, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->hasField(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Z

    move-result v0

    return v0
.end method

.method set(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mTarget:Lcom/google/protobuf/micro/MessageMicro;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->setFieldValue(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;Ljava/lang/Object;)V

    return-void
.end method
