.class public Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
.super Ljava/lang/Object;
.source "NowConfigurationPreferences.java"

# interfaces
.implements Landroid/content/SharedPreferences;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;,
        Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$NoBackingConfigurationException;,
        Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;,
        Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;
    }
.end annotation


# static fields
.field static final DIRTY_BIT_KEY:Ljava/lang/String; = "NowConfigurationPreferences.dirty"

.field private static final PROPERTY_PATH_SPLITTER:Lcom/google/common/base/Splitter;

.field public static final SIDEKICK_PREFIX:Ljava/lang/String; = "sidekick."

.field private static backingConfigurationCache:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

.field private static backingConfigurationCacheKey:Landroid/content/SharedPreferences;

.field private static final sBackingConfigurationCacheLock:Ljava/lang/Object;


# instance fields
.field private final mBackingConfigurationKey:Ljava/lang/String;

.field private final mBackingPreferences:Landroid/content/SharedPreferences;

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private final mPrimaryKeyFactory:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x2e

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->PROPERTY_PATH_SPLITTER:Lcom/google/common/base/Splitter;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->sBackingConfigurationCacheLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mListeners:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mPrimaryKeyFactory:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingConfigurationKey:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/util/Collection;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
    .param p1    # Ljava/util/Collection;
    .param p2    # Ljava/util/Set;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->notifyListeners(Ljava/util/Collection;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$400(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Z

    invoke-static {p0, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mPrimaryKeyFactory:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;
    .locals 1
    .param p0    # Lcom/google/protobuf/micro/MessageMicro;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->traverseTo(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingConfigurationKey:Ljava/lang/String;

    return-object v0
.end method

.method private static backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Z

    const-string v1, "sidekick."

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "sidekick."

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz p1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not begin with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sidekick."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getBackingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v3, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$NoBackingConfigurationException;

    invoke-direct {v3}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$NoBackingConfigurationException;-><init>()V

    throw v3

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_0
    :try_start_1
    invoke-static {v2, p1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->traverseTo(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v3, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;

    const/4 v5, 0x0

    invoke-direct {v3, p1, v5}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$BadPathException;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;)V

    throw v3

    :cond_1
    iget-object v3, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    invoke-static {v3}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->isRepeatedField(Ljava/lang/reflect/Field;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mTarget:Lcom/google/protobuf/micro/MessageMicro;

    iget-object v5, v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->mField:Ljava/lang/reflect/Field;

    invoke-static {v3, v5}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getRepeatedFieldCount(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)I

    move-result v3

    if-lez v3, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->get()Ljava/lang/Object;

    move-result-object p2

    monitor-exit v4

    :goto_1
    return-object p2

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;->has()Z

    move-result v1

    goto :goto_0

    :cond_4
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private notifyListeners(Ljava/util/Collection;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    :goto_1
    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    if-ne v5, v6, :cond_3

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sidekick."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, p0, v5}, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    move v5, v7

    goto :goto_0

    :cond_2
    move v6, v7

    goto :goto_1

    :cond_3
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v6, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;

    invoke-direct {v6, p0, p1, p2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;-><init>(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/util/Collection;Ljava/util/Set;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_4
    return-void
.end method

.method private static traverseTo(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;
    .locals 9

    const/4 v4, 0x0

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->PROPERTY_PATH_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v0, p1}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v4

    move-object v3, p0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v2, :cond_0

    :goto_1
    return-object v4

    :cond_0
    :try_start_0
    invoke-static {v3, v0}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getProtoField(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    const-class v0, Lcom/google/protobuf/micro/MessageMicro;

    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v3, v6}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->getFieldBuilder(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/micro/MessageMicro;

    move-object v8, v1

    move v1, v2

    move-object v2, v0

    move-object v0, v8

    :goto_2
    move-object v3, v2

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    new-instance v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;

    invoke-direct {v0, v3, v6}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$FieldWithTarget;-><init>(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/reflect/Field;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    goto :goto_2

    :cond_2
    move-object v4, v1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public clearBackingConfiguration()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingConfigurationKey:Ljava/lang/String;

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    sget-object v2, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->sBackingConfigurationCacheLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    :try_start_1
    sput-object v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationCache:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public edit()Landroid/content/SharedPreferences$Editor;
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;-><init>(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;)V

    return-object v0
.end method

.method public editConfiguration()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$ConfigurationEditor;-><init>(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences$1;)V

    return-object v0
.end method

.method public getAll()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getBackingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 7

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    sget-object v5, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->sBackingConfigurationCacheLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v3, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationCache:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationCacheKey:Landroid/content/SharedPreferences;

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    if-ne v3, v6, :cond_0

    sget-object v2, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationCache:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    return-object v2

    :cond_0
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingConfigurationKey:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3

    :catchall_1
    move-exception v3

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v3

    :cond_1
    sget-object v5, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->sBackingConfigurationCacheLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    new-instance v2, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;-><init>()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    invoke-static {v2, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->decodeFromString(Lcom/google/protobuf/micro/MessageMicro;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_8
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :goto_1
    :try_start_9
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    sput-object v3, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationCacheKey:Landroid/content/SharedPreferences;

    sput-object v2, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationCache:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/google/common/base/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_1

    :catchall_2
    move-exception v3

    monitor-exit v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public getFloat(Ljava/lang/String;F)F
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # F

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    :goto_0
    return-wide v1

    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0
.end method

.method public getMessage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    const/4 v4, 0x1

    invoke-static {p1, v4}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/micro/MessageMicro;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mPrimaryKeyFactory:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    invoke-interface {v4, v1}, Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;->primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMessages(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/micro/MessageMicro;",
            ">(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v1, p2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-static {p1, v5}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5, p1, p2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    :cond_0
    return-object v4

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v2, v5}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->doGetLocked(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/micro/MessageMicro;

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mPrimaryKeyFactory:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    invoke-interface {v5, v1}, Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;->primaryKeyFor(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public hasBackingConfiguration()Z
    .locals 2

    sget-object v1, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->sBackingConfigurationCacheLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationCache:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isDirty()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    const-string v1, "NowConfigurationPreferences.dirty"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setBackingConfiguration(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)V
    .locals 5
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->encodeAsString(Lcom/google/protobuf/micro/MessageMicro;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingConfigurationKey:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "NowConfigurationPreferences.dirty"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    sget-object v3, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->sBackingConfigurationCacheLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    :try_start_1
    sput-object v1, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->backingConfigurationCache:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method public unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mBackingPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
