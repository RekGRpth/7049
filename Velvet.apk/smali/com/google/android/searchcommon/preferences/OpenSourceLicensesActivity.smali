.class public Lcom/google/android/searchcommon/preferences/OpenSourceLicensesActivity;
.super Landroid/app/Activity;
.source "OpenSourceLicensesActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, -0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f040002

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/OpenSourceLicensesActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OpenSourceLicensesActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setLayout(II)V

    const v1, 0x7f10001d

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/OpenSourceLicensesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/html/licenses.html"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method
