.class public Lcom/google/android/searchcommon/preferences/OptInTask;
.super Landroid/os/AsyncTask;
.source "OptInTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private final mOptInHander:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;
    .param p3    # Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/OptInTask;->mOptInHander:Landroid/os/Handler;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/OptInTask;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    const/4 v5, 0x1

    :try_start_0
    new-instance v2, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    new-instance v3, Lcom/google/geo/sidekick/Sidekick$Configuration;

    invoke-direct {v3}, Lcom/google/geo/sidekick/Sidekick$Configuration;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/geo/sidekick/Sidekick$Configuration;->setOptInToSidekick(Z)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setConfigurationQuery(Lcom/google/geo/sidekick/Sidekick$Configuration;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/OptInTask;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-interface {v2, v1}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v2, "OptInTask"

    const-string v3, "Exception while attempting to authenticate"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/OptInTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptInTask;->mOptInHander:Landroid/os/Handler;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/OptInTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
