.class Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$1;
.super Ljava/lang/Object;
.source "OptOutDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$1;->this$0:Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$1;->this$0:Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    # getter for: Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->mTarget:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;
    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->access$000(Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;)Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->updatePredictiveCardsEnabledSwitch()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$1;->this$0:Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->dismiss()V

    return-void
.end method
