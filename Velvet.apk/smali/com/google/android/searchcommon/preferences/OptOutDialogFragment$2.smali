.class Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$2;
.super Ljava/lang/Object;
.source "OptOutDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

.field final synthetic val$turnOff:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;Landroid/widget/CheckBox;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$2;->val$turnOff:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$2;->val$turnOff:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    # invokes: Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->optOut(Z)V
    invoke-static {v0, v1}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->access$100(Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->dismiss()V

    return-void
.end method
