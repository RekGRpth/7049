.class public Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;
.super Ljava/lang/Object;
.source "OptOutSwitchHandler.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mHasSavedSwitchState:Z

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private final mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

.field private final mOptInIntent:Landroid/content/Intent;

.field private final mPredictiveCardsFragment:Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

.field private mSavedSwitchState:Z

.field private mSwitch:Landroid/widget/Switch;

.field private mSwitchState:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/Tag;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/MarinerOptInSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/content/Intent;Landroid/app/Activity;Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/MarinerOptInSettings;
    .param p2    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;
    .param p3    # Landroid/content/Intent;
    .param p4    # Landroid/app/Activity;
    .param p5    # Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mOptInIntent:Landroid/content/Intent;

    iput-object p4, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mActivity:Landroid/app/Activity;

    iput-object p5, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mPredictiveCardsFragment:Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

    return-void
.end method

.method static findOptOutHandler(Landroid/app/Fragment;)Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;
    .locals 3
    .param p0    # Landroid/app/Fragment;

    invoke-virtual {p0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/searchcommon/preferences/HasOptOutSwitchHandler;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/searchcommon/preferences/HasOptOutSwitchHandler;

    invoke-interface {v1}, Lcom/google/android/searchcommon/preferences/HasOptOutSwitchHandler;->getOptOutSwitchHandler()Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/searchcommon/preferences/HasOptOutSwitchHandler;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/searchcommon/preferences/HasOptOutSwitchHandler;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/HasOptOutSwitchHandler;->getOptOutSwitchHandler()Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    move-result-object v2

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
.end method


# virtual methods
.method public hasSwitch()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iput-boolean p2, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitchState:Z

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v3, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isAccountOptedIn(Landroid/accounts/Account;)Z

    move-result v1

    if-nez p2, :cond_1

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mPredictiveCardsFragment:Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

    invoke-static {v3}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->newInstance(Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;)Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "optout_dialog"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/searchcommon/preferences/OptOutDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mActivity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mOptInIntent:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mHasSavedSwitchState:Z

    if-eqz p1, :cond_0

    const-string v0, "mariner_enabled_switch_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mariner_enabled_switch_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSavedSwitchState:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mHasSavedSwitchState:Z

    :cond_0
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "mariner_enabled_switch_state"

    iget-boolean v1, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitchState:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setSwitch(Landroid/widget/Switch;)V
    .locals 2
    .param p1    # Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mHasSavedSwitchState:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    iget-boolean v1, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSavedSwitchState:Z

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSavedSwitchState:Z

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitchState:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mHasSavedSwitchState:Z

    :goto_1
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->updatePredictiveCardsEnabledSwitch()V

    goto :goto_1
.end method

.method startOptOutTask(Z)V
    .locals 6
    .param p1    # Z

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v4, "opt_out_progress"

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/preferences/OptOutSpinnerDialog;

    if-nez v2, :cond_2

    new-instance v2, Lcom/google/android/searchcommon/preferences/OptOutSpinnerDialog;

    invoke-direct {v2}, Lcom/google/android/searchcommon/preferences/OptOutSpinnerDialog;-><init>()V

    const-string v4, "opt_out_progress"

    invoke-virtual {v2, v1, v4}, Lcom/google/android/searchcommon/preferences/OptOutSpinnerDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_2
    const-string v4, "opt_out_worker_fragment"

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mPredictiveCardsFragment:Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;

    invoke-static {v0, p1, v2, v4}, Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;->newInstance(Landroid/accounts/Account;ZLandroid/app/DialogFragment;Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;)Lcom/google/android/searchcommon/preferences/OptOutWorkerFragment;

    move-result-object v3

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    const-string v5, "opt_out_worker_fragment"

    invoke-virtual {v4, v3, v5}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method updatePredictiveCardsEnabledSwitch()V
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v2, v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isAccountOptedIn(Landroid/accounts/Account;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iput-boolean v1, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitchState:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setEnabled(Z)V

    iput-boolean v3, p0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->mSwitchState:Z

    goto :goto_0
.end method
