.class public Lcom/google/android/searchcommon/preferences/PersonalizedSearchSettingsController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "PersonalizedSearchSettingsController.java"


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/google/gaia/LoginHelper;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/PersonalizedSearchSettingsController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/PersonalizedSearchSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    return-void
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PersonalizedSearchSettingsController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isPersonalizedSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PersonalizedSearchSettingsController;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlePreference(Landroid/preference/Preference;)V
    .locals 0
    .param p1    # Landroid/preference/Preference;

    return-void
.end method
