.class public Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;
.super Ljava/lang/Object;
.source "PredictiveCardsPreferences.java"


# static fields
.field static final CONFIGURATION_ACCOUNT_KEY:Ljava/lang/String; = "configuration_account"

.field private static final MASTER_WORKING_LOCK:Ljava/lang/Object;

.field public static final REPEATED_MESSAGE_INFO:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;

    invoke-direct {v0}, Lcom/google/android/apps/sidekick/sync/SidekickConfigurationRepeatedMessageInfo;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->REPEATED_MESSAGE_INFO:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->MASTER_WORKING_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/GsaPreferenceController;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    return-void
.end method

.method private copyMasterToWorkingFor(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;)V
    .locals 5
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Configuration;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getSidekickConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->setBackingConfiguration(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)V

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "configuration_account"

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "configuration_version"

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode(Landroid/content/Context;)I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Configuration;->hasLocaleConfiguration()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getLocaleConfiguration()Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;->hasBikingDirectionsEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "locale_configuration.biking_directions_enabled"

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getLocaleConfiguration()Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$LocaleConfiguration;->getBikingDirectionsEnabled()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private diffWorkingAgainstMasterConfigurationFor(Landroid/accounts/Account;)Lcom/google/android/apps/sidekick/sync/StateDifference;
    .locals 6
    .param p1    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Lcom/google/android/apps/sidekick/sync/StateDifference",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;",
            ">;"
        }
    .end annotation

    sget-object v3, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->MASTER_WORKING_LOCK:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->hasWorkingConfigurationFor(Landroid/accounts/Account;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v4, "Attemp to diff settings across accounts"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->copyMasterToWorkingFor(Landroid/accounts/Account;)V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getMasterConfigurationFor(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$Configuration;->getSidekickConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v4

    sget-object v5, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->REPEATED_MESSAGE_INFO:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/sidekick/sync/StateDifference;->newStateDifference(Lcom/google/protobuf/micro/MessageMicro;Lcom/google/protobuf/micro/MessageMicro;Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;)Lcom/google/android/apps/sidekick/sync/StateDifference;

    move-result-object v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method private getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->mPrefController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    return-object v0
.end method

.method private static getSavedConfigurationKey(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/accounts/Account;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "configuration_bytes_key_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static newNowConfigurationPreferences(Landroid/content/SharedPreferences;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
    .locals 3
    .param p0    # Landroid/content/SharedPreferences;

    new-instance v0, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    sget-object v1, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->REPEATED_MESSAGE_INFO:Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;

    const-string v2, "configuration_working"

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;-><init>(Lcom/google/android/apps/sidekick/sync/RepeatedMessageInfo;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public clearWorkingConfiguration()V
    .locals 4

    sget-object v2, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->MASTER_WORKING_LOCK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "configuration_account"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->clearBackingConfiguration()V

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public copyMasterToWorkingFor(Landroid/accounts/Account;)V
    .locals 5
    .param p1    # Landroid/accounts/Account;

    sget-object v2, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->MASTER_WORKING_LOCK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getMasterConfigurationFor(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not have a master configuration"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->copyMasterToWorkingFor(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;)V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public getMasterConfigurationFor(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;
    .locals 6
    .param p1    # Landroid/accounts/Account;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getSavedConfigurationKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getBytes(Ljava/lang/String;[B)[B

    move-result-object v1

    if-nez v1, :cond_0

    move-object v2, v3

    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/google/geo/sidekick/Sidekick$Configuration;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$Configuration;-><init>()V

    :try_start_0
    invoke-static {v2, v1}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->decodeFromByteArray(Lcom/google/protobuf/micro/MessageMicro;[B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getSavedConfigurationKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->remove(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    move-object v2, v3

    goto :goto_0
.end method

.method public getNextMeetingNotificationType()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getBackingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getNextMeeting()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$NextMeeting;->getNotificationSetting()I

    move-result v2

    goto :goto_0
.end method

.method public getSettingsChanges(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$StateChanges;
    .locals 7
    .param p1    # Landroid/accounts/Account;

    sget-object v6, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->MASTER_WORKING_LOCK:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->isDirty()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    monitor-exit v6

    :goto_0
    return-object v5

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->diffWorkingAgainstMasterConfigurationFor(Landroid/accounts/Account;)Lcom/google/android/apps/sidekick/sync/StateDifference;

    move-result-object v3

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v1, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;

    invoke-direct {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/sync/StateDifference;->compareForAdds()Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    invoke-static {v0}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->isEmpty(Lcom/google/protobuf/micro/MessageMicro;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v1, v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->setAdds(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/sync/StateDifference;->compareForDeletes()Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    invoke-static {v2}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->isEmpty(Lcom/google/protobuf/micro/MessageMicro;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v1, v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->setDeletes(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/sync/StateDifference;->compareForUpdates()Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    check-cast v4, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    invoke-static {v4}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->isEmpty(Lcom/google/protobuf/micro/MessageMicro;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v1, v4}, Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;->setUpdates(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;)Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;

    :cond_3
    new-instance v5, Lcom/google/geo/sidekick/Sidekick$StateChanges;

    invoke-direct {v5}, Lcom/google/geo/sidekick/Sidekick$StateChanges;-><init>()V

    invoke-virtual {v5, v1}, Lcom/google/geo/sidekick/Sidekick$StateChanges;->setSidekickConfigurationChanges(Lcom/google/geo/sidekick/Sidekick$SidekickConfigurationChanges;)Lcom/google/geo/sidekick/Sidekick$StateChanges;

    move-result-object v5

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method

.method public getTravelMode(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getBackingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getCommuteTravelMode()I

    move-result v2

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getOtherTravelMode()I

    move-result v2

    goto :goto_0
.end method

.method public getUnits()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getBackingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v0}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;->getUnits()I

    move-result v2

    goto :goto_0
.end method

.method public getWorkingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getWorkingPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;->getBackingConfiguration()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public getWorkingPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->newNowConfigurationPreferences(Landroid/content/SharedPreferences;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v0

    return-object v0
.end method

.method public hasWorkingConfigurationFor(Landroid/accounts/Account;)Z
    .locals 4
    .param p1    # Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    const-string v2, "configuration_account"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method

.method public isSavedConfigurationVersionCurrent()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode(Landroid/content/Context;)I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v2

    const-string v3, "configuration_version"

    invoke-interface {v2, v3, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public setMasterConfigurationFor(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;Z)V
    .locals 6
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$Configuration;
    .param p3    # Z

    sget-object v5, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->MASTER_WORKING_LOCK:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getMasterConfigurationFor(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v3

    invoke-static {p2}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->encodeToByteArray(Lcom/google/protobuf/micro/MessageMicro;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v3, :cond_0

    :try_start_1
    invoke-virtual {v3, v1}, Lcom/google/geo/sidekick/Sidekick$Configuration;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {v3}, Lcom/google/android/apps/sidekick/sync/MessageMicroUtil;->encodeToByteArray(Lcom/google/protobuf/micro/MessageMicro;)[B
    :try_end_1
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    move-object p2, v3

    :cond_0
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getPrefs()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->getSavedConfigurationKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->putBytes(Ljava/lang/String;[B)Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;->apply()V

    if-eqz p3, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->copyMasterToWorkingFor(Landroid/accounts/Account;Lcom/google/geo/sidekick/Sidekick$Configuration;)V

    :cond_1
    monitor-exit v5

    return-void

    :catch_0
    move-exception v2

    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4
.end method
