.class public Lcom/google/android/searchcommon/preferences/PreferencesUtil;
.super Ljava/lang/Object;
.source "PreferencesUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/preference/Preference;)Landroid/net/Uri;
    .locals 1
    .param p0    # Landroid/preference/Preference;

    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->getRingtoneUriFromPreference(Landroid/preference/Preference;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getRingtoneUriFromPreference(Landroid/preference/Preference;)Landroid/net/Uri;
    .locals 4
    .param p0    # Landroid/preference/Preference;

    invoke-virtual {p0}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private static getRollupSummary(Landroid/preference/Preference;)Ljava/lang/CharSequence;
    .locals 8
    .param p0    # Landroid/preference/Preference;

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/preference/Preference;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    instance-of v5, p0, Landroid/preference/PreferenceCategory;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    instance-of v5, p0, Landroid/preference/ListPreference;

    if-eqz v5, :cond_2

    move-object v0, p0

    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_0

    :cond_2
    instance-of v5, p0, Landroid/preference/PreferenceGroup;

    if-eqz v5, :cond_4

    move-object v5, p0

    check-cast v5, Landroid/preference/PreferenceGroup;

    invoke-static {v5}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceGroupSummary(Landroid/preference/PreferenceGroup;)V

    invoke-virtual {p0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v5

    const v7, 0x7f0d00d3

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move-object v2, v6

    :cond_3
    move-object v6, v2

    goto :goto_0

    :cond_4
    instance-of v5, p0, Landroid/preference/TwoStatePreference;

    if-eqz v5, :cond_7

    move-object v3, p0

    check-cast v3, Landroid/preference/TwoStatePreference;

    invoke-virtual {v3}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v3}, Landroid/preference/TwoStatePreference;->getSummaryOn()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v3}, Landroid/preference/TwoStatePreference;->getSummaryOn()Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_0

    :cond_5
    invoke-virtual {v3}, Landroid/preference/TwoStatePreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_0

    :cond_6
    invoke-virtual {v3}, Landroid/preference/TwoStatePreference;->getSummaryOff()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3}, Landroid/preference/TwoStatePreference;->getSummaryOff()Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_0

    :cond_7
    instance-of v5, p0, Landroid/preference/RingtonePreference;

    if-eqz v5, :cond_0

    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->getRingtoneUriFromPreference(Landroid/preference/Preference;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p0, v4}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updateRingtoneSummary(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private static updatePreferenceGroupSummary(Landroid/preference/PreferenceGroup;)V
    .locals 5
    .param p0    # Landroid/preference/PreferenceGroup;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceSummary(Landroid/preference/Preference;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v4

    if-ge v2, v4, :cond_3

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->getRollupSummary(Landroid/preference/Preference;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_4

    const-string v4, ", "

    invoke-static {v4}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceGroup;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    const v4, 0x7f0d00d3

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceGroup;->setSummary(I)V

    goto :goto_0
.end method

.method public static updatePreferenceSummaries(Landroid/preference/Preference;)V
    .locals 1
    .param p0    # Landroid/preference/Preference;

    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceValue(Landroid/preference/Preference;)V

    instance-of v0, p0, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/preference/PreferenceGroup;

    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceGroupSummary(Landroid/preference/PreferenceGroup;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceSummary(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private static updatePreferenceSummary(Landroid/preference/Preference;)Z
    .locals 9
    .param p0    # Landroid/preference/Preference;

    const/4 v5, 0x1

    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    :try_start_0
    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    const v2, 0x7f0d00d2

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p0}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "configured_summary"

    invoke-virtual {p0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v2}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    const/4 v5, 0x0

    :cond_1
    :goto_0
    return v5

    :cond_2
    invoke-virtual {p0}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "configured_summary"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method private static updatePreferenceValue(Landroid/preference/Preference;)V
    .locals 13
    .param p0    # Landroid/preference/Preference;

    const/4 v11, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    instance-of v12, p0, Landroid/preference/PreferenceGroup;

    if-eqz v12, :cond_1

    check-cast p0, Landroid/preference/PreferenceGroup;

    invoke-static {p0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceValueGroup(Landroid/preference/PreferenceGroup;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v12, p0, Lcom/google/android/searchcommon/preferences/NotificationGroupPreference;

    if-eqz v12, :cond_3

    move-object v5, p0

    check-cast v5, Lcom/google/android/searchcommon/preferences/NotificationGroupPreference;

    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v6, v12, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v12

    if-ne v12, v0, :cond_2

    :goto_1
    invoke-virtual {v5, v0}, Lcom/google/android/searchcommon/preferences/NotificationGroupPreference;->setChecked(Z)V

    goto :goto_0

    :cond_2
    move v0, v11

    goto :goto_1

    :cond_3
    instance-of v12, p0, Landroid/preference/TwoStatePreference;

    if-eqz v12, :cond_4

    move-object v8, p0

    check-cast v8, Landroid/preference/TwoStatePreference;

    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v6, v12, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    invoke-virtual {v8, v10}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_4
    instance-of v11, p0, Landroid/preference/ListPreference;

    if-eqz v11, :cond_0

    move-object v4, p0

    check-cast v4, Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-interface {v6, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    const/4 v3, 0x0

    :goto_2
    array-length v11, v1

    if-ge v3, v11, :cond_0

    aget-object v11, v2, v3

    invoke-virtual {v11, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    aget-object v7, v1, v3

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private static updatePreferenceValueGroup(Landroid/preference/PreferenceGroup;)V
    .locals 3
    .param p0    # Landroid/preference/PreferenceGroup;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceValue(Landroid/preference/Preference;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static updateRingtoneSummary(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/preference/Preference;
    .param p2    # Landroid/net/Uri;

    new-instance v0, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/PreferencesUtil$UpdateRingtoneTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
