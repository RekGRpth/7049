.class public Lcom/google/android/searchcommon/preferences/SearchableItemsFragment;
.super Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;
.source "SearchableItemsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SettingsFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getPreferencesResourceId()I
    .locals 1

    const v0, 0x7f070015

    return v0
.end method

.method protected handlePreferenceGroup(Landroid/preference/PreferenceGroup;)V
    .locals 1
    .param p1    # Landroid/preference/PreferenceGroup;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SearchableItemsFragment;->getController()Lcom/google/android/searchcommon/preferences/PreferenceController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/preferences/PreferenceController;->handlePreference(Landroid/preference/Preference;)V

    return-void
.end method
