.class public abstract Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.super Ljava/lang/Object;
.source "SettingsControllerBase.java"

# interfaces
.implements Lcom/google/android/searchcommon/preferences/PreferenceController;


# instance fields
.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-void
.end method


# virtual methods
.method public filterPreference(Landroid/preference/Preference;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;

    const/4 v0, 0x0

    return v0
.end method

.method protected getSettings()Lcom/google/android/searchcommon/SearchSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateComplete(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public onStop()V
    .locals 0

    return-void
.end method

.method public setScreen(Landroid/preference/PreferenceScreen;)V
    .locals 0
    .param p1    # Landroid/preference/PreferenceScreen;

    return-void
.end method
