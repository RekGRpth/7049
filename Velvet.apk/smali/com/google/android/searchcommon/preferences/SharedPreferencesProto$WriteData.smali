.class Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
.super Ljava/lang/Object;
.source "SharedPreferencesProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WriteData"
.end annotation


# instance fields
.field public final mWaitLatch:Ljava/util/concurrent/CountDownLatch;

.field public mWriteResult:Z

.field public mWrittenMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->this$0:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWaitLatch:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;
    .param p2    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;-><init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)V

    return-void
.end method
