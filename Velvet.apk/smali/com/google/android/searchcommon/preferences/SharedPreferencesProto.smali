.class public Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;
.super Ljava/lang/Object;
.source "SharedPreferencesProto.java"

# interfaces
.implements Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;,
        Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    }
.end annotation


# instance fields
.field private final mBackupFile:Ljava/io/File;

.field private final mFile:Ljava/io/File;

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLoaded:Z

.field private final mLock:Ljava/lang/Object;

.field private mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoveMarker:Ljava/lang/Object;

.field private mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

.field private mWriteDelayCounter:I


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 3
    .param p1    # Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mRemoveMarker:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".bak"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mBackupFile:Ljava/io/File;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mListeners:Ljava/util/Set;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    new-instance v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;-><init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    new-instance v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$1;

    const-string v1, "Search.SharedPreferencesProto"

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$1;-><init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$1;->start()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->loadFromFile()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->writeToFile()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Ljava/util/Collection;Ljava/util/Set;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;
    .param p1    # Ljava/util/Collection;
    .param p2    # Ljava/util/Set;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->notifyListeners(Ljava/util/Collection;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mRemoveMarker:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Ljava/util/Map;)Ljava/util/Map;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;
    .param p1    # Ljava/util/Map;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;)Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;
    .param p1    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)I
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;

    iget v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteDelayCounter:I

    return v0
.end method

.method private commitLoadedMapLocked(Ljava/util/Map;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    iget-boolean v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    iget-boolean v5, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    if-nez v5, :cond_5

    if-eqz p1, :cond_6

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mRemoveMarker:Ljava/lang/Object;

    if-ne v4, v5, :cond_1

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {p1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-nez v4, :cond_2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_3

    :cond_2
    if-eqz v4, :cond_0

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_3
    invoke-interface {p1, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    goto :goto_0

    :cond_4
    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    :goto_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    :cond_5
    return v3

    :cond_6
    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mRemoveMarker:Ljava/lang/Object;

    invoke-static {v6}, Lcom/google/common/base/Predicates;->equalTo(Ljava/lang/Object;)Lcom/google/common/base/Predicate;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/common/collect/Maps;->filterValues(Ljava/util/Map;Lcom/google/common/base/Predicate;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    const/4 v3, 0x1

    goto :goto_1
.end method

.method private static dataToMap(Lcom/google/android/searchcommon/preferences/SharedPreferencesData;)Ljava/util/Map;
    .locals 12
    .param p0    # Lcom/google/android/searchcommon/preferences/SharedPreferencesData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/preferences/SharedPreferencesData;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/4 v8, 0x0

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData;->getEntryList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasKey()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getKey()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBoolValue()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getBoolValue()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    move-object v4, v8

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasFloatValue()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getFloatValue()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v6, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasIntValue()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getIntValue()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasLongValue()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getLongValue()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasStringValue()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringSetValueCount()I

    move-result v9

    if-eqz v9, :cond_9

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringSetValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_6

    const-string v9, "null"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringSetValueCount()I

    move-result v0

    const/4 v2, 0x1

    :goto_2
    if-eq v2, v0, :cond_8

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getStringSetValue(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    const-string v9, "Search.SharedPreferencesProto"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "dataToMap: invalid nullTag: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "->"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->hasBytesValue()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->getBytesValue()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v9

    invoke-virtual {v6, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v6, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_b
    return-object v6
.end method

.method private doGet(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    monitor-exit v2

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mRemoveMarker:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    move-object v1, v0

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->waitForLoadLocked()V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private doLoadPreferenceMap()Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mBackupFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mBackupFile:Ljava/io/File;

    iget-object v9, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    invoke-virtual {v8, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "Search.SharedPreferencesProto"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to rename backup file to "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v7

    :cond_0
    const/4 v3, 0x0

    :try_start_0
    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    invoke-virtual {p0, v8}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->createFileInputStream(Ljava/io/File;)Ljava/io/FileInputStream;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileInputStream;->available()I

    move-result v0

    const/4 v4, 0x0

    add-int/lit8 v8, v0, 0x1

    const/16 v9, 0x800

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    new-array v6, v8, [B

    invoke-virtual {v3, v6}, Ljava/io/FileInputStream;->read([B)I

    move-result v1

    :goto_1
    if-ltz v1, :cond_2

    add-int/2addr v4, v1

    array-length v8, v6

    if-ne v4, v8, :cond_1

    array-length v8, v6

    mul-int/lit8 v8, v8, 0x2

    invoke-static {v6, v8}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v6

    :cond_1
    array-length v8, v6

    sub-int/2addr v8, v4

    invoke-virtual {v3, v6, v4, v8}, Ljava/io/FileInputStream;->read([BII)I

    move-result v1

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/google/android/searchcommon/preferences/SharedPreferencesData;

    invoke-direct {v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData;-><init>()V

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v8, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData;->mergeFrom([BII)Lcom/google/protobuf/micro/MessageMicro;

    invoke-static {v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->dataToMap(Lcom/google/android/searchcommon/preferences/SharedPreferencesData;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_1
    const-string v7, "Search.SharedPreferencesProto"

    const-string v8, "load shared preferences: file not found"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_2
    const-string v8, "Search.SharedPreferencesProto"

    const-string v9, "load shared preferences"

    invoke-static {v8, v9, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_2
    move-exception v2

    :try_start_3
    const-string v8, "Search.SharedPreferencesProto"

    const-string v9, "load shared preferences"

    invoke-static {v8, v9, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v7

    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v7
.end method

.method private doWritePreferenceMap(Ljava/util/Map;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mBackupFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    :cond_0
    invoke-static {p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mapToData(Ljava/util/Map;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData;->toByteArray()[B

    move-result-object v4

    const/4 v1, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    invoke-virtual {p0, v6}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->createFileOutputStream(Ljava/io/File;)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/FileDescriptor;->sync()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mBackupFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v5, 0x1

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_0
    return v5

    :cond_1
    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mBackupFile:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "Search.SharedPreferencesProto"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to rename to backup file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mBackupFile:Ljava/io/File;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "Search.SharedPreferencesProto"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to create shared preferences directory "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v6, "Search.SharedPreferencesProto"

    const-string v7, "exception while writing to file: "

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v6, "Search.SharedPreferencesProto"

    const-string v7, "exception while writing to file: "

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v5
.end method

.method private loadFromFile()V
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :goto_0
    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->commitLoadedMapLocked(Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWriteResult:Z

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    iget-object v3, v3, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWaitLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    :cond_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->writeToFile()V

    :cond_1
    return-void

    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    :cond_2
    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doLoadPreferenceMap()Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3
.end method

.method private static mapToData(Ljava/util/Map;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/android/searchcommon/preferences/SharedPreferencesData;"
        }
    .end annotation

    new-instance v5, Lcom/google/android/searchcommon/preferences/SharedPreferencesData;

    invoke-direct {v5}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData;-><init>()V

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    new-instance v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    invoke-direct {v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-virtual {v0, v4}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setKey(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    :cond_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_2

    :cond_1
    :goto_1
    invoke-virtual {v5, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData;->addEntry(Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData;

    goto :goto_0

    :cond_2
    instance-of v9, v8, Ljava/lang/Boolean;

    if-eqz v9, :cond_3

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    invoke-virtual {v0, v9}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setBoolValue(Z)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_1

    :cond_3
    instance-of v9, v8, Ljava/lang/Float;

    if-eqz v9, :cond_4

    check-cast v8, Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-virtual {v0, v9}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setFloatValue(F)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_1

    :cond_4
    instance-of v9, v8, Ljava/lang/Integer;

    if-eqz v9, :cond_5

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v0, v9}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setIntValue(I)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_1

    :cond_5
    instance-of v9, v8, Ljava/lang/Long;

    if-eqz v9, :cond_6

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v0, v9, v10}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setLongValue(J)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_1

    :cond_6
    instance-of v9, v8, Ljava/lang/String;

    if-eqz v9, :cond_7

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setStringValue(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_1

    :cond_7
    instance-of v9, v8, Ljava/util/Set;

    if-eqz v9, :cond_a

    move-object v7, v8

    check-cast v7, Ljava/util/Set;

    const/4 v9, 0x0

    invoke-interface {v7, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    const-string v9, "null"

    :goto_2
    invoke-virtual {v0, v9}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->addStringSetValue(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-eqz v6, :cond_8

    invoke-virtual {v0, v6}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->addStringSetValue(Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_3

    :cond_9
    const-string v9, ""

    goto :goto_2

    :cond_a
    instance-of v9, v8, Lcom/google/protobuf/micro/ByteStringMicro;

    if-eqz v9, :cond_b

    check-cast v8, Lcom/google/protobuf/micro/ByteStringMicro;

    invoke-virtual {v0, v8}, Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;->setBytesValue(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/searchcommon/preferences/SharedPreferencesData$SharedPreferenceEntry;

    goto :goto_1

    :cond_b
    const-string v9, "Search.SharedPreferencesProto"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mapToData: invalid entry class = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_c
    return-object v5
.end method

.method private notifyListeners(Ljava/util/Collection;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    :goto_1
    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    if-ne v5, v6, :cond_3

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v3, p0, v2}, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    move v5, v7

    goto :goto_0

    :cond_2
    move v6, v7

    goto :goto_1

    :cond_3
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v6, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$2;

    invoke-direct {v6, p0, p1, p2}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$2;-><init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;Ljava/util/Collection;Ljava/util/Set;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_4
    return-void
.end method

.method private waitForLoadLocked()V
    .locals 3

    iget-boolean v2, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v1, 0x0

    :goto_1
    iget-boolean v2, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    if-nez v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    :cond_2
    return-void
.end method

.method private writeToFile()V
    .locals 7

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v6, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v6

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v3, v2, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWrittenMap:Ljava/util/Map;

    if-ne v3, v0, :cond_0

    move v3, v4

    :goto_1
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWrittenMap:Ljava/util/Map;

    iput-boolean v1, v2, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWriteResult:Z

    iget-object v3, v2, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWaitLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    if-ne v2, v3, :cond_1

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    monitor-exit v6

    return-void

    :cond_0
    move v3, v5

    goto :goto_1

    :cond_1
    :goto_2
    iget v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteDelayCounter:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_2

    :try_start_1
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v3

    goto :goto_2

    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    iget-object v3, v3, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWrittenMap:Ljava/util/Map;

    if-nez v3, :cond_3

    move v3, v4

    :goto_3
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteData:Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;

    iput-object v0, v3, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$WriteData;->mWrittenMap:Ljava/util/Map;

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doWritePreferenceMap(Ljava/util/Map;)Z

    move-result v1

    goto :goto_0

    :cond_3
    move v3, v5

    goto :goto_3

    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3
.end method


# virtual methods
.method public allowWrites()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteDelayCounter:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteDelayCounter:I

    iget v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteDelayCounter:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doGet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected createFileInputStream(Ljava/io/File;)Ljava/io/FileInputStream;
    .locals 1
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method protected createFileOutputStream(Ljava/io/File;)Ljava/io/FileOutputStream;
    .locals 1
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public delayWrites()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteDelayCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mWriteDelayCounter:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bridge synthetic edit()Landroid/content/SharedPreferences$Editor;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;

    move-result-object v0

    return-object v0
.end method

.method public edit()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt$Editor;
    .locals 1

    new-instance v0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto$EditorImpl;-><init>(Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;)V

    return-object v0
.end method

.method public getAll()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLoaded:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->waitForLoadLocked()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mMap:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableMap;->copyOf(Ljava/util/Map;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doGet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    return p2
.end method

.method public getBytes(Ljava/lang/String;[B)[B
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doGet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/google/protobuf/micro/ByteStringMicro;

    invoke-virtual {v0}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method public getFloat(Ljava/lang/String;F)F
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # F

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doGet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p2

    :cond_0
    return p2
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doGet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :cond_0
    return p2
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doGet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doGet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->doGet(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/util/Set;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/SharedPreferencesProto;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
