.class public Lcom/google/android/searchcommon/preferences/TtsModeSettingController;
.super Lcom/google/android/searchcommon/preferences/SettingsControllerBase;
.source "TtsModeSettingController.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mPreference:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/preferences/SettingsControllerBase;-><init>(Lcom/google/android/searchcommon/SearchSettings;)V

    return-void
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateTtsModeSummary(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "handsFreeOnly"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0d03da

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const-string v1, "informativeOnly"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0d03d9

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v1, 0x7f0d03d8

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public handlePreference(Landroid/preference/Preference;)V
    .locals 1
    .param p1    # Landroid/preference/Preference;

    check-cast p1, Landroid/preference/ListPreference;

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->mPreference:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->mPreference:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->updateTtsModeSummary(Ljava/lang/String;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/preferences/TtsModeSettingController;->updateTtsModeSummary(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method
