.class public abstract Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;
.source "AbstractMyStuffSettingsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;-><init>()V

    return-void
.end method

.method public static addOrFixHelpMenuItem(Landroid/content/Context;Landroid/view/Menu;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/Menu;

    const v1, 0x7f1002ae

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/velvet/Help;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/Help;-><init>(Landroid/content/Context;)V

    const-string v2, "profile"

    invoke-virtual {v1, p1, v2}, Lcom/google/android/velvet/Help;->addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/velvet/Help;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/Help;-><init>(Landroid/content/Context;)V

    const-string v2, "profile"

    invoke-virtual {v1, p1, v2}, Lcom/google/android/velvet/Help;->setHelpMenuItemIntent(Landroid/view/Menu;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->addOrFixHelpMenuItem(Landroid/content/Context;Landroid/view/Menu;)V

    return-void
.end method
