.class Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;
.super Ljava/lang/Object;
.source "CardSettingsFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->addCardEnablementSwitch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;

.field final synthetic val$preferencesKey:Ljava/lang/String;

.field final synthetic val$sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;->val$sharedPreferences:Landroid/content/SharedPreferences;

    iput-object p3, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;->val$preferencesKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;->val$sharedPreferences:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;->val$preferencesKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p2}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->persistOverallPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;->this$0:Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceSummaries(Landroid/preference/Preference;)V

    return-void
.end method
