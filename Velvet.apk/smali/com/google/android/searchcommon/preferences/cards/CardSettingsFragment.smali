.class public abstract Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "CardSettingsFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mSharedPreferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

.field private mUpdateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private removeCardEnablementSwitch()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iput-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    :cond_0
    return-void
.end method


# virtual methods
.method protected addCardEnablementSwitch(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v4, v0, Landroid/preference/PreferenceActivity;

    if-eqz v4, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->createAndAddEnablementSwitch(Landroid/app/Activity;)Landroid/widget/Switch;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-virtual {p0, v3, p1}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->isOverallPreferenceEnabled(Landroid/content/SharedPreferences;Ljava/lang/String;)Z

    move-result v2

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v4, v2}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v4, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v5, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;

    invoke-direct {v5, p0, v3, p1}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$2;-><init>(Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->setHasOptionsMenu(Z)V

    :cond_0
    return-void
.end method

.method protected createAndAddEnablementSwitch(Landroid/app/Activity;)Landroid/widget/Switch;
    .locals 7
    .param p1    # Landroid/app/Activity;

    const/16 v6, 0x10

    const/4 v5, -0x2

    const/4 v4, 0x0

    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0028

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v4, v4, v1, v4}, Landroid/widget/Switch;->setPadding(IIII)V

    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x15

    invoke-direct {v3, v5, v5, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v0, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    return-object v0
.end method

.method protected getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mSharedPreferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    return-object v0
.end method

.method protected abstract getPreferenceResourceId()I
.end method

.method protected isOverallPreferenceEnabled(Landroid/content/SharedPreferences;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    const-string v2, "sidekick"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    iput-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mSharedPreferences:Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceResourceId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mUpdateHandler:Landroid/os/Handler;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->removeCardEnablementSwitch()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidateWithDelayedRefresh()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->addCardEnablementSwitch(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->updatePreferenceEnablement()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/searchcommon/preferences/PreferencesUtil;->updatePreferenceSummaries(Landroid/preference/Preference;)V

    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->mUpdateHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment$1;-><init>(Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->updatePreferenceEnablement()V

    return-void
.end method

.method protected persistOverallPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method protected updatePreferenceEnablement()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getKey()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-virtual {p0, v4, v2}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->isOverallPreferenceEnabled(Landroid/content/SharedPreferences;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v4

    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
