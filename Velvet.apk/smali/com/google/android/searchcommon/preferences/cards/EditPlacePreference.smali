.class public Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;
.super Landroid/preference/DialogPreference;
.source "EditPlacePreference.java"


# instance fields
.field private mAddress:Ljava/lang/String;

.field private mAddressEditText:Landroid/widget/EditText;

.field private mName:Ljava/lang/String;

.field private mNameEditText:Landroid/widget/EditText;

.field private mNameEditable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->init()V

    return-void
.end method

.method private init()V
    .locals 1

    const v0, 0x7f040030

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setDialogLayoutResource(I)V

    const v0, 0x104000a

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setPositiveButtonText(I)V

    const/high16 v0, 0x1040000

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setNegativeButtonText(I)V

    const v0, 0x7f0d0210

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setSummary(I)V

    return-void
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const v1, 0x7f1000a3

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mNameEditable:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const v0, 0x7f1000a4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mAddressEditText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mAddressEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mNameEditText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onDialogClosed(Z)V
    .locals 4
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    if-eqz p1, :cond_4

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mNameEditText:Landroid/widget/EditText;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/geo/sidekick/Sidekick$Location;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$Location;-><init>()V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2, v0}, Lcom/google/geo/sidekick/Sidekick$Location;->setAddress(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v2, v1}, Lcom/google/geo/sidekick/Sidekick$Location;->setName(Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Location;

    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v1, :cond_3

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setName(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setAddress(Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mAddress:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mName:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setNameEditable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->mNameEditable:Z

    return-void
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/EditPlacePreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    return-void
.end method
