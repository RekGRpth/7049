.class public Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;
.source "GmailFlightsCardSettingsFragment.java"


# static fields
.field private static final minutesBefore:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->minutesBefore:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1e
        0x3c
        0x5a
        0x78
        0x96
        0xb4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;IIIIZ)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    invoke-direct/range {p0 .. p5}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getTimeMessage(IIIIZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getArriveMinutesBeforeEntries()[Ljava/lang/CharSequence;
    .locals 8

    sget-object v0, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->minutesBefore:[I

    array-length v0, v0

    new-array v7, v0, [Ljava/lang/CharSequence;

    const/4 v6, 0x0

    :goto_0
    array-length v0, v7

    if-ge v6, v0, :cond_0

    const v1, 0x7f0d00ef

    const v2, 0x7f0d00f1

    const v3, 0x7f0d00f2

    sget-object v0, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->minutesBefore:[I

    aget v4, v0, v6

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getTimeMessage(IIIIZ)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    return-object v7
.end method

.method private getArriveMinutesBeforeValues()[Ljava/lang/CharSequence;
    .locals 3

    sget-object v2, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->minutesBefore:[I

    array-length v2, v2

    new-array v1, v2, [Ljava/lang/CharSequence;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->minutesBefore:[I

    aget v2, v2, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private getTimeMessage(IIIIZ)Ljava/lang/String;
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    const/16 v1, 0x3c

    const/4 v7, 0x0

    const/4 v3, 0x1

    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    if-ge p4, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, p1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    if-ne p4, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, p2, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    rem-int/lit8 v1, p4, 0x3c

    if-nez v1, :cond_3

    if-eqz p5, :cond_2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    div-int/lit8 v3, p4, 0x3c

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, p3, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    div-int/lit8 v3, p4, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, p3, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    if-eqz p5, :cond_4

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    int-to-double v3, p4

    const-wide/high16 v5, 0x404e000000000000L

    div-double/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, p3, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, p1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private initArriveByListPreference(I)Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getArriveMinutesBeforeEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->getArriveMinutesBeforeValues()[Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    return-object v1
.end method


# virtual methods
.method protected getPreferenceResourceId()I
    .locals 1

    const v0, 0x7f070004

    return v0
.end method

.method public onResume()V
    .locals 4

    const v3, 0x7f0d00a9

    invoke-direct {p0, v3}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->initArriveByListPreference(I)Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;

    move-result-object v0

    new-instance v2, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment$1;-><init>(Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;->setSummaryCalculator(Lcom/google/common/base/Function;)V

    const-string v3, "60"

    invoke-virtual {v0, v3}, Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;->setDefaultValue(Ljava/lang/Object;)V

    const v3, 0x7f0d00aa

    invoke-direct {p0, v3}, Lcom/google/android/searchcommon/preferences/cards/GmailFlightsCardSettingsFragment;->initArriveByListPreference(I)Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;->setSummaryCalculator(Lcom/google/common/base/Function;)V

    const-string v3, "120"

    invoke-virtual {v1, v3}, Lcom/google/android/searchcommon/preferences/CustomSummaryListPreference;->setDefaultValue(Ljava/lang/Object;)V

    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->onResume()V

    return-void
.end method
