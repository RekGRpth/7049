.class Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;
.super Landroid/os/AsyncTask;
.source "MySportsSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FetchSportsEntities"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/geo/sidekick/Sidekick$SportsTeams;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/geo/sidekick/Sidekick$SportsTeams;
    .locals 10
    .param p1    # [Ljava/lang/Void;

    new-instance v0, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    invoke-direct {v0}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;-><init>()V

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;->addStaticEntities(I)Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;

    new-instance v7, Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    invoke-direct {v7}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;-><init>()V

    invoke-virtual {v7, v0}, Lcom/google/geo/sidekick/Sidekick$RequestPayload;->setFetchStaticEntitiesQuery(Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesQuery;)Lcom/google/geo/sidekick/Sidekick$RequestPayload;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;
    invoke-static {v7}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$700(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v7

    invoke-interface {v7, v3}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->sendRequestWithoutLocation(Lcom/google/geo/sidekick/Sidekick$RequestPayload;)Lcom/google/geo/sidekick/Sidekick$ResponsePayload;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->hasFetchStaticEntitiesResponse()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v4}, Lcom/google/geo/sidekick/Sidekick$ResponsePayload;->getFetchStaticEntitiesResponse()Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;->hasSportsEntities()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v7}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$100(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    new-instance v5, Lcom/google/android/apps/sidekick/StaticEntitiesData;

    invoke-direct {v5}, Lcom/google/android/apps/sidekick/StaticEntitiesData;-><init>()V

    invoke-virtual {v5, v1, v2}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->setLastRefreshMillis(J)Lcom/google/android/apps/sidekick/StaticEntitiesData;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->setStaticEntities(Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;)Lcom/google/android/apps/sidekick/StaticEntitiesData;

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mAsyncFileStorage:Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;
    invoke-static {v7}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$800(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;)Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;

    move-result-object v7

    const-string v8, "static_entities"

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/StaticEntitiesData;->toByteArray()[B

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/google/android/apps/sidekick/inject/AsyncFileStorage;->writeToFile(Ljava/lang/String;[B)V

    invoke-virtual {v6}, Lcom/google/geo/sidekick/Sidekick$FetchStaticEntitiesResponse;->getSportsEntities()Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    move-result-object v7

    :goto_0
    return-object v7

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->doInBackground([Ljava/lang/Void;)Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/geo/sidekick/Sidekick$SportsTeams;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    const-string v2, "add_team_preference_key"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    # setter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->mSportsEntities:Lcom/google/geo/sidekick/Sidekick$SportsTeams;
    invoke-static {v1, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$002(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;Lcom/google/geo/sidekick/Sidekick$SportsTeams;)Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->this$0:Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->updateSportsEntities()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$SportsTeams;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$FetchSportsEntities;->onPostExecute(Lcom/google/geo/sidekick/Sidekick$SportsTeams;)V

    return-void
.end method
