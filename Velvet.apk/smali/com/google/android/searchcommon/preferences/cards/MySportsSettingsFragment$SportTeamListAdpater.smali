.class final Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;
.super Landroid/widget/ArrayAdapter;
.source "MySportsSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SportTeamListAdpater"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;",
        ">;"
    }
.end annotation


# instance fields
.field private mFilter:Ljava/lang/String;

.field private final mMatchStyle:Landroid/text/style/CharacterStyle;

.field private final mPostMatchStyle:Landroid/text/style/CharacterStyle;

.field private final mPreMatchStyle:Landroid/text/style/CharacterStyle;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;",
            ">;)V"
        }
    .end annotation

    const v2, 0x7f0e0048

    const v0, 0x7f0400bb

    const v1, 0x1020016

    invoke-direct {p0, p1, v0, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v0, p1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mPreMatchStyle:Landroid/text/style/CharacterStyle;

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v0, p1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mPostMatchStyle:Landroid/text/style/CharacterStyle;

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e0049

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mMatchStyle:Landroid/text/style/CharacterStyle;

    return-void
.end method

.method private hilightQuery(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v3, -0x1

    if-le v2, v3, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    add-int v0, v2, v3

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mPreMatchStyle:Landroid/text/style/CharacterStyle;

    invoke-virtual {v1, v3, v5, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mMatchStyle:Landroid/text/style/CharacterStyle;

    invoke-virtual {v1, v3, v2, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mPostMatchStyle:Landroid/text/style/CharacterStyle;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_0
    return-object v1

    :cond_0
    iget-object v3, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mPreMatchStyle:Landroid/text/style/CharacterStyle;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v1, v3, v5, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method


# virtual methods
.method filterBy(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mFilter:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->mSportTeamPlayer:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;
    invoke-static {v3}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->access$400(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;->getSport()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->SPORT_ENUM_TO_ICON_ID:Ljava/util/Map;
    invoke-static {}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->access$500()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    const v7, 0x1020006

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mFilter:Ljava/lang/String;

    if-eqz v7, :cond_1

    const v7, 0x1020016

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->mFilter:Ljava/lang/String;

    invoke-direct {p0, v5, v7}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamListAdpater;->hilightQuery(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object v6
.end method
