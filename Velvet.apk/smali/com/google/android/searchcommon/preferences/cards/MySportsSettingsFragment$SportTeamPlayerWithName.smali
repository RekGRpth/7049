.class Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;
.super Ljava/lang/Object;
.source "MySportsSettingsFragment.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SportTeamPlayerWithName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;",
        ">;"
    }
.end annotation


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mSportTeamPlayer:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;


# direct methods
.method public constructor <init>(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->mSportTeamPlayer:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->mName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;)Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->mSportTeamPlayer:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;)I
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->mName:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->compareTo(Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;)I

    move-result v0

    return v0
.end method

.method getSportTeamPlayer()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->mSportTeamPlayer:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment$SportTeamPlayerWithName;->mName:Ljava/lang/String;

    return-object v0
.end method
