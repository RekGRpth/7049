.class final Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$2;
.super Ljava/lang/Object;
.source "MyStocksSettingsFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getStockDataFromPreferences(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)I
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getSymbol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    check-cast p2, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$2;->compare(Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)I

    move-result v0

    return v0
.end method
