.class public Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;
.super Landroid/app/DialogFragment;
.source "MyStocksSettingsFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddStockDialogFragment"
.end annotation


# instance fields
.field private mStocksFilter:Landroid/widget/EditText;

.field private mStocksListView:Landroid/widget/ListView;

.field private mWorkFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1    # Landroid/text/Editable;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->fetchStockSymbolList(Ljava/lang/String;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method fetchStockSymbolList(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mWorkFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->startFetchingSuggestions(Ljava/lang/String;)V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "work"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    iput-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mWorkFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->setListAdapter()V

    iget-object v2, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mStocksFilter:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->fetchStockSymbolList(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f040004

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f100022

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mStocksFilter:Landroid/widget/EditText;

    const v5, 0x7f100023

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mStocksListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mStocksListView:Landroid/widget/ListView;

    const v6, 0x1020004

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v5, "work"

    invoke-virtual {v2, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v5

    check-cast v5, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    iput-object v5, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mWorkFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v5, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mStocksFilter:Landroid/widget/EditText;

    invoke-virtual {v5, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mWorkFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->detachStockDialog()V

    return-void
.end method

.method public onStart()V
    .locals 4

    const/4 v3, -0x1

    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method setListAdapter()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mWorkFragment:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->getAdapter()Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mStocksListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;->mStocksListView:Landroid/widget/ListView;

    new-instance v2, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment$1;-><init>(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$AddStockDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method
