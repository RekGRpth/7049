.class public Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;
.super Landroid/app/DialogFragment;
.source "MyStocksSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RemoveStockDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Landroid/app/Fragment;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;
    .locals 3
    .param p0    # Landroid/app/Fragment;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;

    invoke-direct {v1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "label"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "key"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;->setArguments(Landroid/os/Bundle;)V

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "label"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "key"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0d0212

    new-instance v5, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment$2;

    invoke-direct {v5, p0, v2}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment$2;-><init>(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0d037c

    new-instance v5, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment$1;-><init>(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$RemoveStockDialogFragment;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method
