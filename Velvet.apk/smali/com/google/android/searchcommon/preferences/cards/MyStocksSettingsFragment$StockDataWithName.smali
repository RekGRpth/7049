.class Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;
.super Ljava/lang/Object;
.source "MyStocksSettingsFragment.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StockDataWithName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;",
        ">;"
    }
.end annotation


# instance fields
.field private final stockData:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

.field private final symbol:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->symbol:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->stockData:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;)I
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->symbol:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->symbol:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->compareTo(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;)I

    move-result v0

    return v0
.end method

.method getStockData()Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->stockData:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->symbol:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->stockData:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;->stockData:Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getExchange()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
