.class public Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;
.super Landroid/app/Fragment;
.source "MyStocksSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TickerFetcherFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;,
        Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method detachStockDialog()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->clear()V

    return-void
.end method

.method getAdapter()Landroid/widget/ArrayAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->setRetainInstance(Z)V

    return-void
.end method

.method startFetchingSuggestions(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;-><init>(Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockSymbolSuggestTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method updateSymbolList(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$StockDataWithName;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->clear()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;

    invoke-virtual {v0, p2}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->setQuery(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->addAll(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment;->mAdapter:Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment$TickerFetcherFragment$StockDataListAdpater;->notifyDataSetChanged()V

    return-void
.end method
