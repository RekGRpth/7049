.class public Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;
.source "MyStuffSettingsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;-><init>()V

    return-void
.end method

.method private updateSummary(Ljava/util/List;III)V
    .locals 3
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;III)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", "

    invoke-static {v1}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, p2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u2026"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, p3}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p3}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method


# virtual methods
.method protected getPreferenceResourceId()I
    .locals 1

    const v0, 0x7f07000f

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->remindersEnabled(Lcom/google/android/searchcommon/SearchConfig;)Z

    move-result v2

    if-nez v2, :cond_0

    const v3, 0x7f0d00c2

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 11

    const/4 v10, 0x2

    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/cards/AbstractMyStuffSettingsFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v7

    const v8, 0x7f0d006c

    invoke-virtual {p0, v8}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/searchcommon/preferences/cards/MyStocksSettingsFragment;->getStockDataFromPreferences(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v10, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-ge v0, v7, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;

    invoke-virtual {v2}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$StockQuotes$StockData;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    const v8, 0x7f0d00bf

    const v9, 0x7f0d0309

    invoke-direct {p0, v3, v7, v8, v9}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->updateSummary(Ljava/util/List;III)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getConfigurationPreferences()Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v7

    const v8, 0x7f0d0082

    invoke-virtual {p0, v8}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/searchcommon/preferences/cards/MySportsSettingsFragment;->getSportEntitiesFromPreferences(Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v10, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v0, v7, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$SidekickConfiguration$Sports$SportTeamPlayer;->getTeam()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    const v8, 0x7f0d00c1

    const v9, 0x7f0d030a

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/google/android/searchcommon/preferences/cards/MyStuffSettingsFragment;->updateSummary(Ljava/util/List;III)V

    return-void
.end method
