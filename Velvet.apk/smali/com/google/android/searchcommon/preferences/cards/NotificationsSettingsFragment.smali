.class public Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;
.super Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;
.source "NotificationsSettingsFragment.java"


# instance fields
.field private mRingtonePreference:Landroid/preference/RingtonePreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;-><init>()V

    return-void
.end method

.method private configureRingtonePreference(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0d0097

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/RingtonePreference;

    iput-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->mRingtonePreference:Landroid/preference/RingtonePreference;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->mRingtonePreference:Landroid/preference/RingtonePreference;

    new-instance v1, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment$1;-><init>(Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/preference/RingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method


# virtual methods
.method protected addCardEnablementSwitch(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->onIsMultiPane()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->addCardEnablementSwitch(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getPreferenceResourceId()I
    .locals 1

    const v0, 0x7f070010

    return v0
.end method

.method protected isOverallPreferenceEnabled(Landroid/content/SharedPreferences;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->mRingtonePreference:Landroid/preference/RingtonePreference;

    invoke-virtual {v0, p1, p2, p3}, Landroid/preference/RingtonePreference;->onActivityResult(IILandroid/content/Intent;)Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->remindersEnabled(Lcom/google/android/searchcommon/SearchConfig;)Z

    move-result v2

    if-nez v2, :cond_0

    const v3, 0x7f0d00b5

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->configureRingtonePreference(Landroid/content/Context;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/searchcommon/preferences/cards/CardSettingsFragment;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/searchcommon/preferences/cards/NotificationsSettingsFragment;->mRingtonePreference:Landroid/preference/RingtonePreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/RingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method protected persistOverallPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
