.class public abstract Lcom/google/android/searchcommon/suggest/AbstractPromoter;
.super Ljava/lang/Object;
.source "AbstractPromoter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/Promoter;


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mFilter:Lcom/google/android/searchcommon/suggest/SuggestionFilter;

.field private final mNext:Lcom/google/android/searchcommon/suggest/Promoter;


# direct methods
.method protected constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionFilter;
    .param p2    # Lcom/google/android/searchcommon/suggest/Promoter;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/AbstractPromoter;->mFilter:Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/AbstractPromoter;->mNext:Lcom/google/android/searchcommon/suggest/Promoter;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/AbstractPromoter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-void
.end method


# virtual methods
.method protected accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/AbstractPromoter;->mFilter:Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/AbstractPromoter;->mFilter:Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    invoke-interface {v0, p1, p2}, Lcom/google/android/searchcommon/suggest/SuggestionFilter;->accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected abstract doPickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
.end method

.method public pickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p2    # I
    .param p3    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .param p4    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/searchcommon/suggest/AbstractPromoter;->doPickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/AbstractPromoter;->mNext:Lcom/google/android/searchcommon/suggest/Promoter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/AbstractPromoter;->mNext:Lcom/google/android/searchcommon/suggest/Promoter;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/searchcommon/suggest/Promoter;->pickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    :cond_0
    return-void
.end method
