.class public Lcom/google/android/searchcommon/suggest/CachingPromoter;
.super Ljava/lang/Object;
.source "CachingPromoter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;
    }
.end annotation


# static fields
.field public static final PROMOTED_SOURCE:Ljava/lang/String; = "promoted"


# instance fields
.field private mCache:Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

.field private mMaxPromoted:I

.field private mPromoter:Lcom/google/android/searchcommon/suggest/Promoter;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mMaxPromoted:I

    new-instance v0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;-><init>(Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/Suggestions;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mCache:Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/Suggestions;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->buildPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized buildPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;

    const-string v1, "promoted"

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setSourceSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mPromoter:Lcom/google/android/searchcommon/suggest/Promoter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mPromoter:Lcom/google/android/searchcommon/suggest/Promoter;

    iget v2, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mMaxPromoted:I

    invoke-interface {v1, p1, v2, v0, p2}, Lcom/google/android/searchcommon/suggest/Promoter;->pickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public getPromoted(Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mCache:Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->getPromoted(Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    return-object v0
.end method

.method public notifyChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mCache:Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->onChanged()V

    return-void
.end method

.method public declared-synchronized setPromoter(Lcom/google/android/searchcommon/suggest/Promoter;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Promoter;

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mPromoter:Lcom/google/android/searchcommon/suggest/Promoter;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mPromoter:Lcom/google/android/searchcommon/suggest/Promoter;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mCache:Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->onInvalidated()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mCache:Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mCache:Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;->dispose()V

    :cond_0
    new-instance v0, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;-><init>(Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/Suggestions;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/CachingPromoter;->mCache:Lcom/google/android/searchcommon/suggest/CachingPromoter$CacheEntry;

    return-void
.end method
