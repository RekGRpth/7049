.class public Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;
.super Lcom/google/android/searchcommon/suggest/SuggestionFormatter;
.source "LevenshteinSuggestionFormatter.java"


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/TextAppearanceFactory;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/TextAppearanceFactory;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionFormatter;-><init>(Lcom/google/android/searchcommon/TextAppearanceFactory;)V

    return-void
.end method

.method private normalizeQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method findMatches([Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;)[I
    .locals 7
    .param p1    # [Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;
    .param p2    # [Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    new-instance v3, Lcom/google/android/searchcommon/util/LevenshteinDistance;

    invoke-direct {v3, p1, p2}, Lcom/google/android/searchcommon/util/LevenshteinDistance;-><init>([Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;)V

    invoke-virtual {v3}, Lcom/google/android/searchcommon/util/LevenshteinDistance;->calculate()I

    array-length v4, p2

    new-array v2, v4, [I

    invoke-virtual {v3}, Lcom/google/android/searchcommon/util/LevenshteinDistance;->getTargetOperations()[Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v1, v0

    invoke-virtual {v5}, Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;->getType()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    aget-object v5, v1, v0

    invoke-virtual {v5}, Lcom/google/android/searchcommon/util/LevenshteinDistance$EditOperation;->getPosition()I

    move-result v5

    aput v5, v2, v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, -0x1

    aput v5, v2, v0

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method public formatSuggestion(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;->normalizeQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;->tokenize(Ljava/lang/String;)[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    move-result-object v3

    invoke-virtual {p0, p2}, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;->tokenize(Ljava/lang/String;)[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    move-result-object v6

    invoke-virtual {p0, v3, v6}, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;->findMatches([Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;)[I

    move-result-object v1

    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v7, v6, v0

    const/4 v4, 0x0

    aget v8, v1, v0

    if-ltz v8, :cond_0

    aget-object v9, v3, v8

    invoke-virtual {v9}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->length()I

    move-result v4

    :cond_0
    iget v9, v7, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    add-int/2addr v9, v4

    iget v10, v7, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mEnd:I

    invoke-virtual {p0, v5, v9, v10}, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;->applySuggestedTextStyle(Landroid/text/Spannable;II)V

    iget v9, v7, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    iget v10, v7, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    add-int/2addr v10, v4

    invoke-virtual {p0, v5, v9, v10}, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;->applyQueryTextStyle(Landroid/text/Spannable;II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v5
.end method

.method public bridge synthetic formatSuggestion(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/suggest/LevenshteinSuggestionFormatter;->formatSuggestion(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method tokenize(Ljava/lang/String;)[Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;
    .locals 13
    .param p1    # Ljava/lang/String;

    const/16 v12, 0x20

    const/16 v11, 0x9

    const/4 v10, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    new-array v8, v2, [Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v3, v2, :cond_3

    :goto_1
    if-ge v3, v2, :cond_1

    aget-char v9, v0, v3

    if-eq v9, v12, :cond_0

    aget-char v9, v0, v3

    if-ne v9, v11, :cond_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v5, v3

    :goto_2
    if-ge v3, v2, :cond_2

    aget-char v9, v0, v3

    if-eq v9, v12, :cond_2

    aget-char v9, v0, v3

    if-eq v9, v11, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    move v1, v3

    if-eq v5, v1, :cond_4

    add-int/lit8 v6, v7, 0x1

    new-instance v9, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    invoke-direct {v9, v0, v5, v1}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;-><init>([CII)V

    aput-object v9, v8, v7

    :goto_3
    move v7, v6

    goto :goto_0

    :cond_3
    new-array v4, v7, [Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    invoke-static {v8, v10, v4, v10, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v4

    :cond_4
    move v6, v7

    goto :goto_3
.end method
