.class public interface abstract Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
.super Ljava/lang/Object;
.source "MutableSuggestionList.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionList;


# virtual methods
.method public abstract add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
.end method

.method public abstract addAll(Ljava/lang/Iterable;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;)I"
        }
    .end annotation
.end method

.method public abstract remove(I)V
.end method

.method public abstract setAccount(Ljava/lang/String;)V
.end method

.method public abstract setFinal()V
.end method

.method public abstract setRequestFailed(Z)V
.end method

.method public abstract setRequestMade(Z)V
.end method

.method public abstract setSourceSuggestionCount(I)V
.end method
