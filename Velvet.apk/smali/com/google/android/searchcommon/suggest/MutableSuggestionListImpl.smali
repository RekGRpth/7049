.class public Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;
.super Lcom/google/android/searchcommon/suggest/SuggestionListImpl;
.source "MutableSuggestionListImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/MutableSuggestionList;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;

    const/16 v0, 0x10

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;J)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # J

    const/16 v3, 0x10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;IJ)V

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mExtraColumns:Ljava/util/HashSet;

    const/4 v0, 0x1

    return v0
.end method

.method public addAll(Ljava/lang/Iterable;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;)I"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->getCount()I

    move-result v1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->getCount()I

    move-result v3

    sub-int/2addr v3, v1

    return v3
.end method

.method public remove(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mExtraColumns:Ljava/util/HashSet;

    return-void
.end method

.method public replace(ILcom/google/android/searchcommon/suggest/Suggestion;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mSuggestions:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mExtraColumns:Ljava/util/HashSet;

    return-void
.end method

.method public setAccount(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mAccount:Ljava/lang/String;

    return-void
.end method

.method public setFinal()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mIsFinal:Z

    return-void
.end method

.method public setSourceSuggestionCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->mSourceSuggestionCount:I

    return-void
.end method
