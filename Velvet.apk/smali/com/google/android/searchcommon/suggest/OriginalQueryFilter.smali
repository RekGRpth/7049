.class public Lcom/google/android/searchcommon/suggest/OriginalQueryFilter;
.super Ljava/lang/Object;
.source "OriginalQueryFilter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionFilter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWebSearchSuggestion()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getQueryStringForSearch()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
