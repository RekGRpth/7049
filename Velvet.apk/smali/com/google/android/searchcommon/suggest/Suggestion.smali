.class public Lcom/google/android/searchcommon/suggest/Suggestion;
.super Ljava/lang/Object;
.source "Suggestion.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/searchcommon/suggest/Suggestion;",
        ">;"
    }
.end annotation


# static fields
.field private static final STRING_ORDERING:Lcom/google/common/collect/Ordering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Ordering",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TO_STRING_ORDERING:Lcom/google/common/collect/Ordering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Ordering",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static mNextFreeSuggestionId:J


# instance fields
.field private final mExtras:Lcom/google/android/searchcommon/suggest/SuggestionExtras;

.field private final mHashCode:I

.field private final mIcon1:Ljava/lang/String;

.field private final mIcon2:Ljava/lang/String;

.field private final mIconLarge:Ljava/lang/String;

.field private final mIntentAction:Ljava/lang/String;

.field private final mIntentComponent:Landroid/content/ComponentName;

.field private final mIntentData:Ljava/lang/String;

.field private final mIntentExtraData:Ljava/lang/String;

.field private final mInternalIntent:Landroid/content/Intent;

.field private final mIsCorrectionSuggestion:Z

.field private final mIsHistory:Z

.field private final mIsWordByWordSuggestion:Z

.field private final mLastAccessTime:J

.field private final mLogType:Ljava/lang/String;

.field private final mPrefetch:Z

.field private final mSource:Lcom/google/android/searchcommon/summons/Source;

.field private final mSpinnerWhileRefreshing:Z

.field private mSuggestionId:J

.field private mSuggestionKey:Ljava/lang/String;

.field private final mSuggestionQuery:Ljava/lang/String;

.field private final mText1:Ljava/lang/CharSequence;

.field private final mText2:Ljava/lang/CharSequence;

.field private final mText2Url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/common/collect/Ordering;->natural()Lcom/google/common/collect/Ordering;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/Ordering;->nullsFirst()Lcom/google/common/collect/Ordering;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-static {}, Lcom/google/common/collect/Ordering;->usingToString()Lcom/google/common/collect/Ordering;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/Ordering;->nullsFirst()Lcom/google/common/collect/Ordering;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/suggest/Suggestion;->TO_STRING_ORDERING:Lcom/google/common/collect/Ordering;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText1:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2Url:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon1:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon2:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIconLarge:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLastAccessTime:J

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSpinnerWhileRefreshing:Z

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentAction:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentData:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentExtraData:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentComponent:Landroid/content/ComponentName;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mInternalIntent:Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionQuery:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLogType:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsHistory:Z

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mExtras:Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    iput v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mHashCode:I

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mPrefetch:Z

    iput-wide v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionId:J

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsWordByWordSuggestion:Z

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsCorrectionSuggestion:Z

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/searchcommon/suggest/SuggestionExtras;ZZZ)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/summons/Source;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # J
    .param p10    # Z
    .param p11    # Ljava/lang/String;
    .param p12    # Ljava/lang/String;
    .param p13    # Ljava/lang/String;
    .param p14    # Landroid/content/ComponentName;
    .param p15    # Landroid/content/Intent;
    .param p16    # Ljava/lang/String;
    .param p17    # Ljava/lang/String;
    .param p18    # Z
    .param p19    # Lcom/google/android/searchcommon/suggest/SuggestionExtras;
    .param p20    # Z
    .param p21    # Z
    .param p22    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText1:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2Url:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon1:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon2:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIconLarge:Ljava/lang/String;

    iput-wide p8, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLastAccessTime:J

    iput-boolean p10, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSpinnerWhileRefreshing:Z

    iput-object p11, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentAction:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentData:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentExtraData:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentComponent:Landroid/content/ComponentName;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mInternalIntent:Landroid/content/Intent;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionQuery:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLogType:Ljava/lang/String;

    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsHistory:Z

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mExtras:Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    move/from16 v0, p20

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mPrefetch:Z

    move/from16 v0, p21

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsWordByWordSuggestion:Z

    move/from16 v0, p22

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsCorrectionSuggestion:Z

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->calculateHashCode()I

    move-result v1

    iput v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mHashCode:I

    invoke-static {}, Lcom/google/android/searchcommon/suggest/Suggestion;->getNextSuggestionId()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionId:J

    return-void
.end method

.method public static builder()Lcom/google/android/searchcommon/suggest/Suggestion$Builder;
    .locals 1

    new-instance v0, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    invoke-direct {v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;-><init>()V

    return-object v0
.end method

.method private calculateHashCode()I
    .locals 4

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon1:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon2:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIconLarge:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText1:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentAction:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentData:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentExtraData:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLogType:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    aput-object v0, v1, v2

    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSpinnerWhileRefreshing:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionQuery:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v1

    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v1}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private createSuggestionKey()Ljava/lang/String;
    .locals 8

    const/16 v7, 0x23

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWebSearchSuggestion()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/searchcommon/suggest/Suggestion;->makeKeyComponent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentData:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/searchcommon/suggest/Suggestion;->normalizeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/searchcommon/suggest/Suggestion;->makeKeyComponent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/searchcommon/suggest/Suggestion;->makeKeyComponent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int v4, v5, v6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    return-object v5

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText1:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2Url:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private static declared-synchronized getNextSuggestionId()J
    .locals 6

    const-class v1, Lcom/google/android/searchcommon/suggest/Suggestion;

    monitor-enter v1

    :try_start_0
    sget-wide v2, Lcom/google/android/searchcommon/suggest/Suggestion;->mNextFreeSuggestionId:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    sput-wide v4, Lcom/google/android/searchcommon/suggest/Suggestion;->mNextFreeSuggestionId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static makeKeyComponent(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method

.method static normalizeUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_1

    const-string v4, "://"

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "http"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, "://"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int v3, v4, v5

    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v4, 0x2f

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    add-int/lit8 v5, v0, -0x1

    if-ne v4, v5, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_1
    return-object p0

    :cond_2
    move-object v1, p0

    const-string v4, "://"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int v3, v2, v4

    goto :goto_0
.end method


# virtual methods
.method public canBeLargeImageSuggestion()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIconLarge()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public compareTo(Lcom/google/android/searchcommon/suggest/Suggestion;)I
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    if-ne p0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/common/collect/ComparisonChain;->start()Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText1:Ljava/lang/CharSequence;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mText1:Ljava/lang/CharSequence;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->TO_STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2:Ljava/lang/CharSequence;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2:Ljava/lang/CharSequence;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->TO_STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon1:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon1:Ljava/lang/String;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon2:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon2:Ljava/lang/String;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIconLarge:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mIconLarge:Ljava/lang/String;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentAction:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentAction:Ljava/lang/String;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentData:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentData:Ljava/lang/String;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentExtraData:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentExtraData:Ljava/lang/String;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mInternalIntent:Landroid/content/Intent;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mInternalIntent:Landroid/content/Intent;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->TO_STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLogType:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mLogType:Ljava/lang/String;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSourcePackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSourcePackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionQuery:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionQuery:Ljava/lang/String;

    sget-object v3, Lcom/google/android/searchcommon/suggest/Suggestion;->STRING_ORDERING:Lcom/google/common/collect/Ordering;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/collect/ComparisonChain;->compare(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSpinnerWhileRefreshing:Z

    iget-boolean v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;->mSpinnerWhileRefreshing:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ComparisonChain;->compare(ZZ)Lcom/google/common/collect/ComparisonChain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ComparisonChain;->result()I

    move-result v0

    goto/16 :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->compareTo(Lcom/google/android/searchcommon/suggest/Suggestion;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/searchcommon/suggest/Suggestion;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->compareTo(Lcom/google/android/searchcommon/suggest/Suggestion;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public getExtras()Lcom/google/android/searchcommon/suggest/SuggestionExtras;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mExtras:Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    return-object v0
.end method

.method public getInternalIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mInternalIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getLastAccessTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLastAccessTime:J

    return-wide v0
.end method

.method public getSourcePackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/Source;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSuggestionIcon1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon1:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIcon2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIcon2:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIconLarge()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIconLarge:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIntentAction()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentAction:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentAction:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/Source;->getDefaultIntentAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSuggestionIntentComponent()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentComponent:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentComponent:Landroid/content/ComponentName;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSuggestionIntentDataString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentData:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIntentExtraData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentExtraData:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getSuggestionKey()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionKey:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->createSuggestionKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionKey:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionKey:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSuggestionLogType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLogType:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionSource()Lcom/google/android/searchcommon/summons/Source;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    return-object v0
.end method

.method public getSuggestionText1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText1:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getSuggestionText2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getSuggestionText2Url()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2Url:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mHashCode:I

    return v0
.end method

.method public isContactSuggestion()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/Source;->isContactsSource()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCorrectionSuggestion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsCorrectionSuggestion:Z

    return v0
.end method

.method public isHistorySuggestion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsHistory:Z

    return v0
.end method

.method public isNavSuggestion()Z
    .locals 1

    invoke-static {p0}, Lcom/google/android/searchcommon/suggest/web/WebSuggestions;->isNavSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v0

    return v0
.end method

.method public isSpinnerWhileRefreshing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSpinnerWhileRefreshing:Z

    return v0
.end method

.method public isWebSearchSuggestion()Z
    .locals 1

    invoke-static {p0}, Lcom/google/android/searchcommon/suggest/web/WebSuggestions;->isWebSearchSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v0

    return v0
.end method

.method public isWordByWordSuggestion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIsWordByWordSuggestion:Z

    return v0
.end method

.method public shouldPrefetch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mPrefetch:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "source"

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "text1"

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText1:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "text2"

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mText2:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "intentAction"

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentAction:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "intentData"

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mIntentData:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSuggestionQuery:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "logtype"

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLogType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lastAccesstTime"

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mLastAccessTime:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "prefetch"

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mPrefetch:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestion;->mSource:Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
