.class public abstract Lcom/google/android/searchcommon/suggest/SuggestionFormatter;
.super Ljava/lang/Object;
.source "SuggestionFormatter.java"


# instance fields
.field private final mSpanFactory:Lcom/google/android/searchcommon/TextAppearanceFactory;


# direct methods
.method protected constructor <init>(Lcom/google/android/searchcommon/TextAppearanceFactory;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/TextAppearanceFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionFormatter;->mSpanFactory:Lcom/google/android/searchcommon/TextAppearanceFactory;

    return-void
.end method

.method private setSpans(Landroid/text/Spannable;II[Ljava/lang/Object;)V
    .locals 5
    .param p1    # Landroid/text/Spannable;
    .param p2    # I
    .param p3    # I
    .param p4    # [Ljava/lang/Object;

    move-object v0, p4

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    const/4 v4, 0x0

    invoke-interface {p1, v3, p2, p3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected applyQueryTextStyle(Landroid/text/Spannable;II)V
    .locals 1
    .param p1    # Landroid/text/Spannable;
    .param p2    # I
    .param p3    # I

    if-ne p2, p3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionFormatter;->mSpanFactory:Lcom/google/android/searchcommon/TextAppearanceFactory;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/TextAppearanceFactory;->createSuggestionQueryTextAppearance()[Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/searchcommon/suggest/SuggestionFormatter;->setSpans(Landroid/text/Spannable;II[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected applySuggestedTextStyle(Landroid/text/Spannable;II)V
    .locals 1
    .param p1    # Landroid/text/Spannable;
    .param p2    # I
    .param p3    # I

    if-ne p2, p3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionFormatter;->mSpanFactory:Lcom/google/android/searchcommon/TextAppearanceFactory;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/TextAppearanceFactory;->createSuggestionSuggestedTextAppearance()[Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/searchcommon/suggest/SuggestionFormatter;->setSpans(Landroid/text/Spannable;II[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public abstract formatSuggestion(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
.end method
