.class public Lcom/google/android/searchcommon/suggest/SuggestionLauncher;
.super Ljava/lang/Object;
.source "SuggestionLauncher.java"

# interfaces
.implements Lcom/google/android/searchcommon/ui/SuggestionClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/SuggestionLauncher$Applications;
    }
.end annotation


# instance fields
.field protected mAppSearchData:Landroid/os/Bundle;

.field private final mContext:Landroid/content/Context;

.field private final mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

.field private final mPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

.field private final mQueryState:Lcom/google/android/velvet/presenter/QueryState;

.field private final mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/QueryState;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;
    .param p3    # Lcom/google/android/searchcommon/GlobalSearchServices;
    .param p4    # Lcom/google/android/velvet/presenter/VelvetPresenter;
    .param p5    # Lcom/google/android/velvet/presenter/QueryState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    iput-object p4, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    iput-object p5, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    return-void
.end method

.method private createIntent(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;)Landroid/content/Intent;
    .locals 7
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentExtraData()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x10000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v5, 0x4000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :cond_0
    const-string v5, "user_query"

    invoke-virtual {v3, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v4, :cond_1

    const-string v5, "query"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    if-eqz v2, :cond_2

    const-string v5, "intent_extra_data_key"

    invoke-virtual {v3, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-object v5, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mAppSearchData:Landroid/os/Bundle;

    if-eqz v5, :cond_3

    const-string v5, "app_data"

    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mAppSearchData:Landroid/os/Bundle;

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    return-object v3
.end method

.method private launchIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "QSB.SuggestionLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to start "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public getAppIntent(Lcom/google/android/searchcommon/suggest/Suggestion;)Landroid/content/Intent;
    .locals 7
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    const/4 v3, 0x0

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionSource()Lcom/google/android/searchcommon/summons/Source;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v5, "com.android.providers.applications/.ApplicationLauncher"

    invoke-interface {v4}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/searchcommon/suggest/SuggestionLauncher$Applications;->uriToComponentName(Landroid/net/Uri;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x10200000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onSuggestionClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-virtual {p2, p1}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Lcom/google/android/searchcommon/clicklog/ClickLog;->reportClick(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWebSearchSuggestion()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lcom/google/android/velvet/Query;->fromClickedSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    :goto_0
    return-void

    :cond_0
    const/16 v3, 0x69

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isNavSuggestion()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "navsuggestion"

    :goto_1
    invoke-static {v3, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->getAppIntent(Lcom/google/android/searchcommon/suggest/Suggestion;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->launchIntent(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSourcePackageName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->createIntent(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->launchIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onSuggestionQueryRefineClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/velvet/Query;->fromQueryRefinement(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    return-void
.end method

.method public onSuggestionQuickContactClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    if-eqz p1, :cond_0

    invoke-virtual {p2, p1}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;->getSuggestionList(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/GlobalSearchServices;->getClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/android/searchcommon/clicklog/ClickLog;->reportClick(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onSuggestionRemoveFromHistoryClicked(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;->mPresenter:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;->removeFromHistoryClicked(Lcom/google/android/searchcommon/suggest/Suggestion;)V

    return-void
.end method
