.class public interface abstract Lcom/google/android/searchcommon/suggest/SuggestionList;
.super Ljava/lang/Object;
.source "SuggestionList.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/google/android/searchcommon/suggest/Suggestion;",
        ">;"
    }
.end annotation


# static fields
.field public static final UNSET_LATENCY:I = -0x1


# virtual methods
.method public abstract get(I)Lcom/google/android/searchcommon/suggest/Suggestion;
.end method

.method public abstract getAccount()Ljava/lang/String;
.end method

.method public abstract getCount()I
.end method

.method public abstract getCreationTime()J
.end method

.method public abstract getExtraColumns()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLatency()I
.end method

.method public abstract getMutableCopy()Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
.end method

.method public abstract getSourceName()Ljava/lang/String;
.end method

.method public abstract getSourceSuggestionCount()I
.end method

.method public abstract getSourceSuggestions()Lcom/google/android/searchcommon/suggest/Suggestions;
.end method

.method public abstract getUserQuery()Lcom/google/android/velvet/Query;
.end method

.method public abstract indexOf(Lcom/google/android/searchcommon/suggest/Suggestion;)I
.end method

.method public abstract isFinal()Z
.end method

.method public abstract isFromCache()Z
.end method

.method public abstract isRequestFailed()Z
.end method

.method public abstract setFromCache(Z)V
.end method

.method public abstract setLatency(I)V
.end method

.method public abstract setSourceSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V
.end method

.method public abstract wasRequestMade()Z
.end method
