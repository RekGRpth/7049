.class public Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;
.super Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;
.source "SuggestionListNoDuplicates.java"


# instance fields
.field private final mSuggestionKeys:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->mSuggestionKeys:Ljava/util/HashSet;

    return-void
.end method

.method private findSuggestionPosByKey(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 7
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    const/4 v6, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionKey()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->mSuggestionKeys:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v3

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->shouldPrefetch()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isHistorySuggestion()Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionKey()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->findSuggestionPosByKey(Ljava/lang/String;)I

    move-result v2

    new-instance v4, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    invoke-direct {v4}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->fromSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->shouldPrefetch()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0, v6}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->shouldPrefetch(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isHistorySuggestion()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0, v6}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isHistory(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->build()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->replace(ILcom/google/android/searchcommon/suggest/Suggestion;)V

    goto :goto_0
.end method

.method public addAll(Ljava/lang/Iterable;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/searchcommon/suggest/Suggestion;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public getMutableCopy()Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .locals 3

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->copyInto(Lcom/google/android/searchcommon/suggest/MutableSuggestionList;)V

    return-object v0
.end method

.method public remove(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->mSuggestionKeys:Ljava/util/HashSet;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->remove(I)V

    return-void
.end method

.method public replace(ILcom/google/android/searchcommon/suggest/Suggestion;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->mSuggestionKeys:Ljava/util/HashSet;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->mSuggestionKeys:Ljava/util/HashSet;

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/searchcommon/suggest/MutableSuggestionListImpl;->replace(ILcom/google/android/searchcommon/suggest/Suggestion;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionListNoDuplicates;->remove(I)V

    goto :goto_0
.end method
