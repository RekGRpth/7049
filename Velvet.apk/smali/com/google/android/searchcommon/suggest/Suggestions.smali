.class public Lcom/google/android/searchcommon/suggest/Suggestions;
.super Ljava/lang/Object;
.source "Suggestions.java"


# static fields
.field private static final DBG:Z = false

.field private static final DBG_LIFECYCLE:Z = false

.field public static final NONE:Lcom/google/android/searchcommon/suggest/Suggestions;

.field public static final NOT_QUERYING_SUMMONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "QSB.Suggestions"


# instance fields
.field private mClosed:Z

.field private final mDataSetObservable:Lcom/google/android/searchcommon/util/SafeDataSetObservable;

.field private mDisplayedTentatively:Z

.field private mDone:Z

.field private final mExpectedSources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;"
        }
    .end annotation
.end field

.field protected final mQuery:Lcom/google/android/velvet/Query;

.field private final mSourcePositions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private mTentativeDisplay:Z

.field private mTimedOut:Z

.field private mWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private final mWebSource:Lcom/google/android/searchcommon/google/GoogleSource;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/suggest/Suggestions;->NOT_QUERYING_SUMMONS:Ljava/util/List;

    new-instance v0, Lcom/google/android/searchcommon/suggest/Suggestions$1;

    sget-object v1, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/suggest/Suggestions$1;-><init>(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/google/GoogleSource;)V

    sput-object v0, Lcom/google/android/searchcommon/suggest/Suggestions;->NONE:Lcom/google/android/searchcommon/suggest/Suggestions;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/google/GoogleSource;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;
    .param p3    # Lcom/google/android/searchcommon/google/GoogleSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;",
            "Lcom/google/android/searchcommon/google/GoogleSource;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/searchcommon/util/SafeDataSetObservable;

    invoke-direct {v1}, Lcom/google/android/searchcommon/util/SafeDataSetObservable;-><init>()V

    iput-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDataSetObservable:Lcom/google/android/searchcommon/util/SafeDataSetObservable;

    iput-boolean v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDone:Z

    iput-boolean v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mTentativeDisplay:Z

    iput-boolean v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDisplayedTentatively:Z

    iput-boolean v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mTimedOut:Z

    iput-boolean v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mClosed:Z

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mQuery:Lcom/google/android/velvet/Query;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebSource:Lcom/google/android/searchcommon/google/GoogleSource;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourcePositions:Ljava/util/HashMap;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourcePositions:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v1}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private countSourceResults(Z)I
    .locals 3
    .param p1    # Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    aget-object v2, v2, v1

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v2

    :goto_1
    add-int/2addr v0, v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    return v0
.end method

.method private countWebResults()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addResults(Ljava/util/List;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 7
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isTimedOut()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_3

    iget-object v4, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mQuery:Lcom/google/android/velvet/Query;

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/velvet/Query;->equivalentForSuggest(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got result for wrong query: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " != "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v4, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v4, p0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->setSourceSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    :cond_3
    if-eqz p1, :cond_6

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceName()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mQuery:Lcom/google/android/velvet/Query;

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/velvet/Query;->equivalentForSuggest(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v4

    if-nez v4, :cond_4

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got result for wrong query: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " != "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    iget-object v4, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourcePositions:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_5

    const-string v4, "QSB.Suggestions"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got unexpected result from source "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput-object v3, v4, v5

    invoke-interface {v3, p0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->setSourceSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method

.method public areSourcesDone()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDone:Z

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->countSourceResults(Z)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->getExpectedSourceResultCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public areWebResultsDone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDone:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebSource:Lcom/google/android/searchcommon/google/GoogleSource;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->countWebResults()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mClosed:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDataSetObservable:Lcom/google/android/searchcommon/util/SafeDataSetObservable;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/SafeDataSetObservable;->unregisterAll()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mClosed:Z

    goto :goto_0
.end method

.method public displayedTentatively()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDisplayedTentatively:Z

    return-void
.end method

.method public done()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDone:Z

    return-void
.end method

.method protected finalize()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDataSetObservable:Lcom/google/android/searchcommon/util/SafeDataSetObservable;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/SafeDataSetObservable;->getObserverCount()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "QSB.Suggestions"

    const-string v1, "***LEAK *** : Some observers have not been unregistered !!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public getExpectedResultCount()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->getExpectedSourceResultCount()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebSource:Lcom/google/android/searchcommon/google/GoogleSource;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getExpectedSourceResultCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getExpectedSources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    return-object v0
.end method

.method getIncludedSourceNames()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    if-eqz v3, :cond_0

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method public getOpenedCopy()Lcom/google/android/searchcommon/suggest/Suggestions;
    .locals 9

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isDone()Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    new-instance v4, Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mQuery:Lcom/google/android/velvet/Query;

    iget-object v7, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebSource:Lcom/google/android/searchcommon/google/GoogleSource;

    invoke-direct {v4, v6, v7, v8}, Lcom/google/android/searchcommon/suggest/Suggestions;-><init>(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/google/GoogleSource;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    if-eqz v3, :cond_0

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/searchcommon/suggest/Suggestions;->addResults(Ljava/util/List;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    invoke-virtual {v4}, Lcom/google/android/searchcommon/suggest/Suggestions;->done()V

    return-object v4
.end method

.method public getQuery()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method public getResultCount()I
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->countSourceResults(Z)I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->countWebResults()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getSourceResultCount()I
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->countSourceResults(Z)I

    move-result v0

    return v0
.end method

.method public getSourceResults()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mSourceResults:[Lcom/google/android/searchcommon/suggest/SuggestionList;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getSummonsCount()I
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->countSourceResults(Z)I

    move-result v0

    return v0
.end method

.method public getWebResult()Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-object v0
.end method

.method public getWebSource()Lcom/google/android/searchcommon/google/GoogleSource;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebSource:Lcom/google/android/searchcommon/google/GoogleSource;

    return-object v0
.end method

.method public isClosed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mClosed:Z

    return v0
.end method

.method public isDone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDone:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->areSourcesDone()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->areWebResultsDone()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFetchingSummons()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    sget-object v1, Lcom/google/android/searchcommon/suggest/Suggestions;->NOT_QUERYING_SUMMONS:Ljava/util/List;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTimedOut()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mTimedOut:Z

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDataSetObservable:Lcom/google/android/searchcommon/util/SafeDataSetObservable;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/SafeDataSetObservable;->notifyChanged()V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mClosed:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDataSetObservable:Lcom/google/android/searchcommon/util/SafeDataSetObservable;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/util/SafeDataSetObservable;->registerObserver(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setTentativeDisplay(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mTentativeDisplay:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDisplayedTentatively:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDisplayedTentatively:Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public shouldDisplayTentatively()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mTentativeDisplay:Z

    return v0
.end method

.method public timedOut()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->done()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mTimedOut:Z

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->notifyDataSetChanged()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Suggestions@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{expectedSources="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mExpectedSources:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",webSource="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mWebSource:Lcom/google/android/searchcommon/google/GoogleSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",getResultCount()="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestions;->getResultCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mClosed:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/Suggestions;->mDataSetObservable:Lcom/google/android/searchcommon/util/SafeDataSetObservable;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/util/SafeDataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    goto :goto_0
.end method
