.class public interface abstract Lcom/google/android/searchcommon/suggest/SuggestionsController;
.super Ljava/lang/Object;
.source "SuggestionsController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;
    }
.end annotation


# static fields
.field public static final CORRECTION_SUGGESTIONS:Ljava/lang/Object;

.field public static final SUMMONS:Ljava/lang/Object;

.field public static final WEB_SUGGESTIONS:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsController;->WEB_SUGGESTIONS:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsController;->SUMMONS:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsController;->CORRECTION_SUGGESTIONS:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public abstract addListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V
.end method

.method public abstract addSuggestionsView(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)V
.end method

.method public abstract getFetchState(Ljava/lang/Object;)I
.end method

.method public abstract getSummonsFetchState()I
.end method

.method public abstract removeListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V
.end method

.method public abstract removeSuggestionsView(Ljava/lang/Object;)V
.end method

.method public abstract setMaxDisplayed(Ljava/lang/Object;I)V
.end method

.method public abstract setSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V
.end method

.method public abstract setSuggestionsViewEnabled(Ljava/lang/Object;Z)V
.end method

.method public abstract setWebSuggestionsEnabled(Z)V
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method
