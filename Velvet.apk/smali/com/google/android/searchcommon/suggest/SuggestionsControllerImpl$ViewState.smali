.class Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
.super Ljava/lang/Object;
.source "SuggestionsControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewState"
.end annotation


# instance fields
.field private mEnabled:Z

.field private final mIsWebSuggest:Z

.field private mMaxDisplayed:I

.field private mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

.field private mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private mUi:Lcom/google/android/searchcommon/suggest/SuggestionsUi;

.field private final mViewKey:Ljava/lang/Object;

.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mViewKey:Ljava/lang/Object;

    sget-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsController;->WEB_SUGGESTIONS:Ljava/lang/Object;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mIsWebSuggest:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mEnabled:Z

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mMaxDisplayed:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/CachingPromoter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    .param p1    # Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mUi:Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mUi:Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mEnabled:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mEnabled:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)I
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    iget v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mMaxDisplayed:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;I)I
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    .param p1    # I

    iput p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mMaxDisplayed:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Z
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mIsWebSuggest:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mViewKey:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method showingCurrentResults()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceSuggestions()Lcom/google/android/searchcommon/suggest/Suggestions;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;

    iget-object v1, v1, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method showingFinalResults()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->showingCurrentResults()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->isFinal()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
