.class public Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;
.super Ljava/lang/Object;
.source "SuggestionsControllerImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionsController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    }
.end annotation


# static fields
.field private static final NO_SUGGESTIONS:Lcom/google/android/searchcommon/suggest/SuggestionList;


# instance fields
.field private mDelayTimerTask:Ljava/lang/Runnable;

.field private final mHistoryButtonPresenter:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

.field private mMaxDisplayDelayMillis:J

.field private mShowingWebRemoveFromHistoryButtons:Z

.field private mStarted:Z

.field protected mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

.field private final mSuggestionsObserver:Landroid/database/DataSetObserver;

.field private final mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mUpdateListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mViews:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;",
            ">;"
        }
    .end annotation
.end field

.field private mWebSuggestionsEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;

    const-string v1, ""

    sget-object v2, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    sput-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->NO_SUGGESTIONS:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$1;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestionsObserver:Landroid/database/DataSetObserver;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mUpdateListeners:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    new-instance v0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v2, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$2;

    invoke-direct {v2, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$2;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mHistoryButtonPresenter:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mWebSuggestionsEnabled:Z

    return-void
.end method

.method private getViewState(Ljava/lang/Object;)Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    .locals 2
    .param p1    # Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private isDelayingNewSuggestions()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mDelayTimerTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldPublish(Lcom/google/android/searchcommon/suggest/Suggestions;Lcom/google/android/searchcommon/suggest/SuggestionList;)Z
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->isFinal()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->isDelayingNewSuggestions()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    const/4 v1, 0x1

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mIsWebSuggest:Z
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$500(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mHistoryButtonPresenter:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->shouldShowRemoveFromHistoryButtons()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->showSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;ZZ)V
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p3    # Z
    .param p4    # Z

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mIsWebSuggest:Z
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$500(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mWebSuggestionsEnabled:Z

    if-nez v0, :cond_0

    const/4 p3, 0x0

    :cond_0
    iput-boolean p4, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mShowingWebRemoveFromHistoryButtons:Z

    :cond_1
    # setter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {p1, p2}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$002(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v0

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mMaxDisplayed:I
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$400(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mUi:Lcom/google/android/searchcommon/suggest/SuggestionsUi;
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$200(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    move-result-object v0

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mViewKey:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$600(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Ljava/lang/Object;

    move-result-object v1

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/searchcommon/suggest/SuggestionsUi;->showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V

    return-void
.end method

.method private startDelayTimer(Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->stopDelayTimer()V

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$3;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;Lcom/google/android/searchcommon/suggest/Suggestions;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mDelayTimerTask:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mDelayTimerTask:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mMaxDisplayDelayMillis:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private stopDelayTimer()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->isDelayingNewSuggestions()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mDelayTimerTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mDelayTimerTask:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private updateListeners()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mUpdateListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-interface {v1, p0, v2}, Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;->onSuggestionsUpdated(Lcom/google/android/searchcommon/suggest/SuggestionsController;Lcom/google/android/searchcommon/suggest/Suggestions;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateViewsNow()V
    .locals 4

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    const-string v3, "updateViews when not started"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateView(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateListeners()V

    return-void
.end method


# virtual methods
.method public addListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mUpdateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-interface {p1, p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;->onSuggestionsUpdated(Lcom/google/android/searchcommon/suggest/SuggestionsController;Lcom/google/android/searchcommon/suggest/Suggestions;)V

    return-void
.end method

.method public addSuggestionsView(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .param p3    # Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->getViewState(Ljava/lang/Object;)Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    move-result-object v0

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Can\'t reset a suggestions view"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    # setter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v0, p2}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$102(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/CachingPromoter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    # setter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mUi:Lcom/google/android/searchcommon/suggest/SuggestionsUi;
    invoke-static {v0, p3}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$202(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {p2, v1}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->setSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    iget-boolean v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateView(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method delayTimerExpired(Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mDelayTimerTask:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateViewsNow()V

    :cond_0
    return-void
.end method

.method public getFetchState(Ljava/lang/Object;)I
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mEnabled:Z
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$300(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Z

    move-result v2

    if-eqz v2, :cond_0

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->showingCurrentResults()Z

    move-result v2

    if-eqz v2, :cond_5

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v1, 0x3

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->showingFinalResults()Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v2, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->WEB_SUGGESTIONS:Ljava/lang/Object;

    if-ne p1, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebSource()Lcom/google/android/searchcommon/google/GoogleSource;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_3
    sget-object v2, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->SUMMONS:Ljava/lang/Object;

    if-ne p1, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getExpectedSourceResultCount()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    const/4 v1, 0x2

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSummonsFetchState()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isFetchingSummons()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->getSummonsCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->areSourcesDone()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->getSummonsCount()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSuggestionsChanged()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    if-eqz v2, :cond_0

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->notifyChanged()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateViews()V

    return-void
.end method

.method public removeListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mUpdateListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeSuggestionsView(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setMaxDisplayDelayMillis(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mMaxDisplayDelayMillis:J

    return-void
.end method

.method public setMaxDisplayed(Ljava/lang/Object;I)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->getViewState(Ljava/lang/Object;)Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    move-result-object v0

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mMaxDisplayed:I
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$400(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)I

    move-result v1

    if-eq v1, p2, :cond_0

    # setter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mMaxDisplayed:I
    invoke-static {v0, p2}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$402(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;I)I

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mUi:Lcom/google/android/searchcommon/suggest/SuggestionsUi;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$200(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateView(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)V

    :cond_0
    return-void
.end method

.method public setSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestionsObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/suggest/Suggestions;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    if-eqz v2, :cond_3

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->setSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mHistoryButtonPresenter:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v3, v2}, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->onGotSuggestions(Z)V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestionsObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/suggest/Suggestions;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateViews()V

    goto :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public setSuggestionsViewEnabled(Ljava/lang/Object;Z)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->getViewState(Ljava/lang/Object;)Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    move-result-object v0

    # setter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mEnabled:Z
    invoke-static {v0, p2}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$302(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Z)Z

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mUi:Lcom/google/android/searchcommon/suggest/SuggestionsUi;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$200(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateView(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)V

    :cond_0
    return-void
.end method

.method public setWebSuggestionsEnabled(Z)V
    .locals 3
    .param p1    # Z

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mWebSuggestionsEnabled:Z

    if-eq p1, v2, :cond_0

    iput-boolean p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mWebSuggestionsEnabled:Z

    sget-object v2, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->WEB_SUGGESTIONS:Ljava/lang/Object;

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->getViewState(Ljava/lang/Object;)Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    move-result-object v1

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {v1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateView(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)V

    :cond_0
    return-void
.end method

.method public start()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestionsObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestions;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateViews()V

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mHistoryButtonPresenter:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->onStop()V

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v3, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestionsObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/suggest/Suggestions;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->stopDelayTimer()V

    iput-object v4, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mViews:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    # setter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {v1, v4}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$002(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected updateView(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)V
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;

    const/4 v3, 0x0

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mStarted:Z

    const-string v4, "updateView when not started"

    invoke-static {v2, v4}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mEnabled:Z
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$300(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Z

    move-result v2

    if-eqz v2, :cond_3

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$100(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->getPromoted(Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-direct {p0, v2, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->shouldPublish(Lcom/google/android/searchcommon/suggest/Suggestions;Lcom/google/android/searchcommon/suggest/SuggestionList;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->showSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mShownSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->mIsWebSuggest:Z
    invoke-static {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;->access$500(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mShowingWebRemoveFromHistoryButtons:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-direct {p0, p1, v1, v3, v2}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->showSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;ZZ)V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->NO_SUGGESTIONS:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0, p1, v2, v3, v3}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->showSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl$ViewState;Lcom/google/android/searchcommon/suggest/SuggestionList;ZZ)V

    goto :goto_0
.end method

.method protected updateViews()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mMaxDisplayDelayMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->startDelayTimer(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;->updateViewsNow()V

    return-void
.end method
