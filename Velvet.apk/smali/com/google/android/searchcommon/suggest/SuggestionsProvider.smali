.class public interface abstract Lcom/google/android/searchcommon/suggest/SuggestionsProvider;
.super Ljava/lang/Object;
.source "SuggestionsProvider.java"


# virtual methods
.method public abstract cancelOngoingQuery()V
.end method

.method public abstract getSuggestions(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/google/GoogleSource;Lcom/google/android/searchcommon/summons/IcingSources;)Lcom/google/android/searchcommon/suggest/Suggestions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;",
            "Lcom/google/android/searchcommon/google/GoogleSource;",
            "Lcom/google/android/searchcommon/summons/IcingSources;",
            ")",
            "Lcom/google/android/searchcommon/suggest/Suggestions;"
        }
    .end annotation
.end method
