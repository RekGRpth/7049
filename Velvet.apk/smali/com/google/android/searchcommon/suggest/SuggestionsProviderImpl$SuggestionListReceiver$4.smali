.class Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$4;
.super Ljava/lang/Object;
.source "SuggestionsProviderImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->getWebResultConsumer()Lcom/google/android/searchcommon/util/Consumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Lcom/google/android/searchcommon/suggest/SuggestionList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$4;->this$1:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$4;->this$1:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    # setter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;
    invoke-static {v0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->access$602(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$4;->this$1:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    # invokes: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->handleNewResultAdded(Z)V
    invoke-static {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->access$700(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;Z)V

    return v1
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$4;->consume(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z

    move-result v0

    return v0
.end method
