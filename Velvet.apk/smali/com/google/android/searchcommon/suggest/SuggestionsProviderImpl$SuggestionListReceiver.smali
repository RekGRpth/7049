.class Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;
.super Ljava/lang/Object;
.source "SuggestionsProviderImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuggestionListReceiver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Lcom/google/android/searchcommon/suggest/SuggestionList;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCommitPublishedResultsDelayMillis:J

.field private final mCommitPublishedResultsTask:Ljava/lang/Runnable;

.field private mCountAtWhichToPublishImmediately:I

.field private final mExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

.field private mNumQueriesInProgress:I

.field private mNumQueriesStarted:I

.field private final mPendingResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private final mPublishResultsDelayMillis:J

.field private final mPublishResultsTask:Ljava/lang/Runnable;

.field private final mSingleSourceMode:Z

.field private final mStartNewQueryTask:Ljava/lang/Runnable;

.field private final mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;Lcom/google/android/searchcommon/suggest/Suggestions;IJJZ)V
    .locals 1
    .param p2    # Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;
    .param p3    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p4    # I
    .param p5    # J
    .param p7    # J
    .param p9    # Z

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$1;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPublishResultsTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$2;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$2;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mCommitPublishedResultsTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$3;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$3;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mStartNewQueryTask:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iput p4, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mCountAtWhichToPublishImmediately:I

    iput-wide p5, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPublishResultsDelayMillis:J

    iput-wide p7, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mCommitPublishedResultsDelayMillis:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingResults:Ljava/util/ArrayList;

    iput-boolean p9, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSingleSourceMode:Z

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->publishPendingResults()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->commitPublishedResults()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;I)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->executeNextBatch(I)V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;Lcom/google/android/searchcommon/suggest/SuggestionList;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;Z)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->handleNewResultAdded(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingResults:Ljava/util/ArrayList;

    return-object v0
.end method

.method private commitPublishedResults()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestions;->setTentativeDisplay(Z)V

    return-void
.end method

.method private executeNextBatch(I)V
    .locals 4
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$400(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getMaxConcurrentSourceQueries()I

    move-result v0

    iget v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mNumQueriesInProgress:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mStartNewQueryTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->executeNextBatch(I)I

    move-result p1

    iget v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mNumQueriesInProgress:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mNumQueriesInProgress:I

    iget v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mNumQueriesStarted:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mNumQueriesStarted:I

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSingleSourceMode:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mNumQueriesStarted:I

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getExpectedSourceResultCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mStartNewQueryTask:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v2}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$400(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getNewConcurrentSourceQueryDelay()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    :cond_0
    return-void
.end method

.method private handleNewResultAdded(Z)V
    .locals 4
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mCommitPublishedResultsTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    if-nez p1, :cond_0

    iget-wide v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPublishResultsDelayMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->shouldDelayPublish()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPublishResultsTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPublishResultsTask:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPublishResultsDelayMillis:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPublishResultsTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->publishPendingResults()V

    goto :goto_0
.end method

.method private publishPendingResults()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestions;->setTentativeDisplay(Z)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingResults:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->addResults(Ljava/util/List;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestions;->setTentativeDisplay(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingResults:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mCommitPublishedResultsTask:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mCommitPublishedResultsDelayMillis:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method private shouldDelayPublish()Z
    .locals 6

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingResults:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingWebResult:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v0, v5, v2

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getResultCount()I

    move-result v1

    add-int v2, v0, v1

    iget v5, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mCountAtWhichToPublishImmediately:I

    if-ge v2, v5, :cond_1

    :goto_1
    return v4

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    move v4, v3

    goto :goto_1
.end method


# virtual methods
.method public consume(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mNumQueriesInProgress:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mNumQueriesInProgress:I

    iget-boolean v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSingleSourceMode:Z

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->executeNextBatch(I)V

    :cond_1
    if-nez p1, :cond_2

    const-string v1, "Search.SuggestionsProviderImpl"

    const-string v2, "Source returned a null list."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # invokes: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->updateShouldQueryStrategy(Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    invoke-static {v2, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$500(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mPendingResults:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->handleNewResultAdded(Z)V

    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->consume(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z

    move-result v0

    return v0
.end method

.method public getIcingResultConsumer()Lcom/google/android/searchcommon/util/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$5;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$5;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V

    return-object v0
.end method

.method public getWebResultConsumer()Lcom/google/android/searchcommon/util/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$4;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver$4;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V

    return-object v0
.end method

.method public publishWebResultsImmediately(Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->addResults(Ljava/util/List;Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    return-void
.end method

.method public startFirstBatch()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->mSingleSourceMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->executeNextBatch(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->this$0:Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->access$400(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getConcurrentSourceQueries()I

    move-result v0

    goto :goto_0
.end method
