.class public Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;
.super Ljava/lang/Object;
.source "SuggestionsProviderImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionsProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;
    }
.end annotation


# static fields
.field private static final NO_OP_CONSUMER:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBatchingExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mQueryExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private final mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/searchcommon/util/NoOpConsumer;

    invoke-direct {v0}, Lcom/google/android/searchcommon/util/NoOpConsumer;-><init>()V

    sput-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->NO_OP_CONSUMER:Lcom/google/android/searchcommon/util/Consumer;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;Lcom/google/android/searchcommon/util/NamedTaskExecutor;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;
    .param p3    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .param p4    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mQueryExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    iput-object p4, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;)Lcom/google/android/searchcommon/SearchConfig;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->updateShouldQueryStrategy(Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    return-void
.end method

.method private createGoogleSourceQueryTask(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/google/GoogleSource;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/NamedTask;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/searchcommon/google/GoogleSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/google/GoogleSource;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;)",
            "Lcom/google/android/searchcommon/util/NamedTask;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$1;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$1;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;Lcom/google/android/searchcommon/google/GoogleSource;Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    return-object v0
.end method

.method private createIcingQueryTask(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/summons/IcingSources;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/NamedTask;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/searchcommon/summons/IcingSources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Lcom/google/android/searchcommon/summons/IcingSources;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/suggest/SuggestionList;",
            ">;>;)",
            "Lcom/google/android/searchcommon/util/NamedTask;"
        }
    .end annotation

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$2;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;Lcom/google/android/searchcommon/summons/IcingSources;Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/util/Consumer;)V

    return-object v0
.end method

.method private fetchWebResults(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/google/GoogleSource;Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/searchcommon/google/GoogleSource;
    .param p3    # Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    invoke-interface {p2, p1}, Lcom/google/android/searchcommon/google/GoogleSource;->getCachedSuggestions(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->NO_OP_CONSUMER:Lcom/google/android/searchcommon/util/Consumer;

    if-eqz v0, :cond_1

    if-eqz p3, :cond_0

    invoke-virtual {p3, v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->publishWebResultsImmediately(Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mQueryExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->createGoogleSourceQueryTask(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/google/GoogleSource;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/NamedTask;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    return-void

    :cond_1
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->getWebResultConsumer()Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v1

    goto :goto_0
.end method

.method private filterSource(Lcom/google/android/velvet/Query;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p1    # Lcom/google/android/velvet/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    const/4 v3, 0x0

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/summons/ContentProviderSource;

    invoke-virtual {p0, v1, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->shouldQuerySource(Lcom/google/android/searchcommon/summons/ContentProviderSource;Lcom/google/android/velvet/Query;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private shouldDisplayResults(Lcom/google/android/velvet/Query;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isEmptySuggestQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->showSuggestionsForZeroQuery()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateShouldQueryStrategy(Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->wasRequestMade()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->onZeroResults(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public cancelOngoingQuery()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->cancelPendingTasks()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->cancelRunningTasks()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mQueryExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->cancelPendingTasks()V

    return-void
.end method

.method public getSuggestions(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/google/GoogleSource;Lcom/google/android/searchcommon/summons/IcingSources;)Lcom/google/android/searchcommon/suggest/Suggestions;
    .locals 21
    .param p1    # Lcom/google/android/velvet/Query;
    .param p3    # Lcom/google/android/searchcommon/google/GoogleSource;
    .param p4    # Lcom/google/android/searchcommon/summons/IcingSources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;",
            "Lcom/google/android/searchcommon/google/GoogleSource;",
            "Lcom/google/android/searchcommon/summons/IcingSources;",
            ")",
            "Lcom/google/android/searchcommon/suggest/Suggestions;"
        }
    .end annotation

    invoke-direct/range {p0 .. p2}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->filterSource(Lcom/google/android/velvet/Query;Ljava/util/List;)Ljava/util/List;

    move-result-object v20

    new-instance v18, Ljava/util/ArrayList;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    if-eqz p4, :cond_0

    invoke-interface/range {p4 .. p4}, Lcom/google/android/searchcommon/summons/IcingSources;->getSources()Ljava/util/Collection;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    new-instance v6, Lcom/google/android/searchcommon/suggest/Suggestions;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/searchcommon/suggest/Suggestions;-><init>(Lcom/google/android/velvet/Query;Ljava/util/List;Lcom/google/android/searchcommon/google/GoogleSource;)V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    if-nez p3, :cond_3

    if-eqz p4, :cond_1

    invoke-interface/range {p4 .. p4}, Lcom/google/android/searchcommon/summons/IcingSources;->getSources()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v4

    if-nez v4, :cond_3

    :cond_1
    invoke-virtual {v6}, Lcom/google/android/searchcommon/suggest/Suggestions;->done()V

    :cond_2
    :goto_0
    return-object v6

    :cond_3
    new-instance v4, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mQueryExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-direct {v4, v5}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;-><init>(Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getPublishResultDelayMillis()I

    move-result v4

    int-to-long v8, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getCommitPublishedResultsDelayMillis()I

    move-result v4

    int-to-long v10, v4

    const/4 v3, 0x0

    invoke-direct/range {p0 .. p1}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->shouldDisplayResults(Lcom/google/android/velvet/Query;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v5

    if-eqz p3, :cond_6

    const/4 v4, 0x1

    :goto_1
    add-int v7, v5, v4

    new-instance v3, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->isSingleSourceMode()Z

    move-result v12

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v12}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;Lcom/google/android/searchcommon/suggest/Suggestions;IJJZ)V

    move-object/from16 v16, v3

    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->getMaxResultsPerSource()I

    move-result v15

    if-eqz p3, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->fetchWebResults(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/google/GoogleSource;Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;)V

    :cond_4
    if-eqz p4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mQueryExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->getIcingResultConsumer()Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->createIcingQueryTask(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/summons/IcingSources;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/NamedTask;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    :cond_5
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/searchcommon/summons/ContentProviderSource;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;

    new-instance v12, Lcom/google/android/searchcommon/summons/SourceNamedTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mPublishThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-object/from16 v17, v0

    move-object/from16 v13, p1

    invoke-direct/range {v12 .. v17}, Lcom/google/android/searchcommon/summons/SourceNamedTask;-><init>(Lcom/google/android/velvet/Query;Lcom/google/android/searchcommon/summons/ContentProviderSource;ILcom/google/android/searchcommon/util/Consumer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    invoke-virtual {v4, v12}, Lcom/google/android/searchcommon/util/BatchingNamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    :cond_7
    sget-object v16, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->NO_OP_CONSUMER:Lcom/google/android/searchcommon/util/Consumer;

    invoke-virtual {v6}, Lcom/google/android/searchcommon/suggest/Suggestions;->done()V

    goto :goto_2

    :cond_8
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl$SuggestionListReceiver;->startFirstBatch()V

    goto/16 :goto_0
.end method

.method protected shouldQuerySource(Lcom/google/android/searchcommon/summons/ContentProviderSource;Lcom/google/android/velvet/Query;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/summons/ContentProviderSource;
    .param p2    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/SuggestionsProviderImpl;->mShouldQueryStrategy:Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/searchcommon/summons/ShouldQueryStrategy;->shouldQuerySource(Lcom/google/android/searchcommon/summons/ContentProviderSource;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
