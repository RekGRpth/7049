.class public Lcom/google/android/searchcommon/suggest/WebPromoter;
.super Lcom/google/android/searchcommon/suggest/AbstractPromoter;
.source "WebPromoter.java"


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionFilter;
    .param p2    # Lcom/google/android/searchcommon/suggest/Promoter;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/suggest/AbstractPromoter;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V

    return-void
.end method


# virtual methods
.method public doPickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p2    # I
    .param p3    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .param p4    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebResult()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v3

    if-nez v3, :cond_3

    const/4 v2, 0x0

    :goto_0
    if-eqz v3, :cond_0

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->isFromCache()Z

    move-result v4

    invoke-interface {p3, v4}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setFromCache(Z)V

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->getCount()I

    move-result v4

    if-ge v4, p2, :cond_4

    invoke-interface {v3, v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWebSearchSuggestion()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isNavSuggestion()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    invoke-virtual {p0, v3, v1}, Lcom/google/android/searchcommon/suggest/WebPromoter;->accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p3, v1}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v2

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->areWebResultsDone()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setFinal()V

    :cond_5
    return-void
.end method
