.class public Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;
.super Landroid/database/AbstractCursor;
.source "WebSuggestionsBackedCursor.java"


# instance fields
.field private mExtraColumns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    return-void
.end method

.method private asString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/Object;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private get()Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method private getExtra(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    sget-object v2, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->COLUMNS:[Ljava/lang/String;

    array-length v2, v2

    sub-int v0, p1, v2

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->get()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getExtras()Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->mExtraColumns:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionExtras;->getExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getColumnNames()[Ljava/lang/String;
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getExtraColumns()Ljava/util/Collection;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->COLUMNS:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->mExtraColumns:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->mExtraColumns:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->COLUMNS:[Ljava/lang/String;

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->mSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .locals 3
    .param p1    # I

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :goto_0
    return-wide v1

    :catch_0
    move-exception v0

    const-wide/16 v1, 0x0

    goto :goto_0
.end method

.method public getFloat(I)F
    .locals 2
    .param p1    # I

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 2
    .param p1    # I

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getPosition()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 3
    .param p1    # I

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :goto_0
    return-wide v1

    :catch_0
    move-exception v0

    const-wide/16 v1, 0x0

    goto :goto_0
.end method

.method public getShort(I)S
    .locals 2
    .param p1    # I

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Short;->valueOf(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->get()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    sget-object v2, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->COLUMNS:[Ljava/lang/String;

    array-length v2, v2

    if-ge p1, v2, :cond_0

    packed-switch p1, :pswitch_data_0

    new-instance v1, Landroid/database/CursorIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Requested column "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/searchcommon/summons/CursorSuggestionBuilder;->COLUMNS:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    :pswitch_1
    return-object v1

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->asString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->asString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_4
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2Url()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_5
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_6
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_7
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_8
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentExtraData()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_9
    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_a
    const-string v1, "_-1"

    goto :goto_0

    :pswitch_b
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getExtra(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_1
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public isNull(I)Z
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/suggest/WebSuggestionsBackedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
