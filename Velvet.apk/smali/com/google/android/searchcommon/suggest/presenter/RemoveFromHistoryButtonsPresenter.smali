.class public Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;
.super Ljava/lang/Object;
.source "RemoveFromHistoryButtonsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;
    }
.end annotation


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field protected mShowRemoveFromHistoryButtons:Z

.field private final mShowRemoveFromHistoryTask:Ljava/lang/Runnable;

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mView:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p3    # Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$1;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$1;-><init>(Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mShowRemoveFromHistoryTask:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p3, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mView:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;)Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mView:Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter$HistoryButtonsView;

    return-object v0
.end method


# virtual methods
.method public onGotSuggestions(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getRemoveFromHistoryButtonDisplayDelay()I

    move-result v0

    if-lez v0, :cond_1

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mShowRemoveFromHistoryButtons:Z

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v2, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mShowRemoveFromHistoryTask:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    return-void

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mShowRemoveFromHistoryButtons:Z

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mShowRemoveFromHistoryTask:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public shouldShowRemoveFromHistoryButtons()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/RemoveFromHistoryButtonsPresenter;->mShowRemoveFromHistoryButtons:Z

    return v0
.end method
