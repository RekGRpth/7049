.class Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$2;
.super Ljava/lang/Object;
.source "SuggestionsPresenterImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$2;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$2;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$100(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/suggest/Suggestions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$2;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$100(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/suggest/Suggestions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$2;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mGlobalSearchServices:Lcom/google/android/searchcommon/GlobalSearchServices;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$200(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSuggestionsProvider()Lcom/google/android/searchcommon/suggest/SuggestionsProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/suggest/SuggestionsProvider;->cancelOngoingQuery()V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$2;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mCurrentSuggestions:Lcom/google/android/searchcommon/suggest/Suggestions;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$100(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/suggest/Suggestions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestions;->timedOut()V

    :cond_0
    return-void
.end method
