.class Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$3;
.super Ljava/lang/Object;
.source "SuggestionsPresenterImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/CancellableRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mStopped:Z

.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$3;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized cancel()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$3;->mStopped:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized run()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$3;->mStopped:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$3;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mIcingConnection:Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$300(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/summons/icing/ConnectionToIcing;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
