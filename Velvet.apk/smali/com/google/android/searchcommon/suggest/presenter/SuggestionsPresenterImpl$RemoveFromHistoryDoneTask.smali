.class Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;
.super Ljava/lang/Object;
.source "SuggestionsPresenterImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoveFromHistoryDoneTask"
.end annotation


# instance fields
.field private mFailed:I

.field private mSuccessful:I

.field final synthetic this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mSuccessful:I

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mFailed:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;
    .param p2    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;-><init>(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)V

    return-void
.end method


# virtual methods
.method public addSuggestionRemoved(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mSuccessful:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mSuccessful:I

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mFailed:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mFailed:I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    # getter for: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$800(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->this$0:Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;

    iget v1, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mSuccessful:I

    iget v2, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mFailed:I

    # invokes: Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->removeFromHistoryDone(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;->access$900(Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl;II)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mSuccessful:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenterImpl$RemoveFromHistoryDoneTask;->mFailed:I

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
