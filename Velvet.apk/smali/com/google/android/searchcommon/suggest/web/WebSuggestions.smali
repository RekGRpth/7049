.class public final Lcom/google/android/searchcommon/suggest/web/WebSuggestions;
.super Ljava/lang/Object;
.source "WebSuggestions.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createCorrectionSuggestion(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 2
    .param p0    # Ljava/lang/CharSequence;

    invoke-static {}, Lcom/google/android/searchcommon/suggest/Suggestion;->builder()Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text1(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->suggestionQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isCorrectionSuggestion(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->build()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public static createNavSuggestion(Ljava/lang/String;Ljava/lang/CharSequence;Landroid/net/Uri;ZLandroid/content/Context;)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/net/Uri;
    .param p3    # Z
    .param p4    # Landroid/content/Context;

    invoke-static {}, Lcom/google/android/searchcommon/suggest/Suggestion;->builder()Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text1(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text2Url(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    const v1, 0x7f0200a3

    invoke-static {p4, v1}, Lcom/google/android/searchcommon/util/Util;->getResourceUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/searchcommon/util/Util;->asString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->icon1(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentAction(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentData(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->suggestionQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isHistory(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_NAV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->logType(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->build()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public static createWebSuggestion(Ljava/lang/CharSequence;ZZ)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 1
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # Z
    .param p2    # Z

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/searchcommon/suggest/web/WebSuggestions;->createWebSuggestion(Ljava/lang/CharSequence;ZZLcom/google/android/searchcommon/suggest/SuggestionExtras;)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public static createWebSuggestion(Ljava/lang/CharSequence;ZZLcom/google/android/searchcommon/suggest/SuggestionExtras;)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 3
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # Z
    .param p2    # Z
    .param p3    # Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_SEARCH_HISTORY:Ljava/lang/String;

    :goto_0
    invoke-static {}, Lcom/google/android/searchcommon/suggest/Suggestion;->builder()Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text1(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v1

    const-string v2, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->intentAction(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->suggestionQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->logType(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isHistory(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->shouldPrefetch(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->extras(Lcom/google/android/searchcommon/suggest/SuggestionExtras;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->build()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    return-object v1

    :cond_0
    sget-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_QUERY:Ljava/lang/String;

    goto :goto_0
.end method

.method public static createWordByWordSuggestion(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 1
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/searchcommon/suggest/web/WebSuggestions;->createWordByWordSuggestion(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLcom/google/android/searchcommon/suggest/SuggestionExtras;)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method private static createWordByWordSuggestion(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLcom/google/android/searchcommon/suggest/SuggestionExtras;)Lcom/google/android/searchcommon/suggest/Suggestion;
    .locals 2
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z
    .param p3    # Lcom/google/android/searchcommon/suggest/SuggestionExtras;

    invoke-static {}, Lcom/google/android/searchcommon/suggest/Suggestion;->builder()Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text1(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->text2(Ljava/lang/CharSequence;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->suggestionQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isHistory(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->extras(Lcom/google/android/searchcommon/suggest/SuggestionExtras;)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->isWordByWord(Z)Lcom/google/android/searchcommon/suggest/Suggestion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/suggest/Suggestion$Builder;->build()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public static isNavSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 2
    .param p0    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionSource()Lcom/google/android/searchcommon/summons/Source;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_NAV:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWebSearchSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 2
    .param p0    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionSource()Lcom/google/android/searchcommon/summons/Source;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_QUERY:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_SEARCH_HISTORY:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/searchcommon/google/complete/CompleteServerConstants;->LOG_TYPE_DEVICE_HISTORY:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
