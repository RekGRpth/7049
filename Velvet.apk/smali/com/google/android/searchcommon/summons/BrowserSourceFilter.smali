.class public Lcom/google/android/searchcommon/summons/BrowserSourceFilter;
.super Ljava/lang/Object;
.source "BrowserSourceFilter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionFilter;


# static fields
.field private static final ACCEPTED_URI_SCHEMA:Ljava/util/regex/Pattern;


# instance fields
.field private mAcceptedResults:I

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mMaxResults:I

.field private final mSearchAuthorityPrefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "(?i)((?:http|https|file):\\/\\/|(?:inline|data|about|javascript):)(.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->ACCEPTED_URI_SCHEMA:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0d000d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mSearchAuthorityPrefix:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput p3, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mMaxResults:I

    return-void
.end method

.method private static fixUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;

    const/16 v4, 0x3a

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v0, 0x1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    const-string v4, "http://"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "https://"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_1
    :goto_1
    return-object p0

    :cond_2
    invoke-static {v1}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v4

    and-int/2addr v0, v4

    add-int/lit8 v4, v2, -0x1

    if-ne v3, v4, :cond_3

    if-nez v0, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {p0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    const-string v4, "http:"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "https:"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_5
    const-string v4, "http:/"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "https:/"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    const-string v4, "/"

    const-string v5, "//"

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_7
    const-string v4, ":"

    const-string v5, "://"

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method private isPreviousSearchResultsPage(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mSearchAuthorityPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/SearchConfig;->isGoogleSearchUrlPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isText1Uri(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2Url()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static isUrlLikeByBrowserHeuristics(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->ACCEPTED_URI_SCHEMA:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mAcceptedResults:I

    iget v3, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mMaxResults:I

    if-lt v2, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->isText1Uri(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->isPreviousSearchResultsPage(Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0}, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->isUrlLikeByBrowserHeuristics(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    iget v1, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mAcceptedResults:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/searchcommon/summons/BrowserSourceFilter;->mAcceptedResults:I

    const/4 v1, 0x1

    goto :goto_0
.end method
