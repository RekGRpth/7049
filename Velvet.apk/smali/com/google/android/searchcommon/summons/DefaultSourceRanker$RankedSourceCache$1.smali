.class Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache$1;
.super Ljava/lang/Object;
.source "DefaultSourceRanker.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->create()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache$1;->this$1:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache$1;->consume(Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public consume(Ljava/util/Map;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache$1;->this$1:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->getSources()Ljava/util/Collection;

    move-result-object v3

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/summons/Source;

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache$1;->this$1:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    iget-object v4, v4, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->this$0:Lcom/google/android/searchcommon/summons/DefaultSourceRanker;

    # getter for: Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v4}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->access$300(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/google/android/searchcommon/SearchSettings;->isSourceEnabled(Lcom/google/android/searchcommon/summons/Source;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;

    invoke-direct {v4, p1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$SourceComparator;-><init>(Ljava/util/Map;)V

    invoke-static {v1, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache$1;->this$1:Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;

    # invokes: Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->store(Ljava/lang/Object;)V
    invoke-static {v4, v1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->access$400(Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;Ljava/lang/Object;)V

    const/4 v4, 0x1

    return v4
.end method
