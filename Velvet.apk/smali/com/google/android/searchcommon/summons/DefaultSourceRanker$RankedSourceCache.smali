.class abstract Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;
.super Lcom/google/android/searchcommon/util/CachedLater;
.source "DefaultSourceRanker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/summons/DefaultSourceRanker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "RankedSourceCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/searchcommon/summons/Source;",
        ">",
        "Lcom/google/android/searchcommon/util/CachedLater",
        "<",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/summons/DefaultSourceRanker;


# direct methods
.method private constructor <init>(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->this$0:Lcom/google/android/searchcommon/summons/DefaultSourceRanker;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/CachedLater;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;Lcom/google/android/searchcommon/summons/DefaultSourceRanker$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/summons/DefaultSourceRanker;
    .param p2    # Lcom/google/android/searchcommon/summons/DefaultSourceRanker$1;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;-><init>(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;
    .param p1    # Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->store(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected create()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;->this$0:Lcom/google/android/searchcommon/summons/DefaultSourceRanker;

    # getter for: Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;
    invoke-static {v0}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker;->access$500(Lcom/google/android/searchcommon/summons/DefaultSourceRanker;)Lcom/google/android/searchcommon/clicklog/ClickLog;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache$1;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache$1;-><init>(Lcom/google/android/searchcommon/summons/DefaultSourceRanker$RankedSourceCache;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/clicklog/ClickLog;->getSourceScores(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public abstract getSources()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<+TT;>;"
        }
    .end annotation
.end method
