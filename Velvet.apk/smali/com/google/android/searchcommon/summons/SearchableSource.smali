.class public Lcom/google/android/searchcommon/summons/SearchableSource;
.super Lcom/google/android/searchcommon/summons/AbstractSource;
.source "SearchableSource.java"

# interfaces
.implements Lcom/google/android/searchcommon/summons/ContentProviderSource;


# instance fields
.field private final mActivityInfo:Landroid/content/pm/ActivityInfo;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mContext:Landroid/content/Context;

.field private final mIgnoreIcon1:Z

.field private final mIsTrusted:Z

.field private final mName:Ljava/lang/String;

.field private final mSearchable:Landroid/app/SearchableInfo;

.field private final mSuggestUri:Landroid/net/Uri;

.field private final mSuggestUriString:Ljava/lang/String;

.field private final mSuggestionFilterProvider:Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/Clock;Landroid/app/SearchableInfo;Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Landroid/app/SearchableInfo;
    .param p5    # Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/summons/AbstractSource;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-virtual {p4}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    iput-object p4, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    iput-object p5, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestionFilterProvider:Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-static {v2}, Lcom/google/android/searchcommon/summons/SearchableSource;->getSuggestUriBase(Landroid/app/SearchableInfo;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestUriString:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestUriString:Ljava/lang/String;

    invoke-virtual {p2, v2}, Lcom/google/android/searchcommon/SearchConfig;->isSourceIgnoreIcon1(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mIgnoreIcon1:Z

    invoke-virtual {p2, p0}, Lcom/google/android/searchcommon/SearchConfig;->isSourceTrusted(Lcom/google/android/searchcommon/summons/ContentProviderSource;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mIsTrusted:Z

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private canRead(Landroid/net/Uri;)Z
    .locals 14
    .param p1    # Landroid/net/Uri;

    iget-object v11, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v8

    if-nez v8, :cond_0

    const-string v11, "QSB.SearchableSource"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/SearchableSource;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " has bad suggestion authority "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    :goto_0
    return v11

    :cond_0
    iget-object v9, v8, Landroid/content/pm/ProviderInfo;->readPermission:Ljava/lang/String;

    if-nez v9, :cond_1

    const/4 v11, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v7

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v9, v7, v10}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v11

    if-nez v11, :cond_2

    const/4 v11, 0x1

    goto :goto_0

    :cond_2
    iget-object v4, v8, Landroid/content/pm/ProviderInfo;->pathPermissions:[Landroid/content/pm/PathPermission;

    if-eqz v4, :cond_3

    array-length v11, v4

    if-nez v11, :cond_4

    :cond_3
    const/4 v11, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    move-object v0, v4

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_6

    aget-object v6, v0, v1

    invoke-virtual {v6}, Landroid/content/pm/PathPermission;->getReadPermission()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v6, v3}, Landroid/content/pm/PathPermission;->match(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v5, v7, v10}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v11

    if-nez v11, :cond_5

    const/4 v11, 0x1

    goto :goto_0

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    const/4 v11, 0x0

    goto :goto_0
.end method

.method private getPackageManager()Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method

.method private static getSuggestUriBase(Landroid/app/SearchableInfo;)Landroid/net/Uri;
    .locals 5
    .param p0    # Landroid/app/SearchableInfo;

    const/4 v3, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p0}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "content"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/app/SearchableInfo;->getSuggestPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    const-string v3, "search_suggest_query"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    goto :goto_0
.end method

.method private getSuggestionFilterForQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionFilter;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestionFilterProvider:Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionFilterProvider;->getFilter(Lcom/google/android/searchcommon/summons/ContentProviderSource;Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    move-result-object v0

    return-object v0
.end method

.method private getSuggestions(Landroid/content/Context;Landroid/app/SearchableInfo;Ljava/lang/String;ILandroid/os/CancellationSignal;)Landroid/util/Pair;
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/app/SearchableInfo;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Landroid/os/CancellationSignal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/SearchableInfo;",
            "Ljava/lang/String;",
            "I",
            "Landroid/os/CancellationSignal;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/database/Cursor;",
            "Landroid/content/ContentProviderClient;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {p2}, Lcom/google/android/searchcommon/summons/SearchableSource;->getSuggestUriBase(Landroid/app/SearchableInfo;)Landroid/net/Uri;

    move-result-object v7

    if-nez v7, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v10

    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSuggestSelection()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v4, v2

    :goto_1
    const-string v2, "limit"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/ContentResolver;->acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object/from16 v6, p5

    :try_start_0
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    invoke-static {v8, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_0

    :cond_1
    invoke-virtual {v10, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    :catch_0
    move-exception v9

    invoke-virtual/range {p5 .. p5}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_2

    throw v9

    :cond_2
    const/4 v2, 0x0

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method protected getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method public getDefaultIntentAction()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestIntentAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "android.intent.action.SEARCH"

    goto :goto_0
.end method

.method public getDefaultIntentData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestIntentData()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIconPackage()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIntentComponent()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public getLabel()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/SearchableSource;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryThreshold()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestThreshold()I

    move-result v0

    return v0
.end method

.method public getSettingsDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSettingsDescriptionId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/summons/SearchableSource;->getTextFromResource(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected getSourceIconResource()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v0}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v0

    return v0
.end method

.method protected getSourceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestUriString:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSuggestUriString:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestions(Lcom/google/android/velvet/Query;ILandroid/os/CancellationSignal;)Lcom/google/android/searchcommon/suggest/SuggestionList;
    .locals 13
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # I
    .param p3    # Landroid/os/CancellationSignal;

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    new-instance v8, Lcom/google/android/searchcommon/util/Latency;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-direct {v8, v0}, Lcom/google/android/searchcommon/util/Latency;-><init>(Lcom/google/android/searchcommon/util/Clock;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    move-object v0, p0

    move v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/summons/SearchableSource;->getSuggestions(Landroid/content/Context;Landroid/app/SearchableInfo;Ljava/lang/String;ILandroid/os/CancellationSignal;)Landroid/util/Pair;

    move-result-object v10

    iget-object v6, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Landroid/database/Cursor;

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v12

    :goto_0
    new-instance v0, Lcom/google/android/searchcommon/summons/SourceResultBuilder;

    invoke-direct {p0, v3}, Lcom/google/android/searchcommon/summons/SearchableSource;->getSuggestionFilterForQuery(Ljava/lang/String;)Lcom/google/android/searchcommon/suggest/SuggestionFilter;

    move-result-object v1

    invoke-direct {v0, p0, p1, v6, v1}, Lcom/google/android/searchcommon/summons/SourceResultBuilder;-><init>(Lcom/google/android/searchcommon/summons/ContentProviderSource;Lcom/google/android/velvet/Query;Landroid/database/Cursor;Lcom/google/android/searchcommon/suggest/SuggestionFilter;)V

    invoke-virtual {v0, p2}, Lcom/google/android/searchcommon/summons/SourceResultBuilder;->build(I)Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v11

    invoke-virtual {v8}, Lcom/google/android/searchcommon/util/Latency;->getLatency()I

    move-result v0

    invoke-interface {v11, v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->setLatency(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v10, :cond_0

    iget-object v0, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_0
    :goto_1
    return-object v11

    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    :catch_0
    move-exception v7

    :try_start_1
    const-string v0, "QSB.SearchableSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/SearchableSource;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v9, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/SearchableSource;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;-><init>(Ljava/lang/String;Lcom/google/android/velvet/Query;)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Lcom/google/android/searchcommon/suggest/SuggestionListImpl;->setRequestFailed(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v10, :cond_2

    iget-object v0, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_2
    move-object v11, v9

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v0

    if-eqz v10, :cond_3

    iget-object v0, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_3
    throw v1
.end method

.method public isContactsSource()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "com.android.contacts"

    iget-object v1, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isIgnoreIcon1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mIgnoreIcon1:Z

    return v0
.end method

.method public isReadable()Z
    .locals 6

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v4}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "content"

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v4}, Landroid/app/SearchableInfo;->getSuggestPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    const-string v4, "search_suggest_query"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/searchcommon/summons/SearchableSource;->canRead(Landroid/net/Uri;)Z

    move-result v4

    goto :goto_0
.end method

.method public isTrusted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mIsTrusted:Z

    return v0
.end method

.method public queryAfterZeroResults()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/SearchableSource;->mSearchable:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->queryAfterZeroResults()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SearchableSource["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/summons/SearchableSource;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
