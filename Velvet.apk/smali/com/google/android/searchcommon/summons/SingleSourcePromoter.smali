.class public Lcom/google/android/searchcommon/summons/SingleSourcePromoter;
.super Lcom/google/android/searchcommon/suggest/AbstractPromoter;
.source "SingleSourcePromoter.java"


# instance fields
.field private final mSource:Lcom/google/android/searchcommon/summons/Source;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/summons/Source;Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/suggest/Promoter;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/summons/Source;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionFilter;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Lcom/google/android/searchcommon/suggest/Promoter;

    invoke-direct {p0, p2, p4, p3}, Lcom/google/android/searchcommon/suggest/AbstractPromoter;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/SingleSourcePromoter;->mSource:Lcom/google/android/searchcommon/summons/Source;

    return-void
.end method


# virtual methods
.method public declared-synchronized doPickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p2    # I
    .param p3    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .param p4    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getSourceResults()Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/searchcommon/summons/SingleSourcePromoter;->mSource:Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v5}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v2

    :cond_1
    if-eqz v1, :cond_6

    invoke-interface {v1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->getCount()I

    move-result v4

    if-lt v4, p2, :cond_5

    :cond_3
    invoke-interface {v1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v4

    invoke-interface {p3, v4}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setSourceSuggestionCount(I)V

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setFinal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :goto_1
    monitor-exit p0

    return-void

    :cond_5
    :try_start_1
    invoke-virtual {p0, v1, v3}, Lcom/google/android/searchcommon/summons/SingleSourcePromoter;->accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p3, v3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_6
    :try_start_2
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->areSourcesDone()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x0

    invoke-interface {p3, v4}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setSourceSuggestionCount(I)V

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setFinal()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
