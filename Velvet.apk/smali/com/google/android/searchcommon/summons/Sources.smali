.class public interface abstract Lcom/google/android/searchcommon/summons/Sources;
.super Ljava/lang/Object;
.source "Sources.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/ObservableDataSet;


# virtual methods
.method public abstract getContentProviderSource(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/ContentProviderSource;
.end method

.method public abstract getContentProviderSourceIfExists(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/ContentProviderSource;
.end method

.method public abstract getContentProviderSources()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/searchcommon/summons/ContentProviderSource;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSource(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/Source;
.end method

.method public abstract getSourceIfExists(Ljava/lang/String;)Lcom/google/android/searchcommon/summons/Source;
.end method

.method public abstract getSources()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<+",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;"
        }
    .end annotation
.end method
