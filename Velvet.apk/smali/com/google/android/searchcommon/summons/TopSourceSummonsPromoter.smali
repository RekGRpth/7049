.class public Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;
.super Lcom/google/android/searchcommon/suggest/AbstractPromoter;
.source "TopSourceSummonsPromoter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;
    }
.end annotation


# instance fields
.field private mChosenResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private final mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

.field private final mOrderedSourceStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;",
            ">;"
        }
    .end annotation
.end field

.field private mResultsChosenForQuery:Lcom/google/android/velvet/Query;

.field private final mSourceStates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;",
            ">;"
        }
    .end annotation
.end field

.field private final mSources:Lcom/google/android/searchcommon/summons/Sources;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/summons/Sources;Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/clicklog/ClickLog;Landroid/database/DataSetObservable;Lcom/google/android/searchcommon/suggest/Promoter;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/summons/Sources;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionFilter;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Lcom/google/android/searchcommon/clicklog/ClickLog;
    .param p5    # Landroid/database/DataSetObservable;
    .param p6    # Lcom/google/android/searchcommon/suggest/Promoter;

    invoke-direct {p0, p2, p6, p3}, Lcom/google/android/searchcommon/suggest/AbstractPromoter;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    iput-object p4, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mSourceStates:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mOrderedSourceStates:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    new-instance v1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$1;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$1;-><init>(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/summons/Sources;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->updateSourceStates()V

    if-eqz p5, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$2;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$2;-><init>(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)V

    invoke-virtual {p5, v0}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->updateSourceScores()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->updateSourceStates()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    invoke-direct {p0}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->updateSourceScores()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mOrderedSourceStates:Ljava/util/List;

    return-object v0
.end method

.method private shouldPromoteTentatively(Lcom/google/android/searchcommon/suggest/Suggestions;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->shouldDisplayTentatively()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->areSourcesDone()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private updateSourceScores()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mClickLog:Lcom/google/android/searchcommon/clicklog/ClickLog;

    new-instance v1, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$3;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$3;-><init>(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/clicklog/ClickLog;->getSourceScores(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method private declared-synchronized updateSourceStates()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mSourceStates:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mOrderedSourceStates:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mSources:Lcom/google/android/searchcommon/summons/Sources;

    invoke-interface {v4}, Lcom/google/android/searchcommon/summons/Sources;->getSources()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/summons/Source;

    invoke-interface {v2}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;

    invoke-direct {v3, p0, v1}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;-><init>(Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mSourceStates:Ljava/util/Map;

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mOrderedSourceStates:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized doPickPromoted(Lcom/google/android/searchcommon/suggest/Suggestions;ILcom/google/android/searchcommon/suggest/MutableSuggestionList;Lcom/google/android/searchcommon/suggest/SuggestionList;)V
    .locals 9
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;
    .param p2    # I
    .param p3    # Lcom/google/android/searchcommon/suggest/MutableSuggestionList;
    .param p4    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mChosenResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v7, :cond_0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mResultsChosenForQuery:Lcom/google/android/velvet/Query;

    invoke-static {v7, v8}, Lcom/google/android/velvet/Query;->equivalentForSuggest(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_0
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mChosenResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getExpectedSources()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/searchcommon/summons/Source;

    iget-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mSourceStates:Ljava/util/Map;

    invoke-interface {v4}, Lcom/google/android/searchcommon/summons/Source;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;

    invoke-virtual {v7}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->expectResults()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getSourceResults()Ljava/lang/Iterable;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mSourceStates:Ljava/util/Map;

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getSourceName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;

    invoke-virtual {v7, v3}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->setResults(Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->shouldPromoteTentatively(Lcom/google/android/searchcommon/suggest/Suggestions;)Z

    move-result v2

    const/4 v1, -0x1

    const/4 v3, 0x0

    iget-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mOrderedSourceStates:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->stillWaiting()Z

    move-result v7

    if-eqz v7, :cond_4

    if-eqz v2, :cond_4

    iget v1, v5, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mScore:I

    goto :goto_2

    :cond_4
    invoke-virtual {v5}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->haveResults()Z

    move-result v7

    if-eqz v7, :cond_3

    iget v7, v5, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mScore:I

    if-lt v7, v1, :cond_3

    iget-object v7, v5, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->mResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mChosenResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mResultsChosenForQuery:Lcom/google/android/velvet/Query;

    :cond_5
    iget-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mOrderedSourceStates:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;

    invoke-virtual {v5}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter$SourceState;->resetState()V

    goto :goto_3

    :cond_6
    iget-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mChosenResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mChosenResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-interface {v7}, Lcom/google/android/searchcommon/suggest/SuggestionList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_7
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->getCount()I

    move-result v7

    if-lt v7, p2, :cond_a

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestions;->areSourcesDone()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {p3}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->setFinal()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_9
    monitor-exit p0

    return-void

    :cond_a
    :try_start_2
    iget-object v7, p0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->mChosenResults:Lcom/google/android/searchcommon/suggest/SuggestionList;

    invoke-virtual {p0, v7, v6}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;->accept(Lcom/google/android/searchcommon/suggest/SuggestionList;Lcom/google/android/searchcommon/suggest/Suggestion;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {p3, v6}, Lcom/google/android/searchcommon/suggest/MutableSuggestionList;->add(Lcom/google/android/searchcommon/suggest/Suggestion;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method
