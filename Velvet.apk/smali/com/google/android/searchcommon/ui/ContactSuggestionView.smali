.class public Lcom/google/android/searchcommon/ui/ContactSuggestionView;
.super Lcom/google/android/searchcommon/ui/DefaultSuggestionView;
.source "ContactSuggestionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/ui/ContactSuggestionView$1;,
        Lcom/google/android/searchcommon/ui/ContactSuggestionView$ContactBadgeClickListener;
    }
.end annotation


# static fields
.field private static final SCHEMA_CONTACTS:Ljava/lang/String;


# instance fields
.field private mQuickContact:Lcom/google/android/searchcommon/ui/ContactBadge;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->SCHEMA_CONTACTS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v4, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "tel"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->mQuickContact:Lcom/google/android/searchcommon/ui/ContactBadge;

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/google/android/searchcommon/ui/ContactBadge;->assignContactFromPhone(Ljava/lang/String;Z)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->mQuickContact:Lcom/google/android/searchcommon/ui/ContactBadge;

    new-instance v3, Lcom/google/android/searchcommon/ui/ContactSuggestionView$ContactBadgeClickListener;

    invoke-direct {v3, p0, v4}, Lcom/google/android/searchcommon/ui/ContactSuggestionView$ContactBadgeClickListener;-><init>(Lcom/google/android/searchcommon/ui/ContactSuggestionView;Lcom/google/android/searchcommon/ui/ContactSuggestionView$1;)V

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/ui/ContactBadge;->setExtraOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    const-string v2, "mailto"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->mQuickContact:Lcom/google/android/searchcommon/ui/ContactBadge;

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/google/android/searchcommon/ui/ContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    goto :goto_1

    :cond_2
    sget-object v2, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->SCHEMA_CONTACTS:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "com.android.contacts"

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->mQuickContact:Lcom/google/android/searchcommon/ui/ContactBadge;

    invoke-virtual {v2, v0}, Lcom/google/android/searchcommon/ui/ContactBadge;->assignContactUri(Landroid/net/Uri;)V

    goto :goto_1

    :cond_3
    const-string v2, "ContactSuggestionView"

    const-string v3, "Unsupported URI: "

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->mQuickContact:Lcom/google/android/searchcommon/ui/ContactBadge;

    invoke-virtual {v2, v4}, Lcom/google/android/searchcommon/ui/ContactBadge;->assignContactUri(Landroid/net/Uri;)V

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->onFinishInflate()V

    const v0, 0x7f10006f

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/ui/ContactBadge;

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/ContactSuggestionView;->mQuickContact:Lcom/google/android/searchcommon/ui/ContactBadge;

    return-void
.end method
