.class public Lcom/google/android/searchcommon/ui/DefaultSuggestionView;
.super Lcom/google/android/searchcommon/ui/BaseSuggestionView;
.source "DefaultSuggestionView.java"


# instance fields
.field private mAsyncIcon1:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

.field private mAsyncIcon2:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

.field private mFrameIcon1:Landroid/view/View;

.field private mFrameIconPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private formatUrl(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 5
    .param p1    # Ljava/lang/CharSequence;

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0036

    invoke-direct {v1, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method private setIcon2(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/summons/Source;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mAsyncIcon2:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->set(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z
    .locals 9
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText1()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionSource()Lcom/google/android/searchcommon/summons/Source;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/searchcommon/summons/Source;->isShowSingleLine()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2Url()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-direct {p0, v3}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->formatUrl(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    :cond_2
    :goto_1
    const v4, 0x7f020004

    invoke-virtual {p0, v4}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->setBackgroundResource(I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->setPadding(IIII)V

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    const v5, 0x7f090015

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    if-eqz v1, :cond_5

    invoke-interface {v1}, Lcom/google/android/searchcommon/summons/Source;->hasFullSizeIcon()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    :cond_3
    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mText1:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mText1:Landroid/widget/TextView;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mText1:Landroid/widget/TextView;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :goto_3
    invoke-virtual {p0, v2}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->setText1(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v3}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->setText2(Ljava/lang/CharSequence;)V

    if-nez v1, :cond_7

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mAsyncIcon1:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->set(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon1:Landroid/widget/ImageView;

    const-string v5, " "

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mAsyncIcon2:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->set(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V

    :goto_4
    const/4 v4, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionText2()Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    iget v5, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIconPadding:I

    iget v6, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIconPadding:I

    iget v7, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIconPadding:I

    iget v8, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIconPadding:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_2

    :cond_6
    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mText1:Landroid/widget/TextView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mText1:Landroid/widget/TextView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mText1:Landroid/widget/TextView;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_3

    :cond_7
    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon1:Landroid/widget/ImageView;

    invoke-interface {v1}, Lcom/google/android/searchcommon/summons/Source;->getLabel()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mAsyncIcon1:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    iget v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon1Mode:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    const/4 v4, 0x0

    :goto_5
    iget v5, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon1Mode:I

    if-nez v5, :cond_a

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v5

    :goto_6
    invoke-virtual {v6, v4, v5}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->set(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon2Mode:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_8

    const/4 v1, 0x0

    :cond_8
    iget v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon2Mode:I

    if-nez v4, :cond_b

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v4

    :goto_7
    invoke-direct {p0, v1, v4}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->setIcon2(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon1:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget v4, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon1Mode:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_c

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_9
    move-object v4, v1

    goto :goto_5

    :cond_a
    const/4 v5, 0x0

    goto :goto_6

    :cond_b
    const/4 v4, 0x0

    goto :goto_7

    :cond_c
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->onFinishInflate()V

    const v1, 0x7f100153

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    new-instance v1, Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon1:Landroid/widget/ImageView;

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;-><init>(Landroid/widget/ImageView;I)V

    iput-object v1, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mAsyncIcon1:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    new-instance v1, Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mIcon2:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;-><init>(Landroid/widget/ImageView;I)V

    iput-object v1, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mAsyncIcon2:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIconPadding:I

    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/ui/BaseSuggestionView;->setEnabled(Z)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/DefaultSuggestionView;->mFrameIcon1:Landroid/view/View;

    const/high16 v1, 0x3f000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method
