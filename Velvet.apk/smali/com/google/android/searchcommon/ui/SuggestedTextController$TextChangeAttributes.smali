.class Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;
.super Ljava/lang/Object;
.source "SuggestedTextController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/ui/SuggestedTextController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TextChangeAttributes"
.end annotation


# instance fields
.field public final mCountAfter:I

.field public final mCountBefore:I

.field private mHandled:Z

.field public final mPos:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->mPos:I

    iput p2, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->mCountAfter:I

    iput p3, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->mCountBefore:I

    return-void
.end method


# virtual methods
.method public isHandled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->mHandled:Z

    return v0
.end method

.method public setHandled()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->mHandled:Z

    return-void
.end method
