.class public Lcom/google/android/searchcommon/ui/SuggestedTextController;
.super Ljava/lang/Object;
.source "SuggestedTextController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;,
        Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;,
        Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;,
        Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;,
        Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;,
        Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "SuggestedTextController"


# instance fields
.field private final mBufferSpanWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;

.field private final mBufferTextWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;

.field private mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

.field private final mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

.field private mSuggestedText:Ljava/lang/String;

.field private final mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

.field private mTextSelectionBeforeIgnoringChanges:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;

.field private final mTextWatchers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/searchcommon/util/TextChangeWatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserEntered:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Landroid/widget/EditText;I)V
    .locals 1
    .param p1    # Landroid/widget/EditText;
    .param p2    # I

    new-instance v0, Lcom/google/android/searchcommon/ui/SuggestedTextController$1;

    invoke-direct {v0, p1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$1;-><init>(Landroid/widget/EditText;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/searchcommon/ui/SuggestedTextController;-><init>(Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;I)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;I)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;
    .param p2    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;-><init>(Lcom/google/android/searchcommon/ui/SuggestedTextController;Lcom/google/android/searchcommon/ui/SuggestedTextController$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mBufferTextWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;

    new-instance v0, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;

    invoke-direct {v0, p0, v1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;-><init>(Lcom/google/android/searchcommon/ui/SuggestedTextController;Lcom/google/android/searchcommon/ui/SuggestedTextController$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mBufferSpanWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    new-instance v0, Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-direct {v0, p2}, Lcom/google/android/searchcommon/ui/SuggestedSpan;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextWatchers:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v2, v2, v1}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->initialize(Ljava/lang/String;IILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/searchcommon/ui/SuggestedTextController;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/ui/SuggestedTextController;

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->assertNotIgnoringSelectionChanges()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/searchcommon/ui/SuggestedTextController;Landroid/text/Editable;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/ui/SuggestedTextController;
    .param p1    # Landroid/text/Editable;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->handleTextChanged(Landroid/text/Editable;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/searchcommon/ui/SuggestedTextController;)Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/ui/SuggestedTextController;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/searchcommon/ui/SuggestedTextController;Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;)Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/ui/SuggestedTextController;
    .param p1    # Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/searchcommon/ui/SuggestedTextController;)Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/ui/SuggestedTextController;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextSelectionBeforeIgnoringChanges:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/searchcommon/ui/SuggestedTextController;)Ljava/lang/StringBuffer;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/ui/SuggestedTextController;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/searchcommon/ui/SuggestedTextController;)Lcom/google/android/searchcommon/ui/SuggestedSpan;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/ui/SuggestedTextController;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    return-object v0
.end method

.method private assertNotIgnoringSelectionChanges()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextSelectionBeforeIgnoringChanges:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Illegal operation while cursor movement processing suspended"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private handleTextChanged(Landroid/text/Editable;)V
    .locals 11
    .param p1    # Landroid/text/Editable;

    const/4 v10, -0x1

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    invoke-virtual {v7}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->isHandled()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    invoke-virtual {v7}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->setHandled()V

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    iget v5, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->mPos:I

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    iget v1, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->mCountBefore:I

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    iget v0, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;->mCountAfter:I

    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    add-int v8, v5, v1

    add-int v9, v5, v0

    invoke-interface {p1, v5, v9}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v5, v8, v9}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {p1, v7}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    if-eq v7, v10, :cond_5

    const/4 v3, 0x1

    :goto_1
    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    if-eqz v3, :cond_7

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    if-le v0, v1, :cond_6

    sub-int v4, v0, v1

    add-int v7, v5, v4

    add-int v8, v5, v4

    add-int/2addr v8, v4

    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    :cond_2
    :goto_2
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v7

    if-ne v6, v7, :cond_8

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {p1, v7}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    :cond_3
    :goto_3
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    if-gtz v1, :cond_4

    if-lez v0, :cond_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->notifyUserEnteredChanged()V

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    :cond_6
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1, v6, v7, v8}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    if-nez v1, :cond_2

    invoke-static {p1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    goto :goto_2

    :cond_7
    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v6, v7}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    if-ne v2, v6, :cond_2

    invoke-static {p1, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    goto :goto_2

    :cond_8
    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v8

    const/16 v9, 0x21

    invoke-interface {p1, v7, v6, v8, v9}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3

    :cond_9
    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {p1, v7}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    if-eq v7, v10, :cond_3

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {p1, v7}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    iget-object v7, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v8

    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_3
.end method

.method private initialize(Ljava/lang/String;IILjava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    const/4 v4, -0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    invoke-interface {v3}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;->getText()Landroid/text/Editable;

    move-result-object v2

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    move-object v0, p1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz p4, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, p4

    :cond_1
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-interface {v2, v6, v3, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    if-eq p2, v4, :cond_2

    if-eq p3, v4, :cond_2

    invoke-static {v2, p2, p3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    invoke-virtual {v3, v6, v4, p1}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    iput-object p4, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v3

    if-ge v1, v3, :cond_3

    iget-object v3, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-interface {v2, v3, v1, v4, v5}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mBufferSpanWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v4

    const/16 v5, 0x12

    invoke-interface {v2, v3, v6, v4, v5}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    iget-object v3, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mBufferTextWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void

    :cond_3
    iget-object v3, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {v2, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private notifyUserEnteredChanged()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextWatchers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/util/TextChangeWatcher;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/TextChangeWatcher;->onTextChanged(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addUserTextChangeWatcher(Lcom/google/android/searchcommon/util/TextChangeWatcher;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/TextChangeWatcher;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextWatchers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method checkInvariant(Landroid/text/Spannable;)V
    .locals 10
    .param p1    # Landroid/text/Spannable;

    const/4 v6, 0x1

    const/4 v8, -0x1

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {p1, v5}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    iget-object v5, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggested:Lcom/google/android/searchcommon/ui/SuggestedSpan;

    invoke-interface {p1, v5}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    if-eq v3, v8, :cond_0

    if-ne v1, v8, :cond_1

    :cond_0
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v1

    move v3, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->getUserText()Ljava/lang/String;

    move-result-object v4

    sub-int v2, v1, v3

    const-string v5, "Sum of user and suggested text lengths doesn\'t match total length"

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v8

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v2

    invoke-static {v5, v8, v9}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/String;II)V

    const-string v5, "End of user entered text doesn\'t match start of suggested"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v5, v3, v8}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/String;II)V

    const-string v5, "user entered text does not match start of buffer"

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1, v7, v3}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    invoke-static {v5, v8}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    iget-object v5, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    if-eqz v5, :cond_2

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v5

    if-ge v3, v5, :cond_2

    const-string v5, "User entered is not a prefix of suggested"

    iget-object v8, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    invoke-static {v5, v8}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    const-string v5, "Suggested text does not match buffer contents"

    iget-object v8, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    invoke-static {v5, v8}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    :cond_2
    iget-object v5, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    if-nez v5, :cond_3

    const-string v5, "Non-zero suggention length with null suggestion"

    invoke-static {v5, v7, v2}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/String;II)V

    :goto_0
    const-string v5, "Cursor within suggested part"

    if-gt v0, v3, :cond_5

    :goto_1
    invoke-static {v5, v6}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    return-void

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Suggestion text longer than suggestion ("

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ">"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ")"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v5, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-gt v2, v5, :cond_4

    move v5, v6

    :goto_2
    invoke-static {v8, v5}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    move v5, v7

    goto :goto_2

    :cond_5
    move v6, v7

    goto :goto_1
.end method

.method public getUserText()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->assertNotIgnoringSelectionChanges()V

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mUserEntered:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onFocus()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextWatchers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/util/TextChangeWatcher;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/TextChangeWatcher;->onTextEditStarted()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public restoreInstanceState(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 5
    .param p1    # Landroid/os/Parcelable;

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->assertNotIgnoringSelectionChanges()V

    instance-of v1, p1, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;

    if-nez v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    invoke-interface {v1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mBufferSpanWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;

    invoke-interface {v1, v2}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mBufferTextWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferTextWatcher;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, v0, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->mUserText:Ljava/lang/String;

    iget v2, v0, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->mSelStart:I

    iget v3, v0, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->mSelEnd:I

    iget-object v4, v0, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->mSuggestedText:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->initialize(Ljava/lang/String;IILjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->notifyUserEnteredChanged()V

    invoke-virtual {v0}, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    goto :goto_0
.end method

.method public resumeCursorMovementHandlingAndApplyChanges()V
    .locals 9

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    invoke-interface {v0}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v8, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextSelectionBeforeIgnoringChanges:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextSelectionBeforeIgnoringChanges:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;

    new-instance v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;

    invoke-direct {v7, v1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;-><init>(Ljava/lang/CharSequence;)V

    iget v0, v8, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mStart:I

    iget v2, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mStart:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mBufferSpanWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;

    sget-object v2, Landroid/text/Selection;->SELECTION_START:Ljava/lang/Object;

    iget v3, v8, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mStart:I

    iget v4, v8, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mStart:I

    iget v5, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mStart:I

    iget v6, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mStart:I

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;->onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V

    :cond_0
    iget v0, v8, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mEnd:I

    iget v2, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mEnd:I

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mBufferSpanWatcher:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;

    sget-object v2, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    iget v3, v8, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mEnd:I

    iget v4, v8, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mEnd:I

    iget v5, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mEnd:I

    iget v6, v7, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;->mEnd:I

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSpanWatcher;->onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V

    :cond_1
    return-void
.end method

.method public saveInstanceState(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 3
    .param p1    # Landroid/os/Parcelable;

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->assertNotIgnoringSelectionChanges()V

    new-instance v1, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;

    invoke-direct {v1, p1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    invoke-interface {v2}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->getUserText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->mUserText:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->mSuggestedText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, v1, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->mSelStart:I

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, v1, Lcom/google/android/searchcommon/ui/SuggestedTextController$SavedState;->mSelEnd:I

    return-object v1
.end method

.method public setSuggestedText(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->assertNotIgnoringSelectionChanges()V

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mSuggestedText:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    invoke-direct {v1, v2, v2, v2}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;-><init>(III)V

    iput-object v1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mCurrentTextChange:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextChangeAttributes;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    invoke-interface {v1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->handleTextChanged(Landroid/text/Editable;)V

    :cond_0
    return-void
.end method

.method public suspendCursorMovementHandling()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/SuggestedTextController;->assertNotIgnoringSelectionChanges()V

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextOwner:Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;

    invoke-interface {v1}, Lcom/google/android/searchcommon/ui/SuggestedTextController$TextOwner;->getText()Landroid/text/Editable;

    move-result-object v0

    new-instance v1, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;

    invoke-direct {v1, v0}, Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;-><init>(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/google/android/searchcommon/ui/SuggestedTextController;->mTextSelectionBeforeIgnoringChanges:Lcom/google/android/searchcommon/ui/SuggestedTextController$BufferSelection;

    return-void
.end method
