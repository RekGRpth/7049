.class Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;
.super Ljava/lang/Object;
.source "AsyncIcon.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/ui/util/AsyncIcon;->set(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

.field final synthetic val$source:Lcom/google/android/searchcommon/summons/Source;

.field final synthetic val$uniqueIconId:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/ui/util/AsyncIcon;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->this$0:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    iput-object p2, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->val$uniqueIconId:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->val$source:Lcom/google/android/searchcommon/summons/Source;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Landroid/graphics/drawable/Drawable;)Z
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->val$uniqueIconId:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->this$0:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    # getter for: Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mWantedId:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->access$000(Lcom/google/android/searchcommon/ui/util/AsyncIcon;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->this$0:Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    iget-object v1, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->val$uniqueIconId:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->val$source:Lcom/google/android/searchcommon/summons/Source;

    # invokes: Lcom/google/android/searchcommon/ui/util/AsyncIcon;->handleNewDrawable(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->access$100(Lcom/google/android/searchcommon/ui/util/AsyncIcon;Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;->consume(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method
