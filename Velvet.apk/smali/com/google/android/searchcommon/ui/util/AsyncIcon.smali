.class public Lcom/google/android/searchcommon/ui/util/AsyncIcon;
.super Ljava/lang/Object;
.source "AsyncIcon.java"


# instance fields
.field private mCurrentId:Landroid/net/Uri;

.field private final mFallbackToSourceIcon:Z

.field private final mLoadLargeImage:Z

.field private final mMaybeOverlaySourceIcon:Z

.field private final mReserveSpace:Z

.field private final mView:Landroid/widget/ImageView;

.field private mWantedId:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;I)V
    .locals 3
    .param p1    # Landroid/widget/ImageView;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mView:Landroid/widget/ImageView;

    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mFallbackToSourceIcon:Z

    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mReserveSpace:Z

    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mMaybeOverlaySourceIcon:Z

    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mLoadLargeImage:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/ui/util/AsyncIcon;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/ui/util/AsyncIcon;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mWantedId:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/searchcommon/ui/util/AsyncIcon;Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/ui/util/AsyncIcon;
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/net/Uri;
    .param p3    # Lcom/google/android/searchcommon/summons/Source;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->handleNewDrawable(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V

    return-void
.end method

.method private clearDrawable()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mCurrentId:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private getGSS()Lcom/google/android/searchcommon/GlobalSearchServices;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v0

    return-object v0
.end method

.method private handleNewDrawable(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V
    .locals 7
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/net/Uri;
    .param p3    # Lcom/google/android/searchcommon/summons/Source;

    const/4 v6, 0x1

    const/high16 v5, 0x3f000000

    if-nez p1, :cond_2

    if-eqz p3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mFallbackToSourceIcon:Z

    if-eqz v3, :cond_2

    invoke-interface {p3}, Lcom/google/android/searchcommon/summons/Source;->getSourceIconUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {p3}, Lcom/google/android/searchcommon/summons/Source;->getSourceIconUri()Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mWantedId:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mWantedId:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mCurrentId:Landroid/net/Uri;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p3}, Lcom/google/android/searchcommon/summons/Source;->getSourceIcon()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->setDrawable(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_1

    if-eqz p3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mMaybeOverlaySourceIcon:Z

    if-eqz v3, :cond_1

    invoke-interface {p3}, Lcom/google/android/searchcommon/summons/Source;->isTrusted()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {p3}, Lcom/google/android/searchcommon/summons/Source;->getSourceIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p3}, Lcom/google/android/searchcommon/summons/Source;->getSourceIconUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v1, Landroid/graphics/drawable/ScaleDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0x55

    invoke-direct {v1, v3, v4, v5, v5}, Landroid/graphics/drawable/ScaleDrawable;-><init>(Landroid/graphics/drawable/Drawable;IFF)V

    invoke-virtual {v1, v6}, Landroid/graphics/drawable/ScaleDrawable;->setLevel(I)Z

    const/16 v3, 0xe0

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/ScaleDrawable;->setAlpha(I)V

    instance-of v3, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_3

    move-object v3, p1

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    :cond_3
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    aput-object v1, v3, v6

    invoke-direct {v0, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object p1, v0

    goto :goto_1
.end method

.method private setDrawable(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mCurrentId:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mView:Landroid/widget/ImageView;

    iget-boolean v1, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mReserveSpace:Z

    invoke-static {v0, p1, v1}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->setViewDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method private static setViewDrawable(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;Z)V
    .locals 2
    .param p0    # Landroid/widget/ImageView;
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Z

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    goto :goto_1
.end method


# virtual methods
.method public set(Lcom/google/android/searchcommon/summons/Source;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/summons/Source;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz p2, :cond_5

    if-nez p1, :cond_1

    invoke-static {p2}, Lcom/google/android/searchcommon/util/Util;->parseUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mWantedId:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mWantedId:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mCurrentId:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->getGSS()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/searchcommon/summons/Source;->getIconPackage()Ljava/lang/String;

    move-result-object v4

    iget-boolean v2, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mLoadLargeImage:Z

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v3, v4, v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getIconLoader(Ljava/lang/String;Z)Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NowOrLater;->haveNow()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v2, v1, p1}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->handleNewDrawable(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V

    :cond_0
    :goto_3
    return-void

    :cond_1
    invoke-interface {p1, p2}, Lcom/google/android/searchcommon/summons/Source;->getIconUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->getGSS()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getIconLoader()Lcom/google/android/searchcommon/imageloader/CachingImageLoader;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/searchcommon/imageloader/CachingImageLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v0

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->clearDrawable()V

    new-instance v2, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;

    invoke-direct {v2, p0, v1, p1}, Lcom/google/android/searchcommon/ui/util/AsyncIcon$1;-><init>(Lcom/google/android/searchcommon/ui/util/AsyncIcon;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V

    invoke-interface {v0, v2}, Lcom/google/android/searchcommon/util/NowOrLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_3

    :cond_5
    iput-object v2, p0, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->mWantedId:Landroid/net/Uri;

    invoke-direct {p0, v2, v2, p1}, Lcom/google/android/searchcommon/ui/util/AsyncIcon;->handleNewDrawable(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Lcom/google/android/searchcommon/summons/Source;)V

    goto :goto_3
.end method
