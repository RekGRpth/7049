.class public abstract Lcom/google/android/searchcommon/util/CachedLater;
.super Ljava/lang/Object;
.source "CachedLater.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/CancellableNowOrLater;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private mCancelled:Z

.field private mConsumersWithProgress:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TA;>;>;"
        }
    .end annotation
.end field

.field private mCreating:Z

.field private final mLock:Ljava/lang/Object;

.field private mValid:Z

.field private mValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field

.field private mWaitingConsumers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TA;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    return-void
.end method

.method private storeInternal(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/searchcommon/util/CachedLater;->storeInternal(Ljava/lang/Object;Z)V

    return-void
.end method

.method private storeInternal(Ljava/lang/Object;Z)V
    .locals 6
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;Z)V"
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v5

    if-nez p2, :cond_0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/searchcommon/util/CachedLater;->mValue:Ljava/lang/Object;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/searchcommon/util/CachedLater;->mValid:Z

    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/searchcommon/util/CachedLater;->mCreating:Z

    iget-object v2, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/searchcommon/util/CachedLater;->mConsumersWithProgress:Ljava/util/List;

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/Consumer;

    if-eqz p2, :cond_1

    move-object v3, v4

    :goto_1
    invoke-interface {v0, v3}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_1
    move-object v3, p1

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method protected cancel()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public cancelGetLater(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TA;>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mConsumersWithProgress:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mConsumersWithProgress:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mConsumersWithProgress:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mCreating:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/CachedLater;->cancel()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mCreating:Z

    :cond_3
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clear()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mValue:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mValid:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mCancelled:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected abstract create()V
.end method

.method public getLater(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TA;>;)V"
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/searchcommon/util/CachedLater;->mValid:Z

    iget-object v3, p0, Lcom/google/android/searchcommon/util/CachedLater;->mValue:Ljava/lang/Object;

    if-nez v2, :cond_2

    if-eqz p1, :cond_2

    iget-object v4, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    if-nez v4, :cond_0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    :cond_0
    iget-object v4, p0, Lcom/google/android/searchcommon/util/CachedLater;->mWaitingConsumers:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    instance-of v4, p1, Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/searchcommon/util/CachedLater;->mConsumersWithProgress:Ljava/util/List;

    if-nez v4, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/searchcommon/util/CachedLater;->mConsumersWithProgress:Ljava/util/List;

    :cond_1
    iget-object v6, p0, Lcom/google/android/searchcommon/util/CachedLater;->mConsumersWithProgress:Ljava/util/List;

    move-object v0, p1

    check-cast v0, Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    move-object v4, v0

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_4

    if-eqz p1, :cond_3

    invoke-interface {p1, v3}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    :cond_3
    :goto_0
    return-void

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_4
    const/4 v1, 0x0

    iget-object v5, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_2
    iget-boolean v4, p0, Lcom/google/android/searchcommon/util/CachedLater;->mCreating:Z

    if-nez v4, :cond_5

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/searchcommon/util/CachedLater;->mCreating:Z

    const/4 v1, 0x1

    :cond_5
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/CachedLater;->create()V

    goto :goto_0

    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4
.end method

.method public declared-synchronized getNow()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TA;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/CachedLater;->haveNow()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "getNow() called when haveNow() is false"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mValue:Ljava/lang/Object;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public haveNow()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mValid:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final isCancelled()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mCancelled:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public prefetch()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/util/CachedLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method protected final setCancelled()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/searchcommon/util/CachedLater;->mLock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/searchcommon/util/CachedLater;->mCancelled:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected store(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/CachedLater;->storeInternal(Ljava/lang/Object;)V

    return-void
.end method

.method protected storeNothing()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/searchcommon/util/CachedLater;->storeInternal(Ljava/lang/Object;Z)V

    return-void
.end method
