.class public final Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;
.super Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;
.source "ChunkProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/ChunkProducer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExceptionChunk"
.end annotation


# instance fields
.field private final mException:Ljava/io/IOException;


# direct methods
.method public constructor <init>(Ljava/io/IOException;)V
    .locals 0
    .param p1    # Ljava/io/IOException;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;->mException:Ljava/io/IOException;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    iget-object v0, p1, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;->mException:Ljava/io/IOException;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;->mException:Ljava/io/IOException;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getException()Ljava/io/IOException;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;->mException:Ljava/io/IOException;

    return-object v0
.end method
