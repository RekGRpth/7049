.class public Lcom/google/android/searchcommon/util/EagerBufferedInputStream;
.super Ljava/io/InputStream;
.source "EagerBufferedInputStream.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/io/InputStream;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
        ">;"
    }
.end annotation


# instance fields
.field private final mNewChunks:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;"
        }
    .end annotation
.end field

.field private mReadBytes:I

.field private final mReadChunks:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;"
        }
    .end annotation
.end field

.field private mReadStream:Ljava/io/InputStream;

.field private final mState:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mUnreadBytes:I

.field private final mUnreadChunks:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mNewChunks:Ljava/util/concurrent/LinkedBlockingDeque;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    iput v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    iput v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadBytes:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mState:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private clearNow()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v1}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    iput v2, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    iput v2, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadBytes:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    return-void
.end method

.method private endOfStream()V
    .locals 0

    return-void
.end method

.method public static newStream(Lcom/google/android/searchcommon/util/ChunkProducer;)Lcom/google/android/searchcommon/util/EagerBufferedInputStream;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/util/ChunkProducer;

    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    invoke-direct {v0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/util/ChunkProducer;->start(Lcom/google/android/searchcommon/util/Consumer;)V

    return-object v0
.end method

.method private nextReadStream()Ljava/io/InputStream;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    :goto_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->updateQueues()V

    iget-object v5, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mNewChunks:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v5}, Ljava/util/concurrent/LinkedBlockingDeque;->takeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;

    iget-object v5, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mNewChunks:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v5, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->putFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->updateQueues()V

    :cond_0
    iget-object v5, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;

    instance-of v5, v1, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v5, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_1
    return-object v4

    :cond_1
    instance-of v5, v1, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    if-eqz v5, :cond_2

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->clearNow()V

    check-cast v1, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;->getException()Ljava/io/IOException;

    move-result-object v5

    throw v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :cond_2
    :try_start_1
    instance-of v5, v1, Lcom/google/android/searchcommon/util/ChunkProducer$DataChunk;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    invoke-interface {v5, v1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    check-cast v0, Lcom/google/android/searchcommon/util/ChunkProducer$DataChunk;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/ChunkProducer$DataChunk;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    goto :goto_1

    :cond_3
    const-string v5, "Search.EagerBufferedInputStream"

    const-string v6, "Unknown chunk in stream."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private readOrSkip([BII)I
    .locals 6
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->updateQueues()V

    if-gtz p3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-ge v1, p3, :cond_2

    iget-object v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    if-nez v3, :cond_3

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->nextReadStream()Ljava/io/InputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    if-nez v3, :cond_3

    :cond_2
    if-nez v1, :cond_6

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->endOfStream()V

    if-eqz p1, :cond_0

    const/4 v2, -0x1

    goto :goto_0

    :cond_3
    if-nez p1, :cond_4

    iget-object v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    sub-int v4, p3, v1

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v3

    long-to-int v0, v3

    :goto_2
    if-gtz v0, :cond_5

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    add-int v4, p2, v1

    sub-int v5, p3, v1

    invoke-virtual {v3, p1, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_2

    :cond_5
    add-int/2addr v1, v0

    goto :goto_1

    :cond_6
    move v2, v1

    goto :goto_0
.end method

.method private static final stateToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "normal"

    goto :goto_0

    :pswitch_1
    const-string v0, "reset"

    goto :goto_0

    :pswitch_2
    const-string v0, "closed"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateQueues()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mNewChunks:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;->getDataLength()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mState:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "Unknown state"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mState:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1, v4, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    invoke-virtual {v1, v3, v2}, Ljava/util/LinkedList;->addAll(ILjava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->clear()V

    iget v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    iget v2, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadBytes:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    iput v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadBytes:I

    :cond_1
    iput-object v5, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    :goto_1
    :pswitch_1
    return-void

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->peekLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;

    if-eqz v0, :cond_3

    instance-of v1, v0, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-interface {v1, v2}, Ljava/util/Deque;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    iget v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadBytes:I

    iget v2, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadBytes:I

    iput v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    :cond_3
    iput-object v5, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-interface {v1, v2}, Ljava/util/Deque;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public available()I
    .locals 1

    iget v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    return v0
.end method

.method public final close()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mState:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void
.end method

.method public consume(Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;)Z
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mNewChunks:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->consume(Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;)Z

    move-result v0

    return v0
.end method

.method public final mark(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mark() not supported by EagerBufferedInputStream"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final markSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->updateQueues()V

    :goto_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->nextReadStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->endOfStream()V

    const/4 v0, -0x1

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadStream:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Read into null buffer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->readOrSkip([BII)I

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mState:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-void
.end method

.method public final skip(J)J
    .locals 4
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v1, 0x0

    cmp-long v3, p1, v1

    if-gtz v3, :cond_0

    :goto_0
    return-wide v1

    :cond_0
    long-to-int v0, p1

    if-gez v0, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "byteCount too large: int overflow"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->readOrSkip([BII)I

    move-result v1

    int-to-long v1, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    iget-object v3, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->peekFirst()Ljava/lang/Object;

    move-result-object v3

    instance-of v0, v3, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    if-eqz v0, :cond_0

    const-string v2, " including sentinel"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EagerBufferedInputStream{"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "source "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_1

    const-string v3, "complete"

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mState:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->stateToString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mNewChunks:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " new chunks, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadChunks:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " unread chunks"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadChunks:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " read chunks, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mUnreadBytes:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " unread bytes, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->mReadBytes:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " read bytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "}"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_0
    const-string v2, ""

    goto/16 :goto_0

    :cond_1
    const-string v3, "incomplete"

    goto/16 :goto_1
.end method
