.class public Lcom/google/android/searchcommon/util/ForceableLock;
.super Landroid/database/DataSetObservable;
.source "ForceableLock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/ForceableLock$Owner;
    }
.end annotation


# instance fields
.field private mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

.field private final mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mLock:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/util/ForceableLock;Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/util/ForceableLock;
    .param p1    # Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/ForceableLock;->forceObtainInternal(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    return-void
.end method

.method static synthetic access$101(Lcom/google/android/searchcommon/util/ForceableLock;)V
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/util/ForceableLock;

    invoke-super {p0}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void
.end method

.method private forceObtainInternal(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v1, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ForceableLock$Owner;->forceReleaseLock()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public forceObtain(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/util/ForceableLock$Owner;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/ForceableLock;->forceObtainInternal(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    if-ne v0, p1, :cond_3

    :cond_1
    iput-object p1, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    if-eq v0, p1, :cond_2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v2, Lcom/google/android/searchcommon/util/ForceableLock$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/searchcommon/util/ForceableLock$1;-><init>(Lcom/google/android/searchcommon/util/ForceableLock;Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V

    invoke-interface {v0, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public notifyChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/database/DataSetObservable;->notifyChanged()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v1, Lcom/google/android/searchcommon/util/ForceableLock$2;

    invoke-direct {v1, p0}, Lcom/google/android/searchcommon/util/ForceableLock$2;-><init>(Lcom/google/android/searchcommon/util/ForceableLock;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public release(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/ForceableLock;->notifyChanged()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public tryObtain(Lcom/google/android/searchcommon/util/ForceableLock$Owner;)Z
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    if-ne v0, p1, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/google/android/searchcommon/util/ForceableLock;->mCurrentOwner:Lcom/google/android/searchcommon/util/ForceableLock$Owner;

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
