.class public Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;
.super Lcom/google/android/searchcommon/util/HandlerExecutor;
.source "HandlerScheduledExecutor.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;
    }
.end annotation


# instance fields
.field private final mQueue:Landroid/os/MessageQueue;

.field private final mQueuedIdleHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Runnable;",
            "Landroid/os/MessageQueue$IdleHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/os/MessageQueue;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # Landroid/os/MessageQueue;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/HandlerExecutor;-><init>(Landroid/os/Handler;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueue:Landroid/os/MessageQueue;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueuedIdleHandlers:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;Ljava/lang/Runnable;)Landroid/os/MessageQueue$IdleHandler;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;
    .param p1    # Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->dequeueIdleHandler(Ljava/lang/Runnable;)Landroid/os/MessageQueue$IdleHandler;

    move-result-object v0

    return-object v0
.end method

.method private dequeueIdleHandler(Ljava/lang/Runnable;)Landroid/os/MessageQueue$IdleHandler;
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueuedIdleHandlers:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueuedIdleHandlers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueuedIdleHandlers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/MessageQueue$IdleHandler;

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public cancelExecute(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/util/HandlerExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueue:Landroid/os/MessageQueue;

    invoke-direct {p0, p1}, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->dequeueIdleHandler(Ljava/lang/Runnable;)Landroid/os/MessageQueue$IdleHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->removeIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    return-void
.end method

.method public executeDelayed(Ljava/lang/Runnable;J)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public executeOnIdle(Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor$ExecuteOnIdle;-><init>(Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueuedIdleHandlers:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueuedIdleHandlers:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mQueue:Landroid/os/MessageQueue;

    invoke-virtual {v1, v0}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public isThisThread()Z
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/util/HandlerScheduledExecutor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
