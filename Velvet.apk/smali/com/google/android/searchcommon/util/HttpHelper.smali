.class public interface abstract Lcom/google/android/searchcommon/util/HttpHelper;
.super Ljava/lang/Object;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;,
        Lcom/google/android/searchcommon/util/HttpHelper$UrlRewriter;,
        Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException;,
        Lcom/google/android/searchcommon/util/HttpHelper$HttpException;,
        Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;,
        Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;
    }
.end annotation


# virtual methods
.method public abstract get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/searchcommon/util/HttpHelper$HttpException;
        }
    .end annotation
.end method

.method public abstract getAsync(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;
.end method

.method public abstract getAsync(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;ILcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;",
            "I",
            "Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;)",
            "Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;"
        }
    .end annotation
.end method

.method public abstract haveNetworkConnection()Z
.end method

.method public abstract post(Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/searchcommon/util/HttpHelper$HttpException;
        }
    .end annotation
.end method

.method public abstract rawGet(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/searchcommon/util/HttpHelper$HttpException;
        }
    .end annotation
.end method

.method public abstract rawPost(Lcom/google/android/searchcommon/util/HttpHelper$PostRequest;I)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/searchcommon/util/HttpHelper$HttpException;
        }
    .end annotation
.end method

.method public abstract scheduleCacheFlush()V
.end method
