.class Lcom/google/android/searchcommon/util/InputStreamChunkProducer$1;
.super Ljava/lang/Object;
.source "InputStreamChunkProducer.java"

# interfaces
.implements Lcom/google/common/io/InputSupplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/util/InputStreamChunkProducer;-><init>(Ljava/io/InputStream;Ljava/util/concurrent/ExecutorService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/io/InputSupplier",
        "<",
        "Ljava/io/InputStream;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

.field final synthetic val$inputStream:Ljava/io/InputStream;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/util/InputStreamChunkProducer;Ljava/io/InputStream;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$1;->this$0:Lcom/google/android/searchcommon/util/InputStreamChunkProducer;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$1;->val$inputStream:Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInput()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$1;->val$inputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public bridge synthetic getInput()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer$1;->getInput()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
