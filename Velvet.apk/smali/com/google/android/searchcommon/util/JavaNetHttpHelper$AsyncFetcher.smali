.class Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;
.super Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
.source "JavaNetHttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/JavaNetHttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AsyncFetcher"
.end annotation


# instance fields
.field private final mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field public mResponse:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;

.field private final mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
    .param p2    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    iput-object p3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;

    return-void
.end method


# virtual methods
.method public fetchResponse(Ljava/net/HttpURLConnection;[B)V
    .locals 7
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # [B

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v5, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mCardConsumer:Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;

    iget-object v6, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mSrpMetadataConsumer:Lcom/google/android/searchcommon/util/Consumer;

    move-object v1, p1

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;-><init>(Ljava/net/HttpURLConnection;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mResponse:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p1, v1, p0, p2}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;-><init>(Ljava/net/HttpURLConnection;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[B)V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$AsyncFetcher;->mResponse:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;

    goto :goto_0
.end method
