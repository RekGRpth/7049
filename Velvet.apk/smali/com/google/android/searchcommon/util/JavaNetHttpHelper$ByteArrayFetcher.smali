.class Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ByteArrayFetcher;
.super Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
.source "JavaNetHttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/JavaNetHttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ByteArrayFetcher"
.end annotation


# instance fields
.field public mResponse:[B


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/searchcommon/util/JavaNetHttpHelper$1;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/util/JavaNetHttpHelper$1;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ByteArrayFetcher;-><init>()V

    return-void
.end method


# virtual methods
.method public fetchResponse(Ljava/net/HttpURLConnection;[B)V
    .locals 4
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ByteArrayFetcher;->sendRequest(Ljava/net/HttpURLConnection;[B)V

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    if-gez v1, :cond_1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/google/common/io/ByteStreams;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ByteArrayFetcher;->mResponse:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    # invokes: Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->makeConnectionReusable(Ljava/io/InputStream;)V
    invoke-static {v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->access$300(Ljava/io/InputStream;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_0
    return-void

    :cond_1
    :try_start_1
    new-array v2, v1, [B

    iput-object v2, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ByteArrayFetcher;->mResponse:[B

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ByteArrayFetcher;->mResponse:[B

    invoke-static {v2, v3}, Lcom/google/common/io/ByteStreams;->readFully(Ljava/io/InputStream;[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    # invokes: Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->makeConnectionReusable(Ljava/io/InputStream;)V
    invoke-static {v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->access$300(Ljava/io/InputStream;)V

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v2
.end method
