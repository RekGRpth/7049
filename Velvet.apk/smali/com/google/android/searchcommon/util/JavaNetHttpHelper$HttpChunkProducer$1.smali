.class Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;
.super Ljava/lang/Object;
.source "JavaNetHttpHelper.java"

# interfaces
.implements Lcom/google/common/io/InputSupplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->createInputStreamSupplier(Ljava/net/HttpURLConnection;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;)Lcom/google/common/io/InputSupplier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/io/InputSupplier",
        "<",
        "Ljava/io/InputStream;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

.field final synthetic val$connection:Ljava/net/HttpURLConnection;

.field final synthetic val$headers:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

.field final synthetic val$requestContent:[B

.field final synthetic val$responseFetcher:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;Ljava/net/HttpURLConnection;[BLcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->this$0:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$responseFetcher:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;

    iput-object p3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$connection:Ljava/net/HttpURLConnection;

    iput-object p4, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$requestContent:[B

    iput-object p5, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$headers:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInput()Ljava/io/InputStream;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$responseFetcher:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;

    iget-object v4, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$connection:Ljava/net/HttpURLConnection;

    iget-object v5, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$requestContent:[B

    invoke-virtual {v3, v4, v5}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;->sendRequest(Ljava/net/HttpURLConnection;[B)V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->this$0:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    iget-object v3, v3, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->mHeaderOverrides:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$headers:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    invoke-virtual {v3, v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;->store(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$connection:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    return-object v3

    :catch_0
    move-exception v1

    iget-object v3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->val$headers:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;->store(Ljava/lang/Object;)V

    throw v1
.end method

.method public bridge synthetic getInput()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;->getInput()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
