.class public Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;
.super Lcom/google/android/searchcommon/util/InputStreamChunkProducer;
.source "JavaNetHttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/JavaNetHttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpChunkProducer"
.end annotation


# instance fields
.field private final mConnection:Ljava/net/HttpURLConnection;

.field protected final mHeaderOverrides:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mHeaders:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;


# direct methods
.method protected constructor <init>(Ljava/net/HttpURLConnection;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLjava/util/concurrent/ExecutorService;)V
    .locals 2
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
    .param p3    # [B
    .param p4    # Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, p4}, Lcom/google/android/searchcommon/util/InputStreamChunkProducer;-><init>(Ljava/util/concurrent/ExecutorService;)V

    new-instance v0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;-><init>(Lcom/google/android/searchcommon/util/JavaNetHttpHelper$1;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->mHeaders:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->mHeaderOverrides:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->mConnection:Ljava/net/HttpURLConnection;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->mHeaders:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->createInputStreamSupplier(Ljava/net/HttpURLConnection;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;)Lcom/google/common/io/InputSupplier;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->setInputStreamSupplier(Lcom/google/common/io/InputSupplier;)V

    return-void
.end method

.method private createInputStreamSupplier(Ljava/net/HttpURLConnection;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;)Lcom/google/common/io/InputSupplier;
    .locals 6
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
    .param p3    # [B
    .param p4    # Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            "Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;",
            "[B",
            "Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;",
            ")",
            "Lcom/google/common/io/InputSupplier",
            "<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer$1;-><init>(Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;Ljava/net/HttpURLConnection;[BLcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;)V

    return-object v0
.end method


# virtual methods
.method protected closeSource(Ljava/io/InputStream;Z)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Z

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    # invokes: Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->makeConnectionReusable(Ljava/io/InputStream;)V
    invoke-static {p1}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->access$300(Ljava/io/InputStream;)V

    :cond_0
    invoke-static {p1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->mConnection:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void
.end method

.method getHeaders()Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->mHeaders:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    return-object v0
.end method
