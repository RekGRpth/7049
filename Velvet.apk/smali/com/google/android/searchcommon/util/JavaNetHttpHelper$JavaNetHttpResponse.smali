.class Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;
.super Ljava/lang/Object;
.source "JavaNetHttpHelper.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/HttpHelper$HttpResponse;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/JavaNetHttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "JavaNetHttpResponse"
.end annotation


# instance fields
.field private final mInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

.field private final mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;


# direct methods
.method constructor <init>(Ljava/net/HttpURLConnection;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[B)V
    .locals 1
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # Ljava/util/concurrent/ExecutorService;
    .param p3    # Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
    .param p4    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    invoke-direct {v0, p1, p3, p4, p2}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;-><init>(Ljava/net/HttpURLConnection;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLjava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    invoke-static {v0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->newStream(Lcom/google/android/searchcommon/util/ChunkProducer;)Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    return-void
.end method

.method constructor <init>(Ljava/net/HttpURLConnection;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 7
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # Ljava/util/concurrent/ExecutorService;
    .param p3    # Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
    .param p4    # [B
    .param p5    # Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;",
            "[B",
            "Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/google/PelletHttpChunkProducer;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/searchcommon/google/PelletHttpChunkProducer;-><init>(Ljava/net/HttpURLConnection;Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;[BLjava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/google/PelletDemultiplexer$CardConsumer;Lcom/google/android/searchcommon/util/Consumer;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    invoke-static {v0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->newStream(Lcom/google/android/searchcommon/util/ChunkProducer;)Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->close()V

    return-void
.end method

.method public getContentTypeIfAvailable()[Ljava/lang/String;
    .locals 6

    const/4 v4, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->getHeaders()Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;->haveNow()Z

    move-result v3

    if-nez v3, :cond_0

    move-object v3, v4

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    const-string v5, "Content-Type"

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper;->parseContentTypeHeader(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    :cond_1
    move-object v3, v4

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "Search.JavaNetHttpHelper"

    const-string v5, "Failed to parse Content-Type header of prefetched search results page."

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    goto :goto_0
.end method

.method public getHeaders()Lcom/google/android/searchcommon/util/NowOrLater;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/NowOrLater",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->getHeaders()Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HeadersNowOrLater;

    move-result-object v0

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    return-object v0
.end method

.method public isComplete()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->isComplete()Z

    move-result v0

    return v0
.end method

.method public isFailed()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mProducer:Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$HttpChunkProducer;->isFailed()Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/EagerBufferedInputStream;->reset()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HttpResponse{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$JavaNetHttpResponse;->mInputStream:Lcom/google/android/searchcommon/util/EagerBufferedInputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
