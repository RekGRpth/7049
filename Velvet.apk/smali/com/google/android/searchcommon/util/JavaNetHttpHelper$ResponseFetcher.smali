.class public abstract Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;
.super Ljava/lang/Object;
.source "JavaNetHttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/JavaNetHttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ResponseFetcher"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static responseRedirect(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x12e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x12d

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static responseSuccess(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0xc8

    if-eq p0, v0, :cond_0

    const/16 v0, 0xcc

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method abstract fetchResponse(Ljava/net/HttpURLConnection;[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method sendRequest(Ljava/net/HttpURLConnection;[B)V
    .locals 4
    .param p1    # Ljava/net/HttpURLConnection;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/searchcommon/util/HttpHelper$HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->connect()V

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/io/OutputStream;->write([B)V

    :cond_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;->responseSuccess(I)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v1}, Lcom/google/android/searchcommon/util/JavaNetHttpHelper$ResponseFetcher;->responseRedirect(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Location"

    invoke-virtual {p1, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3, v0}, Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v2

    :cond_1
    new-instance v2, Lcom/google/android/searchcommon/util/HttpHelper$HttpException;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/searchcommon/util/HttpHelper$HttpException;-><init>(ILjava/lang/String;)V

    throw v2

    :cond_2
    return-void
.end method
