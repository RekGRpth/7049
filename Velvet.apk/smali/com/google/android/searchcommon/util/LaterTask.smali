.class public Lcom/google/android/searchcommon/util/LaterTask;
.super Lcom/google/android/searchcommon/util/CachedLater;
.source "LaterTask.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/NamedTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/searchcommon/util/CachedLater",
        "<TA;>;",
        "Lcom/google/android/searchcommon/util/NamedTask;"
    }
.end annotation


# instance fields
.field private final mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field private final mLoader:Lcom/google/android/searchcommon/util/SynchronousLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/SynchronousLoader",
            "<+TA;>;"
        }
    .end annotation
.end field

.field private final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/searchcommon/util/SynchronousLoader;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p3    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/searchcommon/util/SynchronousLoader",
            "<+TA;>;",
            "Lcom/google/android/searchcommon/util/NamedTaskExecutor;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/CachedLater;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/LaterTask;->mUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/searchcommon/util/LaterTask;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/LaterTask;->mLoader:Lcom/google/android/searchcommon/util/SynchronousLoader;

    return-void
.end method


# virtual methods
.method public cancelExecution()V
    .locals 0

    return-void
.end method

.method protected create()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LaterTask;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LaterTask;->mLoader:Lcom/google/android/searchcommon/util/SynchronousLoader;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/LaterTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/SynchronousLoader;->loadNow(Landroid/net/Uri;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/searchcommon/util/LaterTask;->store(Ljava/lang/Object;)V

    return-void
.end method
