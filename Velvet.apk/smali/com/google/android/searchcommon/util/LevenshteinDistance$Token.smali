.class public final Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;
.super Ljava/lang/Object;
.source "LevenshteinDistance.java"

# interfaces
.implements Ljava/lang/CharSequence;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/LevenshteinDistance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Token"
.end annotation


# instance fields
.field private final mContainer:[C

.field public final mEnd:I

.field public final mStart:I


# direct methods
.method public constructor <init>([CII)V
    .locals 0
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mContainer:[C

    iput p2, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    iput p3, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mEnd:I

    return-void
.end method


# virtual methods
.method public charAt(I)C
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mContainer:[C

    iget v1, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    return v0
.end method

.method public length()I
    .locals 2

    iget v0, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mEnd:I

    iget v1, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public prefixOf(Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;)Z
    .locals 9
    .param p1    # Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->length()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->length()I

    move-result v7

    if-le v1, v7, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    iget v5, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    iget v3, p1, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    iget-object v4, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mContainer:[C

    iget-object v2, p1, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mContainer:[C

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    add-int v7, v5, v0

    aget-char v7, v4, v7

    invoke-static {v7}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v7

    add-int v8, v3, v0

    aget-char v8, v2, v8

    invoke-static {v8}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v8

    if-ne v7, v8, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public bridge synthetic subSequence(II)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->subSequence(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public subSequence(II)Ljava/lang/String;
    .locals 4
    .param p1    # I
    .param p2    # I

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mContainer:[C

    iget v2, p0, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->mStart:I

    add-int/2addr v2, p1

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->length()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/searchcommon/util/LevenshteinDistance$Token;->subSequence(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
