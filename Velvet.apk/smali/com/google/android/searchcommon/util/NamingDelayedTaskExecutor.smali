.class public Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;
.super Lcom/google/android/searchcommon/util/NamingTaskExecutor;
.source "NamingDelayedTaskExecutor.java"


# instance fields
.field private final mDelayedExecutor:Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/util/NamingTaskExecutor;-><init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V

    iput-object p2, p0, Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;->mDelayedExecutor:Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;

    return-void
.end method


# virtual methods
.method public executeDelayed(Ljava/lang/Runnable;J)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;->mDelayedExecutor:Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;->createTask(Ljava/lang/Runnable;)Lcom/google/android/searchcommon/util/NamedTask;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/searchcommon/util/NamedDelayedTaskExecutor;->executeDelayed(Lcom/google/android/searchcommon/util/NamedTask;J)V

    return-void
.end method
