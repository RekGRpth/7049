.class public Lcom/google/android/searchcommon/util/NamingTaskExecutor;
.super Lcom/google/android/searchcommon/util/ExecutorServiceAdapter;
.source "NamingTaskExecutor.java"


# instance fields
.field protected final mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

.field protected final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/NamedTaskExecutor;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/ExecutorServiceAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/NamingTaskExecutor;->mName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/searchcommon/util/NamingTaskExecutor;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    return-void
.end method


# virtual methods
.method protected createTask(Ljava/lang/Runnable;)Lcom/google/android/searchcommon/util/NamedTask;
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/searchcommon/util/NamingTaskExecutor$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/NamingTaskExecutor$1;-><init>(Lcom/google/android/searchcommon/util/NamingTaskExecutor;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/NamingTaskExecutor;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/util/NamingTaskExecutor;->createTask(Ljava/lang/Runnable;)Lcom/google/android/searchcommon/util/NamedTask;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->execute(Lcom/google/android/searchcommon/util/NamedTask;)V

    return-void
.end method

.method public submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/NamingTaskExecutor;->mExecutor:Lcom/google/android/searchcommon/util/NamedTaskExecutor;

    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/util/NamingTaskExecutor;->createTask(Ljava/lang/Runnable;)Lcom/google/android/searchcommon/util/NamedTask;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/NamedTaskExecutor;->submit(Lcom/google/android/searchcommon/util/NamedTask;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method
