.class public Lcom/google/android/searchcommon/util/Now;
.super Ljava/lang/Object;
.source "Now.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/CancellableNowOrLater;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
        "<TC;>;"
    }
.end annotation


# instance fields
.field private final mValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TC;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/Now;->mValue:Ljava/lang/Object;

    return-void
.end method

.method public static returnNull()Lcom/google/android/searchcommon/util/Now;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/searchcommon/util/Now",
            "<TC;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/Now;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/util/Now;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static returnThis(Ljava/lang/Object;)Lcom/google/android/searchcommon/util/Now;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            ">(TC;)",
            "Lcom/google/android/searchcommon/util/Now",
            "<TC;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/Now;

    invoke-direct {v0, p0}, Lcom/google/android/searchcommon/util/Now;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public cancelGetLater(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TC;>;)V"
        }
    .end annotation

    return-void
.end method

.method public getLater(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TC;>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/Now;->getNow()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void
.end method

.method public getNow()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TC;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/Now;->mValue:Ljava/lang/Object;

    return-object v0
.end method

.method public haveNow()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
