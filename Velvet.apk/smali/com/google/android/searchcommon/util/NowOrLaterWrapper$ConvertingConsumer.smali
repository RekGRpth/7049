.class public Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumer;
.super Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;
.source "NowOrLaterWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/NowOrLaterWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConvertingConsumer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/NowOrLaterWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/NowOrLaterWrapper;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumer;->this$0:Lcom/google/android/searchcommon/util/NowOrLaterWrapper;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;-><init>(Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->consume(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected doConsume(Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;TA;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumer;->this$0:Lcom/google/android/searchcommon/util/NowOrLaterWrapper;

    invoke-virtual {v0, p2}, Lcom/google/android/searchcommon/util/NowOrLaterWrapper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
