.class public Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumerWithProgress;
.super Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;
.source "NowOrLaterWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/NowOrLaterWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConvertingConsumerWithProgress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/NowOrLaterWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/NowOrLaterWrapper;Lcom/google/android/searchcommon/util/ConsumerWithProgress;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumerWithProgress;->this$0:Lcom/google/android/searchcommon/util/NowOrLaterWrapper;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;-><init>(Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;Lcom/google/android/searchcommon/util/ConsumerWithProgress;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->consume(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic consumePartial(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->consumePartial(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected doConsume(Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;TA;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumerWithProgress;->this$0:Lcom/google/android/searchcommon/util/NowOrLaterWrapper;

    invoke-virtual {v0, p2}, Lcom/google/android/searchcommon/util/NowOrLaterWrapper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected doConsumePartial(Lcom/google/android/searchcommon/util/ConsumerWithProgress;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;TA;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/NowOrLaterWrapper$ConvertingConsumerWithProgress;->this$0:Lcom/google/android/searchcommon/util/NowOrLaterWrapper;

    invoke-virtual {v0, p2}, Lcom/google/android/searchcommon/util/NowOrLaterWrapper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/searchcommon/util/ConsumerWithProgress;->consumePartial(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected doProgressChanged(Lcom/google/android/searchcommon/util/ConsumerWithProgress;I)V
    .locals 0
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;I)V"
        }
    .end annotation

    invoke-interface {p1, p2}, Lcom/google/android/searchcommon/util/ConsumerWithProgress;->progressChanged(I)V

    return-void
.end method

.method public bridge synthetic progressChanged(I)V
    .locals 0
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;->progressChanged(I)V

    return-void
.end method
