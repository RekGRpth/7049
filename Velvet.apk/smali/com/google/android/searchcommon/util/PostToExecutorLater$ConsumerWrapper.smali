.class Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWrapper;
.super Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;
.source "PostToExecutorLater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/PostToExecutorLater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConsumerWrapper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/PostToExecutorLater;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/PostToExecutorLater;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TC;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWrapper;->this$0:Lcom/google/android/searchcommon/util/PostToExecutorLater;

    invoke-direct {p0, p1, p2}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;-><init>(Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method


# virtual methods
.method protected doConsume(Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TC;>;TC;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWrapper;->this$0:Lcom/google/android/searchcommon/util/PostToExecutorLater;

    # getter for: Lcom/google/android/searchcommon/util/PostToExecutorLater;->mExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/searchcommon/util/PostToExecutorLater;->access$000(Lcom/google/android/searchcommon/util/PostToExecutorLater;)Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/searchcommon/util/Consumers;->consumeAsync(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0
.end method
