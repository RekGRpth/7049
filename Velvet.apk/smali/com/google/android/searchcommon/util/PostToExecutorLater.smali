.class public Lcom/google/android/searchcommon/util/PostToExecutorLater;
.super Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;
.source "PostToExecutorLater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWithProgressWrapper;,
        Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWrapper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase",
        "<TC;TC;>;"
    }
.end annotation


# instance fields
.field private final mExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/NowOrLater;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/searchcommon/util/NowOrLater",
            "<+TC;>;)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;-><init>(Lcom/google/android/searchcommon/util/NowOrLater;)V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/PostToExecutorLater;->mExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/searchcommon/util/PostToExecutorLater;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/util/PostToExecutorLater;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/PostToExecutorLater;->mExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method protected createConsumer(Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TC;>;)",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<TC;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWrapper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWrapper;-><init>(Lcom/google/android/searchcommon/util/PostToExecutorLater;Lcom/google/android/searchcommon/util/Consumer;)V

    return-object v0
.end method

.method protected createConsumerWithProgress(Lcom/google/android/searchcommon/util/ConsumerWithProgress;)Lcom/google/android/searchcommon/util/ConsumerWithProgress;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TC;>;)",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<TC;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWithProgressWrapper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/searchcommon/util/PostToExecutorLater$ConsumerWithProgressWrapper;-><init>(Lcom/google/android/searchcommon/util/PostToExecutorLater;Lcom/google/android/searchcommon/util/ConsumerWithProgress;)V

    return-object v0
.end method

.method public getNow()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TC;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/searchcommon/util/PostToExecutorLater;->mWrapped:Lcom/google/android/searchcommon/util/NowOrLater;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
