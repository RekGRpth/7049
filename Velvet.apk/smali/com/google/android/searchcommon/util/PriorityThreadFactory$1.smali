.class Lcom/google/android/searchcommon/util/PriorityThreadFactory$1;
.super Ljava/lang/Thread;
.source "PriorityThreadFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/searchcommon/util/PriorityThreadFactory;->newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/searchcommon/util/PriorityThreadFactory;


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/util/PriorityThreadFactory;Ljava/lang/Runnable;)V
    .locals 0
    .param p2    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/searchcommon/util/PriorityThreadFactory$1;->this$0:Lcom/google/android/searchcommon/util/PriorityThreadFactory;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    invoke-static {}, Lcom/google/android/velvet/VelvetStrictMode;->setThreadPolicy()V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/PriorityThreadFactory$1;->this$0:Lcom/google/android/searchcommon/util/PriorityThreadFactory;

    # getter for: Lcom/google/android/searchcommon/util/PriorityThreadFactory;->mPriority:I
    invoke-static {v0}, Lcom/google/android/searchcommon/util/PriorityThreadFactory;->access$000(Lcom/google/android/searchcommon/util/PriorityThreadFactory;)I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-super {p0}, Ljava/lang/Thread;->run()V

    return-void
.end method
