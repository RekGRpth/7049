.class public interface abstract Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
.super Ljava/lang/Object;
.source "ScheduledSingleThreadedExecutor.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/CancellableSingleThreadedExecutor;


# virtual methods
.method public abstract executeDelayed(Ljava/lang/Runnable;J)V
.end method

.method public abstract executeOnIdle(Ljava/lang/Runnable;)V
.end method

.method public abstract getHandler()Landroid/os/Handler;
.end method

.method public abstract isThisThread()Z
.end method
