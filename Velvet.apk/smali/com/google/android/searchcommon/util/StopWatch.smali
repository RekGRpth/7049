.class public Lcom/google/android/searchcommon/util/StopWatch;
.super Ljava/lang/Object;
.source "StopWatch.java"


# instance fields
.field private mStart:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/searchcommon/util/StopWatch;->mStart:J

    return-void
.end method


# virtual methods
.method public getElapsedTime()I
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/searchcommon/util/StopWatch;->mStart:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public start()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/searchcommon/util/StopWatch;->mStart:J

    return-void
.end method
