.class public Lcom/google/android/searchcommon/util/SystemClockImpl;
.super Ljava/lang/Object;
.source "SystemClockImpl.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Clock;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mListenManagerTimeReset:Lcom/google/android/apps/sidekick/inject/ListenerManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/sidekick/inject/ListenerManager",
            "<",
            "Lcom/google/android/searchcommon/util/Clock$TimeResetListener;",
            ">;"
        }
    .end annotation
.end field

.field private mListenManagerTimeTick:Lcom/google/android/apps/sidekick/inject/ListenerManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/sidekick/inject/ListenerManager",
            "<",
            "Lcom/google/android/searchcommon/util/Clock$TimeTickListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public currentTimeMillis()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public elapsedRealtime()J
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public registerTimeResetListener(Lcom/google/android/searchcommon/util/Clock$TimeResetListener;)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/util/Clock$TimeResetListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeReset:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/inject/ListenerManager;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mContext:Landroid/content/Context;

    const-string v2, "android.intent.action.TIME_SET"

    new-instance v3, Lcom/google/android/searchcommon/util/SystemClockImpl$1;

    invoke-direct {v3, p0}, Lcom/google/android/searchcommon/util/SystemClockImpl$1;-><init>(Lcom/google/android/searchcommon/util/SystemClockImpl;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/inject/ListenerManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeReset:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeReset:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->registerListener(Ljava/lang/Object;)V

    return-void
.end method

.method public registerTimeTickListener(Lcom/google/android/searchcommon/util/Clock$TimeTickListener;)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/util/Clock$TimeTickListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeTick:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/sidekick/inject/ListenerManager;

    iget-object v1, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mContext:Landroid/content/Context;

    const-string v2, "android.intent.action.TIME_TICK"

    new-instance v3, Lcom/google/android/searchcommon/util/SystemClockImpl$2;

    invoke-direct {v3, p0}, Lcom/google/android/searchcommon/util/SystemClockImpl$2;-><init>(Lcom/google/android/searchcommon/util/SystemClockImpl;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/sidekick/inject/ListenerManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/sidekick/inject/ListenerManager$Dispatcher;)V

    iput-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeTick:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeTick:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->registerListener(Ljava/lang/Object;)V

    return-void
.end method

.method public unregisterTimeResetListener(Lcom/google/android/searchcommon/util/Clock$TimeResetListener;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/Clock$TimeResetListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeReset:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeReset:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->unRegisterListener(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeReset:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeReset:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    :cond_0
    return-void
.end method

.method public unregisterTimeTickListener(Lcom/google/android/searchcommon/util/Clock$TimeTickListener;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/Clock$TimeTickListener;

    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeTick:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeTick:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->unRegisterListener(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeTick:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/inject/ListenerManager;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/SystemClockImpl;->mListenManagerTimeTick:Lcom/google/android/apps/sidekick/inject/ListenerManager;

    :cond_0
    return-void
.end method

.method public uptimeMillis()J
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
