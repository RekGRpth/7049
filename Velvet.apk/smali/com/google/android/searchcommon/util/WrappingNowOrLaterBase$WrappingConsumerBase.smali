.class public abstract Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;
.super Ljava/lang/Object;
.source "WrappingNowOrLaterBase.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "WrappingConsumerBase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<TA;>;"
    }
.end annotation


# instance fields
.field protected final mWrappedConsumer:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->this$0:Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->mWrappedConsumer:Lcom/google/android/searchcommon/util/Consumer;

    return-void
.end method


# virtual methods
.method public consume(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->consumer()Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->this$0:Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;

    iget-object v2, v1, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mConsumers:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->this$0:Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;

    iget-object v1, v1, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mConsumers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->doConsume(Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected consumer()Lcom/google/android/searchcommon/util/Consumer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->this$0:Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;

    iget-object v1, v1, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mConsumers:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->mWrappedConsumer:Lcom/google/android/searchcommon/util/Consumer;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/Consumer;

    if-eqz v0, :cond_0

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    if-nez v0, :cond_2

    const/4 v1, 0x0

    :goto_1
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;->mWrappedConsumer:Lcom/google/android/searchcommon/util/Consumer;

    goto :goto_1
.end method

.method protected abstract doConsume(Lcom/google/android/searchcommon/util/Consumer;Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;TA;)Z"
        }
    .end annotation
.end method
