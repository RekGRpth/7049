.class public abstract Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;
.super Ljava/lang/Object;
.source "WrappingNowOrLaterBase.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/CancellableNowOrLater;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerWithProgressBase;,
        Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase$WrappingConsumerBase;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
        "<TB;>;"
    }
.end annotation


# instance fields
.field protected final mConsumers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TA;>;>;"
        }
    .end annotation
.end field

.field protected final mWrapped:Lcom/google/android/searchcommon/util/NowOrLater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/NowOrLater",
            "<+TA;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/util/NowOrLater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/NowOrLater",
            "<+TA;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mWrapped:Lcom/google/android/searchcommon/util/NowOrLater;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mConsumers:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public cancelGetLater(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mConsumers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/Consumer;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mWrapped:Lcom/google/android/searchcommon/util/NowOrLater;

    instance-of v1, v1, Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mWrapped:Lcom/google/android/searchcommon/util/NowOrLater;

    check-cast v1, Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->cancelGetLater(Lcom/google/android/searchcommon/util/Consumer;)V

    :cond_0
    return-void
.end method

.method protected abstract createConsumer(Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;)",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<TA;>;"
        }
    .end annotation
.end method

.method protected abstract createConsumerWithProgress(Lcom/google/android/searchcommon/util/ConsumerWithProgress;)Lcom/google/android/searchcommon/util/ConsumerWithProgress;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<-TB;>;)",
            "Lcom/google/android/searchcommon/util/ConsumerWithProgress",
            "<TA;>;"
        }
    .end annotation
.end method

.method public getLater(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<-TB;>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mConsumers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    instance-of v1, p1, Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    if-eqz v1, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    invoke-virtual {p0, v1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->createConsumerWithProgress(Lcom/google/android/searchcommon/util/ConsumerWithProgress;)Lcom/google/android/searchcommon/util/ConsumerWithProgress;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mConsumers:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mConsumers:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mWrapped:Lcom/google/android/searchcommon/util/NowOrLater;

    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/util/NowOrLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->createConsumer(Lcom/google/android/searchcommon/util/Consumer;)Lcom/google/android/searchcommon/util/Consumer;

    move-result-object v0

    goto :goto_1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public abstract getNow()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TB;"
        }
    .end annotation
.end method

.method public haveNow()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/searchcommon/util/WrappingNowOrLaterBase;->mWrapped:Lcom/google/android/searchcommon/util/NowOrLater;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/NowOrLater;->haveNow()Z

    move-result v0

    return v0
.end method

.method public isCancelled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
