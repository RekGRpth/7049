.class public interface abstract Lcom/google/android/speech/Recognizer;
.super Ljava/lang/Object;
.source "Recognizer.java"


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V
.end method

.method public abstract startRecordedAudioRecognition(Lcom/google/android/speech/params/RecognizerParams;[BLcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V
.end method

.method public abstract stopListening()V
.end method
