.class public Lcom/google/android/speech/ResponseProcessor;
.super Ljava/lang/Object;
.source "ResponseProcessor.java"

# interfaces
.implements Lcom/google/android/speech/audio/EndpointerListener;
.implements Lcom/google/android/speech/callback/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/ResponseProcessor$AudioCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/audio/EndpointerListener;",
        "Lcom/google/android/speech/callback/Callback",
        "<",
        "Lcom/google/android/speech/RecognitionResponse;",
        "Lcom/google/android/speech/exception/RecognizeException;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

.field private final mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

.field private mInvalid:Z

.field private final mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

.field private final mS3ResponseProcessor:Lcom/google/android/speech/message/S3ResponseProcessor;

.field private final mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/ResponseProcessor$AudioCallback;Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/ResponseProcessor$AudioCallback;
    .param p2    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p3    # Lcom/google/android/speech/params/RecognizerParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    iput-object p1, p0, Lcom/google/android/speech/ResponseProcessor;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    iput-object p2, p0, Lcom/google/android/speech/ResponseProcessor;->mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    new-instance v0, Lcom/google/android/speech/message/S3ResponseProcessor;

    invoke-direct {v0}, Lcom/google/android/speech/message/S3ResponseProcessor;-><init>()V

    iput-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mS3ResponseProcessor:Lcom/google/android/speech/message/S3ResponseProcessor;

    iput-object p3, p0, Lcom/google/android/speech/ResponseProcessor;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    return-void
.end method

.method private handleS3Response(Lcom/google/speech/s3/S3$S3Response;)V
    .locals 3
    .param p1    # Lcom/google/speech/s3/S3$S3Response;

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v0

    if-ne v0, v2, :cond_1

    const/16 v0, 0xa

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    invoke-interface {v0}, Lcom/google/android/speech/ResponseProcessor$AudioCallback;->shutdownAudio()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mS3ResponseProcessor:Lcom/google/android/speech/message/S3ResponseProcessor;

    iget-object v1, p0, Lcom/google/android/speech/ResponseProcessor;->mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/speech/message/S3ResponseProcessor;->process(Lcom/google/speech/s3/S3$S3Response;Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const/16 v0, 0x9

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    invoke-interface {v0}, Lcom/google/android/speech/ResponseProcessor$AudioCallback;->shutdownAudio()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->hasRecognizerEventExtension()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getRecognizerEventExtension()Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->getRecognitionEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    invoke-interface {v0}, Lcom/google/android/speech/ResponseProcessor$AudioCallback;->stopAudio()V

    goto :goto_0
.end method


# virtual methods
.method public invalidate()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    return-void
.end method

.method public onEndOfSpeech()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mRecognizerParams:Lcom/google/android/speech/params/RecognizerParams;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->DICTATION:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    invoke-interface {v0}, Lcom/google/android/speech/ResponseProcessor$AudioCallback;->stopAudio()V

    :cond_1
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onEndOfSpeech()V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v0, p1, Lcom/google/android/speech/exception/NetworkRecognizeException;

    if-eqz v0, :cond_1

    const/16 v0, 0x9

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    invoke-interface {v0}, Lcom/google/android/speech/ResponseProcessor$AudioCallback;->shutdownAudio()V

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/speech/exception/RecognizeException;

    invoke-virtual {p0, p1}, Lcom/google/android/speech/ResponseProcessor;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    return-void
.end method

.method public onMusicDetected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMusicDetected()V

    goto :goto_0
.end method

.method public onNoSpeechDetected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x5a

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onNoSpeechDetected()V

    goto :goto_0
.end method

.method public onRecognitionCancelled()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onRecognitionCancelled()V

    goto :goto_0
.end method

.method public onResult(Lcom/google/android/speech/RecognitionResponse;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/RecognitionResponse;

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/speech/RecognitionResponse;->getS3Response()Lcom/google/speech/s3/S3$S3Response;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/speech/ResponseProcessor;->handleS3Response(Lcom/google/speech/s3/S3$S3Response;)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/speech/RecognitionResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/speech/ResponseProcessor;->onResult(Lcom/google/android/speech/RecognitionResponse;)V

    return-void
.end method

.method public onStartOfSpeech(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/ResponseProcessor;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mAudioCallback:Lcom/google/android/speech/ResponseProcessor$AudioCallback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/speech/ResponseProcessor$AudioCallback;->recordingStarted(J)V

    iget-object v0, p0, Lcom/google/android/speech/ResponseProcessor;->mEventListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onBeginningOfSpeech()V

    goto :goto_0
.end method
