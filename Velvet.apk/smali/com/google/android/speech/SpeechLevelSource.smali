.class public Lcom/google/android/speech/SpeechLevelSource;
.super Ljava/lang/Object;
.source "SpeechLevelSource.java"


# instance fields
.field private volatile mSpeechLevel:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpeechLevel()I
    .locals 1

    iget v0, p0, Lcom/google/android/speech/SpeechLevelSource;->mSpeechLevel:I

    return v0
.end method

.method public reset()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/speech/SpeechLevelSource;->mSpeechLevel:I

    return-void
.end method

.method public setSpeechLevel(I)V
    .locals 1
    .param p1    # I

    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput p1, p0, Lcom/google/android/speech/SpeechLevelSource;->mSpeechLevel:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
