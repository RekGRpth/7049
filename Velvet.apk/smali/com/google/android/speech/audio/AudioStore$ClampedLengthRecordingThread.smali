.class Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;
.super Ljava/lang/Thread;
.source "AudioStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/audio/AudioStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ClampedLengthRecordingThread"
.end annotation


# instance fields
.field private mBuf:[B

.field private final mInput:Ljava/io/InputStream;

.field private final mMaxSize:I

.field private final mReadSize:I

.field private mState:I

.field private mTotalLength:I


# direct methods
.method constructor <init>(IILjava/io/InputStream;I)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/io/InputStream;
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput p2, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mMaxSize:I

    iput p4, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mReadSize:I

    iput-object p3, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mInput:Ljava/io/InputStream;

    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mBuf:[B

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    return-void
.end method


# virtual methods
.method public getBuffer()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mBuf:[B

    return-object v0
.end method

.method public getTotalLength()I
    .locals 1

    iget v0, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mTotalLength:I

    return v0
.end method

.method public isGood()Z
    .locals 2

    iget v0, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOverflown()Z
    .locals 2

    iget v0, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestStop()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    invoke-virtual {p0}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->interrupt()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 12

    const/4 v7, 0x3

    const/4 v11, 0x2

    const/4 v8, -0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eq v2, v8, :cond_0

    :try_start_1
    iget v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    if-ne v6, v11, :cond_1

    :cond_0
    iput v5, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mTotalLength:I

    const/4 v6, 0x3

    iput v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mInput:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_1
    return-void

    :cond_1
    add-int v6, v5, v2

    :try_start_2
    iget v9, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mMaxSize:I

    if-le v6, v9, :cond_2

    iget v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mMaxSize:I

    iput v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mTotalLength:I

    const/4 v6, -0x2

    iput v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mInput:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :cond_2
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    add-int/2addr v5, v2

    :try_start_4
    iget v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mMaxSize:I

    if-ge v5, v6, :cond_4

    iget v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mReadSize:I

    add-int/2addr v6, v5

    iget v9, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mMaxSize:I

    invoke-static {v6, v9}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mBuf:[B

    array-length v6, v6

    if-le v0, v6, :cond_3

    mul-int/lit8 v6, v0, 0x2

    iget v9, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mMaxSize:I

    invoke-static {v6, v9}, Ljava/lang/Math;->min(II)I

    move-result v6

    new-array v3, v6, [B

    iget-object v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mBuf:[B

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v6, v9, v3, v10, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mBuf:[B

    :cond_3
    sub-int v4, v0, v5

    iget-object v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mInput:Ljava/io/InputStream;

    iget-object v9, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mBuf:[B

    invoke-virtual {v6, v9, v5, v4}, Ljava/io/InputStream;->read([BII)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v2

    goto :goto_0

    :catchall_0
    move-exception v6

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v6
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catch_0
    move-exception v1

    :try_start_7
    monitor-enter p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    iput v5, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mTotalLength:I

    iget v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    if-ne v6, v11, :cond_5

    move v6, v7

    :goto_2
    iput v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mState:I

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    iget-object v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mInput:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :cond_4
    :try_start_9
    iget-object v6, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mInput:Ljava/io/InputStream;

    const/4 v9, 0x1

    new-array v9, v9, [B

    invoke-virtual {v6, v9}, Ljava/io/InputStream;->read([B)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    move-result v2

    goto :goto_0

    :cond_5
    move v6, v8

    goto :goto_2

    :catchall_1
    move-exception v6

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v6
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :catchall_2
    move-exception v6

    iget-object v7, p0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->mInput:Ljava/io/InputStream;

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v6
.end method
