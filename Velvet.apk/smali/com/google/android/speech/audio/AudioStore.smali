.class public Lcom/google/android/speech/audio/AudioStore;
.super Ljava/lang/Object;
.source "AudioStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;
    }
.end annotation


# instance fields
.field private mEndPos:I

.field private mLastAudio:[B

.field private mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

.field private mStartPos:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    return-void
.end method

.method private doStopRecording(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->requestStop()V

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->isGood()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->getBuffer()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->getTotalLength()I

    move-result v1

    iget v2, p0, Lcom/google/android/speech/audio/AudioStore;->mEndPos:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/speech/audio/AudioStore;->mEndPos:I

    :cond_2
    :goto_1
    iget v1, p0, Lcom/google/android/speech/audio/AudioStore;->mStartPos:I

    iget v2, p0, Lcom/google/android/speech/audio/AudioStore;->mEndPos:I

    if-lt v1, v2, :cond_3

    iput-object v3, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    :cond_3
    iput-object v3, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    goto :goto_0

    :catch_0
    move-exception v0

    iput-object v3, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->isOverflown()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/google/android/speech/audio/AudioStore;->mEndPos:I

    const v2, 0xea600

    if-gt v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->getBuffer()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    goto :goto_1
.end method


# virtual methods
.method public getLastAudio()[B
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/speech/audio/AudioStore;->isRecording()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/speech/audio/AudioStore;->mStartPos:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/speech/audio/AudioStore;->mEndPos:I

    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    array-length v1, v1

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    iget v1, p0, Lcom/google/android/speech/audio/AudioStore;->mStartPos:I

    iget v2, p0, Lcom/google/android/speech/audio/AudioStore;->mEndPos:I

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    goto :goto_1
.end method

.method public hasLastAudio()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/speech/audio/AudioStore;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "AudioStore"

    const-string v2, "hasLastAudio called while still recording"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isRecording()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRecordingStartTime(J)V
    .locals 5
    .param p1    # J

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const-wide/16 v3, 0x0

    cmp-long v0, p1, v3

    if-ltz v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const-wide/16 v0, 0x10

    mul-long/2addr v0, p1

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/speech/audio/AudioStore;->mStartPos:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public startRecording(Ljava/io/InputStream;I)V
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .param p2    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mLastAudio:[B

    iput v1, p0, Lcom/google/android/speech/audio/AudioStore;->mStartPos:I

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/speech/audio/AudioStore;->mEndPos:I

    new-instance v0, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    const v1, 0x27100

    const v2, 0xea600

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;-><init>(IILjava/io/InputStream;I)V

    iput-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    iget-object v0, p0, Lcom/google/android/speech/audio/AudioStore;->mRecordingThread:Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;

    invoke-virtual {v0}, Lcom/google/android/speech/audio/AudioStore$ClampedLengthRecordingThread;->start()V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public waitForRecording()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/speech/audio/AudioStore;->doStopRecording(Z)V

    return-void
.end method
