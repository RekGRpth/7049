.class public Lcom/google/android/speech/audio/MicrophoneInputStream;
.super Ljava/io/InputStream;
.source "MicrophoneInputStream.java"


# instance fields
.field private mAudioRecord:Landroid/media/AudioRecord;

.field private final mBufferSize:I

.field private mClosed:Z

.field private final mLock:Ljava/lang/Object;

.field private final mNoiseSuppressionEnabled:Z

.field private mNoiseSuppressor:Landroid/media/audiofx/NoiseSuppressor;

.field private final mSampleRate:I

.field private final mStartEventSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Landroid/media/MediaSyncEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mStarted:Z


# direct methods
.method public constructor <init>(IIZLcom/google/common/base/Supplier;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZ",
            "Lcom/google/common/base/Supplier",
            "<",
            "Landroid/media/MediaSyncEvent;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mLock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mStarted:Z

    iput p1, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mSampleRate:I

    const/16 v0, 0x10

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mBufferSize:I

    invoke-virtual {p0}, Lcom/google/android/speech/audio/MicrophoneInputStream;->createAudioRecord()Landroid/media/AudioRecord;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    iput-boolean p3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressionEnabled:Z

    iput-object p4, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mStartEventSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method private ensureStartedLocked()Landroid/media/AudioRecord;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mLock:Ljava/lang/Object;

    invoke-static {v3}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkHoldsLock(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    if-nez v3, :cond_0

    new-instance v3, Ljava/io/IOException;

    const-string v4, "AudioRecord failed to initialize."

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    iget-boolean v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mStarted:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    :goto_0
    return-object v3

    :cond_1
    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressionEnabled:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v3}, Landroid/media/AudioRecord;->getAudioSessionId()I

    move-result v3

    invoke-static {v3}, Landroid/media/audiofx/NoiseSuppressor;->create(I)Landroid/media/audiofx/NoiseSuppressor;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressor:Landroid/media/audiofx/NoiseSuppressor;

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressor:Landroid/media/audiofx/NoiseSuppressor;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/media/audiofx/NoiseSuppressor;->setEnabled(Z)I

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressor:Landroid/media/audiofx/NoiseSuppressor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/speech/audio/MicrophoneInputStream;->getStartEvent()Landroid/media/MediaSyncEvent;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v3, v2}, Landroid/media/AudioRecord;->startRecording(Landroid/media/MediaSyncEvent;)V

    :goto_2
    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v3}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v1

    const/4 v3, 0x3

    if-eq v1, v3, :cond_4

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "couldn\'t start recording, state is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_0
    move-exception v0

    iput-object v6, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressor:Landroid/media/audiofx/NoiseSuppressor;

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v3}, Landroid/media/AudioRecord;->startRecording()V

    goto :goto_2

    :cond_4
    iput-boolean v5, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mStarted:Z

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    goto :goto_0
.end method

.method private getStartEvent()Landroid/media/MediaSyncEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mStartEventSupplier:Lcom/google/common/base/Supplier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mStartEventSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaSyncEvent;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mClosed:Z

    if-nez v0, :cond_1

    const-string v0, "MicrophoneInputStream"

    const-string v2, "mic_close"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    iget-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressor:Landroid/media/audiofx/NoiseSuppressor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressor:Landroid/media/audiofx/NoiseSuppressor;

    invoke-virtual {v0}, Landroid/media/audiofx/NoiseSuppressor;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mNoiseSuppressor:Landroid/media/audiofx/NoiseSuppressor;

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mAudioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mClosed:Z

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected createAudioRecord()Landroid/media/AudioRecord;
    .locals 6

    new-instance v0, Landroid/media/AudioRecord;

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mSampleRate:I

    const/16 v3, 0x10

    const/4 v4, 0x2

    iget v5, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mBufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public read()I
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Single-byte read not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([B)I
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/speech/audio/MicrophoneInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 5
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mClosed:Z

    if-eqz v4, :cond_1

    monitor-exit v3

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/speech/audio/MicrophoneInputStream;->ensureStartedLocked()Landroid/media/AudioRecord;

    move-result-object v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioRecord;->read([BII)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-boolean v4, p0, Lcom/google/android/speech/audio/MicrophoneInputStream;->mClosed:Z

    if-eqz v4, :cond_2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v1, v2

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_2
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-ge v1, v2, :cond_0

    const/4 v2, -0x3

    if-ne v1, v2, :cond_3

    new-instance v2, Ljava/io/IOException;

    const-string v3, "not open"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :cond_3
    const/4 v2, -0x2

    if-ne v1, v2, :cond_4

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Bad offset/length arguments for buffer"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected error code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
