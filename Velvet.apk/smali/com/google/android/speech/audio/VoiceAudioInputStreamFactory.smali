.class public final Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;
.super Ljava/lang/Object;
.source "VoiceAudioInputStreamFactory.java"

# interfaces
.implements Lcom/google/android/speech/audio/AudioInputStreamFactory;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDelegate:Lcom/google/android/speech/audio/AudioInputStreamFactory;

.field private final mSettings:Lcom/google/android/speech/SpeechSettings;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/audio/AudioInputStreamFactory;Lcom/google/android/speech/SpeechSettings;Landroid/content/Context;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/audio/AudioInputStreamFactory;
    .param p2    # Lcom/google/android/speech/SpeechSettings;
    .param p3    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;->mDelegate:Lcom/google/android/speech/audio/AudioInputStreamFactory;

    iput-object p2, p0, Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;->mSettings:Lcom/google/android/speech/SpeechSettings;

    iput-object p3, p0, Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public createInputStream()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;->mDelegate:Lcom/google/android/speech/audio/AudioInputStreamFactory;

    invoke-interface {v1}, Lcom/google/android/speech/audio/AudioInputStreamFactory;->createInputStream()Ljava/io/InputStream;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/speech/audio/VoiceAudioInputStreamFactory;->mSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-static {v0, v1, v2}, Lcom/google/android/speech/debug/DebugAudioLogger;->maybeWrapInLogStream(Ljava/io/InputStream;Landroid/content/Context;Lcom/google/android/speech/SpeechSettings;)Ljava/io/InputStream;

    move-result-object v1

    return-object v1
.end method
