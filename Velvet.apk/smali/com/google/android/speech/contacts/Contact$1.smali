.class final Lcom/google/android/speech/contacts/Contact$1;
.super Ljava/lang/Object;
.source "Contact.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/contacts/Contact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/speech/contacts/Contact;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/speech/contacts/Contact;
    .locals 14
    .param p1    # Landroid/os/Parcel;

    const/4 v12, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v13

    if-ne v13, v0, :cond_0

    move v8, v0

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v9

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v13

    if-ne v13, v0, :cond_1

    move v11, v0

    :goto_1
    new-instance v0, Lcom/google/android/speech/contacts/Contact;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/speech/contacts/Contact;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IZDZ)V

    return-object v0

    :cond_0
    move v8, v12

    goto :goto_0

    :cond_1
    move v11, v12

    goto :goto_1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p0, p1}, Lcom/google/android/speech/contacts/Contact$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/speech/contacts/Contact;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/google/android/speech/contacts/Contact;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/speech/contacts/Contact$1;->newArray(I)[Lcom/google/android/speech/contacts/Contact;

    move-result-object v0

    return-object v0
.end method
