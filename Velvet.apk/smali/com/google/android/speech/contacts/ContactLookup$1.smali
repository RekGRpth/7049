.class final Lcom/google/android/speech/contacts/ContactLookup$1;
.super Ljava/lang/Object;
.source "ContactLookup.java"

# interfaces
.implements Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/contacts/ContactLookup;->createRowHandlerForContact(Lcom/google/android/speech/contacts/ContactLookup$Data;Ljava/util/List;Z)Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$data:Lcom/google/android/speech/contacts/ContactLookup$Data;

.field final synthetic val$hasValues:Z

.field final synthetic val$results:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;ZLcom/google/android/speech/contacts/ContactLookup$Data;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$results:Ljava/util/List;

    iput-boolean p2, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$hasValues:Z

    iput-object p3, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$data:Lcom/google/android/speech/contacts/ContactLookup$Data;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleCurrentRow(Landroid/database/Cursor;)V
    .locals 14
    .param p1    # Landroid/database/Cursor;

    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v11, 0x0

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v12, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$results:Ljava/util/List;

    new-instance v0, Lcom/google/android/speech/contacts/Contact;

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-boolean v5, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$hasValues:Z

    if-eqz v5, :cond_1

    const/4 v5, 0x3

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_0
    iget-boolean v7, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$hasValues:Z

    if-eqz v7, :cond_0

    const/4 v6, 0x4

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    :cond_0
    iget-boolean v7, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$hasValues:Z

    if-eqz v7, :cond_2

    const/4 v7, 0x5

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :goto_1
    iget-boolean v9, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$hasValues:Z

    if-eqz v9, :cond_4

    const/4 v9, 0x6

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-eqz v9, :cond_3

    :goto_2
    iget-object v9, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$data:Lcom/google/android/speech/contacts/ContactLookup$Data;

    if-nez v9, :cond_5

    const-wide/16 v9, 0x0

    :goto_3
    iget-object v13, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$data:Lcom/google/android/speech/contacts/ContactLookup$Data;

    if-nez v13, :cond_6

    :goto_4
    invoke-direct/range {v0 .. v11}, Lcom/google/android/speech/contacts/Contact;-><init>(JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IZDZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_5
    return-void

    :cond_1
    move-object v5, v6

    goto :goto_0

    :cond_2
    move v7, v11

    goto :goto_1

    :cond_3
    move v8, v11

    goto :goto_2

    :cond_4
    move v8, v11

    goto :goto_2

    :cond_5
    iget-object v9, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$data:Lcom/google/android/speech/contacts/ContactLookup$Data;

    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Data;->getGrammarWeight()D
    invoke-static {v9}, Lcom/google/android/speech/contacts/ContactLookup$Data;->access$500(Lcom/google/android/speech/contacts/ContactLookup$Data;)D

    move-result-wide v9

    goto :goto_3

    :cond_6
    iget-object v11, p0, Lcom/google/android/speech/contacts/ContactLookup$1;->val$data:Lcom/google/android/speech/contacts/ContactLookup$Data;

    # invokes: Lcom/google/android/speech/contacts/ContactLookup$Data;->getMerged()Z
    invoke-static {v11}, Lcom/google/android/speech/contacts/ContactLookup$Data;->access$600(Lcom/google/android/speech/contacts/ContactLookup$Data;)Z

    move-result v11

    goto :goto_4

    :cond_7
    const-string v0, "ContactLookup"

    const-string v1, "Provider returned contact with no display name."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method
