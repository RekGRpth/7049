.class Lcom/google/android/speech/contacts/ContactLookup$Data;
.super Ljava/lang/Object;
.source "ContactLookup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/contacts/ContactLookup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Data"
.end annotation


# instance fields
.field private final mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

.field private final mPreferredType:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/majel/proto/ActionV2Protos$ActionContact;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ActionContact;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    iput-object p2, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mPreferredType:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/majel/proto/ActionV2Protos$ActionContact;Ljava/lang/String;Lcom/google/android/speech/contacts/ContactLookup$1;)V
    .locals 0
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$ActionContact;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/speech/contacts/ContactLookup$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/speech/contacts/ContactLookup$Data;-><init>(Lcom/google/majel/proto/ActionV2Protos$ActionContact;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/speech/contacts/ContactLookup$Data;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/speech/contacts/ContactLookup$Data;

    invoke-direct {p0}, Lcom/google/android/speech/contacts/ContactLookup$Data;->getType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/speech/contacts/ContactLookup$Data;)D
    .locals 2
    .param p0    # Lcom/google/android/speech/contacts/ContactLookup$Data;

    invoke-direct {p0}, Lcom/google/android/speech/contacts/ContactLookup$Data;->getGrammarWeight()D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$600(Lcom/google/android/speech/contacts/ContactLookup$Data;)Z
    .locals 1
    .param p0    # Lcom/google/android/speech/contacts/ContactLookup$Data;

    invoke-direct {p0}, Lcom/google/android/speech/contacts/ContactLookup$Data;->getMerged()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/speech/contacts/ContactLookup$Data;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/speech/contacts/ContactLookup$Data;

    invoke-direct {p0}, Lcom/google/android/speech/contacts/ContactLookup$Data;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getGrammarWeight()D
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasEmbeddedActionContactExtension()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getEmbeddedActionContactExtension()Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasGrammarWeight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getEmbeddedActionContactExtension()Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getGrammarWeight()D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private getMerged()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->hasEmbeddedActionContactExtension()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getEmbeddedActionContactExtension()Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->hasMerged()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getEmbeddedActionContactExtension()Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/EmbeddedAction$EmbeddedActionContact;->getMerged()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getType()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mActionContact:Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-static {v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getContactType(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactLookup$Data;->mPreferredType:Ljava/lang/String;

    :cond_0
    return-object v0
.end method
