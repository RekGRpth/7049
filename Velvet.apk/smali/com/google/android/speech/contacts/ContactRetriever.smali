.class public Lcom/google/android/speech/contacts/ContactRetriever;
.super Ljava/lang/Object;
.source "ContactRetriever.java"


# instance fields
.field protected final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .param p1    # Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/contacts/ContactRetriever;->mContentResolver:Landroid/content/ContentResolver;

    return-void
.end method


# virtual methods
.method public getContacts(Landroid/net/Uri;I[Ljava/lang/String;Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;)V
    .locals 8
    .param p1    # Landroid/net/Uri;
    .param p2    # I
    .param p3    # [Ljava/lang/String;
    .param p4    # Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;

    const/4 v4, 0x0

    const-string v6, "times_contacted DESC, last_time_contacted DESC"

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/speech/contacts/ContactRetriever;->getContacts(Landroid/net/Uri;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;)V

    return-void
.end method

.method public getContacts(Landroid/net/Uri;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # I
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;

    invoke-virtual/range {p0 .. p6}, Lcom/google/android/speech/contacts/ContactRetriever;->getCursor(Landroid/net/Uri;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {p7, v0}, Lcom/google/android/speech/contacts/Cursors;->iterateCursor(Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;Landroid/database/Cursor;)V

    return-void
.end method

.method public getCursor(Landroid/net/Uri;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # I
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/contacts/ContactRetriever;->mContentResolver:Landroid/content/ContentResolver;

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v6

    const/4 v0, 0x0

    goto :goto_0
.end method
