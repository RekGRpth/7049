.class final Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;
.super Ljava/lang/Object;
.source "RecognitionDispatcher.java"

# interfaces
.implements Lcom/google/android/speech/callback/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/dispatcher/RecognitionDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EngineMessages"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/Callback",
        "<",
        "Lcom/google/android/speech/RecognitionResponse;",
        "Lcom/google/android/speech/exception/RecognizeException;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/speech/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation
.end field

.field private mInvalid:Z

.field private final mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

.field final synthetic this$0:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;


# direct methods
.method private constructor <init>(Lcom/google/android/speech/dispatcher/RecognitionDispatcher;Lcom/google/android/speech/callback/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/android/speech/RecognitionResponse;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->this$0:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->createSameThreadCheck()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mInvalid:Z

    iput-object p2, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mCallback:Lcom/google/android/speech/callback/Callback;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/speech/dispatcher/RecognitionDispatcher;Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/dispatcher/RecognitionDispatcher$1;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/dispatcher/RecognitionDispatcher;
    .param p2    # Lcom/google/android/speech/callback/Callback;
    .param p3    # Lcom/google/android/speech/dispatcher/RecognitionDispatcher$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;-><init>(Lcom/google/android/speech/dispatcher/RecognitionDispatcher;Lcom/google/android/speech/callback/Callback;)V

    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mInvalid:Z

    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->this$0:Lcom/google/android/speech/dispatcher/RecognitionDispatcher;

    # invokes: Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->stop()V
    invoke-static {v0}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher;->access$100(Lcom/google/android/speech/dispatcher/RecognitionDispatcher;)V

    iget-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/callback/Callback;->onError(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/speech/exception/RecognizeException;

    invoke-virtual {p0, p1}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    return-void
.end method

.method public onResult(Lcom/google/android/speech/RecognitionResponse;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/RecognitionResponse;

    iget-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-boolean v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->mCallback:Lcom/google/android/speech/callback/Callback;

    invoke-interface {v0, p1}, Lcom/google/android/speech/callback/Callback;->onResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/speech/RecognitionResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/speech/dispatcher/RecognitionDispatcher$EngineMessages;->onResult(Lcom/google/android/speech/RecognitionResponse;)V

    return-void
.end method
