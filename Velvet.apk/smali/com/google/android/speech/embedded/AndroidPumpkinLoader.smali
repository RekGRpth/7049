.class public Lcom/google/android/speech/embedded/AndroidPumpkinLoader;
.super Lcom/google/speech/grammar/pumpkin/PumpkinLoader;
.source "AndroidPumpkinLoader.java"


# instance fields
.field private final mAssets:Landroid/content/res/AssetManager;

.field private final mLocale:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v0, "%s/patterns/"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%s/grms/"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    const-string v3, "%s/pumpkin_config"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    iput-object p2, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->mLocale:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->mAssets:Landroid/content/res/AssetManager;

    return-void
.end method

.method public static hasConfigForLocale(Landroid/content/res/AssetManager;Ljava/lang/String;)Z
    .locals 6
    .param p0    # Landroid/content/res/AssetManager;
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "%s/pumpkin_config"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object p1, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v2

    :catch_0
    move-exception v0

    move v2, v3

    goto :goto_0
.end method

.method private static loadLibrary()V
    .locals 0

    invoke-static {}, Lcom/google/android/speech/embedded/Greco3Recognizer;->maybeLoadSharedLibrary()V

    return-void
.end method


# virtual methods
.method public createActionFrame(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;)Lcom/google/speech/grammar/pumpkin/ActionFrame;
    .locals 7
    .param p1    # Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;->getActionList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;

    invoke-virtual {v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->hasFarFilename()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getFarFilename()Ljava/lang/String;

    move-result-object v2

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->actionPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".far"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->mAssets:Landroid/content/res/AssetManager;

    invoke-virtual {v5, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    :try_start_0
    invoke-static {v4}, Lcom/google/common/io/ByteStreams;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/protobuf/micro/ByteStringMicro;->copyFrom([B)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->setFarData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionConfig;->getActionName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    throw v5

    :cond_1
    sget-object v5, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->actionFrameManager:Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

    invoke-virtual {v5, p1}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->createActionFrame(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v5

    return-object v5
.end method

.method public init()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->loadLibrary()V

    invoke-super {p0}, Lcom/google/speech/grammar/pumpkin/PumpkinLoader;->init()V

    return-void
.end method

.method protected loadActionSetConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->mAssets:Landroid/content/res/AssetManager;

    const-string v4, "%s/action_config"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->mLocale:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    :try_start_0
    invoke-static {v2}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v1

    invoke-static {v1}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;->parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v3
.end method

.method protected loadPumpkinConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v6, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->mAssets:Landroid/content/res/AssetManager;

    iget-object v7, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->pumpkinConfig:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    :try_start_0
    invoke-static {v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v0

    invoke-static {v0}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->parseFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinConfig;->getValidatorManagerConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ValidatorManagerConfig;->getGrammarList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;

    iget-object v6, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->mAssets:Landroid/content/res/AssetManager;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "%s/grms/"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->mLocale:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->getFilename()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".far"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :try_start_1
    invoke-static {v2}, Lcom/google/common/io/ByteStreams;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v6

    invoke-static {v6}, Lcom/google/protobuf/micro/ByteStringMicro;->copyFrom([B)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;->setData(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$PumpkinResource;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v6

    :catchall_1
    move-exception v6

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_0
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v4
.end method
