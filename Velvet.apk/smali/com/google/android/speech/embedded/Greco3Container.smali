.class public Lcom/google/android/speech/embedded/Greco3Container;
.super Ljava/lang/Object;
.source "Greco3Container.java"


# instance fields
.field private final mDeviceClassSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field private final mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

.field private final mGreco3Preferences:Lcom/google/android/speech/embedded/Greco3Preferences;


# direct methods
.method private constructor <init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/embedded/Greco3EngineManager;Lcom/google/android/speech/embedded/Greco3Preferences;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p2    # Lcom/google/android/speech/embedded/Greco3EngineManager;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Preferences;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/embedded/Greco3DataManager;",
            "Lcom/google/android/speech/embedded/Greco3EngineManager;",
            "Lcom/google/android/speech/embedded/Greco3Preferences;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/embedded/Greco3Container;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iput-object p2, p0, Lcom/google/android/speech/embedded/Greco3Container;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    iput-object p3, p0, Lcom/google/android/speech/embedded/Greco3Container;->mGreco3Preferences:Lcom/google/android/speech/embedded/Greco3Preferences;

    iput-object p4, p0, Lcom/google/android/speech/embedded/Greco3Container;->mDeviceClassSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method public static create(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/util/concurrent/ExecutorService;)Lcom/google/android/speech/embedded/Greco3Container;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/speech/embedded/DeviceClassSupplier;

    invoke-direct {v1, p0}, Lcom/google/android/speech/embedded/DeviceClassSupplier;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/speech/embedded/Greco3Preferences;

    invoke-direct {v3, p1}, Lcom/google/android/speech/embedded/Greco3Preferences;-><init>(Landroid/content/SharedPreferences;)V

    new-instance v0, Lcom/google/android/speech/embedded/Greco3DataManager;

    const/4 v4, 0x2

    invoke-direct {v0, p0, v3, v4, p2}, Lcom/google/android/speech/embedded/Greco3DataManager;-><init>(Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3Preferences;ILjava/util/concurrent/Executor;)V

    new-instance v2, Lcom/google/android/speech/embedded/Greco3EngineManager;

    new-instance v4, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/voicesearch/greco3/BundledEndpointerModelCopier;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/speech/embedded/Greco3EngineManager;-><init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/embedded/Greco3Preferences;Lcom/google/android/speech/embedded/EndpointerModelCopier;)V

    invoke-virtual {v0, v2}, Lcom/google/android/speech/embedded/Greco3DataManager;->setPathDeleter(Lcom/google/android/speech/embedded/Greco3DataManager$PathDeleter;)V

    new-instance v4, Lcom/google/android/speech/embedded/Greco3Container;

    invoke-direct {v4, v0, v2, v3, v1}, Lcom/google/android/speech/embedded/Greco3Container;-><init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/embedded/Greco3EngineManager;Lcom/google/android/speech/embedded/Greco3Preferences;Lcom/google/common/base/Supplier;)V

    return-object v4
.end method


# virtual methods
.method public getDeviceClassSupplier()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Container;->mDeviceClassSupplier:Lcom/google/common/base/Supplier;

    return-object v0
.end method

.method public getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Container;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    return-object v0
.end method

.method public getGreco3EngineManager()Lcom/google/android/speech/embedded/Greco3EngineManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Container;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    return-object v0
.end method

.method public getGreco3Preferences()Lcom/google/android/speech/embedded/Greco3Preferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/embedded/Greco3Container;->mGreco3Preferences:Lcom/google/android/speech/embedded/Greco3Preferences;

    return-object v0
.end method
