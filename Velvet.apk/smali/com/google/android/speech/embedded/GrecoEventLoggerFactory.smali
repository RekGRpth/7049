.class public Lcom/google/android/speech/embedded/GrecoEventLoggerFactory;
.super Ljava/lang/Object;
.source "GrecoEventLoggerFactory.java"

# interfaces
.implements Lcom/google/android/speech/embedded/GrecoEventLogger$Factory;


# static fields
.field private static mDefaultEventLogger:Lcom/google/android/speech/embedded/GrecoEventLogger;

.field private static mHotwordEventLogger:Lcom/google/android/speech/embedded/GrecoEventLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory$1;

    invoke-direct {v0}, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory$1;-><init>()V

    sput-object v0, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory;->mHotwordEventLogger:Lcom/google/android/speech/embedded/GrecoEventLogger;

    new-instance v0, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory$2;

    invoke-direct {v0}, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory$2;-><init>()V

    sput-object v0, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory;->mDefaultEventLogger:Lcom/google/android/speech/embedded/GrecoEventLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEventLoggerForMode(Lcom/google/android/speech/embedded/Greco3Mode;)Lcom/google/android/speech/embedded/GrecoEventLogger;
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Mode;

    invoke-virtual {p1}, Lcom/google/android/speech/embedded/Greco3Mode;->isEndpointerMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->HOTWORD:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory;->mHotwordEventLogger:Lcom/google/android/speech/embedded/GrecoEventLogger;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/speech/embedded/GrecoEventLoggerFactory;->mDefaultEventLogger:Lcom/google/android/speech/embedded/GrecoEventLogger;

    goto :goto_0
.end method
