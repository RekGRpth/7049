.class Lcom/google/android/speech/embedded/OfflineActionsManager$1;
.super Ljava/lang/Object;
.source "OfflineActionsManager.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/embedded/OfflineActionsManager;->createInitGrammarCallback([Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/callback/SimpleCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

.field final synthetic val$grammars:[Lcom/google/android/speech/embedded/Greco3Grammar;


# direct methods
.method constructor <init>(Lcom/google/android/speech/embedded/OfflineActionsManager;[Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$1;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iput-object p2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$1;->val$grammars:[Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/speech/embedded/OfflineActionsManager$1;->onResult(Ljava/lang/Void;)V

    return-void
.end method

.method public onResult(Ljava/lang/Void;)V
    .locals 2
    .param p1    # Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager$1;->this$0:Lcom/google/android/speech/embedded/OfflineActionsManager;

    # getter for: Lcom/google/android/speech/embedded/OfflineActionsManager;->mMainThread:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/speech/embedded/OfflineActionsManager;->access$100(Lcom/google/android/speech/embedded/OfflineActionsManager;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/speech/embedded/OfflineActionsManager$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/speech/embedded/OfflineActionsManager$1$1;-><init>(Lcom/google/android/speech/embedded/OfflineActionsManager$1;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
