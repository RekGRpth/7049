.class public final Lcom/google/android/speech/embedded/OfflineActionsManager$GrammarCompilationException;
.super Lcom/google/android/speech/exception/EmbeddedRecognizeException;
.source "OfflineActionsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/embedded/OfflineActionsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GrammarCompilationException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1493cb3625ac3140L


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Grammar compilation error."

    invoke-direct {p0, v0}, Lcom/google/android/speech/exception/EmbeddedRecognizeException;-><init>(Ljava/lang/String;)V

    return-void
.end method
