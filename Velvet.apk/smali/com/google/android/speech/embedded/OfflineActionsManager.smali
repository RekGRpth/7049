.class public Lcom/google/android/speech/embedded/OfflineActionsManager;
.super Ljava/lang/Object;
.source "OfflineActionsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/embedded/OfflineActionsManager$GrammarCompilationException;
    }
.end annotation


# instance fields
.field private volatile mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mCompilingGrammars:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/speech/embedded/Greco3Grammar;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mGrammarCompilationLocale:Ljava/lang/String;

.field private final mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field private final mMainThread:Ljava/util/concurrent/Executor;

.field private final mSettings:Lcom/google/android/speech/SpeechSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/SpeechSettings;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/speech/embedded/Greco3DataManager;
    .param p3    # Lcom/google/android/speech/SpeechSettings;
    .param p4    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iput-object p3, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mSettings:Lcom/google/android/speech/SpeechSettings;

    iput-object p4, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mMainThread:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/speech/embedded/OfflineActionsManager;[Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 0
    .param p0    # Lcom/google/android/speech/embedded/OfflineActionsManager;
    .param p1    # [Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-direct {p0, p1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->initGrammars([Lcom/google/android/speech/embedded/Greco3Grammar;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/speech/embedded/OfflineActionsManager;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mMainThread:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/speech/embedded/OfflineActionsManager;)Lcom/google/android/speech/callback/SimpleCallback;
    .locals 1
    .param p0    # Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/speech/embedded/OfflineActionsManager;Lcom/google/android/speech/callback/SimpleCallback;)Lcom/google/android/speech/callback/SimpleCallback;
    .locals 0
    .param p0    # Lcom/google/android/speech/embedded/OfflineActionsManager;
    .param p1    # Lcom/google/android/speech/callback/SimpleCallback;

    iput-object p1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/speech/embedded/OfflineActionsManager;)Lcom/google/android/speech/embedded/Greco3DataManager;
    .locals 1
    .param p0    # Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/speech/embedded/OfflineActionsManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/speech/embedded/OfflineActionsManager;Lcom/google/android/speech/embedded/Greco3Grammar;)J
    .locals 2
    .param p0    # Lcom/google/android/speech/embedded/OfflineActionsManager;
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-direct {p0, p1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->getGrammarCompilationFrequency(Lcom/google/android/speech/embedded/Greco3Grammar;)J

    move-result-wide v0

    return-wide v0
.end method

.method private canCompileGrammar()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iget-object v1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGrammarCompilationLocale:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasResourcesForCompilation(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private varargs createInitGrammarCallback([Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/callback/SimpleCallback;
    .locals 1
    .param p1    # [Lcom/google/android/speech/embedded/Greco3Grammar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/speech/embedded/Greco3Grammar;",
            ")",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/speech/embedded/OfflineActionsManager$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/speech/embedded/OfflineActionsManager$1;-><init>(Lcom/google/android/speech/embedded/OfflineActionsManager;[Lcom/google/android/speech/embedded/Greco3Grammar;)V

    return-object v0
.end method

.method private dispatchCallbackOnMainThread(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mMainThread:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/speech/embedded/OfflineActionsManager$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/speech/embedded/OfflineActionsManager$2;-><init>(Lcom/google/android/speech/embedded/OfflineActionsManager;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getGrammarCompilationFrequency(Lcom/google/android/speech/embedded/Greco3Grammar;)J
    .locals 3
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Grammar;->CONTACT_DIALING:Lcom/google/android/speech/embedded/Greco3Grammar;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasEmbeddedRecognizer()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;->hasGrammarCompilationFrequencyMs()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EmbeddedRecognizer;->getGrammarCompilationFrequencyMs()I

    move-result v1

    int-to-long v1, v1

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method private hasCompiledGrammar(Lcom/google/android/speech/embedded/Greco3Grammar;)Z
    .locals 2
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    iget-object v1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGrammarCompilationLocale:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasCompiledGrammar(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Z

    move-result v0

    return v0
.end method

.method private initGrammar(Lcom/google/android/speech/embedded/Greco3Grammar;)I
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p0}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkHoldsLock(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/speech/embedded/OfflineActionsManager;->canCompileGrammar()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->hasCompiledGrammar(Lcom/google/android/speech/embedded/Greco3Grammar;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->startGrammarCompilation(Lcom/google/android/speech/embedded/Greco3Grammar;)V

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private varargs declared-synchronized initGrammars([Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 7
    .param p1    # [Lcom/google/android/speech/embedded/Greco3Grammar;

    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    move-object v0, p1

    :try_start_1
    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    invoke-direct {p0, v1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->initGrammar(Lcom/google/android/speech/embedded/Greco3Grammar;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_2
    :pswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :pswitch_2
    :try_start_2
    iget-object v5, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private internalMaybeScheduleGrammarCompilation(Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-static {}, Lcom/google/android/speech/grammar/GrammarCompilationService;->isGrammarCompilationAlarmSet()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/speech/embedded/OfflineActionsManager$3;

    invoke-direct {v0, p0, p1, p3}, Lcom/google/android/speech/embedded/OfflineActionsManager$3;-><init>(Lcom/google/android/speech/embedded/OfflineActionsManager;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private startGrammarCompilation(Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-static {p0}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkHoldsLock(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGrammarCompilationLocale:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/android/speech/grammar/GrammarCompilationService;->startCompilationForLocale(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized detach(Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public varargs maybeScheduleGrammarCompilation(Ljava/lang/String;Ljava/util/concurrent/Executor;[Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # [Lcom/google/android/speech/embedded/Greco3Grammar;

    move-object v0, p3

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->internalMaybeScheduleGrammarCompilation(Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/google/android/speech/embedded/Greco3Grammar;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public declared-synchronized notifyDone(Lcom/google/android/speech/embedded/Greco3Grammar;Z)V
    .locals 1
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;
    .param p2    # Z

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    if-nez p2, :cond_2

    :try_start_1
    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/speech/embedded/OfflineActionsManager;->dispatchCallbackOnMainThread(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/speech/embedded/OfflineActionsManager;->dispatchCallbackOnMainThread(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized notifyStart(Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/embedded/Greco3Grammar;

    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public varargs declared-synchronized startOfflineDataCheck(Lcom/google/android/speech/callback/SimpleCallback;Ljava/lang/String;[Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 2
    .param p2    # Ljava/lang/String;
    .param p3    # [Lcom/google/android/speech/embedded/Greco3Grammar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Lcom/google/android/speech/embedded/Greco3Grammar;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iput-object p2, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGrammarCompilationLocale:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCallback:Lcom/google/android/speech/callback/SimpleCallback;

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mCompilingGrammars:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/speech/embedded/OfflineActionsManager;->mGreco3DataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-direct {p0, p3}, Lcom/google/android/speech/embedded/OfflineActionsManager;->createInitGrammarCallback([Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/callback/SimpleCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/speech/embedded/Greco3DataManager;->maybeInitialize(Lcom/google/android/speech/callback/SimpleCallback;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p3}, Lcom/google/android/speech/embedded/OfflineActionsManager;->initGrammars([Lcom/google/android/speech/embedded/Greco3Grammar;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
