.class Lcom/google/android/speech/embedded/PumpkinTagger$1;
.super Ljava/lang/Object;
.source "PumpkinTagger.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/embedded/PumpkinTagger;->tagAsync(Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/google/android/speech/callback/SimpleCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/speech/embedded/PumpkinTagger;

.field final synthetic val$callback:Lcom/google/android/speech/callback/SimpleCallback;

.field final synthetic val$callbackExecutor:Ljava/util/concurrent/Executor;

.field final synthetic val$input:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/speech/embedded/PumpkinTagger;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/embedded/PumpkinTagger$1;->this$0:Lcom/google/android/speech/embedded/PumpkinTagger;

    iput-object p2, p0, Lcom/google/android/speech/embedded/PumpkinTagger$1;->val$input:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/speech/embedded/PumpkinTagger$1;->val$callbackExecutor:Ljava/util/concurrent/Executor;

    iput-object p4, p0, Lcom/google/android/speech/embedded/PumpkinTagger$1;->val$callback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/speech/embedded/PumpkinTagger$1;->this$0:Lcom/google/android/speech/embedded/PumpkinTagger;

    iget-object v2, p0, Lcom/google/android/speech/embedded/PumpkinTagger$1;->val$input:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/speech/embedded/PumpkinTagger;->tag(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/embedded/PumpkinTagger$1;->val$callbackExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/speech/embedded/PumpkinTagger$1$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/speech/embedded/PumpkinTagger$1$1;-><init>(Lcom/google/android/speech/embedded/PumpkinTagger$1;Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
