.class public Lcom/google/android/speech/embedded/PumpkinTagger;
.super Ljava/lang/Object;
.source "PumpkinTagger.java"


# instance fields
.field private mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

.field private final mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

.field private final mContactRetriever:Lcom/google/android/speech/grammar/GrammarContactRetriever;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mInitialized:Z

.field private final mPumpkinLoader:Lcom/google/android/speech/embedded/AndroidPumpkinLoader;

.field private mTagger:Lcom/google/speech/grammar/pumpkin/Tagger;

.field private mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;


# direct methods
.method constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/embedded/AndroidPumpkinLoader;Lcom/google/android/speech/grammar/GrammarContactRetriever;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
    .param p2    # Lcom/google/android/speech/embedded/AndroidPumpkinLoader;
    .param p3    # Lcom/google/android/speech/grammar/GrammarContactRetriever;
    .param p4    # Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mInitialized:Z

    iput-object p1, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mPumpkinLoader:Lcom/google/android/speech/embedded/AndroidPumpkinLoader;

    iput-object p3, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mContactRetriever:Lcom/google/android/speech/grammar/GrammarContactRetriever;

    iput-object p4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    return-void
.end method

.method public static createIfAvailable(Ljava/util/concurrent/ExecutorService;Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/speech/embedded/PumpkinTagger;
    .locals 4
    .param p0    # Ljava/util/concurrent/ExecutorService;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->hasConfigForLocale(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/speech/embedded/PumpkinTagger;

    new-instance v1, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;

    invoke-direct {v1, p1, p2}, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/speech/grammar/GrammarContactRetriever;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/speech/grammar/GrammarContactRetriever;-><init>(Landroid/content/ContentResolver;)V

    invoke-static {p1}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->fromContext(Landroid/content/Context;)Lcom/google/android/voicesearch/util/AppSelectionHelper;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/speech/embedded/PumpkinTagger;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/embedded/AndroidPumpkinLoader;Lcom/google/android/speech/grammar/GrammarContactRetriever;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 6

    :try_start_0
    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mPumpkinLoader:Lcom/google/android/speech/embedded/AndroidPumpkinLoader;

    invoke-virtual {v4}, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->init()V

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mPumpkinLoader:Lcom/google/android/speech/embedded/AndroidPumpkinLoader;

    invoke-virtual {v4}, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->getTagger()Lcom/google/speech/grammar/pumpkin/Tagger;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mTagger:Lcom/google/speech/grammar/pumpkin/Tagger;

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mPumpkinLoader:Lcom/google/android/speech/embedded/AndroidPumpkinLoader;

    invoke-virtual {v4}, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->getUserValidators()Lcom/google/speech/grammar/pumpkin/UserValidators;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mPumpkinLoader:Lcom/google/android/speech/embedded/AndroidPumpkinLoader;

    invoke-virtual {v4}, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->loadActionSetConfig()Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mPumpkinLoader:Lcom/google/android/speech/embedded/AndroidPumpkinLoader;

    invoke-virtual {v4, v0}, Lcom/google/android/speech/embedded/AndroidPumpkinLoader;->createActionFrame(Lcom/google/speech/grammar/pumpkin/PumpkinConfigProto$ActionSetConfig;)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mContactRetriever:Lcom/google/android/speech/grammar/GrammarContactRetriever;

    invoke-virtual {v4}, Lcom/google/android/speech/grammar/GrammarContactRetriever;->getLowerCaseContactNames()[Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    invoke-virtual {v4, v2}, Lcom/google/speech/grammar/pumpkin/UserValidators;->setContacts([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {v4}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->getAllAppNamesLowerCase()[Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v5, "APP"

    invoke-virtual {v4, v5, v1}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addUserValidator(Ljava/lang/String;[Ljava/lang/String;)V

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mInitialized:Z

    :goto_0
    return-void

    :catch_0
    move-exception v3

    const-string v4, "PumpkinTagger"

    const-string v5, "Couldn\'t load configuration assets."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public tag(Ljava/lang/String;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mInitialized:Z

    if-nez v1, :cond_0

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    invoke-direct {p0}, Lcom/google/android/speech/embedded/PumpkinTagger;->init()V

    const/16 v1, 0x61

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :cond_0
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    iget-object v1, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mTagger:Lcom/google/speech/grammar/pumpkin/Tagger;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    iget-object v4, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/speech/grammar/pumpkin/Tagger;->tag(Ljava/lang/String;Lcom/google/speech/grammar/pumpkin/ActionFrame;Lcom/google/speech/grammar/pumpkin/UserValidators;)Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;

    move-result-object v0

    const/16 v1, 0x60

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-object v0
.end method

.method public tagAsync(Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Lcom/google/speech/grammar/pumpkin/PumpkinTaggerResultsProto$PumpkinTaggerResults;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/embedded/PumpkinTagger;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/speech/embedded/PumpkinTagger$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/speech/embedded/PumpkinTagger$1;-><init>(Lcom/google/android/speech/embedded/PumpkinTagger;Ljava/lang/String;Ljava/util/concurrent/Executor;Lcom/google/android/speech/callback/SimpleCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
