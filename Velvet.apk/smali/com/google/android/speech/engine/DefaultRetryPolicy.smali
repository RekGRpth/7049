.class public Lcom/google/android/speech/engine/DefaultRetryPolicy;
.super Ljava/lang/Object;
.source "DefaultRetryPolicy.java"

# interfaces
.implements Lcom/google/android/speech/engine/RetryPolicy;


# instance fields
.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mCounter:I

.field private mMaxRetryTimeoutMsec:I

.field private final mNetworkRecognizer:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;",
            ">;"
        }
    .end annotation
.end field

.field private mRecognitionStartedTimestamp:J


# direct methods
.method public constructor <init>(Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/util/Clock;)V
    .locals 0
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;",
            ">;",
            "Lcom/google/android/searchcommon/util/Clock;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mNetworkRecognizer:Lcom/google/common/base/Supplier;

    iput-object p2, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-virtual {p0}, Lcom/google/android/speech/engine/DefaultRetryPolicy;->reset()V

    return-void
.end method


# virtual methods
.method public declared-synchronized canRetry(Lcom/google/android/speech/exception/RecognizeException;)Z
    .locals 6
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mCounter:I

    if-nez v0, :cond_0

    const/16 v0, 0x1b

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mCounter:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mNetworkRecognizer:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;->getMaxRetries()I

    move-result v0

    iput v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mCounter:I

    iget-object v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mNetworkRecognizer:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;->getMaxRetryTimeoutMsec()I

    move-result v0

    iput v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mMaxRetryTimeoutMsec:I

    :cond_1
    iget-wide v2, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mRecognitionStartedTimestamp:J

    iget v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mMaxRetryTimeoutMsec:I

    int-to-long v4, v0

    add-long/2addr v2, v4

    iget-object v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    const/16 v0, 0x1c

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/google/android/speech/exception/NetworkRecognizeException;

    if-nez v0, :cond_3

    instance-of v0, p1, Lcom/google/android/speech/exception/AuthFailureException;

    if-eqz v0, :cond_5

    :cond_3
    iget v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mCounter:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mCounter:I

    instance-of v0, p1, Lcom/google/android/speech/exception/AuthFailureException;

    if-eqz v0, :cond_4

    const/16 v0, 0x1a

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/16 v0, 0x19

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public declared-synchronized equivalentToError(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/android/speech/exception/RecognizeException;
    .locals 2
    .param p1    # Lcom/google/speech/s3/S3$S3Response;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getErrorCode()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/speech/exception/AuthFailureException;->isAuthErrorCode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/speech/exception/AuthFailureException;

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getErrorCode()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/speech/exception/AuthFailureException;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mCounter:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 2

    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mCounter:I

    iget-object v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/speech/engine/DefaultRetryPolicy;->mRecognitionStartedTimestamp:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
