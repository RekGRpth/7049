.class public Lcom/google/android/speech/exception/EmbeddedRecognizeException;
.super Lcom/google/android/speech/exception/RecognizeException;
.source "EmbeddedRecognizeException.java"


# static fields
.field private static final serialVersionUID:J = -0x134d07fb4947f61bL


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/speech/exception/RecognizeException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    invoke-direct {p0, p1, p2}, Lcom/google/android/speech/exception/RecognizeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method
