.class public abstract Lcom/google/android/speech/grammar/GrammarBuilder;
.super Ljava/lang/Object;
.source "GrammarBuilder.java"


# static fields
.field private static final ABNF_RESERVED_TOKENS:Ljava/util/regex/Pattern;

.field private static final DEBUG:Z = false

.field public static final SEMANTIC_INTERP_PREFIX:Ljava/lang/String; = "XX_"

.field private static final TAG:Ljava/lang/String; = "GrammarBuilder"

.field private static final WHITE_SPACE_SPLITTER:Lcom/google/common/base/Splitter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(Lcom/google/common/base/CharMatcher;)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/google/android/speech/grammar/GrammarBuilder;->WHITE_SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    const-string v0, "[\\Q/|*+?=;[]()<>${}\"\\\\E]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/speech/grammar/GrammarBuilder;->ABNF_RESERVED_TOKENS:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;

    const-string v2, "_"

    const-string v3, "XX_"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xb

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    return-object v2
.end method

.method public static decodeWeight(Ljava/lang/String;)D
    .locals 4
    .param p0    # Ljava/lang/String;

    const-string v1, "_"

    const-string v2, "XX_"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "XX_"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x50

    const/16 v3, 0x2e

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    return-wide v1
.end method

.method static getWords(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lcom/google/android/speech/grammar/GrammarBuilder;->stripAbnfTokens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/grammar/GrammarBuilder;->WHITE_SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v1, v0}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterators;->toArray(Ljava/util/Iterator;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method static stripAbnfTokens(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/speech/grammar/GrammarBuilder;->ABNF_RESERVED_TOKENS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public append(Ljava/lang/StringBuilder;)V
    .locals 5
    .param p1    # Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/speech/grammar/GrammarBuilder;->getGrammarTokens()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/speech/grammar/GrammarBuilder;->appendEmptyTokensRules(Ljava/lang/StringBuilder;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/speech/grammar/GrammarBuilder;->appendBeforeDisjunctionRules(Ljava/lang/StringBuilder;)V

    invoke-virtual {p0, p1}, Lcom/google/android/speech/grammar/GrammarBuilder;->appendDisjunctionAssignment(Ljava/lang/StringBuilder;)V

    const/4 v0, 0x1

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/grammar/GrammarToken;

    if-nez v0, :cond_1

    const-string v4, " | "

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v1, p1}, Lcom/google/android/speech/grammar/GrammarToken;->append(Ljava/lang/StringBuilder;)V

    goto :goto_1

    :cond_2
    const-string v4, ";\n"

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected abstract appendBeforeDisjunctionRules(Ljava/lang/StringBuilder;)V
.end method

.method protected abstract appendDisjunctionAssignment(Ljava/lang/StringBuilder;)V
.end method

.method protected abstract appendEmptyTokensRules(Ljava/lang/StringBuilder;)V
.end method

.method protected abstract getGrammarTokens()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/speech/grammar/GrammarToken;",
            ">;"
        }
    .end annotation
.end method
