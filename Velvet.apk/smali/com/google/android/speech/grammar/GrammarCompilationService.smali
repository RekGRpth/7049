.class public Lcom/google/android/speech/grammar/GrammarCompilationService;
.super Landroid/app/IntentService;
.source "GrammarCompilationService.java"


# static fields
.field private static sGrammarCompilationAlarmSet:Z


# instance fields
.field private mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

.field private mMd5Digest:Ljava/security/MessageDigest;

.field private mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

.field private mSpeechSettings:Lcom/google/android/speech/SpeechSettings;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-string v2, ""

    invoke-direct {p0, v2}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    :try_start_0
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mMd5Digest:Ljava/security/MessageDigest;

    return-void

    :catch_0
    move-exception v1

    const-string v2, "VS.GrammarCompilationService"

    const-string v3, "MD5 message digests not supported."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static createNewRevisionId()Ljava/lang/String;
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "v"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private digest(Ljava/lang/String;)[B
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mMd5Digest:Ljava/security/MessageDigest;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mMd5Digest:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mMd5Digest:Ljava/security/MessageDigest;

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    goto :goto_0
.end method

.method private doCompile(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Z
    .locals 18
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Grammar;

    const-string v13, "VS.GrammarCompilationService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Compiling grammar for: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", type="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

    invoke-virtual {v13}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3Preferences()Lcom/google/android/speech/embedded/Greco3Preferences;

    move-result-object v8

    new-instance v2, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

    invoke-virtual {v13}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v13

    new-instance v14, Lcom/google/android/speech/grammar/GrammarContactRetriever;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    invoke-direct {v14, v15}, Lcom/google/android/speech/grammar/GrammarContactRetriever;-><init>(Landroid/content/ContentResolver;)V

    new-instance v15, Lcom/google/android/speech/grammar/TextGrammarLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getPackageName()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v15 .. v17}, Lcom/google/android/speech/grammar/TextGrammarLoader;-><init>(Landroid/content/res/Resources;Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-direct {v2, v13, v14, v15, v0}, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;-><init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/grammar/GrammarContactRetriever;Lcom/google/android/speech/grammar/TextGrammarLoader;Lcom/google/android/speech/embedded/Greco3Grammar;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

    invoke-virtual {v13}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v4

    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Lcom/google/android/speech/embedded/Greco3DataManager;->blockingUpdateResources(Z)V

    sget-object v13, Lcom/google/android/speech/embedded/Greco3Mode;->COMPILER:Lcom/google/android/speech/embedded/Greco3Mode;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v13}, Lcom/google/android/speech/embedded/Greco3DataManager;->hasResources(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Mode;)Z

    move-result v13

    if-nez v13, :cond_0

    const-string v13, "VS.GrammarCompilationService"

    const-string v14, "No grammar compilation resources, aborting."

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x0

    :goto_0
    return v13

    :cond_0
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/speech/embedded/Greco3DataManager;->getResources(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/google/android/speech/embedded/Greco3Preferences;->getCompiledGrammarRevisionId(Lcom/google/android/speech/embedded/Greco3Grammar;)Ljava/lang/String;

    move-result-object v10

    const/4 v9, 0x0

    if-eqz v10, :cond_1

    move-object/from16 v0, p2

    invoke-interface {v11, v0, v10}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getGrammarPath(Lcom/google/android/speech/embedded/Greco3Grammar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getDigestForPath(Ljava/lang/String;)[B

    move-result-object v9

    :cond_1
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->buildGrammar(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    const/4 v13, 0x0

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    move-object/from16 v0, p0

    invoke-static {v5, v0, v13}, Lcom/google/android/speech/debug/DebugAudioLogger;->maybeDumpGrammar(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/speech/SpeechSettings;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/speech/grammar/GrammarCompilationService;->digest(Ljava/lang/String;)[B

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {v3, v9}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v13

    if-eqz v13, :cond_3

    const/4 v13, 0x1

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/google/android/speech/grammar/GrammarCompilationService;->createNewRevisionId()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v4, v0, v1, v12}, Lcom/google/android/speech/embedded/Greco3DataManager;->createOutputPathForGrammar(Lcom/google/android/speech/embedded/Greco3Grammar;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    if-nez v7, :cond_4

    const-string v13, "VS.GrammarCompilationService"

    const-string v14, "Unable to create output directory: dir is null"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x0

    goto :goto_0

    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v2, v5, v0, v7}, Lcom/google/android/speech/grammar/HandsFreeGrammarCompiler;->compileGrammar(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z

    move-result v13

    if-nez v13, :cond_5

    const/4 v13, 0x0

    goto :goto_0

    :cond_5
    :try_start_0
    new-instance v13, Ljava/io/File;

    const-string v14, "digest"

    invoke-direct {v13, v7, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v3, v13}, Lcom/google/common/io/Files;->write([BLjava/io/File;)V

    invoke-interface {v11}, Lcom/google/android/speech/embedded/Greco3DataManager$LocaleResources;->getLanguageMetadata()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->toByteArray()[B

    move-result-object v13

    new-instance v14, Ljava/io/File;

    const-string v15, "metadata"

    invoke-direct {v14, v7, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v13, v14}, Lcom/google/common/io/Files;->write([BLjava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p2

    invoke-virtual {v8, v0, v12}, Lcom/google/android/speech/embedded/Greco3Preferences;->setCompiledGrammarRevisionId(Lcom/google/android/speech/embedded/Greco3Grammar;Ljava/lang/String;)V

    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Lcom/google/android/speech/embedded/Greco3DataManager;->blockingUpdateResources(Z)V

    const/4 v13, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v6

    const-string v13, "VS.GrammarCompilationService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error writing compiled digest/metadata :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method private getDigestForPath(Ljava/lang/String;)[B
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "digest"

    invoke-direct {v2, p1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/common/io/Files;->toByteArray(Ljava/io/File;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static getPendingIntentForLocale(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Landroid/app/PendingIntent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Grammar;

    const/4 v0, 0x0

    invoke-static {p0, p1, p2}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getStartServiceIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static getStartServiceIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/speech/grammar/GrammarCompilationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "compilation_locale"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "grammar_type"

    invoke-virtual {p2}, Lcom/google/android/speech/embedded/Greco3Grammar;->getDirectoryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static declared-synchronized isGrammarCompilationAlarmSet()Z
    .locals 2

    const-class v0, Lcom/google/android/speech/grammar/GrammarCompilationService;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/google/android/speech/grammar/GrammarCompilationService;->sGrammarCompilationAlarmSet:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static isStaleRevision(Ljava/lang/String;)Z
    .locals 8
    .param p0    # Ljava/lang/String;

    const/4 v3, 0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const-wide/16 v1, -0x1

    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-gez v4, :cond_2

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid revisionId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", negative."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid revisionId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x2932e000

    sub-long/2addr v4, v6

    cmp-long v4, v1, v4

    if-ltz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized maybeSchedulePeriodicCompilation(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;J)V
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/speech/embedded/Greco3Grammar;
    .param p4    # J

    const-class v7, Lcom/google/android/speech/grammar/GrammarCompilationService;

    monitor-enter v7

    :try_start_0
    sget-boolean v1, Lcom/google/android/speech/grammar/GrammarCompilationService;->sGrammarCompilationAlarmSet:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->isStaleRevision(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit v7

    return-void

    :cond_1
    :try_start_1
    invoke-static {p1, p2, p3}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getPendingIntentForLocale(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Landroid/app/PendingIntent;

    move-result-object v6

    const-string v1, "alarm"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v1, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x1b7740

    add-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-lez v4, :cond_2

    move-wide v4, p4

    :goto_1
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/speech/grammar/GrammarCompilationService;->sGrammarCompilationAlarmSet:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v7

    throw v1

    :cond_2
    const-wide/32 v4, 0x240c8400

    goto :goto_1
.end method

.method public static startCompilationForLocale(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/embedded/Greco3Grammar;

    invoke-static {p0, p1, p2}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getStartServiceIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getOfflineActionsManager()Lcom/google/android/speech/embedded/OfflineActionsManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/16 v3, 0xa

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Lcom/google/android/speech/embedded/Greco3Recognizer;->maybeLoadSharedLibrary()V

    const-string v3, "compilation_locale"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "grammar_type"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/speech/embedded/Greco3Grammar;->fromDirectoryName(Ljava/lang/String;)Lcom/google/android/speech/embedded/Greco3Grammar;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    invoke-virtual {v3, v0}, Lcom/google/android/speech/embedded/OfflineActionsManager;->notifyStart(Lcom/google/android/speech/embedded/Greco3Grammar;)V

    const/4 v2, 0x0

    const/4 v3, 0x2

    :try_start_0
    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    invoke-direct {p0, v1, v0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->doCompile(Ljava/lang/String;Lcom/google/android/speech/embedded/Greco3Grammar;)Z

    move-result v2

    const/16 v3, 0x52

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v3, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/speech/embedded/OfflineActionsManager;->notifyDone(Lcom/google/android/speech/embedded/Greco3Grammar;Z)V

    return-void

    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    invoke-virtual {v4, v0, v2}, Lcom/google/android/speech/embedded/OfflineActionsManager;->notifyDone(Lcom/google/android/speech/embedded/Greco3Grammar;Z)V

    throw v3
.end method
