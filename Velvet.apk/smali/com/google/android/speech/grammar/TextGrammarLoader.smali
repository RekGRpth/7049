.class public Lcom/google/android/speech/grammar/TextGrammarLoader;
.super Ljava/lang/Object;
.source "TextGrammarLoader.java"


# instance fields
.field private final mPackageName:Ljava/lang/String;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/grammar/TextGrammarLoader;->mResources:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/google/android/speech/grammar/TextGrammarLoader;->mPackageName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;I)Ljava/lang/StringBuilder;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/speech/grammar/TextGrammarLoader;->mResources:Landroid/content/res/Resources;

    const-string v5, "raw"

    iget-object v6, p0, Lcom/google/android/speech/grammar/TextGrammarLoader;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, p2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v4, p0, Lcom/google/android/speech/grammar/TextGrammarLoader;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v2, v3}, Lcom/google/common/io/CharStreams;->copy(Ljava/lang/Readable;Ljava/lang/Appendable;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_0
    return-object v3

    :catchall_0
    move-exception v4

    :goto_1
    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v4

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :catchall_1
    move-exception v4

    move-object v1, v2

    goto :goto_1
.end method
