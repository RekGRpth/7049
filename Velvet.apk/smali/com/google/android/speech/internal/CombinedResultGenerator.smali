.class public Lcom/google/android/speech/internal/CombinedResultGenerator;
.super Ljava/lang/Object;
.source "CombinedResultGenerator.java"


# instance fields
.field private final mRecognitionEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/internal/CombinedResultGenerator;->mRecognitionEvents:Ljava/util/List;

    return-void
.end method

.method private handleMultipleRecognitionEvents()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 13

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f7fffff

    iget-object v11, p0, Lcom/google/android/speech/internal/CombinedResultGenerator;->mRecognitionEvents:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-virtual {v5}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {v5}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisCount()I

    move-result v11

    if-lez v11, :cond_0

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesis(I)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getConfidence()F

    move-result v11

    invoke-static {v2, v11}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v11, p0, Lcom/google/android/speech/internal/CombinedResultGenerator;->mRecognitionEvents:Ljava/util/List;

    iget-object v12, p0, Lcom/google/android/speech/internal/CombinedResultGenerator;->mRecognitionEvents:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    new-instance v4, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-direct {v4}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;-><init>()V

    :try_start_0
    invoke-virtual {v8}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->toByteArray()[B

    move-result-object v11

    invoke-virtual {v4, v11}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    new-instance v0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    invoke-direct {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;-><init>()V

    new-instance v11, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    invoke-direct {v11}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;-><init>()V

    invoke-virtual {v11, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setConfidence(F)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v11

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->setText(Ljava/lang/String;)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->addHypothesis(Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    invoke-virtual {v4, v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setCombinedResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    :cond_2
    :goto_1
    return-object v4

    :catch_0
    move-exception v3

    move-object v4, v8

    goto :goto_1
.end method

.method private handleSingleRecognitionEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 3

    iget-object v1, p0, Lcom/google/android/speech/internal/CombinedResultGenerator;->mRecognitionEvents:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->setCombinedResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public getCombinedResultEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/internal/CombinedResultGenerator;->mRecognitionEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/internal/CombinedResultGenerator;->mRecognitionEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/speech/internal/CombinedResultGenerator;->handleSingleRecognitionEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/speech/internal/CombinedResultGenerator;->handleMultipleRecognitionEvents()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v0

    goto :goto_0
.end method

.method public update(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-object v0, p0, Lcom/google/android/speech/internal/CombinedResultGenerator;->mRecognitionEvents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
