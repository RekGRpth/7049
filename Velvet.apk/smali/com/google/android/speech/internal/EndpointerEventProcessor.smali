.class public Lcom/google/android/speech/internal/EndpointerEventProcessor;
.super Ljava/lang/Object;
.source "EndpointerEventProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/internal/EndpointerEventProcessor$State;
    }
.end annotation


# instance fields
.field private mCurrentProgressMs:J

.field private mEndOfSpeechTriggerMs:J

.field private final mEndpointerListener:Lcom/google/android/speech/audio/EndpointerListener;

.field private mEndpointerParams:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

.field private final mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/StateMachine",
            "<",
            "Lcom/google/android/speech/internal/EndpointerEventProcessor$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/speech/audio/EndpointerListener;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/audio/EndpointerListener;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "VS.EndpointerEventProcessor"

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->NO_SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->newBuilder(Ljava/lang/String;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->NO_SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->NO_SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->DELAY_END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->DELAY_END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->DELAY_END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->addTransition(Ljava/lang/Enum;Ljava/lang/Enum;)Lcom/google/android/searchcommon/util/StateMachine$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/StateMachine$Builder;->build()Lcom/google/android/searchcommon/util/StateMachine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/audio/EndpointerListener;

    iput-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerListener:Lcom/google/android/speech/audio/EndpointerListener;

    iput-object p2, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerParams:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    return-void
.end method

.method private declared-synchronized processEndOfAudioAsEndOfSpeech()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized processEndOfAudioAsNoSpeechDetected()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->NO_SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized processEndOfSpeech(J)Z
    .locals 4
    .param p1    # J

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerParams:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getExtraSilenceAfterEndOfSpeechMsec()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->DELAY_END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V

    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    iget-object v2, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerParams:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getExtraSilenceAfterEndOfSpeechMsec()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->setEndOfSpeechTriggerPoint(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized processStartOfSpeech()Z
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->NO_SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->DELAY_END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setEndOfSpeechTriggerPoint(J)V
    .locals 1
    .param p1    # J

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndOfSpeechTriggerMs:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized shouldTriggerEndOfSpeech()Z
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mCurrentProgressMs:J

    iget-wide v2, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndOfSpeechTriggerMs:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->DELAY_END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized shouldTriggerNoSpeechDetected()Z
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->NO_SPEECH_DETECTED:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mCurrentProgressMs:J

    iget-object v2, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerParams:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->getNoSpeechDetectedTimeoutMsec()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v1, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/StateMachine;->moveTo(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method process(Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;)V
    .locals 4
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;->hasEventType()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const-string v1, "VS.EndpointerEventProcessor"

    const-string v2, "Received EP event without type."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mStateMachine:Lcom/google/android/searchcommon/util/StateMachine;

    sget-object v2, Lcom/google/android/speech/internal/EndpointerEventProcessor$State;->END_OF_SPEECH:Lcom/google/android/speech/internal/EndpointerEventProcessor$State;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/util/StateMachine;->isIn(Ljava/lang/Enum;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;->getEventType()I

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->processStartOfSpeech()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerListener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;->getTimeUsec()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/android/speech/audio/EndpointerListener;->onStartOfSpeech(J)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$EndpointerEvent;->getTimeUsec()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->processEndOfSpeech(J)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerListener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v1}, Lcom/google/android/speech/audio/EndpointerListener;->onEndOfSpeech()V

    goto :goto_0

    :cond_4
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->processEndOfAudioAsEndOfSpeech()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerListener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v1}, Lcom/google/android/speech/audio/EndpointerListener;->onEndOfSpeech()V

    :cond_5
    invoke-direct {p0}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->processEndOfAudioAsNoSpeechDetected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerListener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v1}, Lcom/google/android/speech/audio/EndpointerListener;->onNoSpeechDetected()V

    goto :goto_0
.end method

.method declared-synchronized updateProgress(J)V
    .locals 1
    .param p1    # J

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mCurrentProgressMs:J

    invoke-direct {p0}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->shouldTriggerEndOfSpeech()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerListener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v0}, Lcom/google/android/speech/audio/EndpointerListener;->onEndOfSpeech()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/speech/internal/EndpointerEventProcessor;->shouldTriggerNoSpeechDetected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/internal/EndpointerEventProcessor;->mEndpointerListener:Lcom/google/android/speech/audio/EndpointerListener;

    invoke-interface {v0}, Lcom/google/android/speech/audio/EndpointerListener;->onNoSpeechDetected()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
