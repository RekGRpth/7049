.class public Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;
.super Ljava/lang/Object;
.source "CancellableRecognitionEventListener.java"

# interfaces
.implements Lcom/google/android/speech/listeners/RecognitionEventListener;


# instance fields
.field private mActive:Z

.field private final mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    iput-object p1, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    return-void
.end method

.method public onBeginningOfSpeech()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onBeginningOfSpeech()V

    :cond_0
    return-void
.end method

.method public onDone()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onDone()V

    :cond_0
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onEndOfSpeech()V

    :cond_0
    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    :cond_0
    return-void
.end method

.method public onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V

    :cond_0
    return-void
.end method

.method public onMediaDataResult([B)V
    .locals 1
    .param p1    # [B

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMediaDataResult([B)V

    :cond_0
    return-void
.end method

.method public onMusicDetected()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMusicDetected()V

    :cond_0
    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onNoSpeechDetected()V

    :cond_0
    return-void
.end method

.method public onPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V
    .locals 1
    .param p1    # Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V

    :cond_0
    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onReadyForSpeech(FF)V

    :cond_0
    return-void
.end method

.method public onRecognitionCancelled()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onRecognitionCancelled()V

    :cond_0
    return-void
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 1
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    :cond_0
    return-void
.end method

.method public onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V
    .locals 1
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    iget-boolean v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->mListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V

    :cond_0
    return-void
.end method
