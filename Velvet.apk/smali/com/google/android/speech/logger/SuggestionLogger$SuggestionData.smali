.class public Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;
.super Ljava/lang/Object;
.source "SuggestionLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/logger/SuggestionLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SuggestionData"
.end annotation


# instance fields
.field private final mProto:Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

.field private final mRequestId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;->mProto:Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    iput-object p2, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;->mRequestId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getProto()Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;->mProto:Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    return-object v0
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;->mRequestId:Ljava/lang/String;

    return-object v0
.end method
