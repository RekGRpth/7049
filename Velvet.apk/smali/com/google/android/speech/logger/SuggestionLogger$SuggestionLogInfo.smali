.class public Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;
.super Ljava/lang/Object;
.source "SuggestionLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/logger/SuggestionLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SuggestionLogInfo"
.end annotation


# instance fields
.field private final mLength:I

.field private final mRequestId:Ljava/lang/String;

.field private final mSegmentId:I

.field private final mStart:I

.field final synthetic this$0:Lcom/google/android/speech/logger/SuggestionLogger;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/logger/SuggestionLogger;Ljava/lang/String;III)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->this$0:Lcom/google/android/speech/logger/SuggestionLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->mRequestId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->mSegmentId:I

    iput p4, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->mStart:I

    iput p5, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->mLength:I

    return-void
.end method


# virtual methods
.method public asData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;

    new-instance v1, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    invoke-direct {v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;-><init>()V

    iget v2, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->mSegmentId:I

    invoke-virtual {v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setRecognizerSegmentIndex(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v1

    iget v2, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->mStart:I

    invoke-virtual {v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setStart(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v1

    iget v2, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->mLength:I

    invoke-virtual {v1, v2}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setLength(I)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setOldText(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;->setNewText(Ljava/lang/String;)Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->mRequestId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;-><init>(Lcom/google/speech/logs/VoicesearchClientLogProto$AlternateCorrectionData;Ljava/lang/String;)V

    return-object v0
.end method
