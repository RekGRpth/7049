.class public Lcom/google/android/speech/logger/SuggestionLogger;
.super Ljava/lang/Object;
.source "SuggestionLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;,
        Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;
    }
.end annotation


# instance fields
.field private final mSuggestionInfos:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/speech/logger/SuggestionLogger;->mSuggestionInfos:Ljava/util/LinkedHashMap;

    return-void
.end method

.method private getInfo(I)Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/speech/logger/SuggestionLogger;->mSuggestionInfos:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized addSuggestion(ILjava/lang/String;III)V
    .locals 9
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/speech/logger/SuggestionLogger;->mSuggestionInfos:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/logger/SuggestionLogger;->mSuggestionInfos:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    :cond_0
    iget-object v7, p0, Lcom/google/android/speech/logger/SuggestionLogger;->mSuggestionInfos:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v0, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;-><init>(Lcom/google/android/speech/logger/SuggestionLogger;Ljava/lang/String;III)V

    invoke-virtual {v7, v8, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized log(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/speech/logger/SuggestionLogger;->getInfo(I)Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1, p2, p3}, Lcom/google/android/speech/logger/SuggestionLogger$SuggestionLogInfo;->asData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/speech/logger/SuggestionLogger$SuggestionData;

    move-result-object v0

    :cond_0
    const/16 v2, 0xf

    invoke-static {v2, v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
