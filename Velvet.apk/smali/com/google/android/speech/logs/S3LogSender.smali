.class public Lcom/google/android/speech/logs/S3LogSender;
.super Ljava/lang/Object;
.source "S3LogSender.java"

# interfaces
.implements Lcom/google/android/speech/logger/LogSender;


# instance fields
.field private final mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

.field private final mGogglesHttpServerInfoSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mVoiceHttpServerInfoSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/speech/network/ConnectionFactory;)V
    .locals 0
    .param p3    # Lcom/google/android/speech/network/ConnectionFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;",
            "Lcom/google/android/speech/network/ConnectionFactory;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/logs/S3LogSender;->mVoiceHttpServerInfoSupplier:Lcom/google/common/base/Supplier;

    iput-object p2, p0, Lcom/google/android/speech/logs/S3LogSender;->mGogglesHttpServerInfoSupplier:Lcom/google/common/base/Supplier;

    iput-object p3, p0, Lcom/google/android/speech/logs/S3LogSender;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    return-void
.end method

.method private static createClientLogRequest(Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;)Lcom/google/speech/s3/S3$S3Request;
    .locals 2
    .param p0    # Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createBaseS3Request()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    new-instance v1, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    invoke-direct {v1}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;-><init>()V

    invoke-virtual {v1, p0}, Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;->setVoiceSearch(Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;)Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setClientLogRequestExtension(Lcom/google/speech/s3/ClientLogRequestProto$ClientLogRequest;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    return-object v0
.end method

.method private static createGogglesClientLogRequest(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;)Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .param p0    # Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createBaseS3Request()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/speech/s3/S3$S3Request;->setGogglesClientLogExtension(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    return-object v0
.end method

.method private static createGogglesInitLogRequest()Lcom/google/speech/s3/S3$S3Request;
    .locals 2

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createBaseS3Request()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    const-string v1, "visualsearch-logging"

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setService(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    return-object v0
.end method

.method private static createInitLogRequest()Lcom/google/speech/s3/S3$S3Request;
    .locals 2

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createBaseS3Request()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    const-string v1, "clientlog"

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setService(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    return-object v0
.end method

.method private sendInner(Lcom/google/common/base/Supplier;Lcom/google/speech/s3/S3$S3Request;Ljava/util/ArrayList;)V
    .locals 15
    .param p2    # Lcom/google/speech/s3/S3$S3Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;",
            ">;",
            "Lcom/google/speech/s3/S3$S3Request;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/speech/s3/S3$S3Request;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    new-instance v11, Ljava/net/URL;

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getUrl()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v13, p0, Lcom/google/android/speech/logs/S3LogSender;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    invoke-interface/range {p1 .. p1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;

    invoke-interface {v13, v12}, Lcom/google/android/speech/network/ConnectionFactory;->openHttpConnection(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;)Ljava/net/HttpURLConnection;

    move-result-object v1

    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    new-instance v7, Lcom/google/android/speech/message/S3RequestStream;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v12

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$HttpServerInfo;->getHeader()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-direct {v7, v12, v13, v14}, Lcom/google/android/speech/message/S3RequestStream;-><init>(Ljava/io/OutputStream;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/google/android/speech/message/S3RequestStream;->writeHeader(Lcom/google/speech/s3/S3$S3Request;)V

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/speech/s3/S3$S3Request;

    invoke-virtual {v7, v4}, Lcom/google/android/speech/message/S3RequestStream;->write(Lcom/google/speech/s3/S3$S3Request;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v12

    move-object v6, v7

    :goto_2
    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v9}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1
    throw v12

    :cond_2
    :try_start_2
    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createEndOfData()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v12

    invoke-virtual {v7, v12}, Lcom/google/android/speech/message/S3RequestStream;->write(Lcom/google/speech/s3/S3$S3Request;)V

    invoke-virtual {v7}, Lcom/google/android/speech/message/S3RequestStream;->flush()V

    invoke-virtual {v7}, Lcom/google/android/speech/message/S3RequestStream;->close()V

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    const/16 v12, 0xc8

    if-eq v5, v12, :cond_3

    new-instance v12, Ljava/io/IOException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Http "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12

    :cond_3
    invoke-virtual {v11}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v13

    invoke-virtual {v13}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    new-instance v12, Ljava/io/IOException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Redirect to "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12

    :cond_4
    new-instance v10, Lcom/google/android/speech/message/S3ResponseStream;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    invoke-direct {v10, v12}, Lcom/google/android/speech/message/S3ResponseStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v10}, Lcom/google/android/speech/message/S3ResponseStream;->read()Lcom/google/speech/s3/S3$S3Response;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v12

    const/4 v13, 0x1

    if-eq v12, v13, :cond_5

    new-instance v12, Ljava/io/IOException;

    const-string v13, "Wrong response"

    invoke-direct {v12, v13}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v12

    move-object v9, v10

    move-object v6, v7

    goto/16 :goto_2

    :cond_5
    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v10}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_6
    move-object v9, v10

    move-object v6, v7

    goto/16 :goto_0

    :catchall_2
    move-exception v12

    goto/16 :goto_2
.end method


# virtual methods
.method public send(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;

    invoke-static {v0}, Lcom/google/android/speech/logs/S3LogSender;->createClientLogRequest(Lcom/google/speech/logs/VoicesearchClientLogProto$VoiceSearchClientLog;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/speech/logs/S3LogSender;->mVoiceHttpServerInfoSupplier:Lcom/google/common/base/Supplier;

    invoke-static {}, Lcom/google/android/speech/logs/S3LogSender;->createInitLogRequest()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v4

    invoke-direct {p0, v3, v4, v2}, Lcom/google/android/speech/logs/S3LogSender;->sendInner(Lcom/google/common/base/Supplier;Lcom/google/speech/s3/S3$S3Request;Ljava/util/ArrayList;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;

    invoke-static {v0}, Lcom/google/android/speech/logs/S3LogSender;->createGogglesClientLogRequest(Lcom/google/bionics/goggles/api2/GogglesProtos$GogglesClientLog;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/speech/logs/S3LogSender;->mGogglesHttpServerInfoSupplier:Lcom/google/common/base/Supplier;

    invoke-static {}, Lcom/google/android/speech/logs/S3LogSender;->createGogglesInitLogRequest()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v4

    invoke-direct {p0, v3, v4, v2}, Lcom/google/android/speech/logs/S3LogSender;->sendInner(Lcom/google/common/base/Supplier;Lcom/google/speech/s3/S3$S3Request;Ljava/util/ArrayList;)V

    return-void
.end method
