.class public Lcom/google/android/speech/message/S3ResponseProcessor;
.super Ljava/lang/Object;
.source "S3ResponseProcessor.java"


# instance fields
.field private final mAudioBytes:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/speech/message/S3ResponseProcessor;->mAudioBytes:Ljava/io/ByteArrayOutputStream;

    return-void
.end method

.method private processDone(Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onDone()V

    return-void
.end method

.method public static processMajelServiceEvent(Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;)Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .locals 5
    .param p0    # Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    new-instance v1, Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-direct {v1}, Lcom/google/majel/proto/MajelProtos$MajelResponse;-><init>()V

    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasCompressedMajelResponse()Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->getCompressedMajelResponse()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/speech/network/IoUtils;->uncompress([B)[B

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->clearCompressedMajelResponse()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    invoke-virtual {p0, v1}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->setMajel(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    const-string v3, "S3ResponseProcessor"

    const-string v4, "Could not gunzip response."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->getMajel()Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v1

    goto :goto_1
.end method

.method private processMajelServiceEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    invoke-static {p2}, Lcom/google/android/speech/message/S3ResponseProcessor;->processMajelServiceEvent(Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0xc

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-interface {p1, v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V

    :cond_0
    return-void
.end method

.method private processPinholeOutputEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    invoke-interface {p1, p2}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V

    return-void
.end method

.method private processRecognizerEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    invoke-virtual {p2}, Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;->getRecognitionEvent()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :cond_0
    invoke-interface {p1, v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    return-void
.end method

.method private processSoundSearchEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    invoke-virtual {p2}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;->getResultsResponse()Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0x23

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-virtual {p2}, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;->getResultsResponse()Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V

    :cond_0
    return-void
.end method

.method private processTtsServiceEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    invoke-virtual {p2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;->getEndOfData()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/message/S3ResponseProcessor;->mAudioBytes:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/16 v1, 0x1e

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v1, p0, Lcom/google/android/speech/message/S3ResponseProcessor;->mAudioBytes:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onMediaDataResult([B)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;->getAudio()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/message/S3ResponseProcessor;->mAudioBytes:Ljava/io/ByteArrayOutputStream;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0
.end method


# virtual methods
.method public process(Lcom/google/speech/s3/S3$S3Response;Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 2
    .param p1    # Lcom/google/speech/s3/S3$S3Response;
    .param p2    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->hasTtsServiceEventExtension()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getTtsServiceEventExtension()Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/speech/message/S3ResponseProcessor;->processTtsServiceEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/speech/s3/Synthesis$TtsServiceEvent;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->hasRecognizerEventExtension()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getRecognizerEventExtension()Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/speech/message/S3ResponseProcessor;->processRecognizerEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/speech/s3/Recognizer$RecognizerEvent;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->hasMajelServiceEventExtension()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getMajelServiceEventExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/speech/message/S3ResponseProcessor;->processMajelServiceEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->hasSoundSearchServiceEventExtension()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getSoundSearchServiceEventExtension()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/speech/message/S3ResponseProcessor;->processSoundSearchEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/speech/s3/SoundSearch$SoundSearchServiceEvent;)V

    :cond_4
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->hasPinholeOutputExtension()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getPinholeOutputExtension()Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/speech/message/S3ResponseProcessor;->processPinholeOutputEvent(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/speech/message/S3ResponseProcessor;->processDone(Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/google/android/speech/exception/ServerRecognizeException;

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getErrorCode()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/speech/exception/ServerRecognizeException;-><init>(I)V

    invoke-interface {p2, v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "S3ResponseProcessor"

    const-string v1, "NOT_STARTED received"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/speech/exception/ServerRecognizeException;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/speech/exception/ServerRecognizeException;-><init>(I)V

    invoke-interface {p2, v0}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
