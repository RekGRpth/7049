.class public Lcom/google/android/speech/network/NetworkRecognitionRunner;
.super Ljava/lang/Object;
.source "NetworkRecognitionRunner.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;
    }
.end annotation


# instance fields
.field private final mCallback:Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;

.field private final mCloseConnectionRunnable:Ljava/lang/Runnable;

.field private final mConnectionFactory:Lcom/google/android/speech/network/S3ConnectionFactory;

.field private mCurrentRecognition:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private final mNetworkEventListener:Lcom/google/android/speech/network/NetworkEventListener;

.field private final mNetworkLoopRunnable:Ljava/lang/Runnable;

.field private final mRequestProducer:Lcom/google/android/speech/network/producers/S3RequestProducer;

.field private final mRunnerThread:Ljava/util/concurrent/ExecutorService;

.field private mS3Connection:Lcom/google/android/speech/network/S3Connection;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/network/NetworkEventListener;Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/network/producers/S3RequestProducer;)V
    .locals 6
    .param p2    # Lcom/google/android/speech/network/NetworkEventListener;
    .param p3    # Lcom/google/android/speech/network/S3ConnectionFactory;
    .param p4    # Lcom/google/android/speech/network/producers/S3RequestProducer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/network/NetworkEventListener;",
            "Lcom/google/android/speech/network/S3ConnectionFactory;",
            "Lcom/google/android/speech/network/producers/S3RequestProducer;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    const-string v1, "NetworkRunner"

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->createSafeScheduledExecutorService(ILjava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/speech/network/NetworkRecognitionRunner;-><init>(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/network/NetworkEventListener;Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/network/producers/S3RequestProducer;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/network/NetworkEventListener;Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/network/producers/S3RequestProducer;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p2    # Lcom/google/android/speech/network/NetworkEventListener;
    .param p3    # Lcom/google/android/speech/network/S3ConnectionFactory;
    .param p4    # Lcom/google/android/speech/network/producers/S3RequestProducer;
    .param p5    # Ljava/util/concurrent/ExecutorService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;",
            "Lcom/google/android/speech/network/NetworkEventListener;",
            "Lcom/google/android/speech/network/S3ConnectionFactory;",
            "Lcom/google/android/speech/network/producers/S3RequestProducer;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/speech/network/NetworkRecognitionRunner$1;

    invoke-direct {v0, p0}, Lcom/google/android/speech/network/NetworkRecognitionRunner$1;-><init>(Lcom/google/android/speech/network/NetworkRecognitionRunner;)V

    iput-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mNetworkLoopRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/speech/network/NetworkRecognitionRunner$2;

    invoke-direct {v0, p0}, Lcom/google/android/speech/network/NetworkRecognitionRunner$2;-><init>(Lcom/google/android/speech/network/NetworkRecognitionRunner;)V

    iput-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCloseConnectionRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;

    invoke-direct {v0, p1, p2}, Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;-><init>(Lcom/google/android/speech/callback/Callback;Lcom/google/android/speech/network/NetworkEventListener;)V

    iput-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCallback:Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;

    iput-object p2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mNetworkEventListener:Lcom/google/android/speech/network/NetworkEventListener;

    iput-object p3, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mConnectionFactory:Lcom/google/android/speech/network/S3ConnectionFactory;

    iput-object p4, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRequestProducer:Lcom/google/android/speech/network/producers/S3RequestProducer;

    iput-object p5, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRunnerThread:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/speech/network/NetworkRecognitionRunner;)V
    .locals 0
    .param p0    # Lcom/google/android/speech/network/NetworkRecognitionRunner;

    invoke-direct {p0}, Lcom/google/android/speech/network/NetworkRecognitionRunner;->runNetworkLoop()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/speech/network/NetworkRecognitionRunner;)Lcom/google/android/speech/network/S3Connection;
    .locals 1
    .param p0    # Lcom/google/android/speech/network/NetworkRecognitionRunner;

    iget-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/speech/network/NetworkRecognitionRunner;Lcom/google/android/speech/network/S3Connection;)Lcom/google/android/speech/network/S3Connection;
    .locals 0
    .param p0    # Lcom/google/android/speech/network/NetworkRecognitionRunner;
    .param p1    # Lcom/google/android/speech/network/S3Connection;

    iput-object p1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    return-object p1
.end method

.method private cancel()Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCurrentRecognition:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCurrentRecognition:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    iget-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCurrentRecognition:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCurrentRecognition:Ljava/util/concurrent/Future;

    :cond_0
    return-object v0
.end method

.method private static checkInterrupted()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method private runNetworkLoop()V
    .locals 5

    :try_start_0
    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mNetworkEventListener:Lcom/google/android/speech/network/NetworkEventListener;

    invoke-interface {v2}, Lcom/google/android/speech/network/NetworkEventListener;->onConnectionStarted()V

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mConnectionFactory:Lcom/google/android/speech/network/S3ConnectionFactory;

    invoke-interface {v2}, Lcom/google/android/speech/network/S3ConnectionFactory;->create()Lcom/google/android/speech/network/S3Connection;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    invoke-static {}, Lcom/google/android/speech/network/NetworkRecognitionRunner;->checkInterrupted()V

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRequestProducer:Lcom/google/android/speech/network/producers/S3RequestProducer;

    invoke-interface {v2}, Lcom/google/android/speech/network/producers/S3RequestProducer;->next()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    iget-object v3, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCallback:Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;

    invoke-interface {v2, v3, v1}, Lcom/google/android/speech/network/S3Connection;->connect(Lcom/google/android/speech/callback/Callback;Lcom/google/speech/s3/S3$S3Request;)V

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mNetworkEventListener:Lcom/google/android/speech/network/NetworkEventListener;

    invoke-interface {v2}, Lcom/google/android/speech/network/NetworkEventListener;->onConnectionFinished()V

    invoke-static {}, Lcom/google/android/speech/network/NetworkRecognitionRunner;->checkInterrupted()V

    :goto_0
    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRequestProducer:Lcom/google/android/speech/network/producers/S3RequestProducer;

    invoke-interface {v2}, Lcom/google/android/speech/network/producers/S3RequestProducer;->next()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/speech/network/NetworkRecognitionRunner;->checkInterrupted()V

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    invoke-interface {v2, v1}, Lcom/google/android/speech/network/S3Connection;->send(Lcom/google/speech/s3/S3$S3Request;)V

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mNetworkEventListener:Lcom/google/android/speech/network/NetworkEventListener;

    invoke-interface {v2}, Lcom/google/android/speech/network/NetworkEventListener;->onDataSent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCallback:Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;

    new-instance v3, Lcom/google/android/speech/exception/NetworkRecognizeException;

    const-string v4, "Error in network recognizer: "

    invoke-direct {v3, v4, v0}, Lcom/google/android/speech/exception/NetworkRecognizeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2, v3}, Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mNetworkEventListener:Lcom/google/android/speech/network/NetworkEventListener;

    invoke-interface {v2}, Lcom/google/android/speech/network/NetworkEventListener;->onError()V

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRequestProducer:Lcom/google/android/speech/network/producers/S3RequestProducer;

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_1
    return-void

    :cond_0
    :try_start_2
    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mNetworkEventListener:Lcom/google/android/speech/network/NetworkEventListener;

    invoke-interface {v2}, Lcom/google/android/speech/network/NetworkEventListener;->onDataComplete()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRequestProducer:Lcom/google/android/speech/network/producers/S3RequestProducer;

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :catch_1
    move-exception v2

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRequestProducer:Lcom/google/android/speech/network/producers/S3RequestProducer;

    invoke-static {v2}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRequestProducer:Lcom/google/android/speech/network/producers/S3RequestProducer;

    invoke-static {v3}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v2
.end method


# virtual methods
.method public close()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCallback:Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;

    invoke-virtual {v1}, Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;->invalidate()V

    invoke-direct {p0}, Lcom/google/android/speech/network/NetworkRecognitionRunner;->cancel()Ljava/util/concurrent/Future;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRunnerThread:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRunnerThread:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRunnerThread:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCloseConnectionRunnable:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRunnerThread:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    goto :goto_0
.end method

.method public finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    if-eqz v0, :cond_0

    const-string v0, "VS.NetworkRecognitionRunner"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recognition runner not closed, connection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mS3Connection:Lcom/google/android/speech/network/S3Connection;

    invoke-static {v0}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method getWrappedCallbackForTesting()Lcom/google/android/speech/callback/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/speech/callback/Callback",
            "<",
            "Lcom/google/speech/s3/S3$S3Response;",
            "Lcom/google/android/speech/exception/RecognizeException;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCallback:Lcom/google/android/speech/network/NetworkRecognitionRunner$WrapperCallback;

    return-object v0
.end method

.method public start()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCurrentRecognition:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Duplicate call to start."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mRunnerThread:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mNetworkLoopRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/network/NetworkRecognitionRunner;->mCurrentRecognition:Ljava/util/concurrent/Future;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
