.class public Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;
.super Lcom/google/android/speech/network/producers/Producers$SingleRequestProducer;
.source "SoundSearchHeaderProducer.java"


# instance fields
.field private final mMobileUserInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/MobileUser$MobileUserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestIdSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

.field private final mS3ClientInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3ClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mS3UserInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mService:Ljava/lang/String;

.field private final mSoundSearchInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mTimeoutEnforcer:Lcom/google/android/speech/network/producers/TimeoutEnforcer;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/google/speech/s3/S3$S3AudioInfo;Lcom/google/common/base/Supplier;Ljava/lang/String;)V
    .locals 3
    .param p5    # Lcom/google/speech/s3/S3$S3AudioInfo;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/MobileUser$MobileUserInfo;",
            ">;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3ClientInfo;",
            ">;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3UserInfo;",
            ">;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;",
            ">;",
            "Lcom/google/speech/s3/S3$S3AudioInfo;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/speech/network/producers/Producers$SingleRequestProducer;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mMobileUserInfoFuture:Ljava/util/concurrent/Future;

    iput-object p2, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mS3ClientInfoFuture:Ljava/util/concurrent/Future;

    iput-object p3, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mS3UserInfoFuture:Ljava/util/concurrent/Future;

    iput-object p4, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mSoundSearchInfoFuture:Ljava/util/concurrent/Future;

    iput-object p5, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

    iput-object p6, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mRequestIdSupplier:Lcom/google/common/base/Supplier;

    iput-object p7, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mService:Ljava/lang/String;

    new-instance v0, Lcom/google/android/speech/network/producers/TimeoutEnforcer;

    const-wide/16 v1, 0x3e8

    invoke-direct {v0, v1, v2}, Lcom/google/android/speech/network/producers/TimeoutEnforcer;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mTimeoutEnforcer:Lcom/google/android/speech/network/producers/TimeoutEnforcer;

    return-void
.end method


# virtual methods
.method public bridge synthetic close()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/speech/network/producers/Producers$SingleRequestProducer;->close()V

    return-void
.end method

.method public bridge synthetic next()Lcom/google/speech/s3/S3$S3Request;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/android/speech/network/producers/Producers$SingleRequestProducer;->next()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    return-object v0
.end method

.method produceRequest()Lcom/google/speech/s3/S3$S3Request;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/speech/message/S3RequestUtils;->createBaseS3Request()Lcom/google/speech/s3/S3$S3Request;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mService:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/speech/s3/S3$S3Request;->setService(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3Request;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3AudioInfoExtension(Lcom/google/speech/s3/S3$S3AudioInfo;)Lcom/google/speech/s3/S3$S3Request;

    new-instance v2, Lcom/google/speech/s3/S3$S3SessionInfo;

    invoke-direct {v2}, Lcom/google/speech/s3/S3$S3SessionInfo;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mRequestIdSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/speech/s3/S3$S3SessionInfo;->setSessionId(Ljava/lang/String;)Lcom/google/speech/s3/S3$S3SessionInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3SessionInfoExtension(Lcom/google/speech/s3/S3$S3SessionInfo;)Lcom/google/speech/s3/S3$S3Request;

    iget-object v1, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mTimeoutEnforcer:Lcom/google/android/speech/network/producers/TimeoutEnforcer;

    iget-object v2, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mS3ClientInfoFuture:Ljava/util/concurrent/Future;

    invoke-virtual {v1, v2}, Lcom/google/android/speech/network/producers/TimeoutEnforcer;->waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/s3/S3$S3ClientInfo;

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3ClientInfoExtension(Lcom/google/speech/s3/S3$S3ClientInfo;)Lcom/google/speech/s3/S3$S3Request;

    iget-object v1, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mTimeoutEnforcer:Lcom/google/android/speech/network/producers/TimeoutEnforcer;

    iget-object v2, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mS3UserInfoFuture:Ljava/util/concurrent/Future;

    invoke-virtual {v1, v2}, Lcom/google/android/speech/network/producers/TimeoutEnforcer;->waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/s3/S3$S3UserInfo;

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setS3UserInfoExtension(Lcom/google/speech/s3/S3$S3UserInfo;)Lcom/google/speech/s3/S3$S3Request;

    iget-object v1, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mTimeoutEnforcer:Lcom/google/android/speech/network/producers/TimeoutEnforcer;

    iget-object v2, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mMobileUserInfoFuture:Ljava/util/concurrent/Future;

    invoke-virtual {v1, v2}, Lcom/google/android/speech/network/producers/TimeoutEnforcer;->waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setMobileUserInfoExtension(Lcom/google/speech/s3/MobileUser$MobileUserInfo;)Lcom/google/speech/s3/S3$S3Request;

    iget-object v1, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mTimeoutEnforcer:Lcom/google/android/speech/network/producers/TimeoutEnforcer;

    iget-object v2, p0, Lcom/google/android/speech/network/producers/SoundSearchHeaderProducer;->mSoundSearchInfoFuture:Ljava/util/concurrent/Future;

    invoke-virtual {v1, v2}, Lcom/google/android/speech/network/producers/TimeoutEnforcer;->waitForFuture(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    invoke-virtual {v0, v1}, Lcom/google/speech/s3/S3$S3Request;->setSoundSearchInfoExtension(Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;)Lcom/google/speech/s3/S3$S3Request;

    return-object v0
.end method
