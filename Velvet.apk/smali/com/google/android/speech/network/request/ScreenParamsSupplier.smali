.class public Lcom/google/android/speech/network/request/ScreenParamsSupplier;
.super Ljava/lang/Object;
.source "ScreenParamsSupplier.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;",
        ">;"
    }
.end annotation


# instance fields
.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;)V
    .locals 0
    .param p1    # Landroid/view/WindowManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/network/request/ScreenParamsSupplier;->mWindowManager:Landroid/view/WindowManager;

    return-void
.end method

.method private static getDpiBucket(I)I
    .locals 1
    .param p0    # I

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_3
    const/4 v0, 0x5

    goto :goto_0

    :sswitch_4
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const/4 v0, 0x6

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xa0 -> :sswitch_1
        0xd5 -> :sswitch_3
        0xf0 -> :sswitch_2
        0x140 -> :sswitch_4
        0x1e0 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public get()Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;
    .locals 3

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v1, p0, Lcom/google/android/speech/network/request/ScreenParamsSupplier;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    new-instance v1, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    invoke-direct {v1}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;-><init>()V

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->setWidthPixels(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v1

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->setHeightPixels(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v1

    iget v2, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->setDensityDpi(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v1

    iget v2, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {v2}, Lcom/google/android/speech/network/request/ScreenParamsSupplier;->getDpiBucket(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;->setDpiBucket(I)Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/network/request/ScreenParamsSupplier;->get()Lcom/google/majel/proto/ClientInfoProtos$ScreenParams;

    move-result-object v0

    return-object v0
.end method
