.class public Lcom/google/android/speech/params/AudioInputParams$Builder;
.super Ljava/lang/Object;
.source "AudioInputParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/params/AudioInputParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mEncoding:I

.field private mNoiseSuppressionEnabled:Z

.field private mPlayBeepEnabled:Z

.field private mSamplingRateHz:I

.field private mStoreCompleteAudio:Z

.field private mStreamRewindTimeUsec:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mNoiseSuppressionEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mPlayBeepEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mStoreCompleteAudio:Z

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mEncoding:I

    const/16 v0, 0x1f40

    iput v0, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mSamplingRateHz:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mStreamRewindTimeUsec:J

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/speech/params/AudioInputParams;
    .locals 9

    new-instance v0, Lcom/google/android/speech/params/AudioInputParams;

    iget-boolean v1, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mNoiseSuppressionEnabled:Z

    iget-boolean v2, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mPlayBeepEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mStoreCompleteAudio:Z

    iget v4, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mEncoding:I

    iget v5, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mSamplingRateHz:I

    iget-wide v6, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mStreamRewindTimeUsec:J

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/speech/params/AudioInputParams;-><init>(ZZZIIJLcom/google/android/speech/params/AudioInputParams$1;)V

    return-object v0
.end method

.method public setNoiseSuppressionEnabled(Z)Lcom/google/android/speech/params/AudioInputParams$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mNoiseSuppressionEnabled:Z

    return-object p0
.end method

.method public setPlayBeepEnabled(Z)Lcom/google/android/speech/params/AudioInputParams$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mPlayBeepEnabled:Z

    return-object p0
.end method

.method public setSamplingRate(I)Lcom/google/android/speech/params/AudioInputParams$Builder;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mSamplingRateHz:I

    return-object p0
.end method

.method public setStoreCompleteAudio(Z)Lcom/google/android/speech/params/AudioInputParams$Builder;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mStoreCompleteAudio:Z

    return-object p0
.end method

.method public setStreamRewindTimeUsec(J)Lcom/google/android/speech/params/AudioInputParams$Builder;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/speech/params/AudioInputParams$Builder;->mStreamRewindTimeUsec:J

    return-object p0
.end method
