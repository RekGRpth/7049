.class public Lcom/google/android/speech/params/AudioInputParams;
.super Ljava/lang/Object;
.source "AudioInputParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/params/AudioInputParams$1;,
        Lcom/google/android/speech/params/AudioInputParams$Builder;
    }
.end annotation


# instance fields
.field private final mEncoding:I

.field private final mNoiseSuppressionEnabled:Z

.field private final mPlayBeepEnabled:Z

.field private final mSamplingRateHz:I

.field private final mStoreCompleteAudio:Z

.field private final mStreamRewindTimeUsec:J


# direct methods
.method private constructor <init>(ZZZIIJ)V
    .locals 0
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .param p4    # I
    .param p5    # I
    .param p6    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/speech/params/AudioInputParams;->mNoiseSuppressionEnabled:Z

    iput-boolean p2, p0, Lcom/google/android/speech/params/AudioInputParams;->mPlayBeepEnabled:Z

    iput-boolean p3, p0, Lcom/google/android/speech/params/AudioInputParams;->mStoreCompleteAudio:Z

    iput p4, p0, Lcom/google/android/speech/params/AudioInputParams;->mEncoding:I

    iput p5, p0, Lcom/google/android/speech/params/AudioInputParams;->mSamplingRateHz:I

    iput-wide p6, p0, Lcom/google/android/speech/params/AudioInputParams;->mStreamRewindTimeUsec:J

    return-void
.end method

.method synthetic constructor <init>(ZZZIIJLcom/google/android/speech/params/AudioInputParams$1;)V
    .locals 0
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z
    .param p4    # I
    .param p5    # I
    .param p6    # J
    .param p8    # Lcom/google/android/speech/params/AudioInputParams$1;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/speech/params/AudioInputParams;-><init>(ZZZIIJ)V

    return-void
.end method


# virtual methods
.method public getEncoding()I
    .locals 1

    iget v0, p0, Lcom/google/android/speech/params/AudioInputParams;->mEncoding:I

    return v0
.end method

.method public getSamplingRate()I
    .locals 1

    iget v0, p0, Lcom/google/android/speech/params/AudioInputParams;->mSamplingRateHz:I

    return v0
.end method

.method public getStreamRewindTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/speech/params/AudioInputParams;->mStreamRewindTimeUsec:J

    return-wide v0
.end method

.method public hasStreamRewindTime()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/speech/params/AudioInputParams;->mStreamRewindTimeUsec:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNoiseSuppressionEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/params/AudioInputParams;->mNoiseSuppressionEnabled:Z

    return v0
.end method

.method public isPlayBeepEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/params/AudioInputParams;->mPlayBeepEnabled:Z

    return v0
.end method

.method public shouldStoreCompleteAudio()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/params/AudioInputParams;->mStoreCompleteAudio:Z

    return v0
.end method
