.class public Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;
.super Ljava/lang/Object;
.source "RecognitionEngineParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/params/RecognitionEngineParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EmbeddedParams"
.end annotation


# instance fields
.field private final mBytesPerSample:I

.field private final mCallbackFactory:Lcom/google/android/speech/embedded/Greco3CallbackFactory;

.field private final mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

.field private final mModeSelector:Lcom/google/android/speech/embedded/Greco3ModeSelector;

.field private final mSamplingRate:I

.field private final mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

.field private final mSpeechSettings:Lcom/google/android/speech/SpeechSettings;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/embedded/Greco3CallbackFactory;Lcom/google/android/speech/embedded/Greco3EngineManager;Lcom/google/android/speech/embedded/Greco3ModeSelector;Lcom/google/android/speech/SpeechLevelSource;Lcom/google/android/speech/SpeechSettings;II)V
    .locals 0
    .param p1    # Lcom/google/android/speech/embedded/Greco3CallbackFactory;
    .param p2    # Lcom/google/android/speech/embedded/Greco3EngineManager;
    .param p3    # Lcom/google/android/speech/embedded/Greco3ModeSelector;
    .param p4    # Lcom/google/android/speech/SpeechLevelSource;
    .param p5    # Lcom/google/android/speech/SpeechSettings;
    .param p6    # I
    .param p7    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mCallbackFactory:Lcom/google/android/speech/embedded/Greco3CallbackFactory;

    iput-object p2, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    iput-object p3, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mModeSelector:Lcom/google/android/speech/embedded/Greco3ModeSelector;

    iput-object p4, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    iput-object p5, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    iput p6, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mBytesPerSample:I

    iput p7, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mSamplingRate:I

    return-void
.end method


# virtual methods
.method public getBytesPerSample()I
    .locals 1

    iget v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mBytesPerSample:I

    return v0
.end method

.method public getCallbackFactory()Lcom/google/android/speech/embedded/Greco3CallbackFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mCallbackFactory:Lcom/google/android/speech/embedded/Greco3CallbackFactory;

    return-object v0
.end method

.method public getGreco3EngineManager()Lcom/google/android/speech/embedded/Greco3EngineManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mGreco3EngineManager:Lcom/google/android/speech/embedded/Greco3EngineManager;

    return-object v0
.end method

.method public getModeSelector()Lcom/google/android/speech/embedded/Greco3ModeSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mModeSelector:Lcom/google/android/speech/embedded/Greco3ModeSelector;

    return-object v0
.end method

.method public getSamplingRate()I
    .locals 1

    iget v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mSamplingRate:I

    return v0
.end method

.method public getSpeechLevelSource()Lcom/google/android/speech/SpeechLevelSource;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    return-object v0
.end method
