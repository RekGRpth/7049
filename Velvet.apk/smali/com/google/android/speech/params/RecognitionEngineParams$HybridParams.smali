.class public Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;
.super Ljava/lang/Object;
.source "RecognitionEngineParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/speech/params/RecognitionEngineParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HybridParams"
.end annotation


# instance fields
.field private final mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mMusicExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mNetworkExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private final mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/speech/utils/NetworkInformation;Lcom/google/android/voicesearch/settings/Settings;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/utils/NetworkInformation;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Ljava/util/concurrent/ExecutorService;
    .param p6    # Ljava/util/concurrent/ExecutorService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/utils/NetworkInformation;",
            "Lcom/google/android/voicesearch/settings/Settings;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    iput-object p2, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p3, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p4, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

    iput-object p5, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mNetworkExecutorService:Ljava/util/concurrent/ExecutorService;

    iput-object p6, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mMusicExecutorService:Ljava/util/concurrent/ExecutorService;

    iput-object p7, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method


# virtual methods
.method public getLocalExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public getMusicExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mMusicExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public getNetworkExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mNetworkExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public getNetworkInformation()Lcom/google/android/speech/utils/NetworkInformation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    return-object v0
.end method

.method public getScheduledExecutorService()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public getSettings()Lcom/google/android/voicesearch/settings/Settings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-object v0
.end method

.method public getSoundSearchEnabledSupplier()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;->mSoundSearchEnabledSupplier:Lcom/google/common/base/Supplier;

    return-object v0
.end method
