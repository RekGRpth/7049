.class public Lcom/google/android/speech/params/RecognizerBuilderData;
.super Ljava/lang/Object;
.source "RecognizerBuilderData.java"


# instance fields
.field private final mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

.field private final mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mLocationHelper:Lcom/google/android/speech/helper/SpeechLocationHelper;

.field private final mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

.field private final mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

.field private final mRequestIdGenerator:Lcom/google/android/speech/params/RequestIdGenerator;

.field private final mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/helper/SpeechLocationHelper;Lcom/google/android/speech/helper/AccountHelper;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/SpeechSettings;Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;Lcom/google/android/speech/params/RequestIdGenerator;Lcom/google/android/speech/utils/NetworkInformation;Landroid/view/WindowManager;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/speech/params/DeviceParams;)V
    .locals 0
    .param p1    # Lcom/google/android/speech/helper/SpeechLocationHelper;
    .param p2    # Lcom/google/android/speech/helper/AccountHelper;
    .param p3    # Ljava/util/concurrent/ExecutorService;
    .param p4    # Lcom/google/android/speech/SpeechSettings;
    .param p5    # Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;
    .param p6    # Lcom/google/android/speech/params/RequestIdGenerator;
    .param p7    # Lcom/google/android/speech/utils/NetworkInformation;
    .param p8    # Landroid/view/WindowManager;
    .param p9    # Lcom/google/android/searchcommon/SearchConfig;
    .param p10    # Lcom/google/android/searchcommon/SearchSettings;
    .param p11    # Lcom/google/android/speech/params/DeviceParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mLocationHelper:Lcom/google/android/speech/helper/SpeechLocationHelper;

    iput-object p2, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iput-object p3, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    iput-object p5, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    iput-object p6, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mRequestIdGenerator:Lcom/google/android/speech/params/RequestIdGenerator;

    iput-object p7, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    iput-object p8, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mWindowManager:Landroid/view/WindowManager;

    iput-object p9, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p10, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p11, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    return-void
.end method


# virtual methods
.method public getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    return-object v0
.end method

.method public getDeviceParams()Lcom/google/android/speech/params/DeviceParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mDeviceParams:Lcom/google/android/speech/params/DeviceParams;

    return-object v0
.end method

.method public getExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public getLocationHelper()Lcom/google/android/speech/helper/SpeechLocationHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mLocationHelper:Lcom/google/android/speech/helper/SpeechLocationHelper;

    return-object v0
.end method

.method public getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mNetworkInfo:Lcom/google/android/speech/utils/NetworkInformation;

    return-object v0
.end method

.method public getPinholeParamsBuilder()Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mPinholeParamsBuilder:Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    return-object v0
.end method

.method public getRequestIdGenerator()Lcom/google/android/speech/params/RequestIdGenerator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mRequestIdGenerator:Lcom/google/android/speech/params/RequestIdGenerator;

    return-object v0
.end method

.method public getSearchConfig()Lcom/google/android/searchcommon/SearchConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mSearchConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-object v0
.end method

.method public getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method public getSpeechSettings()Lcom/google/android/speech/SpeechSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mSpeechSettings:Lcom/google/android/speech/SpeechSettings;

    return-object v0
.end method

.method public getWindowManager()Landroid/view/WindowManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerBuilderData;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method
