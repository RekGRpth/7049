.class public Lcom/google/android/speech/params/RecognizerParams;
.super Ljava/lang/Object;
.source "RecognizerParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/speech/params/RecognizerParams$Mode;
    }
.end annotation


# instance fields
.field private final mApplicationId:Ljava/lang/String;

.field private final mAudioInputParams:Lcom/google/android/speech/params/AudioInputParams;

.field private final mBuildParams:Lcom/google/android/speech/params/RecognizerBuilderData;

.field private final mEmbeddedRecognizerFallbackTimeout:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mEmbeddedRecognizerParams:Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

.field private final mEndpointerParamsSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;",
            ">;"
        }
    .end annotation
.end field

.field private final mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

.field private final mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

.field private final mLocationOverride:Landroid/location/Location;

.field private final mMajelClientInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/Majel$MajelClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mMergerStrategy:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

.field private final mMobileUserInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/MobileUser$MobileUserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field private final mPinholeParamsFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/PinholeStream$PinholeParams;",
            ">;"
        }
    .end annotation
.end field

.field private final mRecordedAudio:Z

.field private final mRequestIdSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

.field private final mS3ClientInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3ClientInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mS3RecognizerInfo:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

.field private final mS3UserInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mService:Ljava/lang/String;

.field private final mSoundSearchInfoFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mSoundSearchTtsEnabled:Z

.field private final mSpokenLocaleBcp47:Ljava/lang/String;

.field private final mTimeoutTimestamp:J

.field private final mTriggerApplication:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/speech/params/RecognizerBuilderData;Lcom/google/android/speech/params/RecognizerParams$Mode;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Lcom/google/speech/s3/S3$S3AudioInfo;Lcom/google/common/base/Supplier;Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;JLcom/google/android/speech/embedded/Greco3Grammar;Lcom/google/android/speech/embedded/Greco3Mode;Lcom/google/common/base/Supplier;ZLcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;Lcom/google/common/base/Supplier;Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;Lcom/google/android/speech/params/AudioInputParams;Landroid/location/Location;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Lcom/google/android/speech/params/RecognizerBuilderData;
    .param p2    # Lcom/google/android/speech/params/RecognizerParams$Mode;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p8    # Lcom/google/speech/s3/S3$S3AudioInfo;
    .param p10    # Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .param p14    # J
    .param p16    # Lcom/google/android/speech/embedded/Greco3Grammar;
    .param p17    # Lcom/google/android/speech/embedded/Greco3Mode;
    .param p19    # Z
    .param p20    # Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .param p22    # Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;
    .param p23    # Lcom/google/android/speech/params/AudioInputParams;
    .param p24    # Landroid/location/Location;
    .param p25    # Ljava/lang/String;
    .param p26    # Ljava/lang/String;
    .param p27    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/params/RecognizerBuilderData;",
            "Lcom/google/android/speech/params/RecognizerParams$Mode;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3ClientInfo;",
            ">;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3UserInfo;",
            ">;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/MobileUser$MobileUserInfo;",
            ">;",
            "Lcom/google/speech/s3/S3$S3AudioInfo;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;",
            ">;",
            "Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/Majel$MajelClientInfo;",
            ">;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;",
            ">;",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/PinholeStream$PinholeParams;",
            ">;J",
            "Lcom/google/android/speech/embedded/Greco3Grammar;",
            "Lcom/google/android/speech/embedded/Greco3Mode;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;",
            "Lcom/google/android/speech/params/AudioInputParams;",
            "Landroid/location/Location;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParams;->mBuildParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    iput-object p2, p0, Lcom/google/android/speech/params/RecognizerParams;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    iput-object p3, p0, Lcom/google/android/speech/params/RecognizerParams;->mService:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/speech/params/RecognizerParams;->mSpokenLocaleBcp47:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3ClientInfoFuture:Ljava/util/concurrent/Future;

    iput-object p8, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

    iput-object p6, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3UserInfoFuture:Ljava/util/concurrent/Future;

    iput-object p7, p0, Lcom/google/android/speech/params/RecognizerParams;->mMobileUserInfoFuture:Ljava/util/concurrent/Future;

    iput-object p9, p0, Lcom/google/android/speech/params/RecognizerParams;->mEndpointerParamsSupplier:Lcom/google/common/base/Supplier;

    iput-object p10, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3RecognizerInfo:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    iput-object p11, p0, Lcom/google/android/speech/params/RecognizerParams;->mMajelClientInfoFuture:Ljava/util/concurrent/Future;

    iput-object p12, p0, Lcom/google/android/speech/params/RecognizerParams;->mSoundSearchInfoFuture:Ljava/util/concurrent/Future;

    iput-object p13, p0, Lcom/google/android/speech/params/RecognizerParams;->mPinholeParamsFuture:Ljava/util/concurrent/Future;

    iput-wide p14, p0, Lcom/google/android/speech/params/RecognizerParams;->mTimeoutTimestamp:J

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mRequestIdSupplier:Lcom/google/common/base/Supplier;

    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mRecordedAudio:Z

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mEmbeddedRecognizerParams:Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mEmbeddedRecognizerFallbackTimeout:Lcom/google/common/base/Supplier;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMergerStrategy:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mAudioInputParams:Lcom/google/android/speech/params/AudioInputParams;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mLocationOverride:Landroid/location/Location;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mTriggerApplication:Ljava/lang/String;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mApplicationId:Ljava/lang/String;

    move/from16 v0, p27

    iput-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mSoundSearchTtsEnabled:Z

    return-void
.end method

.method private getFutureValue(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Future",
            "<TV;>;)TV;"
        }
    .end annotation

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    :try_start_0
    iget-wide v4, p0, Lcom/google/android/speech/params/RecognizerParams;->mTimeoutTimestamp:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v2, v3, v4}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v2, "RecognizerParams"

    const-string v3, "#getFutureValue"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "RecognizerParams"

    const-string v2, "#getFutureValue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v0}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->launderCause(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :catch_2
    move-exception v0

    const-string v2, "RecognizerParams"

    const-string v3, "#getFutureValue"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public getApplicationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mApplicationId:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioInputParams()Lcom/google/android/speech/params/AudioInputParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mAudioInputParams:Lcom/google/android/speech/params/AudioInputParams;

    return-object v0
.end method

.method getBuildParams()Lcom/google/android/speech/params/RecognizerBuilderData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mBuildParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    return-object v0
.end method

.method public getEmbeddedRecognizerFallbackTimeout()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mEmbeddedRecognizerFallbackTimeout:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getEmbeddedRecognizerParams()Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mEmbeddedRecognizerParams:Lcom/google/speech/recognizer/api/RecognizerSessionParamsProto$RecognizerSessionParams;

    return-object v0
.end method

.method public getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mEndpointerParamsSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    return-object v0
.end method

.method public getGreco3Grammar()Lcom/google/android/speech/embedded/Greco3Grammar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mGreco3Grammar:Lcom/google/android/speech/embedded/Greco3Grammar;

    return-object v0
.end method

.method public getGreco3Mode()Lcom/google/android/speech/embedded/Greco3Mode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;

    return-object v0
.end method

.method public getLocationOverride()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mLocationOverride:Landroid/location/Location;

    return-object v0
.end method

.method public getMajelClientInfoFuture()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/Majel$MajelClientInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMajelClientInfoFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public getMobileUserInfoFuture()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/MobileUser$MobileUserInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMobileUserInfoFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    return-object v0
.end method

.method public getPinholeParamsFuture()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/PinholeStream$PinholeParams;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mPinholeParamsFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mRequestIdSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRequestIdSupplier()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mRequestIdSupplier:Lcom/google/common/base/Supplier;

    return-object v0
.end method

.method getRequestIdSupplierForRebuild()Lcom/google/common/base/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mRequestIdSupplier:Lcom/google/common/base/Supplier;

    return-object v0
.end method

.method public getResultsMergerStrategy()Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMergerStrategy:Lcom/google/android/voicesearch/greco3/ResultsMergerStrategy;

    return-object v0
.end method

.method public getS3AudioInfo()Lcom/google/speech/s3/S3$S3AudioInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3AudioInfo:Lcom/google/speech/s3/S3$S3AudioInfo;

    return-object v0
.end method

.method public getS3ClientInfoFuture()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3ClientInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3ClientInfoFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public getS3RecognizerInfo()Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3RecognizerInfo:Lcom/google/speech/speech/s3/Recognizer$S3RecognizerInfo;

    return-object v0
.end method

.method public getS3UserInfoFuture()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/s3/S3$S3UserInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3UserInfoFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public getService()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mService:Ljava/lang/String;

    return-object v0
.end method

.method public getSoundSearchInfoFuture()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mSoundSearchInfoFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public getSpokenLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mSpokenLocaleBcp47:Ljava/lang/String;

    return-object v0
.end method

.method public getTriggerApplication()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mTriggerApplication:Ljava/lang/String;

    return-object v0
.end method

.method public isAlternatesEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isBluetoothRecordingEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecordedAudio()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mRecordedAudio:Z

    return v0
.end method

.method public isSingleRequestArchitecture()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mBuildParams:Lcom/google/android/speech/params/RecognizerBuilderData;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSoundSearchTtsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mSoundSearchTtsEnabled:Z

    return v0
.end method

.method public waitForMajelClientInfo()Lcom/google/speech/speech/s3/Majel$MajelClientInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMajelClientInfoFuture:Ljava/util/concurrent/Future;

    invoke-direct {p0, v0}, Lcom/google/android/speech/params/RecognizerParams;->getFutureValue(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/speech/s3/Majel$MajelClientInfo;

    return-object v0
.end method

.method public waitForMobileUserInfo()Lcom/google/speech/s3/MobileUser$MobileUserInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mMobileUserInfoFuture:Ljava/util/concurrent/Future;

    invoke-direct {p0, v0}, Lcom/google/android/speech/params/RecognizerParams;->getFutureValue(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/s3/MobileUser$MobileUserInfo;

    return-object v0
.end method

.method public waitForPinholeParams()Lcom/google/speech/s3/PinholeStream$PinholeParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mPinholeParamsFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mPinholeParamsFuture:Ljava/util/concurrent/Future;

    invoke-direct {p0, v0}, Lcom/google/android/speech/params/RecognizerParams;->getFutureValue(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/s3/PinholeStream$PinholeParams;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public waitForS3ClientInfo()Lcom/google/speech/s3/S3$S3ClientInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3ClientInfoFuture:Ljava/util/concurrent/Future;

    invoke-direct {p0, v0}, Lcom/google/android/speech/params/RecognizerParams;->getFutureValue(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/s3/S3$S3ClientInfo;

    return-object v0
.end method

.method public waitForS3UserInfo()Lcom/google/speech/s3/S3$S3UserInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mS3UserInfoFuture:Ljava/util/concurrent/Future;

    invoke-direct {p0, v0}, Lcom/google/android/speech/params/RecognizerParams;->getFutureValue(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/s3/S3$S3UserInfo;

    return-object v0
.end method

.method public waitForSoundSearchInfo()Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParams;->mSoundSearchInfoFuture:Ljava/util/concurrent/Future;

    invoke-direct {p0, v0}, Lcom/google/android/speech/params/RecognizerParams;->getFutureValue(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/speech/speech/s3/SoundSearch$SoundSearchInfo;

    return-object v0
.end method
