.class Lcom/google/android/speech/params/RecognizerParamsBuilder$1;
.super Ljava/lang/Object;
.source "RecognizerParamsBuilder.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/params/RecognizerParamsBuilder;->getEmbeddedRecognizerFallbackTimeout()Lcom/google/common/base/Supplier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;


# direct methods
.method constructor <init>(Lcom/google/android/speech/params/RecognizerParamsBuilder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$1;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Integer;
    .locals 2

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$1;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;
    invoke-static {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$000(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$1;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;
    invoke-static {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$100(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getEmbeddedRecognizerFallbackTimeout()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$1;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mGreco3Mode:Lcom/google/android/speech/embedded/Greco3Mode;
    invoke-static {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$000(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$1;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;
    invoke-static {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$100(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getVoiceSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;->getEmbeddedRecognizerFallbackTimeout()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder$1;->get()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
