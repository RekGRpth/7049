.class Lcom/google/android/speech/params/RecognizerParamsBuilder$2;
.super Ljava/lang/Object;
.source "RecognizerParamsBuilder.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/params/RecognizerParamsBuilder;->getMajelClientInfoFuture()Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;


# direct methods
.method constructor <init>(Lcom/google/android/speech/params/RecognizerParamsBuilder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$2;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$2;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # invokes: Lcom/google/android/speech/params/RecognizerParamsBuilder;->isSoundSearchEnabled()Z
    invoke-static {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$200(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder$2;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
