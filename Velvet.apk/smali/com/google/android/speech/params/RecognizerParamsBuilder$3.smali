.class Lcom/google/android/speech/params/RecognizerParamsBuilder$3;
.super Ljava/lang/Object;
.source "RecognizerParamsBuilder.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/speech/params/RecognizerParamsBuilder;->getEndpointerParams()Lcom/google/common/base/Supplier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;


# direct methods
.method constructor <init>(Lcom/google/android/speech/params/RecognizerParamsBuilder;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;
    .locals 3

    sget-object v1, Lcom/google/android/speech/params/RecognizerParamsBuilder$5;->$SwitchMap$com$google$android$speech$params$RecognizerParams$Mode:[I

    iget-object v2, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$300(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/speech/params/RecognizerParams$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;
    invoke-static {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$100(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getVoiceSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mNoSpeechDetectedEnabled:Z
    invoke-static {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$400(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x4e20

    invoke-virtual {v0, v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;->setNoSpeechDetectedTimeoutMsec(I)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    :cond_0
    return-object v0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;
    invoke-static {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$100(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;
    invoke-static {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$100(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getIntentApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$IntentApi;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;
    invoke-static {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$100(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getServiceApi()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$ServiceApi;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->this$0:Lcom/google/android/speech/params/RecognizerParamsBuilder;

    # getter for: Lcom/google/android/speech/params/RecognizerParamsBuilder;->mBuilderParams:Lcom/google/android/speech/params/RecognizerBuilderData;
    invoke-static {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->access$100(Lcom/google/android/speech/params/RecognizerParamsBuilder;)Lcom/google/android/speech/params/RecognizerBuilderData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerBuilderData;->getSpeechSettings()Lcom/google/android/speech/SpeechSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/speech/SpeechSettings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getVoiceSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$VoiceSearch;->getEndpointerParams()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/speech/params/RecognizerParamsBuilder$3;->get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$EndpointerParams;

    move-result-object v0

    return-object v0
.end method
