.class public Lcom/google/android/velvet/ActionServerResult;
.super Ljava/lang/Object;
.source "ActionServerResult.java"


# instance fields
.field public final mMetadata:Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

.field public final mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;


# direct methods
.method public constructor <init>(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/PeanutProtos$Peanut;
    .param p2    # Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/PeanutProtos$Peanut;

    iput-object v0, p0, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    iput-object p2, p0, Lcom/google/android/velvet/ActionServerResult;->mMetadata:Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    return-void
.end method

.method public static listFromMajelResponse(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Ljava/util/List;
    .locals 6
    .param p0    # Lcom/google/majel/proto/MajelProtos$MajelResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/majel/proto/MajelProtos$MajelResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ActionServerResult;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->getPeanutCount()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    new-instance v3, Lcom/google/android/velvet/ActionServerResult;

    invoke-virtual {p0, v0}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->getPeanut(I)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/velvet/ActionServerResult;-><init>(Lcom/google/majel/proto/PeanutProtos$Peanut;Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ActionServerResult{ActionV2Count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2Count()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UrlResponseCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ActionServerResult;->mPeanut:Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-virtual {v1}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getUrlResponseCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "CardMetadata "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/velvet/ActionServerResult;->mMetadata:Lcom/google/wireless/voicesearch/proto/CardMetdataProtos$CardMetadata;

    if-nez v0, :cond_0

    const-string v0, "absent"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "present"

    goto :goto_0
.end method
