.class Lcom/google/android/velvet/Corpora$1;
.super Ljava/lang/Object;
.source "Corpora.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/Corpora;->loadWebCorpora()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/Corpora;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/Corpora;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/Corpora$1;->this$0:Lcom/google/android/velvet/Corpora;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v3, p0, Lcom/google/android/velvet/Corpora$1;->this$0:Lcom/google/android/velvet/Corpora;

    # getter for: Lcom/google/android/velvet/Corpora;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v3}, Lcom/google/android/velvet/Corpora;->access$000(Lcom/google/android/velvet/Corpora;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/SearchSettings;->getWebCorpora()[B

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v3, v1

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/velvet/Corpora$1;->this$0:Lcom/google/android/velvet/Corpora;

    # invokes: Lcom/google/android/velvet/Corpora;->loadDefaultCorpora()V
    invoke-static {v3}, Lcom/google/android/velvet/Corpora;->access$100(Lcom/google/android/velvet/Corpora;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {v1}, Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;->parseFrom([B)Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$1;->this$0:Lcom/google/android/velvet/Corpora;

    # invokes: Lcom/google/android/velvet/Corpora;->buildWebCorpora(Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;)V
    invoke-static {v3, v0}, Lcom/google/android/velvet/Corpora;->access$200(Lcom/google/android/velvet/Corpora;Lcom/google/wireless/voicesearch/proto/CorporaConfig$CorporaConfiguration2;)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$1;->this$0:Lcom/google/android/velvet/Corpora;

    # invokes: Lcom/google/android/velvet/Corpora;->loadDefaultCorpora()V
    invoke-static {v3}, Lcom/google/android/velvet/Corpora;->access$100(Lcom/google/android/velvet/Corpora;)V

    goto :goto_0
.end method
