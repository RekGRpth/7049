.class Lcom/google/android/velvet/Corpora$3;
.super Ljava/lang/Object;
.source "Corpora.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/Corpora;->addCorpora(Lcom/google/android/velvet/Corpus;Ljava/util/LinkedHashMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/Corpora;

.field final synthetic val$subCorpora:Ljava/util/LinkedHashMap;

.field final synthetic val$topLevel:Lcom/google/android/velvet/Corpus;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/Corpora;Lcom/google/android/velvet/Corpus;Ljava/util/LinkedHashMap;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/Corpora$3;->this$0:Lcom/google/android/velvet/Corpora;

    iput-object p2, p0, Lcom/google/android/velvet/Corpora$3;->val$topLevel:Lcom/google/android/velvet/Corpus;

    iput-object p3, p0, Lcom/google/android/velvet/Corpora$3;->val$subCorpora:Ljava/util/LinkedHashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/velvet/Corpora$3;->this$0:Lcom/google/android/velvet/Corpora;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/Corpora$3;->this$0:Lcom/google/android/velvet/Corpora;

    # getter for: Lcom/google/android/velvet/Corpora;->mTopLevelCorpora:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/velvet/Corpora;->access$400(Lcom/google/android/velvet/Corpora;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/velvet/Corpora$3;->val$topLevel:Lcom/google/android/velvet/Corpus;

    invoke-virtual {v2}, Lcom/google/android/velvet/Corpus;->getTag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$3;->val$topLevel:Lcom/google/android/velvet/Corpus;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/Corpora$3;->this$0:Lcom/google/android/velvet/Corpora;

    # getter for: Lcom/google/android/velvet/Corpora;->mSubCorpora:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/velvet/Corpora;->access$500(Lcom/google/android/velvet/Corpora;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/velvet/Corpora$3;->val$topLevel:Lcom/google/android/velvet/Corpus;

    iget-object v3, p0, Lcom/google/android/velvet/Corpora$3;->val$subCorpora:Ljava/util/LinkedHashMap;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/Corpora$3;->this$0:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpora;->notifyChanged()V

    iget-object v0, p0, Lcom/google/android/velvet/Corpora$3;->this$0:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
