.class public Lcom/google/android/velvet/Corpus;
.super Ljava/lang/Object;
.source "Corpus.java"


# instance fields
.field private final mCorpora:Lcom/google/android/velvet/Corpora;

.field private final mIconUri:Landroid/net/Uri;

.field private final mId:Ljava/lang/String;

.field private final mNameUri:Landroid/net/Uri;

.field private final mParent:Lcom/google/android/velvet/Corpus;

.field private final mQueryParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectorLayoutId:I

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 9

    const/4 v1, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/velvet/Corpus;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;ILcom/google/android/velvet/Corpus;Lcom/google/android/velvet/Corpora;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;ILcom/google/android/velvet/Corpus;Lcom/google/android/velvet/Corpora;Ljava/util/Map;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/net/Uri;
    .param p4    # Landroid/net/Uri;
    .param p5    # I
    .param p6    # Lcom/google/android/velvet/Corpus;
    .param p7    # Lcom/google/android/velvet/Corpora;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            "I",
            "Lcom/google/android/velvet/Corpus;",
            "Lcom/google/android/velvet/Corpora;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/Corpus;->mTag:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/velvet/Corpus;->mId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/velvet/Corpus;->mIconUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/velvet/Corpus;->mNameUri:Landroid/net/Uri;

    iput p5, p0, Lcom/google/android/velvet/Corpus;->mSelectorLayoutId:I

    iput-object p6, p0, Lcom/google/android/velvet/Corpus;->mParent:Lcom/google/android/velvet/Corpus;

    iput-object p7, p0, Lcom/google/android/velvet/Corpus;->mCorpora:Lcom/google/android/velvet/Corpora;

    iput-object p8, p0, Lcom/google/android/velvet/Corpus;->mQueryParams:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/velvet/Corpus;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/velvet/Corpus;

    if-eqz p0, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/Corpus;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mCorpora:Lcom/google/android/velvet/Corpora;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mCorpora:Lcom/google/android/velvet/Corpora;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/Corpora;->getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;

    move-result-object p0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mTag:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mId:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    new-instance p0, Lcom/google/android/velvet/PendingCorpus;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/PendingCorpus;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final getIconUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mIconUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getIdentifier()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final getNameUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mNameUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getParent()Lcom/google/android/velvet/Corpus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mParent:Lcom/google/android/velvet/Corpus;

    return-object v0
.end method

.method public final getQueryParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mQueryParams:Ljava/util/Map;

    return-object v0
.end method

.method public final getSelectorLayoutId()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/Corpus;->mSelectorLayoutId:I

    return v0
.end method

.method public final getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/Corpus;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/Corpus;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isSummons()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/Corpus;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "summons"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public shouldShowCards()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Corpus["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/Corpus;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
