.class public Lcom/google/android/velvet/Help;
.super Ljava/lang/Object;
.source "Help.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/Help;->mContext:Landroid/content/Context;

    return-void
.end method

.method private addMenuItem(Landroid/view/Menu;IILjava/lang/String;Z)V
    .locals 3
    .param p1    # Landroid/view/Menu;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    invoke-virtual {p0, p4}, Lcom/google/android/velvet/Help;->getHelpIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/view/MenuInflater;

    iget-object v2, p0, Lcom/google/android/velvet/Help;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-direct {p0, p1, p3, v0, p5}, Lcom/google/android/velvet/Help;->applyIntentToMenuItem(Landroid/view/Menu;ILandroid/content/Intent;Z)V

    :cond_0
    return-void
.end method

.method private applyIntentToMenuItem(Landroid/view/Menu;ILandroid/content/Intent;Z)V
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # I
    .param p3    # Landroid/content/Intent;
    .param p4    # Z

    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    if-eqz p4, :cond_0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/view/Menu;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/velvet/Help;->addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;Z)V

    return-void
.end method

.method public addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Landroid/view/Menu;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/high16 v2, 0x7f120000

    const v3, 0x7f1002ae

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/Help;->addMenuItem(Landroid/view/Menu;IILjava/lang/String;Z)V

    return-void
.end method

.method public getHelpIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/Help;->getHelpUrl(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public getHelpUrl(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "gqsb_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/Help;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gsf/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public setHelpMenuItemIntent(Landroid/view/Menu;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/view/Menu;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/google/android/velvet/Help;->getHelpIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f1002ae

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/google/android/velvet/Help;->applyIntentToMenuItem(Landroid/view/Menu;ILandroid/content/Intent;Z)V

    :cond_0
    return-void
.end method
