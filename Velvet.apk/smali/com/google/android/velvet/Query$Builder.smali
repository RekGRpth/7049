.class Lcom/google/android/velvet/Query$Builder;
.super Ljava/lang/Object;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Builder"
.end annotation


# instance fields
.field private mAlternateSpans:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;"
        }
    .end annotation
.end field

.field private mCannedAudio:Ljava/lang/String;

.field private mChanged:Z

.field private mCommitId:J

.field private mCorpus:Lcom/google/android/velvet/Corpus;

.field private mExtras:Landroid/os/Bundle;

.field private mFlags:I

.field private mGogglesDisclosedCapability:I

.field private mGogglesQueryBitmap:Landroid/graphics/Bitmap;

.field private mLocationOverride:Landroid/location/Location;

.field private mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

.field private mPersistCgiParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mQuery:Lcom/google/android/velvet/Query;

.field private mQueryString:Ljava/lang/String;

.field private mRequestId:Ljava/lang/String;

.field private mResultIndex:I

.field private mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

.field private mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/Query$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/Query$1;

    invoke-direct {p0}, Lcom/google/android/velvet/Query$Builder;-><init>()V

    return-void
.end method

.method private clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;
    .locals 3
    .param p1    # I
    .param p2    # I

    iget v1, p0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    or-int v0, v1, p2

    iget-boolean v2, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget v1, p0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    or-int/2addr v1, v2

    iput-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iput v0, p0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    return-object p0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private clearNonRefinementSuggestion()V
    .locals 4

    const/4 v3, 0x0

    const v2, 0xf000

    iget v0, p0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    and-int/2addr v0, v2

    const/16 v1, 0x2000

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v2, v0}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    iput-object v3, p0, Lcom/google/android/velvet/Query$Builder;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    iput-object v3, p0, Lcom/google/android/velvet/Query$Builder;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    :cond_0
    return-void
.end method

.method private clearStateOnQueryChange()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const v0, 0x1810f00

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    iput v1, p0, Lcom/google/android/velvet/Query$Builder;->mResultIndex:I

    iput-object v2, p0, Lcom/google/android/velvet/Query$Builder;->mLocationOverride:Landroid/location/Location;

    iput-object v2, p0, Lcom/google/android/velvet/Query$Builder;->mPersistCgiParameters:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method build()Lcom/google/android/velvet/Query;
    .locals 20

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    if-eqz v2, :cond_0

    new-instance v1, Lcom/google/android/velvet/Query;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/Query$Builder;->mQueryString:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/velvet/Query$Builder;->mRequestId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/velvet/Query$Builder;->mResultIndex:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/velvet/Query$Builder;->mPersistCgiParameters:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/velvet/Query$Builder;->mCommitId:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/velvet/Query$Builder;->mAlternateSpans:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/velvet/Query$Builder;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/velvet/Query$Builder;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/Query$Builder;->mLocationOverride:Landroid/location/Location;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/Query$Builder;->mCannedAudio:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/Query$Builder;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/Query$Builder;->mGogglesDisclosedCapability:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/Query$Builder;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/Query$Builder;->mExtras:Landroid/os/Bundle;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-direct/range {v1 .. v19}, Lcom/google/android/velvet/Query;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/Corpus;ILjava/util/Map;JLjava/util/List;Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;Landroid/location/Location;Ljava/lang/String;Landroid/graphics/Bitmap;ILcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;Lcom/google/android/velvet/Query$1;)V

    :goto_0
    return-object v1

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/velvet/Query$Builder;->mQuery:Lcom/google/android/velvet/Query;

    goto :goto_0
.end method

.method buildUpon(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query$Builder;
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mQuery:Lcom/google/android/velvet/Query;

    # getter for: Lcom/google/android/velvet/Query;->mFlags:I
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$200(Lcom/google/android/velvet/Query;)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    iget-object v0, p1, Lcom/google/android/velvet/Query;->mRequestId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mRequestId:Ljava/lang/String;

    # getter for: Lcom/google/android/velvet/Query;->mQuery:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$300(Lcom/google/android/velvet/Query;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mQueryString:Ljava/lang/String;

    # getter for: Lcom/google/android/velvet/Query;->mCorpus:Lcom/google/android/velvet/Corpus;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$400(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Corpus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    # getter for: Lcom/google/android/velvet/Query;->mCommitId:J
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$500(Lcom/google/android/velvet/Query;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/velvet/Query$Builder;->mCommitId:J

    # getter for: Lcom/google/android/velvet/Query;->mResultIndex:I
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$600(Lcom/google/android/velvet/Query;)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/Query$Builder;->mResultIndex:I

    # getter for: Lcom/google/android/velvet/Query;->mAlternateSpans:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$700(Lcom/google/android/velvet/Query;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mAlternateSpans:Ljava/util/List;

    # getter for: Lcom/google/android/velvet/Query;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$800(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    # getter for: Lcom/google/android/velvet/Query;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$900(Lcom/google/android/velvet/Query;)Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    # getter for: Lcom/google/android/velvet/Query;->mLocationOverride:Landroid/location/Location;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$1000(Lcom/google/android/velvet/Query;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mLocationOverride:Landroid/location/Location;

    # getter for: Lcom/google/android/velvet/Query;->mCannedAudio:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$1100(Lcom/google/android/velvet/Query;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mCannedAudio:Ljava/lang/String;

    # getter for: Lcom/google/android/velvet/Query;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$1200(Lcom/google/android/velvet/Query;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    # getter for: Lcom/google/android/velvet/Query;->mGogglesDisclosedCapability:I
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$1300(Lcom/google/android/velvet/Query;)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/Query$Builder;->mGogglesDisclosedCapability:I

    # getter for: Lcom/google/android/velvet/Query;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$1400(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/UiMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    # getter for: Lcom/google/android/velvet/Query;->mExtras:Landroid/os/Bundle;
    invoke-static {p1}, Lcom/google/android/velvet/Query;->access$1500(Lcom/google/android/velvet/Query;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mExtras:Landroid/os/Bundle;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    return-object p0
.end method

.method clearFlag(I)Lcom/google/android/velvet/Query$Builder;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    return-object v0
.end method

.method committed()Lcom/google/android/velvet/Query$Builder;
    .locals 3

    # getter for: Lcom/google/android/velvet/Query;->sLatestCommitId:Ljava/util/concurrent/atomic/AtomicLong;
    invoke-static {}, Lcom/google/android/velvet/Query;->access$1600()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/velvet/Query$Builder;->mCommitId:J

    iget v1, p0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    and-int/lit8 v0, v1, 0xf

    if-eqz v0, :cond_0

    const-string v1, ""

    iput-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mQueryString:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mAlternateSpans:Ljava/util/List;

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    return-object p0
.end method

.method setCannedAudio(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mCannedAudio:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mCannedAudio:Ljava/lang/String;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setCommitId(J)Lcom/google/android/velvet/Query$Builder;
    .locals 2
    .param p1    # J

    iget-wide v0, p0, Lcom/google/android/velvet/Query$Builder;->mCommitId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/velvet/Query$Builder;->mCommitId:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    :cond_0
    return-object p0
.end method

.method setCorpus(Lcom/google/android/velvet/Corpus;Z)Lcom/google/android/velvet/Query$Builder;
    .locals 3
    .param p1    # Lcom/google/android/velvet/Corpus;
    .param p2    # Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    if-eq v0, p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v2

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/Query$Builder;->clearStateOnQueryChange()V

    if-eqz p2, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/Query$Builder;->clearNonRefinementSuggestion()V

    :cond_1
    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    return-object p0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method setFlag(I)Lcom/google/android/velvet/Query$Builder;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    return-object v0
.end method

.method setGogglesDisclosedCapability(I)Lcom/google/android/velvet/Query$Builder;
    .locals 2
    .param p1    # I

    iget-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget v0, p0, Lcom/google/android/velvet/Query$Builder;->mGogglesDisclosedCapability:I

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iput p1, p0, Lcom/google/android/velvet/Query$Builder;->mGogglesDisclosedCapability:I

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setGogglesQueryBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/velvet/Query$Builder;
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    iget-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setLocationOverride(Landroid/location/Location;)Lcom/google/android/velvet/Query$Builder;
    .locals 2
    .param p1    # Landroid/location/Location;

    iget-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mLocationOverride:Landroid/location/Location;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mLocationOverride:Landroid/location/Location;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setPersistCgiParameters(Ljava/util/Map;)Lcom/google/android/velvet/Query$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/velvet/Query$Builder;"
        }
    .end annotation

    iget-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mPersistCgiParameters:Ljava/util/Map;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mPersistCgiParameters:Ljava/util/Map;

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setQueryString(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/velvet/Query$Builder;->setQueryString(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    return-object v0
.end method

.method setQueryString(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/velvet/Query$Builder;
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;)",
            "Lcom/google/android/velvet/Query$Builder;"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/Query$Builder;->mQueryString:Ljava/lang/String;

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    move v0, v2

    :goto_0
    iget-boolean v5, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v4, p0, Lcom/google/android/velvet/Query$Builder;->mAlternateSpans:Ljava/util/List;

    if-ne v4, p2, :cond_0

    if-eqz v0, :cond_5

    :cond_0
    move v4, v2

    :goto_1
    or-int/2addr v4, v5

    iput-boolean v4, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    if-eqz v0, :cond_3

    iget v4, p0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    and-int/lit8 v1, v4, 0xf

    const/4 v4, 0x2

    if-eq v1, v4, :cond_1

    const/high16 v4, 0x200000

    invoke-direct {p0, v4, v3}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/velvet/Query$Builder;->mGogglesQueryBitmap:Landroid/graphics/Bitmap;

    :cond_1
    if-eq v1, v2, :cond_2

    const/high16 v2, 0x100000

    invoke-direct {p0, v2, v3}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    :cond_2
    invoke-direct {p0}, Lcom/google/android/velvet/Query$Builder;->clearStateOnQueryChange()V

    invoke-direct {p0}, Lcom/google/android/velvet/Query$Builder;->clearNonRefinementSuggestion()V

    :cond_3
    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mQueryString:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/velvet/Query$Builder;->mAlternateSpans:Ljava/util/List;

    return-object p0

    :cond_4
    move v0, v3

    goto :goto_0

    :cond_5
    move v4, v3

    goto :goto_1
.end method

.method public setRequestId(Ljava/lang/String;)Lcom/google/android/velvet/Query$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mRequestId:Ljava/lang/String;

    return-object p0
.end method

.method setResultIndex(I)Lcom/google/android/velvet/Query$Builder;
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/velvet/Query$Builder;->mResultIndex:I

    if-eq p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/Query$Builder;->clearStateOnQueryChange()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    iput p1, p0, Lcom/google/android/velvet/Query$Builder;->mResultIndex:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    :cond_0
    return-object p0
.end method

.method setSelection(I)Lcom/google/android/velvet/Query$Builder;
    .locals 1
    .param p1    # I

    const/16 v0, 0xf00

    invoke-direct {p0, v0, p1}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    return-object v0
.end method

.method setSentinel(Lcom/google/android/velvet/presenter/UiMode;Landroid/os/Bundle;)Lcom/google/android/velvet/Query$Builder;
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Landroid/os/Bundle;

    const/16 v0, 0xf

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    iget-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mExtras:Landroid/os/Bundle;

    if-eq v0, p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iput-object p1, p0, Lcom/google/android/velvet/Query$Builder;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    iput-object p2, p0, Lcom/google/android/velvet/Query$Builder;->mExtras:Landroid/os/Bundle;

    return-object p0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setSuggestion(ILcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query$Builder;
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p3    # Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    const/4 v0, 0x0

    const v1, 0xf000

    invoke-direct {p0, v1, p1}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    iput-object p2, p0, Lcom/google/android/velvet/Query$Builder;->mSuggestion:Lcom/google/android/searchcommon/suggest/Suggestion;

    iput-object p3, p0, Lcom/google/android/velvet/Query$Builder;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/velvet/Query$Builder;->clearStateOnQueryChange()V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/Query$Builder;->setType(I)Lcom/google/android/velvet/Query$Builder;

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mQueryString:Ljava/lang/String;

    const/16 v1, 0x2000

    if-ne p1, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/velvet/Query$Builder;->mQueryString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mQueryString:Ljava/lang/String;

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mAlternateSpans:Ljava/util/List;

    const/16 v1, 0x3000

    if-ne p1, v1, :cond_1

    const/16 v0, 0x20

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/Query$Builder;->setTrigger(I)Lcom/google/android/velvet/Query$Builder;

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    return-object p0
.end method

.method setTrigger(I)Lcom/google/android/velvet/Query$Builder;
    .locals 1
    .param p1    # I

    const/16 v0, 0xf0

    invoke-direct {p0, v0, p1}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    move-result-object v0

    return-object v0
.end method

.method setType(I)Lcom/google/android/velvet/Query$Builder;
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    const/high16 v4, 0x80000

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v1, p0, Lcom/google/android/velvet/Query$Builder;->mFlags:I

    and-int/lit8 v0, v1, 0xf

    if-eq p1, v0, :cond_3

    const v1, 0x104000f

    invoke-direct {p0, v1, p1}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    if-ne p1, v3, :cond_4

    invoke-direct {p0, v2, v4}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    :cond_0
    :goto_0
    if-eq p1, v3, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    const-string v2, "web"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/Corpus;->getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    :cond_2
    iput-object v5, p0, Lcom/google/android/velvet/Query$Builder;->mCannedAudio:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/velvet/Query$Builder;->mSentinelMode:Lcom/google/android/velvet/presenter/UiMode;

    :cond_3
    return-object p0

    :cond_4
    if-nez p1, :cond_0

    invoke-direct {p0, v4, v2}, Lcom/google/android/velvet/Query$Builder;->clearAndSetFlags(II)Lcom/google/android/velvet/Query$Builder;

    goto :goto_0
.end method

.method updateCorpora(Lcom/google/android/velvet/Corpora;)Lcom/google/android/velvet/Query$Builder;
    .locals 3
    .param p1    # Lcom/google/android/velvet/Corpora;

    invoke-virtual {p1}, Lcom/google/android/velvet/Corpora;->areWebCorporaLoaded()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    invoke-virtual {v1}, Lcom/google/android/velvet/Corpus;->getTag()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    invoke-virtual {v2}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/velvet/Corpora;->getCorpus(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/Corpus;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iget-object v1, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    if-eq v1, v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    or-int/2addr v1, v2

    iput-boolean v1, p0, Lcom/google/android/velvet/Query$Builder;->mChanged:Z

    iput-object v0, p0, Lcom/google/android/velvet/Query$Builder;->mCorpus:Lcom/google/android/velvet/Corpus;

    :cond_0
    return-object p0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
