.class Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;
.super Ljava/util/concurrent/FutureTask;
.source "VelvetBackgroundTasksImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/VelvetBackgroundTasksImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotifyOnDoneFutureTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mTaskName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;Ljava/util/concurrent/Callable;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    invoke-direct {p0, p3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iput-object p2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;->mTaskName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;Ljava/util/concurrent/Callable;Lcom/google/android/velvet/VelvetBackgroundTasksImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/util/concurrent/Callable;
    .param p4    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;-><init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    return-void
.end method


# virtual methods
.method public done()V
    .locals 2

    invoke-super {p0}, Ljava/util/concurrent/FutureTask;->done()V

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$1100(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$1100(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NotifyOnDoneFutureTask["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;->mTaskName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
