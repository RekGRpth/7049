.class Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;
.super Ljava/lang/Object;
.source "VelvetBackgroundTasksImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/VelvetBackgroundTasksImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaskParams"
.end annotation


# instance fields
.field private mEarliestNextRunTimeMs:J

.field private mForcedRunTimeMs:J

.field private final mMinPeriodMs:J

.field private final mTaskName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;J)V
    .locals 2
    .param p2    # Ljava/lang/String;
    .param p3    # J

    iput-object p1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mTaskName:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mMinPeriodMs:J

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {p1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$900(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/searchcommon/SearchSettings;->getBackgroundTaskEarliestNextRun(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {p1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$900(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/searchcommon/SearchSettings;->getBackgroundTaskForcedRun(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->maybeFixupInvalidTimes()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;JLcom/google/android/velvet/VelvetBackgroundTasksImpl$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$1;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;-><init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;J)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->setForcedRun(J)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;J)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->isReadyToRun(J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;)J
    .locals 2
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;J)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->isForcedRun(J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;J)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->updateParamsOnTaskStart(J)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;)J
    .locals 2
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->commitParamsOnTaskCompletion()V

    return-void
.end method

.method private commitParamsOnTaskCompletion()V
    .locals 6

    const-wide/16 v4, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$900(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mTaskName:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/SearchSettings;->setBackgroundTaskEarliestNextRun(Ljava/lang/String;J)V

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$900(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mTaskName:Ljava/lang/String;

    invoke-interface {v0, v1, v4, v5}, Lcom/google/android/searchcommon/SearchSettings;->setBackgroundTaskForcedRun(Ljava/lang/String;J)V

    :cond_0
    return-void
.end method

.method private isForcedRun(J)Z
    .locals 4
    .param p1    # J

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isReadyToRun(J)Z
    .locals 4
    .param p1    # J

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    cmp-long v0, v0, p1

    if-lez v0, :cond_1

    :cond_0
    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeFixupInvalidTimes()V
    .locals 6

    const-wide/16 v4, 0x0

    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v2}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$1000(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mMinPeriodMs:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    iget-wide v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mMinPeriodMs:J

    add-long/2addr v4, v0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    :cond_0
    iget-wide v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mMinPeriodMs:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v2}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$900(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mTaskName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/searchcommon/SearchSettings;->setBackgroundTaskEarliestNextRun(Ljava/lang/String;J)V

    :cond_1
    iget-wide v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    iget-wide v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mMinPeriodMs:J

    add-long/2addr v4, v0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    const-string v2, "Velvet.VelvetBackgroundTasksImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid forced run for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " set it to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v2}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$900(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mTaskName:Ljava/lang/String;

    invoke-interface {v2, v3, v0, v1}, Lcom/google/android/searchcommon/SearchSettings;->setBackgroundTaskForcedRun(Ljava/lang/String;J)V

    :cond_2
    return-void
.end method

.method private setForcedRun(J)V
    .locals 2
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->this$0:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->access$900(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mTaskName:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/searchcommon/SearchSettings;->setBackgroundTaskForcedRun(Ljava/lang/String;J)V

    return-void
.end method

.method private updateParamsOnTaskStart(J)V
    .locals 4
    .param p1    # J

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mMinPeriodMs:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mMinPeriodMs:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J

    :cond_0
    iput-wide v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
