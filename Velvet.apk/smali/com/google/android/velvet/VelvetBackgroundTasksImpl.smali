.class public Lcom/google/android/velvet/VelvetBackgroundTasksImpl;
.super Ljava/lang/Object;
.source "VelvetBackgroundTasksImpl.java"

# interfaces
.implements Lcom/google/android/velvet/VelvetBackgroundTasks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/VelvetBackgroundTasksImpl$Service;,
        Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;,
        Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;
    }
.end annotation


# static fields
.field static final FORCE_RUN_NOW_WITH_INTERRUPT:J = -0x1L

.field static final UNSET:J


# instance fields
.field private final mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

.field private final mApp:Lcom/google/android/velvet/VelvetApplication;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mFactory:Lcom/google/android/velvet/VelvetFactory;

.field private final mLock:Ljava/lang/Object;

.field private final mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

.field private final mReadyTasks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mScheduleTasksRunnable:Ljava/lang/Runnable;

.field private mServiceRunning:Z

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mTasks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/VelvetApplication;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Ljava/util/concurrent/Executor;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/VelvetApplication;
    .param p2    # Lcom/google/android/searchcommon/util/Clock;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Lcom/google/android/searchcommon/SearchSettings;
    .param p5    # Ljava/util/concurrent/Executor;
    .param p6    # Lcom/google/android/velvet/VelvetFactory;
    .param p7    # Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;
    .param p8    # Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$1;-><init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)V

    iput-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mScheduleTasksRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mApp:Lcom/google/android/velvet/VelvetApplication;

    iput-object p2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p3, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p5, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iput-object p7, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    iput-object p8, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mTasks:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->loadTasks()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->serviceTasks()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;)Lcom/google/android/searchcommon/SearchSettings;
    .locals 1
    .param p0    # Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method private forceRunInternal(Ljava/lang/String;J)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mTasks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;

    # invokes: Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->setForcedRun(J)V
    invoke-static {v0, p2, p3}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->access$000(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;J)V

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mScheduleTasksRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static getEarliest(JJ)J
    .locals 3
    .param p0    # J
    .param p2    # J

    const-wide/16 v1, 0x0

    cmp-long v0, p0, v1

    if-nez v0, :cond_0

    :goto_0
    return-wide p2

    :cond_0
    cmp-long v0, p2, v1

    if-nez v0, :cond_1

    move-wide p2, p0

    goto :goto_0

    :cond_1
    invoke-static {p0, p1, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    goto :goto_0
.end method

.method private loadTasks()V
    .locals 12

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getBackgroundTasks()[Ljava/lang/String;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getBackgroundTaskMinPeriods()[Ljava/lang/String;

    move-result-object v8

    const/4 v6, 0x0

    :goto_0
    array-length v1, v10

    if-ge v6, v1, :cond_2

    aget-object v2, v10, v6

    const-wide/16 v3, 0x0

    const/4 v7, 0x0

    :goto_1
    :try_start_0
    array-length v1, v8

    add-int/lit8 v1, v1, -0x1

    if-ge v7, v1, :cond_1

    aget-object v1, v8, v7

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v1, v7, 0x1

    aget-object v1, v8, v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->parseMinPeriod(Ljava/lang/String;)J

    move-result-wide v3

    :cond_0
    add-int/lit8 v7, v7, 0x2

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;-><init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;JLcom/google/android/velvet/VelvetBackgroundTasksImpl$1;)V

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mTasks:Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :catch_0
    move-exception v9

    const-string v1, "Velvet.VelvetBackgroundTasksImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception parsing min period of "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, ": "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_2
    return-void
.end method

.method private parseMinPeriod(Ljava/lang/String;)J
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/NumberFormatException;

    invoke-direct {v3}, Ljava/lang/NumberFormatException;-><init>()V

    throw v3

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v3, Ljava/lang/NumberFormatException;

    invoke-direct {v3}, Ljava/lang/NumberFormatException;-><init>()V

    throw v3

    :sswitch_0
    const-wide/32 v3, 0xea60

    mul-long/2addr v3, v1

    :goto_0
    return-wide v3

    :sswitch_1
    const-wide/32 v3, 0x36ee80

    mul-long/2addr v3, v1

    goto :goto_0

    :sswitch_2
    const-wide/32 v3, 0x5265c00

    mul-long/2addr v3, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_2
        0x68 -> :sswitch_1
        0x6d -> :sswitch_0
    .end sparse-switch
.end method

.method private scheduleAlarm(JLandroid/content/Intent;)V
    .locals 3
    .param p1    # J
    .param p3    # Landroid/content/Intent;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mPendingIntentFactory:Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;

    invoke-interface {v1, v2, p3, v2}, Lcom/google/android/apps/sidekick/inject/PendingIntentFactory;->getService(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v1, v0}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->cancel(Landroid/app/PendingIntent;)V

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAlarmManager:Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;

    invoke-interface {v1, v2, p1, p2, v0}, Lcom/google/android/apps/sidekick/inject/AlarmManagerInjectable;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private serviceTasks()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mServiceRunning:Z

    if-nez v4, :cond_1

    move v4, v2

    :goto_0
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mServiceRunning:Z

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->startReadyTasks(Ljava/util/Map;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cancelPreemptedTasks(Ljava/util/Map;Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->waitForTasks()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->retireCompletedTasks(Ljava/util/Map;Ljava/util/List;)V

    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->scheduleTasks()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v5

    if-nez v5, :cond_2

    :goto_2
    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mServiceRunning:Z

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :cond_1
    move v4, v3

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    :try_start_3
    monitor-exit v4

    goto :goto_1

    :catchall_1
    move-exception v2

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method private startService(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/VelvetApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private waitForTasks()V
    .locals 4

    iget-object v3, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/FutureTask;

    invoke-virtual {v2}, Ljava/util/concurrent/FutureTask;->isDone()Z

    move-result v2

    if-eqz v2, :cond_0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    :try_start_2
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :catch_0
    move-exception v2

    goto :goto_1
.end method


# virtual methods
.method cancelPreemptedTasks(Ljava/util/Map;Ljava/util/List;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const/4 v6, 0x0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/FutureTask;

    const/4 v3, 0x0

    iget-object v8, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    iget-object v7, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Ljava/util/concurrent/FutureTask;

    move-object v3, v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    if-eq v3, v4, :cond_0

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    invoke-interface {p2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v6, 0x1

    goto :goto_0

    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    :cond_1
    if-eqz v6, :cond_3

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {p1, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-interface {p2}, Ljava/util/List;->clear()V

    :cond_3
    return v6
.end method

.method public forceRun(Ljava/lang/String;J)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->forceRunInternal(Ljava/lang/String;J)V

    return-void
.end method

.method public forceRunInterruptingOngoing(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->forceRunInternal(Ljava/lang/String;J)V

    return-void
.end method

.method getReadyTasksForTesting()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    return-object v0
.end method

.method public maybeStartTasks()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mScheduleTasksRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyUiLaunched()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/google/android/searchcommon/SearchSettings;->setLastApplicationLaunch(J)V

    return-void
.end method

.method retireCompletedTasks(Ljava/util/Map;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/FutureTask;

    invoke-virtual {v2}, Ljava/util/concurrent/FutureTask;->isDone()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-ne v4, v6, :cond_2

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/FutureTask;

    invoke-virtual {v4}, Ljava/util/concurrent/FutureTask;->isDone()Z

    move-result v4

    invoke-static {v4}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mTasks:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;

    # invokes: Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->commitParamsOnTaskCompletion()V
    invoke-static {v4}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->access$700(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;)V

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    :cond_3
    invoke-interface {p2}, Ljava/util/List;->clear()V

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method scheduleTasks()Z
    .locals 14

    iget-object v6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    monitor-enter v6

    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v7

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mTasks:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;

    # invokes: Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->isReadyToRun(J)Z
    invoke-static {v0, v7, v8}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->access$100(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;J)Z

    move-result v10

    if-eqz v10, :cond_1

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J
    invoke-static {v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->access$200(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;)J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v10, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    :cond_0
    iget-object v10, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    new-instance v11, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;

    iget-object v12, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    # invokes: Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->isForcedRun(J)Z
    invoke-static {v0, v7, v8}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->access$300(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;J)Z

    move-result v13

    invoke-virtual {v12, v1, v13}, Lcom/google/android/velvet/VelvetFactory;->createBackgroundTask(Ljava/lang/String;Z)Ljava/util/concurrent/Callable;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {v11, p0, v1, v12, v13}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$NotifyOnDoneFutureTask;-><init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;Ljava/util/concurrent/Callable;Lcom/google/android/velvet/VelvetBackgroundTasksImpl$1;)V

    invoke-interface {v10, v1, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    # invokes: Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->updateParamsOnTaskStart(J)V
    invoke-static {v0, v7, v8}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->access$500(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;J)V

    move-wide v0, v2

    move-wide v2, v4

    :goto_1
    move-wide v4, v2

    move-wide v2, v0

    goto :goto_0

    :cond_1
    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mEarliestNextRunTimeMs:J
    invoke-static {v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->access$600(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;)J

    move-result-wide v10

    invoke-static {v4, v5, v10, v11}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->getEarliest(JJ)J

    move-result-wide v4

    # getter for: Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->mForcedRunTimeMs:J
    invoke-static {v0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;->access$200(Lcom/google/android/velvet/VelvetBackgroundTasksImpl$TaskParams;)J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->getEarliest(JJ)J

    move-result-wide v0

    move-wide v2, v4

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_6

    iget-boolean v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mServiceRunning:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    :cond_3
    :goto_3
    monitor-exit v6

    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mApp:Lcom/google/android/velvet/VelvetApplication;

    const-class v3, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$Service;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->startService(Landroid/content/Intent;)V

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_6
    :try_start_1
    invoke-virtual {p0, v7, v8, v4, v5}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->throttleAlarm(JJ)J

    move-result-wide v4

    invoke-static {v4, v5, v2, v3}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->getEarliest(JJ)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_3

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mApp:Lcom/google/android/velvet/VelvetApplication;

    const-class v5, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$Service;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->scheduleAlarm(JLandroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :cond_7
    move-wide v0, v2

    move-wide v2, v4

    goto :goto_1
.end method

.method startReadyTasks(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;>;)V"
        }
    .end annotation

    iget-object v7, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mLock:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    iget-object v6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mReadyTasks:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/FutureTask;

    invoke-interface {p1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-interface {p1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v6, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    :cond_1
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method throttleAlarm(JJ)J
    .locals 14
    .param p1    # J
    .param p3    # J

    const-wide/16 v10, 0x0

    cmp-long v10, p3, v10

    if-nez v10, :cond_0

    :goto_0
    return-wide p3

    :cond_0
    iget-object v10, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v10}, Lcom/google/android/searchcommon/SearchConfig;->getBackgroundTasksPeriodMs()J

    move-result-wide v8

    iget-object v10, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v10}, Lcom/google/android/searchcommon/SearchSettings;->getLastApplicationLaunch()J

    move-result-wide v10

    sub-long v10, p1, v10

    const-wide/32 v12, 0x5265c00

    div-long v2, v10, v12

    const-wide/16 v10, 0x0

    cmp-long v10, v2, v10

    if-lez v10, :cond_1

    iget-object v10, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v10}, Lcom/google/android/searchcommon/SearchConfig;->getMaxBackgroundTasksPeriodMs()J

    move-result-wide v6

    iget-object v10, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v10}, Lcom/google/android/searchcommon/SearchConfig;->getBackgroundTasksPeriodDaysOfDisuseSquaredMultipleMs()J

    move-result-wide v4

    mul-long v10, v2, v2

    mul-long/2addr v10, v4

    add-long/2addr v10, v8

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    :cond_1
    add-long v10, p1, v8

    move-wide/from16 v0, p3

    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p3

    goto :goto_0
.end method
