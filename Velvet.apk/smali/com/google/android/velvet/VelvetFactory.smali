.class public Lcom/google/android/velvet/VelvetFactory;
.super Ljava/lang/Object;
.source "VelvetFactory.java"


# instance fields
.field private final mApp:Lcom/google/android/velvet/VelvetApplication;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/VelvetApplication;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/VelvetApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/velvet/VelvetApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/VelvetFactory;)Lcom/google/android/velvet/VelvetApplication;
    .locals 1
    .param p0    # Lcom/google/android/velvet/VelvetFactory;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    return-object v0
.end method

.method private createSuggestionListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;I)Lcom/google/android/velvet/ui/widget/SuggestionListView;
    .locals 4
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # I

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, p3, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/widget/SuggestionListView;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSuggestionFormatter()Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionViewRecycler()Lcom/google/android/velvet/ui/util/ViewRecycler;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->init(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/searchcommon/suggest/SuggestionFormatter;Lcom/google/android/velvet/ui/util/ViewRecycler;)V

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionClickListener()Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setSuggestionClickListener(Lcom/google/android/searchcommon/ui/SuggestionClickListener;)V

    return-object v1
.end method

.method private getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method private getRetainedLayoutInflater()Landroid/view/LayoutInflater;
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method public createAdClickHandler(Lcom/google/android/velvet/presenter/AdClickHandler$Client;)Lcom/google/android/velvet/presenter/AdClickHandler;
    .locals 4
    .param p1    # Lcom/google/android/velvet/presenter/AdClickHandler$Client;

    new-instance v0, Lcom/google/android/velvet/presenter/AdClickHandler;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v2

    const-string v3, "ad-click"

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/android/velvet/presenter/AdClickHandler;-><init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/velvet/presenter/AdClickHandler$Client;)V

    return-object v0
.end method

.method public createBackgroundTask(Ljava/lang/String;Z)Ljava/util/concurrent/Callable;
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "refresh_search_history"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/velvet/VelvetFactory$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/VelvetFactory$1;-><init>(Lcom/google/android/velvet/VelvetFactory;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "flush_analytics"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/velvet/VelvetFactory$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/VelvetFactory$2;-><init>(Lcom/google/android/velvet/VelvetFactory;)V

    goto :goto_0

    :cond_1
    const-string v0, "refresh_search_domain_and_cookies"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getCookies()Lcom/google/android/velvet/Cookies;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v8}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getWebViewGoogleCookiesLock()Lcom/google/android/searchcommon/util/ForceableLock;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v9}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v9

    move-object v10, p0

    move v11, p2

    invoke-direct/range {v0 .. v11}, Lcom/google/android/searchcommon/google/RefreshSearchDomainAndCookiesTask;-><init>(Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/velvet/Cookies;Lcom/google/android/searchcommon/util/ForceableLock;Ljava/util/concurrent/Executor;Lcom/google/android/velvet/VelvetFactory;Z)V

    goto :goto_0

    :cond_2
    const-string v0, "update_gservices_config"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/searchcommon/GservicesUpdateTask;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getVersionCode()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/GservicesUpdateTask;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/voicesearch/settings/Settings;I)V

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t create task for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public createCardListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/CardListView;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/CardListView;

    return-object v1
.end method

.method public createCardSettingsForAdapter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;
    .locals 7
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p3    # Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f040013

    const/4 v5, 0x0

    invoke-virtual {v1, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v5

    move-object v4, v3

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {p3}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getCardView()Landroid/view/View;

    move-result-object v6

    invoke-interface {v0, v5, v4, v1, v6}, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;->populateBackOfCard(Landroid/app/Activity;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Landroid/view/View;)V

    const v4, 0x7f100012

    invoke-virtual {v3, v4, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-object v3
.end method

.method public createConnectionErrorPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)",
            "Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    return-object v0
.end method

.method public createContextHeaderPresenter(Lcom/google/android/velvet/presenter/ContextHeaderUi;)Lcom/google/android/velvet/presenter/ContextHeaderPresenter;
    .locals 7
    .param p1    # Lcom/google/android/velvet/presenter/ContextHeaderUi;

    new-instance v0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getPooledBackgroundExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getImageLoader()Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v6

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;-><init>(Lcom/google/android/velvet/presenter/ContextHeaderUi;Landroid/content/res/Resources;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V

    return-object v0
.end method

.method public createControllerFactory(Lcom/google/android/voicesearch/CardController;)Lcom/google/android/voicesearch/fragments/ControllerFactory;
    .locals 13
    .param p1    # Lcom/google/android/voicesearch/CardController;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v12

    new-instance v0, Lcom/google/android/voicesearch/fragments/ControllerFactory;

    invoke-virtual {v12}, Lcom/google/android/voicesearch/VoiceSearchServices;->getTtsAudioPlayer()Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    move-result-object v3

    invoke-virtual {v12}, Lcom/google/android/voicesearch/VoiceSearchServices;->getContactLookup()Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Lcom/google/android/voicesearch/VoiceSearchServices;->getLocalTtsManager()Lcom/google/android/voicesearch/util/LocalTtsManager;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v7

    invoke-virtual {v12}, Lcom/google/android/voicesearch/VoiceSearchServices;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v8

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getDeviceCapabilityManager()Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-result-object v9

    invoke-virtual {v12}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v10

    invoke-virtual {v12}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;

    move-result-object v11

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/voicesearch/fragments/ControllerFactory;-><init>(Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/speech/contacts/ContactLookup;Landroid/content/Context;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/DeviceCapabilityManager;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/helper/AccountHelper;)V

    return-object v0
.end method

.method public createCorpusSelector(Lcom/google/android/velvet/presenter/FooterPresenter;Landroid/view/ViewGroup;Lcom/google/android/velvet/Corpus;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/FooterPresenter;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Lcom/google/android/velvet/Corpus;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/FooterPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/velvet/Corpus;->getSelectorLayoutId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createCorrectionPromoter()Lcom/google/android/searchcommon/suggest/Promoter;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/searchcommon/suggest/CorrectionPromoter;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/searchcommon/suggest/CorrectionPromoter;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V

    return-object v0
.end method

.method public createDismissTrail(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;)Lcom/google/android/apps/sidekick/DismissTrail;
    .locals 5
    .param p1    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    new-instance v2, Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-direct {v2, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f040015

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/DismissTrail;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/sidekick/DismissTrail;->setEntryAdapter(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    return-object v0
.end method

.method public createErrorCard(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040032

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createFooterPresenter(Lcom/google/android/velvet/presenter/FooterUi;)Lcom/google/android/velvet/presenter/FooterPresenter;
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/FooterUi;

    new-instance v0, Lcom/google/android/velvet/presenter/FooterPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getCorpora()Lcom/google/android/velvet/Corpora;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/velvet/presenter/FooterPresenter;-><init>(Lcom/google/android/velvet/Corpora;Lcom/google/android/velvet/presenter/FooterUi;)V

    return-object v0
.end method

.method public createFragment(Ljava/lang/String;)Lcom/google/android/velvet/ui/VelvetFragment;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public createFragmentPresenter(Lcom/google/android/velvet/ui/VelvetFragment;)Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/VelvetFragment",
            "<*>;)",
            "Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "results"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createResultsPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/ResultsPresenter;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "searchplate"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast p1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createSearchPlatePresenter(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v1, "contextheader"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p1, Lcom/google/android/velvet/presenter/ContextHeaderUi;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createContextHeaderPresenter(Lcom/google/android/velvet/presenter/ContextHeaderUi;)Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v1, "suggest"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    check-cast p1, Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createSuggestPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    move-result-object v1

    goto :goto_0

    :cond_3
    const-string v1, "footer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    check-cast p1, Lcom/google/android/velvet/presenter/FooterUi;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createFooterPresenter(Lcom/google/android/velvet/presenter/FooterUi;)Lcom/google/android/velvet/presenter/FooterPresenter;

    move-result-object v1

    goto :goto_0

    :cond_4
    const-string v1, "summons"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    check-cast p1, Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createSummonsPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/SummonsPresenter;

    move-result-object v1

    goto :goto_0

    :cond_5
    const-string v1, "error"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    check-cast p1, Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createConnectionErrorPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;

    move-result-object v1

    goto :goto_0

    :cond_6
    const-string v1, "gogglesdisambig"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    check-cast p1, Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createGogglesDisambiguationPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;

    move-result-object v1

    goto :goto_0

    :cond_7
    const-string v1, "goggleshistorymsg"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    check-cast p1, Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createGogglesHistoryMessagePresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;

    move-result-object v1

    goto :goto_0

    :cond_8
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Don\'t know how to create presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public createGogglesConnectionFactory()Lcom/google/android/speech/network/ConnectionFactory;
    .locals 6

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;

    new-instance v2, Lcom/google/android/speech/network/ConnectionFactoryImpl;

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/google/android/speech/network/ConnectionFactoryImpl;-><init>(Landroid/content/Context;Lcom/google/common/base/Supplier;)V

    new-instance v3, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;

    iget-object v4, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;-><init>(Landroid/content/Context;Lcom/google/common/base/Supplier;)V

    iget-object v4, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;-><init>(Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/searchcommon/SearchConfig;Z)V

    return-object v1
.end method

.method public createGogglesController(Lcom/google/android/goggles/GogglesController$Callback;Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/goggles/GogglesController;
    .locals 13
    .param p1    # Lcom/google/android/goggles/GogglesController$Callback;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v12

    new-instance v0, Lcom/google/android/goggles/GogglesController;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    const-string v2, "goggles networking executor"

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/AsyncServices;->getExclusiveNamedExecutor(Ljava/lang/String;)Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getGogglesSettings()Lcom/google/android/goggles/GogglesSettings;

    move-result-object v5

    invoke-virtual {v12}, Lcom/google/android/searchcommon/SearchConfig;->isGogglesBarcodingEnabled()Z

    move-result v6

    invoke-virtual {v12}, Lcom/google/android/searchcommon/SearchConfig;->getGogglesMaxSessionMs()I

    move-result v7

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v8

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v9

    invoke-virtual {v12}, Lcom/google/android/searchcommon/SearchConfig;->getGogglesSceneChangeDelayMs()I

    move-result v10

    move-object v1, p1

    move-object v2, p0

    move-object v11, p2

    invoke-direct/range {v0 .. v11}, Lcom/google/android/goggles/GogglesController;-><init>(Lcom/google/android/goggles/GogglesController$Callback;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/voicesearch/VoiceSearchServices;Ljava/util/concurrent/Executor;Lcom/google/android/goggles/GogglesSettings;ZILcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/google/LocationSettings;ILcom/google/android/velvet/presenter/QueryState;)V

    return-object v0
.end method

.method public createGogglesDisambiguation(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040045

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createGogglesDisambiguationItem(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040046

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createGogglesDisambiguationPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)",
            "Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    return-object v0
.end method

.method public createGogglesHistoryMessagePresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)",
            "Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    return-object v0
.end method

.method public createGogglesHistoryMessageView(Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040049

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createInAppWebPagePresenter(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    .locals 7
    .param p1    # Lcom/google/android/velvet/ui/InAppWebPageActivity;

    new-instance v0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v5

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;-><init>(Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/ui/InAppWebPageActivity;)V

    return-object v0
.end method

.method public createJavascriptExtensions(Lcom/google/android/searchcommon/util/IntentStarter;Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;)Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/util/IntentStarter;
    .param p2    # Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;
    .param p3    # Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

    new-instance v0, Lcom/google/android/velvet/presenter/JavascriptExtensions;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v3

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/presenter/JavascriptExtensions;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/IntentStarter;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;)V

    return-object v0
.end method

.method public createJavascriptExtensionsForSearchResults(Lcom/google/android/searchcommon/util/IntentStarter;Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;)Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/util/IntentStarter;
    .param p2    # Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/velvet/presenter/JavascriptExtensions;->searchResultsTrustPolicy(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;

    move-result-object v4

    new-instance v0, Lcom/google/android/velvet/presenter/JavascriptExtensions;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/presenter/JavascriptExtensions;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/IntentStarter;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;)V

    return-object v0
.end method

.method public createLoadingCard(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040068

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createMoreCorporaSelector(Lcom/google/android/velvet/presenter/FooterPresenter;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/FooterPresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/FooterPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040024

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createNoSummonsMessageView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040083

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createOffscreenWebView()Landroid/webkit/WebView;
    .locals 2

    new-instance v0, Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public createPredictiveCardForAdapter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-interface {p2, v0, v1, p4}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-interface {p2, v0, v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->registerBackOfCardMenuListener(Landroid/app/Activity;Landroid/view/View;)V

    invoke-interface {p2, v0, v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->registerActions(Landroid/app/Activity;Landroid/view/View;)V

    invoke-interface {p2, v0, v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->registerDetailsClickListener(Landroid/app/Activity;Landroid/view/View;)V

    move-object v5, v2

    check-cast v5, Landroid/view/ViewGroup;

    invoke-interface {p2, v5, v1}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->maybeShowFeedbackPrompt(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    new-instance v4, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v2}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->setCardView(Landroid/view/View;)V

    const v5, 0x7f100011

    invoke-virtual {v4, v5, p2}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->setTag(ILjava/lang/Object;)V

    const v5, 0x7f100013

    invoke-virtual {v4, v5, p3}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->setTag(ILjava/lang/Object;)V

    return-object v4
.end method

.method public createPresenter(Lcom/google/android/velvet/presenter/VelvetUi;)Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 10
    .param p1    # Lcom/google/android/velvet/presenter/VelvetUi;

    new-instance v0, Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v8

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v9

    move-object v2, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/velvet/presenter/VelvetPresenter;-><init>(Landroid/content/Context;Lcom/google/android/velvet/presenter/VelvetUi;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/searchcommon/AsyncServices;Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/velvet/ActivityLifecycleObserver;Lcom/google/android/apps/sidekick/inject/LocationOracle;Lcom/google/android/searchcommon/debug/DebugFeatures;)V

    return-object v0
.end method

.method public createQueryState()Lcom/google/android/velvet/presenter/QueryState;
    .locals 8

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v7

    new-instance v0, Lcom/google/android/velvet/presenter/QueryState;

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getCorpora()Lcom/google/android/velvet/Corpora;

    move-result-object v2

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v4

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/presenter/QueryState;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/velvet/Corpora;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Ljava/util/Random;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    return-object v0
.end method

.method public createRecognizerController(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/voicesearch/VoiceSearchServices;->createRecognizerController(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v1

    return-object v1
.end method

.method public createReminderSaver()Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;
    .locals 7

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v6

    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v2

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v3

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getCookies()Lcom/google/android/velvet/Cookies;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/voicesearch/VoiceSearchServices;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/Cookies;Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method

.method public createResultsPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/ResultsPresenter;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)",
            "Lcom/google/android/velvet/presenter/ResultsPresenter;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/velvet/presenter/ResultsPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getBeamHelper()Lcom/google/android/velvet/util/BeamHelper;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getTtsAudioPlayer()Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    move-result-object v4

    new-instance v5, Lcom/google/android/voicesearch/CardFactory;

    invoke-virtual {p1}, Lcom/google/android/velvet/ui/MainContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/google/android/voicesearch/CardFactory;-><init>(Landroid/content/Context;)V

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/presenter/ResultsPresenter;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/velvet/util/BeamHelper;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/CardFactory;)V

    return-object v0
.end method

.method public createResultsWebView()Landroid/webkit/WebView;
    .locals 6

    new-instance v0, Lcom/google/android/velvet/util/AttachedActivityContext;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-direct {v0, v2}, Lcom/google/android/velvet/util/AttachedActivityContext;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/velvet/VelvetFactory;->getRetainedLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0400ab

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/util/AttachedActivityContext;->setView(Landroid/view/View;)V

    return-object v1
.end method

.method public createSearchController()Lcom/google/android/velvet/presenter/SearchController;
    .locals 8

    new-instance v0, Lcom/google/android/velvet/presenter/SearchController;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iget-object v4, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSuggestionsPresenter()Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    move-result-object v7

    move-object v4, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/SearchController;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/CoreSearchServices;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/lang/String;Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;)V

    return-object v0
.end method

.method public createSearchPlatePresenter(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/velvet/presenter/SearchPlatePresenter;
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    new-instance v0, Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;-><init>(Lcom/google/android/velvet/presenter/SearchPlateUi;)V

    return-object v0
.end method

.method public createSearchResultPageFetcher(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/webview/GsaWebViewController;Lcom/google/android/velvet/prefetch/SearchResultPageCache;)Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;
    .locals 13
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;
    .param p2    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p3    # Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v12

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v0

    const-string v1, "prefetch"

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isAlwaysPrefetchEnabled()Z

    move-result v11

    new-instance v0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v1

    invoke-interface {v12}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v3

    invoke-interface {v12}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v4

    invoke-interface {v12}, Lcom/google/android/searchcommon/CoreSearchServices;->getHttpHelper()Lcom/google/android/searchcommon/util/HttpHelper;

    move-result-object v5

    sget-object v10, Lcom/google/android/speech/params/RequestIdGenerator;->INSTANCE:Lcom/google/android/speech/params/RequestIdGenerator;

    move-object v2, p2

    move-object v6, p1

    move-object/from16 v7, p3

    invoke-direct/range {v0 .. v11}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;-><init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/velvet/webview/GsaWebViewController;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/prefetch/SearchResultPageCache;Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;Ljava/util/concurrent/Executor;Lcom/google/android/speech/params/RequestIdGenerator;Z)V

    return-object v0
.end method

.method public createSingleSourcePromoter(Lcom/google/android/searchcommon/summons/Source;)Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/summons/Source;

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v1

    new-instance v2, Lcom/google/android/searchcommon/summons/SingleSourcePromoter;

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    invoke-direct {v2, p1, v4, v3, v4}, Lcom/google/android/searchcommon/summons/SingleSourcePromoter;-><init>(Lcom/google/android/searchcommon/summons/Source;Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/suggest/Promoter;)V

    new-instance v0, Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/searchcommon/SearchConfig;->getMaxResultsPerSource()I

    move-result v3

    invoke-direct {v0, v3}, Lcom/google/android/searchcommon/suggest/CachingPromoter;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->setPromoter(Lcom/google/android/searchcommon/suggest/Promoter;)V

    return-object v0
.end method

.method public createSuggestPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)",
            "Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v12

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v11

    new-instance v7, Lcom/google/android/velvet/presenter/ViewActionRecorder;

    invoke-interface {v11}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    invoke-interface {v12}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getExecutedUserActionStore()Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;

    move-result-object v3

    invoke-direct {v7, v2, v3}, Lcom/google/android/velvet/presenter/ViewActionRecorder;-><init>(Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/apps/sidekick/inject/ExecutedUserActionStore;)V

    new-instance v0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-interface {v11}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    invoke-interface {v12}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v3

    invoke-interface {v12}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v4

    invoke-interface {v12}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryInvalidator()Lcom/google/android/apps/sidekick/inject/EntryInvalidator;

    move-result-object v5

    invoke-interface {v12}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getLocalBroadcastManager()Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;-><init>(Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryInvalidator;Landroid/support/v4/content/LocalBroadcastManager;)V

    new-instance v1, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    invoke-interface {v11}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v3

    invoke-interface {v11}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v4

    invoke-interface {v11}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v6

    invoke-interface {v12}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v9

    invoke-interface {v12}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v10

    move-object v5, p1

    move-object v8, v0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;-><init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/google/SearchBoxLogging;Lcom/google/android/searchcommon/google/UserInteractionLogger;Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/velvet/presenter/ViewActionRecorder;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {v7, v1}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->setPresenter(Lcom/google/android/velvet/presenter/MainContentPresenter;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->setPresenter(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;)V

    return-object v1
.end method

.method public createSuggestionView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/searchcommon/suggest/Suggestion;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p3    # Z
    .param p4    # Landroid/view/ViewGroup;

    const v0, 0x7f0400c2

    invoke-virtual {p0, p2, p3}, Lcom/google/android/velvet/VelvetFactory;->getSuggestionViewType(Lcom/google/android/searchcommon/suggest/Suggestion;Z)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1

    :pswitch_0
    const v0, 0x7f040067

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0400d9

    goto :goto_0

    :pswitch_2
    const v0, 0x7f04007b

    goto :goto_0

    :pswitch_3
    const v0, 0x7f040020

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public createSuggestionViewRecycler()Lcom/google/android/velvet/ui/util/ViewRecycler;
    .locals 3

    new-instance v0, Lcom/google/android/velvet/ui/util/ViewRecycler;

    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetFactory;->getNumSuggestionViewTypes()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getSuggestionViewRecycleBinSize()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/ui/util/ViewRecycler;-><init>(II)V

    return-object v0
.end method

.method public createSuggestionsCachingPromoter()Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getMaxPromotedSuggestions()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/suggest/CachingPromoter;-><init>(I)V

    return-object v0
.end method

.method public createSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;
    .locals 3

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/suggest/SuggestionsControllerImpl;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    return-object v0
.end method

.method public createSuggestionsLauncher(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;)Lcom/google/android/searchcommon/suggest/SuggestionLauncher;
    .locals 6
    .param p1    # Lcom/google/android/velvet/presenter/VelvetPresenter;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;
    .param p3    # Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;

    new-instance v0, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v3

    move-object v2, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/suggest/SuggestionLauncher;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/suggest/presenter/SuggestionsPresenter;Lcom/google/android/searchcommon/GlobalSearchServices;Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/QueryState;)V

    return-object v0
.end method

.method public createSummonsCachingPromoter()Lcom/google/android/searchcommon/suggest/CachingPromoter;
    .locals 2

    new-instance v0, Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getMaxPromotedSummons()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/searchcommon/suggest/CachingPromoter;-><init>(I)V

    return-object v0
.end method

.method public createSummonsListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/widget/SuggestionListView;
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    const v0, 0x7f0400c4

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;I)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v0

    return-object v0
.end method

.method public createSummonsListViewForSuggest(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/widget/SuggestionListView;
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    const v0, 0x7f0400c5

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;I)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v0

    return-object v0
.end method

.method public createSummonsPresenter(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/velvet/presenter/SummonsPresenter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)",
            "Lcom/google/android/velvet/presenter/SummonsPresenter;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v6

    new-instance v0, Lcom/google/android/velvet/presenter/SummonsPresenter;

    invoke-interface {v6}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSourceRanker()Lcom/google/android/searchcommon/summons/SourceRanker;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getMaxPromotedSummonsPerSourceInitial()I

    move-result v4

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getMaxPromotedSummonsPerSourceIncrease()I

    move-result v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/presenter/SummonsPresenter;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/searchcommon/summons/SourceRanker;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;II)V

    return-object v0
.end method

.method public createSummonsPromoter()Lcom/google/android/searchcommon/suggest/Promoter;
    .locals 8

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    new-instance v2, Lcom/google/android/searchcommon/summons/SummonsFilter;

    invoke-direct {v2}, Lcom/google/android/searchcommon/summons/SummonsFilter;-><init>()V

    new-instance v0, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;

    invoke-interface {v7}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSources()Lcom/google/android/searchcommon/summons/Sources;

    move-result-object v1

    invoke-interface {v7}, Lcom/google/android/searchcommon/GlobalSearchServices;->getClickLog()Lcom/google/android/searchcommon/clicklog/ClickLog;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchHistoryChangedObservable()Landroid/database/DataSetObservable;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/searchcommon/summons/TopSourceSummonsPromoter;-><init>(Lcom/google/android/searchcommon/summons/Sources;Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/clicklog/ClickLog;Landroid/database/DataSetObservable;Lcom/google/android/searchcommon/suggest/Promoter;)V

    return-object v0
.end method

.method public createVelvetCardController(Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/voicesearch/VelvetCardController;
    .locals 6
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;

    new-instance v0, Lcom/google/android/voicesearch/VelvetCardController;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getDeviceCapabilityManager()Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v3

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/VelvetCardController;-><init>(Lcom/google/android/searchcommon/DeviceCapabilityManager;Landroid/content/Context;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/velvet/presenter/QueryState;)V

    return-object v0
.end method

.method public createWebPromoter()Lcom/google/android/searchcommon/suggest/Promoter;
    .locals 4

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->shouldDedupeUserQuery()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Lcom/google/android/searchcommon/suggest/OriginalQueryFilter;

    invoke-direct {v1}, Lcom/google/android/searchcommon/suggest/OriginalQueryFilter;-><init>()V

    :cond_0
    new-instance v2, Lcom/google/android/searchcommon/suggest/WebPromoter;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3, v0}, Lcom/google/android/searchcommon/suggest/WebPromoter;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionFilter;Lcom/google/android/searchcommon/suggest/Promoter;Lcom/google/android/searchcommon/SearchConfig;)V

    return-object v2
.end method

.method public createWebResultsText(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/VelvetFactory;->getActivityLayoutInflater(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400d8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public createWebSuggestionListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/widget/SuggestionListView;
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Landroid/view/ViewGroup;

    const v0, 0x7f0400da

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;I)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v0

    return-object v0
.end method

.method public createWebViewController(Lcom/google/android/velvet/webview/WebViewControllerClient;Lcom/google/android/velvet/presenter/QueryState;)Lcom/google/android/velvet/webview/GsaWebViewController;
    .locals 13
    .param p1    # Lcom/google/android/velvet/webview/WebViewControllerClient;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;

    new-instance v0, Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v5}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v8

    iget-object v6, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v6

    const-string v7, "webview-controller"

    invoke-interface {v6, v7}, Lcom/google/android/searchcommon/AsyncServices;->getPooledNamedExecutor(Ljava/lang/String;)Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    move-result-object v9

    iget-object v6, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v10

    iget-object v6, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getCookies()Lcom/google/android/velvet/Cookies;

    move-result-object v11

    iget-object v6, p0, Lcom/google/android/velvet/VelvetFactory;->mApp:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v12}, Lcom/google/android/velvet/webview/GsaWebViewController;-><init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/velvet/webview/WebViewControllerClient;Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/searchcommon/google/LocationSettings;Ljava/util/concurrent/Executor;Lcom/google/common/base/Supplier;Lcom/google/android/velvet/Cookies;Landroid/content/Context;)V

    return-object v0
.end method

.method public getNumSuggestionViewTypes()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public getSuggestionViewType(Lcom/google/android/searchcommon/suggest/Suggestion;Z)I
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->canBeLargeImageSuggestion()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isWebSearchSuggestion()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isNavSuggestion()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isContactSuggestion()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
