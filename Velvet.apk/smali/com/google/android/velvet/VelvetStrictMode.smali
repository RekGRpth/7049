.class public Lcom/google/android/velvet/VelvetStrictMode;
.super Ljava/lang/Object;
.source "VelvetStrictMode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkStartupAtLeast(I)V
    .locals 0
    .param p0    # I

    return-void
.end method

.method public static init()V
    .locals 0

    return-void
.end method

.method public static logW(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static logW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    invoke-static {p0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-void
.end method

.method public static maybeTrackUiExecutor(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 0
    .param p0    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object p0
.end method

.method public static onPreImeKeyEvent(Landroid/view/KeyEvent;)V
    .locals 0
    .param p0    # Landroid/view/KeyEvent;

    return-void
.end method

.method public static onStartupPoint(I)V
    .locals 0
    .param p0    # I

    return-void
.end method

.method public static onUiOperationEnd(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    return-void
.end method

.method public static onUiOperationStart(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    return-void
.end method

.method public static setThreadPolicy()V
    .locals 0

    return-void
.end method
