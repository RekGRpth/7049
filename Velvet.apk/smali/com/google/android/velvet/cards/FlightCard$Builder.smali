.class public Lcom/google/android/velvet/cards/FlightCard$Builder;
.super Ljava/lang/Object;
.source "FlightCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/FlightCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mAirlineName:Ljava/lang/String;

.field private mFlightAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

.field private final mFlightNumber:Ljava/lang/String;

.field private mGmailReferenceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;"
        }
    .end annotation
.end field

.field private mNavigateLabel:Ljava/lang/String;

.field private final mSegments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mAirlineName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mFlightNumber:Ljava/lang/String;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mSegments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addSegment()Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;
    .locals 2

    new-instance v0, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;

    invoke-direct {v0}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mSegments:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public setFlightStatusEntryAdapter(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;)Lcom/google/android/velvet/cards/FlightCard$Builder;
    .locals 0
    .param p1    # Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mFlightAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    return-object p0
.end method

.method public setGmailReferenceList(Ljava/util/List;)Lcom/google/android/velvet/cards/FlightCard$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/geo/sidekick/Sidekick$GmailReference;",
            ">;)",
            "Lcom/google/android/velvet/cards/FlightCard$Builder;"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mGmailReferenceList:Ljava/util/List;

    return-object p0
.end method

.method public setNavigateAction(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mNavigateLabel:Ljava/lang/String;

    return-object p0
.end method

.method public update(Lcom/google/android/velvet/cards/FlightCard;)V
    .locals 8
    .param p1    # Lcom/google/android/velvet/cards/FlightCard;

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mFlightAdapter:Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;

    iget-object v2, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mAirlineName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mFlightNumber:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mNavigateLabel:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mGmailReferenceList:Ljava/util/List;

    move-object v0, p1

    # invokes: Lcom/google/android/velvet/cards/FlightCard;->setFlightInfo(Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/velvet/cards/FlightCard;->access$000(Lcom/google/android/velvet/cards/FlightCard;Lcom/google/android/apps/sidekick/FlightStatusEntryAdapter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    # invokes: Lcom/google/android/velvet/cards/FlightCard;->removeSegments()V
    invoke-static {p1}, Lcom/google/android/velvet/cards/FlightCard;->access$100(Lcom/google/android/velvet/cards/FlightCard;)V

    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$Builder;->mSegments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;

    # invokes: Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->addTo(Lcom/google/android/velvet/cards/FlightCard;)V
    invoke-static {v7, p1}, Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;->access$200(Lcom/google/android/velvet/cards/FlightCard$SegmentBuilder;Lcom/google/android/velvet/cards/FlightCard;)V

    goto :goto_0

    :cond_0
    return-void
.end method
