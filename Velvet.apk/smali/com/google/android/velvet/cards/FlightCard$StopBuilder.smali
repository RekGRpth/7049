.class public Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
.super Ljava/lang/Object;
.source "FlightCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/FlightCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StopBuilder"
.end annotation


# instance fields
.field private mActual:Ljava/util/Calendar;

.field private mAirportCode:Ljava/lang/String;

.field private mAirportName:Ljava/lang/String;

.field private mGate:Ljava/lang/String;

.field private mScheduled:Ljava/util/Calendar;

.field private mTerminal:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;)Ljava/util/Calendar;
    .locals 1
    .param p0    # Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mActual:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;)Ljava/util/Calendar;
    .locals 1
    .param p0    # Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mScheduled:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/velvet/cards/FlightCard$StopBuilder;

    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mAirportName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/velvet/cards/FlightCard$StopBuilder;Lcom/google/android/velvet/cards/FlightCard;Ljava/util/Calendar;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .param p1    # Lcom/google/android/velvet/cards/FlightCard;
    .param p2    # Ljava/util/Calendar;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->addTo(Lcom/google/android/velvet/cards/FlightCard;Ljava/util/Calendar;)V

    return-void
.end method

.method private addTo(Lcom/google/android/velvet/cards/FlightCard;Ljava/util/Calendar;)V
    .locals 8
    .param p1    # Lcom/google/android/velvet/cards/FlightCard;
    .param p2    # Ljava/util/Calendar;

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mAirportName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mAirportCode:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mTerminal:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mGate:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mScheduled:Ljava/util/Calendar;

    iget-object v6, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mActual:Ljava/util/Calendar;

    move-object v0, p1

    move-object v7, p2

    # invokes: Lcom/google/android/velvet/cards/FlightCard;->populateStop(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V
    invoke-static/range {v0 .. v7}, Lcom/google/android/velvet/cards/FlightCard;->access$800(Lcom/google/android/velvet/cards/FlightCard;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V

    return-void
.end method


# virtual methods
.method public setActual(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .locals 0
    .param p1    # Ljava/util/Calendar;

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mActual:Ljava/util/Calendar;

    return-object p0
.end method

.method public setAirportCode(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mAirportCode:Ljava/lang/String;

    return-object p0
.end method

.method public setAirportName(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mAirportName:Ljava/lang/String;

    return-object p0
.end method

.method public setGate(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mGate:Ljava/lang/String;

    return-object p0
.end method

.method public setScheduled(Ljava/util/Calendar;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .locals 0
    .param p1    # Ljava/util/Calendar;

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mScheduled:Ljava/util/Calendar;

    return-object p0
.end method

.method public setTerminal(Ljava/lang/String;)Lcom/google/android/velvet/cards/FlightCard$StopBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightCard$StopBuilder;->mTerminal:Ljava/lang/String;

    return-object p0
.end method
