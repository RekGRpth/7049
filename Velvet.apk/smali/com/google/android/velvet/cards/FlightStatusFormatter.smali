.class public Lcom/google/android/velvet/cards/FlightStatusFormatter;
.super Ljava/lang/Object;
.source "FlightStatusFormatter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/velvet/cards/FlightStatusFormatter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/velvet/cards/FlightStatusFormatter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/cards/FlightStatusFormatter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public formatDateTime(Ljava/util/Calendar;I)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/util/Calendar;
    .param p2    # I

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1, v8}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v9, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/cards/FlightStatusFormatter;->mContext:Landroid/content/Context;

    const-string v7, "UTC"

    move-wide v4, v2

    move v6, p2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public formatTime(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/util/Calendar;

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightStatusFormatter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getColorForStatus(I)I
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightStatusFormatter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    const v1, 0x1060012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_0
    return v1

    :pswitch_0
    const v1, 0x7f09003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    :pswitch_1
    const v1, 0x7f090038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getFlightStatus(ILjava/util/Calendar;)Ljava/lang/CharSequence;
    .locals 10
    .param p1    # I
    .param p2    # Ljava/util/Calendar;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const v4, 0x80016

    invoke-virtual {p0, p2, v4}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->formatDateTime(Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->getColorForStatus(I)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/cards/FlightStatusFormatter;->getStatusSummary(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "#%1$h"

    new-array v6, v9, [Ljava/lang/Object;

    const v7, 0xffffff

    and-int/2addr v7, v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/velvet/cards/FlightStatusFormatter;->mContext:Landroid/content/Context;

    const v5, 0x7f0d0120

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v8

    aput-object v3, v6, v9

    const/4 v7, 0x2

    aput-object v1, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    return-object v4
.end method

.method public getStatusSummary(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/velvet/cards/FlightStatusFormatter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-lt p1, v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/cards/FlightStatusFormatter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown status code "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    :cond_0
    aget-object v1, v0, p1

    return-object v1
.end method
