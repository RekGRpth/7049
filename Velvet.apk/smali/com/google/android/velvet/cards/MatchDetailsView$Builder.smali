.class public Lcom/google/android/velvet/cards/MatchDetailsView$Builder;
.super Ljava/lang/Object;
.source "MatchDetailsView.java"

# interfaces
.implements Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/MatchDetailsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCumulativeScoreHeaders:[I

.field private mDate:Ljava/util/Calendar;

.field private mFirst:Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

.field private mLocation:Ljava/lang/String;

.field private mPeriodNumbers:[I

.field private mSecond:Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;


# direct methods
.method public constructor <init>([I)V
    .locals 3
    .param p1    # [I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0d0145

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;-><init>([I[I)V

    return-void
.end method

.method public constructor <init>([I[I)V
    .locals 4
    .param p1    # [I
    .param p2    # [I

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mPeriodNumbers:[I

    iput-object p2, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mCumulativeScoreHeaders:[I

    new-instance v0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    array-length v1, p1

    array-length v2, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;-><init>(IILcom/google/android/velvet/cards/MatchDetailsView$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mFirst:Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    new-instance v0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    array-length v1, p1

    array-length v2, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;-><init>(IILcom/google/android/velvet/cards/MatchDetailsView$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mSecond:Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    return-void
.end method


# virtual methods
.method public bridge synthetic build(Landroid/content/Context;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->build(Landroid/content/Context;)Lcom/google/android/velvet/cards/MatchDetailsView;

    move-result-object v0

    return-object v0
.end method

.method public build(Landroid/content/Context;)Lcom/google/android/velvet/cards/MatchDetailsView;
    .locals 5
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/google/android/velvet/cards/MatchDetailsView;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/cards/MatchDetailsView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mDate:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mLocation:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mPeriodNumbers:[I

    iget-object v4, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mCumulativeScoreHeaders:[I

    # invokes: Lcom/google/android/velvet/cards/MatchDetailsView;->setMatchDetails(Ljava/util/Calendar;Ljava/lang/String;[I[I)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/velvet/cards/MatchDetailsView;->access$100(Lcom/google/android/velvet/cards/MatchDetailsView;Ljava/util/Calendar;Ljava/lang/String;[I[I)V

    iget-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mFirst:Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->addTo(Lcom/google/android/velvet/cards/MatchDetailsView;I)V
    invoke-static {v1, v0, v2}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->access$200(Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;Lcom/google/android/velvet/cards/MatchDetailsView;I)V

    iget-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mSecond:Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->addTo(Lcom/google/android/velvet/cards/MatchDetailsView;I)V
    invoke-static {v1, v0, v2}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->access$200(Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;Lcom/google/android/velvet/cards/MatchDetailsView;I)V

    return-object v0
.end method

.method public firstContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mFirst:Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    return-object v0
.end method

.method public secondContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->mSecond:Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    return-object v0
.end method
