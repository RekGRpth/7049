.class public Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;
.super Ljava/lang/Object;
.source "MatchDetailsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/MatchDetailsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContestantBuilder"
.end annotation


# instance fields
.field private mCumulativeScores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mPeriodScores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->createNullList(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mPeriodScores:Ljava/util/List;

    invoke-static {p2}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->createNullList(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mCumulativeScores:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(IILcom/google/android/velvet/cards/MatchDetailsView$1;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/google/android/velvet/cards/MatchDetailsView$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;-><init>(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;Lcom/google/android/velvet/cards/MatchDetailsView;I)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;
    .param p1    # Lcom/google/android/velvet/cards/MatchDetailsView;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->addTo(Lcom/google/android/velvet/cards/MatchDetailsView;I)V

    return-void
.end method

.method private addTo(Lcom/google/android/velvet/cards/MatchDetailsView;I)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/cards/MatchDetailsView;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mPeriodScores:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mCumulativeScores:Ljava/util/List;

    # invokes: Lcom/google/android/velvet/cards/MatchDetailsView;->addScores(ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    invoke-static {p1, p2, v0, v1, v2}, Lcom/google/android/velvet/cards/MatchDetailsView;->access$300(Lcom/google/android/velvet/cards/MatchDetailsView;ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static createNullList(I)Ljava/util/List;
    .locals 3
    .param p0    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_0

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mName:Ljava/lang/String;

    return-object p0
.end method

.method public setPeriodScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mPeriodScores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkElementIndex(II)I

    iget-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mPeriodScores:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->mCumulativeScores:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setScore(Ljava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v0

    return-object v0
.end method
