.class public Lcom/google/android/velvet/cards/MatchDetailsView;
.super Landroid/widget/TableLayout;
.source "MatchDetailsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/cards/MatchDetailsView$1;,
        Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;,
        Lcom/google/android/velvet/cards/MatchDetailsView$Builder;
    }
.end annotation


# instance fields
.field private mHeader:Landroid/view/ViewGroup;

.field private mTeam1Name:Landroid/widget/TextView;

.field private mTeam1Scores:Landroid/view/ViewGroup;

.field private mTeam2Name:Landroid/widget/TextView;

.field private mTeam2Scores:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/MatchDetailsView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/MatchDetailsView;->init()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/cards/MatchDetailsView;Ljava/util/Calendar;Ljava/lang/String;[I[I)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/MatchDetailsView;
    .param p1    # Ljava/util/Calendar;
    .param p2    # Ljava/lang/String;
    .param p3    # [I
    .param p4    # [I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/cards/MatchDetailsView;->setMatchDetails(Ljava/util/Calendar;Ljava/lang/String;[I[I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/cards/MatchDetailsView;ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/MatchDetailsView;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/util/List;
    .param p4    # Ljava/util/List;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/cards/MatchDetailsView;->addScores(ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private addCell(Landroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/MatchDetailsView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d012a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/cards/MatchDetailsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f04004c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private addScores(ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mTeam1Name:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mTeam1Scores:Landroid/view/ViewGroup;

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/velvet/cards/MatchDetailsView;->addCell(Landroid/view/ViewGroup;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    iget-object v4, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mTeam2Name:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mTeam2Scores:Landroid/view/ViewGroup;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/cards/MatchDetailsView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f04004b

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/velvet/cards/MatchDetailsView;->addCell(Landroid/view/ViewGroup;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    return-void
.end method

.method private init()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/MatchDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bc

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f100133

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/MatchDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mHeader:Landroid/view/ViewGroup;

    const v1, 0x7f10023b

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/MatchDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mTeam1Scores:Landroid/view/ViewGroup;

    const v1, 0x7f10023d

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/MatchDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mTeam2Scores:Landroid/view/ViewGroup;

    const v1, 0x7f10023c

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/MatchDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mTeam1Name:Landroid/widget/TextView;

    const v1, 0x7f10023e

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/MatchDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mTeam2Name:Landroid/widget/TextView;

    return-void
.end method

.method private setMatchDetails(Ljava/util/Calendar;Ljava/lang/String;[I[I)V
    .locals 9
    .param p1    # Ljava/util/Calendar;
    .param p2    # Ljava/lang/String;
    .param p3    # [I
    .param p4    # [I

    move-object v0, p3

    array-length v4, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget v5, v0, v2

    iget-object v6, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mHeader:Landroid/view/ViewGroup;

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/velvet/cards/MatchDetailsView;->addCell(Landroid/view/ViewGroup;Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/cards/MatchDetailsView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v6, 0x7f04004b

    iget-object v7, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mHeader:Landroid/view/ViewGroup;

    const/4 v8, 0x1

    invoke-virtual {v3, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-object v0, p4

    array-length v4, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_1

    aget v1, v0, v2

    iget-object v6, p0, Lcom/google/android/velvet/cards/MatchDetailsView;->mHeader:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/MatchDetailsView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/velvet/cards/MatchDetailsView;->addCell(Landroid/view/ViewGroup;Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
