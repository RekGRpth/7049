.class public Lcom/google/android/velvet/cards/SportsMatchCard$Builder;
.super Ljava/lang/Object;
.source "SportsMatchCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/SportsMatchCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mDetailsBuilder:Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;

.field private mLeftContestant:Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

.field private mRightContestant:Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

.field private mStatus:Ljava/lang/CharSequence;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    invoke-direct {v0}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mLeftContestant:Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    new-instance v0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    invoke-direct {v0}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mRightContestant:Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    return-void
.end method


# virtual methods
.method public build(Landroid/content/Context;)Lcom/google/android/velvet/cards/SportsMatchCard;
    .locals 1
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/google/android/velvet/cards/SportsMatchCard;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/cards/SportsMatchCard;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->update(Lcom/google/android/velvet/cards/SportsMatchCard;)V

    return-object v0
.end method

.method public leftContestant()Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mLeftContestant:Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    return-object v0
.end method

.method public rightContestant()Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mRightContestant:Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    return-object v0
.end method

.method public setMatchDetailsBuilder(Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;)Lcom/google/android/velvet/cards/SportsMatchCard$Builder;
    .locals 0
    .param p1    # Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;

    iput-object p1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mDetailsBuilder:Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;

    return-object p0
.end method

.method public setStatus(Ljava/lang/CharSequence;)Lcom/google/android/velvet/cards/SportsMatchCard$Builder;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mStatus:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mTitle:Ljava/lang/String;

    return-object p0
.end method

.method public update(Lcom/google/android/velvet/cards/SportsMatchCard;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/cards/SportsMatchCard;

    iget-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mStatus:Ljava/lang/CharSequence;

    # invokes: Lcom/google/android/velvet/cards/SportsMatchCard;->setMatchInfo(Ljava/lang/String;Ljava/lang/CharSequence;)V
    invoke-static {p1, v1, v2}, Lcom/google/android/velvet/cards/SportsMatchCard;->access$000(Lcom/google/android/velvet/cards/SportsMatchCard;Ljava/lang/String;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mLeftContestant:Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    const/4 v2, 0x1

    # invokes: Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->update(Lcom/google/android/velvet/cards/SportsMatchCard;Z)V
    invoke-static {v1, p1, v2}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->access$100(Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;Lcom/google/android/velvet/cards/SportsMatchCard;Z)V

    iget-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mRightContestant:Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->update(Lcom/google/android/velvet/cards/SportsMatchCard;Z)V
    invoke-static {v1, p1, v2}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->access$100(Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;Lcom/google/android/velvet/cards/SportsMatchCard;Z)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mDetailsBuilder:Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->mDetailsBuilder:Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;

    invoke-virtual {p1}, Lcom/google/android/velvet/cards/SportsMatchCard;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;->build(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    :cond_0
    # invokes: Lcom/google/android/velvet/cards/SportsMatchCard;->setDetailsView(Landroid/view/View;)V
    invoke-static {p1, v0}, Lcom/google/android/velvet/cards/SportsMatchCard;->access$200(Lcom/google/android/velvet/cards/SportsMatchCard;Landroid/view/View;)V

    return-void
.end method
