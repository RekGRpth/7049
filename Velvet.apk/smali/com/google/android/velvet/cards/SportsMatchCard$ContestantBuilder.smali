.class public Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;
.super Ljava/lang/Object;
.source "SportsMatchCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/SportsMatchCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContestantBuilder"
.end annotation


# instance fields
.field private mLogo:Landroid/net/Uri;

.field private mLogoClip:Landroid/graphics/Rect;

.field private mName:Ljava/lang/String;

.field private mScore:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;Lcom/google/android/velvet/cards/SportsMatchCard;Z)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;
    .param p1    # Lcom/google/android/velvet/cards/SportsMatchCard;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->update(Lcom/google/android/velvet/cards/SportsMatchCard;Z)V

    return-void
.end method

.method private update(Lcom/google/android/velvet/cards/SportsMatchCard;Z)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/cards/SportsMatchCard;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mLogo:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mLogoClip:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mScore:Ljava/lang/String;

    # invokes: Lcom/google/android/velvet/cards/SportsMatchCard;->setLeftContestant(Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V
    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/android/velvet/cards/SportsMatchCard;->access$300(Lcom/google/android/velvet/cards/SportsMatchCard;Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mLogo:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mLogoClip:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mScore:Ljava/lang/String;

    # invokes: Lcom/google/android/velvet/cards/SportsMatchCard;->setRightContestant(Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V
    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/android/velvet/cards/SportsMatchCard;->access$400(Lcom/google/android/velvet/cards/SportsMatchCard;Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Rect;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public setLogo(Landroid/net/Uri;Landroid/graphics/Rect;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/graphics/Rect;

    iput-object p1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mLogo:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mLogoClip:Landroid/graphics/Rect;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mName:Ljava/lang/String;

    return-object p0
.end method

.method public setScore(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->mScore:Ljava/lang/String;

    return-object p0
.end method
