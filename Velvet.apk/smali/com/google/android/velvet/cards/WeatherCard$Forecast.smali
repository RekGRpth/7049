.class Lcom/google/android/velvet/cards/WeatherCard$Forecast;
.super Ljava/lang/Object;
.source "WeatherCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/cards/WeatherCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Forecast"
.end annotation


# instance fields
.field mDescription:Ljava/lang/CharSequence;

.field mHigh:I

.field mIcon:Landroid/net/Uri;

.field mLabel:Ljava/lang/CharSequence;

.field mLow:I


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/net/Uri;II)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/net/Uri;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mLabel:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mIcon:Landroid/net/Uri;

    iput p3, p0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mLow:I

    iput p4, p0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mHigh:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mLabel:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mDescription:Ljava/lang/CharSequence;

    return-void
.end method
