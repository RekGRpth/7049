.class public Lcom/google/android/velvet/cards/WeatherCard;
.super Landroid/widget/LinearLayout;
.source "WeatherCard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/cards/WeatherCard$Forecast;,
        Lcom/google/android/velvet/cards/WeatherCard$ForecastBuilder;,
        Lcom/google/android/velvet/cards/WeatherCard$Builder;
    }
.end annotation


# instance fields
.field private mCurrentChanceOfPrecipitation:Landroid/widget/TextView;

.field private mCurrentConditions:Landroid/widget/TextView;

.field private mCurrentTemperature:Landroid/widget/TextView;

.field private mCurrentWeatherIcon:Lcom/google/android/velvet/ui/WebImageView;

.field private mCurrentWindSpeed:Landroid/widget/TextView;

.field private mLaterConditionChanges:Landroid/view/ViewGroup;

.field private mLocationName:Landroid/widget/TextView;

.field private mWeekGrid:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/WeatherCard;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/WeatherCard;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/android/velvet/cards/WeatherCard;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/cards/WeatherCard;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/WeatherCard;
    .param p1    # I
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Ljava/lang/Integer;
    .param p6    # Ljava/lang/String;

    invoke-direct/range {p0 .. p6}, Lcom/google/android/velvet/cards/WeatherCard;->setCurrentWeather(ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/cards/WeatherCard;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/WeatherCard;
    .param p1    # Ljava/lang/CharSequence;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/cards/WeatherCard;->setLocation(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/cards/WeatherCard;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/cards/WeatherCard;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/cards/WeatherCard;->setWeekdayForecasts(Ljava/util/List;)V

    return-void
.end method

.method private init()V
    .locals 4

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/cards/WeatherCard;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->setOrientation(I)V

    const v2, 0x7f02000b

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/WeatherCard;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0400d4

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v2, 0x7f100031

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard;->mLocationName:Landroid/widget/TextView;

    const v2, 0x7f10027d

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentTemperature:Landroid/widget/TextView;

    const v2, 0x7f100279

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentWeatherIcon:Lcom/google/android/velvet/ui/WebImageView;

    const v2, 0x7f10027a

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentConditions:Landroid/widget/TextView;

    const v2, 0x7f10027c

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentChanceOfPrecipitation:Landroid/widget/TextView;

    const v2, 0x7f10027b

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentWindSpeed:Landroid/widget/TextView;

    const v2, 0x7f10027f

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TableLayout;

    iput-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard;->mWeekGrid:Landroid/view/ViewGroup;

    const v2, 0x7f10027e

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/cards/WeatherCard;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TableLayout;

    iput-object v2, p0, Lcom/google/android/velvet/cards/WeatherCard;->mLaterConditionChanges:Landroid/view/ViewGroup;

    return-void
.end method

.method private setCurrentWeather(ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 9
    .param p1    # I
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Ljava/lang/Integer;
    .param p6    # Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentTemperature:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentTemperature:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/WeatherCard;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0134

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentWeatherIcon:Lcom/google/android/velvet/ui/WebImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentWeatherIcon:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v3, p2}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    :cond_0
    if-eqz p3, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentConditions:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentConditions:Landroid/widget/TextView;

    invoke-static {p3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    if-eqz p4, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/WeatherCard;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0135

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentChanceOfPrecipitation:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentChanceOfPrecipitation:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    if-eqz p5, :cond_3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/WeatherCard;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d0136

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p5, v4, v5

    const/4 v5, 0x1

    aput-object p6, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentWindSpeed:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/velvet/cards/WeatherCard;->mCurrentWindSpeed:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method private setLocation(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/velvet/cards/WeatherCard;->mLocationName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setWeekdayForecasts(Ljava/util/List;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/cards/WeatherCard$Forecast;",
            ">;)V"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/cards/WeatherCard;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/cards/WeatherCard;->mWeekGrid:Landroid/view/ViewGroup;

    const v16, 0x7f100280

    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TableRow;

    invoke-virtual {v9}, Landroid/widget/TableRow;->removeAllViews()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/cards/WeatherCard;->mWeekGrid:Landroid/view/ViewGroup;

    const v16, 0x7f100281

    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TableRow;

    invoke-virtual {v6}, Landroid/widget/TableRow;->removeAllViews()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/cards/WeatherCard;->mWeekGrid:Landroid/view/ViewGroup;

    const v16, 0x7f100283

    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TableRow;

    invoke-virtual {v13}, Landroid/widget/TableRow;->removeAllViews()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/cards/WeatherCard;->mWeekGrid:Landroid/view/ViewGroup;

    const v16, 0x7f100282

    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TableRow;

    invoke-virtual {v3}, Landroid/widget/TableRow;->removeAllViews()V

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/cards/WeatherCard;->mWeekGrid:Landroid/view/ViewGroup;

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/cards/WeatherCard;->mWeekGrid:Landroid/view/ViewGroup;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/cards/WeatherCard;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090031

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/cards/WeatherCard$Forecast;

    const v15, 0x7f04004c

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v15, v9, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iget-object v15, v1, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mLabel:Ljava/lang/CharSequence;

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v15, 0x1

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setAllCaps(Z)V

    invoke-virtual {v9, v10}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    const v15, 0x7f0400d5

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v15, v6, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/velvet/ui/WebImageView;

    iget-object v15, v1, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mIcon:Landroid/net/Uri;

    invoke-virtual {v7, v15}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    invoke-virtual {v6, v7}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/cards/WeatherCard;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0d0134

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    iget v0, v1, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mHigh:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v15, 0x7f04004c

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v15, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v15

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    invoke-virtual {v3, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/cards/WeatherCard;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0d0134

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    iget v0, v1, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mLow:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const v15, 0x7f04004c

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v15, v13, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    invoke-virtual {v14, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v14, v12}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v13, v14}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    goto/16 :goto_0
.end method


# virtual methods
.method setLaterConditionChanges(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/cards/WeatherCard$Forecast;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/cards/WeatherCard;->mLaterConditionChanges:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/velvet/cards/WeatherCard;->mLaterConditionChanges:Landroid/view/ViewGroup;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/velvet/cards/WeatherCard;->mLaterConditionChanges:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/cards/WeatherCard;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;

    const v4, 0x7f0400d7

    iget-object v5, p0, Lcom/google/android/velvet/cards/WeatherCard;->mLaterConditionChanges:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TableRow;

    const v4, 0x7f10028e

    invoke-virtual {v3, v4}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mLabel:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f10028f

    invoke-virtual {v3, v4}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/android/velvet/cards/WeatherCard$Forecast;->mDescription:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/velvet/cards/WeatherCard;->mLaterConditionChanges:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
