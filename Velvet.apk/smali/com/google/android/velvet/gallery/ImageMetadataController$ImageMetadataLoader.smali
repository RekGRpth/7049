.class Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;
.super Ljava/lang/Object;
.source "ImageMetadataController.java"

# interfaces
.implements Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/gallery/ImageMetadataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageMetadataLoader"
.end annotation


# instance fields
.field private final mExtractor:Lcom/google/android/velvet/gallery/ImageMetadataExtractor;

.field private final mImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;"
        }
    .end annotation
.end field

.field private final mStartIndex:I

.field private mXml:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/gallery/ImageMetadataController;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mStartIndex:I

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mImages:Ljava/util/List;

    new-instance v0, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;-><init>(Lcom/google/android/velvet/gallery/ImageMetadataExtractor$ResultHandler;)V

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mExtractor:Lcom/google/android/velvet/gallery/ImageMetadataExtractor;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/gallery/ImageMetadataController;ILcom/google/android/velvet/gallery/ImageMetadataController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/gallery/ImageMetadataController;
    .param p2    # I
    .param p3    # Lcom/google/android/velvet/gallery/ImageMetadataController$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;-><init>(Lcom/google/android/velvet/gallery/ImageMetadataController;I)V

    return-void
.end method


# virtual methods
.method public onMetadataSectionExtracted(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    # getter for: Lcom/google/android/velvet/gallery/ImageMetadataController;->mParser:Lcom/google/android/velvet/gallery/ImageMetadataParser;
    invoke-static {v1}, Lcom/google/android/velvet/gallery/ImageMetadataController;->access$500(Lcom/google/android/velvet/gallery/ImageMetadataController;)Lcom/google/android/velvet/gallery/ImageMetadataParser;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/gallery/ImageMetadataParser;->readJson(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    # += operator for: Lcom/google/android/velvet/gallery/ImageMetadataController;->mPulledCount:I
    invoke-static {v1, v2}, Lcom/google/android/velvet/gallery/ImageMetadataController;->access$612(Lcom/google/android/velvet/gallery/ImageMetadataController;I)I

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mImages:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public run()V
    .locals 9

    const/4 v2, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mXml:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mImages:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    if-nez v5, :cond_0

    iget-object v6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v7, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    # getter for: Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;
    invoke-static {v7}, Lcom/google/android/velvet/gallery/ImageMetadataController;->access$400(Lcom/google/android/velvet/gallery/ImageMetadataController;)Lcom/google/android/velvet/Query;

    move-result-object v7

    iget v8, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mStartIndex:I

    invoke-virtual {v6, v7, v8}, Lcom/google/android/velvet/gallery/ImageMetadataController;->fetchXml(Lcom/google/android/velvet/Query;I)Ljava/lang/String;

    move-result-object v5

    :cond_0
    if-eqz v5, :cond_1

    iget-object v6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mExtractor:Lcom/google/android/velvet/gallery/ImageMetadataExtractor;

    invoke-virtual {v6, v5}, Lcom/google/android/velvet/gallery/ImageMetadataExtractor;->parseXml(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x1

    :cond_1
    :goto_0
    if-eqz v2, :cond_3

    iget-object v6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mImages:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/velvet/gallery/VelvetImage;

    invoke-virtual {v3}, Lcom/google/android/velvet/gallery/VelvetImage;->getUri()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v2, 0x0

    :cond_3
    if-eqz v2, :cond_4

    iget-object v6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v7, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mImages:Ljava/util/List;

    const/4 v8, 0x1

    invoke-virtual {v6, p0, v7, v8}, Lcom/google/android/velvet/gallery/ImageMetadataController;->loadComplete(Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;Ljava/util/List;Z)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v6, "Velvet.ImageMetadataController"

    const-string v7, "IOException fetching image metadata"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v4

    const-string v6, "Velvet.ImageMetadataController"

    const-string v7, "IllegalStateException fetching image metadata"

    invoke-static {v6, v7, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_4
    iget-object v6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mXml:Ljava/lang/String;

    if-eqz v6, :cond_5

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mXml:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->run()V

    goto :goto_1

    :cond_5
    iget-object v6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->this$0:Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v7, p0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;->mImages:Ljava/util/List;

    const/4 v8, 0x0

    invoke-virtual {v6, p0, v7, v8}, Lcom/google/android/velvet/gallery/ImageMetadataController;->loadComplete(Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;Ljava/util/List;Z)V

    goto :goto_1
.end method
