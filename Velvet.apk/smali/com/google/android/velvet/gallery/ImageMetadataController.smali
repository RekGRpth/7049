.class public Lcom/google/android/velvet/gallery/ImageMetadataController;
.super Ljava/lang/Object;
.source "ImageMetadataController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;
    }
.end annotation


# static fields
.field protected static final TIMER_DURATION_SECONDS:J = 0x2L


# instance fields
.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mContext:Landroid/content/Context;

.field private mCurrentLoader:Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;

.field private mCurrentTimer:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mInitialImageData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;"
        }
    .end annotation
.end field

.field private mIsParsing:Z

.field private final mLoadedImageData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;"
        }
    .end annotation
.end field

.field private mNoMoreImages:Z

.field private final mParser:Lcom/google/android/velvet/gallery/ImageMetadataParser;

.field private mPulledCount:I

.field private mQuery:Lcom/google/android/velvet/Query;

.field private final mTimerExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mTimerRunnable:Ljava/lang/Runnable;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private mWaitingForId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/velvet/gallery/ImageMetadataParser;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p3    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p4    # Ljava/util/concurrent/Executor;
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
    .param p6    # Lcom/google/android/velvet/gallery/ImageMetadataParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/velvet/gallery/ImageMetadataController$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/gallery/ImageMetadataController$1;-><init>(Lcom/google/android/velvet/gallery/ImageMetadataController;)V

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mTimerRunnable:Ljava/lang/Runnable;

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p3, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p4, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mBgExecutor:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mParser:Lcom/google/android/velvet/gallery/ImageMetadataParser;

    iput-object p5, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mTimerExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mInitialImageData:Ljava/util/List;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/velvet/gallery/ImageMetadataController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/velvet/gallery/ImageMetadataController;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/velvet/gallery/ImageMetadataController;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/gallery/ImageMetadataController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/gallery/ImageMetadataController;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p0    # Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/gallery/ImageMetadataController;)Lcom/google/android/velvet/gallery/ImageMetadataParser;
    .locals 1
    .param p0    # Lcom/google/android/velvet/gallery/ImageMetadataController;

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mParser:Lcom/google/android/velvet/gallery/ImageMetadataParser;

    return-object v0
.end method

.method static synthetic access$612(Lcom/google/android/velvet/gallery/ImageMetadataController;I)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/gallery/ImageMetadataController;
    .param p1    # I

    iget v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mPulledCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mPulledCount:I

    return v0
.end method

.method private cancelCurrentLoad()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentLoader:Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mIsParsing:Z

    return-void
.end method

.method private cancelWaitingTimeout()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentTimer:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentTimer:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method private setQueryInternal(Lcom/google/android/velvet/Query;Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->equivalentForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object p2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->cancelWaitingTimeout()V

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->startWaitingTimeout()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->cancelCurrentLoad()V

    iput-boolean v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mNoMoreImages:Z

    iput v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mPulledCount:I

    :cond_2
    iput-object p1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;

    return-void
.end method

.method private startWaitingTimeout()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mTimerExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mTimerRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentTimer:Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

.method private static velvetImageFromParcelable(ILcom/google/android/voicesearch/speechservice/ImageParcelable;)Lcom/google/android/velvet/gallery/VelvetImage;
    .locals 4
    .param p0    # I
    .param p1    # Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/ImageParcelable;->getImage()Lcom/google/majel/proto/PeanutProtos$Image;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/PeanutProtos$Image;->getAttribution(I)Lcom/google/majel/proto/AttributionProtos$Attribution;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/gallery/VelvetImage;

    invoke-direct {v1}, Lcom/google/android/velvet/gallery/VelvetImage;-><init>()V

    invoke-virtual {v0}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/gallery/VelvetImage;->setName(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/majel/proto/PeanutProtos$Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/gallery/VelvetImage;->setUri(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/majel/proto/PeanutProtos$Image;->getThumbUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/gallery/VelvetImage;->setThumbnailUri(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageDomain()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/gallery/VelvetImage;->setDomain(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/majel/proto/PeanutProtos$Image;->getWidth()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/gallery/VelvetImage;->setWidth(I)V

    invoke-virtual {v2}, Lcom/google/majel/proto/PeanutProtos$Image;->getHeight()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/gallery/VelvetImage;->setHeight(I)V

    invoke-virtual {v0}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/gallery/VelvetImage;->setSourceUri(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/velvet/gallery/VelvetImage;->setId(Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public declared-synchronized fetchMoreImages()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentLoader:Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mIsParsing:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mNoMoreImages:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->canUseToSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;

    iget v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mPulledCount:I

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;-><init>(Lcom/google/android/velvet/gallery/ImageMetadataController;ILcom/google/android/velvet/gallery/ImageMetadataController$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentLoader:Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mBgExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentLoader:Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected fetchXml(Lcom/google/android/velvet/Query;I)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, v2, p2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getImageMetadataUrl(Lcom/google/android/velvet/Query;I)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    new-instance v2, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x7

    invoke-interface {v1, v2, v3}, Lcom/google/android/searchcommon/util/HttpHelper;->get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected getCurrentLoader()Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentLoader:Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;

    return-object v0
.end method

.method public declared-synchronized getImage(I)Lcom/google/android/velvet/gallery/VelvetImage;
    .locals 1
    .param p1    # I

    monitor-enter p0

    if-ltz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/gallery/VelvetImage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getImages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getPulledCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mPulledCount:I

    return v0
.end method

.method protected declared-synchronized loadComplete(Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;Ljava/util/List;Z)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;Z)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentLoader:Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;

    if-ne p1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/gallery/VelvetImage;

    invoke-virtual {v1}, Lcom/google/android/velvet/gallery/VelvetImage;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->cancelWaitingTimeout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    if-eqz p3, :cond_3

    :try_start_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {p2, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/velvet/gallery/ImageProvider;->reportMoreImagesAvailable(Landroid/content/Context;I)V

    :goto_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mCurrentLoader:Lcom/google/android/velvet/gallery/ImageMetadataController$ImageMetadataLoader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    const/4 v2, 0x1

    :try_start_2
    iput-boolean v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mNoMoreImages:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized setQuery(Lcom/google/android/velvet/Query;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->setQueryInternal(Lcom/google/android/velvet/Query;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->fetchMoreImages()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setQueryWithImageProtos(Lcom/google/android/velvet/Query;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/speechservice/ImageParcelable;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/velvet/gallery/ImageMetadataController;->setQueryInternal(Lcom/google/android/velvet/Query;Ljava/lang/String;Z)V

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    invoke-static {v0, v2}, Lcom/google/android/velvet/gallery/ImageMetadataController;->velvetImageFromParcelable(ILcom/google/android/voicesearch/speechservice/ImageParcelable;)Lcom/google/android/velvet/gallery/VelvetImage;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mInitialImageData:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setQueryWithImages(Lcom/google/android/velvet/Query;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/Query;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->setQueryInternal(Lcom/google/android/velvet/Query;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setQueryWithJson(Lcom/google/android/velvet/Query;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/velvet/gallery/ImageMetadataController;->setQueryInternal(Lcom/google/android/velvet/Query;Ljava/lang/String;Z)V

    invoke-direct {p0}, Lcom/google/android/velvet/gallery/ImageMetadataController;->cancelCurrentLoad()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mIsParsing:Z

    new-instance v1, Lcom/google/android/velvet/gallery/ImageMetadataParser;

    invoke-direct {v1}, Lcom/google/android/velvet/gallery/ImageMetadataParser;-><init>()V

    invoke-virtual {v1, p3}, Lcom/google/android/velvet/gallery/ImageMetadataParser;->readJson(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mInitialImageData:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mWaitingForId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mLoadedImageData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/velvet/gallery/ImageProvider;->reportMoreImagesAvailable(Landroid/content/Context;I)V

    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mIsParsing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/velvet/gallery/ImageMetadataController;->mNoMoreImages:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
