.class public Lcom/google/android/velvet/gallery/ImageMetadataParser;
.super Ljava/lang/Object;
.source "ImageMetadataParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/gallery/ImageMetadataParser$1;,
        Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readImage(Landroid/util/JsonReader;)Lcom/google/android/velvet/gallery/VelvetImage;
    .locals 5
    .param p1    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Lcom/google/android/velvet/gallery/VelvetImage;

    invoke-direct {v1}, Lcom/google/android/velvet/gallery/VelvetImage;-><init>()V

    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "id"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setId(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v4, "oh"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setHeight(I)V

    goto :goto_0

    :cond_2
    const-string v4, "ow"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setWidth(I)V

    goto :goto_0

    :cond_3
    const-string v4, "ou"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setUri(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v4, "rh"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setDomain(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v4, "ru"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setSourceUri(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v4, "s"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setSnippet(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string v4, "th"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-direct {p0, p1}, Lcom/google/android/velvet/gallery/ImageMetadataParser;->readThumbnailArray(Landroid/util/JsonReader;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;

    iget v4, v0, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;->height:I

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setThumbnailHeight(I)V

    iget v4, v0, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;->width:I

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setThumbnailWidth(I)V

    iget-object v4, v0, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;->url:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/google/android/velvet/gallery/VelvetImage;->setThumbnailUri(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    invoke-virtual {v1}, Lcom/google/android/velvet/gallery/VelvetImage;->getUri()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v1}, Lcom/google/android/velvet/gallery/VelvetImage;->getThumbnailUri()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_b

    :cond_a
    const/4 v1, 0x0

    :cond_b
    return-object v1
.end method

.method private readImageArray(Landroid/util/JsonReader;)Ljava/util/List;
    .locals 5
    .param p1    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonReader;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    :try_start_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/velvet/gallery/ImageMetadataParser;->readImage(Landroid/util/JsonReader;)Lcom/google/android/velvet/gallery/VelvetImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "ImageMetadataParser"

    const-string v4, "Error whilst parsing metadata:"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    return-object v2

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private readThumbnail(Landroid/util/JsonReader;)Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;
    .locals 3
    .param p1    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;-><init>(Lcom/google/android/velvet/gallery/ImageMetadataParser;Lcom/google/android/velvet/gallery/ImageMetadataParser$1;)V

    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "h"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    iput v2, v1, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;->height:I

    goto :goto_0

    :cond_0
    const-string v2, "w"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    iput v2, v1, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;->width:I

    goto :goto_0

    :cond_1
    const-string v2, "u"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;->url:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    return-object v1
.end method

.method private readThumbnailArray(Landroid/util/JsonReader;)Ljava/util/List;
    .locals 2
    .param p1    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonReader;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/gallery/ImageMetadataParser;->readThumbnail(Landroid/util/JsonReader;)Lcom/google/android/velvet/gallery/ImageMetadataParser$Thumbnail;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    return-object v0
.end method


# virtual methods
.method public readJson(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/gallery/VelvetImage;",
            ">;"
        }
    .end annotation

    new-instance v1, Landroid/util/JsonReader;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/velvet/gallery/ImageMetadataParser;->readImageArray(Landroid/util/JsonReader;)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "ImageMetadataParser"

    const-string v3, "Error whilst parsing metadata:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x0

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v2
.end method
