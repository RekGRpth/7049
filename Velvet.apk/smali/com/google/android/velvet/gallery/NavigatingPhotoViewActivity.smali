.class public Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;
.super Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;
.source "NavigatingPhotoViewActivity.java"


# instance fields
.field private mNavMenuItem:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f1002b1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->mNavMenuItem:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->getCursorAtProperPosition()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->updateNavItem(Landroid/database/Cursor;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->onPageSelected(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->getCursorAtProperPosition()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->updateNavItem(Landroid/database/Cursor;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->updateCopyrightString(Landroid/database/Cursor;)V

    return-void
.end method

.method setNavMenuItemForTest(Landroid/view/MenuItem;)V
    .locals 0
    .param p1    # Landroid/view/MenuItem;

    iput-object p1, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->mNavMenuItem:Landroid/view/MenuItem;

    return-void
.end method

.method updateCopyrightString(Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const v4, 0x7f1000f3

    invoke-virtual {p0, v4}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const-string v4, "source"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v2, 0x1

    :cond_0
    if-nez v2, :cond_1

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method updateNavItem(Landroid/database/Cursor;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->mNavMenuItem:Landroid/view/MenuItem;

    if-eqz v3, :cond_1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const-string v3, "nav_uri"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->mNavMenuItem:Landroid/view/MenuItem;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->mNavMenuItem:Landroid/view/MenuItem;

    new-instance v4, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity$1;

    invoke-direct {v4, p0, v2}, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity$1;-><init>(Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/4 v1, 0x1

    :cond_0
    if-nez v1, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->mNavMenuItem:Landroid/view/MenuItem;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/google/android/velvet/gallery/NavigatingPhotoViewActivity;->mNavMenuItem:Landroid/view/MenuItem;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_1
    return-void
.end method
