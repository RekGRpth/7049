.class Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$1;
.super Ljava/lang/Object;
.source "VelvetPhotoViewActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$1;->this$0:Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$1;->this$0:Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;

    # getter for: Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentSourceUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->access$000(Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$1;->this$0:Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;

    invoke-virtual {v2, v1}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Velvet.VelvetPhotoViewActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No activity found for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$1;->this$0:Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;

    # getter for: Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentSourceUrl:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->access$000(Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
