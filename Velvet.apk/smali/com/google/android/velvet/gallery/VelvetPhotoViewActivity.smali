.class public Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;
.super Lcom/android/ex/photo/PhotoViewActivity;
.source "VelvetPhotoViewActivity.java"

# interfaces
.implements Lcom/android/ex/photo/PhotoViewCallbacks$CursorChangedListener;


# static fields
.field private static final PHOTO_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAttributionClickListener:Landroid/view/View$OnClickListener;

.field private mCurrentDimensionsText:Ljava/lang/String;

.field private mCurrentDomainText:Ljava/lang/String;

.field private mCurrentSourceUrl:Ljava/lang/String;

.field private mCursorChanged:Z

.field private mFetchMore:Z

.field private mFetchMoreThreshold:I

.field private mFocusId:Ljava/lang/String;

.field private mImageMetadataController:Lcom/google/android/velvet/gallery/ImageMetadataController;

.field private mShareActionProvider:Landroid/widget/ShareActionProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "uri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contentUri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "thumbnailUri"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "contentType"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "loadingIndicator"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "domain"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "source"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->PHOTO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/ex/photo/PhotoViewActivity;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentSourceUrl:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDomainText:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDimensionsText:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mFetchMore:Z

    new-instance v0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$1;-><init>(Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;)V

    iput-object v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mAttributionClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentSourceUrl:Ljava/lang/String;

    return-object v0
.end method

.method public static createPhotoViewIntent(Ljava/lang/String;IZ)Landroid/content/Intent;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # Z

    invoke-static {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getPartialIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photo_index"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "fetchMore"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method public static createPhotoViewIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getPartialIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selectedId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private static getPartialIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, p0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "photos_uri"

    const-string v2, "content://com.google.android.velvet.gallery.ImageProvider/images"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "projection"

    sget-object v2, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->PHOTO_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "max_scale"

    const/high16 v2, 0x447a0000

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    return-object v0
.end method

.method private hideDetails(Z)V
    .locals 3
    .param p1    # Z

    const v1, 0x7f1000f1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity$2;-><init>(Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setPhotoIndexById(Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;

    const-string v1, "id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->setPhotoIndex(I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private setShareIntent(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mShareActionProvider:Landroid/widget/ShareActionProvider;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mShareActionProvider:Landroid/widget/ShareActionProvider;

    invoke-virtual {v1, v0}, Landroid/widget/ShareActionProvider;->setShareIntent(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private showDetails(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    const/4 v2, 0x0

    const v1, 0x7f1000f1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "selectedId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "selectedId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mFocusId:Ljava/lang/String;

    :cond_0
    const-string v2, "fetchMore"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "fetchMore"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mFetchMore:Z

    :cond_1
    invoke-super {p0, p1}, Lcom/android/ex/photo/PhotoViewActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_2

    const-string v2, "seenFocus"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCursorChanged:Z

    const-string v2, "detailLink"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentSourceUrl:Ljava/lang/String;

    const-string v2, "detailDomain"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDomainText:Ljava/lang/String;

    const-string v2, "detailDimensions"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDimensionsText:Ljava/lang/String;

    const v2, 0x7f1000f1

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->isFullScreen()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    :cond_2
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mFetchMore:Z

    if-eqz v2, :cond_3

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getImageMetadataController()Lcom/google/android/velvet/gallery/ImageMetadataController;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mImageMetadataController:Lcom/google/android/velvet/gallery/ImageMetadataController;

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/SearchConfig;->getImageFetchMoreThreshold()I

    move-result v2

    iput v2, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mFetchMoreThreshold:I

    :cond_3
    invoke-virtual {p0, p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->addCursorListener(Lcom/android/ex/photo/PhotoViewCallbacks$CursorChangedListener;)V

    return-void

    :cond_4
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f120003

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f1002b0

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionProvider()Landroid/view/ActionProvider;

    move-result-object v1

    check-cast v1, Landroid/widget/ShareActionProvider;

    iput-object v1, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mShareActionProvider:Landroid/widget/ShareActionProvider;

    const/4 v1, 0x1

    return v1
.end method

.method public onCursorChanged(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget-boolean v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCursorChanged:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCursorChanged:Z

    iget-object v0, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mFocusId:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->setPhotoIndexById(Landroid/database/Cursor;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/android/ex/photo/PhotoViewActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "photo_index"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "photo_index"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->setPhotoIndex(I)V

    :cond_0
    const-string v4, "selectedId"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "selectedId"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0, v2}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->setPhotoIndexById(Landroid/database/Cursor;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Lcom/android/ex/photo/PhotoViewActivity;->onResume()V

    const v3, 0x7f1000f1

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v3, 0x7f1000f2

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f1000f4

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mAttributionClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDomainText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDimensionsText:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/ex/photo/PhotoViewActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "seenFocus"

    iget-boolean v1, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCursorChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "detailLink"

    iget-object v1, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentSourceUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "detailDomain"

    iget-object v1, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDomainText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "detailDimensions"

    iget-object v1, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDimensionsText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected setLightsOutMode(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-super {p0, p1}, Lcom/android/ex/photo/PhotoViewActivity;->setLightsOutMode(Z)V

    if-eqz p1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->hideDetails(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->showDetails(Z)V

    goto :goto_0
.end method

.method protected updateActionBar()V
    .locals 12

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getCursorAtProperPosition()Landroid/database/Cursor;

    move-result-object v1

    const v8, 0x7f1000f1

    invoke-virtual {p0, v8}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v8, 0x7f1000f2

    invoke-virtual {p0, v8}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v8, 0x7f1000f4

    invoke-virtual {p0, v8}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    const-string v9, "_display_name"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    const-string v8, "domain"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDomainText:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDomainText:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v8, "width"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v8, "height"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v8, ""

    iput-object v8, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDimensionsText:Ljava/lang/String;

    if-lez v6, :cond_0

    if-lez v5, :cond_0

    const v8, 0x7f0d0384

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v7

    const/4 v10, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDimensionsText:Ljava/lang/String;

    :cond_0
    iget-object v8, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDimensionsText:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentDimensionsText:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/16 v7, 0x8

    :cond_1
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v7, "source"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mCurrentSourceUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mAttributionClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v7, "contentUri"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->setShareIntent(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mFetchMore:Z

    if-eqz v7, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    sub-int v7, v0, v7

    iget v8, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mFetchMoreThreshold:I

    if-ge v7, v8, :cond_2

    iget-object v7, p0, Lcom/google/android/velvet/gallery/VelvetPhotoViewActivity;->mImageMetadataController:Lcom/google/android/velvet/gallery/ImageMetadataController;

    invoke-virtual {v7}, Lcom/google/android/velvet/gallery/ImageMetadataController;->fetchMoreImages()V

    :cond_2
    return-void
.end method
