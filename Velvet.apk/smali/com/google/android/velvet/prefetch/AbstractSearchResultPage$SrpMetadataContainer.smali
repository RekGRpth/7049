.class Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;
.super Lcom/google/android/searchcommon/util/CachedLater;
.source "AbstractSearchResultPage.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SrpMetadataContainer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/searchcommon/util/CachedLater",
        "<",
        "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
        ">;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/searchcommon/util/CachedLater;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$1;

    invoke-direct {p0}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;)Z
    .locals 3
    .param p1    # Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;

    invoke-virtual {p0}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;->haveNow()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;->getNow()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Velvet.AbstractSearchResultPage"

    const-string v2, "Received multiple different SRP metadata objects."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;->store(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;->consume(Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;)Z

    move-result v0

    return v0
.end method

.method protected create()V
    .locals 0

    return-void
.end method
