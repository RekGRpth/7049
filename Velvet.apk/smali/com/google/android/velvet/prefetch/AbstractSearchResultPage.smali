.class public abstract Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;
.super Ljava/lang/Object;
.source "AbstractSearchResultPage.java"

# interfaces
.implements Lcom/google/android/velvet/prefetch/SearchResultPage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;
    }
.end annotation


# instance fields
.field private final mEventIdConsumer:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/velvet/prefetch/SearchResultPage$SrpMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private final mFetchTimeMillis:J

.field protected final mSrpMetadataConsumer:Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;


# direct methods
.method constructor <init>(J)V
    .locals 2
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$1;-><init>(Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;)V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;->mEventIdConsumer:Lcom/google/android/searchcommon/util/Consumer;

    new-instance v0, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;-><init>(Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;->mSrpMetadataConsumer:Lcom/google/android/velvet/prefetch/AbstractSearchResultPage$SrpMetadataContainer;

    iput-wide p1, p0, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;->mFetchTimeMillis:J

    return-void
.end method


# virtual methods
.method public getFetchTimeMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/velvet/prefetch/AbstractSearchResultPage;->mFetchTimeMillis:J

    return-wide v0
.end method

.method protected maybeLogEventId()V
    .locals 0

    return-void
.end method
