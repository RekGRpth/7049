.class public Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;
.super Lcom/google/android/searchcommon/util/ChunkProducer;
.source "ForwardingChunkProducer.java"


# static fields
.field private static final UNUSABLE_EXECUTOR:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final mCharset:Ljava/nio/charset/Charset;

.field private mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/searchcommon/util/ExecutorServiceAdapter;

    invoke-direct {v0}, Lcom/google/android/searchcommon/util/ExecutorServiceAdapter;-><init>()V

    sput-object v0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->UNUSABLE_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->UNUSABLE_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/google/android/searchcommon/util/ChunkProducer;-><init>(Ljava/util/concurrent/ExecutorService;)V

    invoke-static {p1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mCharset:Ljava/nio/charset/Charset;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    return-void
.end method

.method public markComplete()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;

    new-instance v1, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;

    invoke-direct {v1}, Lcom/google/android/searchcommon/util/ChunkProducer$SentinelChunk;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public offerChunk(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;

    new-instance v1, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mCharset:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/searchcommon/util/ChunkProducer$ByteDataChunk;-><init>([B)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reportError(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;

    new-instance v1, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    check-cast p1, Ljava/io/IOException;

    invoke-direct {v1, p1}, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;-><init>(Ljava/io/IOException;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;

    new-instance v1, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    invoke-direct {v1, v2}, Lcom/google/android/searchcommon/util/ChunkProducer$ExceptionChunk;-><init>(Ljava/io/IOException;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/Consumer;->consume(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected runBufferTask(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public start(Lcom/google/android/searchcommon/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Lcom/google/android/searchcommon/util/ChunkProducer$Chunk;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/Consumer;

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/ForwardingChunkProducer;->mChunkConsumer:Lcom/google/android/searchcommon/util/Consumer;

    return-void
.end method
