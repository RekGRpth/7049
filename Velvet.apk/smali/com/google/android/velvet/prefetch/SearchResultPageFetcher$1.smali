.class Lcom/google/android/velvet/prefetch/SearchResultPageFetcher$1;
.super Ljava/lang/Object;
.source "SearchResultPageFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->fetchThrottled(Lcom/google/android/velvet/prefetch/SearchResultPage;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher$1;->this$0:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher$1;->this$0:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    # getter for: Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v2}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->access$000(Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher$1;->this$0:Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    # getter for: Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;
    invoke-static {v2}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->access$100(Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;)Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->prefetchWaitingPage(J)V

    return-void
.end method
