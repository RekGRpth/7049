.class public Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;
.super Ljava/lang/Object;
.source "SearchResultPageFetcher.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;
.implements Lcom/google/android/velvet/presenter/QueryState$Observer;


# instance fields
.field private final mAlwaysPrefetch:Z

.field private final mBgExecutor:Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

.field private final mCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mQueryState:Lcom/google/android/velvet/presenter/QueryState;

.field private final mRequestIdGenerator:Lcom/google/android/speech/params/RequestIdGenerator;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/velvet/webview/GsaWebViewController;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/prefetch/SearchResultPageCache;Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;Ljava/util/concurrent/Executor;Lcom/google/android/speech/params/RequestIdGenerator;Z)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/SearchSettings;
    .param p2    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p5    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p6    # Lcom/google/android/velvet/presenter/QueryState;
    .param p7    # Lcom/google/android/velvet/prefetch/SearchResultPageCache;
    .param p8    # Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;
    .param p9    # Ljava/util/concurrent/Executor;
    .param p10    # Lcom/google/android/speech/params/RequestIdGenerator;
    .param p11    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    iput-object p1, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    iput-object p3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p5, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p6, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iput-object p8, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mBgExecutor:Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    invoke-static {p9}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mUiExecutor:Ljava/util/concurrent/Executor;

    iput-object p10, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mRequestIdGenerator:Lcom/google/android/speech/params/RequestIdGenerator;

    iput-boolean p11, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mAlwaysPrefetch:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;)Lcom/google/android/velvet/prefetch/SearchResultPageCache;
    .locals 1
    .param p0    # Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    return-object v0
.end method

.method private createPage(Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/velvet/prefetch/HttpHelperResultPage;
    .locals 8
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v2

    new-instance v0, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iget-object v5, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v6, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mUiExecutor:Ljava/util/concurrent/Executor;

    move-object v1, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/prefetch/HttpHelperResultPage;-><init>(Lcom/google/android/velvet/Query;JLcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Ljava/util/concurrent/Executor;Ljava/lang/String;)V

    return-object v0
.end method

.method private fetch(Lcom/google/android/velvet/Query;Z)Lcom/google/android/velvet/prefetch/SearchResultPage;
    .locals 4
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Z

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {p0, p1, v2, v3}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->getCachedResultForQuery(Lcom/google/android/velvet/Query;J)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mRequestIdGenerator:Lcom/google/android/speech/params/RequestIdGenerator;

    invoke-virtual {v2}, Lcom/google/android/speech/params/RequestIdGenerator;->newRequestId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->logTextSearchStart(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->createPage(Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/velvet/prefetch/HttpHelperResultPage;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->isPrefetch()Z

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->fetchThrottled(Lcom/google/android/velvet/prefetch/SearchResultPage;Z)V

    :cond_1
    return-object v0
.end method

.method private fetchSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 4
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestions;

    new-instance v1, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebResult()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionList;)V

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Lcom/google/android/velvet/Query;->fromSuggestionToPrefetch(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)Lcom/google/android/velvet/Query;

    move-result-object v2

    const/4 v0, 0x0

    invoke-direct {p0, v2, v0}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->fetch(Lcom/google/android/velvet/Query;Z)Lcom/google/android/velvet/prefetch/SearchResultPage;

    return-void
.end method

.method private fetchThrottled(Lcom/google/android/velvet/prefetch/SearchResultPage;Z)V
    .locals 8
    .param p1    # Lcom/google/android/velvet/prefetch/SearchResultPage;
    .param p2    # Z

    const-wide/16 v5, -0x1

    iget-object v7, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    if-eqz p2, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v3

    :goto_0
    invoke-virtual {v7, p1, v3, v4}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->setWaitingPage(Lcom/google/android/velvet/prefetch/SearchResultPage;J)J

    move-result-wide v0

    cmp-long v3, v0, v5

    if-nez v3, :cond_1

    :goto_1
    return-void

    :cond_0
    move-wide v3, v5

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher$1;

    invoke-direct {v2, p0}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher$1;-><init>(Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;)V

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mBgExecutor:Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    invoke-virtual {v3, v2}, Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    const-string v3, "Velvet.SearchResultPageFetcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Throttling prefetch: waiting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v3, 0x3e8

    cmp-long v3, v0, v3

    if-lez v3, :cond_3

    const-string v3, "Velvet.SearchResultPageFetcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Large delay ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms). Is this an error?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mBgExecutor:Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;

    invoke-virtual {v3, v2, v0, v1}, Lcom/google/android/searchcommon/util/NamingDelayedTaskExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    goto :goto_1
.end method

.method private usingWebCorpus()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, ""

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/Corpus;->getIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public fetchAsNewTextSearch(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/prefetch/SearchResultPage;
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->fetch(Lcom/google/android/velvet/Query;Z)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v0

    return-object v0
.end method

.method public getCachedResultAndCancelOthers(Lcom/google/android/velvet/Query;J)Lcom/google/android/velvet/prefetch/SearchResultPage;
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->get(Lcom/google/android/velvet/Query;JZ)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v0

    return-object v0
.end method

.method public getCachedResultForQuery(Lcom/google/android/velvet/Query;J)Lcom/google/android/velvet/prefetch/SearchResultPage;
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->get(Lcom/google/android/velvet/Query;JZ)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v0

    return-object v0
.end method

.method public onQueryStateChanged()V
    .locals 8

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v4}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v4}, Lcom/google/android/velvet/webview/GsaWebViewController;->canUseWebView()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->takeQueryToLoadViaGws()Lcom/google/android/velvet/Query;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->getCachedResultAndCancelOthers(Lcom/google/android/velvet/Query;J)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->isVoiceSearch()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "Velvet.SearchResultPageFetcher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Voice search result missing from cache: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->getCommitId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v4, 0x1

    invoke-direct {p0, v1, v4}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->fetch(Lcom/google/android/velvet/Query;Z)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v0

    :cond_1
    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mWebViewController:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-virtual {v4, v1, v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->commitQueryForSingleRequestArchitecture(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/prefetch/SearchResultPage;)V

    invoke-virtual {v1}, Lcom/google/android/velvet/Query;->expectActionFromGws()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-interface {v0, v1, v4}, Lcom/google/android/velvet/prefetch/SearchResultPage;->sendCardsToQueryStateAsync(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/QueryState;)V

    :cond_2
    return-void
.end method

.method public onSuggestionsUpdated(Lcom/google/android/searchcommon/suggest/SuggestionsController;Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 5
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsController;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestions;

    invoke-direct {p0}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->usingWebCorpus()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebResult()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->isEditingQuery()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebResult()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/searchcommon/suggest/Suggestion;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/suggest/Suggestion;->shouldPrefetch()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, v2, p2}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->fetchSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/Suggestions;)V

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->mAlwaysPrefetch:Z

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebResult()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {p2}, Lcom/google/android/searchcommon/suggest/Suggestions;->getWebResult()Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->fetchSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Lcom/google/android/searchcommon/suggest/Suggestions;)V

    goto :goto_0
.end method
