.class Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;
.super Ljava/lang/Object;
.source "AbstractActionCardsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Update"
.end annotation


# instance fields
.field protected final mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;"
        }
    .end annotation
.end field

.field protected final mCreate:Z

.field protected final mIndex:I

.field protected final mView:Landroid/view/View;

.field final synthetic this$1:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;Lcom/google/android/voicesearch/fragments/AbstractCardController;Landroid/view/View;IZ)V
    .locals 1
    .param p3    # Landroid/view/View;
    .param p4    # I
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;",
            "Landroid/view/View;",
            "IZ)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->this$1:Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mView:Landroid/view/View;

    iput p4, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mIndex:I

    iput-boolean p5, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction$Update;->mCreate:Z

    return-void
.end method
