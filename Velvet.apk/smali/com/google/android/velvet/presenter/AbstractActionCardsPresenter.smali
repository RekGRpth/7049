.class public abstract Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;
.super Lcom/google/android/velvet/presenter/MainContentPresenter;
.source "AbstractActionCardsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;
    }
.end annotation


# instance fields
.field private final actionObserver:Landroid/database/DataSetObserver;

.field private final mCardFactory:Lcom/google/android/voicesearch/CardFactory;

.field private mCurrentAction:Lcom/google/android/velvet/presenter/Action;

.field private final mShownControllers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/voicesearch/CardFactory;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p3    # Lcom/google/android/voicesearch/CardFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;",
            "Lcom/google/android/voicesearch/CardFactory;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/presenter/MainContentPresenter;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;)V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;

    new-instance v0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$1;-><init>(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->actionObserver:Landroid/database/DataSetObserver;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCardFactory:Lcom/google/android/voicesearch/CardFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Lcom/google/android/velvet/presenter/Action;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)Lcom/google/android/voicesearch/CardFactory;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCardFactory:Lcom/google/android/voicesearch/CardFactory;

    return-object v0
.end method


# virtual methods
.method protected clearActionOnly()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->actionObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->unregisterObserver(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    invoke-super {p0, p1, p2}, Lcom/google/android/velvet/presenter/MainContentPresenter;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    return-void
.end method

.method protected final getCurrentAction()Lcom/google/android/velvet/presenter/Action;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    return-object v0
.end method

.method onActionChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;-><init>(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;Lcom/google/android/velvet/presenter/Action;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected onBeforeCardsShown(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->onPause()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->cancelCardCountDown()V

    :cond_0
    return-void
.end method

.method public onViewScrolled()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->cancelCardCountDownByUser()V

    :cond_0
    return-void
.end method

.method public onViewsDismissed(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    instance-of v2, v1, Lcom/google/android/voicesearch/fragments/AbstractCardView;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/voicesearch/fragments/AbstractCardView;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->dismissed()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected setAction(Lcom/google/android/velvet/presenter/Action;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/Action;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->actionObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->unregisterObserver(Ljava/lang/Object;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mCurrentAction:Lcom/google/android/velvet/presenter/Action;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->actionObserver:Landroid/database/DataSetObserver;

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/presenter/Action;->registerObserver(Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;

    invoke-direct {v0, p0, p1}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$UpdateCardsTransaction;-><init>(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;Lcom/google/android/velvet/presenter/Action;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->mShownControllers:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    new-instance v0, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter$2;-><init>(Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    goto :goto_0
.end method

.method protected setLastVisibleCard(Landroid/view/View;Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/velvet/presenter/MainContentUi;

    return-void
.end method
