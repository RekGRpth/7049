.class public Lcom/google/android/velvet/presenter/AdClickHandler;
.super Ljava/lang/Object;
.source "AdClickHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/AdClickHandler$Client;
    }
.end annotation


# instance fields
.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private mClickedAd:Lcom/google/android/searchcommon/util/UriRequest;

.field private final mClient:Lcom/google/android/velvet/presenter/AdClickHandler$Client;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/velvet/presenter/AdClickHandler$Client;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p2    # Ljava/util/concurrent/Executor;
    .param p3    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p4    # Lcom/google/android/velvet/presenter/AdClickHandler$Client;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mBgExecutor:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mClient:Lcom/google/android/velvet/presenter/AdClickHandler$Client;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/AdClickHandler;Lcom/google/android/searchcommon/util/UriRequest;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/AdClickHandler;
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/AdClickHandler;->logAdClickAndGetRedirectDestination(Lcom/google/android/searchcommon/util/UriRequest;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/AdClickHandler;Lcom/google/android/searchcommon/util/UriRequest;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/AdClickHandler;
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/presenter/AdClickHandler;->handleRedirect(Lcom/google/android/searchcommon/util/UriRequest;Ljava/lang/String;)V

    return-void
.end method

.method private handleRedirect(Lcom/google/android/searchcommon/util/UriRequest;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mClickedAd:Lcom/google/android/searchcommon/util/UriRequest;

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mClickedAd:Lcom/google/android/searchcommon/util/UriRequest;

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mClient:Lcom/google/android/velvet/presenter/AdClickHandler$Client;

    invoke-interface {v0, p2}, Lcom/google/android/velvet/presenter/AdClickHandler$Client;->onReceivedAdClickRedirect(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mClient:Lcom/google/android/velvet/presenter/AdClickHandler$Client;

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/AdClickHandler$Client;->onAdClickRedirectError()V

    goto :goto_0
.end method

.method private logAdClickAndGetRedirectDestination(Lcom/google/android/searchcommon/util/UriRequest;)V
    .locals 8
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;

    invoke-virtual {p1}, Lcom/google/android/searchcommon/util/UriRequest;->asGetRequest()Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;->setFollowRedirects(Z)V

    const/4 v1, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    const/4 v6, 0x5

    invoke-interface {v5, v4, v6}, Lcom/google/android/searchcommon/util/HttpHelper;->get(Lcom/google/android/searchcommon/util/HttpHelper$GetRequest;I)Ljava/lang/String;

    const-string v5, "Velvet.AdClickHandler"

    const-string v6, "Did not receive a redirect from an ad click!"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    move-object v0, v1

    iget-object v5, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v6, Lcom/google/android/velvet/presenter/AdClickHandler$2;

    invoke-direct {v6, p0, p1, v0}, Lcom/google/android/velvet/presenter/AdClickHandler$2;-><init>(Lcom/google/android/velvet/presenter/AdClickHandler;Lcom/google/android/searchcommon/util/UriRequest;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/google/android/searchcommon/util/HttpHelper$HttpRedirectException;->getRedirectLocation()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v5, "Velvet.AdClickHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Ad click failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mClickedAd:Lcom/google/android/searchcommon/util/UriRequest;

    return-void
.end method

.method public onAdClicked(Lcom/google/android/searchcommon/util/UriRequest;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/util/UriRequest;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->isThisThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mClickedAd:Lcom/google/android/searchcommon/util/UriRequest;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/AdClickHandler;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/velvet/presenter/AdClickHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/velvet/presenter/AdClickHandler$1;-><init>(Lcom/google/android/velvet/presenter/AdClickHandler;Lcom/google/android/searchcommon/util/UriRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
