.class public Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;
.super Lcom/google/android/velvet/presenter/MainContentPresenter;
.source "ConnectionErrorPresenter.java"


# instance fields
.field private mError:Lcom/google/android/velvet/presenter/SearchError;

.field private mQuery:Lcom/google/android/velvet/Query;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)V"
        }
    .end annotation

    const-string v0, "error"

    invoke-direct {p0, v0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->onTryAgainClicked()V

    return-void
.end method

.method private onTryAgainClicked()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/SearchError;->retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V

    :cond_0
    return-void
.end method

.method private prepareErrorCard()Landroid/view/View;
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-virtual {v5, p0, v6}, Lcom/google/android/velvet/VelvetFactory;->createErrorCard(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v5, 0x7f1000a9

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v5, 0x7f1000a8

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    const v5, 0x7f1000aa

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    new-instance v5, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter$1;

    invoke-direct {v5, p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter$1;-><init>(Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    if-nez v5, :cond_0

    const-string v5, ""

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v5, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SearchError;->getErrorMessageResId()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v5, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SearchError;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v5, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SearchError;->getErrorImageResId()I

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SearchError;->getErrorImageResId()I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_2
    iget-object v5, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SearchError;->getButtonTextId()I

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SearchError;->getButtonTextId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v5}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v5

    const-string v6, " Frown Clown "

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    const v5, 0x7f020001

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_3
    const v5, 0x7f020022

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_4
    const v5, 0x7f0d038b

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->getError()Lcom/google/android/velvet/presenter/SearchError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->mError:Lcom/google/android/velvet/presenter/SearchError;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->prepareErrorCard()Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->postAddViews([Landroid/view/View;)V

    return-void
.end method

.method protected onPreDetach()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->postRemoveAllViews()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ConnectionErrorPresenter;->postResetScroll()V

    return-void
.end method
