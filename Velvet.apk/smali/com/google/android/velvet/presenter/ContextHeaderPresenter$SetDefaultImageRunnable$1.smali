.class Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;
.super Ljava/lang/Object;
.source "ContextHeaderPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

.field final synthetic val$defaultImage:Landroid/graphics/drawable/Drawable;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;->this$1:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;->val$defaultImage:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;->this$1:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    # getter for: Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->mCanceled:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->access$700(Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;->this$1:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEnabled:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->access$300(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;->this$1:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->isAttached()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;->this$1:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->this$0:Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUi:Lcom/google/android/velvet/presenter/ContextHeaderUi;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->access$400(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Lcom/google/android/velvet/presenter/ContextHeaderUi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable$1;->val$defaultImage:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/velvet/presenter/ContextHeaderUi;->setContextImageDrawable(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/sidekick/inject/BackgroundImage;)V

    goto :goto_0
.end method
