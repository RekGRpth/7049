.class public Lcom/google/android/velvet/presenter/ContextHeaderPresenter;
.super Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
.source "ContextHeaderPresenter.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/EntryProviderObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

.field private mContextImageHeight:I

.field private mContextImageUri:Landroid/net/Uri;

.field private mContextImageWidth:I

.field private mEnabled:Z

.field private final mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private final mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mResources:Landroid/content/res/Resources;

.field private final mSetDefaultImageRunnable:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

.field private final mUi:Lcom/google/android/velvet/presenter/ContextHeaderUi;

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mUpdateContextImageRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/ContextHeaderUi;Landroid/content/res/Resources;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/util/UriLoader;Lcom/google/android/apps/sidekick/inject/EntryProvider;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/presenter/ContextHeaderUi;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p6    # Lcom/google/android/apps/sidekick/inject/EntryProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/presenter/ContextHeaderUi;",
            "Landroid/content/res/Resources;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Lcom/google/android/apps/sidekick/inject/EntryProvider;",
            ")V"
        }
    .end annotation

    const-string v1, "contextheader"

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$1;-><init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)V

    iput-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUpdateContextImageRunnable:Ljava/lang/Runnable;

    new-instance v1, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;-><init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;Lcom/google/android/velvet/presenter/ContextHeaderPresenter$1;)V

    iput-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mSetDefaultImageRunnable:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUi:Lcom/google/android/velvet/presenter/ContextHeaderUi;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mResources:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;

    iput-object p6, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageWidth:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/lit8 v1, v1, 0x18

    div-int/lit8 v1, v1, 0x64

    iget-object v2, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0c002a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageHeight:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->maybeUpdateContextImage()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mSetDefaultImageRunnable:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEnabled:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Lcom/google/android/velvet/presenter/ContextHeaderUi;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUi:Lcom/google/android/velvet/presenter/ContextHeaderUi;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->getDefaultImage()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ContextHeaderPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method private getDefaultImage()Landroid/graphics/drawable/Drawable;
    .locals 14

    const/16 v9, 0x14

    const/16 v8, 0x8

    const/4 v10, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    const/16 v7, 0xb

    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const v6, 0x7f020014

    const/4 v7, 0x6

    if-lt v5, v7, :cond_1

    if-ge v5, v8, :cond_1

    const v6, 0x7f020011

    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    iget v8, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageWidth:I

    sub-int/2addr v7, v8

    div-int/lit8 v0, v7, 0x2

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    iget v8, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageHeight:I

    sub-int/2addr v7, v8

    div-int/lit8 v1, v7, 0x2

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v8, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mResources:Landroid/content/res/Resources;

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v10, v1}, Ljava/lang/Math;->max(II)I

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    iget v12, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageWidth:I

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v11

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    iget v13, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageHeight:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v12

    invoke-static {v2, v9, v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v7

    :cond_1
    if-lt v5, v8, :cond_2

    if-ge v5, v9, :cond_2

    const v6, 0x7f020012

    goto :goto_0

    :cond_2
    if-lt v5, v9, :cond_0

    const/16 v7, 0x16

    if-ge v5, v7, :cond_0

    const v6, 0x7f020013

    goto :goto_0
.end method

.method private getDefaultImageOnIdle()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mSetDefaultImageRunnable:Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$SetDefaultImageRunnable;->setCanceled(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v1, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$3;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$3;-><init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeOnIdle(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getImageFromEntryResponse()Lcom/google/android/apps/sidekick/inject/BackgroundImage;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->isInitializedFromStorage()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getBackgroundImagePhotos()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getBackgroundImagePhotos()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getBackgroundImagePhotos()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/inject/BackgroundImage;

    goto :goto_0
.end method

.method private maybeUpdateContextImage()V
    .locals 6

    iget-boolean v3, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEnabled:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->isAttached()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->isInitializedFromStorage()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->getImageFromEntryResponse()Lcom/google/android/apps/sidekick/inject/BackgroundImage;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrlType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageWidth:I

    iget v4, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageHeight:I

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/inject/BackgroundImage;->getPhoto()Lcom/google/geo/sidekick/Sidekick$Photo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/sidekick/Sidekick$Photo;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/sidekick/FifeImageUrlUtil;->setImageUrlCenterCrop(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-interface {v3, v2}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->haveNow()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUi:Lcom/google/android/velvet/presenter/ContextHeaderUi;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/Drawable;

    invoke-interface {v4, v3, v0}, Lcom/google/android/velvet/presenter/ContextHeaderUi;->setContextImageDrawable(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/sidekick/inject/BackgroundImage;)V

    :goto_1
    iput-object v2, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageUri:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->getDefaultImageOnIdle()V

    new-instance v3, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter$2;-><init>(Lcom/google/android/velvet/presenter/ContextHeaderPresenter;Lcom/google/android/apps/sidekick/inject/BackgroundImage;)V

    invoke-interface {v1, v3}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageUri:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->getDefaultImageOnIdle()V

    goto :goto_0
.end method


# virtual methods
.method public isAttached()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->isAttached()Z

    move-result v0

    return v0
.end method

.method public onCardListEntriesRefreshed()V
    .locals 0

    return-void
.end method

.method public onEntriesAdded(Lcom/google/geo/sidekick/Sidekick$Interest;Ljava/util/List;)V
    .locals 0
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Interest;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onInvalidated()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUpdateContextImageRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onPostAttach(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUi:Lcom/google/android/velvet/presenter/ContextHeaderUi;

    iget v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mContextImageHeight:I

    invoke-interface {v0, v1}, Lcom/google/android/velvet/presenter/ContextHeaderUi;->setContextHeaderHeight(I)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->maybeUpdateContextImage()V

    return-void
.end method

.method public onPreDetach()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->setEnabled(Z)V

    return-void
.end method

.method public onRefreshed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUpdateContextImageRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEnabled:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEnabled:Z

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0, p0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->registerEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->maybeUpdateContextImage()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v0, p0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->unregisterEntryProviderObserver(Lcom/google/android/apps/sidekick/EntryProviderObserver;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ContextHeaderPresenter;->mUpdateContextImageRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
