.class public interface abstract Lcom/google/android/velvet/presenter/FooterUi;
.super Ljava/lang/Object;
.source "FooterUi.java"


# virtual methods
.method public abstract addCorpusSelector(Lcom/google/android/velvet/Corpus;Z)V
.end method

.method public abstract isCorpusBarLoaded()Z
.end method

.method public abstract removeCorpusSelectors(Lcom/google/android/velvet/Corpus;)V
.end method

.method public abstract resetShowMoreCorpora()V
.end method

.method public abstract setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V
.end method

.method public abstract setShowCorpusBar(Z)V
.end method

.method public abstract setShowTgFooterButton(Z)V
.end method

.method public abstract setTgFooterButtonEnabled(Z)V
.end method

.method public abstract setTgFooterButtonText(Ljava/lang/CharSequence;)V
.end method
