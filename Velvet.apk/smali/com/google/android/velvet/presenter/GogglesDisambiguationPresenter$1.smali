.class final Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$1;
.super Ljava/lang/Object;
.source "GogglesDisambiguationPresenter.java"

# interfaces
.implements Lcom/google/android/velvet/ui/WebImageView$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter;->createDownloadListener(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/velvet/ui/WebImageView$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$sessionId:Ljava/lang/String;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$1;->val$sessionId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$1;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImageDownloaded(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$1;->val$sessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/goggles/TraceTracker;->getSession(Ljava/lang/String;)Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/GogglesDisambiguationPresenter$1;->val$url:Ljava/lang/String;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Lcom/google/android/goggles/TraceTracker$Session;->addClientEventThumbnailGet(Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
