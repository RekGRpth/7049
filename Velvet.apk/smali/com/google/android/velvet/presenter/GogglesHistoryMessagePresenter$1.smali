.class Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter$1;
.super Ljava/lang/Object;
.source "GogglesHistoryMessagePresenter.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;->onPostAttach(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;

.field final synthetic val$msg:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter$1;->this$0:Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter$1;->val$msg:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Ljava/lang/Boolean;)Z
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter$1;->val$msg:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter$1;->consume(Ljava/lang/Boolean;)Z

    move-result v0

    return v0
.end method
