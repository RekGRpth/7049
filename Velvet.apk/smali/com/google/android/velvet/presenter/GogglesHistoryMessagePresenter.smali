.class public Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;
.super Lcom/google/android/velvet/presenter/MainContentPresenter;
.source "GogglesHistoryMessagePresenter.java"


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)V"
        }
    .end annotation

    const-string v0, "goggleshistorymsg"

    invoke-direct {p0, v0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;)V

    return-void
.end method


# virtual methods
.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v4, p0, v5}, Lcom/google/android/velvet/VelvetFactory;->createGogglesHistoryMessageView(Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f100103

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/view/View;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-virtual {p0, v4}, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;->postAddViews([Landroid/view/View;)V

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getGlobalSearchServices()Lcom/google/android/searchcommon/GlobalSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/GlobalSearchServices;->getSearchHistoryHelper()Lcom/google/android/searchcommon/history/SearchHistoryHelper;

    move-result-object v4

    new-instance v5, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter$1;-><init>(Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;Landroid/widget/TextView;)V

    invoke-virtual {v4, v0, v5}, Lcom/google/android/searchcommon/history/SearchHistoryHelper;->getHistoryEnabled(Landroid/accounts/Account;Lcom/google/android/searchcommon/util/Consumer;)V

    :cond_0
    return-void
.end method

.method protected onPreDetach()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/GogglesHistoryMessagePresenter;->postRemoveAllViews()V

    return-void
.end method
