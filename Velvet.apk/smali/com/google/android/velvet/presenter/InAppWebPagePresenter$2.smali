.class Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;
.super Ljava/lang/Object;
.source "InAppWebPagePresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->showWebView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$100(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$600(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->showWebView()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$100(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->hideLoadingIndicator()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$600(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->showWebView()V

    goto :goto_0
.end method
