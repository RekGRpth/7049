.class public Lcom/google/android/velvet/presenter/InAppWebPagePresenter$InAppPageEventListener;
.super Ljava/lang/Object;
.source "InAppWebPagePresenter.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "InAppPageEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$InAppPageEventListener;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public delayedPageLoad()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$InAppPageEventListener;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$300(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->delayedPageLoad()V

    return-void
.end method

.method public pageReady()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$InAppPageEventListener;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # getter for: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$300(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->pageReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$InAppPageEventListener;->this$0:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    # invokes: Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->showWebView()V
    invoke-static {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->access$800(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V

    :cond_0
    return-void
.end method
