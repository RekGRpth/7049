.class Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;
.super Ljava/lang/Object;
.source "InAppWebPagePresenter.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PrintableLog"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private MAX_CAPACITY:I

.field private elementsRemoved:Z

.field private mLog:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x28

    iput v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->MAX_CAPACITY:I

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->mLog:Ljava/util/LinkedList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->elementsRemoved:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->mLog:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->mLog:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->MAX_CAPACITY:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->mLog:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->elementsRemoved:Z

    :cond_0
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->elementsRemoved:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[History log trimmed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->MAX_CAPACITY:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " elements]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/common/collect/Iterators;->forArray([Ljava/lang/Object;)Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->mLog:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/collect/Iterators;->concat(Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;->mLog:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0
.end method
