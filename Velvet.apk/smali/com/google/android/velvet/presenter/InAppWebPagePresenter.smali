.class public Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
.super Ljava/lang/Object;
.source "InAppWebPagePresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/InAppWebPagePresenter$PrintableLog;,
        Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;,
        Lcom/google/android/velvet/presenter/InAppWebPagePresenter$InAppPageEventListener;
    }
.end annotation


# instance fields
.field private final mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

.field private mErrorDumpMessage:Lcom/google/android/searchcommon/util/LazyString;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private mInitialized:Z

.field private final mJavascriptExtensions:Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;

.field private final mLoadingIndicatorTimer:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mShowLoadingDelayMs:I

.field private mTitle:Ljava/lang/String;

.field private final mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mUri:Landroid/net/Uri;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mVisible:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/VelvetFactory;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/ui/InAppWebPageActivity;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/VelvetFactory;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p3    # Lcom/google/android/searchcommon/SearchConfig;
    .param p4    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p5    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p6    # Lcom/google/android/velvet/ui/InAppWebPageActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mInitialized:Z

    new-instance v0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    invoke-direct {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;-><init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mErrorDumpMessage:Lcom/google/android/searchcommon/util/LazyString;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mLoadingIndicatorTimer:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p6, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {p3}, Lcom/google/android/searchcommon/SearchConfig;->getInAppLoadingIndicatorDelayMs()I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mShowLoadingDelayMs:I

    new-instance v0, Lcom/google/android/searchcommon/util/ActivityIntentStarter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/util/ActivityIntentStarter;-><init>(Landroid/app/Activity;I)V

    invoke-static {}, Lcom/google/android/velvet/presenter/JavascriptExtensions;->permissiveTrustPolicy()Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$InAppPageEventListener;

    invoke-direct {v2, p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$InAppPageEventListener;-><init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/velvet/VelvetFactory;->createJavascriptExtensions(Lcom/google/android/searchcommon/util/IntentStarter;Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;)Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mJavascriptExtensions:Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;Lcom/google/android/searchcommon/util/LazyString;)Lcom/google/android/searchcommon/util/LazyString;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    .param p1    # Lcom/google/android/searchcommon/util/LazyString;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mErrorDumpMessage:Lcom/google/android/searchcommon/util/LazyString;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Lcom/google/android/velvet/ui/InAppWebPageActivity;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mLoadingIndicatorTimer:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->showWebView()V

    return-void
.end method

.method private cancelLoadingIndicatorTimer()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mLoadingIndicatorTimer:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mLoadingIndicatorTimer:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-interface {v1, v0}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private initializeLoadingIndicatorTimer()V
    .locals 4

    new-instance v0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$4;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$4;-><init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mLoadingIndicatorTimer:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget v2, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mShowLoadingDelayMs:I

    int-to-long v2, v2

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    :cond_0
    return-void
.end method

.method private showError()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->cancelLoadingIndicatorTimer()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/HttpHelper;->haveNetworkConnection()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0d0392

    :goto_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v2, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$3;

    invoke-direct {v2, p0, v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$3;-><init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;I)V

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    const v0, 0x7f0d0393

    goto :goto_0
.end method

.method private showWebView()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->cancelLoadingIndicatorTimer()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v1, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$2;-><init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    new-instance v0, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;

    invoke-direct {v0, p2, p1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    const-string v1, "InAppWebPagePresenter:"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->println(Ljava/lang/String;)V

    const-string v1, "  "

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->addToPrefix(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requested URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->println(Ljava/lang/String;)V

    const-string v1, "SyncControl log:"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->println(Ljava/lang/String;)V

    const-string v1, "  "

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->addToPrefix(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->dumpEvents(Ljava/io/PrintWriter;)V

    return-void

    :pswitch_0
    const-string v1, "NOTHING_SHOWN"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v1, "WEB_VIEW_SHOWN"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "LOADING_SHOWN"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v1, "ERROR_SHOWN (%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mErrorDumpMessage:Lcom/google/android/searchcommon/util/LazyString;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/util/LinePrefixPrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public pageLoadError(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v0, Lcom/google/android/searchcommon/util/LazyString;

    const-string v1, "pageLoadError %s: %d %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/searchcommon/util/LazyString;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mErrorDumpMessage:Lcom/google/android/searchcommon/util/LazyString;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->showError()V

    return-void
.end method

.method public pageLoadFinished(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->pageLoadFinished(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->showWebView()V

    :cond_0
    return-void
.end method

.method public pageLoadStarted(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->pageLoadStarted(Ljava/lang/String;)V

    return-void
.end method

.method public restoreState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mInitialized:Z

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    if-eqz p1, :cond_0

    const-string v1, "InAppWebPage.WebView"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->initializeLoadingIndicatorTimer()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->restoreWebViewState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mJavascriptExtensions:Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;

    const-string v4, "agsa_ext"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mInitialized:Z

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "InAppWebPage.WebView"

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->getWebViewState()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mTitle:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mVisible:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/net/Uri;)Z
    .locals 7
    .param p1    # Landroid/net/Uri;

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mWebViewSyncControl:Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$WebViewSyncControl;->isPageLoading()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v3, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSecureGoogleUri(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->showError()V

    new-instance v3, Lcom/google/android/searchcommon/util/LazyString;

    const-string v4, "Aborted redirect to %s"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-direct {v3, v4, v5}, Lcom/google/android/searchcommon/util/LazyString;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mErrorDumpMessage:Lcom/google/android/searchcommon/util/LazyString;

    move v1, v2

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "android.intent.category.BROWSABLE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->startActivity(Landroid/content/Intent;)V

    move v1, v2

    goto :goto_0
.end method

.method public start(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mInitialized:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mJavascriptExtensions:Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;

    const-string v2, "agsa_ext"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->initializeLoadingIndicatorTimer()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mActivity:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->loadUri(Landroid/net/Uri;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mInitialized:Z

    :cond_1
    return-void
.end method

.method public tryAgain()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v1, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter$1;-><init>(Lcom/google/android/velvet/presenter/InAppWebPagePresenter;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
