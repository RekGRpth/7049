.class public final Lcom/google/android/velvet/presenter/JavascriptExtensions;
.super Ljava/lang/Object;
.source "JavascriptExtensions.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/AgsaExtJavascriptInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;,
        Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "Velvet.JavascriptExtensions"


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private final mIntentStarter:Lcom/google/android/searchcommon/util/IntentStarter;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPageEventListener:Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mTrustPolicy:Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/IntentStarter;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/IntentStarter;
    .param p3    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p4    # Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;
    .param p5    # Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mApplicationContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mIntentStarter:Lcom/google/android/searchcommon/util/IntentStarter;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mTrustPolicy:Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mPackageManager:Landroid/content/pm/PackageManager;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mPageEventListener:Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

    return-void
.end method

.method private createPackageSpecificIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.category.BROWSABLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static permissiveTrustPolicy()Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;
    .locals 1

    new-instance v0, Lcom/google/android/velvet/presenter/JavascriptExtensions$2;

    invoke-direct {v0}, Lcom/google/android/velvet/presenter/JavascriptExtensions$2;-><init>()V

    return-object v0
.end method

.method public static searchResultsTrustPolicy(Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;
    .locals 1
    .param p0    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    new-instance v0, Lcom/google/android/velvet/presenter/JavascriptExtensions$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/JavascriptExtensions$1;-><init>(Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    return-object v0
.end method


# virtual methods
.method public canUriBeHandledByPackage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mTrustPolicy:Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;

    invoke-interface {v4}, Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;->isTrusted()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/presenter/JavascriptExtensions;->createPackageSpecificIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_1

    const/4 v2, 0x1

    :cond_1
    goto :goto_0

    :catch_0
    move-exception v3

    invoke-static {v3}, Lcom/google/common/base/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v4

    throw v4
.end method

.method public delayedPageLoad()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mPageEventListener:Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mPageEventListener:Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;->delayedPageLoad()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/common/base/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public isTrusted()Z
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mTrustPolicy:Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;

    invoke-interface {v2}, Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;->isTrusted()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/google/common/base/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v2

    throw v2
.end method

.method public openInApp(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const/4 v3, 0x0

    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mTrustPolicy:Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;

    invoke-interface {v4}, Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;->isTrusted()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v4, v2}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSecureGoogleUri(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v4, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mApplicationContext:Landroid/content/Context;

    const-class v5, Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mIntentStarter:Lcom/google/android/searchcommon/util/IntentStarter;

    invoke-interface {v3, v0}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/google/common/base/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v3

    throw v3
.end method

.method public openWithPackage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v3, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mTrustPolicy:Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;

    invoke-interface {v3}, Lcom/google/android/velvet/presenter/JavascriptExtensions$TrustPolicy;->isTrusted()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/presenter/JavascriptExtensions;->createPackageSpecificIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mIntentStarter:Lcom/google/android/searchcommon/util/IntentStarter;

    invoke-interface {v3, v0}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-static {v2}, Lcom/google/common/base/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v3

    throw v3
.end method

.method public pageReady()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mPageEventListener:Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/JavascriptExtensions;->mPageEventListener:Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;

    invoke-interface {v1}, Lcom/google/android/velvet/presenter/JavascriptExtensions$PageEventListener;->pageReady()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/common/base/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public prefetch(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    return-void
.end method
