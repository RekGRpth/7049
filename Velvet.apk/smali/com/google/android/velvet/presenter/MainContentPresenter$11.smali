.class Lcom/google/android/velvet/presenter/MainContentPresenter$11;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "MainContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/MainContentPresenter;->postSetLayoutAnimationsEnabled(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

.field final synthetic val$enabled:Z


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$11;->this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

    iput-boolean p4, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$11;->val$enabled:Z

    invoke-direct {p0, p2, p3}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$11;->val$enabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setLayoutTransitionsEnabled(Z)V

    return-void
.end method
