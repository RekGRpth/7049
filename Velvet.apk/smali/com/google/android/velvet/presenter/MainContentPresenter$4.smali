.class Lcom/google/android/velvet/presenter/MainContentPresenter$4;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "MainContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/MainContentPresenter;->postAddAndScrollToView(Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

.field final synthetic val$lockFooter:Z

.field final synthetic val$offsetFromTop:I

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;IZLandroid/view/View;I)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;
    .param p4    # I

    iput-object p1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;->this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

    iput-boolean p5, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;->val$lockFooter:Z

    iput-object p6, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;->val$view:Landroid/view/View;

    iput p7, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;->val$offsetFromTop:I

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>(Ljava/lang/String;Ljava/lang/Object;I)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;->val$lockFooter:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/android/velvet/presenter/MainContentUi;->setFooterPositionLocked(Z)V

    :cond_0
    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;->val$view:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addView(Landroid/view/View;)V

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;->val$view:Landroid/view/View;

    iget v2, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;->val$offsetFromTop:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->scrollToView(Landroid/view/View;I)V

    return-void
.end method
