.class Lcom/google/android/velvet/presenter/MainContentPresenter$6;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "MainContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/MainContentPresenter;->postRemoveViews([Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

.field final synthetic val$views:[Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;[Landroid/view/View;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$6;->this$0:Lcom/google/android/velvet/presenter/MainContentPresenter;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$6;->val$views:[Landroid/view/View;

    invoke-direct {p0, p2, p3}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter$6;->val$views:[Landroid/view/View;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
