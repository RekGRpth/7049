.class public abstract Lcom/google/android/velvet/presenter/MainContentPresenter;
.super Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
.source "MainContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
    }
.end annotation


# static fields
.field private static final REMOVE_ALL_VIEWS:Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

.field private static final RESET_SCROLL:Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;


# instance fields
.field private final mFragment:Lcom/google/android/velvet/ui/MainContentFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$1;

    const-string v1, "RESET SCROLL"

    invoke-direct {v0, v1}, Lcom/google/android/velvet/presenter/MainContentPresenter$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/velvet/presenter/MainContentPresenter;->RESET_SCROLL:Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$2;

    const-string v1, "REMOVE ALL VIEWS"

    invoke-direct {v0, v1}, Lcom/google/android/velvet/presenter/MainContentPresenter$2;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/velvet/presenter/MainContentPresenter;->REMOVE_ALL_VIEWS:Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/velvet/presenter/MainContentPresenter;->mFragment:Lcom/google/android/velvet/ui/MainContentFragment;

    return-void
.end method


# virtual methods
.method protected getCardContainer()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter;->mFragment:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    return-object v0
.end method

.method protected getRelativeScrollDistanceFromTop(Landroid/view/View;)F
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter;->mFragment:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;->getRelativeScrollDistanceFromTop(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public onStackViewOrderChanged(Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onViewScrolled()V
    .locals 0

    return-void
.end method

.method public onViewsDismissed(Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter;->mFragment:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postAddAndScrollToView(Landroid/view/View;II)V
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$4;

    const-string v2, "addAndScrollToView"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v6, p1

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/MainContentPresenter$4;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;IZLandroid/view/View;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    if-eqz v5, :cond_0

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$5;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$5;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    :cond_0
    return-void

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method protected postAddViews(ILjava/lang/Iterable;)V
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Iterable",
            "<+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$3;

    const-string v2, "addViews"

    move-object v1, p0

    move-object v3, p2

    move v4, p1

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/presenter/MainContentPresenter$3;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;IILjava/lang/Iterable;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected varargs postAddViews(I[Landroid/view/View;)V
    .locals 1
    .param p1    # I
    .param p2    # [Landroid/view/View;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->postAddViews(ILjava/lang/Iterable;)V

    return-void
.end method

.method protected varargs postAddViews([Landroid/view/View;)V
    .locals 1
    .param p1    # [Landroid/view/View;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter;->postAddViews(I[Landroid/view/View;)V

    return-void
.end method

.method protected postImmediate(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter;->mFragment:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;->postImmediate(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postRemoveAllViews()V
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/MainContentPresenter;->REMOVE_ALL_VIEWS:Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected varargs postRemoveViews([Landroid/view/View;)V
    .locals 2
    .param p1    # [Landroid/view/View;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$6;

    const-string v1, "removeViews"

    invoke-direct {v0, p0, v1, p1, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter$6;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;[Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postResetScroll()V
    .locals 1

    sget-object v0, Lcom/google/android/velvet/presenter/MainContentPresenter;->RESET_SCROLL:Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postRestoreSearchPlateStickiness()V
    .locals 1

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$15;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$15;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postScrollToView(Landroid/view/View;I)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$9;

    const-string v2, "scrollToView"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/presenter/MainContentPresenter$9;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;ILandroid/view/View;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postSetLayoutAnimationsEnabled(Z)V
    .locals 3
    .param p1    # Z

    new-instance v1, Lcom/google/android/velvet/presenter/MainContentPresenter$11;

    const-string v2, "setLayoutTransitionsEnabled"

    if-eqz p1, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-direct {v1, p0, v2, v0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter$11;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void

    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method protected postSetLayoutTransitionStartDelay(IJ)V
    .locals 1
    .param p1    # I
    .param p2    # J

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$12;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/velvet/presenter/MainContentPresenter$12;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;IJ)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postSetMatchPortraitMode(Z)V
    .locals 1
    .param p1    # Z

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$13;

    invoke-direct {v0, p0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter$13;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postSetVerticalItemMargin(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$10;

    const-string v1, "setVerticalItemMargin"

    invoke-direct {v0, p0, v1, p1, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter$10;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;II)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postSetVisibility(Landroid/view/View;I)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$8;

    const-string v2, "setVisibility"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/presenter/MainContentPresenter$8;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;Ljava/lang/Object;ILandroid/view/View;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected postSetWhiteBackgroundState(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->postSetWhiteBackgroundState(IZ)V

    return-void
.end method

.method protected postSetWhiteBackgroundState(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$14;

    const-string v1, "setWhiteBackgroundState"

    invoke-direct {v0, p0, v1, p1, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter$14;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;II)V

    if-eqz p2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->postImmediate(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    goto :goto_0
.end method

.method protected postSmoothScrollTo(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Lcom/google/android/velvet/presenter/MainContentPresenter$7;

    const-string v1, "smoothScrollTo"

    invoke-direct {v0, p0, v1, p1, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter$7;-><init>(Lcom/google/android/velvet/presenter/MainContentPresenter;Ljava/lang/String;II)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method protected resetChildDismissState(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter;->mFragment:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->resetChildDismissState(Landroid/view/View;)V

    return-void
.end method

.method protected showToast(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/MainContentPresenter;->mFragment:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;->showToast(I)V

    return-void
.end method
