.class public interface abstract Lcom/google/android/velvet/presenter/MainContentUi;
.super Ljava/lang/Object;
.source "MainContentUi.java"


# virtual methods
.method public abstract getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;
.end method

.method public abstract getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;
.end method

.method public abstract getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;
.end method

.method public abstract setFooterPositionLocked(Z)V
.end method

.method public abstract setMatchPortraitMode(Z)V
.end method

.method public abstract setSearchPlateStuckToScrollingView(Z)V
.end method

.method public abstract setWhiteBackgroundState(I)V
.end method

.method public abstract showViewsPendingLayout(Landroid/view/View;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation
.end method
