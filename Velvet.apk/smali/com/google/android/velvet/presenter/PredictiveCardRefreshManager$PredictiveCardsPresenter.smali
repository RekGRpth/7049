.class public interface abstract Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;
.super Ljava/lang/Object;
.source "PredictiveCardRefreshManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PredictiveCardsPresenter"
.end annotation


# virtual methods
.method public abstract addEntries(Lcom/google/geo/sidekick/Sidekick$Interest;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Interest;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getAppContext()Landroid/content/Context;
.end method

.method public abstract isAttached()Z
.end method

.method public abstract populateView()V
.end method

.method public abstract resetView()V
.end method

.method public abstract showError()V
.end method

.method public abstract showLoading()V
.end method

.method public abstract showSampleCards()V
.end method

.method public abstract startInitialRefresh()V
.end method
