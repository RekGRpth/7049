.class Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PredictiveCardRefreshManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServerErrorReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
    .param p2    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$200(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->isAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.apps.sidekick.REFRESH_SERVER_UNREACHABLE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$200(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->showError()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeoutHandler:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$400(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$RefreshTimeoutHandler;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # invokes: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->cancelRefreshTimeoutHandler()V
    invoke-static {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$600(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # invokes: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->handleRefreshTimeout()V
    invoke-static {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$500(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    goto :goto_0

    :cond_2
    const-string v2, "com.google.android.apps.sidekick.STARTING_REFRESH"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.google.android.apps.sidekick.INTEREST"

    invoke-static {p2, v2}, Lcom/google/android/apps/sidekick/ProtoUtils;->getInterestFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$ServerErrorReceiver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$200(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->showLoading()V

    goto :goto_0
.end method
