.class Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$1;
.super Ljava/lang/Object;
.source "PredictiveCardRefreshManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->onRefreshed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$1;->this$1:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$1;->this$1:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mRefreshTimeOfLastPopulate:J
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$700(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$1;->this$1:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

    iget-object v2, v2, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$800(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getLastRefreshTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$1;->this$1:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mPresenter:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$200(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;->populateView()V

    :cond_0
    return-void
.end method
