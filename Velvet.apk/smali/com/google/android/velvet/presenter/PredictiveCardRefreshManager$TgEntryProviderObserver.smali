.class Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;
.super Ljava/lang/Object;
.source "PredictiveCardRefreshManager.java"

# interfaces
.implements Lcom/google/android/apps/sidekick/EntryProviderObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TgEntryProviderObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
    .param p2    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    return-void
.end method


# virtual methods
.method public onCardListEntriesRefreshed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # invokes: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->cancelRefreshTimeoutHandler()V
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$600(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$900(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$4;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$4;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onEntriesAdded(Lcom/google/geo/sidekick/Sidekick$Interest;Ljava/util/List;)V
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Interest;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # invokes: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->cancelRefreshTimeoutHandler()V
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$600(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$900(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$3;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;Lcom/google/geo/sidekick/Sidekick$Interest;Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onInvalidated()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # invokes: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->cancelRefreshTimeoutHandler()V
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$600(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$900(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$2;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$2;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRefreshed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # invokes: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->cancelRefreshTimeoutHandler()V
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$600(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;->this$0:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    # getter for: Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->access$900(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver$1;-><init>(Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$TgEntryProviderObserver;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
