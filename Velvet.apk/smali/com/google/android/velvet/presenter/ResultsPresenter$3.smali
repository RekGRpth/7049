.class Lcom/google/android/velvet/presenter/ResultsPresenter$3;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "ResultsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/ResultsPresenter;->addWebResultsText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/ResultsPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$3;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$3;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$200(Lcom/google/android/velvet/presenter/ResultsPresenter;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$3;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$3;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$200(Lcom/google/android/velvet/presenter/ResultsPresenter;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    # setter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsTextLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    invoke-static {v1, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$302(Lcom/google/android/velvet/presenter/ResultsPresenter;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter$3;->this$0:Lcom/google/android/velvet/presenter/ResultsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsTextLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->access$300(Lcom/google/android/velvet/presenter/ResultsPresenter;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(I)V

    return-void
.end method
