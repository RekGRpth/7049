.class public Lcom/google/android/velvet/presenter/ResultsPresenter;
.super Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;
.source "ResultsPresenter.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/QueryState$Observer;
.implements Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;,
        Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;
    }
.end annotation


# static fields
.field private static final TTS_CALLBACK:Lcom/google/android/voicesearch/audio/TtsAudioPlayer$Callback;


# instance fields
.field private final mBeamHelper:Lcom/google/android/velvet/util/BeamHelper;

.field private mLastVisibleCard:Landroid/view/View;

.field private final mTtsAudioPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

.field private mWebResultsText:Landroid/view/View;

.field private mWebResultsTextLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

.field private mWebView:Landroid/view/View;

.field private mWebViewLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

.field private mWebViewShown:Z

.field private mWebViewVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/velvet/presenter/ResultsPresenter$1;

    invoke-direct {v0}, Lcom/google/android/velvet/presenter/ResultsPresenter$1;-><init>()V

    sput-object v0, Lcom/google/android/velvet/presenter/ResultsPresenter;->TTS_CALLBACK:Lcom/google/android/voicesearch/audio/TtsAudioPlayer$Callback;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/velvet/util/BeamHelper;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/CardFactory;)V
    .locals 1
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p3    # Lcom/google/android/velvet/util/BeamHelper;
    .param p4    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p5    # Lcom/google/android/voicesearch/CardFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "Lcom/google/android/velvet/util/BeamHelper;",
            "Lcom/google/android/voicesearch/audio/TtsAudioPlayer;",
            "Lcom/google/android/voicesearch/CardFactory;",
            ")V"
        }
    .end annotation

    const-string v0, "results"

    invoke-direct {p0, v0, p1, p5}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/voicesearch/CardFactory;)V

    iput-object p3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mBeamHelper:Lcom/google/android/velvet/util/BeamHelper;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mTtsAudioPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/ResultsPresenter;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ResultsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/ResultsPresenter;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ResultsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsTextLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/velvet/presenter/ResultsPresenter;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/ResultsPresenter;
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsTextLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/velvet/presenter/ResultsPresenter;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ResultsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/presenter/ResultsPresenter;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/ResultsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/velvet/presenter/ResultsPresenter;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/ResultsPresenter;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewVisible:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/velvet/presenter/ResultsPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/ResultsPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->setupWebView()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/velvet/presenter/ResultsPresenter;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/ResultsPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->updateWebViewLayout()V

    return-void
.end method

.method private addWebResultsText()V
    .locals 1

    new-instance v0, Lcom/google/android/velvet/presenter/ResultsPresenter$3;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/ResultsPresenter$3;-><init>(Lcom/google/android/velvet/presenter/ResultsPresenter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postImmediate(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method private areWebResultsMajorityOfView()Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->isOffscreen()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getRelativeScrollDistanceFromTop(Landroid/view/View;)F

    move-result v1

    float-to-double v1, v1

    const-wide/high16 v3, 0x3fe0000000000000L

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getConsumableCardMargin(Landroid/view/View;)I
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v4

    :cond_0
    const v5, 0x7f0c0016

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getViewportHeight()I

    move-result v3

    iget-object v5, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const v5, 0x7f0c0027

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v5

    sub-int v5, v3, v5

    sub-int/2addr v5, v1

    sub-int/2addr v5, v0

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto :goto_0
.end method

.method private postClearAll(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    const-string v1, "Velvet.ResultsPresenter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "postClearAll("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;

    invoke-direct {v0, p0, v4}, Lcom/google/android/velvet/presenter/ResultsPresenter$ClearUiTransaction;-><init>(Lcom/google/android/velvet/presenter/ResultsPresenter;Lcom/google/android/velvet/presenter/ResultsPresenter$1;)V

    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postImmediate(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewShown:Z

    iput-object v4, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mLastVisibleCard:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->clearActionOnly()V

    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    goto :goto_0
.end method

.method private postShowWebView()V
    .locals 2

    const-string v0, "Velvet.ResultsPresenter"

    const-string v1, "postShowWebView()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/ResultsPresenter$ShowWebViewTransaction;-><init>(Lcom/google/android/velvet/presenter/ResultsPresenter;Lcom/google/android/velvet/presenter/ResultsPresenter$1;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewShown:Z

    return-void
.end method

.method private removeWebResultsText()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    new-instance v1, Lcom/google/android/velvet/presenter/ResultsPresenter$4;

    invoke-direct {v1, p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter$4;-><init>(Lcom/google/android/velvet/presenter/ResultsPresenter;Landroid/view/View;)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postImmediate(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method private setupWebView()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSearchController()Lcom/google/android/velvet/presenter/SearchController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->getWebView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    :cond_0
    return-void
.end method

.method private updateWebViewLayout()V
    .locals 6

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewVisible:Z

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getCurrentAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mLastVisibleCard:Landroid/view/View;

    if-eqz v3, :cond_1

    const v3, 0x7f0c0016

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getCurrentAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/Action;->getEffectOnWebResults()Lcom/google/android/voicesearch/EffectOnWebResults;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/voicesearch/EffectOnWebResults;->shouldSuppressWebResults()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mLastVisibleCard:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getConsumableCardMargin(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsTextLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mLastVisibleCard:Landroid/view/View;

    invoke-virtual {v3, v4, v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(Landroid/view/View;II)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v0, v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mLastVisibleCard:Landroid/view/View;

    invoke-virtual {v3, v4, v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(Landroid/view/View;II)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    invoke-virtual {v3, v5}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(I)V

    goto :goto_0
.end method


# virtual methods
.method public createNdefMessage(Lcom/google/android/velvet/util/BeamHelper;)Landroid/nfc/NdefMessage;
    .locals 1
    .param p1    # Lcom/google/android/velvet/util/BeamHelper;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/util/BeamHelper;->createWebSearchMessage(Lcom/google/android/velvet/Query;)Landroid/nfc/NdefMessage;

    move-result-object v0

    return-object v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    invoke-super {p0, p1, p2}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    return-void
.end method

.method protected onBeforeCardsShown(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    return-void
.end method

.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x1

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postSetLayoutTransitionStartDelay(IJ)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postSetVerticalItemMargin(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->shouldCenterResultCardAndMatchPortraitWidthInLandscape()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postSetMatchPortraitMode(Z)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/velvet/VelvetFactory;->createWebResultsText(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    new-instance v1, Lcom/google/android/velvet/presenter/ResultsPresenter$2;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/ResultsPresenter$2;-><init>(Lcom/google/android/velvet/presenter/ResultsPresenter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->addWebResultsText()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mBeamHelper:Lcom/google/android/velvet/util/BeamHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/util/BeamHelper;->registerProvider(Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/QueryState;->addObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    return-void
.end method

.method protected onPreDetach()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/QueryState;->removeObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mBeamHelper:Lcom/google/android/velvet/util/BeamHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/util/BeamHelper;->unregisterProvider(Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->removeWebResultsText()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postSetMatchPortraitMode(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postClearAll(Z)V

    iput-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebView:Landroid/view/View;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsText:Landroid/view/View;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebResultsTextLayoutParams:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    return-void
.end method

.method onPreModeSwitch(Lcom/google/android/velvet/presenter/UiModeManager;Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;Z)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiModeManager;
    .param p2    # Lcom/google/android/velvet/presenter/UiMode;
    .param p3    # Lcom/google/android/velvet/presenter/UiMode;
    .param p4    # Z

    const/4 v1, 0x1

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p3, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->RESULTS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p3, v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postClearAll(Z)V

    :cond_0
    return v1
.end method

.method public onQueryStateChanged()V
    .locals 7

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowCards()Z

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->shouldShowWebView()Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getCurrentAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v6

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewShown:Z

    if-eqz v6, :cond_3

    :cond_0
    const/4 v4, 0x1

    :goto_0
    if-nez v2, :cond_4

    if-nez v3, :cond_4

    if-eqz v4, :cond_1

    invoke-direct {p0, v5}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postClearAll(Z)V

    :cond_1
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->takePlayTtsWithoutCard()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mTtsAudioPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    sget-object v6, Lcom/google/android/velvet/presenter/ResultsPresenter;->TTS_CALLBACK:Lcom/google/android/voicesearch/audio/TtsAudioPlayer$Callback;

    invoke-virtual {v5, v6}, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->requestPlayback(Lcom/google/android/voicesearch/audio/TtsAudioPlayer$Callback;)V

    :cond_2
    return-void

    :cond_3
    move v4, v5

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getCurrentAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v6

    if-eq v0, v6, :cond_5

    if-nez v3, :cond_5

    invoke-direct {p0, v5}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postClearAll(Z)V

    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->setAction(Lcom/google/android/velvet/presenter/Action;)V

    :goto_2
    if-eqz v3, :cond_7

    iget-boolean v5, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewShown:Z

    if-nez v5, :cond_7

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->postShowWebView()V

    goto :goto_1

    :cond_6
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/presenter/ResultsPresenter;->setAction(Lcom/google/android/velvet/presenter/Action;)V

    goto :goto_2

    :cond_7
    if-nez v3, :cond_1

    iget-boolean v5, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewShown:Z

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Unexpected request to hide WebView while showing a card."

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public onViewScrolled()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/AbstractActionCardsPresenter;->onViewScrolled()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->areWebResultsMajorityOfView()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->unsupressCorpora()V

    :cond_0
    return-void
.end method

.method protected setLastVisibleCard(Landroid/view/View;Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/velvet/presenter/MainContentUi;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mLastVisibleCard:Landroid/view/View;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mLastVisibleCard:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/ResultsPresenter;->mWebViewVisible:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/ResultsPresenter;->updateWebViewLayout()V

    :cond_0
    return-void
.end method
