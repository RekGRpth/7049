.class Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/IntentStarter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DynamicIntentStarter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchController;
    .param p2    # Lcom/google/android/velvet/presenter/SearchController$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;-><init>(Lcom/google/android/velvet/presenter/SearchController;)V

    return-void
.end method


# virtual methods
.method public getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # invokes: Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$2700(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/util/IntentStarter;->getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)Z
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # invokes: Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$2500(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivity(Landroid/content/Intent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;)Z
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$DynamicIntentStarter;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # invokes: Lcom/google/android/velvet/presenter/SearchController;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$2600(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getIntentStarter()Lcom/google/android/searchcommon/util/IntentStarter;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/searchcommon/util/IntentStarter;->startActivityForResult(Landroid/content/Intent;Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
