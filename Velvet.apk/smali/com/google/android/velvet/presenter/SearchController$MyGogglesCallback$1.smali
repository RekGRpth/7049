.class Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback$1;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->onError()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback$1;->this$1:Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback$1;->this$1:Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;

    iget-object v0, v0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback$1;->this$1:Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;

    iget-object v1, v1, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/goggles/GogglesError;

    invoke-direct {v2}, Lcom/google/android/goggles/GogglesError;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->cardLoadError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V

    return-void
.end method
