.class Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lcom/google/android/goggles/GogglesController$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyGogglesCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchController;
    .param p2    # Lcom/google/android/velvet/presenter/SearchController$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;-><init>(Lcom/google/android/velvet/presenter/SearchController;)V

    return-void
.end method

.method private logImpressionDisambiguation(Lcom/google/android/goggles/ResultSet;)V
    .locals 7
    .param p1    # Lcom/google/android/goggles/ResultSet;

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/goggles/ResultSet;->getSessionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/goggles/TraceTracker;->getSession(Ljava/lang/String;)Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/goggles/ResultSet;->getResultSetNumber()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/google/android/goggles/TraceTracker$Session;->addImpressionDisambiguation(I)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/goggles/ResultSet;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    new-instance v3, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;

    invoke-direct {v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/android/goggles/ResultSet;->isOutOfScene(I)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;->addResultDispositions(I)Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;

    :cond_0
    invoke-virtual {v2, v3}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;->addResultInfos(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private logImpressionSingleResult(Lcom/google/android/goggles/ResultSet;)V
    .locals 4
    .param p1    # Lcom/google/android/goggles/ResultSet;

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/goggles/ResultSet;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/goggles/TraceTracker;->getSession(Ljava/lang/String;)Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/goggles/ResultSet;->getResultSetNumber()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;

    invoke-virtual {v1}, Lcom/google/bionics/goggles/api2/GogglesProtos$Result;->getResultId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/google/android/goggles/TraceTracker$Session;->addImpressionSingleResult(ILjava/lang/String;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    move-result-object v1

    new-instance v2, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;

    invoke-direct {v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;->addResultInfos(Lcom/google/bionics/goggles/api2/GogglesProtos$ClientResultInfo;)Lcom/google/bionics/goggles/api2/GogglesProtos$Impression;

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$2400(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback$1;-><init>(Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onNoResult()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$2300(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getGogglesQueryImage()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->onGogglesQueryImageReady(Lcom/google/android/velvet/Query;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    sget-object v2, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    return-void
.end method

.method public onResultReady(Lcom/google/android/goggles/ResultSet;)V
    .locals 4
    .param p1    # Lcom/google/android/goggles/ResultSet;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SearchController;->isAttached()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Velvet.SearchController"

    const-string v2, "Results ready, but presenter not attached."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/goggles/ResultSet;->getSceneResults()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->logImpressionSingleResult(Lcom/google/android/goggles/ResultSet;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mSearchPlatePresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$2300(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getGogglesQueryImage()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/presenter/QueryState;->onGogglesQueryImageReady(Lcom/google/android/velvet/Query;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/velvet/presenter/Action;->fromGogglesResultSet(Lcom/google/android/goggles/ResultSet;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyGogglesCallback;->logImpressionDisambiguation(Lcom/google/android/goggles/ResultSet;)V

    goto :goto_1
.end method
