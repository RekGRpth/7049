.class Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyRecognizerControllerListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchController;
    .param p2    # Lcom/google/android/velvet/presenter/SearchController$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;-><init>(Lcom/google/android/velvet/presenter/SearchController;)V

    return-void
.end method


# virtual methods
.method public onDone()V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/String;Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/presenter/VoiceSearchError;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$2200(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAudioStore()Lcom/google/android/speech/audio/AudioStore;

    move-result-object v3

    invoke-direct {v2, p2, v3}, Lcom/google/android/velvet/presenter/VoiceSearchError;-><init>(Lcom/google/android/speech/exception/RecognizeException;Lcom/google/android/speech/audio/AudioStore;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->cardLoadError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V

    return-void
.end method

.method public onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/velvet/ActionServerResult;->listFromMajelResponse(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/velvet/presenter/Action;->fromActionServerResults(Ljava/util/List;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    return-void
.end method

.method public onNoMajelResult()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    sget-object v2, Lcom/google/android/velvet/presenter/Action;->NONE:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    return-void
.end method

.method public onNoMatch(Lcom/google/android/speech/exception/NoMatchRecognizeException;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/exception/NoMatchRecognizeException;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/presenter/VoiceSearchError;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$2200(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAudioStore()Lcom/google/android/speech/audio/AudioStore;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/google/android/velvet/presenter/VoiceSearchError;-><init>(Lcom/google/android/speech/exception/RecognizeException;Lcom/google/android/speech/audio/AudioStore;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->cardLoadError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V

    return-void
.end method

.method public onNoSoundSearchMatch(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/exception/SoundSearchRecognizeException;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/ears/SoundSearchError;

    invoke-direct {v2, p1}, Lcom/google/android/ears/SoundSearchError;-><init>(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->cardLoadError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V

    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 0

    return-void
.end method

.method public onRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/velvet/prefetch/SearchResultPage;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/velvet/prefetch/SearchResultPage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/prefetch/SearchResultPage;",
            ")V"
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mSrpCache:Lcom/google/android/velvet/prefetch/SearchResultPageCache;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$2100(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/prefetch/SearchResultPageCache;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/velvet/prefetch/SearchResultPageCache;->add(Lcom/google/android/velvet/prefetch/SearchResultPage;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/presenter/QueryState;->onTextRecognizedWithProxiedSrp(Lcom/google/android/velvet/Query;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/velvet/prefetch/SearchResultPage;)V

    :cond_1
    return-void
.end method

.method public onSoundSearchError(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/exception/SoundSearchRecognizeException;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/ears/SoundSearchError;

    invoke-direct {v2, p1}, Lcom/google/android/ears/SoundSearchError;-><init>(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->cardLoadError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V

    return-void
.end method

.method public onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V
    .locals 3
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/velvet/presenter/Action;->fromEarsResponse(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)Lcom/google/android/velvet/presenter/Action;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedAction(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/Action;)V

    return-void
.end method

.method public onTtsAvailable()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SearchController$MyRecognizerControllerListener;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCurrentQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SearchController;->access$2000(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->onReceivedTts(Lcom/google/android/velvet/Query;)V

    return-void
.end method
