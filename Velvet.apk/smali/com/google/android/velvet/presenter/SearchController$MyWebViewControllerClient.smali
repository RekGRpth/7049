.class Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;
.super Ljava/lang/Object;
.source "SearchController.java"

# interfaces
.implements Lcom/google/android/velvet/webview/WebViewControllerClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewControllerClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/SearchController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/velvet/presenter/SearchController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchController;
    .param p2    # Lcom/google/android/velvet/presenter/SearchController$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;-><init>(Lcom/google/android/velvet/presenter/SearchController;)V

    return-void
.end method


# virtual methods
.method public commitQueryAndMaybeGetPrefetch(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/prefetch/SearchResultPage;
    .locals 5
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # invokes: Lcom/google/android/velvet/presenter/SearchController;->getSearchResultPageFetcher()Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SearchController;->access$1600(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SearchController;->access$1700(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/searchcommon/SearchConfig;->isNativeIgEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SearchController;->access$1800(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SearchController;->access$1400(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->getCachedResultAndCancelOthers(Lcom/google/android/velvet/Query;J)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SearchController;->access$1800(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/prefetch/SearchResultPageFetcher;->fetchAsNewTextSearch(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v1

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onEndResultsPage()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->resultsPageEnd()V

    return-void
.end method

.method public onLinkClicked(Landroid/net/Uri;)V
    .locals 4
    .param p1    # Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mAdClickHandler:Lcom/google/android/velvet/presenter/AdClickHandler;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$1200(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/AdClickHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/AdClickHandler;->cancel()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$1300(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getAdUriForRedirectHandling(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mAdClickHandler:Lcom/google/android/velvet/presenter/AdClickHandler;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$1200(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/AdClickHandler;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/velvet/presenter/AdClickHandler;->onAdClicked(Lcom/google/android/searchcommon/util/UriRequest;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/QueryState;->onAdClickStart()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$1300(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getResultTargetAndLogUrl(Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SearchController;->access$1400(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v3

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/searchcommon/util/UriRequest;

    invoke-virtual {v3, v2}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logResultClick(Lcom/google/android/searchcommon/util/UriRequest;)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/searchcommon/util/UriRequest;

    # invokes: Lcom/google/android/velvet/presenter/SearchController;->openUrlInBrowser(Lcom/google/android/searchcommon/util/UriRequest;)V
    invoke-static {v3, v2}, Lcom/google/android/velvet/presenter/SearchController;->access$1500(Lcom/google/android/velvet/presenter/SearchController;Lcom/google/android/searchcommon/util/UriRequest;)V

    goto :goto_0
.end method

.method public onLogoutRedirect()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # invokes: Lcom/google/android/velvet/presenter/SearchController;->forceRefreshCookies()V
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$1900(Lcom/google/android/velvet/presenter/SearchController;)V

    return-void
.end method

.method public onNewQuery(Lcom/google/android/velvet/Query;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->fromWebView()Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->newQueryFromWebView(Lcom/google/android/velvet/Query;)V

    return-void
.end method

.method public onPageError(Lcom/google/android/velvet/Query;ILjava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/presenter/WebSearchConnectionError;

    invoke-direct {v1, p2, p3}, Lcom/google/android/velvet/presenter/WebSearchConnectionError;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/velvet/presenter/QueryState;->resultsPageError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V

    return-void
.end method

.method public onShowedPrefetchedSrp(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->isPrefetch()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$1400(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SearchController;->access$1300(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v4

    invoke-virtual {v3, p1, p2, p3, v4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->sendGen204(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/QueryState;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$1800(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->takePumpkinUnloggedEvents()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mCoreServices:Lcom/google/android/searchcommon/CoreSearchServices;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SearchController;->access$1400(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchBoxLogging()Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SearchController;->access$1300(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v4

    invoke-virtual {v2, v1, v3, p3, v4}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->logEventsToGws(ILcom/google/wireless/voicesearch/proto/CardMetdataProtos$LoggingUrls;Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    :cond_1
    return-void
.end method

.method public onStartResultsPage()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->resultsPageStart()V

    return-void
.end method

.method public onStateChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mWebViewActive:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$700(Lcom/google/android/velvet/presenter/SearchController;)Z

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # setter for: Lcom/google/android/velvet/presenter/SearchController;->mWebViewActive:Z
    invoke-static {v0, p1}, Lcom/google/android/velvet/presenter/SearchController;->access$702(Lcom/google/android/velvet/presenter/SearchController;Z)Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchController$MyWebViewControllerClient;->this$0:Lcom/google/android/velvet/presenter/SearchController;

    # getter for: Lcom/google/android/velvet/presenter/SearchController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchController;->access$900(Lcom/google/android/velvet/presenter/SearchController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/QueryState;->webViewReadyToShowChanged(Z)V

    :cond_0
    return-void
.end method
