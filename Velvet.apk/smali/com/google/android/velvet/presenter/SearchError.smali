.class public abstract Lcom/google/android/velvet/presenter/SearchError;
.super Ljava/lang/Object;
.source "SearchError.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getButtonTextId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getErrorImageResId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getErrorMessageResId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;
    .param p2    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1, p2}, Lcom/google/android/velvet/presenter/QueryState;->retry(Lcom/google/android/velvet/Query;)V

    return-void
.end method
