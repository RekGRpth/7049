.class Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;
.super Ljava/lang/Object;
.source "SearchPlatePresenter.java"

# interfaces
.implements Lcom/google/android/voicesearch/ui/RecognizerView$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SearchPlatePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelRecordingClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->goBack()Z

    :cond_0
    return-void
.end method

.method public onStartRecordingClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    # getter for: Lcom/google/android/velvet/presenter/SearchPlatePresenter;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->access$000(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startVoiceSearch()V

    :cond_0
    return-void
.end method

.method public onStopRecordingClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SearchPlatePresenter$1;->this$0:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchPlatePresenter;->onStopListening()V

    return-void
.end method
