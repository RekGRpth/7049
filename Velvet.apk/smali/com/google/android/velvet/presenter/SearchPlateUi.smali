.class public interface abstract Lcom/google/android/velvet/presenter/SearchPlateUi;
.super Ljava/lang/Object;
.source "SearchPlateUi.java"

# interfaces
.implements Lcom/google/android/goggles/GogglesController$GogglesUi;
.implements Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;


# virtual methods
.method public abstract focusSearchBox()V
.end method

.method public abstract getGogglesQueryImage()Landroid/graphics/Bitmap;
.end method

.method public abstract hideFocus()V
.end method

.method public abstract hideKeyboard()V
.end method

.method public abstract hideLogo(Z)V
.end method

.method public abstract hideProgress()V
.end method

.method public abstract restoreInstanceState(Landroid/os/Bundle;)V
.end method

.method public abstract setHintText(I)V
.end method

.method public abstract setQuery(Lcom/google/android/velvet/Query;)V
.end method

.method public abstract setTextQueryCorrections(Landroid/text/Spanned;)V
.end method

.method public abstract showClearButton(Z)V
.end method

.method public abstract showGogglesInput(Z)V
.end method

.method public abstract showGogglesResponse(ZI)V
.end method

.method public abstract showKeyboard()V
.end method

.method public abstract showLogo()V
.end method

.method public abstract showProgress()V
.end method

.method public abstract showSoundSearchInput(Z)V
.end method

.method public abstract showSoundSearchListening()V
.end method

.method public abstract showSpeechInput(Z)V
.end method

.method public abstract showTextInput(Z)V
.end method

.method public abstract showTextQueryMode()V
.end method

.method public abstract showVoiceQueryMode()V
.end method
