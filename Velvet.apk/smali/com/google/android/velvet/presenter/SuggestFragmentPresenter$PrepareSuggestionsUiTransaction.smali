.class Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "SuggestFragmentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrepareSuggestionsUiTransaction"
.end annotation


# instance fields
.field private mRegisteredWithController:Z

.field final synthetic this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .param p2    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;-><init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$400(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    invoke-interface {p1}, Lcom/google/android/velvet/presenter/MainContentUi;->getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$600(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public prepare()Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$400(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v5, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/velvet/VelvetFactory;->createWebSuggestionListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v3

    # setter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v2, v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$402(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/velvet/ui/widget/SuggestionListView;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionsCachingPromoter()Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v3

    # setter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v2, v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$502(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/searchcommon/suggest/CachingPromoter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$500(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetFactory;->createWebPromoter()Lcom/google/android/searchcommon/suggest/Promoter;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->setPromoter(Lcom/google/android/searchcommon/suggest/Promoter;)V

    :goto_0
    return v1

    :cond_0
    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$600(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v5, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/velvet/VelvetFactory;->createSummonsListViewForSuggest(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v3

    # setter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v2, v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$602(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/velvet/ui/widget/SuggestionListView;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$600(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v2

    new-instance v3, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction$1;

    invoke-direct {v3, p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction$1;-><init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;)V

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setFooterClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetFactory;->createSummonsCachingPromoter()Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v3

    # setter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v2, v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$702(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/searchcommon/suggest/CachingPromoter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$700(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetFactory;->createSummonsPromoter()Lcom/google/android/searchcommon/suggest/Promoter;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/suggest/CachingPromoter;->setPromoter(Lcom/google/android/searchcommon/suggest/Promoter;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    iget-object v5, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/velvet/VelvetFactory;->createNoSummonsMessageView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    # setter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v2, v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$802(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Landroid/view/View;)Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->mRegisteredWithController:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->WEB_SUGGESTIONS:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$500(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSearchBoxLogging:Lcom/google/android/searchcommon/google/SearchBoxLogging;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$1000(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/searchcommon/google/SearchBoxLogging;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;
    invoke-static {v5}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$900(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/searchcommon/google/SearchBoxLogging;->captureShownWebSuggestions(Lcom/google/android/searchcommon/suggest/SuggestionsUi;)Lcom/google/android/searchcommon/suggest/SuggestionsUi;

    move-result-object v4

    invoke-interface {v0, v1, v3, v4}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->addSuggestionsView(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)V

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->SUMMONS:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsPromoter:Lcom/google/android/searchcommon/suggest/CachingPromoter;
    invoke-static {v3}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$700(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mUpdateSuggestionsTransaction:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;
    invoke-static {v4}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$900(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    move-result-object v4

    invoke-interface {v0, v1, v3, v4}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->addSuggestionsView(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)V

    iput-boolean v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$PrepareSuggestionsUiTransaction;->mRegisteredWithController:Z

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method
