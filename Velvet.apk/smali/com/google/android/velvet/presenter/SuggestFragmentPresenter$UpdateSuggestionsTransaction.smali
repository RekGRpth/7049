.class Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "SuggestFragmentPresenter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;
.implements Lcom/google/android/searchcommon/suggest/SuggestionsUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateSuggestionsTransaction"
.end annotation


# instance fields
.field private mNumSummons:I

.field private mNumWebSugggestions:I

.field private mPosted:Z

.field private mReportNoSummons:Z

.field private mSummons:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private mSummonsEnabled:Z

.field private mWebEnabled:Z

.field private mWebShowRemoveFromHistoryButtons:Z

.field private mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field final synthetic this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    .param p2    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;-><init>(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->updateHaveNoResults()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->maybePost()V

    return-void
.end method

.method private maybePost()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mPosted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSuggestionsInitPosted:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$1100(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mPosted:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    :cond_0
    return-void
.end method

.method private shouldShowNoResults()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowPredictive:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$1300(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSuggest:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$1400(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$1200(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mReportNoSummons:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateHaveNoResults()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mReportNoSummons:Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$1200(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z

    move-result v1

    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mReportNoSummons:Z

    return-void
.end method

.method private updateHaveNoResults(Lcom/google/android/searchcommon/suggest/SuggestionsController;Ljava/lang/Object;Z)Z
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsController;
    .param p2    # Ljava/lang/Object;
    .param p3    # Z

    invoke-interface {p1, p2}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->getFetchState(Ljava/lang/Object;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return p3

    :pswitch_0
    const/4 p3, 0x1

    goto :goto_0

    :pswitch_1
    const/4 p3, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 8
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    const/4 v7, 0x0

    const/16 v1, 0x8

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mPosted:Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->shouldShowNoResults()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->maybePost()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$400(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mWebSuggestionsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$400(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->WEB_SUGGESTIONS:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mNumWebSugggestions:I

    iget-boolean v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mWebEnabled:Z

    iget-boolean v5, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mWebShowRemoveFromHistoryButtons:Z

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V

    iput-object v7, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mSummons:Lcom/google/android/searchcommon/suggest/SuggestionList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$600(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mSummonsView:Lcom/google/android/velvet/ui/widget/SuggestionListView;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$600(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v0

    sget-object v1, Lcom/google/android/searchcommon/suggest/SuggestionsController;->SUMMONS:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mSummons:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iget v3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mNumSummons:I

    iget-boolean v4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mSummonsEnabled:Z

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V

    iput-object v7, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mSummons:Lcom/google/android/searchcommon/suggest/SuggestionList;

    :cond_3
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->shouldShowNoResults()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mNoResultsView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$800(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onSuggestionsUpdated(Lcom/google/android/searchcommon/suggest/SuggestionsController;Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsController;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->this$0:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->mShowSummons:Z
    invoke-static {v0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;->access$1200(Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsController;->SUMMONS:Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mReportNoSummons:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->updateHaveNoResults(Lcom/google/android/searchcommon/suggest/SuggestionsController;Ljava/lang/Object;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mReportNoSummons:Z

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->maybePost()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p3    # I
    .param p4    # Z
    .param p5    # Z

    sget-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsController;->WEB_SUGGESTIONS:Ljava/lang/Object;

    if-ne p1, v0, :cond_1

    iput-object p2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mWebSuggestions:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput p3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mNumWebSugggestions:I

    iput-boolean p4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mWebEnabled:Z

    iput-boolean p5, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mWebShowRemoveFromHistoryButtons:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/searchcommon/suggest/SuggestionsController;->SUMMONS:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    iput-object p2, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mSummons:Lcom/google/android/searchcommon/suggest/SuggestionList;

    iput p3, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mNumSummons:I

    iput-boolean p4, p0, Lcom/google/android/velvet/presenter/SuggestFragmentPresenter$UpdateSuggestionsTransaction;->mSummonsEnabled:Z

    goto :goto_0
.end method
