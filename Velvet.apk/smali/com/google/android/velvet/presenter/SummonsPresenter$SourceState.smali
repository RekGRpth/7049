.class Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;
.super Ljava/lang/Object;
.source "SummonsPresenter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/searchcommon/suggest/SuggestionsUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/SummonsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SourceState"
.end annotation


# instance fields
.field private mMaxDisplayed:I

.field private final mSuggestionListView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

.field final synthetic this$0:Lcom/google/android/velvet/presenter/SummonsPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/SummonsPresenter;Lcom/google/android/searchcommon/summons/Source;Lcom/google/android/velvet/ui/widget/SuggestionListView;)V
    .locals 3
    .param p2    # Lcom/google/android/searchcommon/summons/Source;
    .param p3    # Lcom/google/android/velvet/ui/widget/SuggestionListView;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->this$0:Lcom/google/android/velvet/presenter/SummonsPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mSuggestionListView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    # getter for: Lcom/google/android/velvet/presenter/SummonsPresenter;->mMaxSuggestionsPerSourceInitial:I
    invoke-static {p1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->access$200(Lcom/google/android/velvet/presenter/SummonsPresenter;)I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mMaxDisplayed:I

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/velvet/VelvetFactory;->createSingleSourcePromoter(Lcom/google/android/searchcommon/summons/Source;)Lcom/google/android/searchcommon/suggest/CachingPromoter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mSuggestionListView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    invoke-interface {p2}, Lcom/google/android/searchcommon/summons/Source;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mSuggestionListView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    invoke-virtual {v1, p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setFooterClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v1

    iget v2, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mMaxDisplayed:I

    invoke-interface {v1, p0, v2}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->setMaxDisplayed(Ljava/lang/Object;I)V

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v1

    invoke-interface {v1, p0, v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->addSuggestionsView(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/CachingPromoter;Lcom/google/android/searchcommon/suggest/SuggestionsUi;)V

    return-void
.end method


# virtual methods
.method dispose()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->this$0:Lcom/google/android/velvet/presenter/SummonsPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->removeSuggestionsView(Ljava/lang/Object;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mMaxDisplayed:I

    iget-object v1, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->this$0:Lcom/google/android/velvet/presenter/SummonsPresenter;

    # getter for: Lcom/google/android/velvet/presenter/SummonsPresenter;->mMaxSuggestionsPerSourceIncrease:I
    invoke-static {v1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->access$300(Lcom/google/android/velvet/presenter/SummonsPresenter;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mMaxDisplayed:I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->this$0:Lcom/google/android/velvet/presenter/SummonsPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mMaxDisplayed:I

    invoke-interface {v0, p0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->setMaxDisplayed(Ljava/lang/Object;I)V

    return-void
.end method

.method public showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V
    .locals 8
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p3    # I
    .param p4    # Z
    .param p5    # Z

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mSuggestionListView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setCountText(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mSuggestionListView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V

    invoke-interface {p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    const/4 v6, 0x1

    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->mSuggestionListView:Lcom/google/android/velvet/ui/widget/SuggestionListView;

    if-eqz v6, :cond_1

    :goto_1
    invoke-virtual {v0, v7}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setFooterVisibility(I)V

    return-void

    :cond_0
    move v6, v7

    goto :goto_0

    :cond_1
    const/16 v7, 0x8

    goto :goto_1
.end method
