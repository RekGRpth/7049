.class public Lcom/google/android/velvet/presenter/SummonsPresenter;
.super Lcom/google/android/velvet/presenter/MainContentPresenter;
.source "SummonsPresenter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;
    }
.end annotation


# instance fields
.field private final mMaxSuggestionsPerSourceIncrease:I

.field private final mMaxSuggestionsPerSourceInitial:I

.field private mNoResultsView:Landroid/view/View;

.field private final mSourceRanker:Lcom/google/android/searchcommon/summons/SourceRanker;

.field private final mSourceStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;",
            ">;"
        }
    .end annotation
.end field

.field private final mSourceViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/ui/widget/SuggestionListView;",
            ">;"
        }
    .end annotation
.end field

.field private mStarted:Z

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/searchcommon/summons/SourceRanker;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;II)V
    .locals 1
    .param p2    # Lcom/google/android/searchcommon/summons/SourceRanker;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;",
            "Lcom/google/android/searchcommon/summons/SourceRanker;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "II)V"
        }
    .end annotation

    const-string v0, "summons"

    invoke-direct {p0, v0, p1}, Lcom/google/android/velvet/presenter/MainContentPresenter;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object p3, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceRanker:Lcom/google/android/searchcommon/summons/SourceRanker;

    iput p4, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mMaxSuggestionsPerSourceInitial:I

    iput p5, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mMaxSuggestionsPerSourceIncrease:I

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceStates:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceViews:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/SummonsPresenter;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/SummonsPresenter;
    .param p1    # Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->createSourceViews(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/presenter/SummonsPresenter;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SummonsPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/SummonsPresenter;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SummonsPresenter;

    iget v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mMaxSuggestionsPerSourceInitial:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/SummonsPresenter;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/SummonsPresenter;

    iget v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mMaxSuggestionsPerSourceIncrease:I

    return v0
.end method

.method private createSourceViews(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/searchcommon/summons/Source;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->isAttached()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mStarted:Z

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/searchcommon/summons/Source;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v3, p0, v4}, Lcom/google/android/velvet/VelvetFactory;->createSummonsListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/widget/SuggestionListView;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceViews:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceStates:Ljava/util/List;

    new-instance v4, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;

    invoke-direct {v4, p0, v1, v2}, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;-><init>(Lcom/google/android/velvet/presenter/SummonsPresenter;Lcom/google/android/searchcommon/summons/Source;Lcom/google/android/velvet/ui/widget/SuggestionListView;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceViews:Ljava/util/List;

    invoke-virtual {p0, v3, v4}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postAddViews(ILjava/lang/Iterable;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postSetWhiteBackgroundState(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postSetVerticalItemMargin(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/velvet/VelvetFactory;->createNoSummonsMessageView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mNoResultsView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mNoResultsView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onPreDetach()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postSetWhiteBackgroundState(IZ)V

    return-void
.end method

.method public onStart()V
    .locals 3

    const/4 v0, 0x1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->onStart()V

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mStarted:Z

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mNoResultsView:Landroid/view/View;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postAddViews([Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->addListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceRanker:Lcom/google/android/searchcommon/summons/SourceRanker;

    new-instance v1, Lcom/google/android/velvet/presenter/SummonsPresenter$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/SummonsPresenter$1;-><init>(Lcom/google/android/velvet/presenter/SummonsPresenter;)V

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/summons/SourceRanker;->getSourcesForUi(Lcom/google/android/searchcommon/util/Consumer;)V

    return-void
.end method

.method public onStop()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->onStop()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mStarted:Z

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSuggestionsController()Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->removeListener(Lcom/google/android/searchcommon/suggest/SuggestionsController$Listener;)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceStates:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/SummonsPresenter$SourceState;->dispose()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceStates:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mSourceViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postRemoveAllViews()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postResetScroll()V

    return-void
.end method

.method public onSuggestionsUpdated(Lcom/google/android/searchcommon/suggest/SuggestionsController;Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionsController;
    .param p2    # Lcom/google/android/searchcommon/suggest/Suggestions;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->getSummonsFetchState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mNoResultsView:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postSetVisibility(Landroid/view/View;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postSetWhiteBackgroundState(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/SummonsPresenter;->mNoResultsView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v2, v2}, Lcom/google/android/velvet/presenter/SummonsPresenter;->postSetWhiteBackgroundState(IZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
