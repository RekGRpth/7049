.class Lcom/google/android/velvet/presenter/TgPresenter$6;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "TgPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/TgPresenter;->toggleBackOfCard(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;

.field final synthetic val$adapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/TgPresenter$6;->val$adapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 25
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->val$adapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-object/from16 v22, v0

    # invokes: Lcom/google/android/velvet/presenter/TgPresenter;->findCardForAdapter(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Landroid/view/View;
    invoke-static/range {v21 .. v22}, Lcom/google/android/velvet/presenter/TgPresenter;->access$800(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    instance-of v0, v4, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    move/from16 v21, v0

    if-eqz v21, :cond_0

    move-object v5, v4

    check-cast v5, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->isExpanded()Z

    move-result v21

    if-eqz v21, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getSettingsViewHeight()I

    move-result v15

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->hideSettingsView()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v14

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lcom/google/android/velvet/presenter/MainContentUi;->setSearchPlateStuckToScrollingView(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v21

    div-int/lit8 v22, v15, 0x2

    sub-int v22, v14, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v23

    const/16 v24, 0x4

    invoke-interface/range {v21 .. v24}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->smoothScrollToYSyncWithTransition(ILandroid/view/ViewGroup;I)V

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v3, v5, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->commitFeedback(Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->val$adapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-object/from16 v23, v0

    check-cast v4, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/velvet/VelvetFactory;->createCardSettingsForAdapter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;)Landroid/view/View;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->showSettingsView(Lcom/google/android/velvet/presenter/TgPresenter;Landroid/view/View;)V

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getTop()I

    move-result v9

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getMeasuredWidth()I

    move-result v8

    if-nez v8, :cond_4

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    :goto_1
    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->measure(II)V

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getMeasuredHeight()I

    move-result v7

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int v6, v9, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    const v22, 0x7f0c00a2

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/velvet/presenter/TgPresenter;->getDimensionPixelSize(I)I

    move-result v18

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getSettingsViewTop()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getViewportHeight()I

    move-result v19

    const/4 v12, -0x1

    mul-int/lit8 v21, v18, 0x2

    sub-int v21, v19, v21

    move/from16 v0, v21

    if-ge v7, v0, :cond_5

    div-int/lit8 v21, v15, 0x2

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v21, v0

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->floor(D)D

    move-result-wide v21

    move-wide/from16 v0, v21

    double-to-int v13, v0

    sub-int v21, v9, v14

    sub-int v11, v21, v18

    if-ge v11, v13, :cond_2

    move v13, v11

    :cond_2
    sub-int v21, v6, v14

    sub-int v21, v21, v19

    add-int v10, v21, v18

    if-le v10, v13, :cond_3

    move v13, v10

    :cond_3
    add-int v12, v14, v13

    :goto_2
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v12, v0, :cond_0

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lcom/google/android/velvet/presenter/MainContentUi;->setSearchPlateStuckToScrollingView(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/presenter/TgPresenter$6;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v22

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v12, v1, v2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->smoothScrollToYSyncWithTransition(ILandroid/view/ViewGroup;I)V

    goto/16 :goto_0

    :cond_4
    const/high16 v21, 0x40000000

    move/from16 v0, v21

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    goto/16 :goto_1

    :cond_5
    mul-int/lit8 v21, v18, 0x2

    sub-int v21, v19, v21

    move/from16 v0, v21

    if-ge v15, v0, :cond_6

    sub-int v21, v6, v19

    add-int v12, v21, v18

    goto :goto_2

    :cond_6
    add-int v21, v9, v16

    sub-int v12, v21, v18

    goto :goto_2
.end method
