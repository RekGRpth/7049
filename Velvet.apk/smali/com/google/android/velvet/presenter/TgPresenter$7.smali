.class Lcom/google/android/velvet/presenter/TgPresenter$7;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "TgPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/TgPresenter;->commitFeedback(Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;

.field final synthetic val$cardContainer:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

.field final synthetic val$entriesToDismiss:Ljava/util/Collection;

.field final synthetic val$entryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/util/Collection;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$7;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/TgPresenter$7;->val$entriesToDismiss:Ljava/util/Collection;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/TgPresenter$7;->val$entryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iput-object p4, p0, Lcom/google/android/velvet/presenter/TgPresenter$7;->val$cardContainer:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 6
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$7;->val$entriesToDismiss:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/sidekick/Sidekick$Entry;

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$7;->val$entryAdapter:Lcom/google/android/apps/sidekick/EntryItemAdapter;

    iget-object v5, p0, Lcom/google/android/velvet/presenter/TgPresenter$7;->val$cardContainer:Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getCardView()Landroid/view/View;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->findViewForChildEntry(Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter$7;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/TgPresenter;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const/high16 v5, 0x7f050000

    invoke-static {v4, v5}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setStartDelay(J)V

    invoke-virtual {v2, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    new-instance v4, Lcom/google/android/velvet/presenter/TgPresenter$7$1;

    invoke-direct {v4, p0, v1}, Lcom/google/android/velvet/presenter/TgPresenter$7$1;-><init>(Lcom/google/android/velvet/presenter/TgPresenter$7;Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_1
    return-void
.end method
