.class Lcom/google/android/velvet/presenter/TgPresenter$8;
.super Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
.source "TgPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/TgPresenter;->onBackPressed()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$8;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public commit(Lcom/google/android/velvet/presenter/MainContentUi;)V
    .locals 16
    .param p1    # Lcom/google/android/velvet/presenter/MainContentUi;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/velvet/presenter/TgPresenter$8;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v12}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v12

    add-int/lit8 v7, v12, -0x1

    :goto_0
    if-ltz v7, :cond_2

    invoke-virtual {v3, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v12, v4, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    if-eqz v12, :cond_1

    move-object v5, v4

    check-cast v5, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->isExpanded()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v2, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getSettingsViewHeight()I

    move-result v12

    add-int/2addr v11, v12

    invoke-virtual {v5}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->hideSettingsView()V

    :cond_1
    add-int/lit8 v7, v7, -0x1

    goto :goto_0

    :cond_2
    if-lez v11, :cond_3

    move v10, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/velvet/presenter/TgPresenter$8;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v12}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v12

    invoke-interface {v12}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v9

    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Lcom/google/android/velvet/presenter/MainContentUi;->setSearchPlateStuckToScrollingView(Z)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/velvet/presenter/TgPresenter$8;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v12}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v12

    div-int/lit8 v13, v10, 0x2

    sub-int v13, v9, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/presenter/TgPresenter$8;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-virtual {v14}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v14

    const/4 v15, 0x4

    invoke-interface {v12, v13, v14, v15}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->smoothScrollToYSyncWithTransition(ILandroid/view/ViewGroup;I)V

    :cond_3
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/presenter/TgPresenter$8;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    const/4 v15, 0x1

    invoke-virtual {v14, v12, v13, v15}, Lcom/google/android/velvet/presenter/TgPresenter;->commitFeedback(Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;Z)V

    goto :goto_1

    :cond_4
    return-void
.end method
