.class Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;
.super Ljava/lang/Object;
.source "TgPresenter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/TgPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CardListClickListener"
.end annotation


# instance fields
.field private final mEntryType:I

.field final synthetic this$0:Lcom/google/android/velvet/presenter/TgPresenter;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;->mEntryType:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p2    # I
    .param p3    # Lcom/google/android/velvet/presenter/TgPresenter$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;->this$0:Lcom/google/android/velvet/presenter/TgPresenter;

    iget v1, p0, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;->mEntryType:I

    # invokes: Lcom/google/android/velvet/presenter/TgPresenter;->showCardFromCardList(I)V
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->access$200(Lcom/google/android/velvet/presenter/TgPresenter;I)V

    return-void
.end method
