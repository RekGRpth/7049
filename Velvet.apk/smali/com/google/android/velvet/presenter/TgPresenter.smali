.class public abstract Lcom/google/android/velvet/presenter/TgPresenter;
.super Lcom/google/android/velvet/presenter/MainContentPresenter;
.source "TgPresenter.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager$PredictiveCardsPresenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;,
        Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;
    }
.end annotation


# static fields
.field static final STALE_THRESHOLD_MILLIS:J = 0xdbba0L


# instance fields
.field private final mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

.field private mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mDisableAnimationsOnNextPopulate:Z

.field private final mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mLoadingCard:Landroid/view/View;

.field private mMode:I

.field private final mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mPendingCardsState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private final mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

.field private final mScrollListener:Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

.field private mScrollPosAfterNextPopulate:I

.field private mShowLoadingCard:Ljava/lang/Runnable;

.field private final mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

.field private final mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/google/UserInteractionLogger;Lcom/google/android/velvet/ui/MainContentFragment;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/velvet/presenter/ViewActionRecorder;Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p3    # Lcom/google/android/searchcommon/google/UserInteractionLogger;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .param p6    # Lcom/google/android/velvet/presenter/ViewActionRecorder;
    .param p7    # Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
    .param p8    # Lcom/google/android/apps/sidekick/inject/NetworkClient;
    .param p9    # Lcom/google/android/apps/sidekick/inject/ActivityHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "Lcom/google/android/searchcommon/google/UserInteractionLogger;",
            "Lcom/google/android/velvet/ui/MainContentFragment",
            "<*>;",
            "Lcom/google/android/searchcommon/util/Clock;",
            "Lcom/google/android/velvet/presenter/ViewActionRecorder;",
            "Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;",
            "Lcom/google/android/apps/sidekick/inject/NetworkClient;",
            "Lcom/google/android/apps/sidekick/inject/ActivityHelper;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p4}, Lcom/google/android/velvet/presenter/MainContentPresenter;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mDisableAnimationsOnNextPopulate:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mScrollPosAfterNextPopulate:I

    new-instance v0, Lcom/google/android/velvet/presenter/TgPresenter$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/TgPresenter$1;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mScrollListener:Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    new-instance v0, Lcom/google/android/velvet/presenter/TgPresenter$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/TgPresenter$2;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    iput-object p2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p3, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    iput-object p6, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;

    iput-object p5, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p7, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    iput-object p8, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iput-object p9, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/ViewActionRecorder;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/TgPresenter;I)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->showCardFromCardList(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/presenter/TgPresenter;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/velvet/presenter/TgPresenter;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/CardDismissalHandler;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/velvet/presenter/TgPresenter;)Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->findCardForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/presenter/TgPresenter;
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->findCardForAdapter(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private addCardListType(Lcom/google/android/velvet/ui/CardListView;Lcom/google/android/apps/sidekick/EntryItemAdapter;Z)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/ui/CardListView;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;
    .param p3    # Z

    const v3, 0x7f020097

    const/4 v4, 0x0

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const v1, 0x7f020091

    new-instance v2, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/16 v3, 0xe

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_2
    new-instance v1, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/16 v2, 0x11

    invoke-direct {v1, p0, v2, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v3, v1}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_3
    new-instance v1, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/16 v2, 0x10

    invoke-direct {v1, p0, v2, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v3, v1}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_4
    new-instance v1, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/16 v2, 0xf

    invoke-direct {v1, p0, v2, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v3, v1}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_5
    const v1, 0x7f020092

    new-instance v2, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/16 v3, 0xc

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_6
    const v1, 0x7f020096

    new-instance v2, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_7
    const v1, 0x7f020095

    new-instance v2, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/16 v3, 0xd

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_8
    const v1, 0x7f020094

    new-instance v2, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/16 v3, 0x9

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_9
    const v1, 0x7f020098

    new-instance v2, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/4 v3, 0x7

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_a
    const v1, 0x7f020093

    new-instance v2, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;

    const/16 v3, 0x12

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/velvet/presenter/TgPresenter$CardListClickListener;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;ILcom/google/android/velvet/presenter/TgPresenter$1;)V

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/google/android/velvet/ui/CardListView;->addCardType(Lcom/google/android/apps/sidekick/EntryItemAdapter;ZILandroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_a
    .end packed-switch
.end method

.method private addViewActionListeners()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mScrollListener:Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    invoke-interface {v0, v1}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->addScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method private buildPredictiveView()V
    .locals 3

    const/4 v2, 0x3

    const/4 v1, 0x2

    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    if-ne v0, v2, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->registerPredictiveCardsListeners()V

    :goto_0
    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->buildView()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->unregisterPredictiveCardsListeners()V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->showSampleCards()V

    goto :goto_1
.end method

.method private commitAllFeedbackWithoutAnimations()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    add-int/lit8 v4, v5, -0x1

    :goto_0
    if-ltz v4, :cond_1

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v5, v2, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    if-eqz v5, :cond_0

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->isExpanded()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {p0, v0, v3, v5}, Lcom/google/android/velvet/presenter/TgPresenter;->commitFeedback(Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;Z)V

    :cond_0
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private findCardForAdapter(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Landroid/view/View;
    .locals 6
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f100011

    invoke-virtual {v4, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v0, v4

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private findCardForEntry(Lcom/google/geo/sidekick/Sidekick$Entry;)Landroid/view/View;
    .locals 11
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Entry;

    new-instance v7, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v7, p1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v4

    const/4 v3, 0x0

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    const v9, 0x7f100011

    invoke-virtual {v8, v9}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/sidekick/ProtoKey;

    invoke-direct {v2, v1}, Lcom/google/android/apps/sidekick/ProtoKey;-><init>(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {v7, v2}, Lcom/google/android/apps/sidekick/ProtoKey;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    :goto_1
    return-object v8

    :cond_0
    invoke-virtual {p1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v9

    invoke-virtual {v1}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v10

    if-ne v9, v10, :cond_1

    if-nez v3, :cond_1

    move-object v3, v8

    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_2
    move-object v8, v3

    goto :goto_1
.end method

.method private findStackForType(Ljava/util/List;I)Lcom/google/android/apps/sidekick/EntryItemStack;
    .locals 3
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;I)",
            "Lcom/google/android/apps/sidekick/EntryItemStack;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntryType()I

    move-result v2

    if-ne v2, p2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getTargetEntry()Lcom/google/geo/sidekick/Sidekick$Entry;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "target_entry"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "target_entry"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/sidekick/ProtoUtils;->getEntryFromByteArray([B)Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private postAddPredictiveCards(Ljava/util/List;ZLcom/google/geo/sidekick/Sidekick$Entry;I)V
    .locals 8
    .param p2    # Z
    .param p3    # Lcom/google/geo/sidekick/Sidekick$Entry;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;Z",
            "Lcom/google/geo/sidekick/Sidekick$Entry;",
            "I)V"
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mPendingCardsState:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mPendingCardsState:Ljava/util/Map;

    iget-object v5, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    new-instance v0, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/velvet/presenter/TgPresenter$AddPredictiveCardsTransaction;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/util/List;ZLjava/util/Map;Landroid/view/View;Lcom/google/geo/sidekick/Sidekick$Entry;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method private removeLoadingCard()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->removeLoadingCard(Z)V

    return-void
.end method

.method private removeLoadingCard(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->postSetVisibility(Landroid/view/View;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRemoveViews([Landroid/view/View;)V

    iput-object v3, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    goto :goto_0
.end method

.method private removeViewActionListeners()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mScrollListener:Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    invoke-interface {v0, v1}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->removeScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-void
.end method

.method private savePredictiveCardsState(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    add-int/lit8 v5, v8, -0x1

    :goto_0
    if-ltz v5, :cond_4

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v8, 0x7f100013

    invoke-virtual {v3, v8}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v3, v7}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v8

    if-lez v8, :cond_0

    const-string v8, "card:views"

    invoke-virtual {v0, v8, v7}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_1

    const-string v8, "card:focusedViewId"

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    instance-of v8, v3, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    if-eqz v8, :cond_2

    check-cast v3, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->isExpanded()Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "card_expanded"

    const/4 v9, 0x1

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v5, v5, -0x1

    goto :goto_0

    :cond_4
    const-string v8, "card_state_map"

    invoke-virtual {p1, v8, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method

.method private showCardFromCardList(I)V
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getCardListEntries()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->findStackForType(Ljava/util/List;I)Lcom/google/android/apps/sidekick/EntryItemStack;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntries()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntries()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/google/android/velvet/presenter/VelvetPresenter;->showCardInDialog(Lcom/google/geo/sidekick/Sidekick$Entry;Z)V

    goto :goto_0
.end method

.method private showSampleCards(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v8

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->removeLoadingCard()V

    invoke-virtual {p0, v10}, Lcom/google/android/velvet/presenter/TgPresenter;->postSmoothScrollTo(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v9

    invoke-virtual {v8, p0, v9}, Lcom/google/android/velvet/VelvetFactory;->createCardListView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/CardListView;

    move-result-object v0

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/sidekick/EntryItemStack;

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/EntryItemStack;->getEntries()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-interface {v3}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/geo/sidekick/Sidekick$Entry;->getType()I

    move-result v2

    const/4 v8, -0x1

    if-eq v2, v8, :cond_1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v7}, Lcom/google/android/apps/sidekick/EntryItemStack;->containsRealEntries()Z

    move-result v4

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/velvet/presenter/TgPresenter;->addCardListType(Lcom/google/android/velvet/ui/CardListView;Lcom/google/android/apps/sidekick/EntryItemAdapter;Z)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/velvet/ui/CardListView;->addOtherCardsRow()V

    const/4 v8, 0x1

    new-array v8, v8, [Landroid/view/View;

    aput-object v0, v8, v10

    invoke-virtual {p0, v8}, Lcom/google/android/velvet/presenter/TgPresenter;->postAddViews([Landroid/view/View;)V

    goto :goto_0
.end method

.method private updateTgFooterButtonText()V
    .locals 11

    const/4 v10, 0x1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getLastRefreshTimeMillis()J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v4}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v4

    const v5, 0x7f0d01e0

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/presenter/TgPresenter;->getString(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setTgFooterButtonText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setTgFooterButtonEnabled(Z)V

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v4

    const v5, 0x7f0d00d7

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/presenter/TgPresenter;->getString(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setTgFooterButtonText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    sub-long v4, v2, v0

    const-wide/32 v8, 0xdbba0

    cmp-long v4, v4, v8

    if-lez v4, :cond_3

    const-wide/32 v4, 0xea60

    const/high16 v6, 0x80000

    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v4

    const v5, 0x7f0d01e1

    new-array v6, v10, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/google/android/velvet/presenter/TgPresenter;->getString(I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setTgFooterButtonText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->entriesIncludeMore()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v4

    const v5, 0x7f0d00d9

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/presenter/TgPresenter;->getString(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setTgFooterButtonText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v4

    const v5, 0x7f0d0133

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/presenter/TgPresenter;->getString(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setTgFooterButtonText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public addEntries(Lcom/google/geo/sidekick/Sidekick$Interest;Ljava/util/List;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/geo/sidekick/Sidekick$Interest;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/sidekick/EntryItemStack;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_2

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->removeLoadingCard(Z)V

    if-nez p1, :cond_1

    const/4 v0, 0x0

    const/4 v2, -0x1

    invoke-direct {p0, p2, v1, v0, v2}, Lcom/google/android/velvet/presenter/TgPresenter;->postAddPredictiveCards(Ljava/util/List;ZLcom/google/geo/sidekick/Sidekick$Entry;I)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->updateTgFooterButtonText()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v1, "CARD_RENDER"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnStacks(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public commitFeedback(Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;Z)V
    .locals 4
    .param p1    # Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;
    .param p2    # Lcom/google/android/apps/sidekick/PredictiveCardContainer;
    .param p3    # Z

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;->commitFeedback(Landroid/content/Context;)V

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;->getEntryAdapter()Lcom/google/android/apps/sidekick/EntryItemAdapter;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;->dismissCardAfterCommit()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v2

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->handleDismissedEntries(Ljava/util/Collection;)V

    if-eqz p3, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->handleEntryDelete(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;->getChildEntriesToDismissAfterCommit()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->removeGroupChildEntries(Lcom/google/geo/sidekick/Sidekick$Entry;Ljava/util/Collection;)V

    if-eqz p3, :cond_0

    new-instance v2, Lcom/google/android/velvet/presenter/TgPresenter$7;

    invoke-direct {v2, p0, v0, v1, p2}, Lcom/google/android/velvet/presenter/TgPresenter$7;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/util/Collection;Lcom/google/android/apps/sidekick/EntryItemAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;)V

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/presenter/TgPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    goto :goto_0
.end method

.method public doIncrementalUserRefreshForMore(Lcom/google/geo/sidekick/Sidekick$Interest;)V
    .locals 2
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->showLoadingCard()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0, v1, v1, v1, p1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->refreshCards(ZZZLcom/google/geo/sidekick/Sidekick$Interest;)V

    return-void
.end method

.method public doUserRefresh(Lcom/google/geo/sidekick/Sidekick$Interest;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->commitAllFeedbackWithoutAnimations()V

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/presenter/TgPresenter;->postSmoothScrollTo(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRemovePredictiveCards()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->showLoadingCard()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2, p1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->refreshCards(ZZZLcom/google/geo/sidekick/Sidekick$Interest;)V

    return-void
.end method

.method public doUserRefreshForMore(Lcom/google/geo/sidekick/Sidekick$Interest;)V
    .locals 3
    .param p1    # Lcom/google/geo/sidekick/Sidekick$Interest;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->commitAllFeedbackWithoutAnimations()V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->postSmoothScrollTo(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRemovePredictiveCards()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->showLoadingCard()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0, v2, v2, v1, p1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->refreshCards(ZZZLcom/google/geo/sidekick/Sidekick$Interest;)V

    return-void
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public handleEntryDelete(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->findCardForAdapter(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/velvet/presenter/TgPresenter$5;

    const-string v2, "placeDelete"

    invoke-direct {v1, p0, v2, p1, v0}, Lcom/google/android/velvet/presenter/TgPresenter$5;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/lang/String;Ljava/lang/Object;Landroid/view/View;)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    goto :goto_0
.end method

.method public handlePlaceEdit(Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->findCardForAdapter(Lcom/google/android/apps/sidekick/EntryItemAdapter;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/sidekick/GenericPlaceEntryAdapter;->handlePlaceEdit(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected isNonPredictiveCardView(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onBackPressed()Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_4

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v4, v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    if-eqz v4, :cond_3

    check-cast v0, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_1

    new-instance v1, Lcom/google/android/velvet/presenter/TgPresenter$8;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/TgPresenter$8;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRestoreSearchPlateStickiness()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v1

    const-string v2, "BACK_BUTTON_CLOSE_FEEDBACK"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->commitAllFeedbackWithoutAnimations()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->recordViewEndTimes()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->removeViewActionListeners()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->unregisterPredictiveCardsListeners()V

    return-void
.end method

.method protected onPostAttach(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    const-string v2, "predictive_mode"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mDisableAnimationsOnNextPopulate:Z

    const-string v1, "scroll_pos"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mScrollPosAfterNextPopulate:I

    const-string v1, "card_state_map"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    iput-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mPendingCardsState:Ljava/util/Map;

    :cond_1
    new-instance v1, Lcom/google/android/velvet/presenter/CardDismissalHandler;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/presenter/CardDismissalHandler;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;)V

    iput-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->resetTimeOfLastPopulate()V

    return-void
.end method

.method protected onPreDetach()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->onResume()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->buildPredictiveView()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isPredictiveOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mViewActionRecorder:Lcom/google/android/velvet/presenter/ViewActionRecorder;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/ViewActionRecorder;->recordViewStartTimes()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->addViewActionListeners()V

    return-void
.end method

.method public onStackViewOrderChanged(Ljava/lang/Iterable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    instance-of v4, v3, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    if-eqz v4, :cond_0

    move-object v1, v3

    check-cast v1, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->isExpanded()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getBackOfCardAdapter()Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->hideSettingsView()V

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v1, v4}, Lcom/google/android/velvet/presenter/TgPresenter;->commitFeedback(Lcom/google/android/apps/sidekick/feedback/BackOfCardAdapter;Lcom/google/android/apps/sidekick/PredictiveCardContainer;Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onTgFooterButtonClicked()V
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getLastRefreshTimeMillis()J

    move-result-wide v0

    iget-object v5, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v5}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v2

    iget v5, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/VelvetPresenter;->goBack()Z

    const-string v4, "HIDE_CARD_LIST"

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/presenter/VelvetPresenter;->setTgFooterButtonEnabled(Z)V

    iget-object v5, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v6, "BUTTON_PRESS"

    invoke-virtual {v5, v6, v4}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-nez v5, :cond_2

    invoke-virtual {p0, v9}, Lcom/google/android/velvet/presenter/TgPresenter;->doUserRefresh(Lcom/google/geo/sidekick/Sidekick$Interest;)V

    const-string v4, "NO_CARDS_REFRESH"

    goto :goto_1

    :cond_2
    sub-long v5, v2, v0

    const-wide/32 v7, 0xdbba0

    cmp-long v5, v5, v7

    if-lez v5, :cond_3

    invoke-virtual {p0, v9}, Lcom/google/android/velvet/presenter/TgPresenter;->doUserRefresh(Lcom/google/geo/sidekick/Sidekick$Interest;)V

    const-string v4, "STALE_CARDS_REFRESH"

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->entriesIncludeMore()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/VelvetPresenter;->showTheGoogleCardList()V

    const-string v4, "SHOW_CARD_LIST"

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v9}, Lcom/google/android/velvet/presenter/TgPresenter;->doIncrementalUserRefreshForMore(Lcom/google/geo/sidekick/Sidekick$Interest;)V

    const-string v4, "SHOW_MORE_CARDS"

    goto :goto_1
.end method

.method public onViewsDismissed(Ljava/lang/Iterable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    iget-object v6, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v7}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->newBuilder(Lcom/google/android/apps/sidekick/inject/NetworkClient;Lcom/google/android/apps/sidekick/inject/EntryProvider;)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v7}, Lcom/google/android/searchcommon/util/Clock;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->setTimestamp(J)Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;

    move-result-object v4

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const v6, 0x7f100011

    invoke-virtual {v5, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/sidekick/EntryItemAdapter;

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v0, v6, v4}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->prepareDismissTask(Landroid/content/Context;Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;)V

    iget-object v6, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v7, "DISMISS_CARD"

    invoke-virtual {v6, v7, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnEntryAdapter(Ljava/lang/String;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask$Builder;->build()Lcom/google/android/apps/sidekick/actions/DismissEntryTask;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->hasActions()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Void;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/sidekick/actions/DismissEntryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    iget-object v6, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v6}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v6

    invoke-interface {v6, v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->handleDismissedEntries(Ljava/util/Collection;)V

    return-void
.end method

.method public populateView()V
    .locals 12

    const/4 v11, 0x0

    const/4 v10, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isMarinerEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mDisableAnimationsOnNextPopulate:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0, v8}, Lcom/google/android/velvet/presenter/TgPresenter;->postSetLayoutAnimationsEnabled(Z)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->removeLoadingCard()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRemovePredictiveCards()V

    invoke-virtual {p0, v8}, Lcom/google/android/velvet/presenter/TgPresenter;->postSmoothScrollTo(I)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    sget-object v3, Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;->PROMO_CARD:Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->shouldShowFirstUseCard(Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-array v2, v9, [Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/sidekick/PromoCardAdapter;

    iget-object v5, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v5}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    iget-object v7, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v4, p0, v5, v6, v7}, Lcom/google/android/apps/sidekick/PromoCardAdapter;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/velvet/presenter/CardDismissalHandler;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getCardContainer()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v3, p0, v4, v11, v5}, Lcom/google/android/velvet/VelvetFactory;->createPredictiveCardForAdapter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;Ljava/lang/String;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/presenter/TgPresenter;->postAddViews([Landroid/view/View;)V

    :cond_3
    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    sget-object v3, Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;->SWIPE_TUTORIAL:Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/presenter/CardDismissalHandler;->shouldShowFirstUseCard(Lcom/google/android/velvet/presenter/CardDismissalHandler$FirstUseCardType;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v1, Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mCardDismissalHandler:Lcom/google/android/velvet/presenter/CardDismissalHandler;

    iget-object v3, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v3}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mActivityHelper:Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/sidekick/SwipeTutorialCardAdapter;-><init>(Lcom/google/android/velvet/presenter/CardDismissalHandler;Lcom/google/android/apps/sidekick/inject/EntryProvider;Lcom/google/android/apps/sidekick/inject/ActivityHelper;)V

    new-array v2, v9, [Lcom/google/android/apps/sidekick/EntryItemStack;

    new-instance v3, Lcom/google/android/apps/sidekick/EntryItemStack;

    new-array v4, v9, [Lcom/google/android/apps/sidekick/EntryItemAdapter;

    aput-object v1, v4, v8

    invoke-direct {v3, v4}, Lcom/google/android/apps/sidekick/EntryItemStack;-><init>([Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    aput-object v3, v2, v8

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {p0, v2, v8, v11, v10}, Lcom/google/android/velvet/presenter/TgPresenter;->postAddPredictiveCards(Ljava/util/List;ZLcom/google/geo/sidekick/Sidekick$Entry;I)V

    :cond_4
    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getEntries()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getTargetEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v2

    iget v3, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mScrollPosAfterNextPopulate:I

    invoke-direct {p0, v0, v8, v2, v3}, Lcom/google/android/velvet/presenter/TgPresenter;->postAddPredictiveCards(Ljava/util/List;ZLcom/google/geo/sidekick/Sidekick$Entry;I)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v3, "CARD_RENDER"

    invoke-virtual {v2, v3, v0}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiActionOnStacks(Ljava/lang/String;Ljava/util/List;)V

    :cond_5
    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->updateTgFooterButtonText()V

    iget-boolean v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mDisableAnimationsOnNextPopulate:Z

    if-eqz v2, :cond_6

    invoke-virtual {p0, v9}, Lcom/google/android/velvet/presenter/TgPresenter;->postSetLayoutAnimationsEnabled(Z)V

    :cond_6
    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v2, v8}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->setWaitBeforeNextPopulate(Z)V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->updateTimeOfLastPopulate()V

    iput-boolean v8, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mDisableAnimationsOnNextPopulate:Z

    iput v10, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mScrollPosAfterNextPopulate:I

    goto/16 :goto_0
.end method

.method postRemovePredictiveCards()V
    .locals 2

    new-instance v0, Lcom/google/android/velvet/presenter/TgPresenter$3;

    const-string v1, "removePredictiveCards"

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/TgPresenter$3;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    return-void
.end method

.method public resetView()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->resetView(Z)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->showLoadingCard()V

    return-void
.end method

.method public resetView(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRemovePredictiveCards()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->resetTimeOfLastPopulate()V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->setWaitBeforeNextPopulate(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mDisableAnimationsOnNextPopulate:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mScrollPosAfterNextPopulate:I

    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;Z)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
    .param p2    # Z

    const-string v0, "predictive_mode"

    iget v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "scroll_pos"

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter;->savePredictiveCardsState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method setCardStateForTest(I)V
    .locals 1
    .param p1    # I

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput p1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setPredictiveMode(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->isMarinerEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->unregisterPredictiveCardsListeners()V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRemovePredictiveCards()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    if-eq p1, v1, :cond_0

    iget v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    iput p1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->commitAllFeedbackWithoutAnimations()V

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->resetTimeOfLastPopulate()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRemovePredictiveCards()V

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->buildPredictiveView()V

    goto :goto_0
.end method

.method public showError()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->removeLoadingCard()V

    const v0, 0x7f0d00d4

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->showToast(I)V

    return-void
.end method

.method public showLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->showLoadingCard()V

    return-void
.end method

.method showLoadingCard()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Velvet.TgPresenter"

    const-string v1, "showLoadingCard when unattached"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mLoadingCard:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/velvet/presenter/TgPresenter$4;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/presenter/TgPresenter$4;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;)V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mUiThread:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mShowLoadingCard:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public showSampleCards()V
    .locals 4

    iget v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mMode:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->isAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->updateTgFooterButtonText()V

    iget-object v2, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->getCardListEntries()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->showSampleCards(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/google/geo/sidekick/Sidekick$Interest;

    invoke-direct {v2}, Lcom/google/geo/sidekick/Sidekick$Interest;-><init>()V

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/geo/sidekick/Sidekick$Interest;->setTargetDisplay(I)Lcom/google/geo/sidekick/Sidekick$Interest;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/presenter/TgPresenter;->doUserRefreshForMore(Lcom/google/geo/sidekick/Sidekick$Interest;)V

    goto :goto_0
.end method

.method public startInitialRefresh()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v2, v2, v1}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->refreshCards(ZZZLcom/google/geo/sidekick/Sidekick$Interest;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/TgPresenter;->mRefreshManager:Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/presenter/PredictiveCardRefreshManager;->setWaitBeforeNextPopulate(Z)V

    return-void
.end method

.method public toggleBackOfCard(Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    new-instance v0, Lcom/google/android/velvet/presenter/TgPresenter$6;

    invoke-direct {v0, p0, p1}, Lcom/google/android/velvet/presenter/TgPresenter$6;-><init>(Lcom/google/android/velvet/presenter/TgPresenter;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/TgPresenter;->post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/TgPresenter;->postRestoreSearchPlateStickiness()V

    return-void
.end method
