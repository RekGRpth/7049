.class public Lcom/google/android/velvet/presenter/UiModeManager;
.super Ljava/lang/Object;
.source "UiModeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/UiModeManager$1;
    }
.end annotation


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private mStartupComplete:Z


# direct methods
.method constructor <init>(Lcom/google/android/searchcommon/SearchConfig;)V
    .locals 0
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/UiModeManager;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-void
.end method

.method private zeroQueryFromPredictiveMode(Lcom/google/android/velvet/presenter/QueryState;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/QueryState;->isZeroQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->isSentinel()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/QueryState;->getCommittedQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/Query;->getSentinelMode()Lcom/google/android/velvet/presenter/UiMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method getFooterStickiness(Lcom/google/android/velvet/presenter/UiMode;Z)I
    .locals 4
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Z

    const/4 v0, 0x3

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/velvet/presenter/UiModeManager$1;->$SwitchMap$com$google$android$velvet$presenter$UiMode:[I

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :cond_0
    :goto_0
    :pswitch_1
    return v0

    :pswitch_2
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/UiModeManager;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->areSearchPlateAndFooterStickyInWebSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_3
    move v0, v1

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method getSearchPlateStickiness(Lcom/google/android/velvet/presenter/UiMode;Z)I
    .locals 3
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Z

    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/velvet/presenter/UiModeManager$1;->$SwitchMap$com$google$android$velvet$presenter$UiMode:[I

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    if-nez p2, :cond_0

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/velvet/presenter/UiModeManager;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->areSearchPlateAndFooterStickyInWebSearch()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method isStartupComplete()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/UiModeManager;->mStartupComplete:Z

    return v0
.end method

.method setStartupComplete()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/UiModeManager;->mStartupComplete:Z

    return-void
.end method

.method shouldGoBackOnPreImeBackPress(Lcom/google/android/velvet/presenter/UiMode;ZZ)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Z
    .param p3    # Z

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->isSuggestMode()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_FALLBACK:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v0, :cond_1

    :cond_0
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method shouldResetSearchBox(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Lcom/google/android/velvet/presenter/UiMode;

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v1, :cond_1

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p2, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_2

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v1, :cond_0

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p2, v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p2, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method shouldScrollToTopOnSearchBoxTouch(Lcom/google/android/velvet/presenter/UiMode;Z)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->isSuggestMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method shouldShowCardList(Lcom/google/android/velvet/presenter/UiMode;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method shouldShowContextHeader(Lcom/google/android/velvet/presenter/UiMode;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v0

    return v0
.end method

.method shouldShowCorpusBarInMode(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;Z)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Lcom/google/android/velvet/presenter/UiMode;
    .param p3    # Z

    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->RESULTS:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->CONNECTION_ERROR:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v1, :cond_1

    if-nez p3, :cond_0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->NONE:Lcom/google/android/velvet/presenter/UiMode;

    invoke-virtual {p0, p2, v1, v0}, Lcom/google/android/velvet/presenter/UiModeManager;->shouldShowCorpusBarInMode(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method shouldShowGogglesMenuItem(Lcom/google/android/velvet/presenter/UiMode;)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->GOGGLES_CAPTURE:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method shouldShowTgFooterButton(Lcom/google/android/velvet/presenter/UiMode;ZZ)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Z
    .param p3    # Z

    const/4 v0, 0x0

    if-eqz p3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->isPredictiveMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/UiMode;->isSuggestMode()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public shouldStopQueryEditOnMainViewClick(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/QueryState;)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;

    sget-object v0, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/velvet/presenter/UiModeManager;->zeroQueryFromPredictiveMode(Lcom/google/android/velvet/presenter/QueryState;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method shouldSuggestFragmentShowSuggestInMode(Lcom/google/android/velvet/presenter/UiMode;)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->RESULTS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/UiModeManager;->mStartupComplete:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method shouldSuggestFragmentShowSummonsInMode(Lcom/google/android/velvet/presenter/UiMode;Z)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Z

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v1, :cond_1

    :cond_0
    if-nez p2, :cond_1

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/UiModeManager;->mStartupComplete:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldSwitchToSummonsOnWebSuggestDismiss(Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/QueryState;)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Lcom/google/android/velvet/presenter/QueryState;

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v1, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/velvet/presenter/UiModeManager;->zeroQueryFromPredictiveMode(Lcom/google/android/velvet/presenter/QueryState;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method shouldUsePredictiveInMode(Lcom/google/android/velvet/presenter/UiMode;Z)Z
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/UiMode;
    .param p2    # Z

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_1

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->PREDICTIVE_CARD_LIST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_1

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/presenter/UiMode;->SUMMONS_SUGGEST:Lcom/google/android/velvet/presenter/UiMode;

    if-ne p1, v1, :cond_2

    :cond_0
    if-eqz p2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/velvet/presenter/UiModeManager;->mStartupComplete:Z

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
