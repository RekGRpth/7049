.class public abstract Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
.super Ljava/lang/Object;
.source "VelvetFragmentPresenter.java"


# instance fields
.field private mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

.field private final mTag:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    return-void
.end method

.method public getDimensionPixelSize(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public getFactory()Lcom/google/android/velvet/VelvetFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v0

    return-object v0
.end method

.method public getQueryState()Lcom/google/android/velvet/presenter/QueryState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSearchController()Lcom/google/android/velvet/presenter/SearchController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/SearchController;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    return-object v0
.end method

.method public getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v0

    return-object v0
.end method

.method public getString(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getString(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public varargs getString(I[Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getString(I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-object v0
.end method

.method public isAttached()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Lcom/google/android/velvet/presenter/VelvetPresenter;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/VelvetPresenter;
    .param p2    # Landroid/os/Bundle;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {p0, p2}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onPostAttach(Landroid/os/Bundle;)V

    return-void
.end method

.method public final onDetach()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onPreDetach()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method protected abstract onPostAttach(Landroid/os/Bundle;)V
.end method

.method protected abstract onPreDetach()V
.end method

.method onPreModeSwitch(Lcom/google/android/velvet/presenter/UiModeManager;Lcom/google/android/velvet/presenter/UiMode;Lcom/google/android/velvet/presenter/UiMode;Z)Z
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/UiModeManager;
    .param p2    # Lcom/google/android/velvet/presenter/UiMode;
    .param p3    # Lcom/google/android/velvet/presenter/UiMode;
    .param p4    # Z

    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public onStart()V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 0

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;Z)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
    .param p2    # Z

    return-void
.end method
