.class Lcom/google/android/velvet/presenter/VelvetPresenter$10;
.super Ljava/lang/Object;
.source "VelvetPresenter.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/VelvetPresenter;->addTheGoogleMenuItems(Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$10;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$10;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$10;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestFragmentPresenter:Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$1200(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/SuggestFragmentPresenter;

    move-result-object v1

    # invokes: Lcom/google/android/velvet/presenter/VelvetPresenter;->isReady(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$1300(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$10;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$900(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v0

    const-string v1, "BUTTON_PRESS"

    const-string v2, "SHOW_CARD_LIST_MENU"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$10;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->showTheGoogleCardList()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
