.class Lcom/google/android/velvet/presenter/VelvetPresenter$12;
.super Ljava/lang/Object;
.source "VelvetPresenter.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/VelvetPresenter;->addFeedbackMenuItem(Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$12;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$12;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$1500(Lcom/google/android/velvet/presenter/VelvetPresenter;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$12;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/SendGoogleFeedback;->launchGoogleFeedback(Landroid/content/Context;Landroid/view/View;)V

    const/4 v0, 0x1

    return v0
.end method
