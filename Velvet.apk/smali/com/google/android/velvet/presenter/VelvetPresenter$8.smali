.class Lcom/google/android/velvet/presenter/VelvetPresenter$8;
.super Ljava/lang/Object;
.source "VelvetPresenter.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/presenter/VelvetPresenter;->addGogglesMenuItem(Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$8;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    invoke-static {}, Lcom/google/android/goggles/TraceTracker;->getMainTraceTracker()Lcom/google/android/goggles/TraceTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/goggles/TraceTracker;->newSession()Lcom/google/android/goggles/TraceTracker$Session;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/goggles/TraceTracker$Session;->addUserEventStartSearch(I)V

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$8;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$800(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$8;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$800(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/Query;->goggles(I)Lcom/google/android/velvet/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->set(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->commit()V

    const/4 v0, 0x0

    return v0
.end method
