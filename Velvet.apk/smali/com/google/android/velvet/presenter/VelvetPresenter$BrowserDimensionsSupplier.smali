.class Lcom/google/android/velvet/presenter/VelvetPresenter$BrowserDimensionsSupplier;
.super Ljava/lang/Object;
.source "VelvetPresenter.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/VelvetPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BrowserDimensionsSupplier"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Landroid/graphics/Point;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$BrowserDimensionsSupplier;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/VelvetPresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/VelvetPresenter;
    .param p2    # Lcom/google/android/velvet/presenter/VelvetPresenter$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter$BrowserDimensionsSupplier;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    return-void
.end method


# virtual methods
.method public get()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$BrowserDimensionsSupplier;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getResultsAreaSizeDp()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/presenter/VelvetPresenter$BrowserDimensionsSupplier;->get()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method
