.class Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;
.super Ljava/lang/Object;
.source "VelvetPresenter.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/presenter/SuggestionsUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/VelvetPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PresenterSuggestionsUi"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/presenter/VelvetPresenter;Lcom/google/android/velvet/presenter/VelvetPresenter$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/VelvetPresenter;
    .param p2    # Lcom/google/android/velvet/presenter/VelvetPresenter$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;-><init>(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    return-void
.end method


# virtual methods
.method public getQuery()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$800(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$800(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/QueryState;->get()Lcom/google/android/velvet/Query;

    move-result-object v0

    return-object v0
.end method

.method public indicateRemoveFromHistoryFailed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mUi:Lcom/google/android/velvet/presenter/VelvetUi;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$700(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/velvet/presenter/VelvetUi;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/velvet/presenter/VelvetUi;->indicateRemoveFromHistoryFailed()V

    return-void
.end method

.method public setWebSuggestionsEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$1600(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->setWebSuggestionsEnabled(Z)V

    return-void
.end method

.method public showSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V
    .locals 1
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestions;

    iget-object v0, p0, Lcom/google/android/velvet/presenter/VelvetPresenter$PresenterSuggestionsUi;->this$0:Lcom/google/android/velvet/presenter/VelvetPresenter;

    # getter for: Lcom/google/android/velvet/presenter/VelvetPresenter;->mSuggestionsController:Lcom/google/android/searchcommon/suggest/SuggestionsController;
    invoke-static {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->access$1600(Lcom/google/android/velvet/presenter/VelvetPresenter;)Lcom/google/android/searchcommon/suggest/SuggestionsController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/searchcommon/suggest/SuggestionsController;->setSuggestions(Lcom/google/android/searchcommon/suggest/Suggestions;)V

    return-void
.end method
