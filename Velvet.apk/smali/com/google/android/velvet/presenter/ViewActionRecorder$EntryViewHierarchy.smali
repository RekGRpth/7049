.class Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;
.super Ljava/lang/Object;
.source "ViewActionRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/ViewActionRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EntryViewHierarchy"
.end annotation


# instance fields
.field final mCardView:Landroid/view/View;

.field final mEntryViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final mIsGroupCard:Z

.field final mRootView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/google/android/apps/sidekick/EntryItemAdapter;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/apps/sidekick/EntryItemAdapter;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mRootView:Landroid/view/View;

    instance-of v1, p1, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/sidekick/PredictiveCardContainer;

    invoke-virtual {p1}, Lcom/google/android/apps/sidekick/PredictiveCardContainer;->getCardView()Landroid/view/View;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mCardView:Landroid/view/View;

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getGroupEntryTreeNode()Lcom/google/geo/sidekick/Sidekick$EntryTreeNode;

    move-result-object v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mIsGroupCard:Z

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mCardView:Landroid/view/View;

    const v4, 0x7f100015

    invoke-interface {p2}, Lcom/google/android/apps/sidekick/EntryItemAdapter;->getEntry()Lcom/google/geo/sidekick/Sidekick$Entry;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    new-array v1, v2, [Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mCardView:Landroid/view/View;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mEntryViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mCardView:Landroid/view/View;

    const v2, 0x7f100016

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mEntryViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0
.end method


# virtual methods
.method getEntryViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mEntryViews:Ljava/util/List;

    return-object v0
.end method

.method isExpanded()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mIsGroupCard:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/presenter/ViewActionRecorder$EntryViewHierarchy;->mCardView:Landroid/view/View;

    const v1, 0x7f100016

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
