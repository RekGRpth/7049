.class public Lcom/google/android/velvet/presenter/VoiceSearchError;
.super Lcom/google/android/velvet/presenter/SearchError;
.source "VoiceSearchError.java"


# instance fields
.field private final mImageId:I

.field private final mMessageId:I

.field private final mResendSameAudio:Z


# direct methods
.method public constructor <init>(Lcom/google/android/speech/exception/RecognizeException;Lcom/google/android/speech/audio/AudioStore;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;
    .param p2    # Lcom/google/android/speech/audio/AudioStore;

    invoke-direct {p0}, Lcom/google/android/velvet/presenter/SearchError;-><init>()V

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->getErrorMessage(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/presenter/VoiceSearchError;->mMessageId:I

    invoke-virtual {p2}, Lcom/google/android/speech/audio/AudioStore;->hasLastAudio()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->canResendSameAudio(Lcom/google/android/speech/exception/RecognizeException;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/presenter/VoiceSearchError;->mResendSameAudio:Z

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->getErrorImage(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/presenter/VoiceSearchError;->mImageId:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getButtonTextId()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/VoiceSearchError;->mResendSameAudio:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0d038c

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/velvet/presenter/SearchError;->getButtonTextId()I

    move-result v0

    goto :goto_0
.end method

.method public getErrorImageResId()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/presenter/VoiceSearchError;->mImageId:I

    return v0
.end method

.method public getErrorMessageResId()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/presenter/VoiceSearchError;->mMessageId:I

    return v0
.end method

.method public retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/QueryState;
    .param p2    # Lcom/google/android/velvet/Query;

    iget-boolean v0, p0, Lcom/google/android/velvet/presenter/VoiceSearchError;->mResendSameAudio:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/velvet/Query;->voiceSearchWithLastRecording()Lcom/google/android/velvet/Query;

    move-result-object p2

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/velvet/presenter/SearchError;->retry(Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/velvet/Query;)V

    return-void
.end method
