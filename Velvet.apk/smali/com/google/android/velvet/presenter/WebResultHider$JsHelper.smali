.class public Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;
.super Lcom/google/android/velvet/util/JavascriptInterfaceHelper;
.source "WebResultHider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/presenter/WebResultHider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "JsHelper"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/presenter/WebResultHider;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/presenter/WebResultHider;Landroid/webkit/WebView;)V
    .locals 2
    .param p2    # Landroid/webkit/WebView;

    iput-object p1, p0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->this$0:Lcom/google/android/velvet/presenter/WebResultHider;

    const-string v0, "wrhagsa"

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;-><init>(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;Ljava/lang/String;ZI)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->googleAgsaHideAll(Ljava/lang/String;ZI)V

    return-void
.end method

.method private googleAgsaHideAll(Ljava/lang/String;ZI)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # I

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v7, 0x0

    const-string v1, "wrhagsa"

    const-string v2, "onHideFailed"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v2, v7, v3}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->callback(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/Object;)Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;

    move-result-object v0

    const-string v1, "google.agsa.hideAll"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v7

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "wrhagsa"

    const-string v4, "onHideResults"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v8, v5}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->callback(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/Object;)Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {p0, v1, v2}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->addJsCallWithArgs(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;->callJs(Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;)V

    return-void
.end method


# virtual methods
.method protected addJavaScriptInterface(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/presenter/WebResultHider$JsHelper$JsBridge;-><init>(Lcom/google/android/velvet/presenter/WebResultHider$JsHelper;Lcom/google/android/velvet/presenter/WebResultHider$1;)V

    invoke-virtual {p1, v0, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
