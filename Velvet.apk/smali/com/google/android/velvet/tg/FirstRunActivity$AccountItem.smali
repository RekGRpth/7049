.class Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;
.super Ljava/lang/Object;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AccountItem"
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mEnabled:Z


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Z)V
    .locals 0
    .param p1    # Landroid/accounts/Account;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mAccount:Landroid/accounts/Account;

    iput-boolean p2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mEnabled:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mEnabled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Landroid/accounts/Account;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    return-object v0
.end method
