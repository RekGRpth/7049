.class Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$2;
.super Ljava/lang/Object;
.source "FirstRunActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;

.field final synthetic val$accountList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$2;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;

    iput-object p2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$2;->val$accountList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$2;->val$accountList:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$2;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;

    invoke-virtual {v2}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/tg/FirstRunActivity;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mAccount:Landroid/accounts/Account;
    invoke-static {v0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->access$100(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Landroid/accounts/Account;

    move-result-object v2

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->optInAccount(Landroid/accounts/Account;)V
    invoke-static {v1, v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2100(Lcom/google/android/velvet/tg/FirstRunActivity;Landroid/accounts/Account;)V

    return-void
.end method
