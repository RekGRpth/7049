.class public Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;
.super Landroid/app/DialogFragment;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AccountSelectorDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/FirstRunActivity;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->setButtonsEnabled(Z)V
    invoke-static {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2200(Lcom/google/android/velvet/tg/FirstRunActivity;Z)V

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/tg/FirstRunActivity;

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->getAnnotedAccounts()Ljava/util/List;
    invoke-static {v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$2000(Lcom/google/android/velvet/tg/FirstRunActivity;)Ljava/util/List;

    move-result-object v0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0d0140

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$1;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x1090003

    invoke-direct {v1, p0, v4, v5, v0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$1;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;Landroid/content/Context;ILjava/util/List;)V

    new-instance v3, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog$2;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;Ljava/util/List;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method
