.class Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;
.super Ljava/lang/Object;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FirstRunScreen"
.end annotation


# instance fields
.field private final mStubId:I

.field private final mViewId:I

.field final synthetic this$0:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;II)V
    .locals 0
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->mStubId:I

    iput p3, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->mViewId:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/velvet/tg/FirstRunActivity$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;II)V

    return-void
.end method


# virtual methods
.method setup(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v1, 0x7f1000c4

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected show()Landroid/view/View;
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    # invokes: Lcom/google/android/velvet/tg/FirstRunActivity;->hideEverything()V
    invoke-static {v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->access$1700(Lcom/google/android/velvet/tg/FirstRunActivity;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->mViewId:I

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->mStubId:I

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->this$0:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->mViewId:I

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->setup(Landroid/view/View;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-object v0
.end method
