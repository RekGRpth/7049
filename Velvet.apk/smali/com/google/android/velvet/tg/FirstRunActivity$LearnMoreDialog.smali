.class public Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog;
.super Landroid/app/DialogFragment;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/FirstRunActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LearnMoreDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # Landroid/os/Bundle;

    new-instance v3, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/searchcommon/UserAgentHelper;->onWebViewCreated(Landroid/webkit/WebView;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v4, 0x7f0d01dd

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const v7, 0x7f0d01de

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const v7, 0x7f0d0017

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "text/html; charset=utf-8"

    const-string v6, "utf-8"

    invoke-virtual {v3, v4, v5, v6}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x104000a

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog$1;

    invoke-direct {v5, p0}, Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog$1;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method
