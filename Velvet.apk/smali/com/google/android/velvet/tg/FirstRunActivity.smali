.class public Lcom/google/android/velvet/tg/FirstRunActivity;
.super Landroid/app/Activity;
.source "FirstRunActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/tg/FirstRunActivity$1;,
        Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;,
        Lcom/google/android/velvet/tg/FirstRunActivity$LearnMoreDialog;,
        Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;,
        Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;,
        Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardAnimator;,
        Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;,
        Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreenWithNext;,
        Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunIntroScreen;,
        Lcom/google/android/velvet/tg/FirstRunActivity$SampleCardScreen;,
        Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;,
        Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;,
        Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;,
        Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;
    }
.end annotation


# instance fields
.field private mAnnotatedAccounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;",
            ">;"
        }
    .end annotation
.end field

.field private mBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

.field private mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

.field private mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mCurrentScreen:I

.field private mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

.field private mFetchingConfigs:Z

.field private mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

.field private mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

.field private mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

.field private mOptInTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mScreens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;",
            ">;"
        }
    .end annotation
.end field

.field private mSkipToOptIn:Z

.field private mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

.field private mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mFetchingConfigs:Z

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/velvet/tg/FirstRunActivity;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/velvet/tg/FirstRunActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->showNextScreen()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/velvet/tg/FirstRunActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->hideEverything()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/velvet/tg/FirstRunActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getAnnotedAccounts()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/velvet/tg/FirstRunActivity;Landroid/accounts/Account;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p1    # Landroid/accounts/Account;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity;->optInAccount(Landroid/accounts/Account;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/velvet/tg/FirstRunActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity;->setButtonsEnabled(Z)V

    return-void
.end method

.method static synthetic access$2302(Lcom/google/android/velvet/tg/FirstRunActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mFetchingConfigs:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/google/android/velvet/tg/FirstRunActivity;)Lcom/google/android/searchcommon/MarinerOptInSettings;
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/google/android/velvet/tg/FirstRunActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/google/android/velvet/tg/FirstRunActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mSkipToOptIn:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/google/android/velvet/tg/FirstRunActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->loggedInUserCanRunMariner()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lcom/google/android/velvet/tg/FirstRunActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->startMarinerOrSearch()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/velvet/tg/FirstRunActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->optInClicked()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/velvet/tg/FirstRunActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->handleDecline()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/velvet/tg/FirstRunActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/tg/FirstRunActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V

    return-void
.end method

.method private canAnyAccountRunTheGoogle()I
    .locals 3

    iget-boolean v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mFetchingConfigs:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mEnabled:Z
    invoke-static {v0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->access$000(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    goto :goto_0
.end method

.method private getAnnotedAccounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private handleDecline()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v1, "BUTTON_PRESS"

    const-string v2, "FIRST_RUN_DECLINE_OPT_IN"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->optAccountOut(Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/MarinerOptInSettings;->setFirstRunScreensShown()V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->clearWorkingConfiguration()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V

    return-void
.end method

.method private hideEverything()V
    .locals 5

    const v3, 0x7f1000b9

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private initializeScreens()V
    .locals 10

    const v9, 0x7f1000bb

    const v8, 0x7f1000ba

    const/4 v6, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;

    invoke-direct {v1, p0, v8, v9, v6}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mSkipToOptIn:Z

    if-nez v0, :cond_0

    iget-object v7, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    new-instance v0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunIntroScreen;

    const v2, 0x7f1000bc

    const v3, 0x7f1000bd

    const v1, 0x7f0d01bf

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v1, 0x7f0d01c0

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunIntroScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/velvet/tg/FirstRunActivity$1;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;

    const v2, 0x7f1000be

    const v3, 0x7f1000bf

    invoke-direct {v1, p0, v2, v3, v6}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleTrafficCardScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;

    const v2, 0x7f1000c0

    const v3, 0x7f1000c1

    invoke-direct {v1, p0, v2, v3, v6}, Lcom/google/android/velvet/tg/FirstRunActivity$SampleFlightCardScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;

    const v2, 0x7f1000c2

    const v3, 0x7f1000c3

    invoke-direct {v1, p0, v2, v3, v6}, Lcom/google/android/velvet/tg/FirstRunActivity$OptInScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    new-instance v1, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;

    invoke-direct {v1, p0, v8, v9, v6}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;IILcom/google/android/velvet/tg/FirstRunActivity$1;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private loggedInUserCanRunMariner()Z
    .locals 4

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->access$100(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Landroid/accounts/Account;

    move-result-object v3

    if-ne v0, v3, :cond_0

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mEnabled:Z
    invoke-static {v1}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->access$000(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Z

    move-result v3

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private optInAccount(Landroid/accounts/Account;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setAccountToUseByName(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->clearWorkingConfiguration()V

    iget v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    iget v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->show()Landroid/view/View;

    :cond_0
    new-instance v0, Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;

    invoke-direct {v0, p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity$OptInHander;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;Landroid/accounts/Account;)V

    new-instance v1, Lcom/google/android/searchcommon/preferences/OptInTask;

    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/searchcommon/preferences/OptInTask;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/apps/sidekick/inject/NetworkClient;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/preferences/OptInTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mOptInTask:Landroid/os/AsyncTask;

    return-void
.end method

.method private optInClicked()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v2, "BUTTON_PRESS"

    const-string v3, "FIRST_RUN_ACCEPT_OPT_IN"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logUiAction(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0d013f

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->setButtonsEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->access$100(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Landroid/accounts/Account;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->optInAccount(Landroid/accounts/Account;)V

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mSkipToOptIn:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->loggedInUserCanRunMariner()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->optInAccount(Landroid/accounts/Account;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;

    invoke-direct {v0}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "selectAccount"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountSelectorDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private returnToVelvet(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mSkipToOptIn:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

    const-string v1, "refresh_search_domain_and_cookies"

    invoke-interface {v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasks;->forceRunInterruptingOngoing(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz p1, :cond_1

    const-string v1, "android.intent.action.ASSIST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "from_first_run_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->finish()V

    return-void
.end method

.method private setButtonsEnabled(Z)V
    .locals 1
    .param p1    # Z

    const v0, 0x7f1000c9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    const v0, 0x7f1000c8

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private setupLayoutTransitions()V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x3

    const/4 v7, 0x2

    const v4, 0x7f1000b9

    invoke-virtual {p0, v4}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v3, Landroid/animation/LayoutTransition;

    invoke-direct {v3}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x10e0002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(J)V

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    fill-array-data v5, :array_0

    invoke-static {v9, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v7}, Landroid/animation/LayoutTransition;->getDuration(I)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    fill-array-data v5, :array_1

    invoke-static {v9, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v8}, Landroid/animation/LayoutTransition;->getDuration(I)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v3, v7, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    invoke-virtual {v3, v8, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000
    .end array-data

    :array_1
    .array-data 4
        0x3f800000
        0x0
    .end array-data
.end method

.method private showHeader(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000

    const v1, 0x7f1000b7

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private showNextScreen()V
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    iget v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->show()Landroid/view/View;

    :cond_0
    return-void
.end method

.method private showPreviousScreen()V
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    if-gt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    iget v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->show()Landroid/view/View;

    goto :goto_0
.end method

.method private startMarinerOrSearch()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->canAnyAccountRunTheGoogle()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->showHeader(Z)V

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->showNextScreen()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method completeOptIn(Landroid/accounts/Account;)V
    .locals 3
    .param p1    # Landroid/accounts/Account;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->getSavedConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "FirstRunActivity"

    const-string v2, "Null configuration in completeOptIn"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->setAccountToUseByName(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v1, p1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->optAccountIn(Landroid/accounts/Account;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->setFirstRunScreensShown()V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->copyMasterToWorkingFor(Landroid/accounts/Account;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->optIntoLatitude()V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->startTgServices()V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->maybeRegisterSidekickAlarms()V

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v1}, Lcom/google/android/apps/sidekick/inject/EntryProvider;->invalidate()V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mSkipToOptIn:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->handleDecline()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->showPreviousScreen()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    iget-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getNetworkClient()Lcom/google/android/apps/sidekick/inject/NetworkClient;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    iget-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getBackgroundTasks()Lcom/google/android/velvet/VelvetBackgroundTasks;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mBackgroundTasks:Lcom/google/android/velvet/VelvetBackgroundTasks;

    iget-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mVelvetApplication:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v6}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getEntryProvider()Lcom/google/android/apps/sidekick/inject/EntryProvider;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mEntryProvider:Lcom/google/android/apps/sidekick/inject/EntryProvider;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mMarinerOptInSettings:Lcom/google/android/searchcommon/MarinerOptInSettings;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "skip_to_end"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mSkipToOptIn:Z

    new-instance v6, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    invoke-direct {v6, p0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCardsPrefs:Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    iget-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mNetworkClient:Lcom/google/android/apps/sidekick/inject/NetworkClient;

    invoke-interface {v6}, Lcom/google/android/apps/sidekick/inject/NetworkClient;->isNetworkAvailable()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-direct {p0, v9}, Lcom/google/android/velvet/tg/FirstRunActivity;->returnToVelvet(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->initializeScreens()V

    const v6, 0x7f040036

    invoke-virtual {p0, v6}, Lcom/google/android/velvet/tg/FirstRunActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->setupLayoutTransitions()V

    if-eqz p1, :cond_3

    const-string v6, "current_screen"

    invoke-virtual {p1, v6, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    const-string v6, "header_visible"

    const/4 v7, 0x1

    invoke-virtual {p1, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-direct {p0, v9}, Lcom/google/android/velvet/tg/FirstRunActivity;->showHeader(Z)V

    :cond_2
    const-string v6, "enabled"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v4

    const-string v6, "accounts"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v4, :cond_3

    if-eqz v1, :cond_3

    array-length v6, v4

    array-length v7, v1

    if-ne v6, v7, :cond_3

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    const/4 v5, 0x0

    :goto_1
    array-length v6, v4

    if-ge v5, v6, :cond_3

    aget-object v0, v1, v5

    check-cast v0, Landroid/accounts/Account;

    iget-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    new-instance v7, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    aget-boolean v8, v4, v5

    invoke-direct {v7, v0, v8}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;-><init>(Landroid/accounts/Account;Z)V

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    invoke-virtual {v6}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAllAccounts()[Landroid/accounts/Account;

    move-result-object v2

    new-instance v6, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;

    invoke-direct {v6, p0, v2}, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;[Landroid/accounts/Account;)V

    new-array v7, v9, [Ljava/lang/Void;

    invoke-virtual {v6, v7}, Lcom/google/android/velvet/tg/FirstRunActivity$UpdateAccountConfigurationsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mOptInTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mOptInTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mOptInTask:Landroid/os/AsyncTask;

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    iget v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->show()Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mUserInteractionLogger:Lcom/google/android/searchcommon/google/UserInteractionLogger;

    const-string v1, "FIRST_RUN"

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logView(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mFetchingConfigs:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mScreens:Ljava/util/List;

    iget v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/FirstRunActivity$FirstRunScreen;->show()Landroid/view/View;

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v5, "current_screen"

    iget v6, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mCurrentScreen:I

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const v5, 0x7f1000b7

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v6, "header_visible"

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-virtual {p1, v6, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v5, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v1, v5, [Z

    iget-object v5, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v0, v5, [Landroid/accounts/Account;

    const/4 v3, 0x0

    :goto_1
    iget-object v5, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    iget-object v5, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAnnotatedAccounts:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mEnabled:Z
    invoke-static {v4}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->access$000(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Z

    move-result v5

    aput-boolean v5, v1, v3

    # getter for: Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->mAccount:Landroid/accounts/Account;
    invoke-static {v4}, Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;->access$100(Lcom/google/android/velvet/tg/FirstRunActivity$AccountItem;)Landroid/accounts/Account;

    move-result-object v5

    aput-object v5, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    const-string v5, "enabled"

    invoke-virtual {p1, v5, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    const-string v5, "accounts"

    invoke-virtual {p1, v5, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_2
    return-void
.end method
