.class Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SimpleGridItem"
.end annotation


# instance fields
.field mDismissTrail:Landroid/view/View;

.field mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

.field final synthetic this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

.field view:Landroid/view/View;

.field views:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;Lcom/google/android/velvet/tg/DismissTrailFactory;)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/google/android/velvet/tg/DismissTrailFactory;

    iput-object p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

    return-void
.end method


# virtual methods
.method public getGridLayoutParams()Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    return-object v0
.end method

.method public getMeasuredHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getMeasuredWidth()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public getViews()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    return-object v0
.end method

.method public gridLayout(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public gridMeasure(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public inheritPadding()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method public isGone()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public shouldShowDismissTrail()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

    invoke-interface {v0}, Lcom/google/android/velvet/tg/DismissTrailFactory;->shouldShowDismissTrail()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showDismissTrail()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

    invoke-interface {v2}, Lcom/google/android/velvet/tg/DismissTrailFactory;->createDismissTrail()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    iget v4, v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->column:I

    # invokes: Lcom/google/android/velvet/tg/SuggestionGridLayout;->setupLayoutParams(Landroid/view/View;I)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;
    invoke-static {v2, v3, v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1300(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;I)Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    move-result-object v0

    sget-object v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->FADE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iput-object v2, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    # invokes: Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    invoke-static {v2, v3, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1401(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    iput-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->view:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->views:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$SimpleGridItem;->mDismissTrail:Landroid/view/View;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method
