.class Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"

# interfaces
.implements Lcom/google/android/velvet/tg/SuggestionGridLayout$GridItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/tg/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StackGridItem"
.end annotation


# instance fields
.field public bottom:I

.field public left:I

.field mChildHeight:I

.field mCollapsedCardsBottom:I

.field mCollapsedCardsTop:I

.field final mDensity:F

.field mDisableClipping:Z

.field mDismissTrail:Landroid/view/View;

.field mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

.field mExpanded:Z

.field final mExpandedOverlapAmount:I

.field mLp:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

.field mMeasuredHeight:I

.field mMeasuredWidth:I

.field mPaddingAdjustment:I

.field mShowingDismissTrail:Z

.field mTmpClipRect:Landroid/graphics/Rect;

.field mTopItemBeingDragged:Z

.field mTotalCollapsedSpacing:I

.field mViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public right:I

.field final synthetic this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

.field public top:I


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/content/Context;Ljava/util/ArrayList;ILcom/google/android/velvet/tg/DismissTrailFactory;)V
    .locals 4
    .param p2    # Landroid/content/Context;
    .param p4    # I
    .param p5    # Lcom/google/android/velvet/tg/DismissTrailFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I",
            "Lcom/google/android/velvet/tg/DismissTrailFactory;",
            ")V"
        }
    .end annotation

    const/4 v3, -0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mShowingDismissTrail:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTmpClipRect:Landroid/graphics/Rect;

    iput v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDisableClipping:Z

    iput v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mPaddingAdjustment:I

    iput v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mCollapsedCardsTop:I

    iput v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mCollapsedCardsBottom:I

    iput-boolean v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    iput-boolean v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTopItemBeingDragged:Z

    iput-object p5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

    iput-object p3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v3, v1, p4}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mLp:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDensity:F

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->updateTotalCollapsedSpacing()V

    iget v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDensity:F

    const/high16 v1, 0x42be0000

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->updateItemBackgrounds()V

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    return-void
.end method

.method private getTallestChildHeight(II)I
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0

    :cond_0
    return v2
.end method

.method private unwrapCardView(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/View;

    instance-of v0, p1, Lcom/google/android/velvet/tg/SuggestionGridLayout$CardWrapper;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/velvet/tg/SuggestionGridLayout$CardWrapper;

    invoke-interface {p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$CardWrapper;->getCardView()Landroid/view/View;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public bringToFrontAndCollapse(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isTopCard(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setDisableClipping(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->invalidate()V

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    new-instance v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$2;-><init>(Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;Landroid/view/View;)V

    const-wide/16 v2, 0x46

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method clippingDisabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDisableClipping:Z

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v5, 0x0

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    iget-object v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int v6, v7, v8

    sub-int v3, v6, v5

    const/4 v7, 0x0

    int-to-float v8, v3

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v1, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    move v5, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public enableAnimation(Z)V
    .locals 6
    .param p1    # Z

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    iget-object v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    instance-of v5, v4, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    if-eqz v5, :cond_0

    move-object v3, v4

    check-cast v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    if-eqz p1, :cond_1

    sget-object v5, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->DEAL:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    :goto_1
    iput-object v5, v3, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;->appearAnimationType:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    sget-object v5, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;->NONE:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams$AnimationType;

    goto :goto_1

    :cond_2
    return-void
.end method

.method getChildClipRect(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 8
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_0

    iget-object v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    add-int/lit8 v7, v2, 0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-direct {p0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->unwrapCardView(Landroid/view/View;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    add-int v0, v6, v7

    :cond_0
    iget-object v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTmpClipRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v3, v5, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTmpClipRect:Landroid/graphics/Rect;

    return-object v6
.end method

.method public getGridLayoutParams()Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mLp:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    return-object v0
.end method

.method public getMeasuredHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mMeasuredHeight:I

    return v0
.end method

.method public getMeasuredWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mMeasuredWidth:I

    return v0
.end method

.method public getViews()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    return-object v0
.end method

.method public gridLayout(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->onLayout(ZIIII)V

    return-void
.end method

.method public gridMeasure(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->onMeasure(II)V

    return-void
.end method

.method public inheritPadding()V
    .locals 2

    const-string v0, "SuggestionGridLayout"

    const-string v1, "Stacks can\'t inherit padding"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method isCollapsible()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    return v0
.end method

.method public isGone()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isTopCard(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    iput p2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->left:I

    iput p4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->right:I

    iput p3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->top:I

    iput p5, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->bottom:I

    iget v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->top:I

    iput v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mCollapsedCardsTop:I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_4

    iget-object v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-boolean v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-nez v6, :cond_0

    const/4 v6, 0x1

    if-ne v1, v6, :cond_2

    const/4 v4, 0x0

    :goto_1
    float-to-double v6, v4

    const-wide/high16 v8, 0x4000000000000000L

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-float v4, v6

    iget v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    int-to-float v6, v6

    mul-float/2addr v6, v4

    float-to-int v5, v6

    iget v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->top:I

    add-int v2, v6, v5

    if-lez v3, :cond_0

    iget v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mPaddingAdjustment:I

    sub-int/2addr v2, v6

    :cond_0
    add-int/lit8 v6, v1, -0x1

    if-ge v3, v6, :cond_3

    iget v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->left:I

    iget v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->right:I

    iget v8, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    add-int/2addr v8, v2

    invoke-virtual {v0, v6, v2, v7, v8}, Landroid/view/View;->layout(IIII)V

    :goto_2
    iget-boolean v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v6, :cond_1

    add-int/2addr v2, v5

    if-nez v3, :cond_1

    iget v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mPaddingAdjustment:I

    sub-int/2addr v2, v6

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/high16 v6, 0x3f800000

    int-to-float v7, v3

    mul-float/2addr v6, v7

    add-int/lit8 v7, v1, -0x1

    int-to-float v7, v7

    div-float v4, v6, v7

    goto :goto_1

    :cond_3
    iput v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mCollapsedCardsBottom:I

    iget v6, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->left:I

    iget v7, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->right:I

    iget v8, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->bottom:I

    invoke-virtual {v0, v6, v2, v7, v8}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    :cond_4
    return-void
.end method

.method public onMeasure(II)V
    .locals 16
    .param p1    # I
    .param p2    # I

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    move/from16 v5, p1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    const/4 v6, 0x0

    const/high16 v13, 0x40000000

    if-ne v7, v13, :cond_4

    move v6, v8

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    sub-int v13, v8, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    const/high16 v14, 0x40000000

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mPaddingAdjustment:I

    const/4 v9, 0x0

    :goto_2
    if-ge v9, v3, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    instance-of v13, v2, Lcom/google/android/velvet/tg/SuggestionGridLayout$CardWrapper;

    if-eqz v13, :cond_2

    move-object v13, v2

    check-cast v13, Lcom/google/android/velvet/tg/SuggestionGridLayout$CardWrapper;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    invoke-interface {v13, v14}, Lcom/google/android/velvet/tg/SuggestionGridLayout$CardWrapper;->setForcedCardHeight(I)V

    :cond_2
    move/from16 v0, p1

    invoke-virtual {v2, v0, v4}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->unwrapCardView(Landroid/view/View;)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTmpClipRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v13}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mPaddingAdjustment:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTmpClipRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mPaddingAdjustment:I

    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_4
    const/high16 v13, -0x80000000

    if-ne v7, v13, :cond_7

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    add-int/lit8 v14, v3, -0x1

    mul-int/2addr v13, v14

    sub-int v10, v8, v13

    const/high16 v13, -0x80000000

    invoke-static {v10, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v11}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->getTallestChildHeight(II)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v13, :cond_5

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    add-int/lit8 v15, v3, -0x1

    mul-int/2addr v14, v15

    add-int v6, v13, v14

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    const/4 v13, 0x1

    if-le v3, v13, :cond_6

    const/4 v13, 0x1

    :goto_3
    mul-int/2addr v13, v15

    add-int v6, v14, v13

    goto/16 :goto_1

    :cond_6
    const/4 v13, 0x0

    goto :goto_3

    :cond_7
    if-nez v7, :cond_1

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v11}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->getTallestChildHeight(II)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eqz v13, :cond_8

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpandedOverlapAmount:I

    add-int/lit8 v15, v3, -0x1

    mul-int/2addr v14, v15

    add-int v6, v13, v14

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    const/4 v13, 0x1

    if-le v3, v13, :cond_9

    const/4 v13, 0x1

    :goto_4
    mul-int/2addr v13, v15

    add-int v6, v14, v13

    goto/16 :goto_1

    :cond_9
    const/4 v13, 0x0

    goto :goto_4

    :cond_a
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mPaddingAdjustment:I

    sub-int/2addr v6, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v6}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setMeasuredDimension(II)V

    goto/16 :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isCollapsible()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setExpanded(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->updateItemBackgrounds()V

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->updateTotalCollapsedSpacing()V

    return-void
.end method

.method setDisableClipping(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDisableClipping:Z

    return-void
.end method

.method setExpanded(Z)V
    .locals 4
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->isCollapsible()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    if-eq p1, v0, :cond_1

    iput-boolean p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mExpanded:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->setDisableClipping(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->requestLayout()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    new-instance v1, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem$1;-><init>(Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method setMeasuredDimension(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mMeasuredWidth:I

    iput p2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mMeasuredHeight:I

    return-void
.end method

.method public setVisibility(I)V
    .locals 4
    .param p1    # I

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public shouldShowDismissTrail()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mShowingDismissTrail:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

    invoke-interface {v0}, Lcom/google/android/velvet/tg/DismissTrailFactory;->shouldShowDismissTrail()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showDismissTrail()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDismissTrailFactory:Lcom/google/android/velvet/tg/DismissTrailFactory;

    invoke-interface {v1}, Lcom/google/android/velvet/tg/DismissTrailFactory;->createDismissTrail()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDismissTrail:Landroid/view/View;

    new-instance v0, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mLp:Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;

    invoke-direct {v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDismissTrail:Landroid/view/View;

    # invokes: Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    invoke-static {v1, v2, v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1701(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mChildHeight:I

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDismissTrail:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->updateTotalCollapsedSpacing()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mShowingDismissTrail:Z

    iget-object v1, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDismissTrail:Landroid/view/View;

    # invokes: Lcom/google/android/velvet/tg/SuggestionGridLayout;->fadeIn(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1800(Lcom/google/android/velvet/tg/SuggestionGridLayout;Landroid/view/View;)V

    return-void
.end method

.method public updateItemBackgrounds()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->unwrapCardView(Landroid/view/View;)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mResources:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->unwrapCardView(Landroid/view/View;)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->this$0:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    # getter for: Lcom/google/android/velvet/tg/SuggestionGridLayout;->mResources:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->access$1500(Lcom/google/android/velvet/tg/SuggestionGridLayout;)Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02000b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method updateTotalCollapsedSpacing()V
    .locals 5

    const/high16 v4, 0x3f800000

    iget v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mDensity:F

    const/high16 v3, 0x42e60000

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v2, v0

    mul-float/2addr v2, v4

    const/high16 v3, 0x40400000

    div-float/2addr v2, v3

    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float v3, v4, v1

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/velvet/tg/SuggestionGridLayout$StackGridItem;->mTotalCollapsedSpacing:I

    return-void
.end method
