.class Lcom/google/android/velvet/ui/CardListView$2;
.super Ljava/lang/Object;
.source "CardListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/CardListView;->addOtherCardsRow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/CardListView;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/CardListView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/CardListView$2;->this$0:Lcom/google/android/velvet/ui/CardListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v2, Lcom/google/android/velvet/Help;

    iget-object v3, p0, Lcom/google/android/velvet/ui/CardListView$2;->this$0:Lcom/google/android/velvet/ui/CardListView;

    invoke-virtual {v3}, Lcom/google/android/velvet/ui/CardListView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/velvet/Help;-><init>(Landroid/content/Context;)V

    const-string v3, "cardlist"

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/Help;->getHelpIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/CardListView$2;->this$0:Lcom/google/android/velvet/ui/CardListView;

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/CardListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getSidekickInjector()Lcom/google/android/apps/sidekick/inject/SidekickInjector;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/sidekick/inject/SidekickInjector;->getActivityHelper()Lcom/google/android/apps/sidekick/inject/ActivityHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/velvet/ui/CardListView$2;->this$0:Lcom/google/android/velvet/ui/CardListView;

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/CardListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/sidekick/inject/ActivityHelper;->safeStartActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    return-void
.end method
