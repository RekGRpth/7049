.class public Lcom/google/android/velvet/ui/FooterFragment;
.super Lcom/google/android/velvet/ui/VelvetFragment;
.source "FooterFragment.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/FooterUi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/velvet/ui/VelvetFragment",
        "<",
        "Lcom/google/android/velvet/presenter/FooterPresenter;",
        ">;",
        "Lcom/google/android/velvet/presenter/FooterUi;"
    }
.end annotation


# instance fields
.field private mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

.field private mCorpusBarContainer:Landroid/view/View;

.field private mCorpusBarStub:Landroid/view/ViewStub;

.field private mFooterDividerCorpora:Landroid/view/View;

.field private mFooterDividerTgButton:Landroid/view/View;

.field private mMoreCardsButton:Landroid/widget/Button;

.field private mShowingCorpusBar:Z

.field private mShowingMoreCards:Z

.field private mTgOverflowButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetFragment;-><init>()V

    return-void
.end method

.method private inflateCorpusBar()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterFragment;->isCorpusBarLoaded()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarStub:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarContainer:Landroid/view/View;

    const v2, 0x7f100078

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/widget/CorpusBar;

    iput-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

    iget-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    check-cast v1, Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v2, v1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->setPresenter(Lcom/google/android/velvet/presenter/FooterPresenter;)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarContainer:Landroid/view/View;

    const v2, 0x7f100079

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterFragment;->getPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/FooterPresenter;->updateCorpora()V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterFragment;->getPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/FooterPresenter;->getSelectedCorpus()Lcom/google/android/velvet/Corpus;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/FooterFragment;->setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addCorpusSelector(Lcom/google/android/velvet/Corpus;Z)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpus;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterFragment;->isCorpusBarLoaded()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/ui/widget/CorpusBar;->addCorpusSelector(Lcom/google/android/velvet/Corpus;Z)V

    return-void
.end method

.method public isCorpusBarLoaded()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v2, 0x7f040040

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f1000e6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    iput-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarStub:Landroid/view/ViewStub;

    iput-object v3, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarContainer:Landroid/view/View;

    iput-object v3, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

    const v2, 0x7f1000e7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mMoreCardsButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mMoreCardsButton:Landroid/widget/Button;

    new-instance v3, Lcom/google/android/velvet/ui/FooterFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/velvet/ui/FooterFragment$1;-><init>(Lcom/google/android/velvet/ui/FooterFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f1000e5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mFooterDividerTgButton:Landroid/view/View;

    const v2, 0x7f1000e4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mFooterDividerCorpora:Landroid/view/View;

    const v2, 0x7f1000e8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mTgOverflowButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mTgOverflowButton:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mMoreCardsButton:Landroid/widget/Button;

    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/widget/Button;->setPadding(IIII)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Lcom/google/android/velvet/ui/FooterFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/FooterFragment$2;-><init>(Lcom/google/android/velvet/ui/FooterFragment;)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/FooterFragment;->mTgOverflowButton:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mShowingCorpusBar:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mShowingMoreCards:Z

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mMoreCardsButton:Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mTgOverflowButton:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarStub:Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarContainer:Landroid/view/View;

    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->onDestroyView()V

    return-void
.end method

.method public removeCorpusSelectors(Lcom/google/android/velvet/Corpus;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpus;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterFragment;->isCorpusBarLoaded()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->removeCorpusSelectors(Lcom/google/android/velvet/Corpus;)V

    return-void
.end method

.method public resetShowMoreCorpora()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterFragment;->isCorpusBarLoaded()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->resetSelectedCorpus()V

    return-void
.end method

.method public setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Corpus;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterFragment;->isCorpusBarLoaded()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBar:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V

    return-void
.end method

.method public setShowCorpusBar(Z)V
    .locals 2
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterFragment;->isCorpusBarLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/ui/FooterFragment;->inflateCorpusBar()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mFooterDividerCorpora:Landroid/view/View;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mShowingCorpusBar:Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mCorpusBarContainer:Landroid/view/View;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/velvet/ui/FooterFragment;->showOrHideView(ZZLandroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mShowingCorpusBar:Z

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setShowTgFooterButton(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mFooterDividerTgButton:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mShowingMoreCards:Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterFragment;->mMoreCardsButton:Landroid/widget/Button;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/velvet/ui/FooterFragment;->showOrHideView(ZZLandroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mShowingMoreCards:Z

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTgFooterButtonEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mMoreCardsButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public setTgFooterButtonText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterFragment;->mMoreCardsButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
