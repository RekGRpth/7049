.class Lcom/google/android/velvet/ui/InAppWebPageActivity$TryAgainButtonListener;
.super Ljava/lang/Object;
.source "InAppWebPageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/InAppWebPageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TryAgainButtonListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$TryAgainButtonListener;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;Lcom/google/android/velvet/ui/InAppWebPageActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/ui/InAppWebPageActivity;
    .param p2    # Lcom/google/android/velvet/ui/InAppWebPageActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/InAppWebPageActivity$TryAgainButtonListener;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity$TryAgainButtonListener;->this$0:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    # getter for: Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->tryAgain()V

    return-void
.end method
