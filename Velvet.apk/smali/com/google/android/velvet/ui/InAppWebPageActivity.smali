.class public Lcom/google/android/velvet/ui/InAppWebPageActivity;
.super Landroid/app/Activity;
.source "InAppWebPageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/InAppWebPageActivity$TryAgainButtonListener;,
        Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebChromeClient;,
        Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;
    }
.end annotation


# instance fields
.field private mErrorCard:Landroid/view/View;

.field private mErrorMessageView:Landroid/widget/TextView;

.field private mFadeDuration:I

.field private mLoadingIndicator:Landroid/view/View;

.field private mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mLoadingIndicator:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    return-object v0
.end method


# virtual methods
.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/FileDescriptor;
    .param p3    # Ljava/io/PrintWriter;
    .param p4    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    return-void
.end method

.method public getWebViewState()Landroid/os/Bundle;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    return-object v0
.end method

.method public hideError()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorCard:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorCard:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorCard:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method public hideLoadingIndicator()V
    .locals 4

    new-instance v0, Lcom/google/android/velvet/ui/InAppWebPageActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity$1;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mLoadingIndicator:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mLoadingIndicator:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v2, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mFadeDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method public loadUri(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x10e0000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mFadeDuration:I

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    const v4, 0x7f040062

    invoke-virtual {p0, v4}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setContentView(I)V

    const v4, 0x7f100142

    invoke-virtual {p0, v4}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/webkit/WebView;

    iput-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    iget-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v5, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;

    invoke-direct {v5, p0, v6}, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebViewClient;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;Lcom/google/android/velvet/ui/InAppWebPageActivity$1;)V

    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v5, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebChromeClient;

    invoke-direct {v5, p0, v6}, Lcom/google/android/velvet/ui/InAppWebPageActivity$InAppWebChromeClient;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;Lcom/google/android/velvet/ui/InAppWebPageActivity$1;)V

    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/UserAgentHelper;->onWebViewCreated(Landroid/webkit/WebView;)V

    invoke-virtual {v3}, Lcom/google/android/searchcommon/UserAgentHelper;->getUserAgent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    const v4, 0x7f100143

    invoke-virtual {p0, v4}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorCard:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorCard:Landroid/view/View;

    const v5, 0x7f1000a9

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorMessageView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorCard:Landroid/view/View;

    const v5, 0x7f1000aa

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v4, Lcom/google/android/velvet/ui/InAppWebPageActivity$TryAgainButtonListener;

    invoke-direct {v4, p0, v6}, Lcom/google/android/velvet/ui/InAppWebPageActivity$TryAgainButtonListener;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;Lcom/google/android/velvet/ui/InAppWebPageActivity$1;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f100144

    invoke-virtual {p0, v4}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mLoadingIndicator:Landroid/view/View;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/google/android/velvet/VelvetFactory;->createInAppWebPagePresenter(Lcom/google/android/velvet/ui/InAppWebPageActivity;)Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-virtual {v4, p1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->restoreState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->saveState(Landroid/os/Bundle;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mPresenter:Lcom/google/android/velvet/presenter/InAppWebPagePresenter;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/InAppWebPagePresenter;->start(Landroid/content/Intent;)V

    return-void
.end method

.method public restoreWebViewState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    move-result-object v0

    return-void
.end method

.method public showError(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorMessageView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorCard:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mErrorCard:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mFadeDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method public showLoadingIndicator()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mLoadingIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mLoadingIndicator:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mFadeDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method public showWebView()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mFadeDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method public stopLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    return-void
.end method
