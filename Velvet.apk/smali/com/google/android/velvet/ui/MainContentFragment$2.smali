.class Lcom/google/android/velvet/ui/MainContentFragment$2;
.super Ljava/lang/Object;
.source "MainContentFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/MainContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/MainContentFragment;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/MainContentFragment$2;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$2;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/ui/MainContentFragment;->mNotifyScrollPosted:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/MainContentFragment;->access$002(Lcom/google/android/velvet/ui/MainContentFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$2;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$2;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->getPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/MainContentPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->onViewScrolled()V

    :cond_0
    return-void
.end method
