.class Lcom/google/android/velvet/ui/MainContentFragment$6;
.super Ljava/lang/Object;
.source "MainContentFragment.java"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/MainContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/MainContentFragment;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/MainContentFragment$6;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/animation/LayoutTransition;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/View;
    .param p4    # I

    const/4 v0, 0x3

    if-ne p4, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$6;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    # operator-- for: Lcom/google/android/velvet/ui/MainContentFragment;->mNumDisappearTransitions:I
    invoke-static {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->access$910(Lcom/google/android/velvet/ui/MainContentFragment;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$6;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    # invokes: Lcom/google/android/velvet/ui/MainContentFragment;->checkLayoutTransitionsComplete()V
    invoke-static {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->access$1000(Lcom/google/android/velvet/ui/MainContentFragment;)V

    return-void
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/animation/LayoutTransition;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/view/View;
    .param p4    # I

    const/4 v0, 0x3

    if-ne p4, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment$6;->this$0:Lcom/google/android/velvet/ui/MainContentFragment;

    # operator++ for: Lcom/google/android/velvet/ui/MainContentFragment;->mNumDisappearTransitions:I
    invoke-static {v0}, Lcom/google/android/velvet/ui/MainContentFragment;->access$908(Lcom/google/android/velvet/ui/MainContentFragment;)I

    :cond_0
    return-void
.end method
