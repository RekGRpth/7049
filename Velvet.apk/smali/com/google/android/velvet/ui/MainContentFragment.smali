.class public Lcom/google/android/velvet/ui/MainContentFragment;
.super Lcom/google/android/velvet/ui/VelvetFragment;
.source "MainContentFragment.java"

# interfaces
.implements Lcom/google/android/velvet/presenter/MainContentUi;
.implements Lcom/google/android/velvet/ui/HasLayoutTransitions;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/velvet/presenter/MainContentPresenter;",
        ">",
        "Lcom/google/android/velvet/ui/VelvetFragment",
        "<TT;>;",
        "Lcom/google/android/velvet/presenter/MainContentUi;",
        "Lcom/google/android/velvet/ui/HasLayoutTransitions;"
    }
.end annotation


# static fields
.field private static final COMMIT_TRANSACTIONS_BUDGET_MS:I = 0x5

.field private static final DBG:Z = false

.field private static final DBG_TIMING:Z = false

.field private static final MAX_WAIT_FOR_LAYOUT_MS:J = 0x7d0L

.field private static final NOTIFY_SCROLL_THROTTLE_DELAY_MS:I = 0x64

.field private static final NOT_POSTED:I = 0x0

.field private static final POSTED:I = 0x2

.field private static final POSTED_WITH_TIMEOUT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Velvet.MainContentFragment"


# instance fields
.field private final mAttachListener:Landroid/view/View$OnAttachStateChangeListener;

.field private mAttachedToWindow:Z

.field private mCardsLayoutTransition:Landroid/animation/LayoutTransition;

.field private mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

.field private mCheckPendingViewsLaidOutPosted:I

.field private final mCheckPendingViewsLaidOutRunnable:Ljava/lang/Runnable;

.field private mClock:Lcom/google/android/searchcommon/util/Clock;

.field private final mCommitTransactionsRunnable:Ljava/lang/Runnable;

.field private mCommitTransactionsRunnablePosted:Z

.field private mCommittingTransactions:Z

.field private final mContainerLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private final mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mNotifyScrollPosted:Z

.field private final mNotifyScrolledRunnable:Ljava/lang/Runnable;

.field private mNumDisappearTransitions:I

.field private final mPendingTransactions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final mScrollListener:Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

.field private mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

.field private final mShowWhenLaidOut:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final mTmpWaitingForLayoutViewsToShow:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

.field private mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mView:Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

.field private mWhiteBackgroundView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/MainContentFragment;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetFragment;-><init>()V

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/MainContentFragment$1;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollListener:Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/MainContentFragment$2;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNotifyScrolledRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/MainContentFragment$3;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/MainContentFragment$4;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment$5;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/MainContentFragment$5;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment$6;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/MainContentFragment$6;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment$7;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/MainContentFragment$7;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mContainerLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    new-instance v0, Lcom/google/android/velvet/ui/MainContentFragment$8;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/MainContentFragment$8;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mAttachListener:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;->setVelvetTag(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mTmpWaitingForLayoutViewsToShow:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/ui/MainContentFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNotifyScrollPosted:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/velvet/ui/MainContentFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNotifyScrollPosted:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/velvet/ui/MainContentFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNotifyScrolledRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->checkLayoutTransitionsComplete()V

    return-void
.end method

.method static synthetic access$1102(Lcom/google/android/velvet/ui/MainContentFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mAttachedToWindow:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/velvet/ui/MainContentFragment;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/ui/MainContentFragment;Z)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;->maybePostCommitTransactions(Z)V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/velvet/ui/MainContentFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnablePosted:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/velvet/ui/MainContentFragment;Z)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;->commitTransactions(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/velvet/ui/MainContentFragment;J)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/ui/MainContentFragment;->postCheckPendingViewsLaidOut(J)V

    return-void
.end method

.method static synthetic access$702(Lcom/google/android/velvet/ui/MainContentFragment;I)I
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutPosted:I

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/velvet/ui/MainContentFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->checkPendingViewsLaidOut()V

    return-void
.end method

.method static synthetic access$908(Lcom/google/android/velvet/ui/MainContentFragment;)I
    .locals 2
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;

    iget v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNumDisappearTransitions:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNumDisappearTransitions:I

    return v0
.end method

.method static synthetic access$910(Lcom/google/android/velvet/ui/MainContentFragment;)I
    .locals 2
    .param p0    # Lcom/google/android/velvet/ui/MainContentFragment;

    iget v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNumDisappearTransitions:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNumDisappearTransitions:I

    return v0
.end method

.method private areTransitionsRunningOrLayoutPending(Z)Z
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mAttachedToWindow:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->isRunningLayoutTransition()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->isAnimatingScroll()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkLayoutTransitionsComplete()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->isRunningLayoutTransition()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->onLayoutTransitionFinished()V

    :cond_0
    return-void
.end method

.method private checkPendingViewsLaidOut()V
    .locals 15

    iget-object v12, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v12

    if-nez v12, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v12, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v12}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v7

    const-wide v0, 0x7fffffffffffffffL

    iget-object v12, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v12

    if-eqz v12, :cond_1

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v12

    if-eqz v12, :cond_1

    iget-object v12, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mTmpWaitingForLayoutViewsToShow:Ljava/util/List;

    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/util/Pair;

    iget-object v12, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v12, Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v12, v7, v3

    if-lez v12, :cond_2

    const-string v12, "Velvet.MainContentFragment"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Gave up waiting for layout of "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v12, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mTmpWaitingForLayoutViewsToShow:Ljava/util/List;

    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-static {v3, v4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_1

    :cond_3
    iget-object v12, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mTmpWaitingForLayoutViewsToShow:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    iget-object v12, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-interface {v12, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/util/Pair;

    iget-object v11, v12, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/util/List;

    invoke-direct {p0, v10}, Lcom/google/android/velvet/ui/MainContentFragment;->stopWaitingForViewLayout(Landroid/view/View;)V

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_5
    iget-object v12, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mTmpWaitingForLayoutViewsToShow:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->clear()V

    const-wide v12, 0x7fffffffffffffffL

    cmp-long v12, v0, v12

    if-eqz v12, :cond_6

    sub-long v12, v0, v7

    invoke-direct {p0, v12, v13}, Lcom/google/android/velvet/ui/MainContentFragment;->postCheckPendingViewsLaidOut(J)V

    goto/16 :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->checkLayoutTransitionsComplete()V

    goto/16 :goto_0
.end method

.method private commitTransactions(Z)V
    .locals 12
    .param p1    # Z

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x5

    add-long v1, v6, v8

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnablePosted:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v7, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnable:Ljava/lang/Runnable;

    invoke-interface {v6, v7}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iput-boolean v10, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnablePosted:Z

    :cond_1
    if-nez p1, :cond_2

    iget-boolean v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommittingTransactions:Z

    if-nez v6, :cond_7

    invoke-direct {p0, v10}, Lcom/google/android/velvet/ui/MainContentFragment;->areTransitionsRunningOrLayoutPending(Z)Z

    move-result v6

    if-nez v6, :cond_7

    :cond_2
    iget-boolean v5, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommittingTransactions:Z

    iput-boolean v11, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommittingTransactions:Z

    const/4 v0, 0x1

    :cond_3
    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_6

    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-interface {v6, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    const/4 v3, 0x0

    :cond_4
    if-nez v3, :cond_5

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;->prepare()Z

    move-result v3

    const-string v6, "prepare()"

    invoke-direct {p0, v4, v6, v1, v2}, Lcom/google/android/velvet/ui/MainContentFragment;->logTimeSpent(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;Ljava/lang/String;J)V

    if-nez p1, :cond_4

    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v6

    cmp-long v6, v6, v1

    if-ltz v6, :cond_4

    const/4 v0, 0x0

    :cond_5
    if-eqz v0, :cond_9

    invoke-virtual {v4, p0}, Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;->commit(Lcom/google/android/velvet/presenter/MainContentUi;)V

    const-string v6, "commit()"

    invoke-direct {p0, v4, v6, v1, v2}, Lcom/google/android/velvet/ui/MainContentFragment;->logTimeSpent(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;Ljava/lang/String;J)V

    const/4 v4, 0x0

    if-nez p1, :cond_9

    invoke-direct {p0, v10}, Lcom/google/android/velvet/ui/MainContentFragment;->areTransitionsRunningOrLayoutPending(Z)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_6
    :goto_1
    iput-boolean v5, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommittingTransactions:Z

    :cond_7
    const/4 v6, 0x0

    const-string v7, "commitTransactions"

    invoke-direct {p0, v6, v7, v1, v2}, Lcom/google/android/velvet/ui/MainContentFragment;->logTimeSpent(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;Ljava/lang/String;J)V

    goto :goto_0

    :cond_8
    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v6}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v6

    cmp-long v6, v6, v1

    if-ltz v6, :cond_9

    const/4 v0, 0x0

    :cond_9
    if-nez v0, :cond_3

    if-eqz v4, :cond_a

    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-interface {v6, v10, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_a
    if-nez v5, :cond_6

    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_6

    iput-boolean v11, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnablePosted:Z

    iget-object v6, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v7, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnable:Ljava/lang/Runnable;

    invoke-interface {v6, v7}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private getWhiteBackgroundView()Landroid/view/View;
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mWhiteBackgroundView:Landroid/view/View;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f100272

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mWhiteBackgroundView:Landroid/view/View;

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mWhiteBackgroundView:Landroid/view/View;

    return-object v1
.end method

.method private logTimeSpent(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;Ljava/lang/String;J)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    return-void
.end method

.method private maybePostCommitTransactions(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnablePosted:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommittingTransactions:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/MainContentFragment;->areTransitionsRunningOrLayoutPending(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnablePosted:Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private postCheckPendingViewsLaidOut(J)V
    .locals 4
    .param p1    # J

    const-wide/16 v2, 0x0

    iget v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutPosted:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    :pswitch_1
    cmp-long v0, p1, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutPosted:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutPosted:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private stopWaitingForViewLayout(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private tag()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Velvet.MainContentFragment@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/FileDescriptor;
    .param p3    # Ljava/io/PrintWriter;
    .param p4    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/ui/VelvetFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "MainContentFragment State:"

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-void
.end method

.method public getCardsView()Lcom/google/android/velvet/tg/SuggestionGridLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    return-object v0
.end method

.method public getRelativeScrollDistanceFromTop(Landroid/view/View;)F
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getDescendantTop(Landroid/view/View;)I

    move-result v1

    if-gez v1, :cond_0

    const v2, 0x7f7fffff

    :goto_0
    return v2

    :cond_0
    invoke-interface {v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v2

    sub-int v2, v1, v2

    int-to-float v2, v2

    invoke-interface {v0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getViewportHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    goto :goto_0
.end method

.method public getScrollViewControl()Lcom/google/android/velvet/ui/util/ScrollViewControl;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;

    move-result-object v0

    return-object v0
.end method

.method public getScrollingContainer()Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .locals 3

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    :goto_0
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer;

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iget-object v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollListener:Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->addScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iget-object v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mContainerLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    return-object v1
.end method

.method public isRunningDisappearTransitions()Z
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNumDisappearTransitions:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRunningLayoutTransition()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v2, 0x0

    const v0, 0x7f04006e

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mView:Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mView:Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

    const v1, 0x7f100168

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    new-instance v1, Lcom/google/android/velvet/ui/MainContentFragment$9;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/MainContentFragment$9;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setOnDismissListener(Lcom/google/android/velvet/tg/SuggestionGridLayout$OnDismissListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    new-instance v1, Lcom/google/android/velvet/ui/MainContentFragment$10;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/MainContentFragment$10;-><init>(Lcom/google/android/velvet/ui/MainContentFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setOnStackChangeListener(Lcom/google/android/velvet/tg/SuggestionGridLayout$OnStackChangeListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsLayoutTransition:Landroid/animation/LayoutTransition;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsLayoutTransition:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsLayoutTransition:Landroid/animation/LayoutTransition;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mView:Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mAttachListener:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getUiExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mView:Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mView:Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mAttachListener:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsLayoutTransition:Landroid/animation/LayoutTransition;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mTransitionListener:Landroid/animation/LayoutTransition$TransitionListener;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->removeTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    iput v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mNumDisappearTransitions:I

    iget v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutPosted:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iput v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCheckPendingViewsLaidOutPosted:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollListener:Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->removeScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mContainerLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    invoke-virtual {p0, v2}, Lcom/google/android/velvet/ui/MainContentFragment;->setFooterPositionLocked(Z)V

    iput-boolean v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCommitTransactionsRunnablePosted:Z

    iput-object v3, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mScrollingContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iput-object v3, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mWhiteBackgroundView:Landroid/view/View;

    iput-object v3, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mView:Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

    iput-object v3, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    return-void
.end method

.method onLayoutTransitionFinished()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    check-cast v0, Lcom/google/android/velvet/presenter/MainContentPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/MainContentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onFragmentLayoutTransitionFinished()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/MainContentFragment;->maybePostCommitTransactions(Z)V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/VelvetFragment;->onTrimMemory(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->releaseStackBitmap()V

    :cond_0
    return-void
.end method

.method public post(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/MainContentFragment;->commitTransactions(Z)V

    return-void
.end method

.method public postImmediate(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/MainContentFragment;->commitTransactions(Z)V

    return-void
.end method

.method public postToHead(Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/MainContentPresenter$Transaction;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mPendingTransactions:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/google/android/velvet/ui/MainContentFragment;->commitTransactions(Z)V

    return-void
.end method

.method public setFooterPositionLocked(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/VelvetActivity;->setFooterPositionLocked(Z)V

    return-void
.end method

.method public final setHeaderAndFooterPadding(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mCardsView:Lcom/google/android/velvet/tg/SuggestionGridLayout;

    invoke-virtual {v2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->getPaddingRight()I

    move-result v2

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/google/android/velvet/tg/SuggestionGridLayout;->setPadding(IIII)V

    :cond_0
    return-void
.end method

.method public setMatchPortraitMode(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mView:Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/ChildPaddingLayout;->setMatchPortraitMode(Z)V

    return-void
.end method

.method public setSearchPlateStuckToScrollingView(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/VelvetActivity;

    if-eqz p1, :cond_0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/ui/VelvetActivity;->setSearchPlateStickiness(IZZ)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/presenter/MainContentPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/MainContentPresenter;->getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getSearchPlateStickiness()I

    move-result v1

    invoke-virtual {v0, v1, v3, v3}, Lcom/google/android/velvet/ui/VelvetActivity;->setSearchPlateStickiness(IZZ)V

    goto :goto_0
.end method

.method public setWhiteBackgroundState(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getWhiteBackgroundView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public showToast(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public showViewsPendingLayout(Landroid/view/View;Ljava/util/List;)V
    .locals 7
    .param p1    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-wide/16 v5, 0x7d0

    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p1, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mShowWhenLaidOut:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/velvet/ui/MainContentFragment;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v3

    add-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v5, v6}, Lcom/google/android/velvet/ui/MainContentFragment;->postCheckPendingViewsLaidOut(J)V

    :cond_2
    return-void

    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
