.class public abstract Lcom/google/android/velvet/ui/RetainedFragmentPresenter;
.super Ljava/lang/Object;
.source "RetainedFragmentPresenter.java"


# instance fields
.field private mAttached:Z

.field private mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getVelvetPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->mAttached:Z

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-object v0
.end method

.method public final isAttached()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->mAttached:Z

    return v0
.end method

.method public final onAttach(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetPresenter;

    iput-object p1, p0, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->mAttached:Z

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->onPostAttach(Lcom/google/android/velvet/presenter/VelvetPresenter;)V

    return-void
.end method

.method public final onDetach()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->onPreDetach()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->mAttached:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/RetainedFragmentPresenter;->mVelvetPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;

    return-void
.end method

.method protected abstract onPostAttach(Lcom/google/android/velvet/presenter/VelvetPresenter;)V
.end method

.method protected abstract onPreDetach()V
.end method
