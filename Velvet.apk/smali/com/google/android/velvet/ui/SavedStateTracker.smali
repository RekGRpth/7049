.class public Lcom/google/android/velvet/ui/SavedStateTracker;
.super Ljava/lang/Object;
.source "SavedStateTracker.java"


# instance fields
.field private mStateSaved:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private noteStateNotSaved()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/SavedStateTracker;->mStateSaved:Z

    return-void
.end method


# virtual methods
.method public haveSavedState()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/SavedStateTracker;->mStateSaved:Z

    return v0
.end method

.method public onActivityResult()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/SavedStateTracker;->noteStateNotSaved()V

    return-void
.end method

.method public onCreate()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/SavedStateTracker;->noteStateNotSaved()V

    return-void
.end method

.method public onNewIntent()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/SavedStateTracker;->noteStateNotSaved()V

    return-void
.end method

.method public onRestart()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/SavedStateTracker;->noteStateNotSaved()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/SavedStateTracker;->noteStateNotSaved()V

    return-void
.end method

.method public onSaveInstanceState()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/SavedStateTracker;->mStateSaved:Z

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/SavedStateTracker;->noteStateNotSaved()V

    return-void
.end method
