.class public Lcom/google/android/velvet/ui/SavedStateTrackingActivity;
.super Landroid/app/Activity;
.source "SavedStateTrackingActivity.java"


# instance fields
.field private mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-direct {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    return-void
.end method


# virtual methods
.method public haveSavedState()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->haveSavedState()Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onActivityResult()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onCreate()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onNewIntent()V

    return-void
.end method

.method protected onRestart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onRestart()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onResume()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onSaveInstanceState()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingActivity;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onStart()V

    return-void
.end method
