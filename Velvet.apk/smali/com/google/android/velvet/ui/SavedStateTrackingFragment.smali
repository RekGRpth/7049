.class public Lcom/google/android/velvet/ui/SavedStateTrackingFragment;
.super Landroid/app/Fragment;
.source "SavedStateTrackingFragment.java"


# instance fields
.field private mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-direct {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    return-void
.end method


# virtual methods
.method public haveSavedState()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->haveSavedState()Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onActivityResult()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onCreate()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onSaveInstanceState()V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->mSavedState:Lcom/google/android/velvet/ui/SavedStateTracker;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/SavedStateTracker;->onStart()V

    return-void
.end method
