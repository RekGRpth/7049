.class Lcom/google/android/velvet/ui/VelvetActivity$6;
.super Ljava/lang/Object;
.source "VelvetActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/VelvetActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/VelvetActivity;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/VelvetActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/VelvetActivity$6;->this$0:Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity$6;->this$0:Lcom/google/android/velvet/ui/VelvetActivity;

    # getter for: Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lcom/google/android/velvet/presenter/VelvetPresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/VelvetActivity;->access$000(Lcom/google/android/velvet/ui/VelvetActivity;)Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->onMainViewTouched()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
