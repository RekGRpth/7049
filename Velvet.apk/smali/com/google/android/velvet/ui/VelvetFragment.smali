.class public Lcom/google/android/velvet/ui/VelvetFragment;
.super Lcom/google/android/velvet/ui/SavedStateTrackingFragment;
.source "VelvetFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;",
        ">",
        "Lcom/google/android/velvet/ui/SavedStateTrackingFragment;"
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "Velvet.VelvetFragment"

.field private static final VELVET_TAG_KEY:Ljava/lang/String; = "velvet_tag"


# instance fields
.field private mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

.field protected mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mVelvetTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;-><init>()V

    return-void
.end method

.method private tag()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Velvet.VelvetFragment["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final attachFragmentManagerForTesting(Lcom/google/android/velvet/presenter/VelvetFragments;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragments;

    iput-object p1, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    invoke-virtual {p1, p0}, Lcom/google/android/velvet/presenter/VelvetFragments;->onFragmentCreated(Lcom/google/android/velvet/ui/VelvetFragment;)V

    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Lcom/google/android/velvet/presenter/VelvetFragments;->onFragmentViewCreated(Lcom/google/android/velvet/ui/VelvetFragment;Landroid/os/Bundle;)V

    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/FileDescriptor;
    .param p3    # Ljava/io/PrintWriter;
    .param p4    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "VelvetFragment tag:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getVelvetTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    return-void
.end method

.method protected getClock()Lcom/google/android/searchcommon/util/Clock;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPresenter()Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    return-object v0
.end method

.method protected getUiExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getVelvetTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mVelvetTag:Ljava/lang/String;

    return-object v0
.end method

.method public final isAttached()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCurrentBackFragment()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getId()I

    move-result v0

    const v1, 0x7f100273

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCurrentFrontFragment()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getId()I

    move-result v0

    const v1, 0x7f100274

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onAttach(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getRetainInstance()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    check-cast p1, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {p1}, Lcom/google/android/velvet/ui/VelvetActivity;->getPresenter()Lcom/google/android/velvet/presenter/VelvetPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetPresenter;->getFragments()Lcom/google/android/velvet/presenter/VelvetFragments;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mVelvetTag:Ljava/lang/String;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    const-string v0, "velvet_tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mVelvetTag:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/VelvetFragments;->onFragmentCreated(Lcom/google/android/velvet/ui/VelvetFragment;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/VelvetFragments;->onFragmentDestroyed(Lcom/google/android/velvet/ui/VelvetFragment;)V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/velvet/presenter/VelvetFragments;->onFragmentViewDestroyed(Lcom/google/android/velvet/ui/VelvetFragment;Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onDestroyView()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # Landroid/os/Bundle;

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/googlequicksearchbox/R$styleable;->VelvetFragment:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/VelvetFragment;->setVelvetTag(Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "velvet_tag"

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mVelvetTag:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->saveInstanceState(Landroid/os/Bundle;Z)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getId()I

    move-result v1

    const v2, 0x7f100274

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getId()I

    move-result v1

    const v2, 0x7f100273

    if-ne v1, v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/ui/util/Animations;->showAndFadeIn(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getId()I

    move-result v1

    const v2, 0x7f100274

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getId()I

    move-result v1

    const v2, 0x7f100273

    if-ne v1, v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onStop()V

    invoke-super {p0}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onStop()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/SavedStateTrackingFragment;->onTrimMemory(I)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetFragment;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->onTrimMemory(I)V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/os/Bundle;

    iput-object p2, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mSavedInstanceState:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mFragments:Lcom/google/android/velvet/presenter/VelvetFragments;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/velvet/presenter/VelvetFragments;->onFragmentViewCreated(Lcom/google/android/velvet/ui/VelvetFragment;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mSavedInstanceState:Landroid/os/Bundle;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setPresenter(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    return-void
.end method

.method protected setVelvetTag(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/ui/VelvetFragment;->mVelvetTag:Ljava/lang/String;

    return-void
.end method

.method protected showOrHideView(ZZLandroid/view/View;)Z
    .locals 1
    .param p1    # Z
    .param p2    # Z
    .param p3    # Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/velvet/ui/VelvetFragment;->showOrHideView(ZZLandroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method protected showOrHideView(ZZLandroid/view/View;I)Z
    .locals 0
    .param p1    # Z
    .param p2    # Z
    .param p3    # Landroid/view/View;
    .param p4    # I

    if-eq p1, p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p1, :cond_1

    invoke-static {p3}, Lcom/google/android/velvet/ui/util/Animations;->showAndFadeIn(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    :goto_0
    return p1

    :cond_1
    invoke-static {p3, p4}, Lcom/google/android/velvet/ui/util/Animations;->fadeOutAndHide(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method
