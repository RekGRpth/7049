.class public Lcom/google/android/velvet/ui/VelvetTopLevelContainer;
.super Landroid/widget/FrameLayout;
.source "VelvetTopLevelContainer.java"


# instance fields
.field private mBaseFooterPadding:I

.field private mBaseHeaderPadding:I

.field private mContextHeader:Landroid/view/View;

.field private mContextHeaderMargin:I

.field private mContextHeaderShown:Z

.field private mFooter:Landroid/view/View;

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private mIncludeFooterPadding:Z

.field private mIncludeFooterSpace:Z

.field private mPreImeKeyListener:Landroid/view/View$OnKeyListener;

.field private mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

.field private mSearchPlate:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private setFragmentPadding(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v1, p1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/velvet/ui/MainContentFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/velvet/ui/MainContentFragment;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/velvet/ui/MainContentFragment;->setHeaderAndFooterPadding(II)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->setHeaderAndFooterPadding(II)V

    :cond_1
    return-void
.end method

.method private updateFragmentPadding()V
    .locals 4

    iget v1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mBaseHeaderPadding:I

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mContextHeaderShown:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mContextHeader:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mContextHeaderMargin:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    :goto_0
    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mIncludeFooterPadding:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mBaseFooterPadding:I

    add-int/2addr v0, v2

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mIncludeFooterSpace:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mFooter:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    const v2, 0x7f100274

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->setFragmentPadding(III)V

    const v2, 0x7f100273

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->setFragmentPadding(III)V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mSearchPlate:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    const-string v0, "DRAW"

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onUiOperationStart(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    const-string v0, "DRAW"

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onUiOperationEnd(Ljava/lang/String;)V

    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    invoke-static {p1}, Lcom/google/android/velvet/VelvetStrictMode;->onPreImeKeyEvent(Landroid/view/KeyEvent;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mPreImeKeyListener:Landroid/view/View$OnKeyListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mPreImeKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-interface {v0, p0, v1, p1}, Landroid/view/View$OnKeyListener;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f100275

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mContextHeader:Landroid/view/View;

    const v0, 0x7f100276

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mSearchPlate:Landroid/view/View;

    const v0, 0x7f100277

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mFooter:Landroid/view/View;

    const v0, 0x7f100271

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mScrollView:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mBaseHeaderPadding:I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mBaseFooterPadding:I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mContextHeaderMargin:I

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const-string v0, "LAYOUT"

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onUiOperationStart(Ljava/lang/String;)V

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    const-string v0, "LAYOUT"

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onUiOperationEnd(Ljava/lang/String;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    const-string v0, "MEASURE"

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onUiOperationStart(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mContextHeader:Landroid/view/View;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->measureChildWithMargins(Landroid/view/View;IIII)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mSearchPlate:Landroid/view/View;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->measureChildWithMargins(Landroid/view/View;IIII)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mFooter:Landroid/view/View;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->measureChildWithMargins(Landroid/view/View;IIII)V

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->updateFragmentPadding()V

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    const-string v0, "MEASURE"

    invoke-static {v0}, Lcom/google/android/velvet/VelvetStrictMode;->onUiOperationEnd(Ljava/lang/String;)V

    return-void
.end method

.method public setContextHeaderShown(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mContextHeaderShown:Z

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->updateFragmentPadding()V

    return-void
.end method

.method public setFragmentManager(Landroid/app/FragmentManager;)V
    .locals 0
    .param p1    # Landroid/app/FragmentManager;

    iput-object p1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mFragmentManager:Landroid/app/FragmentManager;

    return-void
.end method

.method public setIncludeFooterPadding(ZZ)V
    .locals 0
    .param p1    # Z
    .param p2    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mIncludeFooterSpace:Z

    iput-boolean p2, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mIncludeFooterPadding:Z

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->updateFragmentPadding()V

    return-void
.end method

.method public setPreImeKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnKeyListener;

    iput-object p1, p0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->mPreImeKeyListener:Landroid/view/View$OnKeyListener;

    return-void
.end method
