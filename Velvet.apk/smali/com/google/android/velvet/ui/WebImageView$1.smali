.class Lcom/google/android/velvet/ui/WebImageView$1;
.super Ljava/lang/Object;
.source "WebImageView.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/WebImageView;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/WebImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/WebImageView$1;->this$0:Lcom/google/android/velvet/ui/WebImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Landroid/graphics/drawable/Drawable;)Z
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView$1;->this$0:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageFromUri(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView$1;->this$0:Lcom/google/android/velvet/ui/WebImageView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->access$002(Lcom/google/android/velvet/ui/WebImageView;Lcom/google/android/searchcommon/util/CancellableNowOrLater;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView$1;->this$0:Lcom/google/android/velvet/ui/WebImageView;

    # getter for: Lcom/google/android/velvet/ui/WebImageView;->mDownloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;
    invoke-static {v0}, Lcom/google/android/velvet/ui/WebImageView;->access$100(Lcom/google/android/velvet/ui/WebImageView;)Lcom/google/android/velvet/ui/WebImageView$Listener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView$1;->this$0:Lcom/google/android/velvet/ui/WebImageView;

    # getter for: Lcom/google/android/velvet/ui/WebImageView;->mDownloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;
    invoke-static {v0}, Lcom/google/android/velvet/ui/WebImageView;->access$100(Lcom/google/android/velvet/ui/WebImageView;)Lcom/google/android/velvet/ui/WebImageView$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/velvet/ui/WebImageView$Listener;->onImageDownloaded(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/WebImageView$1;->consume(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method
