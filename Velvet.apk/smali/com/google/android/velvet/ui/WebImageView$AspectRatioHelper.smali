.class public Lcom/google/android/velvet/ui/WebImageView$AspectRatioHelper;
.super Ljava/lang/Object;
.source "WebImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/WebImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "AspectRatioHelper"
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static setAspectRatio(Lcom/google/android/velvet/ui/WebImageView;DIIIII)V
    .locals 5
    .param p0    # Lcom/google/android/velvet/ui/WebImageView;
    .param p1    # D
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-double v3, v1

    mul-double/2addr v3, p1

    double-to-int v0, v3

    invoke-static {p5}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p5}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p7, v3}, Ljava/lang/Math;->min(II)I

    move-result p7

    :cond_0
    if-ge p7, v0, :cond_1

    sub-int v3, v0, p7

    mul-int/2addr v3, p3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/ui/WebImageView;->setScrollY(I)V

    move v0, p7

    :cond_1
    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/google/android/velvet/ui/WebImageView;->setDimensions(II)V

    :cond_2
    return-void
.end method

.method protected static shouldApplyAspectRatio(DI)Z
    .locals 3
    .param p0    # D
    .param p2    # I

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmpl-double v1, p0, v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "WebImageView"

    const-string v2, "fixedAspectRatio set, but neither width nor height is restricted."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
