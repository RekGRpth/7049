.class public Lcom/google/android/velvet/ui/WebImageView;
.super Landroid/widget/ImageView;
.source "WebImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/WebImageView$AspectRatioHelper;,
        Lcom/google/android/velvet/ui/WebImageView$Listener;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "WebImageView"


# instance fields
.field private mAspectRatio:D

.field private mDownloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

.field private mFromCache:Z

.field private mImageScroll:I

.field private mImageUri:Landroid/net/Uri;

.field private mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/CancellableNowOrLater",
            "<+",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mLoaderCallback:Lcom/google/android/searchcommon/util/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/Consumer",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mPreloadDrawable:Landroid/graphics/drawable/Drawable;

.field private mUsingDefaultPlaceholderColor:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/ui/WebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/WebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/google/android/googlequicksearchbox/R$styleable;->WebImageView:[I

    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    float-to-double v1, v1

    iput-wide v1, p0, Lcom/google/android/velvet/ui/WebImageView;->mAspectRatio:D

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/ui/WebImageView;->mImageScroll:I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/WebImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setBackgroundColor(I)V

    iput-boolean v4, p0, Lcom/google/android/velvet/ui/WebImageView;->mUsingDefaultPlaceholderColor:Z

    :cond_0
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/velvet/ui/WebImageView;Lcom/google/android/searchcommon/util/CancellableNowOrLater;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/WebImageView;
    .param p1    # Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    iput-object p1, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/velvet/ui/WebImageView;)Lcom/google/android/velvet/ui/WebImageView$Listener;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/WebImageView;

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mDownloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

    return-object v0
.end method


# virtual methods
.method public getImageUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getImageUriString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mImageUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mImageUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isLoadedFromCache()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mFromCache:Z

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    iget-object v1, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoaderCallback:Lcom/google/android/searchcommon/util/Consumer;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->cancelGetLater(Lcom/google/android/searchcommon/util/Consumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    :cond_0
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    iget-wide v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mAspectRatio:D

    invoke-static {v0, v1, p1}, Lcom/google/android/velvet/ui/WebImageView$AspectRatioHelper;->shouldApplyAspectRatio(DI)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v1, p0, Lcom/google/android/velvet/ui/WebImageView;->mAspectRatio:D

    iget v3, p0, Lcom/google/android/velvet/ui/WebImageView;->mImageScroll:I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/WebImageView;->getSuggestedMinimumWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/WebImageView;->getMaxHeight()I

    move-result v7

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-static/range {v0 .. v7}, Lcom/google/android/velvet/ui/WebImageView$AspectRatioHelper;->setAspectRatio(Lcom/google/android/velvet/ui/WebImageView;DIIIII)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    goto :goto_0
.end method

.method protected resetBackgroundForDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mUsingDefaultPlaceholderColor:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/WebImageView;->setBackgroundColor(I)V

    :cond_0
    return-void
.end method

.method protected setDimensions(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/velvet/ui/WebImageView;->setMeasuredDimension(II)V

    return-void
.end method

.method protected setImageFromUri(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mPreloadDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/WebImageView;->resetBackgroundForDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setImageUri(Landroid/net/Uri;)V
    .locals 7
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/velvet/ui/WebImageView;->mImageUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    iget-object v6, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoaderCallback:Lcom/google/android/searchcommon/util/Consumer;

    invoke-interface {v5, v6}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->cancelGetLater(Lcom/google/android/searchcommon/util/Consumer;)V

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/WebImageView;->mPreloadDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/WebImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getImageLoader()Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->haveNow()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mFromCache:Z

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mDownloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

    if-eqz v5, :cond_1

    if-eqz v1, :cond_1

    iget-object v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mDownloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

    invoke-interface {v5, v1}, Lcom/google/android/velvet/ui/WebImageView$Listener;->onImageDownloaded(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setImageFromUri(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_2
    iget-object v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoaderCallback:Lcom/google/android/searchcommon/util/Consumer;

    if-nez v5, :cond_3

    new-instance v5, Lcom/google/android/velvet/ui/WebImageView$1;

    invoke-direct {v5, p0}, Lcom/google/android/velvet/ui/WebImageView$1;-><init>(Lcom/google/android/velvet/ui/WebImageView;)V

    iput-object v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoaderCallback:Lcom/google/android/searchcommon/util/Consumer;

    :cond_3
    iget-object v5, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoaderCallback:Lcom/google/android/searchcommon/util/Consumer;

    invoke-interface {v2, v5}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    iput-object v2, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    goto :goto_0
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/WebImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v2

    if-eqz p1, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->parseMaybeRelative(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    return-void
.end method

.method public setOnDownloadListener(Lcom/google/android/velvet/ui/WebImageView$Listener;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/ui/WebImageView$Listener;

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mDownloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-object p1, p0, Lcom/google/android/velvet/ui/WebImageView;->mDownloadListener:Lcom/google/android/velvet/ui/WebImageView$Listener;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPreloadDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/velvet/ui/WebImageView;->mPreloadDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/velvet/ui/WebImageView;->mLoader:Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method
