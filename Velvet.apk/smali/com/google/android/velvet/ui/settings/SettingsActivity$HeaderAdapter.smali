.class Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/settings/SettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HeaderAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/preference/PreferenceActivity$Header;",
        ">;"
    }
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mNotificationsSwitchHandler:Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

.field private final mOptOutSwitchHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;Ljava/util/List;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;
    .param p3    # Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;",
            "Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->mOptOutSwitchHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    iput-object p3, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->mNotificationsSwitchHandler:Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v11, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    if-nez p2, :cond_1

    iget-object v4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f040099

    invoke-virtual {v4, v7, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    new-instance v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;

    invoke-direct {v1, v11}, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;-><init>(Lcom/google/android/velvet/ui/settings/SettingsActivity$1;)V

    const v4, 0x7f1001e2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    const v4, 0x1020016

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    const v4, 0x1020010

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    const v4, 0x7f1001e3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Switch;

    iput-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->switchWidget:Landroid/widget/Switch;

    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    iget v7, v0, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceActivity$Header;->getSummary(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v7, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    :goto_1
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-wide v7, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v9, 0x7f1002a2

    cmp-long v4, v7, v9

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->mOptOutSwitchHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->hasSwitch()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->mOptOutSwitchHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    iget-object v5, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->switchWidget:Landroid/widget/Switch;

    invoke-virtual {v4, v5}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->setSwitch(Landroid/widget/Switch;)V

    iget-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->switchWidget:Landroid/widget/Switch;

    invoke-virtual {v4, v6}, Landroid/widget/Switch;->setVisibility(I)V

    :cond_0
    :goto_2
    return-object v3

    :cond_1
    move-object v3, p2

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;

    goto :goto_0

    :cond_2
    move v4, v6

    goto :goto_1

    :cond_3
    iget-wide v7, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v9, 0x7f1002a3

    cmp-long v4, v7, v9

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->mNotificationsSwitchHandler:Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->hasSwitch()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;->mNotificationsSwitchHandler:Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

    iget-object v5, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->switchWidget:Landroid/widget/Switch;

    invoke-virtual {v4, v5}, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->setSwitch(Landroid/widget/Switch;)V

    iget-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->switchWidget:Landroid/widget/Switch;

    invoke-virtual {v4, v6}, Landroid/widget/Switch;->setVisibility(I)V

    goto :goto_2

    :cond_4
    iget-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->switchWidget:Landroid/widget/Switch;

    invoke-virtual {v4, v5}, Landroid/widget/Switch;->setVisibility(I)V

    iget-object v4, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter$HeaderViewHolder;->switchWidget:Landroid/widget/Switch;

    invoke-virtual {v4, v11}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_2
.end method
