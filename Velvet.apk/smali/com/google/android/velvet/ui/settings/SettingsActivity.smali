.class public Lcom/google/android/velvet/ui/settings/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SettingsActivity.java"

# interfaces
.implements Lcom/google/android/searchcommon/preferences/HasOptOutSwitchHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;
    }
.end annotation


# instance fields
.field private mInitialHeader:Landroid/preference/PreferenceActivity$Header;

.field private mNotificationsHandler:Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

.field private mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private createOptInIntent(Lcom/google/android/searchcommon/MarinerOptInSettings;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-interface {p1}, Lcom/google/android/searchcommon/MarinerOptInSettings;->userHasSeenFirstRunScreens()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "skip_to_end"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method private maybeInitializeDataManager()V
    .locals 3

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/speech/embedded/Greco3DataManager;->maybeInitialize(Lcom/google/android/speech/callback/SimpleCallback;)Z

    return-void
.end method


# virtual methods
.method public getOptOutSwitchHandler()Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    return-object v0
.end method

.method public getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getPreferenceController()Lcom/google/android/searchcommon/GsaPreferenceController;

    move-result-object v0

    const-string v1, "sidekick"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->newNowConfigurationPreferences(Landroid/content/SharedPreferences;)Lcom/google/android/searchcommon/preferences/NowConfigurationPreferences;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/google/android/searchcommon/GsaPreferenceController;->isMainPreferencesName(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    goto :goto_0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v4}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v15

    invoke-interface {v15}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserInteractionLogger()Lcom/google/android/searchcommon/google/UserInteractionLogger;

    move-result-object v15

    const-string v16, "SETTINGS"

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/searchcommon/google/UserInteractionLogger;->logView(Ljava/lang/String;)V

    const v15, 0x7f070014

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v15, v1}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->loadHeadersFromResource(ILjava/util/List;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v11

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v12

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v5

    new-instance v13, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    invoke-static {v5}, Lcom/google/android/apps/sidekick/ReminderEntryAdapter;->remindersEnabled(Lcom/google/android/searchcommon/SearchConfig;)Z

    move-result v14

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_13

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/preference/PreferenceActivity$Header;

    invoke-virtual {v11}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-wide v15, v8, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v17, 0x7f1002a2

    cmp-long v15, v15, v17

    if-eqz v15, :cond_1

    iget-wide v15, v8, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v17, 0x7f1002a3

    cmp-long v15, v15, v17

    if-eqz v15, :cond_1

    iget-wide v15, v8, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v17, 0x7f1002a4

    cmp-long v15, v15, v17

    if-nez v15, :cond_f

    :cond_1
    iget-wide v15, v8, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v17, 0x7f1002a2

    cmp-long v15, v15, v17

    if-nez v15, :cond_4

    const/4 v10, 0x1

    :goto_1
    if-eqz v10, :cond_2

    const v15, 0x7f0d00e0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/searchcommon/SearchConfig;->isTheGoogleDeployed()Z

    move-result v15

    if-eqz v15, :cond_3

    if-nez v2, :cond_5

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_4
    const/4 v10, 0x0

    goto :goto_1

    :cond_5
    invoke-interface {v12, v2}, Lcom/google/android/searchcommon/MarinerOptInSettings;->canAccountRunTheGoogle(Landroid/accounts/Account;)I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_9

    invoke-interface {v12, v2}, Lcom/google/android/searchcommon/MarinerOptInSettings;->getSavedConfiguration(Landroid/accounts/Account;)Lcom/google/geo/sidekick/Sidekick$Configuration;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-interface {v12, v6}, Lcom/google/android/searchcommon/MarinerOptInSettings;->localeIsBlockedFromNow(Lcom/google/geo/sidekick/Sidekick$Configuration;)Z

    move-result v15

    if-eqz v15, :cond_7

    :cond_6
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_7
    if-eqz v10, :cond_8

    const v15, 0x7f0d0297

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    const/4 v15, 0x0

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const/4 v15, 0x0

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    goto :goto_0

    :cond_8
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    :cond_9
    if-eqz v2, :cond_a

    invoke-interface {v12, v2}, Lcom/google/android/searchcommon/MarinerOptInSettings;->isAccountOptedIn(Landroid/accounts/Account;)Z

    move-result v15

    if-nez v15, :cond_e

    :cond_a
    if-eqz v10, :cond_d

    const v15, 0x7f0d00ca

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->createOptInIntent(Lcom/google/android/searchcommon/MarinerOptInSettings;)Landroid/content/Intent;

    move-result-object v15

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    const/4 v15, 0x0

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    :cond_b
    :goto_2
    iget-object v15, v8, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-eqz v15, :cond_c

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mInitialHeader:Landroid/preference/PreferenceActivity$Header;

    if-nez v15, :cond_c

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mInitialHeader:Landroid/preference/PreferenceActivity$Header;

    :cond_c
    iget-wide v15, v8, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v17, 0x7f1002a4

    cmp-long v15, v15, v17

    if-nez v15, :cond_0

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    if-eqz v14, :cond_12

    const v15, 0x7f0d00e2

    :goto_3
    iput v15, v8, Landroid/preference/PreferenceActivity$Header;->summaryRes:I

    goto/16 :goto_0

    :cond_d
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_e
    invoke-virtual {v13}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;->isSavedConfigurationVersionCurrent()Z

    move-result v15

    if-nez v15, :cond_b

    const/4 v15, 0x0

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    goto :goto_2

    :cond_f
    iget-wide v15, v8, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v17, 0x7f1002a6

    cmp-long v15, v15, v17

    if-nez v15, :cond_11

    invoke-virtual {v11}, Lcom/google/android/searchcommon/google/gaia/LoginHelper;->getAccountName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_10

    const v15, 0x7f0d00c9

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v8, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_10
    iput-object v3, v8, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_11
    iget-wide v15, v8, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v17, 0x7f1002a5

    cmp-long v15, v15, v17

    if-nez v15, :cond_b

    invoke-static {}, Lcom/google/android/searchcommon/debug/DebugFeatures;->getInstance()Lcom/google/android/searchcommon/debug/DebugFeatures;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/searchcommon/debug/DebugFeatures;->dogfoodDebugEnabled()Z

    move-result v15

    if-nez v15, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    :cond_12
    const v15, 0x7f0d00e3

    goto :goto_3

    :cond_13
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x4

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getMarinerOptInSettings()Lcom/google/android/searchcommon/MarinerOptInSettings;

    move-result-object v1

    new-instance v0, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-interface {v7}, Lcom/google/android/searchcommon/CoreSearchServices;->getLoginHelper()Lcom/google/android/searchcommon/google/gaia/LoginHelper;

    move-result-object v2

    invoke-direct {p0, v1}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->createOptInIntent(Lcom/google/android/searchcommon/MarinerOptInSettings;)Landroid/content/Intent;

    move-result-object v3

    const/4 v5, 0x0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;-><init>(Lcom/google/android/searchcommon/MarinerOptInSettings;Lcom/google/android/searchcommon/google/gaia/LoginHelper;Landroid/content/Intent;Landroid/app/Activity;Lcom/google/android/searchcommon/preferences/PredictiveCardsFragment;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->restoreInstanceState(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

    const-string v2, "sidekick"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const v3, 0x7f0d00b7

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNotificationsHandler:Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6, v8, v8}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->maybeInitializeDataManager()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f120006

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    new-instance v3, Lcom/google/android/velvet/Help;

    invoke-direct {v3, v0}, Lcom/google/android/velvet/Help;-><init>(Landroid/content/Context;)V

    const-string v4, "settings"

    invoke-virtual {v3, p1, v4}, Lcom/google/android/velvet/Help;->setHelpMenuItemIntent(Landroid/view/Menu;Ljava/lang/String;)V

    const v3, 0x7f1002b3

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v3, Lcom/google/android/velvet/ui/settings/SettingsActivity$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity$1;-><init>(Lcom/google/android/velvet/ui/settings/SettingsActivity;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/4 v3, 0x1

    return v3
.end method

.method public onGetInitialHeader()Landroid/preference/PreferenceActivity$Header;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mInitialHeader:Landroid/preference/PreferenceActivity$Header;

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNotificationsHandler:Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;->updateSwitchState()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;->saveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 6
    .param p1    # Landroid/widget/ListAdapter;

    if-nez p1, :cond_0

    const/4 v3, 0x0

    invoke-super {p0, v3}, Landroid/preference/PreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-interface {p1, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceActivity$Header;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    new-instance v3, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;

    iget-object v4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mOptOutHandler:Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;

    iget-object v5, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNotificationsHandler:Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;

    invoke-direct {v3, p0, v4, v5, v1}, Lcom/google/android/velvet/ui/settings/SettingsActivity$HeaderAdapter;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/preferences/OptOutSwitchHandler;Lcom/google/android/searchcommon/preferences/NotificationsSwitchHandler;Ljava/util/List;)V

    invoke-super {p0, v3}, Landroid/preference/PreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
