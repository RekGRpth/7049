.class Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Animations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/util/Animations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SetVisibilityOnAnimationEnd"
.end annotation


# instance fields
.field private mCancelled:Z

.field private final mView:Landroid/view/View;

.field private final mVisibility:I


# direct methods
.method constructor <init>(Landroid/view/View;I)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;->mView:Landroid/view/View;

    iput p2, p0, Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;->mVisibility:I

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;->mCancelled:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;->mCancelled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;->mView:Landroid/view/View;

    iget v1, p0, Lcom/google/android/velvet/ui/util/Animations$SetVisibilityOnAnimationEnd;->mVisibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method
