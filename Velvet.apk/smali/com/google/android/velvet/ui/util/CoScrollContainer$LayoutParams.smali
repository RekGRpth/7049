.class public Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
.super Landroid/widget/FrameLayout$LayoutParams;
.source "CoScrollContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/util/CoScrollContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;
    }
.end annotation


# instance fields
.field private mAnchor:Landroid/view/View;

.field private mAnchorMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field private mAnimationEndAction:Ljava/lang/Runnable;

.field private mAnimationEndTime:J

.field private mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

.field private final mEndAction:Ljava/lang/Runnable;

.field private mFillViewport:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field private mIsScrolling:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field private mOriginalConsumableMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field private mResolvedTopInScrollableArea:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field private mScrollConsumableMargin:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field private mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

.field private mSpuriousScrollAmount:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field private mTranslationType:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "regular"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "header"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "footer"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x4
                to = "header onwards"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x5
                to = "offscreen"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x6
                to = "draggable"
            .end subannotation
        }
    .end annotation
.end field

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/velvet/ui/util/CoScrollContainer;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .param p3    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$1;-><init>(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V

    iput-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mEndAction:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    sget-object v1, Lcom/google/android/googlequicksearchbox/R$styleable;->CoScrollContainer_Layout:[I

    invoke-virtual {p1, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mFillViewport:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/velvet/ui/util/CoScrollContainer;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer;

    const/4 v0, -0x1

    invoke-direct {p0, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    new-instance v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$1;-><init>(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mEndAction:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/velvet/ui/util/CoScrollContainer;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .param p2    # Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p0, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$1;-><init>(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V

    iput-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mEndAction:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    instance-of v1, p2, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I

    iput v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I

    iget-boolean v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mFillViewport:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mFillViewport:Z

    iget-object v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;

    iput-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchor:Landroid/view/View;

    iput-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchor:Landroid/view/View;

    iget v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchorMargin:I

    iput v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchorMargin:I

    iget-boolean v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z

    iget-object v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    iput-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    iget v1, v0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I

    iput v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchor:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchorMargin:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)J
    .locals 2
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-wide v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnimationEndTime:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;J)J
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnimationEndTime:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mEndAction:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnimationEndAction:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;Lcom/google/android/velvet/ui/util/CoScrollContainer;)Lcom/google/android/velvet/ui/util/CoScrollContainer;
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p1    # Lcom/google/android/velvet/ui/util/CoScrollContainer;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mFillViewport:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I

    return p1
.end method

.method static synthetic access$520(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p1    # I

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mResolvedTopInScrollableArea:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mSpuriousScrollAmount:I

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mSpuriousScrollAmount:I

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I

    return v0
.end method

.method static synthetic access$820(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;
    .param p1    # I

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mOriginalConsumableMargin:I

    return v0
.end method

.method private setParams(ILandroid/view/View;II)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I

    iput-object p2, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchor:Landroid/view/View;

    iput p3, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnchorMargin:I

    iput p4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I

    iput p4, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mOriginalConsumableMargin:I

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollConsumableMargin:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    # setter for: Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHaveScrollToConsume:Z
    invoke-static {v0, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->access$1802(Lcom/google/android/velvet/ui/util/CoScrollContainer;Z)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    # invokes: Lcom/google/android/velvet/ui/util/CoScrollContainer;->resolveChildTop(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V
    invoke-static {v0, p0}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->access$1900(Lcom/google/android/velvet/ui/util/CoScrollContainer;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    # invokes: Lcom/google/android/velvet/ui/util/CoScrollContainer;->syncChild(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V
    invoke-static {v0, p0, v2}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->access$2000(Lcom/google/android/velvet/ui/util/CoScrollContainer;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private setView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mView:Landroid/view/View;

    instance-of v0, p1, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    :goto_0
    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mScrollableChild:Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams$ScrollableChild;

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mIsScrolling:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I

    :cond_0
    return-void

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public adjustScrollToY(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    # invokes: Lcom/google/android/velvet/ui/util/CoScrollContainer;->adjustChildScrollToY(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    invoke-static {v0, p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->access$1700(Lcom/google/android/velvet/ui/util/CoScrollContainer;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    move-result p1

    :cond_0
    return p1
.end method

.method public consumeVerticalScroll(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    # invokes: Lcom/google/android/velvet/ui/util/CoScrollContainer;->consumeChildVerticalScroll(Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I
    invoke-static {v0, p0, p1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->access$1600(Lcom/google/android/velvet/ui/util/CoScrollContainer;Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;I)I

    move-result p1

    :cond_0
    return p1
.end method

.method public cropDrawingRectByPadding(Landroid/graphics/Rect;)V
    .locals 2
    .param p1    # Landroid/graphics/Rect;

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer;->mHeaderPadding:I
    invoke-static {v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->access$2100(Lcom/google/android/velvet/ui/util/CoScrollContainer;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mContainer:Lcom/google/android/velvet/ui/util/CoScrollContainer;

    # getter for: Lcom/google/android/velvet/ui/util/CoScrollContainer;->mFooterPadding:I
    invoke-static {v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer;->access$2200(Lcom/google/android/velvet/ui/util/CoScrollContainer;)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method public isOffscreen()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mTranslationType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAnimationEndAction(Ljava/lang/Runnable;)V
    .locals 0
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->mAnimationEndAction:Ljava/lang/Runnable;

    return-void
.end method

.method public setParams(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(ILandroid/view/View;II)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public setParams(Landroid/view/View;II)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/velvet/ui/util/CoScrollContainer$LayoutParams;->setParams(ILandroid/view/View;II)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
