.class public Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CustomStringArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mCustomValue:Ljava/lang/CharSequence;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mLastIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # [Ljava/lang/String;

    const-string v0, ""

    invoke-static {p3, v0}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->append([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    array-length v0, p3

    iput v0, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mLastIndex:I

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private static append([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p0    # [Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    array-length v1, p0

    add-int/lit8 v1, v1, 0x1

    invoke-static {p0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    array-length v1, p0

    aput-object p1, v0, v1

    return-object v0
.end method


# virtual methods
.method public getCustomValuePosition()I
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mLastIndex:I

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v2, 0x7f10008a

    const/4 v3, 0x0

    iget v1, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mLastIndex:I

    if-ne p1, v1, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v1, v2, :cond_0

    :goto_0
    return-object p2

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f040028

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04002f

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object p2, v0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mLastIndex:I

    if-ne p1, v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mCustomValue:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v0
.end method

.method public setCustomValue(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mCustomValue:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->mCustomValue:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/CustomStringArrayAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
