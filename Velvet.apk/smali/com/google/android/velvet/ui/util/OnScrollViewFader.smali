.class public Lcom/google/android/velvet/ui/util/OnScrollViewFader;
.super Ljava/lang/Object;
.source "OnScrollViewFader.java"

# interfaces
.implements Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;


# static fields
.field private static final DBG:Z = false

.field private static final TAG:Ljava/lang/String; = "Velvet.OnScrollViewFader"


# instance fields
.field private mCurrentMaxScrollY:I

.field private mCurrentScrollY:I

.field private mFadeBackgroundOnly:Z

.field private mFixedFade:Z

.field private mOpaqueAlpha:F

.field private mOpaqueAtScroll:I

.field private mTransparentAlpha:F

.field private mTransparentAtScroll:I

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/velvet/ui/util/ScrollViewControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mView:Landroid/view/View;

    invoke-interface {p2, p0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->addScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V

    invoke-interface {p2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentScrollY:I

    invoke-interface {p2}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->getMaxScrollY()I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentMaxScrollY:I

    return-void
.end method

.method private setAlpha(F)V
    .locals 3
    .param p1    # F

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mFadeBackgroundOnly:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    const/high16 v2, 0x43800000

    mul-float/2addr v2, p1

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private tag()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Velvet.OnScrollViewFader[FADE(H="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mFixedFade:Z

    if-eqz v0, :cond_0

    const-string v0, " FIXEDFADE"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " T@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " O@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private updateFade()V
    .locals 5

    const/high16 v0, 0x3f800000

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    if-le v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentScrollY:I

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    if-ge v1, v2, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAlpha:F

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAlpha:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAlpha:F

    add-float v0, v1, v2

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setAlpha(F)V

    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentScrollY:I

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentScrollY:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    iget v3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v1, v2

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    if-le v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentScrollY:I

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    if-ge v1, v2, :cond_3

    const/high16 v0, 0x3f800000

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentScrollY:I

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    if-ge v1, v2, :cond_4

    const/high16 v1, 0x3f800000

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentScrollY:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    iget v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float v0, v1, v2

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onScrollAnimationFinished()V
    .locals 0

    return-void
.end method

.method public onScrollChanged(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentScrollY:I

    iput p2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mCurrentMaxScrollY:I

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mFixedFade:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->updateFade()V

    :cond_0
    return-void
.end method

.method public onScrollFinished()V
    .locals 0

    return-void
.end method

.method public onScrollMarginConsumed(Landroid/view/View;II)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public setFadeBackgroundOnly(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mFadeBackgroundOnly:Z

    return-void
.end method

.method public setFadePoints(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setFadePoints(IIFF)V

    return-void
.end method

.method public setFadePoints(IIFF)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # F
    .param p4    # F

    iput p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAtScroll:I

    iput p2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAtScroll:I

    iput p3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mTransparentAlpha:F

    iput p4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mOpaqueAlpha:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mFixedFade:Z

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->updateFade()V

    return-void
.end method

.method public setFixedAlpha(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->mFixedFade:Z

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/util/OnScrollViewFader;->setAlpha(F)V

    return-void
.end method
