.class public Lcom/google/android/velvet/ui/util/OnScrollViewHider;
.super Ljava/lang/Object;
.source "OnScrollViewHider.java"

# interfaces
.implements Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;


# instance fields
.field private mAnimationStartDelay:I

.field private final mAtTop:Z

.field private mCurrentMaxScrollY:I

.field private mCurrentOffset:I

.field private mCurrentScrollY:I

.field private mFadeWithTranslation:Z

.field private mForceShowOrHideOnScrollFinishedDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

.field private mLocked:Z

.field private mMaxHeightHidden:I

.field private mOffsetFromEdge:I

.field private mRevealAtScrollEndDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

.field private mStickiness:I

.field private final mView:Landroid/view/View;

.field private mViewHeight:I


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/velvet/ui/util/ScrollViewControl;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/velvet/ui/util/ScrollViewControl;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mView:Landroid/view/View;

    iput-boolean p3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mAtTop:Z

    iput-object p0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mForceShowOrHideOnScrollFinishedDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iput-object p0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mRevealAtScrollEndDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mView:Landroid/view/View;

    new-instance v1, Lcom/google/android/velvet/ui/util/OnScrollViewHider$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider$1;-><init>(Lcom/google/android/velvet/ui/util/OnScrollViewHider;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    invoke-interface {p2, p0}, Lcom/google/android/velvet/ui/util/ScrollViewControl;->addScrollListener(Lcom/google/android/velvet/ui/util/ScrollViewControl$ScrollListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/ui/util/OnScrollViewHider;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    return v0
.end method

.method private allowOverscrollBounce()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mAtTop:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    iget v3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentMaxScrollY:I

    if-gt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private getAlphaToFadeWithTranslation(II)F
    .locals 3
    .param p1    # I
    .param p2    # I

    const/high16 v0, 0x3f800000

    neg-int v1, p2

    if-gt p1, v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-gez p1, :cond_0

    int-to-float v1, p1

    neg-int v2, p2

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method private getDistanceFromScrollEnd()I
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentMaxScrollY:I

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private getScrollDistanceFromEdge()I
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mAtTop:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentMaxScrollY:I

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method private isAtLeastHalfVisible()Z
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentOffset:I

    neg-int v0, v0

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isScrollDistanceNearToEnd(I)Z
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    add-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldHideOrShowOnScrollEnd()Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    if-eq v2, v0, :cond_0

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mAtTop:Z

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    if-eqz v2, :cond_1

    :cond_3
    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    iget v3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    add-int/2addr v2, v3

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->getScrollDistanceFromEdge()I

    move-result v3

    if-le v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method private updateTranslation(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mLocked:Z

    if-nez v7, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->getScrollDistanceFromEdge()I

    move-result v6

    const/4 v5, 0x0

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    if-gez v7, :cond_a

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    neg-int v5, v7

    :cond_2
    :goto_1
    iget v2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mMaxHeightHidden:I

    if-eqz v7, :cond_3

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mMaxHeightHidden:I

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_3
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    const/4 v8, 0x3

    if-eq v7, v8, :cond_4

    and-int/lit8 v7, p2, 0x4

    if-eqz v7, :cond_b

    :cond_4
    add-int v7, v2, v5

    neg-int v3, v7

    :cond_5
    :goto_2
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentOffset:I

    if-eq v3, v7, :cond_0

    move v0, v3

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->allowOverscrollBounce()Z

    move-result v7

    if-nez v7, :cond_6

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_6
    iget-boolean v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mAtTop:Z

    if-nez v7, :cond_7

    neg-int v0, v0

    :cond_7
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentOffset:I

    sub-int v7, v3, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-le v7, v8, :cond_8

    and-int/lit8 v7, p2, 0x1

    if-eqz v7, :cond_10

    :cond_8
    iget-object v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mView:Landroid/view/View;

    int-to-float v8, v0

    invoke-virtual {v7, v8}, Landroid/view/View;->setTranslationY(F)V

    iget-boolean v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mFadeWithTranslation:Z

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mView:Landroid/view/View;

    invoke-direct {p0, v3, v2}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->getAlphaToFadeWithTranslation(II)F

    move-result v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setAlpha(F)V

    :cond_9
    :goto_3
    iput v3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentOffset:I

    goto :goto_0

    :cond_a
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    iget v8, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentMaxScrollY:I

    if-le v7, v8, :cond_2

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    iget v8, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentMaxScrollY:I

    sub-int v5, v7, v8

    goto :goto_1

    :cond_b
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_c

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    sub-int v4, v7, v6

    neg-int v7, v2

    add-int v8, v4, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_2

    :cond_c
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    if-le v7, v6, :cond_d

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    sub-int/2addr v7, v6

    add-int v3, v7, v5

    goto :goto_2

    :cond_d
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    const/4 v8, 0x1

    if-eq v7, v8, :cond_e

    and-int/lit8 v7, p2, 0x2

    if-eqz v7, :cond_f

    :cond_e
    move v3, v5

    goto :goto_2

    :cond_f
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentOffset:I

    add-int v3, v7, p1

    add-int v7, v2, v5

    neg-int v7, v7

    invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_5

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->getDistanceFromScrollEnd()I

    move-result v7

    add-int/2addr v7, v5

    neg-int v7, v7

    invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto/16 :goto_2

    :cond_10
    iget-object v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    int-to-float v8, v0

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-boolean v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mFadeWithTranslation:Z

    if-eqz v7, :cond_11

    invoke-direct {p0, v3, v2}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->getAlphaToFadeWithTranslation(II)F

    move-result v7

    invoke-virtual {v1, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    :cond_11
    iget v7, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mAnimationStartDelay:I

    int-to-long v7, v7

    invoke-virtual {v1, v7, v8}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_3
.end method


# virtual methods
.method public onScrollAnimationFinished()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->onScrollFinished()V

    return-void
.end method

.method public onScrollChanged(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    if-ne p1, v1, :cond_0

    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentMaxScrollY:I

    if-eq p2, v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    sub-int v0, v1, p1

    iput p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    iput p2, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentMaxScrollY:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->updateTranslation(II)V

    :cond_1
    return-void
.end method

.method public onScrollFinished()V
    .locals 6

    const/4 v3, 0x4

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->shouldHideOrShowOnScrollEnd()Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    if-gtz v4, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    const/4 v3, 0x2

    :cond_0
    invoke-direct {p0, v2, v3}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->updateTranslation(II)V

    :cond_1
    return-void

    :cond_2
    iget v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    if-ne v4, v3, :cond_5

    invoke-direct {p0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->getDistanceFromScrollEnd()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mForceShowOrHideOnScrollFinishedDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-direct {v4}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->isAtLeastHalfVisible()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mRevealAtScrollEndDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-direct {v4, v0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->isScrollDistanceNearToEnd(I)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/4 v1, 0x1

    :goto_1
    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    iget v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentScrollY:I

    iget v5, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentMaxScrollY:I

    if-lt v4, v5, :cond_6

    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mForceShowOrHideOnScrollFinishedDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    invoke-direct {v4}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->isAtLeastHalfVisible()Z

    move-result v1

    goto :goto_0
.end method

.method public onScrollMarginConsumed(Landroid/view/View;II)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method protected onViewHeightChanged(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    if-eq v3, p1, :cond_0

    iget v3, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    if-nez v3, :cond_1

    move v0, v1

    :goto_0
    iput p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mViewHeight:I

    if-eqz v0, :cond_2

    :goto_1
    invoke-direct {p0, v2, v1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->updateTranslation(II)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public setAnimationStartDelay(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mAnimationStartDelay:I

    return-void
.end method

.method public setFadeWithTranslation(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mFadeWithTranslation:Z

    return-void
.end method

.method public setForceShowOrHideOnScrollFinishedDelegate(Lcom/google/android/velvet/ui/util/OnScrollViewHider;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mForceShowOrHideOnScrollFinishedDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    return-void
.end method

.method public setLocked(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    iput-boolean p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mLocked:Z

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mLocked:Z

    if-nez v0, :cond_0

    invoke-direct {p0, v1, v1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->updateTranslation(II)V

    :cond_0
    return-void
.end method

.method public setOffsetFromEdge(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mOffsetFromEdge:I

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->updateTranslation(II)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public setPartialHide(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mMaxHeightHidden:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mMaxHeightHidden:I

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mMaxHeightHidden:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mCurrentOffset:I

    neg-int v1, p1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->updateTranslation(II)V

    :cond_0
    return-void
.end method

.method public setRevealAtScrollEndDelegate(Lcom/google/android/velvet/ui/util/OnScrollViewHider;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mRevealAtScrollEndDelegate:Lcom/google/android/velvet/ui/util/OnScrollViewHider;

    return-void
.end method

.method public setStickiness(IZZ)V
    .locals 6
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    if-eq p1, v4, :cond_1

    iget v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    if-ne v4, v3, :cond_2

    move v0, v1

    :goto_0
    iput p1, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    iget-boolean v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mFadeWithTranslation:Z

    if-nez v4, :cond_0

    if-eqz p2, :cond_0

    iget-object v4, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mView:Landroid/view/View;

    const/high16 v5, 0x3f800000

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    if-eqz v0, :cond_3

    if-eqz p3, :cond_3

    :goto_1
    if-eqz p2, :cond_4

    :goto_2
    or-int/2addr v1, v3

    invoke-direct {p0, v2, v1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->updateTranslation(II)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public show()V
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->mStickiness:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/velvet/ui/util/OnScrollViewHider;->updateTranslation(II)V

    :cond_1
    return-void
.end method
