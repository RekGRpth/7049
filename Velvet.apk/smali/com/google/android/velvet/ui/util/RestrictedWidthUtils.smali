.class public Lcom/google/android/velvet/ui/util/RestrictedWidthUtils;
.super Ljava/lang/Object;
.source "RestrictedWidthUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getRestrictedWidthMeasureSpec(Landroid/content/res/Resources;IZ)I
    .locals 8
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I
    .param p2    # Z

    const/high16 v7, 0x40000000

    const v5, 0x7f0c0047

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v3, v5

    const v5, 0x7f0b0047

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const v5, 0x7f0c0048

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v1, v5

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    if-ne v5, v7, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    if-eqz p2, :cond_1

    mul-int/lit8 v5, v1, 0x2

    sub-int v5, v4, v5

    mul-int v6, v4, v2

    div-int/lit8 v6, v6, 0x64

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_1
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    return v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    mul-int v5, v4, v2

    div-int/lit8 v5, v5, 0x64

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v5

    mul-int/lit8 v6, v1, 0x2

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1
.end method
