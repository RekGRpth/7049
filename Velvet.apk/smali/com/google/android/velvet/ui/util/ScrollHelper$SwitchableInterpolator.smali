.class Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;
.super Ljava/lang/Object;
.source "ScrollHelper.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/util/ScrollHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SwitchableInterpolator"
.end annotation


# instance fields
.field private mUseInterpolator:Landroid/animation/TimeInterpolator;

.field private final mViscousFluidNormalize:F


# direct methods
.method constructor <init>()V
    .locals 2

    const/high16 v1, 0x3f800000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x41000000

    invoke-static {v1, v1, v0}, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->viscousFluid(FFF)F

    move-result v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->mViscousFluidNormalize:F

    return-void
.end method

.method private static viscousFluid(FFF)F
    .locals 4
    .param p0    # F
    .param p1    # F
    .param p2    # F

    const/high16 v3, 0x3f800000

    mul-float/2addr p0, p2

    cmpg-float v1, p0, v3

    if-gez v1, :cond_0

    neg-float v1, p0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float v1, v3, v1

    sub-float/2addr p0, v1

    :goto_0
    mul-float/2addr p0, p1

    return p0

    :cond_0
    const v0, 0x3ebc5ab2

    sub-float v1, v3, p0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    sub-float p0, v3, v1

    sub-float v1, v3, v0

    mul-float/2addr v1, p0

    add-float p0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 2
    .param p1    # F

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->mUseInterpolator:Landroid/animation/TimeInterpolator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->mUseInterpolator:Landroid/animation/TimeInterpolator;

    invoke-interface {v0, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->mViscousFluidNormalize:F

    const/high16 v1, 0x41000000

    invoke-static {p1, v0, v1}, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->viscousFluid(FFF)F

    move-result v0

    goto :goto_0
.end method

.method public setInterpolatorOverride(Landroid/animation/TimeInterpolator;)V
    .locals 0
    .param p1    # Landroid/animation/TimeInterpolator;

    iput-object p1, p0, Lcom/google/android/velvet/ui/util/ScrollHelper$SwitchableInterpolator;->mUseInterpolator:Landroid/animation/TimeInterpolator;

    return-void
.end method
