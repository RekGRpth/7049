.class Lcom/google/android/velvet/ui/widget/CorpusBar$1;
.super Ljava/lang/Object;
.source "CorpusBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/ui/widget/CorpusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/widget/CorpusBar;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/widget/CorpusBar;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar$1;->this$0:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar$1;->this$0:Lcom/google/android/velvet/ui/widget/CorpusBar;

    # getter for: Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->access$000(Lcom/google/android/velvet/ui/widget/CorpusBar;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar$1;->this$0:Lcom/google/android/velvet/ui/widget/CorpusBar;

    # invokes: Lcom/google/android/velvet/ui/widget/CorpusBar;->moreCorporaClicked()V
    invoke-static {v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->access$100(Lcom/google/android/velvet/ui/widget/CorpusBar;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar$1;->this$0:Lcom/google/android/velvet/ui/widget/CorpusBar;

    # getter for: Lcom/google/android/velvet/ui/widget/CorpusBar;->mPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;
    invoke-static {v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->access$200(Lcom/google/android/velvet/ui/widget/CorpusBar;)Lcom/google/android/velvet/presenter/FooterPresenter;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/Corpus;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/FooterPresenter;->onCorpusClicked(Lcom/google/android/velvet/Corpus;)V

    goto :goto_0
.end method
