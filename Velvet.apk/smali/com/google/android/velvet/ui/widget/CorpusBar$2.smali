.class Lcom/google/android/velvet/ui/widget/CorpusBar$2;
.super Ljava/lang/Object;
.source "CorpusBar.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/widget/CorpusBar;->addCorpusSelector(Lcom/google/android/velvet/Corpus;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/Consumer",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/widget/CorpusBar;

.field final synthetic val$corpusIconView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/widget/CorpusBar;Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar$2;->this$0:Lcom/google/android/velvet/ui/widget/CorpusBar;

    iput-object p2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar$2;->val$corpusIconView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar$2;->val$corpusIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/widget/CorpusBar$2;->consume(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method
