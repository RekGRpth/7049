.class public Lcom/google/android/velvet/ui/widget/CorpusBar;
.super Landroid/widget/LinearLayout;
.source "CorpusBar.java"


# instance fields
.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mCurrentlySelectedCorpus:Lcom/google/android/velvet/Corpus;

.field private final mHiddenCorpora:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/velvet/Corpus;",
            ">;"
        }
    .end annotation
.end field

.field private mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/searchcommon/util/UriLoader",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mInitialMaxWebSelectors:I

.field private mMoreCorporaSelector:Landroid/view/View;

.field private mPendingScrollToView:Landroid/view/View;

.field private mPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

.field private mShowAllCorpora:Z

.field private final mTmpRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mHiddenCorpora:Ljava/util/Set;

    new-instance v0, Lcom/google/android/velvet/ui/widget/CorpusBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/widget/CorpusBar$1;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mHiddenCorpora:Ljava/util/Set;

    new-instance v0, Lcom/google/android/velvet/ui/widget/CorpusBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/widget/CorpusBar$1;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mHiddenCorpora:Ljava/util/Set;

    new-instance v0, Lcom/google/android/velvet/ui/widget/CorpusBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/widget/CorpusBar$1;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/ui/widget/CorpusBar;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/widget/CorpusBar;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/ui/widget/CorpusBar;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->moreCorporaClicked()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/velvet/ui/widget/CorpusBar;)Lcom/google/android/velvet/presenter/FooterPresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/widget/CorpusBar;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/ui/widget/CorpusBar;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/ui/widget/CorpusBar;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->scrollParentTo(Landroid/view/View;)V

    return-void
.end method

.method private moreCorporaClicked()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mShowAllCorpora:Z

    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->updateSelectorVisibility()V

    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mInitialMaxWebSelectors:I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mInitialMaxWebSelectors:I

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    return-void
.end method

.method private scrollParentTo(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->isLayoutRequested()Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPendingScrollToView:Landroid/view/View;

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/velvet/ui/widget/CorpusBar;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    sub-int v2, v1, v2

    div-int/lit8 v0, v2, 0x2

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v0

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mTmpRect:Landroid/graphics/Rect;

    invoke-interface {v2, p0, v3, v5}, Landroid/view/ViewParent;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    goto :goto_0
.end method

.method private updateSelectorVisibility()V
    .locals 7

    const/16 v4, 0x8

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_5

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;

    if-ne v2, v3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mShowAllCorpora:Z

    if-eqz v3, :cond_0

    move v3, v4

    :goto_1
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v3, v5

    goto :goto_1

    :cond_1
    iget-boolean v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mShowAllCorpora:Z

    if-nez v3, :cond_2

    iget v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mInitialMaxWebSelectors:I

    if-lt v1, v3, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mHiddenCorpora:Ljava/util/Set;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_4
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_5
    return-void
.end method


# virtual methods
.method public addCorpusSelector(Lcom/google/android/velvet/Corpus;Z)V
    .locals 11
    .param p1    # Lcom/google/android/velvet/Corpus;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/google/android/velvet/Corpus;->getSelectorLayoutId()I

    move-result v8

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v8, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v8}, Lcom/google/android/velvet/presenter/FooterPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v8, v9, p0, p1}, Lcom/google/android/velvet/VelvetFactory;->createCorpusSelector(Lcom/google/android/velvet/presenter/FooterPresenter;Landroid/view/ViewGroup;Lcom/google/android/velvet/Corpus;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v8, 0x7f10007a

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/android/velvet/Corpus;->getIconUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    iget-object v8, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-interface {v8, v4}, Lcom/google/android/searchcommon/util/UriLoader;->load(Landroid/net/Uri;)Lcom/google/android/searchcommon/util/CancellableNowOrLater;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->haveNow()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v3}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_1
    const v8, 0x7f10007b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/velvet/Corpus;->getNameUri()Landroid/net/Uri;

    move-result-object v6

    if-eqz v1, :cond_2

    if-eqz v6, :cond_2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v6}, Lcom/google/android/searchcommon/util/Util;->getResourceId(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v5

    iget-object v8, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Landroid/content/res/Resources;

    iget-object v9, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p2, :cond_3

    iget-object v8, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;

    if-nez v8, :cond_6

    :cond_3
    invoke-virtual {p0, v7}, Lcom/google/android/velvet/ui/widget/CorpusBar;->addView(Landroid/view/View;)V

    :goto_3
    iget-object v8, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mHiddenCorpora:Ljava/util/Set;

    invoke-interface {v8, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->updateSelectorVisibility()V

    goto :goto_0

    :cond_5
    new-instance v8, Lcom/google/android/velvet/ui/widget/CorpusBar$2;

    invoke-direct {v8, p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar$2;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;Landroid/widget/ImageView;)V

    invoke-interface {v3, v8}, Lcom/google/android/searchcommon/util/CancellableNowOrLater;->getLater(Lcom/google/android/searchcommon/util/Consumer;)V

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v8, "Velvet.CorpusBar"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Couldn\'t get name for corpus "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    iget-object v8, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;

    invoke-virtual {p0, v8}, Lcom/google/android/velvet/ui/widget/CorpusBar;->indexOfChild(Landroid/view/View;)I

    move-result v8

    invoke-virtual {p0, v7, v8}, Lcom/google/android/velvet/ui/widget/CorpusBar;->addView(Landroid/view/View;I)V

    goto :goto_3
.end method

.method public onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getImageLoader()Lcom/google/android/searchcommon/util/UriLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mImageLoader:Lcom/google/android/searchcommon/util/UriLoader;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getMaxInitialWebCorpusSelectors()I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mInitialMaxWebSelectors:I

    iget v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mInitialMaxWebSelectors:I

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mShowAllCorpora:Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPendingScrollToView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPendingScrollToView:Landroid/view/View;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPendingScrollToView:Landroid/view/View;

    new-instance v1, Lcom/google/android/velvet/ui/widget/CorpusBar$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar$3;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;Landroid/view/View;)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public removeCorpusSelectors(Lcom/google/android/velvet/Corpus;)V
    .locals 4
    .param p1    # Lcom/google/android/velvet/Corpus;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    :goto_0
    if-ltz v1, :cond_3

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/Corpus;

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/Corpus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/google/android/velvet/Corpus;->getParent()Lcom/google/android/velvet/Corpus;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/velvet/Corpus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/velvet/ui/widget/CorpusBar;->removeView(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public resetSelectedCorpus()V
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mInitialMaxWebSelectors:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mShowAllCorpora:Z

    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->updateSelectorVisibility()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPresenter(Lcom/google/android/velvet/presenter/FooterPresenter;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/presenter/FooterPresenter;

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mInitialMaxWebSelectors:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/FooterPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mPresenter:Lcom/google/android/velvet/presenter/FooterPresenter;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/velvet/VelvetFactory;->createMoreCorporaSelector(Lcom/google/android/velvet/presenter/FooterPresenter;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mMoreCorporaSelector:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setSelectedCorpus(Lcom/google/android/velvet/Corpus;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Corpus;

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mCurrentlySelectedCorpus:Lcom/google/android/velvet/Corpus;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mCurrentlySelectedCorpus:Lcom/google/android/velvet/Corpus;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/Corpus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mCurrentlySelectedCorpus:Lcom/google/android/velvet/Corpus;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mCurrentlySelectedCorpus:Lcom/google/android/velvet/Corpus;

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    :cond_1
    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mCurrentlySelectedCorpus:Lcom/google/android/velvet/Corpus;

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mCurrentlySelectedCorpus:Lcom/google/android/velvet/Corpus;

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    iput-boolean v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mShowAllCorpora:Z

    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->updateSelectorVisibility()V

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->scrollParentTo(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mCurrentlySelectedCorpus:Lcom/google/android/velvet/Corpus;

    goto :goto_0
.end method
