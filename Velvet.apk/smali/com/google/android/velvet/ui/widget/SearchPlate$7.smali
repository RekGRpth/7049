.class Lcom/google/android/velvet/ui/widget/SearchPlate$7;
.super Ljava/lang/Object;
.source "SearchPlate.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/velvet/ui/widget/SearchPlate;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/ui/widget/SearchPlate;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate$7;->this$0:Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v1, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate$7;->this$0:Lcom/google/android/velvet/ui/widget/SearchPlate;

    # getter for: Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->access$200(Lcom/google/android/velvet/ui/widget/SearchPlate;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate$7;->this$0:Lcom/google/android/velvet/ui/widget/SearchPlate;

    # getter for: Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->access$200(Lcom/google/android/velvet/ui/widget/SearchPlate;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    :cond_0
    return-void
.end method
