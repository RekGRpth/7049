.class public Lcom/google/android/velvet/ui/widget/SearchPlate;
.super Landroid/widget/RelativeLayout;
.source "SearchPlate.java"


# instance fields
.field private mClearButton:Landroid/widget/ImageButton;

.field private mCommitMode:Z

.field private final mFadeInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field private mInputManager:Landroid/view/inputmethod/InputMethodManager;

.field private mLogo:Landroid/view/View;

.field private mLogoProgressContainer:Landroid/view/View;

.field private mLogoVisible:Z

.field private mPresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

.field private mProgressIndicator:Landroid/view/View;

.field private mProgressVisible:Z

.field private mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

.field private final mSlideInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field private mVoiceSearchButton:Landroid/widget/ImageButton;

.field private mVoiceSearchButtonDrawable:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40200000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSlideInterpolator:Landroid/view/animation/DecelerateInterpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mFadeInterpolator:Landroid/view/animation/DecelerateInterpolator;

    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/ui/widget/SearchPlate;)Lcom/google/android/searchcommon/ui/SimpleSearchText;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/widget/SearchPlate;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/ui/widget/SearchPlate;)Lcom/google/android/velvet/presenter/SearchPlatePresenter;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/widget/SearchPlate;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mPresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/ui/widget/SearchPlate;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/widget/SearchPlate;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/velvet/ui/widget/SearchPlate;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/google/android/velvet/ui/widget/SearchPlate;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mClearButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private hideView(Landroid/view/View;JZ)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # J
    .param p4    # Z

    const/4 v1, 0x0

    if-eqz p4, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mFadeInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/velvet/ui/widget/SearchPlate$8;

    invoke-direct {v1, p0, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate$8;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method private showView(Landroid/view/View;J)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # J

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mFadeInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private slideLeft(Landroid/view/View;IJZZ)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # J
    .param p5    # Z
    .param p6    # Z

    const/high16 v4, 0x3f800000

    if-eqz p6, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    neg-int v1, p2

    int-to-float v1, v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    if-eqz p5, :cond_0

    invoke-virtual {p1, v4}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    neg-int v2, p2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSlideInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p5, :cond_0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method private slideRight(Landroid/view/View;IJZLjava/lang/Runnable;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # J
    .param p5    # Z
    .param p6    # Ljava/lang/Runnable;

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v2, p2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSlideInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p6}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p5, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method private slideToCommitMode()V
    .locals 7

    const-wide/16 v3, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mCommitMode:Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    move-object v0, p0

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideRight(Landroid/view/View;IJZLjava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    move-object v0, p0

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideRight(Landroid/view/View;IJZLjava/lang/Runnable;)V

    return-void
.end method

.method private slideToEditMode(JZ)V
    .locals 7
    .param p1    # J
    .param p3    # Z

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mCommitMode:Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    move-object v0, p0

    move-wide v3, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideLeft(Landroid/view/View;IJZZ)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    move-object v0, p0

    move-wide v3, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideLeft(Landroid/view/View;IJZZ)V

    return-void
.end method


# virtual methods
.method public focusSearchBox()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public hideFocus()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method public hideKeyboard()Z
    .locals 4

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v2}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    move-result v0

    return v0
.end method

.method public hideLogo(Z)V
    .locals 5
    .param p1    # Z

    const-wide/16 v3, 0x0

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mCommitMode:Z

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    if-eqz v2, :cond_2

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressIndicator:Landroid/view/View;

    invoke-direct {p0, v2, v3, v4, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideView(Landroid/view/View;JZ)V

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoVisible:Z

    if-eqz v2, :cond_3

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoVisible:Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogo:Landroid/view/View;

    invoke-direct {p0, v1, v3, v4, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideView(Landroid/view/View;JZ)V

    :cond_3
    invoke-direct {p0, v3, v4, p1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideToEditMode(JZ)V

    goto :goto_1
.end method

.method public hideProgress()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressIndicator:Landroid/view/View;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideView(Landroid/view/View;JZ)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoVisible:Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogo:Landroid/view/View;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showView(Landroid/view/View;J)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f1000e9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/ui/SimpleSearchText;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    const v0, 0x7f10010a

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mClearButton:Landroid/widget/ImageButton;

    const v0, 0x7f10010b

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mVoiceSearchButton:Landroid/widget/ImageButton;

    const v0, 0x7f10020b

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressIndicator:Landroid/view/View;

    const v0, 0x7f10020c

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogo:Landroid/view/View;

    const v0, 0x7f100108

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressIndicator:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoVisible:Z

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    new-instance v1, Lcom/google/android/velvet/ui/widget/SearchPlate$1;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/widget/SearchPlate$1;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->addQueryTextWatcher(Lcom/google/android/searchcommon/util/TextChangeWatcher;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    new-instance v1, Lcom/google/android/velvet/ui/widget/SearchPlate$2;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/widget/SearchPlate$2;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    new-instance v1, Lcom/google/android/velvet/ui/widget/SearchPlate$3;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/widget/SearchPlate$3;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    new-instance v1, Lcom/google/android/velvet/ui/widget/SearchPlate$4;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/widget/SearchPlate$4;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mVoiceSearchButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/velvet/ui/widget/SearchPlate$5;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/widget/SearchPlate$5;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mClearButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/velvet/ui/widget/SearchPlate$6;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/widget/SearchPlate$6;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    new-instance v1, Lcom/google/android/velvet/ui/widget/SearchPlate$7;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/ui/widget/SearchPlate$7;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f0200ca

    iput v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mVoiceSearchButtonDrawable:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/google/android/velvet/ui/widget/SearchPlate;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setHintText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setHint(I)V

    return-void
.end method

.method public setPresenter(Lcom/google/android/velvet/presenter/SearchPlatePresenter;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mPresenter:Lcom/google/android/velvet/presenter/SearchPlatePresenter;

    return-void
.end method

.method public setQuery(Lcom/google/android/velvet/Query;)V
    .locals 8
    .param p1    # Lcom/google/android/velvet/Query;

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getAlternateSpans()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getAlternateSpans()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getRequestId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSuggestionLogger()Lcom/google/android/speech/logger/SuggestionLogger;

    move-result-object v7

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/speech/alternates/Suggestions;->getSuggestionSpanForSearchBox(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/speech/logger/SuggestionLogger;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v3, v1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setQuery(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getSelection()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v3, "Velvet.SearchPlate"

    const-string v4, "Couldn\'t generate alternates"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/velvet/Query;->getQueryString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_0
    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->selectAll()V

    goto :goto_1

    :sswitch_1
    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setSelection(I)V

    goto :goto_1

    :sswitch_2
    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    iget-object v4, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v4}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setSelection(I)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_1
        0x200 -> :sswitch_2
        0x300 -> :sswitch_0
    .end sparse-switch
.end method

.method public setTextQueryCorrections(Landroid/text/Spanned;)V
    .locals 1
    .param p1    # Landroid/text/Spanned;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->setTextQueryCorrections(Landroid/text/Spanned;)V

    return-void
.end method

.method public showClearButton(Z)V
    .locals 7
    .param p1    # Z

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mClearButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mClearButton:Landroid/widget/ImageButton;

    move-object v0, p0

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideLeft(Landroid/view/View;IJZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mClearButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mClearButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    new-instance v6, Lcom/google/android/velvet/ui/widget/SearchPlate$9;

    invoke-direct {v6, p0}, Lcom/google/android/velvet/ui/widget/SearchPlate$9;-><init>(Lcom/google/android/velvet/ui/widget/SearchPlate;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideRight(Landroid/view/View;IJZLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public showKeyboard()Z
    .locals 4

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public showLogo()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mCommitMode:Z

    if-nez v3, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideToCommitMode()V

    :cond_0
    iget-boolean v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoVisible:Z

    if-nez v3, :cond_1

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoVisible:Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogo:Landroid/view/View;

    invoke-direct {p0, v1, v4, v5}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showView(Landroid/view/View;J)V

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    if-eqz v1, :cond_2

    iput-boolean v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressIndicator:Landroid/view/View;

    invoke-direct {p0, v1, v4, v5, v2}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideView(Landroid/view/View;JZ)V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public showProgress()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mCommitMode:Z

    if-nez v3, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/SearchPlate;->slideToCommitMode()V

    :cond_0
    iget-boolean v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoVisible:Z

    if-eqz v3, :cond_1

    iput-boolean v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogoVisible:Z

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mLogo:Landroid/view/View;

    invoke-direct {p0, v3, v4, v5, v2}, Lcom/google/android/velvet/ui/widget/SearchPlate;->hideView(Landroid/view/View;JZ)V

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    if-nez v2, :cond_2

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressVisible:Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mProgressIndicator:Landroid/view/View;

    invoke-direct {p0, v1, v4, v5}, Lcom/google/android/velvet/ui/widget/SearchPlate;->showView(Landroid/view/View;J)V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public showTextQueryMode()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mVoiceSearchButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200ca

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->showTextQueryMode()V

    return-void
.end method

.method public showVoiceQueryMode()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mVoiceSearchButton:Landroid/widget/ImageButton;

    const v1, 0x7f020189

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SearchPlate;->mSearchBox:Lcom/google/android/searchcommon/ui/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/ui/SimpleSearchText;->showVoiceQueryMode()V

    return-void
.end method
