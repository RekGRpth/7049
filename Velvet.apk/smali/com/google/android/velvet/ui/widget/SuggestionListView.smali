.class public Lcom/google/android/velvet/ui/widget/SuggestionListView;
.super Landroid/widget/FrameLayout;
.source "SuggestionListView.java"

# interfaces
.implements Lcom/google/android/searchcommon/suggest/SuggestionsUi;


# instance fields
.field private mColumnCount:I

.field private mCountView:Landroid/widget/TextView;

.field private mFactory:Lcom/google/android/velvet/VelvetFactory;

.field private mFooterView:Landroid/view/View;

.field private mFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

.field private mHeaderView:Landroid/view/View;

.field private mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

.field private mNumNonSuggestionViews:I

.field private mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

.field private mShowAllDividers:Z

.field private mSuggestionClickListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

.field private mSuggestionList:Lcom/google/android/searchcommon/suggest/SuggestionList;

.field private mSuggestionViewTypeCount:I

.field private mTitleView:Landroid/widget/TextView;

.field private mViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v3, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mColumnCount:I

    sget-object v1, Lcom/google/android/googlequicksearchbox/R$styleable;->SuggestionListView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mColumnCount:I

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mShowAllDividers:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private getViewType(Landroid/view/View;)I
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    check-cast p1, Lcom/google/android/searchcommon/ui/SuggestionView;

    invoke-interface {p1}, Lcom/google/android/searchcommon/ui/SuggestionView;->getBoundSuggestion()Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/VelvetFactory;->getSuggestionViewType(Lcom/google/android/searchcommon/suggest/Suggestion;Z)I

    move-result v0

    return v0
.end method

.method private hasDuplicateIcons(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z
    .locals 11
    .param p1    # Lcom/google/android/searchcommon/suggest/SuggestionList;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-interface {p1}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v4

    if-gt v4, v8, :cond_1

    :cond_0
    :goto_0
    return v9

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v9}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->isContactSuggestion()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v0

    :cond_2
    const/4 v3, 0x1

    :goto_1
    if-ge v3, v4, :cond_8

    invoke-interface {p1, v3}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v6

    if-eqz v6, :cond_3

    if-eqz v0, :cond_4

    invoke-virtual {v6}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    move v2, v8

    :goto_2
    if-nez v0, :cond_5

    move v7, v8

    :goto_3
    invoke-virtual {v6}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_6

    move v10, v8

    :goto_4
    if-eq v7, v10, :cond_7

    move v5, v8

    :goto_5
    if-nez v5, :cond_0

    if-eqz v2, :cond_3

    invoke-virtual {v6}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    move v2, v9

    goto :goto_2

    :cond_5
    move v7, v9

    goto :goto_3

    :cond_6
    move v10, v9

    goto :goto_4

    :cond_7
    move v5, v9

    goto :goto_5

    :cond_8
    move v9, v8

    goto :goto_0
.end method

.method private isEndOfRow(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x1

    add-int/lit8 v2, p2, -0x1

    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v2, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    sub-int v2, p1, v2

    add-int/lit8 v0, v2, 0x1

    iget v2, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mColumnCount:I

    rem-int v2, v0, v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private layoutChildAt(Landroid/view/View;II)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p3

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method private prepareSuggestionView(Lcom/google/android/searchcommon/suggest/Suggestion;Landroid/view/View;Ljava/lang/String;ZZZZ)Landroid/view/View;
    .locals 6
    .param p1    # Lcom/google/android/searchcommon/suggest/Suggestion;
    .param p2    # Landroid/view/View;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z
    .param p7    # Z

    const/4 v3, 0x2

    const/4 v4, -0x1

    const/4 v2, 0x0

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iget-object v5, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    invoke-virtual {v1, v5, p1, v2, p0}, Lcom/google/android/velvet/VelvetFactory;->createSuggestionView(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/searchcommon/suggest/Suggestion;ZLandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object v0, p2

    check-cast v0, Lcom/google/android/searchcommon/ui/SuggestionView;

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/ui/SuggestionView;->setSuggestionFormatter(Lcom/google/android/searchcommon/suggest/SuggestionFormatter;)V

    if-eqz p4, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/searchcommon/suggest/Suggestion;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    move v3, v2

    :cond_1
    invoke-interface {v0, v1, v3}, Lcom/google/android/searchcommon/ui/SuggestionView;->setIconModes(II)V

    invoke-interface {v0, p1, p3, p6}, Lcom/google/android/searchcommon/ui/SuggestionView;->bindAsSuggestion(Lcom/google/android/searchcommon/suggest/Suggestion;Ljava/lang/String;Z)Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionClickListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-interface {v0, v1, v3}, Lcom/google/android/searchcommon/ui/SuggestionView;->setClickListener(Lcom/google/android/searchcommon/ui/SuggestionClickListener;Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;)V

    invoke-interface {v0, p5}, Lcom/google/android/searchcommon/ui/SuggestionView;->setEnabled(Z)V

    invoke-interface {v0, v2}, Lcom/google/android/searchcommon/ui/SuggestionView;->setDividerVisibility(I)V

    invoke-virtual {p2, v4}, Landroid/view/View;->setNextFocusDownId(I)V

    if-eqz p7, :cond_3

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    :goto_1
    invoke-virtual {p2, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    return-object p2

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1
.end method

.method private updateLastRow()V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildCount()I

    move-result v5

    iget v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    sub-int v4, v5, v6

    if-lez v4, :cond_2

    add-int/lit8 v5, v4, -0x1

    iget v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mColumnCount:I

    div-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mColumnCount:I

    mul-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    add-int v2, v5, v6

    const/4 v3, -0x1

    iget-object v5, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v3

    :cond_0
    move v1, v2

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setNextFocusDownId(I)V

    check-cast v0, Lcom/google/android/searchcommon/ui/SuggestionView;

    iget-boolean v5, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mShowAllDividers:Z

    if-eqz v5, :cond_1

    const/4 v5, 0x0

    :goto_1
    invoke-interface {v0, v5}, Lcom/google/android/searchcommon/ui/SuggestionView;->setDividerVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/16 v5, 0x8

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public init(Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;Lcom/google/android/searchcommon/suggest/SuggestionFormatter;Lcom/google/android/velvet/ui/util/ViewRecycler;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionFormatter;
    .param p3    # Lcom/google/android/velvet/ui/util/ViewRecycler;

    invoke-virtual {p1}, Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;->getFactory()Lcom/google/android/velvet/VelvetFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mPresenter:Lcom/google/android/velvet/presenter/VelvetFragmentPresenter;

    iput-object p2, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFormatter:Lcom/google/android/searchcommon/suggest/SuggestionFormatter;

    iput-object p3, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetFactory;->getNumSuggestionViewTypes()I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionViewTypeCount:I

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f100255

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    const v0, 0x7f100258

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    const v0, 0x7f100256

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mTitleView:Landroid/widget/TextView;

    const v0, 0x7f100257

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mCountView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/16 v7, 0x8

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getPaddingTop()I

    move-result v2

    iget-object v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    invoke-direct {p0, v6, v1, v2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->layoutChildAt(Landroid/view/View;II)V

    iget-object v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v2, v6

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildCount()I

    move-result v4

    const/4 v5, 0x0

    iget v3, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    :goto_0
    if-ge v3, v4, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->layoutChildAt(Landroid/view/View;II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-direct {p0, v3, v4}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->isEndOfRow(II)Z

    move-result v6

    if-eqz v6, :cond_1

    add-int/2addr v2, v5

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getPaddingLeft()I

    move-result v1

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v1, v6

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eq v6, v7, :cond_3

    iget-object v6, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-direct {p0, v6, v1, v2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->layoutChildAt(Landroid/view/View;II)V

    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    const/high16 v11, 0x40000000

    if-ne v9, v11, :cond_2

    const/4 v11, 0x1

    :goto_0
    invoke-static {v11}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getPaddingTop()I

    move-result v2

    iget-object v11, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-eq v11, v12, :cond_0

    iget-object v11, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {p0, v11, p1, p2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->measureChild(Landroid/view/View;II)V

    iget-object v11, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mHeaderView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v2, v11

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildCount()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getPaddingLeft()I

    move-result v11

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getPaddingRight()I

    move-result v12

    add-int v1, v11, v12

    sub-int v11, v10, v1

    iget v12, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mColumnCount:I

    div-int/2addr v11, v12

    add-int/2addr v11, v1

    const/high16 v12, 0x40000000

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    const/4 v7, 0x0

    iget v5, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    :goto_1
    if-ge v5, v6, :cond_3

    invoke-virtual {p0, v5}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, v8, p2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->measureChild(Landroid/view/View;II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    invoke-static {v11, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-direct {p0, v5, v6}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->isEndOfRow(II)Z

    move-result v11

    if-eqz v11, :cond_1

    add-int/2addr v2, v7

    const/4 v7, 0x0

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    :cond_3
    iget-object v11, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-eq v11, v12, :cond_4

    iget-object v11, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-virtual {p0, v11, p1, p2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->measureChild(Landroid/view/View;II)V

    iget-object v11, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v2, v11

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getPaddingBottom()I

    move-result v11

    add-int/2addr v2, v11

    const/high16 v11, 0x40000000

    if-ne v3, v11, :cond_6

    move v2, v4

    :cond_5
    :goto_2
    invoke-virtual {p0, v10, v2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setMeasuredDimension(II)V

    return-void

    :cond_6
    const/high16 v11, -0x80000000

    if-ne v3, v11, :cond_5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_2
.end method

.method public setCountText(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mCountView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mCountView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setFooterClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setFooterVisibility(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFooterView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->updateLastRow()V

    :cond_0
    return-void
.end method

.method public setSuggestionClickListener(Lcom/google/android/searchcommon/ui/SuggestionClickListener;)V
    .locals 2
    .param p1    # Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionClickListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t reset click listener"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionClickListener:Lcom/google/android/searchcommon/ui/SuggestionClickListener;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mTitleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public showSuggestions(Ljava/lang/Object;Lcom/google/android/searchcommon/suggest/SuggestionList;IZZ)V
    .locals 26
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/google/android/searchcommon/suggest/SuggestionList;
    .param p3    # I
    .param p4    # Z
    .param p5    # Z

    invoke-interface/range {p2 .. p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getCount()I

    move-result v3

    move/from16 v0, p3

    if-gt v0, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    if-nez p3, :cond_1

    const/16 v3, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionList:Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-object/from16 v23, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionList:Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionList:Lcom/google/android/searchcommon/suggest/SuggestionList;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->hasDuplicateIcons(Lcom/google/android/searchcommon/suggest/SuggestionList;)Z

    move-result v21

    new-instance v3, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {v3, v0, v1}, Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;-><init>(Lcom/google/android/searchcommon/suggest/SuggestionList;I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mLogInfo:Lcom/google/android/searchcommon/suggest/SuggestionList$LogInfo;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/searchcommon/suggest/SuggestionList;->getUserQuery()Lcom/google/android/velvet/Query;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/velvet/Query;->getQueryStringForSuggest()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildCount()I

    move-result v20

    const/16 v22, 0x0

    :goto_2
    move/from16 v0, v22

    move/from16 v1, p3

    if-ge v0, v1, :cond_8

    add-int/lit8 v3, v22, 0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mColumnCount:I

    rem-int/2addr v3, v8

    if-nez v3, :cond_3

    const/4 v10, 0x1

    :goto_3
    if-eqz v21, :cond_2

    if-nez v22, :cond_4

    :cond_2
    const/4 v7, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionList:Lcom/google/android/searchcommon/suggest/SuggestionList;

    move/from16 v0, v22

    invoke-interface {v3, v0}, Lcom/google/android/searchcommon/suggest/SuggestionList;->get(I)Lcom/google/android/searchcommon/suggest/Suggestion;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mFactory:Lcom/google/android/velvet/VelvetFactory;

    const/4 v8, 0x0

    invoke-virtual {v3, v4, v8}, Lcom/google/android/velvet/VelvetFactory;->getSuggestionViewType(Lcom/google/android/searchcommon/suggest/Suggestion;Z)I

    move-result v24

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    add-int v25, v3, v22

    move/from16 v0, v25

    move/from16 v1, v20

    if-ge v0, v1, :cond_5

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    :goto_5
    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getViewType(Landroid/view/View;)I

    move-result v3

    move/from16 v0, v24

    if-ne v3, v0, :cond_6

    move-object/from16 v3, p0

    move/from16 v8, p4

    move/from16 v9, p5

    invoke-direct/range {v3 .. v10}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->prepareSuggestionView(Lcom/google/android/searchcommon/suggest/Suggestion;Landroid/view/View;Ljava/lang/String;ZZZZ)Landroid/view/View;

    :goto_6
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    :cond_3
    const/4 v10, 0x0

    goto :goto_3

    :cond_4
    const/4 v7, 0x0

    goto :goto_4

    :cond_5
    const/4 v5, 0x0

    goto :goto_5

    :cond_6
    if-eqz v5, :cond_7

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->removeViewAt(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getViewType(Landroid/view/View;)I

    move-result v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionViewTypeCount:I

    invoke-virtual {v3, v5, v8, v9}, Lcom/google/android/velvet/ui/util/ViewRecycler;->releaseView(Landroid/view/View;II)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Lcom/google/android/velvet/ui/util/ViewRecycler;->getView(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v11, p0

    move-object v12, v4

    move-object v14, v6

    move v15, v7

    move/from16 v16, p4

    move/from16 v17, p5

    move/from16 v18, v10

    invoke-direct/range {v11 .. v18}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->prepareSuggestionView(Lcom/google/android/searchcommon/suggest/Suggestion;Landroid/view/View;Ljava/lang/String;ZZZZ)Landroid/view/View;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->addView(Landroid/view/View;I)V

    goto :goto_6

    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    sub-int v3, v20, v3

    add-int/lit8 v22, v3, -0x1

    :goto_7
    move/from16 v0, v22

    move/from16 v1, p3

    if-lt v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mNumNonSuggestionViews:I

    add-int v25, v3, v22

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->removeViewAt(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mViewRecycler:Lcom/google/android/velvet/ui/util/ViewRecycler;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->getViewType(Landroid/view/View;)I

    move-result v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/velvet/ui/widget/SuggestionListView;->mSuggestionViewTypeCount:I

    move-object/from16 v0, v19

    invoke-virtual {v3, v0, v8, v9}, Lcom/google/android/velvet/ui/util/ViewRecycler;->releaseView(Landroid/view/View;II)V

    add-int/lit8 v22, v22, -0x1

    goto :goto_7

    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->updateLastRow()V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/velvet/ui/widget/SuggestionListView;->setVisibility(I)V

    goto/16 :goto_1
.end method
