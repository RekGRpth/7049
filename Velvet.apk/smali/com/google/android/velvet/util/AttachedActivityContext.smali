.class public Lcom/google/android/velvet/util/AttachedActivityContext;
.super Landroid/content/ContextWrapper;
.source "AttachedActivityContext.java"


# instance fields
.field private mAttachedToActivity:Landroid/app/Activity;

.field private mHaveView:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    instance-of v0, p1, Landroid/app/Activity;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Expected an application context"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/velvet/util/AttachedActivityContext;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/util/AttachedActivityContext;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/util/AttachedActivityContext;->setActivityFromView(Landroid/view/View;)V

    return-void
.end method

.method private setActivityFromView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Landroid/app/Activity;

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    move-object p1, v0

    check-cast p1, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    :goto_1
    iput-object v1, p0, Lcom/google/android/velvet/util/AttachedActivityContext;->mAttachedToActivity:Landroid/app/Activity;

    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/util/AttachedActivityContext;->mAttachedToActivity:Landroid/app/Activity;

    if-eqz v0, :cond_1

    const-string v0, "window"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/util/AttachedActivityContext;->mAttachedToActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getTheme()Landroid/content/res/Resources$Theme;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/util/AttachedActivityContext;->mAttachedToActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/util/AttachedActivityContext;->mAttachedToActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/content/ContextWrapper;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    goto :goto_0
.end method

.method public setView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/velvet/util/AttachedActivityContext;->mHaveView:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iput-boolean v1, p0, Lcom/google/android/velvet/util/AttachedActivityContext;->mHaveView:Z

    new-instance v0, Lcom/google/android/velvet/util/AttachedActivityContext$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/util/AttachedActivityContext$1;-><init>(Lcom/google/android/velvet/util/AttachedActivityContext;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
