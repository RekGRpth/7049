.class public Lcom/google/android/velvet/util/BeamHelper;
.super Ljava/lang/Object;
.source "BeamHelper.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;
    }
.end annotation


# instance fields
.field private mProvider:Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mVersionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/velvet/util/BeamHelper;->mVersionCode:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/velvet/util/BeamHelper;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 1
    .param p1    # Landroid/nfc/NfcEvent;

    iget-object v0, p0, Lcom/google/android/velvet/util/BeamHelper;->mProvider:Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/util/BeamHelper;->mProvider:Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

    invoke-interface {v0, p0}, Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;->createNdefMessage(Lcom/google/android/velvet/util/BeamHelper;)Landroid/nfc/NdefMessage;

    move-result-object v0

    goto :goto_0
.end method

.method public createWebSearchMessage(Lcom/google/android/velvet/Query;)Landroid/nfc/NdefMessage;
    .locals 5
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/velvet/util/BeamHelper;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getBeamUrlFromQuery(Lcom/google/android/velvet/Query;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Landroid/nfc/NdefMessage;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/nfc/NdefRecord;

    const/4 v3, 0x0

    invoke-static {v0}, Landroid/nfc/NdefRecord;->createUri(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v1, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0
.end method

.method public initialize(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-static {p1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/app/Activity;

    invoke-virtual {v0, p0, p1, v1}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public registerProvider(Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

    iget-object v0, p0, Lcom/google/android/velvet/util/BeamHelper;->mProvider:Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

    if-eqz v0, :cond_0

    :cond_0
    iput-object p1, p0, Lcom/google/android/velvet/util/BeamHelper;->mProvider:Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

    return-void
.end method

.method public unregisterProvider(Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

    iget-object v0, p0, Lcom/google/android/velvet/util/BeamHelper;->mProvider:Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/util/BeamHelper;->mProvider:Lcom/google/android/velvet/util/BeamHelper$BeamDataProvider;

    :cond_0
    return-void
.end method
