.class public abstract Lcom/google/android/velvet/util/JavascriptInterfaceHelper;
.super Ljava/lang/Object;
.source "JavascriptInterfaceHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Eval;,
        Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;
    }
.end annotation


# instance fields
.field private mConditions:I

.field private final mDebug:Z

.field private final mInterfaceName:Ljava/lang/String;

.field private final mJsCallBuilder:Ljava/lang/StringBuilder;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    iput-object p1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mWebView:Landroid/webkit/WebView;

    iput-object p2, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mInterfaceName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->addJavaScriptInterface(Landroid/webkit/WebView;Ljava/lang/String;)V

    iput-boolean p3, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mDebug:Z

    return-void
.end method

.method static synthetic access$000(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # [Ljava/lang/Object;

    invoke-static {p0, p1}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->appendArgsTo(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V

    return-void
.end method

.method private static appendArgTo(Ljava/lang/StringBuilder;Ljava/lang/Object;)V
    .locals 7
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Ljava/lang/Object;

    if-nez p1, :cond_0

    const-string v4, "null"

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-void

    :cond_0
    instance-of v4, p1, Ljava/util/Map;

    if-eqz v4, :cond_3

    move-object v3, p1

    check-cast v3, Ljava/util/Map;

    const-string v4, "["

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->appendArgTo(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    const-string v4, ","

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->appendArgTo(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    const-string v4, ","

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    const-string v4, "]"

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    instance-of v4, p1, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    instance-of v4, p1, Ljava/lang/Boolean;

    if-nez v4, :cond_5

    instance-of v4, p1, Ljava/lang/Integer;

    if-nez v4, :cond_5

    instance-of v4, p1, Ljava/lang/Double;

    if-nez v4, :cond_5

    instance-of v4, p1, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Eval;

    if-eqz v4, :cond_6

    :cond_5
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_6
    instance-of v4, p1, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;

    if-eqz v4, :cond_7

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_7
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addJsCallWithArgs() does not support argument type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private static varargs appendArgsTo(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V
    .locals 6
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # [Ljava/lang/Object;

    const/4 v2, 0x1

    move-object v1, p1

    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    invoke-static {p0, v0}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->appendArgTo(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/16 v5, 0x2c

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public static varargs callback(Ljava/lang/String;Ljava/lang/String;I[Ljava/lang/Object;)Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # [Ljava/lang/Object;

    new-instance v0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;-><init>(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method protected addElse()V
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mConditions:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    const-string v1, "} else {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract addJavaScriptInterface(Landroid/webkit/WebView;Ljava/lang/String;)V
.end method

.method protected varargs addJsCallWithArgs(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    invoke-static {v0, p2}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->appendArgsTo(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public callJs(Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;)V
    .locals 5
    .param p1    # Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;

    const/4 v2, 0x0

    iget v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mConditions:I

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    const-string v3, "javascript:(function(){"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "window[\'google\']&&window[\'google\'][\'x\']"

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->startCondition(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "google.x({id:\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mInterfaceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'}, function() {"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    const-string v3, "});"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->addElse()V

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {p1, v3}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper$Callback;->callNow([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->endCondition()V

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    const-string v3, ";})()"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->loadJsString(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mWebView:Landroid/webkit/WebView;

    return-void
.end method

.method protected endCondition()V
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mConditions:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mConditions:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mConditions:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadJsString(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mDebug:Z

    if-eqz v0, :cond_0

    const-string v0, "Velvet.JavascriptInterfaceHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LOADING: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected startCondition(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mJsCallBuilder:Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "if ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mConditions:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->mConditions:I

    return-void
.end method
