.class public Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;
.super Lcom/google/android/velvet/util/JavascriptInterfaceHelper;
.source "GsaCommunicationJsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$1;,
        Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$JsBridge;
    }
.end annotation


# instance fields
.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mJsEventController:Lcom/google/android/velvet/presenter/JsEventController;

.field private mResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Lcom/google/android/searchcommon/SearchConfig;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-virtual {p2}, Lcom/google/android/searchcommon/SearchConfig;->getVelvetGsaBridgeInterfaceName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;-><init>(Landroid/webkit/WebView;Ljava/lang/String;Z)V

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mResults:Ljava/util/Map;

    iput-object p2, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    new-instance v0, Lcom/google/android/velvet/presenter/JsEventController;

    invoke-direct {v0, p3}, Lcom/google/android/velvet/presenter/JsEventController;-><init>(Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mJsEventController:Lcom/google/android/velvet/presenter/JsEventController;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->read(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;)Lcom/google/android/velvet/presenter/JsEventController;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mJsEventController:Lcom/google/android/velvet/presenter/JsEventController;

    return-object v0
.end method

.method private read(Ljava/lang/String;)Ljava/util/Map;
    .locals 9
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v6, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mResults:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->clear()V

    new-instance v4, Landroid/util/JsonReader;

    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    :try_start_0
    invoke-virtual {v4}, Landroid/util/JsonReader;->beginObject()V

    :cond_0
    :goto_0
    invoke-virtual {v4}, Landroid/util/JsonReader;->hasNext()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_1

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v6

    sget-object v7, Landroid/util/JsonToken;->NAME:Landroid/util/JsonToken;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v6, v7, :cond_2

    :cond_1
    :try_start_2
    invoke-virtual {v4}, Landroid/util/JsonReader;->endObject()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    :goto_1
    iget-object v6, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mResults:Ljava/util/Map;

    return-object v6

    :cond_2
    :try_start_3
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$1;->$SwitchMap$android$util$JsonToken:[I

    invoke-virtual {v4}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v7

    invoke-virtual {v7}, Landroid/util/JsonToken;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    invoke-virtual {v4}, Landroid/util/JsonReader;->skipValue()V

    :goto_2
    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mResults:Ljava/util/Map;

    invoke-interface {v6, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    const-string v6, "GsaCommunicationJsHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception parsing JS event: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_5
    const-string v6, "GsaCommunicationJsHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception parsing JS events: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :pswitch_0
    :try_start_6
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v5

    goto :goto_2

    :pswitch_1
    :try_start_7
    invoke-virtual {v4}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v5

    goto :goto_2

    :catch_2
    move-exception v3

    :try_start_8
    invoke-virtual {v4}, Landroid/util/JsonReader;->skipValue()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    :catch_3
    move-exception v1

    :try_start_9
    const-string v6, "GsaCommunicationJsHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception parsing JS event: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    invoke-static {v4}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected addJavaScriptInterface(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$JsBridge;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$JsBridge;-><init>(Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;Lcom/google/android/velvet/webview/GsaCommunicationJsHelper$1;)V

    invoke-virtual {p1, v0, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public dispose()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mJsEventController:Lcom/google/android/velvet/presenter/JsEventController;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/JsEventController;->dispose()V

    invoke-super {p0}, Lcom/google/android/velvet/util/JavascriptInterfaceHelper;->dispose()V

    return-void
.end method

.method public registerHandler(Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mJsEventController:Lcom/google/android/velvet/presenter/JsEventController;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/JsEventController;->registerHandler(Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;)V

    return-void
.end method

.method public registerJsBridge()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getRegisterGsaBridgeJavascript()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->loadJsString(Ljava/lang/String;)V

    return-void
.end method
