.class Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "GsaWebViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/webview/GsaWebViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GsaWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/webview/GsaWebViewController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/webview/GsaWebViewController;Lcom/google/android/velvet/webview/GsaWebViewController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p2    # Lcom/google/android/velvet/webview/GsaWebViewController$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalPageFinished(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1000(Lcom/google/android/velvet/webview/GsaWebViewController;Landroid/net/Uri;)V

    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalPageStarted(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$900(Lcom/google/android/velvet/webview/GsaWebViewController;Landroid/net/Uri;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$700(Lcom/google/android/velvet/webview/GsaWebViewController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1100(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/Query;

    move-result-object v1

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalPageError(ILcom/google/android/velvet/Query;Ljava/lang/String;)V
    invoke-static {v0, p2, v1, p3}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1200(Lcom/google/android/velvet/webview/GsaWebViewController;ILcom/google/android/velvet/Query;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/SslErrorHandler;
    .param p3    # Landroid/net/http/SslError;

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 10
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    if-eqz p2, :cond_1

    const-string v7, "http"

    invoke-virtual {p2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClientLock:Ljava/lang/Object;
    invoke-static {v7}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1600(Lcom/google/android/velvet/webview/GsaWebViewController;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    :try_start_0
    iget-object v8, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchLoadUrl:Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1700(Lcom/google/android/velvet/webview/GsaWebViewController;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchLoadUrl:Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1700(Lcom/google/android/velvet/webview/GsaWebViewController;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchResult:Lcom/google/android/velvet/prefetch/SearchResultPage;
    invoke-static {v8}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1800(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchQuery:Lcom/google/android/velvet/Query;
    invoke-static {v8}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1900(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/Query;

    move-result-object v3

    :goto_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    if-eqz v2, :cond_6

    :try_start_1
    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v7}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1300(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/searchcommon/SearchConfig;->getInterceptedRequestHeaderTimeoutMs()I

    move-result v7

    int-to-long v7, v7

    invoke-interface {v2, v7, v8}, Lcom/google/android/velvet/prefetch/SearchResultPage;->waitUntilHeadersAvailable(J)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->toWebResourceResponse()Landroid/webkit/WebResourceResponse;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->setCookiesFromPrefetchedPage(Ljava/lang/String;Ljava/util/Map;)Z
    invoke-static {v7, p2, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$2000(Lcom/google/android/velvet/webview/GsaWebViewController;Ljava/lang/String;Ljava/util/Map;)Z

    move-result v7

    if-eqz v7, :cond_2

    new-instance v5, Lcom/google/android/velvet/webview/WebViewInputStream;

    invoke-virtual {v4}, Landroid/webkit/WebResourceResponse;->getData()Ljava/io/InputStream;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v8}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$2100(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    invoke-static {v9}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$2200(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v9

    invoke-direct {v5, v3, v7, v8, v9}, Lcom/google/android/velvet/webview/WebViewInputStream;-><init>(Lcom/google/android/velvet/Query;Ljava/io/InputStream;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v4, v5}, Landroid/webkit/WebResourceResponse;->setData(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    return-object v4

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    goto :goto_0

    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    :try_start_3
    const-string v7, "Velvet.GsaWebViewController"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not set cookies for page: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_3
    invoke-interface {v2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->cancel()V

    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v7}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$2100(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v7

    const/16 v8, 0x1f

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    const/4 v8, 0x0

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->loadFromCacheFailed(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/Exception;)V
    invoke-static {v7, p2, v3, v8}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$2300(Lcom/google/android/velvet/webview/GsaWebViewController;Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/Exception;)V

    :cond_4
    :goto_4
    move-object v4, v6

    goto :goto_2

    :cond_5
    const-string v7, "Velvet.GsaWebViewController"

    const-string v8, "Timed out getting headers"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    if-eqz v3, :cond_7

    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->loadFromCacheFailed(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/Exception;)V
    invoke-static {v7, p2, v3, v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$2300(Lcom/google/android/velvet/webview/GsaWebViewController;Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/Exception;)V

    goto :goto_4

    :cond_6
    :try_start_4
    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I
    invoke-static {v7}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$400(Lcom/google/android/velvet/webview/GsaWebViewController;)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_4

    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalReadyToShow()V
    invoke-static {v7}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$500(Lcom/google/android/velvet/webview/GsaWebViewController;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_4

    :cond_7
    const-string v7, "Velvet.GsaWebViewController"

    const-string v8, "Could not set exception to QueryState because there is no Query"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    const-string v1, "about:blank"

    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;
    invoke-static {v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1300(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/searchcommon/SearchConfig;->isGoogleSearchLogoutRedirect(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;
    invoke-static {v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1400(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/webview/WebViewControllerClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onLogoutRedirect()V

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    # invokes: Lcom/google/android/velvet/webview/GsaWebViewController;->shouldOverrideWebViewClick(Landroid/net/Uri;)Z
    invoke-static {v1, v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->access$1500(Lcom/google/android/velvet/webview/GsaWebViewController;Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method
