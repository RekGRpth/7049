.class Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;
.super Ljava/lang/Object;
.source "GsaWebViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/webview/GsaWebViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalEvent"
.end annotation


# instance fields
.field private mErrorCode:I

.field private mErrorDescription:Ljava/lang/String;

.field private mEvent:I

.field private mQuery:Lcom/google/android/velvet/Query;

.field private mUrl:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/velvet/webview/GsaWebViewController;


# direct methods
.method private constructor <init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->this$0:Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/velvet/webview/GsaWebViewController;Lcom/google/android/velvet/webview/GsaWebViewController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p2    # Lcom/google/android/velvet/webview/GsaWebViewController$1;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;

    iget v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I

    return v0
.end method

.method static synthetic access$2402(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;I)I
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I

    return p1
.end method

.method static synthetic access$2500(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query;
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;
    .param p1    # Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mQuery:Lcom/google/android/velvet/Query;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;

    iget v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mErrorCode:I

    return v0
.end method

.method static synthetic access$2702(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;I)I
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;
    .param p1    # I

    iput p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mErrorCode:I

    return p1
.end method

.method static synthetic access$2800(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mErrorDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mErrorDescription:Ljava/lang/String;

    return-object p1
.end method

.method private eventString()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UNKNOWN("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "ERROR"

    goto :goto_0

    :pswitch_1
    const-string v0, "STARTED"

    goto :goto_0

    :pswitch_2
    const-string v0, "PROGRESS"

    goto :goto_0

    :pswitch_3
    const-string v0, "FINISHED"

    goto :goto_0

    :pswitch_4
    const-string v0, "INTERCEPT"

    goto :goto_0

    :pswitch_5
    const-string v0, "READY_TO_SHOW"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->eventString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
