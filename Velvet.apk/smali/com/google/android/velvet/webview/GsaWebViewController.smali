.class public Lcom/google/android/velvet/webview/GsaWebViewController;
.super Ljava/lang/Object;
.source "GsaWebViewController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;,
        Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;
    }
.end annotation


# instance fields
.field private final mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mCommittedQuery:Lcom/google/android/velvet/Query;

.field private final mConfig:Lcom/google/android/searchcommon/SearchConfig;

.field private final mCookies:Lcom/google/android/velvet/Cookies;

.field private final mEndPreviousResultsSuppression:Ljava/lang/Runnable;

.field private final mGeolocationDir:Ljava/io/File;

.field private mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

.field private final mHandleInternalEvents:Ljava/lang/Runnable;

.field private mHasPendingHistoryClear:Z

.field private final mInternalEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;",
            ">;"
        }
    .end annotation
.end field

.field private mLastLoadHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLastLoadUrl:Ljava/lang/String;

.field private mLastPage:Landroid/net/Uri;

.field private mLastPrefetchLoadUrl:Ljava/lang/String;

.field private mLastPrefetchQuery:Lcom/google/android/velvet/Query;

.field private mLastPrefetchResult:Lcom/google/android/velvet/prefetch/SearchResultPage;

.field private mLoadState:I

.field private final mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

.field private mNumInternalEvents:I

.field private final mPictureListener:Landroid/webkit/WebView$PictureListener;

.field private mPreviousResultsSuppressedUntil:J

.field private final mQueryObserver:Lcom/google/android/velvet/presenter/QueryState$Observer;

.field private final mQueryState:Lcom/google/android/velvet/presenter/QueryState;

.field private final mSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private mSuppressingPreviousResults:Z

.field private final mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private final mUserAgent:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWebView:Landroid/webkit/WebView;

.field private mWebViewAccessAllowed:Z

.field private final mWebViewClient:Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;

.field private final mWebViewClientLock:Ljava/lang/Object;

.field private mWebViewInUse:Z


# direct methods
.method public constructor <init>(Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/velvet/webview/WebViewControllerClient;Lcom/google/android/velvet/presenter/QueryState;Lcom/google/android/searchcommon/google/LocationSettings;Ljava/util/concurrent/Executor;Lcom/google/common/base/Supplier;Lcom/google/android/velvet/Cookies;Landroid/content/Context;)V
    .locals 3
    .param p1    # Lcom/google/android/searchcommon/SearchConfig;
    .param p2    # Lcom/google/android/searchcommon/SearchSettings;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p5    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p6    # Lcom/google/android/velvet/webview/WebViewControllerClient;
    .param p7    # Lcom/google/android/velvet/presenter/QueryState;
    .param p8    # Lcom/google/android/searchcommon/google/LocationSettings;
    .param p9    # Ljava/util/concurrent/Executor;
    .param p11    # Lcom/google/android/velvet/Cookies;
    .param p12    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/searchcommon/SearchConfig;",
            "Lcom/google/android/searchcommon/SearchSettings;",
            "Lcom/google/android/searchcommon/util/Clock;",
            "Lcom/google/android/searchcommon/google/SearchUrlHelper;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "Lcom/google/android/velvet/webview/WebViewControllerClient;",
            "Lcom/google/android/velvet/presenter/QueryState;",
            "Lcom/google/android/searchcommon/google/LocationSettings;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/velvet/Cookies;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/velvet/webview/GsaWebViewController$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/webview/GsaWebViewController$1;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHandleInternalEvents:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/webview/GsaWebViewController$2;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/webview/GsaWebViewController$2;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mEndPreviousResultsSuppression:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/velvet/webview/GsaWebViewController$3;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/webview/GsaWebViewController$3;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryObserver:Lcom/google/android/velvet/presenter/QueryState$Observer;

    new-instance v0, Lcom/google/android/velvet/webview/GsaWebViewController$4;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/webview/GsaWebViewController$4;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClientLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    iput-object p2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iput-object p5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p8, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    iput-object p6, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    iput-object p10, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUserAgent:Lcom/google/common/base/Supplier;

    iput-object p11, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastLoadHeaders:Ljava/util/Map;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    new-instance v0, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;Lcom/google/android/velvet/webview/GsaWebViewController$1;)V

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClient:Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;

    iput v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I

    iput-object p7, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryObserver:Lcom/google/android/velvet/presenter/QueryState$Observer;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->addObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    const-string v0, "webview_geolocation"

    invoke-virtual {p12, v0, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mGeolocationDir:Ljava/io/File;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/webview/GsaWebViewController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/webview/GsaWebViewController;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleInternalEvents()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/velvet/webview/GsaWebViewController;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalPageFinished(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/velvet/webview/GsaWebViewController;ILcom/google/android/velvet/Query;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p1    # I
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalPageError(ILcom/google/android/velvet/Query;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/searchcommon/SearchConfig;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/webview/WebViewControllerClient;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/velvet/webview/GsaWebViewController;Landroid/net/Uri;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/webview/GsaWebViewController;->shouldOverrideWebViewClick(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/velvet/webview/GsaWebViewController;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClientLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/velvet/webview/GsaWebViewController;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchLoadUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/prefetch/SearchResultPage;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchResult:Lcom/google/android/velvet/prefetch/SearchResultPage;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/velvet/webview/GsaWebViewController;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSuppressingPreviousResults:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/velvet/webview/GsaWebViewController;Ljava/lang/String;Ljava/util/Map;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Map;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/webview/GsaWebViewController;->setCookiesFromPrefetchedPage(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/velvet/webview/GsaWebViewController;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSuppressingPreviousResults:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/velvet/presenter/QueryState;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/velvet/webview/GsaWebViewController;)Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/velvet/webview/GsaWebViewController;Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/Exception;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/webview/GsaWebViewController;->loadFromCacheFailed(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/velvet/webview/GsaWebViewController;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleStateChange()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/velvet/webview/GsaWebViewController;)I
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/velvet/webview/GsaWebViewController;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalReadyToShow()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/velvet/webview/GsaWebViewController;)Z
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;

    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/velvet/webview/GsaWebViewController;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p1    # Ljava/util/Map;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleAgsaEvents(Ljava/util/Map;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/velvet/webview/GsaWebViewController;Landroid/net/Uri;)V
    .locals 0
    .param p0    # Lcom/google/android/velvet/webview/GsaWebViewController;
    .param p1    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalPageStarted(Landroid/net/Uri;)V

    return-void
.end method

.method private dumpUrlAndHeaders(Ljava/lang/String;Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/PrintWriter;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Requested URL: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {p3}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    if-eqz p4, :cond_0

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Loaded URL: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {p4}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private getLoadedUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private handleAgsaEvents(Ljava/util/Map;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "agsai"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->onJsModuleInitialized(Landroid/net/Uri;)V

    :cond_0
    const-string v0, "agsase"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "agsase"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->onJsEventId(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private handleInternalEvents()V
    .locals 14

    const/4 v13, 0x3

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x0

    iget-boolean v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z

    if-eqz v9, :cond_9

    const/4 v7, 0x0

    const/4 v2, 0x0

    :goto_0
    iget v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    if-ge v2, v9, :cond_3

    iget-object v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2400(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I

    move-result v9

    if-ne v9, v13, :cond_0

    const/4 v7, 0x1

    :cond_0
    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2400(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I

    move-result v9

    const/4 v10, 0x4

    if-ne v9, v10, :cond_2

    if-nez v1, :cond_1

    move-object v1, v0

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2400(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I

    move-result v9

    if-nez v9, :cond_a

    move-object v3, v0

    const/4 v4, 0x0

    :cond_3
    if-eqz v7, :cond_4

    iget-object v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    invoke-virtual {v9}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->registerJsBridge()V

    :cond_4
    if-eqz v5, :cond_5

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;
    invoke-static {v5}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2500(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleLoadStarted(Landroid/net/Uri;)V

    :cond_5
    if-eqz v4, :cond_d

    iget-object v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v10, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPage:Landroid/net/Uri;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;
    invoke-static {v4}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2500(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->shouldAllowBackBetween(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v9

    if-nez v9, :cond_6

    iget-boolean v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    iput-boolean v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHasPendingHistoryClear:Z

    :cond_6
    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;
    invoke-static {v4}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2500(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPage:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->handlePendingClearHistory()V

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;
    invoke-static {v4}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2500(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleLoadFinished(Landroid/net/Uri;)V

    :cond_7
    :goto_2
    if-eqz v1, :cond_8

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2500(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Landroid/net/Uri;

    move-result-object v8

    if-eqz v8, :cond_e

    iget-object v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    invoke-interface {v9, v8}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onLinkClicked(Landroid/net/Uri;)V

    :cond_8
    :goto_3
    if-eqz v6, :cond_9

    iget v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_9

    invoke-direct {p0, v13}, Lcom/google/android/velvet/webview/GsaWebViewController;->setLoadState(I)V

    :cond_9
    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->releaseInternalEvents()V

    return-void

    :cond_a
    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2400(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_b

    move-object v5, v0

    const/4 v4, 0x0

    const/4 v3, 0x0

    goto :goto_1

    :cond_b
    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2400(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I

    move-result v9

    if-ne v9, v13, :cond_c

    move-object v4, v0

    const/4 v3, 0x0

    goto :goto_1

    :cond_c
    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I
    invoke-static {v0}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2400(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I

    move-result v9

    const/4 v10, 0x5

    if-ne v9, v10, :cond_1

    move-object v6, v0

    goto :goto_1

    :cond_d
    if-eqz v3, :cond_7

    iget-object v9, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mQuery:Lcom/google/android/velvet/Query;
    invoke-static {v3}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2600(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Lcom/google/android/velvet/Query;

    move-result-object v10

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mErrorCode:I
    invoke-static {v3}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2700(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)I

    move-result v11

    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mErrorDescription:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2800(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v9, v10, v11, v12}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onPageError(Lcom/google/android/velvet/Query;ILjava/lang/String;)V

    goto :goto_2

    :cond_e
    # getter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2600(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;)Lcom/google/android/velvet/Query;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/google/android/velvet/webview/GsaWebViewController;->maybeSetNewQueryFromWebView(Lcom/google/android/velvet/Query;)V

    goto :goto_3
.end method

.method private handleLoadFinished(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    const/16 v1, 0xb

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleLoadStartedOrFinished(Landroid/net/Uri;II)V

    return-void
.end method

.method private handleLoadStarted(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x2

    const/16 v1, 0x9

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleLoadStartedOrFinished(Landroid/net/Uri;II)V

    return-void
.end method

.method private handleLoadStartedOrFinished(Landroid/net/Uri;II)V
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;)Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->maybeSetNewQueryFromWebView(Lcom/google/android/velvet/Query;)V

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1, p3}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    invoke-direct {p0, p2}, Lcom/google/android/velvet/webview/GsaWebViewController;->setLoadState(I)V

    :cond_0
    return-void
.end method

.method private handlePendingClearHistory()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHasPendingHistoryClear:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->canUseWebView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHasPendingHistoryClear:Z

    :cond_0
    return-void
.end method

.method private handleStateChange()V
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-boolean v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->canUseWebView()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->haveStartedLoading()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->setPreviousResultsSuppressionDeadline()V

    :cond_0
    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v4}, Lcom/google/android/velvet/presenter/QueryState;->takeQueryToCommitToWebView()Lcom/google/android/velvet/Query;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->clearWebViewsHistory()V

    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    iput-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    monitor-enter v4

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->releaseInternalEvents()V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    iget-object v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-interface {v4, v5}, Lcom/google/android/velvet/webview/WebViewControllerClient;->commitQueryAndMaybeGetPrefetch(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/prefetch/SearchResultPage;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0, v4, v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->loadRegularSearchResults(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/prefetch/SearchResultPage;)V

    iput v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I

    iput-boolean v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z

    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->maybeSuppressPreviousResults()V

    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    iget-boolean v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    if-eqz v5, :cond_3

    iget-boolean v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSuppressingPreviousResults:Z

    if-nez v5, :cond_3

    :goto_1
    invoke-interface {v4, v2}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onStateChanged(Z)V

    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_2
    const-string v4, "Velvet.GsaWebViewController"

    const-string v5, "WebView Access disallowed"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private hasPendingClearHistoryForCurrentWebView()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHasPendingHistoryClear:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private haveStartedLoading()Z
    .locals 1

    iget v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initWebSettings()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUserAgent:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mGeolocationDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setGeolocationDatabasePath(Ljava/lang/String;)V

    return-void
.end method

.method private initWebView()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->initWebSettings()V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClient:Lcom/google/android/velvet/webview/GsaWebViewController$GsaWebViewClient;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/google/android/velvet/webview/GsaWebChromeClient;

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLocationSettings:Lcom/google/android/searchcommon/google/LocationSettings;

    invoke-direct {v1, v2, v3}, Lcom/google/android/velvet/webview/GsaWebChromeClient;-><init>(Landroid/webkit/WebView;Lcom/google/android/searchcommon/google/LocationSettings;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mPictureListener:Landroid/webkit/WebView$PictureListener;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    new-instance v1, Lcom/google/android/velvet/webview/GsaWebViewController$5;

    invoke-direct {v1, p0}, Lcom/google/android/velvet/webview/GsaWebViewController$5;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->registerHandler(Lcom/google/android/velvet/presenter/JsEventController$JsEventHandler;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadFromCacheFailed(Ljava/lang/String;Lcom/google/android/velvet/Query;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/Exception;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    const-string v0, "Velvet.GsaWebViewController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not load page from cache: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/16 v0, 0x1bc

    const-string v1, "No response"

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalPageError(ILcom/google/android/velvet/Query;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "Velvet.GsaWebViewController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error loading page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private loadRegularSearchResults(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/prefetch/SearchResultPage;)V
    .locals 6
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/prefetch/SearchResultPage;

    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->clearView()V

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->getSpeechRequestId()Ljava/lang/String;

    move-result-object v1

    :cond_0
    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v4, p1, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSearchRequest(Lcom/google/android/velvet/Query;Ljava/lang/String;)Lcom/google/android/searchcommon/util/UriRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/searchcommon/util/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    iput-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastLoadUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastLoadHeaders:Ljava/util/Map;

    if-eqz p2, :cond_1

    iget-object v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClientLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iput-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchLoadUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchResult:Lcom/google/android/velvet/prefetch/SearchResultPage;

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchQuery:Lcom/google/android/velvet/Query;

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4, v3, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    return-void

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_1
    iget-object v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClientLock:Ljava/lang/Object;

    monitor-enter v5

    const/4 v4, 0x0

    :try_start_2
    iput-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchLoadUrl:Ljava/lang/String;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchResult:Lcom/google/android/velvet/prefetch/SearchResultPage;

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchQuery:Lcom/google/android/velvet/Query;

    monitor-exit v5

    goto :goto_0

    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4
.end method

.method private maybeSetNewQueryFromWebView(Lcom/google/android/velvet/Query;)V
    .locals 2
    .param p1    # Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->equivalentForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    invoke-interface {v0, p1}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onNewQuery(Lcom/google/android/velvet/Query;)V

    :cond_0
    return-void
.end method

.method private maybeSuppressPreviousResults()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v2}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mEndPreviousResultsSuppression:Ljava/lang/Runnable;

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-wide v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mPreviousResultsSuppressedUntil:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSuppressingPreviousResults:Z

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mEndPreviousResultsSuppression:Ljava/lang/Runnable;

    iget-wide v4, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mPreviousResultsSuppressedUntil:J

    sub-long/2addr v4, v0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSuppressingPreviousResults:Z

    goto :goto_0
.end method

.method private onJsEventId(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClientLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchLoadUrl:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchLoadUrl:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchResult:Lcom/google/android/velvet/prefetch/SearchResultPage;

    invoke-interface {v2}, Lcom/google/android/velvet/prefetch/SearchResultPage;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    iget-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-interface {v2, v3, v0, p1}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onShowedPrefetchedSrp(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_2
    const-string v2, "Velvet.GsaWebViewController"

    const-string v3, "Couldn\'t get WebView so couldn\'t send gen_204"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v2, "Velvet.GsaWebViewController"

    const-string v3, "Could not get event id from prefetched SRP"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private onJsModuleInitialized(Landroid/net/Uri;)V
    .locals 3
    .param p1    # Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;)Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->equivalentForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/QueryState;->pintoModuleLoaded()V

    goto :goto_0
.end method

.method private postInternalEvent(ILandroid/net/Uri;Lcom/google/android/velvet/Query;ILjava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Landroid/net/Uri;
    .param p3    # Lcom/google/android/velvet/Query;
    .param p4    # I
    .param p5    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    iget-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    new-instance v3, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;-><init>(Lcom/google/android/velvet/webview/GsaWebViewController;Lcom/google/android/velvet/webview/GsaWebViewController$1;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    iget v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;

    iget v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    # setter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mEvent:I
    invoke-static {v0, p1}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2402(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;I)I

    # setter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mUrl:Landroid/net/Uri;
    invoke-static {v0, p2}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2502(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;Landroid/net/Uri;)Landroid/net/Uri;

    # setter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mQuery:Lcom/google/android/velvet/Query;
    invoke-static {v0, p3}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2602(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/Query;

    # setter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mErrorCode:I
    invoke-static {v0, p4}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2702(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;I)I

    # setter for: Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->mErrorDescription:Ljava/lang/String;
    invoke-static {v0, p5}, Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;->access$2802(Lcom/google/android/velvet/webview/GsaWebViewController$InternalEvent;Ljava/lang/String;)Ljava/lang/String;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHandleInternalEvents:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHandleInternalEvents:Ljava/lang/Runnable;

    const-wide/16 v3, 0xa

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private postInternalInterceptedPageLoad(Landroid/net/Uri;Lcom/google/android/velvet/Query;)V
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/velvet/Query;

    const/4 v1, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalEvent(ILandroid/net/Uri;Lcom/google/android/velvet/Query;ILjava/lang/String;)V

    return-void
.end method

.method private postInternalPageError(ILcom/google/android/velvet/Query;Ljava/lang/String;)V
    .locals 6
    .param p1    # I
    .param p2    # Lcom/google/android/velvet/Query;
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p2

    move v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalEvent(ILandroid/net/Uri;Lcom/google/android/velvet/Query;ILjava/lang/String;)V

    return-void
.end method

.method private postInternalPageFinished(Landroid/net/Uri;)V
    .locals 6
    .param p1    # Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v1, 0x3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalEvent(ILandroid/net/Uri;Lcom/google/android/velvet/Query;ILjava/lang/String;)V

    return-void
.end method

.method private postInternalPageStarted(Landroid/net/Uri;)V
    .locals 6
    .param p1    # Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalEvent(ILandroid/net/Uri;Lcom/google/android/velvet/Query;ILjava/lang/String;)V

    return-void
.end method

.method private postInternalReadyToShow()V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x5

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalEvent(ILandroid/net/Uri;Lcom/google/android/velvet/Query;ILjava/lang/String;)V

    return-void
.end method

.method private releaseInternalEvents()V
    .locals 2

    :goto_0
    iget v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    iget v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mNumInternalEvents:I

    return-void
.end method

.method private setCookiesFromPrefetchedPage(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)Z"
        }
    .end annotation

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/velvet/Cookies;->setCookiesFromHeaders(Ljava/lang/String;Ljava/util/Map;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setLoadState(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    invoke-interface {v0}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onStartResultsPage()V

    :cond_0
    :goto_0
    iput p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleStateChange()V

    :cond_1
    return-void

    :cond_2
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->setPreviousResultsSuppressionDeadline()V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    invoke-interface {v0}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onEndResultsPage()V

    goto :goto_0
.end method

.method private setPreviousResultsSuppressionDeadline()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mConfig:Lcom/google/android/searchcommon/SearchConfig;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/SearchConfig;->getWebViewSuppressPreviousResultsForMs()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClock:Lcom/google/android/searchcommon/util/Clock;

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->uptimeMillis()J

    move-result-wide v1

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mPreviousResultsSuppressedUntil:J

    return-void
.end method

.method private shouldOverrideWebViewClick(Landroid/net/Uri;)Z
    .locals 5
    .param p1    # Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;)Lcom/google/android/velvet/Query;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v2, "Velvet.GsaWebViewController"

    const-string v3, "URL change initiated from the page"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSettings:Lcom/google/android/searchcommon/SearchSettings;

    invoke-interface {v2}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v4, v0}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalInterceptedPageLoad(Landroid/net/Uri;Lcom/google/android/velvet/Query;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, v4}, Lcom/google/android/velvet/webview/GsaWebViewController;->postInternalInterceptedPageLoad(Landroid/net/Uri;Lcom/google/android/velvet/Query;)V

    goto :goto_0
.end method


# virtual methods
.method public canUseWebView()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public clearWebViewsHistory()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHasPendingHistoryClear:Z

    return-void
.end method

.method public commitQueryForSingleRequestArchitecture(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/prefetch/SearchResultPage;)V
    .locals 3
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Lcom/google/android/velvet/prefetch/SearchResultPage;

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->canUseWebView()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mInternalEvents:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->releaseInternalEvents()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->clearWebViewsHistory()V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-direct {p0, v0, p2}, Lcom/google/android/velvet/webview/GsaWebViewController;->loadRegularSearchResults(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/prefetch/SearchResultPage;)V

    iput v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLoadState:I

    iput-boolean v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->maybeSuppressPreviousResults()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized dispose()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryObserver:Lcom/google/android/velvet/presenter/QueryState$Observer;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->removeObserver(Lcom/google/android/velvet/presenter/QueryState$Observer;)V

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->releaseInternalEvents()V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHandleInternalEvents:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    invoke-virtual {v0}, Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;->dispose()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    iget-object v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewClientLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v6, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastPrefetchLoadUrl:Ljava/lang/String;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "GsaWebViewController state:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Last load:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastLoadUrl:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->getLoadedUrl()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mLastLoadHeaders:Ljava/util/Map;

    move-object v0, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/webview/GsaWebViewController;->dumpUrlAndHeaders(Ljava/lang/String;Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Last prefetched load:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "SuppressPreviousResults:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mSuppressingPreviousResults:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mPreviousResultsSuppressedUntil:J

    invoke-virtual {p2, v2, v3}, Ljava/io/PrintWriter;->println(J)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onBackPressed()Z
    .locals 9

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewInUse:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v6}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->hasPendingClearHistoryForCurrentWebView()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v6}, Landroid/webkit/WebView;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {v2}, Landroid/webkit/WebBackForwardList;->getCurrentIndex()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    :goto_0
    if-ltz v3, :cond_0

    invoke-virtual {v2, v3}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v6

    invoke-virtual {v6}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v6, v7, v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getQueryFromUrl(Lcom/google/android/velvet/Query;Landroid/net/Uri;)Lcom/google/android/velvet/Query;

    move-result-object v4

    if-eqz v4, :cond_2

    :cond_0
    if-nez v4, :cond_3

    :cond_1
    :goto_1
    return v5

    :cond_2
    const-string v6, "Velvet.GsaWebViewController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Went back to non-search URL:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->safeLogUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    iget-object v7, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    invoke-virtual {v6, v4, v7}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->equivalentForSearch(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "Velvet.GsaWebViewController"

    const-string v7, "WebView back wants to change the query"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    iget-object v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    invoke-interface {v5}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onStartResultsPage()V

    iget-object v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    sub-int v6, v3, v1

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->goBackOrForward(I)V

    iget-object v5, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mClient:Lcom/google/android/velvet/webview/WebViewControllerClient;

    invoke-interface {v5}, Lcom/google/android/velvet/webview/WebViewControllerClient;->onEndResultsPage()V

    const/4 v5, 0x1

    goto :goto_1
.end method

.method removePendingHistoryClear()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mHasPendingHistoryClear:Z

    return-void
.end method

.method setCommittedQuery(Lcom/google/android/velvet/Query;)V
    .locals 0
    .param p1    # Lcom/google/android/velvet/Query;

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mCommittedQuery:Lcom/google/android/velvet/Query;

    return-void
.end method

.method public setWebViewAccessAllowed(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    const-string v0, "Velvet.GsaWebViewController"

    const-string v1, "Attempting to set WebView access on null WebView."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    iget-boolean v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebViewAccessAllowed:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/QueryState;->reportLatencyEvent(I)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->handleStateChange()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    goto :goto_1
.end method

.method public setWebViewAndGsaCommunicationJsHelper(Landroid/webkit/WebView;Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "WebView may only be set once."

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    iput-object p2, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mGsaCommunicationJsHelper:Lcom/google/android/velvet/webview/GsaCommunicationJsHelper;

    invoke-direct {p0}, Lcom/google/android/velvet/webview/GsaWebViewController;->initWebView()V

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v0}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->isSearchDomainSchemeHttps()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/velvet/webview/GsaWebViewController;->mUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/google/SearchUrlHelper;->getSslPrewarmUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
