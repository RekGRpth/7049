.class public interface abstract Lcom/google/android/velvet/webview/WebViewControllerClient;
.super Ljava/lang/Object;
.source "WebViewControllerClient.java"


# virtual methods
.method public abstract commitQueryAndMaybeGetPrefetch(Lcom/google/android/velvet/Query;)Lcom/google/android/velvet/prefetch/SearchResultPage;
.end method

.method public abstract onEndResultsPage()V
.end method

.method public abstract onLinkClicked(Landroid/net/Uri;)V
.end method

.method public abstract onLogoutRedirect()V
.end method

.method public abstract onNewQuery(Lcom/google/android/velvet/Query;)V
.end method

.method public abstract onPageError(Lcom/google/android/velvet/Query;ILjava/lang/String;)V
.end method

.method public abstract onShowedPrefetchedSrp(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/Query;Ljava/lang/String;)V
.end method

.method public abstract onStartResultsPage()V
.end method

.method public abstract onStateChanged(Z)V
.end method
