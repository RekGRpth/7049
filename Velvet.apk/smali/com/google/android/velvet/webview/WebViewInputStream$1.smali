.class Lcom/google/android/velvet/webview/WebViewInputStream$1;
.super Ljava/lang/Object;
.source "WebViewInputStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/velvet/webview/WebViewInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/velvet/webview/WebViewInputStream;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/webview/WebViewInputStream;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/velvet/webview/WebViewInputStream$1;->this$0:Lcom/google/android/velvet/webview/WebViewInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/velvet/webview/WebViewInputStream$1;->this$0:Lcom/google/android/velvet/webview/WebViewInputStream;

    # getter for: Lcom/google/android/velvet/webview/WebViewInputStream;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;
    invoke-static {v0}, Lcom/google/android/velvet/webview/WebViewInputStream;->access$100(Lcom/google/android/velvet/webview/WebViewInputStream;)Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream$1;->this$0:Lcom/google/android/velvet/webview/WebViewInputStream;

    # getter for: Lcom/google/android/velvet/webview/WebViewInputStream;->mQuery:Lcom/google/android/velvet/Query;
    invoke-static {v1}, Lcom/google/android/velvet/webview/WebViewInputStream;->access$000(Lcom/google/android/velvet/webview/WebViewInputStream;)Lcom/google/android/velvet/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/velvet/presenter/WebSearchConnectionError;

    const/16 v3, 0x1bc

    const-string v4, "No Response"

    invoke-direct {v2, v3, v4}, Lcom/google/android/velvet/presenter/WebSearchConnectionError;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/presenter/QueryState;->resultsPageError(Lcom/google/android/velvet/Query;Lcom/google/android/velvet/presenter/SearchError;)V

    return-void
.end method
