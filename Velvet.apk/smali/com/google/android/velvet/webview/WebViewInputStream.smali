.class Lcom/google/android/velvet/webview/WebViewInputStream;
.super Ljava/io/InputStream;
.source "WebViewInputStream.java"


# instance fields
.field private final mDelegate:Ljava/io/InputStream;

.field private final mErrorReportingThread:Ljava/util/concurrent/Executor;

.field private final mQuery:Lcom/google/android/velvet/Query;

.field private final mQueryState:Lcom/google/android/velvet/presenter/QueryState;

.field private final mReportConnectionError:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/velvet/Query;Ljava/io/InputStream;Lcom/google/android/velvet/presenter/QueryState;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/Query;
    .param p2    # Ljava/io/InputStream;
    .param p3    # Lcom/google/android/velvet/presenter/QueryState;
    .param p4    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    new-instance v0, Lcom/google/android/velvet/webview/WebViewInputStream$1;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/webview/WebViewInputStream$1;-><init>(Lcom/google/android/velvet/webview/WebViewInputStream;)V

    iput-object v0, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mReportConnectionError:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mQuery:Lcom/google/android/velvet/Query;

    iput-object p2, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mDelegate:Ljava/io/InputStream;

    iput-object p3, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    iput-object p4, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mErrorReportingThread:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/velvet/webview/WebViewInputStream;)Lcom/google/android/velvet/Query;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/WebViewInputStream;

    iget-object v0, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mQuery:Lcom/google/android/velvet/Query;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/velvet/webview/WebViewInputStream;)Lcom/google/android/velvet/presenter/QueryState;
    .locals 1
    .param p0    # Lcom/google/android/velvet/webview/WebViewInputStream;

    iget-object v0, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mQueryState:Lcom/google/android/velvet/presenter/QueryState;

    return-object v0
.end method

.method private reportErrorFromInterceptedStream(Ljava/io/IOException;)V
    .locals 3
    .param p1    # Ljava/io/IOException;

    const-string v0, "Velvet.WebViewInputStream"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error reported from delegate stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mErrorReportingThread:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mReportConnectionError:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public available()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/WebViewInputStream;->reportErrorFromInterceptedStream(Ljava/io/IOException;)V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/WebViewInputStream;->reportErrorFromInterceptedStream(Ljava/io/IOException;)V

    goto :goto_0
.end method

.method public read()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/WebViewInputStream;->reportErrorFromInterceptedStream(Ljava/io/IOException;)V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public read([B)I
    .locals 2
    .param p1    # [B

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v1, p1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/WebViewInputStream;->reportErrorFromInterceptedStream(Ljava/io/IOException;)V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public read([BII)I
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/WebViewInputStream;->reportErrorFromInterceptedStream(Ljava/io/IOException;)V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/WebViewInputStream;->reportErrorFromInterceptedStream(Ljava/io/IOException;)V

    throw v0
.end method

.method public skip(J)J
    .locals 3
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/velvet/webview/WebViewInputStream;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2}, Ljava/io/InputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    return-wide v1

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/webview/WebViewInputStream;->reportErrorFromInterceptedStream(Ljava/io/IOException;)V

    throw v0
.end method
