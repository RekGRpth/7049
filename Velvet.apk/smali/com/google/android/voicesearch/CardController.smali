.class public interface abstract Lcom/google/android/voicesearch/CardController;
.super Ljava/lang/Object;
.source "CardController.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/IntentStarter;


# virtual methods
.method public abstract canPlayLocalTts()Z
.end method

.method public abstract getAction()Lcom/google/android/velvet/presenter/Action;
.end method

.method public abstract getDefaultCountdownDurationMs()J
.end method

.method public abstract getLastAudioUri(Lcom/google/android/speech/audio/AudioUtils$Encoding;)Landroid/net/Uri;
.end method

.method public abstract getQuery()Lcom/google/android/velvet/Query;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getQueryState()Lcom/google/android/velvet/presenter/QueryState;
.end method

.method public abstract isAttached()Z
.end method

.method public abstract takeStartCountdown()Z
.end method
