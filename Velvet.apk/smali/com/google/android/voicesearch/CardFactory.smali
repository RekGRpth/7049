.class public Lcom/google/android/voicesearch/CardFactory;
.super Ljava/lang/Object;
.source "CardFactory.java"


# instance fields
.field private final mContextWrapper:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0e004f

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    return-void
.end method

.method private static setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;>(",
            "Lcom/google/android/voicesearch/fragments/AbstractCardView",
            "<TT;>;TT;)",
            "Lcom/google/android/voicesearch/fragments/AbstractCardView",
            "<TT;>;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V

    return-object p0
.end method


# virtual methods
.method public createCard(Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/fragments/AbstractCardController",
            "<*>;)",
            "Lcom/google/android/voicesearch/fragments/AbstractCardView",
            "<*>;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.voicesearch.fragments"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "ContactSelectController"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "AtHomeController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Lcom/google/android/voicesearch/fragments/AtHomeCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/AtHomeCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/AtHomeController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    const-string v3, "CalendarEventController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Lcom/google/android/voicesearch/fragments/CalendarEventCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/CalendarEventController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string v3, "ContactSelectController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Lcom/google/android/voicesearch/contacts/ContactSelectCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/contacts/ContactSelectCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto :goto_1

    :cond_4
    const-string v3, "DictionaryResultController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/DictionaryResultController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto :goto_1

    :cond_5
    const-string v3, "EmailController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    new-instance v3, Lcom/google/android/voicesearch/fragments/EmailCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/EmailCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto :goto_1

    :cond_6
    const-string v3, "FlightResultController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Lcom/google/android/voicesearch/fragments/FlightResultCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/FlightResultCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/FlightResultController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto :goto_1

    :cond_7
    const-string v3, "GogglesGenericController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v3, Lcom/google/android/goggles/GogglesGenericCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/goggles/GogglesGenericCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/GogglesGenericController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_8
    const-string v3, "HighConfidenceAnswerController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    new-instance v3, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_9
    const-string v3, "HtmlAnswerController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    new-instance v3, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/HtmlAnswerCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/HtmlAnswerController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_a
    const-string v3, "ImageResultController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    new-instance v3, Lcom/google/android/voicesearch/fragments/ImageResultCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/ImageResultCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/ImageResultController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_b
    const-string v3, "MediumConfidenceAnswerController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    new-instance v3, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_c
    const-string v3, "MessageEditorController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    new-instance v3, Lcom/google/android/voicesearch/fragments/MessageEditorCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/MessageEditorCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/MessageEditorController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_d
    const-string v3, "MultipleLocalResultsController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    new-instance v3, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/MultipleLocalResultsController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_e
    const-string v3, "OpenAppController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    new-instance v3, Lcom/google/android/voicesearch/fragments/OpenAppCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/OpenAppCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/OpenAppController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_f
    const-string v3, "OpenAppPlayMediaController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    new-instance v3, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/OpenAppPlayMediaController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_10
    const-string v3, "OpenBookController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    new-instance v3, Lcom/google/android/voicesearch/fragments/OpenBookCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/OpenBookCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/OpenBookController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_11
    const-string v3, "OpenUrlController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    new-instance v3, Lcom/google/android/voicesearch/fragments/OpenUrlCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/OpenUrlCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/OpenUrlController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_12
    const-string v3, "PhoneCallController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    move-object v3, p1

    check-cast v3, Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->isNumberOnlyContact()Z

    move-result v3

    if-eqz v3, :cond_13

    new-instance v3, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;-><init>(Landroid/content/Context;)V

    :goto_2
    check-cast p1, Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_13
    new-instance v3, Lcom/google/android/voicesearch/fragments/PhoneCallCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/PhoneCallCard;-><init>(Landroid/content/Context;)V

    goto :goto_2

    :cond_14
    const-string v3, "PlayMovieController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    new-instance v3, Lcom/google/android/voicesearch/fragments/PlayMovieCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/PlayMovieCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/PlayMovieController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_15
    const-string v3, "PlayMusicController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    new-instance v3, Lcom/google/android/voicesearch/fragments/PlayMusicCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/PlayMusicCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/PlayMusicController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_16
    const-string v3, "PuntController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    new-instance v3, Lcom/google/android/voicesearch/fragments/PuntCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/PuntCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/PuntController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_17
    const-string v3, "QueryCalendarController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    new-instance v3, Lcom/google/android/voicesearch/fragments/QueryCalendarCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/QueryCalendarCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_18
    const-string v3, "RecognizedContactController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    new-instance v3, Lcom/google/android/goggles/RecognizedContactCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/goggles/RecognizedContactCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/RecognizedContactController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_19
    const-string v3, "SelfNoteController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    new-instance v3, Lcom/google/android/voicesearch/fragments/SelfNoteCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/SelfNoteCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/SelfNoteController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_1a
    const-string v3, "SetAlarmController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    new-instance v3, Lcom/google/android/voicesearch/fragments/SetAlarmCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/SetAlarmCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/SetAlarmController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_1b
    const-string v3, "SetReminderController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c

    new-instance v3, Lcom/google/android/voicesearch/fragments/SetReminderCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/SetReminderCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/SetReminderController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_1c
    const-string v3, "ShowContactInformationController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    new-instance v3, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_1d
    const-string v3, "SingleImageResultController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    new-instance v3, Lcom/google/android/voicesearch/fragments/ImageResultCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/ImageResultCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/SingleImageResultController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_1e
    const-string v3, "SingleLocalResultController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    new-instance v3, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_1f
    const-string v3, "SocialUpdateController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    new-instance v3, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/SocialUpdateCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_20
    const-string v3, "SportsResultController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    new-instance v3, Lcom/google/android/voicesearch/fragments/SportsResultCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/SportsResultCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/SportsResultController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_21
    const-string v3, "WeatherResultController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    new-instance v3, Lcom/google/android/voicesearch/fragments/WeatherResultCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/WeatherResultCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/WeatherResultController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_22
    const-string v3, "HelpController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    new-instance v3, Lcom/google/android/voicesearch/fragments/HelpCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/HelpCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_23
    const-string v3, "PlainTitleAndTextController"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_24

    new-instance v3, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextCard;

    iget-object v4, p0, Lcom/google/android/voicesearch/CardFactory;->mContextWrapper:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextCard;-><init>(Landroid/content/Context;)V

    check-cast p1, Lcom/google/android/voicesearch/fragments/PlainTitleAndTextController;

    invoke-static {v3, p1}, Lcom/google/android/voicesearch/CardFactory;->setController(Lcom/google/android/voicesearch/fragments/AbstractCardView;Lcom/google/android/voicesearch/fragments/AbstractCardController;)Lcom/google/android/voicesearch/fragments/AbstractCardView;

    move-result-object v3

    goto/16 :goto_1

    :cond_24
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown controller "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
