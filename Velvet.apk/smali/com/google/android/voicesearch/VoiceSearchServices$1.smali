.class Lcom/google/android/voicesearch/VoiceSearchServices$1;
.super Ljava/lang/Object;
.source "VoiceSearchServices.java"

# interfaces
.implements Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/VoiceSearchServices;-><init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/CoreSearchServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/VoiceSearchServices$1;->this$0:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V
    .locals 1
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices$1;->this$0:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getVoiceImeSubtypeUpdater()Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->onChange(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    return-void
.end method
