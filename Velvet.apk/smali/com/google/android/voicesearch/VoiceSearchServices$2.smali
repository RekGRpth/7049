.class Lcom/google/android/voicesearch/VoiceSearchServices$2;
.super Ljava/lang/Object;
.source "VoiceSearchServices.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/VoiceSearchServices;->createNetworkParams(Ljava/util/concurrent/ExecutorService;)Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/VoiceSearchServices$2;->this$0:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices$2;->this$0:Lcom/google/android/voicesearch/VoiceSearchServices;

    # getter for: Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;
    invoke-static {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->access$000(Lcom/google/android/voicesearch/VoiceSearchServices;)Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getNetworkRecognizer()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices$2;->get()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$NetworkRecognizer;

    move-result-object v0

    return-object v0
.end method
