.class Lcom/google/android/voicesearch/VoiceSearchServices$3;
.super Ljava/lang/Object;
.source "VoiceSearchServices.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/VoiceSearchServices;->createNetworkRequestProducerParams()Lcom/google/android/speech/params/NetworkRequestProducerParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/VoiceSearchServices$3;->this$0:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices$3;->this$0:Lcom/google/android/voicesearch/VoiceSearchServices;

    # invokes: Lcom/google/android/voicesearch/VoiceSearchServices;->isSoundSearchEnabled()Z
    invoke-static {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->access$100(Lcom/google/android/voicesearch/VoiceSearchServices;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices$3;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
