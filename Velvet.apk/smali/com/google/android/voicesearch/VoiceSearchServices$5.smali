.class Lcom/google/android/voicesearch/VoiceSearchServices$5;
.super Ljava/lang/Object;
.source "VoiceSearchServices.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/VoiceSearchServices;->createRecognizerController(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/VoiceSearchServices$5;->this$0:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices$5;->this$0:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getHeadsetAudioRecorderRouter()Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->shouldDisableExtraTts()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices$5;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
