.class public Lcom/google/android/voicesearch/VoiceSearchServices;
.super Ljava/lang/Object;
.source "VoiceSearchServices.java"


# instance fields
.field private final mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

.field private mAudioController:Lcom/google/android/speech/audio/AudioController;

.field private final mAudioStore:Lcom/google/android/speech/audio/AudioStore;

.field private mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

.field private mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

.field private final mContext:Landroid/content/Context;

.field private final mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field private final mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private mEarsProviderHelper:Lcom/google/android/ears/EarsContentProviderHelper;

.field private mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

.field private mHeadsetAudioRecorderRouter:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

.field private mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

.field private mLanguageDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

.field private mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field private final mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

.field private mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

.field private final mPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

.field private mRecognizer:Lcom/google/android/speech/Recognizer;

.field private mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

.field private final mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

.field private mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

.field private mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

.field private mTextRecognizer:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

.field private mTtsAudioPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

.field private mVoiceImeSubtypeUpdater:Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/searchcommon/CoreSearchServices;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p3    # Lcom/google/android/searchcommon/GsaPreferenceController;
    .param p4    # Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object p3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

    const/4 v0, 0x5

    const-string v1, "ContainerScheduledExecutor"

    invoke-static {v0, v1}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->createSafeScheduledExecutorService(ILjava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lcom/google/android/speech/audio/AudioStore;

    invoke-direct {v0}, Lcom/google/android/speech/audio/AudioStore;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    invoke-interface {p4}, Lcom/google/android/searchcommon/CoreSearchServices;->getDeviceCapabilityManager()Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-interface {p4}, Lcom/google/android/searchcommon/CoreSearchServices;->getVoiceSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    new-instance v1, Lcom/google/android/voicesearch/VoiceSearchServices$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/VoiceSearchServices$1;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/settings/Settings;->addConfigurationListener(Lcom/google/android/voicesearch/settings/Settings$ConfigurationChangeListener;)V

    new-instance v0, Lcom/google/android/voicesearch/util/AccountHelperImpl;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-interface {p4}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/voicesearch/util/AccountHelperImpl;-><init>(Landroid/accounts/AccountManager;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchSettings;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iput-object p4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/VoiceSearchServices;)Lcom/google/android/voicesearch/settings/Settings;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/VoiceSearchServices;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/VoiceSearchServices;)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-direct {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->isSoundSearchEnabled()Z

    move-result v0

    return v0
.end method

.method private createDeviceParams(Lcom/google/android/searchcommon/UserAgentHelper;Landroid/view/WindowManager;)Lcom/google/android/speech/params/DeviceParams;
    .locals 8
    .param p1    # Lcom/google/android/searchcommon/UserAgentHelper;
    .param p2    # Landroid/view/WindowManager;

    new-instance v0, Lcom/google/android/speech/params/DeviceParamsImpl;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVersionCodeString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/speech/network/request/BrowserParamsSupplier;

    new-instance v3, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchUrlHelper()Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v4

    invoke-direct {v2, v3, v4, p1, p2}, Lcom/google/android/speech/network/request/BrowserParamsSupplier;-><init>(Lcom/google/android/searchcommon/preferences/PredictiveCardsPreferences;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/common/base/Supplier;Landroid/view/WindowManager;)V

    new-instance v3, Lcom/google/android/speech/network/request/PreviewParamsSupplier;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/speech/network/request/PreviewParamsSupplier;-><init>(Landroid/content/res/Resources;)V

    invoke-static {v3}, Lcom/google/common/base/Suppliers;->memoize(Lcom/google/common/base/Supplier;)Lcom/google/common/base/Supplier;

    move-result-object v3

    new-instance v4, Lcom/google/android/speech/network/request/ScreenParamsSupplier;

    invoke-direct {v4, p2}, Lcom/google/android/speech/network/request/ScreenParamsSupplier;-><init>(Landroid/view/WindowManager;)V

    invoke-static {v4}, Lcom/google/common/base/Suppliers;->memoize(Lcom/google/common/base/Supplier;)Lcom/google/common/base/Supplier;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v6

    iget-object v5, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v5}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v7

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/speech/params/DeviceParamsImpl;-><init>(Ljava/lang/String;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/SearchConfig;Lcom/google/android/searchcommon/SearchSettings;)V

    return-object v0
.end method

.method private createEmbeddedParams()Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;
    .locals 8

    new-instance v0, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;

    new-instance v1, Lcom/google/android/speech/internal/DefaultCallbackFactory;

    invoke-direct {v1}, Lcom/google/android/speech/internal/DefaultCallbackFactory;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3EngineManager()Lcom/google/android/speech/embedded/Greco3EngineManager;

    move-result-object v2

    new-instance v3, Lcom/google/android/speech/internal/DefaultModeSelector;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-interface {v4}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->isTelephoneCapable()Z

    move-result v4

    invoke-direct {v3, v4}, Lcom/google/android/speech/internal/DefaultModeSelector;-><init>(Z)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSpeechLevelSource()Lcom/google/android/speech/SpeechLevelSource;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    const/4 v6, 0x2

    const/16 v7, 0x1f40

    invoke-direct/range {v0 .. v7}, Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;-><init>(Lcom/google/android/speech/embedded/Greco3CallbackFactory;Lcom/google/android/speech/embedded/Greco3EngineManager;Lcom/google/android/speech/embedded/Greco3ModeSelector;Lcom/google/android/speech/SpeechLevelSource;Lcom/google/android/speech/SpeechSettings;II)V

    return-object v0
.end method

.method private createHybridParams(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;
    .locals 8
    .param p1    # Ljava/util/concurrent/ExecutorService;
    .param p2    # Ljava/util/concurrent/ExecutorService;
    .param p3    # Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getNetworkInformation()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v7, Lcom/google/android/voicesearch/VoiceSearchServices$4;

    invoke-direct {v7, p0}, Lcom/google/android/voicesearch/VoiceSearchServices$4;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;-><init>(Lcom/google/android/speech/utils/NetworkInformation;Lcom/google/android/voicesearch/settings/Settings;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Lcom/google/common/base/Supplier;)V

    return-object v0
.end method

.method private createMusicDetectorParams()Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;
    .locals 2

    new-instance v0, Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {v0, v1}, Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;-><init>(Lcom/google/android/voicesearch/settings/Settings;)V

    return-object v0
.end method

.method private createNetworkParams(Ljava/util/concurrent/ExecutorService;)Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;
    .locals 9
    .param p1    # Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/speech/network/PairHttpConnectionFactory;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getPairHttpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchConfig;)Lcom/google/common/base/Supplier;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getConnectionFactory()Lcom/google/android/speech/network/ConnectionFactory;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/google/android/speech/network/PairHttpConnectionFactory;-><init>(Lcom/google/common/base/Supplier;Lcom/google/android/speech/network/ConnectionFactory;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->isUseHttpForVoice()Z

    move-result v8

    const/4 v7, 0x0

    if-nez v8, :cond_0

    new-instance v7, Lcom/google/android/speech/network/TcpConnectionFactory;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v1}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getTcpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/searchcommon/SearchConfig;)Lcom/google/common/base/Supplier;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getConnectionFactory()Lcom/google/android/speech/network/ConnectionFactory;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v4

    invoke-direct {v7, v0, v1, v4}, Lcom/google/android/speech/network/TcpConnectionFactory;-><init>(Lcom/google/common/base/Supplier;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/speech/utils/NetworkInformation;)V

    :cond_0
    new-instance v6, Lcom/google/android/voicesearch/VoiceSearchServices$2;

    invoke-direct {v6, p0}, Lcom/google/android/voicesearch/VoiceSearchServices$2;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V

    new-instance v3, Lcom/google/android/speech/engine/DefaultRetryPolicy;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getClock()Lcom/google/android/searchcommon/util/Clock;

    move-result-object v0

    invoke-direct {v3, v6, v0}, Lcom/google/android/speech/engine/DefaultRetryPolicy;-><init>(Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/util/Clock;)V

    new-instance v0, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;

    if-eqz v8, :cond_1

    move-object v1, v2

    :goto_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->createNetworkRequestProducerParams()Lcom/google/android/speech/params/NetworkRequestProducerParams;

    move-result-object v5

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;-><init>(Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/network/S3ConnectionFactory;Lcom/google/android/speech/engine/RetryPolicy;Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/params/NetworkRequestProducerParams;)V

    return-object v0

    :cond_1
    move-object v1, v7

    goto :goto_0
.end method

.method private createNetworkRequestProducerParams()Lcom/google/android/speech/params/NetworkRequestProducerParams;
    .locals 10

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    new-instance v0, Lcom/google/android/speech/params/NetworkRequestProducerParams;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getPinholeParamsBuilder()Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;

    move-result-object v4

    new-instance v5, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;

    iget-object v6, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v6}, Lcom/google/android/searchcommon/CoreSearchServices;->getLocationSettings()Lcom/google/android/searchcommon/google/LocationSettings;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getLocationOracle()Lcom/google/android/apps/sidekick/inject/LocationOracle;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/voicesearch/speechservice/s3/VelvetSpeechLocationHelper;-><init>(Lcom/google/android/searchcommon/google/LocationSettings;Lcom/google/android/apps/sidekick/inject/LocationOracle;)V

    iget-object v6, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    new-instance v7, Lcom/google/android/voicesearch/VoiceSearchServices$3;

    invoke-direct {v7, p0}, Lcom/google/android/voicesearch/VoiceSearchServices$3;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V

    invoke-direct {p0, v9, v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->createDeviceParams(Lcom/google/android/searchcommon/UserAgentHelper;Landroid/view/WindowManager;)Lcom/google/android/speech/params/DeviceParams;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/speech/params/NetworkRequestProducerParams;-><init>(Lcom/google/android/speech/helper/AuthTokenHelper;Landroid/view/WindowManager;Lcom/google/android/speech/utils/NetworkInformation;Lcom/google/android/voicesearch/speechservice/s3/PinholeParamsBuilder;Lcom/google/android/speech/helper/SpeechLocationHelper;Lcom/google/android/speech/SpeechSettings;Lcom/google/common/base/Supplier;Lcom/google/android/speech/params/DeviceParams;)V

    return-object v0
.end method

.method private createRecognizer()Lcom/google/android/speech/Recognizer;
    .locals 11

    const/4 v9, 0x1

    const-string v7, "VS.Container"

    const-string v8, "create_speech_recognizer"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "LocalEngine"

    invoke-static {v9, v7}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->createSafeScheduledExecutorService(ILjava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    const-string v7, "NetworkEngine"

    invoke-static {v9, v7}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->createSafeScheduledExecutorService(ILjava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    const-string v7, "MusicDetector"

    invoke-static {v9, v7}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->createSafeScheduledExecutorService(ILjava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->createEmbeddedParams()Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;

    move-result-object v0

    invoke-direct {p0, v5}, Lcom/google/android/voicesearch/VoiceSearchServices;->createNetworkParams(Ljava/util/concurrent/ExecutorService;)Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;

    move-result-object v6

    invoke-direct {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->createMusicDetectorParams()Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;

    move-result-object v3

    invoke-direct {p0, v2, v5, v4}, Lcom/google/android/voicesearch/VoiceSearchServices;->createHybridParams(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;

    move-result-object v1

    const-string v7, "GrecoExecutor"

    invoke-static {v7}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->newSingleThreadExecutor(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v7

    new-instance v8, Lcom/google/android/speech/params/RecognitionEngineParams;

    invoke-direct {v8, v0, v6, v3, v1}, Lcom/google/android/speech/params/RecognitionEngineParams;-><init>(Lcom/google/android/speech/params/RecognitionEngineParams$EmbeddedParams;Lcom/google/android/speech/params/RecognitionEngineParams$NetworkParams;Lcom/google/android/speech/params/RecognitionEngineParams$MusicDetectorParams;Lcom/google/android/speech/params/RecognitionEngineParams$HybridParams;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAudioController()Lcom/google/android/speech/audio/AudioController;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    invoke-static {v7, v8, v9, v10}, Lcom/google/android/speech/GrecoRecognizer;->create(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/params/RecognitionEngineParams;Lcom/google/android/speech/audio/AudioController;Lcom/google/android/speech/audio/AudioStore;)Lcom/google/android/speech/Recognizer;

    move-result-object v7

    return-object v7
.end method

.method private createTextRecognizer()Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;
    .locals 6

    const-string v0, "VS.Container"

    const-string v2, "create_text_recognizer"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    const-string v2, "TextRecognizer"

    invoke-static {v0, v2}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->createSafeScheduledExecutorService(ILjava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    new-instance v0, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getConnectionFactory()Lcom/google/android/speech/network/ConnectionFactory;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizerParamsFactory()Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {v4}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getSpokenLocaleBcp47Supplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {v5}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getSingleHttpServerInfoSupplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;)V

    return-object v0
.end method

.method private isBlacklistedSoundSearchDevice()Z
    .locals 5

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v4}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasSoundSearch()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getSoundSearch()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$SoundSearch;->getBlacklistedDevicesList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isSoundSearchEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/SearchConfig;->getSoundSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->isBlacklistedSoundSearchDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->hasDebug()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createPumpkinTagger(Ljava/lang/String;)Lcom/google/android/speech/embedded/PumpkinTagger;
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    const-string v2, "PumpkinTagger"

    invoke-static {v1, v2}, Lcom/google/android/searchcommon/util/ConcurrentUtils;->createSafeScheduledExecutorService(ILjava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, p1}, Lcom/google/android/speech/embedded/PumpkinTagger;->createIfAvailable(Ljava/util/concurrent/ExecutorService;Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/speech/embedded/PumpkinTagger;

    move-result-object v1

    return-object v1
.end method

.method public createRecognizerController(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .locals 7
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    .param p2    # Lcom/google/android/searchcommon/SearchSettings;
    .param p3    # Lcom/google/android/searchcommon/util/Clock;
    .param p4    # Lcom/google/android/searchcommon/google/SearchUrlHelper;

    new-instance v3, Lcom/google/android/voicesearch/VoiceSearchServices$5;

    invoke-direct {v3, p0}, Lcom/google/android/voicesearch/VoiceSearchServices$5;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {v0}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getSpokenLocaleBcp47Supplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->createForGoogleNow(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v0

    return-object v0
.end method

.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mHeadsetAudioRecorderRouter:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mHeadsetAudioRecorderRouter:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->destroy(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    return-object v0
.end method

.method public getAudioController()Lcom/google/android/speech/audio/AudioController;
    .locals 8

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/speech/audio/AudioController;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSpeechLevelSource()Lcom/google/android/speech/SpeechLevelSource;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSoundManager()Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v4

    const/4 v5, 0x1

    new-instance v6, Lcom/google/android/voicesearch/handsfree/AudioRoutingSupplier;

    iget-object v7, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/google/android/voicesearch/handsfree/AudioRoutingSupplier;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getHeadsetAudioRecorderRouter()Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/speech/audio/AudioController;-><init>(Landroid/content/Context;Lcom/google/android/speech/SpeechSettings;Lcom/google/android/speech/SpeechLevelSource;Lcom/google/android/speech/audio/SpeechSoundManager;ILcom/google/common/base/Supplier;Lcom/google/android/voicesearch/AudioRecorderRouter;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAudioController:Lcom/google/android/speech/audio/AudioController;

    return-object v0
.end method

.method public getAudioStore()Lcom/google/android/speech/audio/AudioStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    return-object v0
.end method

.method public declared-synchronized getConnectionFactory()Lcom/google/android/speech/network/ConnectionFactory;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;

    new-instance v1, Lcom/google/android/speech/network/ConnectionFactoryImpl;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/speech/network/ConnectionFactoryImpl;-><init>(Landroid/content/Context;Lcom/google/common/base/Supplier;)V

    new-instance v2, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getUserAgentHelper()Lcom/google/android/searchcommon/UserAgentHelper;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/voicesearch/speechservice/spdy/SpdyConnectionFactory;-><init>(Landroid/content/Context;Lcom/google/common/base/Supplier;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v3}, Lcom/google/android/searchcommon/CoreSearchServices;->getConfig()Lcom/google/android/searchcommon/SearchConfig;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/speechservice/s3/VoiceSearchConnectionFactory;-><init>(Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/speech/network/ConnectionFactory;Lcom/google/android/searchcommon/SearchConfig;Z)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mConnectionFactory:Lcom/google/android/speech/network/ConnectionFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getContactLookup()Lcom/google/android/speech/contacts/ContactLookup;
    .locals 2

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/speech/contacts/ContactLookup;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/speech/contacts/ContactLookup;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    return-object v0
.end method

.method public getEarsProviderHelper()Lcom/google/android/ears/EarsContentProviderHelper;
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mEarsProviderHelper:Lcom/google/android/ears/EarsContentProviderHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ears/EarsContentProviderHelper;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/ears/EarsContentProviderHelper;-><init>(Landroid/content/ContentResolver;Landroid/content/pm/PackageManager;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mEarsProviderHelper:Lcom/google/android/ears/EarsContentProviderHelper;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mEarsProviderHelper:Lcom/google/android/ears/EarsContentProviderHelper;

    return-object v0
.end method

.method public getExecutorService()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

    invoke-virtual {v1}, Lcom/google/android/searchcommon/GsaPreferenceController;->getMainPreferences()Lcom/google/android/searchcommon/preferences/SharedPreferencesExt;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, Lcom/google/android/speech/embedded/Greco3Container;->create(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/util/concurrent/ExecutorService;)Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mGreco3Container:Lcom/google/android/speech/embedded/Greco3Container;

    return-object v0
.end method

.method public declared-synchronized getHeadsetAudioRecorderRouter()Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mHeadsetAudioRecorderRouter:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;-><init>(Landroid/content/Context;Landroid/media/AudioManager;Lcom/google/android/voicesearch/settings/Settings;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mHeadsetAudioRecorderRouter:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mHeadsetAudioRecorderRouter:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getHotwordDetector()Lcom/google/android/voicesearch/hotword/HotwordDetector;
    .locals 6

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/hotword/HotwordDetector;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {v1}, Lcom/google/android/voicesearch/settings/SettingsUtils;->getSpokenLocaleBcp47Supplier(Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/common/base/Supplier;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/hotword/HotwordDetector;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;Landroid/content/Context;Lcom/google/android/voicesearch/settings/Settings;Ljava/util/concurrent/Executor;Lcom/google/common/base/Supplier;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mHotwordDetector:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    return-object v0
.end method

.method public getLangugageDownloadHelper()Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;
    .locals 4

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mLanguageDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3Preferences()Lcom/google/android/speech/embedded/Greco3Preferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->create(Landroid/app/DownloadManager;Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3Preferences;Lcom/google/android/voicesearch/settings/Settings;)Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mLanguageDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mLanguageDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    return-object v0
.end method

.method public getLocalTtsManager()Lcom/google/android/voicesearch/util/LocalTtsManager;
    .locals 5

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/util/LocalTtsManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/util/LocalTtsManager;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/voicesearch/settings/Settings;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    return-object v0
.end method

.method public getMainThreadExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method public getNetworkInformation()Lcom/google/android/speech/utils/NetworkInformation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v0

    return-object v0
.end method

.method public getOfflineActionsManager()Lcom/google/android/speech/embedded/OfflineActionsManager;
    .locals 5

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/speech/embedded/OfflineActionsManager;-><init>(Landroid/content/Context;Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/SpeechSettings;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    return-object v0
.end method

.method public getPersonalizationPrefManager()Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;
    .locals 5

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mPreferenceController:Lcom/google/android/searchcommon/GsaPreferenceController;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v4}, Lcom/google/android/searchcommon/CoreSearchServices;->getNetworkInfo()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/personalization/PersonalizationPrefManagerImpl;-><init>(Lcom/google/android/searchcommon/GsaPreferenceController;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/helper/AccountHelper;Lcom/google/android/speech/utils/NetworkInformation;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mPersonalizationPrefManager:Lcom/google/android/voicesearch/personalization/PersonalizationPrefManager;

    return-object v0
.end method

.method public getRecognizer()Lcom/google/android/speech/Recognizer;
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mRecognizer:Lcom/google/android/speech/Recognizer;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->createRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mRecognizer:Lcom/google/android/speech/Recognizer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mRecognizer:Lcom/google/android/speech/Recognizer;

    return-object v0
.end method

.method public declared-synchronized getRecognizerParamsFactory()Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mAccountHelper:Lcom/google/android/speech/helper/AccountHelper;

    iget-object v4, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v5, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;-><init>(Landroid/content/Context;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/helper/AccountHelper;Ljava/util/concurrent/ExecutorService;Lcom/google/android/searchcommon/CoreSearchServices;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getScheduledExecutorService()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public getSettings()Lcom/google/android/voicesearch/settings/Settings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-object v0
.end method

.method public getSoundManager()Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    .locals 4

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;-><init>(Landroid/content/Context;ILjava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    return-object v0
.end method

.method public getSpeechLevelSource()Lcom/google/android/speech/SpeechLevelSource;
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/speech/SpeechLevelSource;

    invoke-direct {v0}, Lcom/google/android/speech/SpeechLevelSource;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSpeechLevelSource:Lcom/google/android/speech/SpeechLevelSource;

    return-object v0
.end method

.method public getSuggestionLogger()Lcom/google/android/speech/logger/SuggestionLogger;
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/speech/logger/SuggestionLogger;

    invoke-direct {v0}, Lcom/google/android/speech/logger/SuggestionLogger;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

    return-object v0
.end method

.method public getTextRecognizer()Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;
    .locals 1

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mTextRecognizer:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/VoiceSearchServices;->createTextRecognizer()Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mTextRecognizer:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mTextRecognizer:Lcom/google/android/voicesearch/speechservice/s3/TextRecognizer;

    return-object v0
.end method

.method public getTtsAudioPlayer()Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .locals 3

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mTtsAudioPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {v1, v0, v2}, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;-><init>(Landroid/media/AudioManager;Ljava/util/concurrent/Executor;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mTtsAudioPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mTtsAudioPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    return-object v0
.end method

.method public declared-synchronized getVoiceImeSubtypeUpdater()Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mVoiceImeSubtypeUpdater:Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    iget-object v1, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mVoiceImeSubtypeUpdater:Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mVoiceImeSubtypeUpdater:Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public init()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/VoiceSearchServices;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->asyncLoad()V

    return-void
.end method
