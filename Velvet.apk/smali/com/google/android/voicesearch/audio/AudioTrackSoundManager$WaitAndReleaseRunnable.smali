.class final Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;
.super Ljava/lang/Object;
.source "AudioTrackSoundManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "WaitAndReleaseRunnable"
.end annotation


# instance fields
.field final mAudioTrack:Landroid/media/AudioTrack;

.field final mLengthBytes:I


# direct methods
.method constructor <init>(Landroid/media/AudioTrack;I)V
    .locals 0
    .param p1    # Landroid/media/AudioTrack;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;->mAudioTrack:Landroid/media/AudioTrack;

    iput p2, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;->mLengthBytes:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const-wide/16 v4, 0x1f4

    iget v2, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;->mLengthBytes:I

    div-int/lit8 v10, v2, 0x2

    const/4 v11, -0x1

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v8

    if-ge v8, v10, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    sub-int v2, v10, v8

    mul-int/lit16 v2, v2, 0x3e8

    div-int/lit16 v2, v2, 0x3e80

    int-to-long v0, v2

    const-wide/16 v2, 0x32

    # invokes: Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->clip(JJJ)J
    invoke-static/range {v0 .. v5}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->access$100(JJJ)J

    move-result-wide v12

    if-ne v8, v11, :cond_1

    add-long/2addr v6, v12

    cmp-long v2, v6, v4

    if-lez v2, :cond_2

    const-string v2, "AudioTrackSoundManager"

    const-string v3, "Waited unsuccessfully for 500ms for AudioTrack to make progress, Aborting"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager$WaitAndReleaseRunnable;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->release()V

    return-void

    :cond_1
    const-wide/16 v6, 0x0

    :cond_2
    move v11, v8

    :try_start_0
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v9

    goto :goto_1
.end method
