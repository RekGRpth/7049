.class public Lcom/google/android/voicesearch/audio/ByteArrayPlayer;
.super Ljava/lang/Object;
.source "ByteArrayPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;
    }
.end annotation


# instance fields
.field private final mAudioBytes:[B

.field private final mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private volatile mCallback:Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

.field private final mDone:Landroid/os/ConditionVariable;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private mPlayer:Landroid/media/MediaPlayer;

.field private final mRunnerThread:Ljava/lang/Thread;

.field private final mStartPlayback:Landroid/os/ConditionVariable;

.field private volatile mStopped:Z


# direct methods
.method public constructor <init>(Landroid/media/AudioManager;[BLjava/util/concurrent/Executor;)V
    .locals 2
    .param p1    # Landroid/media/AudioManager;
    .param p2    # [B
    .param p3    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$1;-><init>(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mAudioBytes:[B

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mStartPlayback:Landroid/os/ConditionVariable;

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mDone:Landroid/os/ConditionVariable;

    iput-object p1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mAudioManager:Landroid/media/AudioManager;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$2;-><init>(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mRunnerThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mRunnerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    invoke-direct {p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->doRun()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mDone:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mCallback:Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

    return-object v0
.end method

.method private buildDataUri()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "data:;base64,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mAudioBytes:[B

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private doRun()V
    .locals 5

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    :try_start_0
    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->buildDataUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$3;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$3;-><init>(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$4;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$4;-><init>(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mStartPlayback:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    iget-boolean v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mStopped:Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->finish()V

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$5;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$5;-><init>(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "VS.ByteArrayPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "I/O Exception initializing media player :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->finish()V

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mDone:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->finish()V

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$5;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$5;-><init>(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-direct {p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->finish()V

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$5;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$5;-><init>(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-direct {p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->finish()V

    iget-object v2, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$5;

    invoke-direct {v3, p0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$5;-><init>(Lcom/google/android/voicesearch/audio/ByteArrayPlayer;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    throw v1
.end method

.method private finish()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public start(Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mCallback:Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mStartPlayback:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method

.method public stop()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mStopped:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mDone:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->mStartPlayback:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method
