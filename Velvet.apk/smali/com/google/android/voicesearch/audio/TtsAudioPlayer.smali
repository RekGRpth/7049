.class public Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
.super Ljava/lang/Object;
.source "TtsAudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/audio/TtsAudioPlayer$Callback;
    }
.end annotation


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

.field private mCallback:Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private mPlayAudioWhenReady:Z


# direct methods
.method public constructor <init>(Landroid/media/AudioManager;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Landroid/media/AudioManager;
    .param p2    # Ljava/util/concurrent/Executor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mExecutor:Ljava/util/concurrent/Executor;

    iput-object p1, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mPlayAudioWhenReady:Z

    return-void
.end method


# virtual methods
.method public requestPlayback(Lcom/google/android/voicesearch/audio/TtsAudioPlayer$Callback;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer$Callback;

    iput-object p1, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mCallback:Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mPlayAudioWhenReady:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mCallback:Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->start(Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;)V

    :cond_0
    return-void
.end method

.method public setAudio([B)V
    .locals 3
    .param p1    # [B

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    new-instance v0, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;-><init>(Landroid/media/AudioManager;[BLjava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mPlayAudioWhenReady:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    iget-object v1, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mCallback:Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->start(Lcom/google/android/voicesearch/audio/ByteArrayPlayer$Callback;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopAudioPlayback()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/ByteArrayPlayer;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mByteArrayPlayer:Lcom/google/android/voicesearch/audio/ByteArrayPlayer;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->mPlayAudioWhenReady:Z

    return-void
.end method
