.class Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$1;
.super Ljava/lang/Object;
.source "BluetoothHeadsetQuery.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->getPairedHeadset(Landroid/content/Context;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;

.field final synthetic val$pairedBTHeadsetFuture:Lcom/google/common/util/concurrent/SettableFuture;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$1;->this$0:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;

    iput-object p2, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$1;->val$pairedBTHeadsetFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 4
    .param p1    # I
    .param p2    # Landroid/bluetooth/BluetoothProfile;

    move-object v0, p2

    check-cast v0, Landroid/bluetooth/BluetoothHeadset;

    # getter for: Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->ALL_HEADSET_STATES:[I
    invoke-static {}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->access$000()[I

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothHeadset;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$1;->val$pairedBTHeadsetFuture:Lcom/google/common/util/concurrent/SettableFuture;

    sget-object v3, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->NOT_FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    invoke-virtual {v2, v3}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    :goto_0
    iget-object v2, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$1;->this$0:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;

    # getter for: Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->access$100(Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$1;->val$pairedBTHeadsetFuture:Lcom/google/common/util/concurrent/SettableFuture;

    sget-object v3, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    invoke-virtual {v2, v3}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
