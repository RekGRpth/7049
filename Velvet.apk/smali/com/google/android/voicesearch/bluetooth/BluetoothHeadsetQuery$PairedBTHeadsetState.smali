.class public final enum Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;
.super Ljava/lang/Enum;
.source "BluetoothHeadsetQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PairedBTHeadsetState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

.field public static final enum DISABLED:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

.field public static final enum FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

.field public static final enum NOT_FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

.field public static final enum NOT_SUPPORTED:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    const-string v1, "NOT_SUPPORTED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->NOT_SUPPORTED:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    new-instance v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->DISABLED:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    new-instance v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    const-string v1, "NOT_FOUND"

    invoke-direct {v0, v1, v4}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->NOT_FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    new-instance v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    const-string v1, "FOUND"

    invoke-direct {v0, v1, v5}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    sget-object v1, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->NOT_SUPPORTED:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->DISABLED:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->NOT_FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->FOUND:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->$VALUES:[Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->$VALUES:[Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    invoke-virtual {v0}, [Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    return-object v0
.end method
