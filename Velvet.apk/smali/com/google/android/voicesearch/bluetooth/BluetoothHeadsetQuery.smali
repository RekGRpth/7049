.class public Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;
.super Ljava/lang/Object;
.source "BluetoothHeadsetQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;
    }
.end annotation


# static fields
.field private static final ALL_HEADSET_STATES:[I


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mExecutorService:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->ALL_HEADSET_STATES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x1
        0x0
        0x3
    .end array-data
.end method

.method public constructor <init>(Landroid/media/AudioManager;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .param p1    # Landroid/media/AudioManager;
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mAudioManager:Landroid/media/AudioManager;

    iput-object p2, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    :goto_1
    return-void

    :cond_0
    iput-object v1, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    goto :goto_0

    :cond_1
    iput-object v1, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mAudioManager:Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    goto :goto_1
.end method

.method static synthetic access$000()[I
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->ALL_HEADSET_STATES:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method private static final threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/google/android/searchcommon/util/ThreadChanger;->createNonBlockingThreadChangeProxy(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getPairedHeadset(Landroid/content/Context;)Ljava/util/concurrent/Future;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->NOT_SUPPORTED:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;->DISABLED:Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$PairedBTHeadsetState;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->mExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    const-class v3, Landroid/bluetooth/BluetoothProfile$ServiceListener;

    new-instance v4, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$1;

    invoke-direct {v4, p0, v1}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery$1;-><init>(Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-static {v0, v3, v4}, Lcom/google/android/voicesearch/bluetooth/BluetoothHeadsetQuery;->threadChange(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v0, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-object v0, v1

    goto :goto_0
.end method
