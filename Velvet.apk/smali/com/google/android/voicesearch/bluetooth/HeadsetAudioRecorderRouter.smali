.class public Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;
.super Ljava/lang/Object;
.source "HeadsetAudioRecorderRouter.java"

# interfaces
.implements Lcom/google/android/voicesearch/AudioRecorderRouter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;
    }
.end annotation


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mBluetoothEnabled:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mScoBroadcastReceiver:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;

.field private mScoState:I

.field private mScoStateLastUpdate:I

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/media/AudioManager;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/media/AudioManager;
    .param p3    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->createBluetoothEnableCallable()Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;-><init>(Landroid/content/Context;Landroid/media/AudioManager;Ljava/util/concurrent/Callable;Lcom/google/android/voicesearch/settings/Settings;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/media/AudioManager;Ljava/util/concurrent/Callable;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/media/AudioManager;
    .param p4    # Lcom/google/android/voicesearch/settings/Settings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/media/AudioManager;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/voicesearch/settings/Settings;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    invoke-virtual {p2}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v0

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/settings/Settings;

    iput-object v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_1

    const-string v1, "VS.HeadsetAudioRecorderRouter"

    const-string v2, "BT Sco is not available"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mBluetoothEnabled:Ljava/util/concurrent/Callable;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mBluetoothEnabled:Ljava/util/concurrent/Callable;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;

    invoke-direct {v1, p0, v3}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;-><init>(Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$1;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoBroadcastReceiver:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoBroadcastReceiver:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_1
    return-void

    :cond_1
    iput-object p3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mBluetoothEnabled:Ljava/util/concurrent/Callable;

    iget-object v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mBluetoothEnabled:Ljava/util/concurrent/Callable;

    if-nez v1, :cond_0

    const-string v1, "VS.HeadsetAudioRecorderRouter"

    const-string v2, "BT adapter is not available"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iput-object v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoBroadcastReceiver:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;

    goto :goto_1
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;I)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->updateScoState(I)V

    return-void
.end method

.method private static createBluetoothEnableCallable()Ljava/util/concurrent/Callable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$1;

    invoke-direct {v1, v0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$1;-><init>(Landroid/bluetooth/BluetoothAdapter;)V

    goto :goto_0
.end method

.method private static getScoStateLabel(I)Ljava/lang/String;
    .locals 2
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "SCO_AUDIO_STATE_CONNECTED"

    goto :goto_0

    :pswitch_1
    const-string v0, "SCO_AUDIO_STATE_CONNECTING"

    goto :goto_0

    :pswitch_2
    const-string v0, "SCO_AUDIO_STATE_DISCONNECTED"

    goto :goto_0

    :pswitch_3
    const-string v0, "SCO_AUDIO_STATE_ERROR"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private isConnectedToBluetoothHeadset()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isConnectedToWiredHeadset()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method private declared-synchronized startInternal()Z
    .locals 6

    const/4 v4, 0x0

    monitor-enter p0

    const-wide/16 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mBluetoothEnabled:Ljava/util/concurrent/Callable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    move v3, v4

    :goto_0
    monitor-exit p0

    return v3

    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/settings/Settings;->isBluetoothHeadsetEnabled()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    move v3, v4

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mBluetoothEnabled:Ljava/util/concurrent/Callable;

    invoke-interface {v3}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    move v3, v4

    goto :goto_0

    :catch_0
    move-exception v0

    move v3, v4

    goto :goto_0

    :cond_2
    const/16 v3, 0x9

    :try_start_3
    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordLatencyStart(I)V

    iget v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    if-nez v3, :cond_3

    const-string v3, "VS.HeadsetAudioRecorderRouter"

    const-string v4, "#start - starting sco"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->startBluetoothSco()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->waitForScoConnection()Z

    move-result v3

    goto :goto_0

    :cond_3
    iget v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_4

    const-string v3, "VS.HeadsetAudioRecorderRouter"

    const-string v4, "#start - waiting for sco"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->waitForScoConnection()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_4
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_4
    move v3, v4

    goto :goto_0
.end method

.method private declared-synchronized updateScoState(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    iget v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoStateLastUpdate:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoStateLastUpdate:I

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private waitForNextScoState(J)V
    .locals 5
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoStateLastUpdate:I

    :goto_0
    iget v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoStateLastUpdate:I

    if-ne v3, v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v1, p1, v3

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    invoke-virtual {p0, v1, v2}, Ljava/lang/Object;->wait(J)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private waitForScoConnection()Z
    .locals 11

    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    if-eq v3, v10, :cond_0

    iget v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    if-nez v3, :cond_3

    :cond_0
    move v3, v5

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x7d0

    add-long v1, v6, v8

    invoke-direct {p0, v1, v2}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->waitForNextScoState(J)V

    iget v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    if-ne v3, v10, :cond_1

    invoke-direct {p0, v1, v2}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->waitForNextScoState(J)V

    :cond_1
    iget v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    if-ne v3, v5, :cond_4

    const-string v3, "VS.HeadsetAudioRecorderRouter"

    const-string v6, "#waitForSco - done"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x6a

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    move v4, v5

    :cond_2
    :goto_1
    return v4

    :cond_3
    move v3, v4

    goto :goto_0

    :cond_4
    const-string v5, "VS.HeadsetAudioRecorderRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "#waitForSco - failed "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v6, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    invoke-static {v6}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->getScoStateLabel(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    cmp-long v3, v7, v1

    if-lez v3, :cond_5

    const-string v3, "timeout"

    :goto_2
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x6b

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoState:I

    if-ne v3, v10, :cond_2

    iget-object v3, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->stopBluetoothSco()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v3, "VS.HeadsetAudioRecorderRouter"

    const-string v5, "#waitForSco - failed"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :cond_5
    :try_start_1
    const-string v3, ""
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method


# virtual methods
.method public declared-synchronized destroy(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->requestStop()V

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoBroadcastReceiver:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mScoBroadcastReceiver:Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter$ScoBroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isConnectedToHeadset()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->isConnectedToWiredHeadset()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->isConnectedToBluetoothHeadset()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->isBluetoothHeadsetEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    :cond_0
    return-void
.end method

.method public shouldDisableExtraTts()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getTtsMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "handsFreeOnly"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->isConnectedToHeadset()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public shouldSetHandsFreeTtsMode()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getTtsMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "always"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "handsFreeOnly"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->isConnectedToHeadset()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public start()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;->startInternal()Z

    move-result v0

    return v0
.end method
