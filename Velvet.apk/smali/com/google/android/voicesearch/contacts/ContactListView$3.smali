.class Lcom/google/android/voicesearch/contacts/ContactListView$3;
.super Ljava/lang/Object;
.source "ContactListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/contacts/ContactListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/contacts/ContactListView;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/contacts/ContactListView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/contacts/ContactListView$3;->this$0:Lcom/google/android/voicesearch/contacts/ContactListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactListView$3;->this$0:Lcom/google/android/voicesearch/contacts/ContactListView;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactListView;->mListener:Lcom/google/android/voicesearch/contacts/ContactListView$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/contacts/ContactListView;->access$000(Lcom/google/android/voicesearch/contacts/ContactListView;)Lcom/google/android/voicesearch/contacts/ContactListView$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactListView$3;->this$0:Lcom/google/android/voicesearch/contacts/ContactListView;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactListView;->mListener:Lcom/google/android/voicesearch/contacts/ContactListView$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/contacts/ContactListView;->access$000(Lcom/google/android/voicesearch/contacts/ContactListView;)Lcom/google/android/voicesearch/contacts/ContactListView$Listener;

    move-result-object v0

    check-cast p1, Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->getContact()Lcom/google/android/speech/contacts/Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/contacts/ContactListView$Listener;->onActionButtonClicked(Lcom/google/android/speech/contacts/Contact;)V

    :cond_0
    return-void
.end method
