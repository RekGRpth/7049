.class public interface abstract Lcom/google/android/voicesearch/contacts/ContactListView$Listener;
.super Ljava/lang/Object;
.source "ContactListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/contacts/ContactListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onActionButtonClicked(Lcom/google/android/speech/contacts/Contact;)V
.end method

.method public abstract onContactSelected(Lcom/google/android/speech/contacts/Contact;)V
.end method

.method public abstract onContactTouched(Lcom/google/android/speech/contacts/Contact;)V
.end method
