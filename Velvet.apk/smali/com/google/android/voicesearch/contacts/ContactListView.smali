.class public Lcom/google/android/voicesearch/contacts/ContactListView;
.super Landroid/widget/LinearLayout;
.source "ContactListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/contacts/ContactListView$Listener;
    }
.end annotation


# instance fields
.field private final mClickButtonListener:Landroid/view/View$OnClickListener;

.field private final mClickContactListener:Landroid/view/View$OnClickListener;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private mListener:Lcom/google/android/voicesearch/contacts/ContactListView$Listener;

.field private final mTouchContactListener:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/voicesearch/contacts/ContactListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/contacts/ContactListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/contacts/ContactListView$1;-><init>(Lcom/google/android/voicesearch/contacts/ContactListView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mClickContactListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactListView$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/contacts/ContactListView$2;-><init>(Lcom/google/android/voicesearch/contacts/ContactListView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mTouchContactListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/google/android/voicesearch/contacts/ContactListView$3;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/contacts/ContactListView$3;-><init>(Lcom/google/android/voicesearch/contacts/ContactListView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mClickButtonListener:Landroid/view/View$OnClickListener;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/contacts/ContactListView;->setOrientation(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/contacts/ContactListView;)Lcom/google/android/voicesearch/contacts/ContactListView$Listener;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/contacts/ContactListView;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mListener:Lcom/google/android/voicesearch/contacts/ContactListView$Listener;

    return-object v0
.end method


# virtual methods
.method public setContactSelectedListener(Lcom/google/android/voicesearch/contacts/ContactListView$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/contacts/ContactListView$Listener;

    iput-object p1, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mListener:Lcom/google/android/voicesearch/contacts/ContactListView$Listener;

    return-void
.end method

.method public setContacts([Lcom/google/android/speech/contacts/Contact;IIIZ)V
    .locals 5
    .param p1    # [Lcom/google/android/speech/contacts/Contact;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/contacts/ContactListView;->removeAllViews()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p4, :cond_1

    array-length v3, p1

    if-ge v1, v3, :cond_1

    aget-object v0, p1, v1

    iget-object v3, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/4 v4, 0x0

    invoke-virtual {v3, p2, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/contacts/ContactSelectItem;

    invoke-virtual {v2, v0, p3}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setContact(Lcom/google/android/speech/contacts/Contact;I)V

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/contacts/ContactListView;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mClickContactListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mTouchContactListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    if-eqz p5, :cond_0

    iget-object v3, p0, Lcom/google/android/voicesearch/contacts/ContactListView;->mClickButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/contacts/ContactSelectItem;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
