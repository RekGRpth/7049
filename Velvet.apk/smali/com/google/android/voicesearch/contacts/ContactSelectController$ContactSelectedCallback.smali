.class public interface abstract Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;
.super Ljava/lang/Object;
.source "ContactSelectController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/contacts/ContactSelectController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactSelectedCallback"
.end annotation


# virtual methods
.method public abstract onBailOut()V
.end method

.method public abstract onContactSelected(Lcom/google/android/speech/contacts/Contact;Z)V
.end method

.method public abstract onNoContactFound()V
.end method

.method public abstract onShowingDisambiguation()V
.end method
