.class Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;
.super Landroid/os/AsyncTask;
.source "ContactSelectController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/contacts/ContactSelectController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FetchContactsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/speech/contacts/Contact;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mContactName:Ljava/lang/String;

.field private final mPreferredContactType:Ljava/lang/String;

.field private final mRecognizedContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/contacts/ContactSelectController;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->mPreferredContactType:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->mRecognizedContacts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 4
    .param p1    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->mRecognizedContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->mRecognizedContacts:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getContactName(Lcom/google/majel/proto/ActionV2Protos$ActionContact;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->mContactName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;
    invoke-static {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->access$100(Lcom/google/android/voicesearch/contacts/ContactSelectController;)Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectController;->mMode:Lcom/google/android/voicesearch/contacts/ContactSelectMode;
    invoke-static {v1}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->access$000(Lcom/google/android/voicesearch/contacts/ContactSelectController;)Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->getContactLookupMode()Lcom/google/android/speech/contacts/ContactLookup$Mode;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->mRecognizedContacts:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->mPreferredContactType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/speech/contacts/ContactLookup;->findAllByDisplayName(Lcom/google/android/speech/contacts/ContactLookup$Mode;Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    # invokes: Lcom/google/android/voicesearch/contacts/ContactSelectController;->showNoCard()V
    invoke-static {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->access$200(Lcom/google/android/voicesearch/contacts/ContactSelectController;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;
    invoke-static {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->access$300(Lcom/google/android/voicesearch/contacts/ContactSelectController;)Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;->onNoContactFound()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    # invokes: Lcom/google/android/voicesearch/contacts/ContactSelectController;->showNoCard()V
    invoke-static {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->access$400(Lcom/google/android/voicesearch/contacts/ContactSelectController;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    # getter for: Lcom/google/android/voicesearch/contacts/ContactSelectController;->mContactCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;
    invoke-static {v0}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->access$300(Lcom/google/android/voicesearch/contacts/ContactSelectController;)Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    move-result-object v1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-interface {v1, v0, v2}, Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;->onContactSelected(Lcom/google/android/speech/contacts/Contact;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->this$0:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iget-object v1, p0, Lcom/google/android/voicesearch/contacts/ContactSelectController$FetchContactsTask;->mContactName:Ljava/lang/String;

    # invokes: Lcom/google/android/voicesearch/contacts/ContactSelectController;->selectContact(Ljava/lang/String;Ljava/util/List;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->access$500(Lcom/google/android/voicesearch/contacts/ContactSelectController;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method
