.class public abstract Lcom/google/android/voicesearch/fragments/AbstractCardController;
.super Ljava/lang/Object;
.source "AbstractCardController.java"

# interfaces
.implements Lcom/google/android/searchcommon/util/IntentStarter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CARD_UI::",
        "Lcom/google/android/voicesearch/fragments/BaseCardUi;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/searchcommon/util/IntentStarter;"
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mAction:Lcom/google/android/velvet/presenter/Action;

.field private final mCardController:Lcom/google/android/voicesearch/CardController;

.field private mCountDownDuration:J

.field private mCountDownStatus:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

.field private final mExecuteActionRunnable:Ljava/lang/Runnable;

.field private mIsFallback:Z

.field private final mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private mReady:Z

.field private mShowCard:Z

.field private mStartTtsOnAttach:Z

.field private final mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

.field private mUi:Lcom/google/android/voicesearch/fragments/BaseCardUi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TCARD_UI;"
        }
    .end annotation
.end field

.field private mWaitingForShowCard:Z


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AbstractCardController."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->TAG:Ljava/lang/String;

    sget-object v0, Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;->ENABLED:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownStatus:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {p1}, Lcom/google/android/voicesearch/CardController;->getAction()Lcom/google/android/velvet/presenter/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    new-instance v0, Lcom/google/android/voicesearch/fragments/AbstractCardController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController$1;-><init>(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mExecuteActionRunnable:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/Action;->addCardController(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->internalTtsPlaybackComplete()V

    return-void
.end method

.method private internalTtsPlaybackComplete()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->onTtsPlaybackComplete()V

    return-void
.end method

.method private onReady(ZZ)V
    .locals 3
    .param p1    # Z
    .param p2    # Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mWaitingForShowCard:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mReady:Z

    iget-boolean v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mShowCard:Z

    if-eq p1, v1, :cond_1

    iput-boolean p1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mShowCard:Z

    iget-boolean v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mShowCard:Z

    if-eqz v1, :cond_1

    iput-boolean p2, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mStartTtsOnAttach:Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1}, Lcom/google/android/velvet/presenter/Action;->getActionParserLog()I

    move-result v0

    const/16 v1, 0x5d

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    const v2, 0x8000

    invoke-virtual {v1, v2}, Lcom/google/android/velvet/presenter/Action;->setPumpkinLoggableEvent(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getActionTypeLog()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v1, p0}, Lcom/google/android/velvet/presenter/Action;->onCardControllerReady(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V

    return-void
.end method

.method private startCountDown()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    sget-object v0, Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;->STARTED:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownStatus:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mExecuteActionRunnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownDuration:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getCountDownUi()Lcom/google/android/voicesearch/fragments/CountDownUi;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownDuration:J

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/CountDownUi;->startCountDownAnimation(J)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tryStartCountDown()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->canExecuteAction()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownStatus:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    sget-object v1, Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;->ENABLED:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->shouldAutoExecute()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {v0}, Lcom/google/android/voicesearch/CardController;->takeStartCountdown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->getCountdownDurationMs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownDuration:J

    iget-wide v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownDuration:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getCardController()Lcom/google/android/voicesearch/CardController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/CardController;->getDefaultCountdownDurationMs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownDuration:J

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->startCountDown()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected actionComplete()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/Action;->onCardActionComplete(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->clearCard()V

    return-void
.end method

.method public attach(Lcom/google/android/voicesearch/fragments/BaseCardUi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCARD_UI;)V"
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->TAG:Ljava/lang/String;

    const-string v2, "#attach"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mWaitingForShowCard:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getScreenTypeLog()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getActionTypeLog()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordScreen(ILjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->setUi(Lcom/google/android/voicesearch/fragments/BaseCardUi;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->initUi()V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mStartTtsOnAttach:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mStartTtsOnAttach:Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->startTtsPlayback()V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bailOut()V
    .locals 2

    const/16 v0, 0x32

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getActionTypeLog()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->setGwsLoggableEvent(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancelCountDown()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->internalBailOut()V

    return-void
.end method

.method protected canExecuteAction()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public cancel()V
    .locals 0

    return-void
.end method

.method public cancelCountDown()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownStatus:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    sget-object v1, Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;->STARTED:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;->FINISHED:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownStatus:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mExecuteActionRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getCountDownUi()Lcom/google/android/voicesearch/fragments/CountDownUi;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/CountDownUi;->cancelCountDownAnimation()V

    :cond_0
    return-void
.end method

.method public cancelCountDownByUser()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownStatus:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    sget-object v1, Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;->STARTED:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    if-ne v0, v1, :cond_0

    const/16 v0, 0x48

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getActionTypeLog()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->suppressGwsLoggableEvent(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->setGwsLoggableEvent(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancelCountDown()V

    :cond_0
    return-void
.end method

.method protected clearCard()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/presenter/Action;->removeCardController(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V

    return-void
.end method

.method protected destroyUi()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->setUi(Lcom/google/android/voicesearch/fragments/BaseCardUi;)V

    return-void
.end method

.method public detach()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancelCountDown()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->destroyUi()V

    return-void
.end method

.method protected disableCountDown()V
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;->DISABLED:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCountDownStatus:Lcom/google/android/voicesearch/fragments/AbstractCardController$CountDownStatus;

    return-void
.end method

.method public dismissed()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->logDismissed()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->clearCard()V

    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintWriter;

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Nothing to dump from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method public final executeAction(Z)V
    .locals 2
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->canExecuteAction()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getActionTypeLog()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v1, 0x200

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->setGwsLoggableEvent(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancelCountDown()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->internalExecuteAction()V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->setGwsLoggableEvent(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->bailOut()V

    goto :goto_1
.end method

.method protected getActionState()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract getActionTypeLog()I
.end method

.method public getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/CardController;->getActivityIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getCardController()Lcom/google/android/voicesearch/CardController;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    return-object v0
.end method

.method protected final getCountDownUi()Lcom/google/android/voicesearch/fragments/CountDownUi;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mUi:Lcom/google/android/voicesearch/fragments/BaseCardUi;

    check-cast v0, Lcom/google/android/voicesearch/fragments/CountDownUi;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInstanceState()Landroid/os/Bundle;
    .locals 4

    const/4 v1, 0x0

    iget-boolean v3, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mIsFallback:Z

    if-nez v3, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getActionState()[B

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "velvet:action"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getParcelableState()Landroid/os/Parcelable;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "velvet:parcelable"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-object v1
.end method

.method public getLastAudioUri(Lcom/google/android/speech/audio/AudioUtils$Encoding;)Landroid/net/Uri;
    .locals 1
    .param p1    # Lcom/google/android/speech/audio/AudioUtils$Encoding;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/CardController;->getLastAudioUri(Lcom/google/android/speech/audio/AudioUtils$Encoding;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected getMainThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mMainThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    return-object v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final getQueryState()Lcom/google/android/velvet/presenter/QueryState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {v0}, Lcom/google/android/voicesearch/CardController;->getQueryState()Lcom/google/android/velvet/presenter/QueryState;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getScreenTypeLog()I
.end method

.method protected getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TCARD_UI;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mUi:Lcom/google/android/voicesearch/fragments/BaseCardUi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mUi:Lcom/google/android/voicesearch/fragments/BaseCardUi;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initUi()V
    .locals 0

    return-void
.end method

.method protected internalBailOut()V
    .locals 0

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 0

    return-void
.end method

.method public isAttached()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mUi:Lcom/google/android/voicesearch/fragments/BaseCardUi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mReady:Z

    return v0
.end method

.method protected logDismissed()V
    .locals 2

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->getActionTypeLog()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->suppressGwsLoggableEvent(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/presenter/Action;->setGwsLoggableEvent(I)V

    return-void
.end method

.method protected onTtsPlaybackComplete()V
    .locals 0

    return-void
.end method

.method protected parseActionState([B)V
    .locals 0
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    return-void
.end method

.method protected requireShowCard()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mWaitingForShowCard:Z

    return-void
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    iget-boolean v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mIsFallback:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    if-eqz p1, :cond_1

    const-string v1, "velvet:action"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    const-string v1, "velvet:action"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->parseActionState([B)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const-string v1, "velvet:parcelable"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "velvet:parcelable"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->setParcelableState(Landroid/os/Parcelable;)V

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->TAG:Ljava/lang/String;

    const-string v2, "Error in restoring the state"

    invoke-static {v1, v2, v0}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public restoreStart()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->showCard()V

    return-void
.end method

.method protected final setFallbackController()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mIsFallback:Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->showNoCard()V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;

    return-void
.end method

.method protected setUi(Lcom/google/android/voicesearch/fragments/BaseCardUi;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCARD_UI;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mUi:Lcom/google/android/voicesearch/fragments/BaseCardUi;

    return-void
.end method

.method public shouldShowCard()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->isReady()Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mShowCard:Z

    return v0
.end method

.method protected final showCard()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->onReady(ZZ)V

    return-void
.end method

.method protected final showCardAndPlayTts()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->onReady(ZZ)V

    return-void
.end method

.method protected final showNoCard()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->onReady(ZZ)V

    return-void
.end method

.method public startActivity(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {v0}, Lcom/google/android/voicesearch/CardController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/CardController;->startActivity(Landroid/content/Intent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->TAG:Ljava/lang/String;

    const-string v1, "Failed to start activity"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {v0}, Lcom/google/android/voicesearch/CardController;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mCardController:Lcom/google/android/voicesearch/CardController;

    invoke-interface {v0, p1, p2}, Lcom/google/android/voicesearch/CardController;->startActivityForResult(Landroid/content/Intent;Lcom/google/android/searchcommon/util/IntentStarter$ResultCallback;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->TAG:Ljava/lang/String;

    const-string v1, "Failed to start activity"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected startTtsPlayback()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mAction:Lcom/google/android/velvet/presenter/Action;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/Action;->canPlayTts()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardController;->mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    new-instance v1, Lcom/google/android/voicesearch/fragments/AbstractCardController$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController$2;-><init>(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->requestPlayback(Lcom/google/android/voicesearch/audio/TtsAudioPlayer$Callback;)V

    :cond_0
    return-void
.end method

.method public uiReady()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->tryStartCountDown()V

    return-void
.end method
