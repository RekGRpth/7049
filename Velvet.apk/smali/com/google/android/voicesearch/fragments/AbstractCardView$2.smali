.class Lcom/google/android/voicesearch/fragments/AbstractCardView$2;
.super Ljava/lang/Object;
.source "AbstractCardView.java"

# interfaces
.implements Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleAttach()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/AbstractCardView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$2;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBailOut()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$2;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    # getter for: Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->access$100(Lcom/google/android/voicesearch/fragments/AbstractCardView;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->bailOut()V

    return-void
.end method

.method public onCancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$2;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    # getter for: Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->access$100(Lcom/google/android/voicesearch/fragments/AbstractCardView;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancel()V

    return-void
.end method

.method public onCancelCountdown()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$2;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    # getter for: Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->access$100(Lcom/google/android/voicesearch/fragments/AbstractCardView;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancelCountDownByUser()V

    return-void
.end method

.method public onExecute()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView$2;->this$0:Lcom/google/android/voicesearch/fragments/AbstractCardView;

    # getter for: Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->access$100(Lcom/google/android/voicesearch/fragments/AbstractCardView;)Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->executeAction(Z)V

    return-void
.end method
