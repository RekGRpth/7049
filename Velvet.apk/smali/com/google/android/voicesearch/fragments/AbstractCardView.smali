.class public abstract Lcom/google/android/voicesearch/fragments/AbstractCardView;
.super Landroid/widget/FrameLayout;
.source "AbstractCardView.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTRO",
        "L:Lcom/google/android/voicesearch/fragments/AbstractCardController;",
        ">",
        "Landroid/widget/FrameLayout;",
        "Lcom/google/android/voicesearch/fragments/BaseCardUi;"
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

.field private mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TCONTRO",
            "L;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AbstractCardView."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->TAG:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->onCreateView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/AbstractCardView;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/AbstractCardView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/AbstractCardView;)Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/AbstractCardView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    return-object v0
.end method

.method public static varargs clearTextViews([Landroid/widget/TextView;)V
    .locals 5
    .param p0    # [Landroid/widget/TextView;

    move-object v0, p0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private onCreateView()Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v3, "layout_inflater"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v1, p0, v3}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/fragments/AbstractCardView$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView$1;-><init>(Lcom/google/android/voicesearch/fragments/AbstractCardView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-object v0
.end method


# virtual methods
.method public cancelCountDownAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->cancelCountDownAnimation()V

    return-void
.end method

.method protected createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # I

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;IIZ)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    return-object v0
.end method

.method protected createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;IIZ)Lcom/google/android/voicesearch/ui/ActionEditorView;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    invoke-virtual {p0, p5}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->getActionEditorViewLayout(Z)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/ActionEditorView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v1, p3}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setMainContent(I)V

    const/4 v1, -0x1

    if-eq p4, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v1, p4}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setPanelContent(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    return-object v1
.end method

.method public dismissed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->dismissed()V

    return-void
.end method

.method protected getActionEditorViewLayout(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const v0, 0x7f040001

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x7f040000

    goto :goto_0
.end method

.method public getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TCONTRO",
            "L;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AbstractCardController;

    return-object v0
.end method

.method public getPendingLayoutView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public handleAttach()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    new-instance v1, Lcom/google/android/voicesearch/fragments/AbstractCardView$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView$2;-><init>(Lcom/google/android/voicesearch/fragments/AbstractCardView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setActionEditorListener(Lcom/google/android/voicesearch/ui/ActionEditorView$ActionEditorListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->attach(Lcom/google/android/voicesearch/fragments/BaseCardUi;)V

    :cond_1
    return-void
.end method

.method public handleDetach()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->detach()V

    :cond_0
    return-void
.end method

.method public abstract onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowVisibilityChanged(I)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->cancelCountDown()V

    :cond_0
    return-void
.end method

.method public setConfirmIcon(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmIcon(I)V

    return-void
.end method

.method public setConfirmText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmText(I)V

    return-void
.end method

.method public varargs setConfirmText(I[Ljava/lang/Object;)V
    .locals 2
    .param p1    # I
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmText(Ljava/lang/String;)V

    return-void
.end method

.method public final setController(Lcom/google/android/voicesearch/fragments/AbstractCardController;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCONTRO",
            "L;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mController:Lcom/google/android/voicesearch/fragments/AbstractCardController;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->TAG:Ljava/lang/String;

    const-string v1, "#handleAttach - setController"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleAttach()V

    :cond_0
    return-void
.end method

.method protected showTextIfNonEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startCountDownAnimation(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AbstractCardView;->mActionEditorView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->startCountDownAnimation(J)V

    return-void
.end method
