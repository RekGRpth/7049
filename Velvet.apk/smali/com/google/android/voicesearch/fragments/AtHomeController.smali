.class public Lcom/google/android/voicesearch/fragments/AtHomeController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "AtHomeController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/AtHomeController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/AtHomeController$Ui;",
        ">;"
    }
.end annotation


# static fields
.field static final ACTION_OFF:Ljava/lang/String; = "com.android.athome.action.OFF"

.field static final ACTION_ON:Ljava/lang/String; = "com.android.athome.action.ON"

.field static final EXTRA_DEVICE_NAME:Ljava/lang/String; = "device_name"


# instance fields
.field private mLevel:I

.field private mTarget:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method

.method private isTurnOnAction()Z
    .locals 2

    iget v0, p0, Lcom/google/android/voicesearch/fragments/AtHomeController;->mLevel:I

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method createPowerControlIntent(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    new-instance v0, Landroid/content/Intent;

    if-eqz p2, :cond_1

    const-string v1, "com.android.athome.action.ON"

    :goto_0
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v1, "device_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0

    :cond_1
    const-string v1, "com.android.athome.action.OFF"

    goto :goto_0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x1d

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x21

    return v0
.end method

.method protected initUi()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/AtHomeController;->isTurnOnAction()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AtHomeController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AtHomeController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AtHomeController;->mTarget:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/AtHomeController$Ui;->setShowTurnOnLights(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AtHomeController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/AtHomeController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/AtHomeController;->mTarget:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/AtHomeController$Ui;->setShowTurnOffLights(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected internalBailOut()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/AtHomeController;->mTarget:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/AtHomeController;->isTurnOnAction()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/AtHomeController;->createPowerControlIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/AtHomeController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public start(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/AtHomeController;->mTarget:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/voicesearch/fragments/AtHomeController;->mLevel:I

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/AtHomeController;->showCard()V

    return-void
.end method
