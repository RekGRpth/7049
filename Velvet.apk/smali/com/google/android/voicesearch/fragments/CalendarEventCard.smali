.class public Lcom/google/android/voicesearch/fragments/CalendarEventCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "CalendarEventCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/CalendarEventController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;"
    }
.end annotation


# instance fields
.field private mLocationView:Landroid/widget/TextView;

.field private mTimeView:Landroid/widget/TextView;

.field private mTitleSet:Z

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private checkUiReady()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTitleSet:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/CalendarEventController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->uiReady()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f04000c

    invoke-virtual {p0, p2, p3, v1}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    const v1, 0x7f100046

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTitleView:Landroid/widget/TextView;

    const v1, 0x7f100045

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTimeView:Landroid/widget/TextView;

    const v1, 0x7f100047

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mLocationView:Landroid/widget/TextView;

    return-object v0
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mLocationView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mLocationView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTimeView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTimeView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTimeView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d040e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTitleSet:Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->checkUiReady()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public showCreateEvent()V
    .locals 1

    const v0, 0x7f0d040f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->setConfirmText(I)V

    const v0, 0x7f02007a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->setConfirmIcon(I)V

    return-void
.end method

.method public showEditEvent()V
    .locals 1

    const v0, 0x7f0d0410

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->setConfirmText(I)V

    const v0, 0x7f020057

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->setConfirmIcon(I)V

    return-void
.end method

.method public showEventCreated()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventCard;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0411

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
