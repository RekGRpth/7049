.class final Lcom/google/android/voicesearch/fragments/CalendarEventController$1;
.super Ljava/lang/Object;
.source "CalendarEventController.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/CalendarEventController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController$1;->apply(Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public apply(Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;

    invoke-virtual {p1}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
