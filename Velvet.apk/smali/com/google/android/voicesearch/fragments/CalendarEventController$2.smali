.class Lcom/google/android/voicesearch/fragments/CalendarEventController$2;
.super Ljava/lang/Object;
.source "CalendarEventController.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/CalendarEventController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/voicesearch/util/CalendarTextHelper;Landroid/content/res/Resources;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/CalendarEventController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/CalendarEventController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$2;->this$0:Lcom/google/android/voicesearch/fragments/CalendarEventController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$2;->this$0:Lcom/google/android/voicesearch/fragments/CalendarEventController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->actionComplete()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController$2;->this$0:Lcom/google/android/voicesearch/fragments/CalendarEventController;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/voicesearch/fragments/CalendarEventController;->createEvent(Z)V
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->access$000(Lcom/google/android/voicesearch/fragments/CalendarEventController;Z)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController$2;->onResult(Ljava/lang/Boolean;)V

    return-void
.end method
