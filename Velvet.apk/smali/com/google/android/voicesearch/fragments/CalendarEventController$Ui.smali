.class public interface abstract Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;
.super Ljava/lang/Object;
.source "CalendarEventController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;
.implements Lcom/google/android/voicesearch/fragments/CountDownUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/CalendarEventController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setLocation(Ljava/lang/String;)V
.end method

.method public abstract setTime(Ljava/lang/String;)V
.end method

.method public abstract setTitle(Ljava/lang/String;)V
.end method

.method public abstract showCreateEvent()V
.end method

.method public abstract showEditEvent()V
.end method

.method public abstract showEventCreated()V
.end method
