.class public Lcom/google/android/voicesearch/fragments/CalendarEventController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "CalendarEventController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;,
        Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;",
        ">;"
    }
.end annotation


# static fields
.field private static final FUNCTION_ATTENDEE_TO_NAME:Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Function",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAddEventCallback:Lcom/google/android/speech/callback/SimpleCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

.field private final mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

.field private mEndTimeMs:J

.field private mLocation:Ljava/lang/String;

.field private mRecognizedAttendees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReminders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;",
            ">;"
        }
    .end annotation
.end field

.field private final mResources:Landroid/content/res/Resources;

.field private mStartTimeMs:J

.field private mStartTimeSpecified:Z

.field private mSummary:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/fragments/CalendarEventController$1;

    invoke-direct {v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController$1;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->FUNCTION_ATTENDEE_TO_NAME:Lcom/google/common/base/Function;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/voicesearch/util/CalendarTextHelper;Landroid/content/res/Resources;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/voicesearch/util/CalendarHelper;
    .param p4    # Lcom/google/android/voicesearch/util/CalendarTextHelper;
    .param p5    # Landroid/content/res/Resources;
    .param p6    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0, p1, p2, p6}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mResources:Landroid/content/res/Resources;

    new-instance v0, Lcom/google/android/voicesearch/fragments/CalendarEventController$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController$2;-><init>(Lcom/google/android/voicesearch/fragments/CalendarEventController;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mAddEventCallback:Lcom/google/android/speech/callback/SimpleCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/CalendarEventController;Z)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/CalendarEventController;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->createEvent(Z)V

    return-void
.end method

.method public static createData(Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;
    .locals 9
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Attendee;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;",
            ">;)",
            "Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/voicesearch/fragments/CalendarEventController$3;-><init>(Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method private createEvent(Z)V
    .locals 12
    .param p1    # Z

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->getDisplaySummary()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mLocation:Ljava/lang/String;

    iget-wide v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    iget-wide v7, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mEndTimeMs:J

    iget-object v10, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mReminders:Ljava/util/List;

    iget-object v11, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mAddEventCallback:Lcom/google/android/speech/callback/SimpleCallback;

    move-object v2, v1

    move-object v9, v1

    invoke-virtual/range {v0 .. v11}, Lcom/google/android/voicesearch/util/CalendarHelper;->addEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;Ljava/util/List;Lcom/google/android/speech/callback/SimpleCallback;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;->showEventCreated()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->actionComplete()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->getDisplaySummary()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mLocation:Ljava/lang/String;

    iget-wide v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    iget-wide v7, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mEndTimeMs:J

    move-object v9, v1

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/voicesearch/util/CalendarHelper;->createAddEventIntent(Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->startActivity(Landroid/content/Intent;)Z

    goto :goto_0
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeSpecified:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mSummary:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0xc

    return v0
.end method

.method getDisplaySummary()Ljava/lang/String;
    .locals 8

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mSummary:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0d040e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mRecognizedAttendees:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mRecognizedAttendees:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mSummary:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f11000e

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mRecognizedAttendees:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    const-string v6, ", "

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mRecognizedAttendees:Ljava/util/List;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "summary"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mSummary:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "location"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mLocation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "recognized_attendees"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mRecognizedAttendees:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/searchcommon/util/Util;->asArrayList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "reminders"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mReminders:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/searchcommon/util/Util;->asArrayList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "start_time_ms"

    iget-wide v2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "start_time_specified"

    iget-boolean v2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeSpecified:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "end_time_ms"

    iget-wide v2, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mEndTimeMs:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method protected initUi()V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v6

    check-cast v6, Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->getDisplaySummary()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;->setTitle(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    iget-wide v3, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mEndTimeMs:J

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->formatDisplayTime(JJZ)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;->setTime(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mLocation:Ljava/lang/String;

    invoke-interface {v6, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;->setLocation(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->canExecuteAction()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;->showCreateEvent()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v6}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Ui;->showEditEvent()V

    goto :goto_0
.end method

.method protected internalBailOut()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->createEvent(Z)V

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->createEvent(Z)V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "summary"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mSummary:Ljava/lang/String;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mLocation:Ljava/lang/String;

    const-string v1, "recognized_attendees"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mRecognizedAttendees:Ljava/util/List;

    const-string v1, "reminders"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mReminders:Ljava/util/List;

    const-string v1, "start_time_ms"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    const-string v1, "start_time_specified"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeSpecified:Z

    const-string v1, "end_time_ms"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mEndTimeMs:J

    return-void
.end method

.method public start(Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;)V
    .locals 12
    .param p1    # Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;

    const-wide/16 v10, 0x0

    const/16 v7, 0xf

    const/4 v6, 0x0

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;->getSummary()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mSummary:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;->getLocation()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mLocation:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;->getStartTimeMs()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    iget-wide v8, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    cmp-long v5, v8, v10

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    :goto_0
    iput-boolean v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeSpecified:Z

    iget-boolean v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeSpecified:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/google/android/voicesearch/util/CalendarHelper;->getDefaultStartTimeMs(J)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    :cond_0
    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;->getEndTimeMs()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mEndTimeMs:J

    iget-wide v8, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mEndTimeMs:J

    cmp-long v5, v8, v10

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-wide v8, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mStartTimeMs:J

    invoke-virtual {v5, v8, v9}, Lcom/google/android/voicesearch/util/CalendarHelper;->getDefaultEndTimeMs(J)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mEndTimeMs:J

    :cond_1
    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;->getAttendees()Ljava/util/List;

    move-result-object v5

    sget-object v8, Lcom/google/android/voicesearch/fragments/CalendarEventController;->FUNCTION_ATTENDEE_TO_NAME:Lcom/google/common/base/Function;

    invoke-static {v5, v8}, Lcom/google/common/collect/Lists;->transform(Ljava/util/List;Lcom/google/common/base/Function;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mRecognizedAttendees:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mReminders:Ljava/util/List;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/CalendarEventController$Data;->getReminders()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mReminders:Ljava/util/List;

    new-instance v8, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;

    invoke-direct {v8, v7, v6}, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;-><init>(II)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/CalendarEventController;->showCardAndPlayTts()V

    return-void

    :cond_3
    move v5, v6

    goto :goto_0

    :cond_4
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;

    invoke-virtual {v3}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMinutes()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v3}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->getMinutes()I

    move-result v2

    :goto_2
    invoke-virtual {v3}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->hasMethod()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v3}, Lcom/google/majel/proto/CalendarProtos$CalendarEvent$Reminder;->getMethod()I

    move-result v1

    :goto_3
    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/CalendarEventController;->mReminders:Ljava/util/List;

    new-instance v8, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;

    invoke-direct {v8, v2, v1}, Lcom/google/android/voicesearch/util/CalendarHelper$Reminder;-><init>(II)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move v2, v7

    goto :goto_2

    :cond_6
    move v1, v6

    goto :goto_3
.end method
