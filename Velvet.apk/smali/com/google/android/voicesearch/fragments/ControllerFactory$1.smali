.class Lcom/google/android/voicesearch/fragments/ControllerFactory$1;
.super Ljava/lang/Object;
.source "ControllerFactory.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/ControllerFactory;->createAccountNameSupplier()Lcom/google/common/base/Supplier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/ControllerFactory;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/ControllerFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory$1;->this$0:Lcom/google/android/voicesearch/fragments/ControllerFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ControllerFactory$1;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ControllerFactory$1;->this$0:Lcom/google/android/voicesearch/fragments/ControllerFactory;

    # getter for: Lcom/google/android/voicesearch/fragments/ControllerFactory;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/ControllerFactory;->access$000(Lcom/google/android/voicesearch/fragments/ControllerFactory;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getSearchSettings()Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->getGoogleAccountToUse()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
