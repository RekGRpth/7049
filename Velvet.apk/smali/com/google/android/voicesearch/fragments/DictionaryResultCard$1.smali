.class Lcom/google/android/voicesearch/fragments/DictionaryResultCard$1;
.super Ljava/lang/Object;
.source "DictionaryResultCard.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->setExternalDictionaryLinks(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/DictionaryResultCard;

.field final synthetic val$link:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/DictionaryResultCard;Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard$1;->this$0:Lcom/google/android/voicesearch/fragments/DictionaryResultCard;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard$1;->val$link:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard$1;->this$0:Lcom/google/android/voicesearch/fragments/DictionaryResultCard;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/DictionaryResultController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard$1;->val$link:Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/DictionaryResultController;->goToDictionary(Ljava/lang/String;)V

    return-void
.end method
