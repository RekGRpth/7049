.class public Lcom/google/android/voicesearch/fragments/DictionaryResultCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "DictionaryResultCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/DictionaryResultController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/DictionaryResultController$Ui;"
    }
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field private mLinks:[Landroid/widget/TextView;

.field private mLinksDivider:Landroid/widget/ImageView;

.field private mMeaningContainer:Landroid/view/ViewGroup;

.field private mPronunciation:Landroid/widget/TextView;

.field private final mViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mWord:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mViews:Ljava/util/List;

    return-void
.end method

.method private addDictionaryContent(Ljava/lang/String;Landroid/text/Spanned;Landroid/text/Spanned;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/text/Spanned;
    .param p3    # Landroid/text/Spanned;

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f04002c

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mMeaningContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f04002b

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mMeaningContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v4, 0x7f100095

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_0

    const v4, 0x7f100096

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mMeaningContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mMeaningContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mViews:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mViews:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private buildSynonymsContent(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;->getSynonymCount()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;->hasPartOfSpeech()Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "<i>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;->getPartOfSpeech()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".</i> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;->getSynonymCount()I

    move-result v4

    if-ge v1, v4, :cond_4

    invoke-virtual {v2, v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;->getSynonym(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;->getSynonymCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v1, v4, :cond_3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-eq v0, v4, :cond_0

    const-string v4, "<br />"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public handleDetach()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleDetach()V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mMeaningContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    return-void
.end method

.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const/4 v3, 0x0

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04002a

    invoke-virtual {p2, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10008d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mWord:Landroid/widget/TextView;

    const v1, 0x7f10008e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mPronunciation:Landroid/widget/TextView;

    const v1, 0x7f10008f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mMeaningContainer:Landroid/view/ViewGroup;

    const v1, 0x7f100090

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinksDivider:Landroid/widget/ImageView;

    const/4 v1, 0x4

    new-array v1, v1, [Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    const v1, 0x7f100091

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    const/4 v3, 0x1

    const v1, 0x7f100092

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    const/4 v3, 0x2

    const v1, 0x7f100093

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    const/4 v3, 0x3

    const v1, 0x7f100094

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    return-object v0
.end method

.method public setExternalDictionaryLinks(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;",
            ">;)V"
        }
    .end annotation

    const/16 v5, 0x8

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinksDivider:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v1}, Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    new-instance v3, Lcom/google/android/voicesearch/fragments/DictionaryResultCard$1;

    invoke-direct {v3, p0, v1}, Lcom/google/android/voicesearch/fragments/DictionaryResultCard$1;-><init>(Lcom/google/android/voicesearch/fragments/DictionaryResultCard;Lcom/google/majel/proto/EcoutezStructuredResponse$DictionaryLink;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinksDivider:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mLinks:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    return-void
.end method

.method public setMeaningsAndSynonyms(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .locals 8
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/EcoutezStructuredResponse$Synonym;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;->getMeaningCount()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2, v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;->getMeaning(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Meaning;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/majel/proto/EcoutezStructuredResponse$Meaning;->getExampleCount()I

    move-result v5

    if-nez v5, :cond_1

    move-object v0, v4

    :goto_1
    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;->getPartOfSpeech()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;->getMeaning(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Meaning;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/majel/proto/EcoutezStructuredResponse$Meaning;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-direct {p0, v5, v6, v0}, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->addDictionaryContent(Ljava/lang/String;Landroid/text/Spanned;Landroid/text/Spanned;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$PosMeaning;->getMeaning(I)Lcom/google/majel/proto/EcoutezStructuredResponse$Meaning;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$Meaning;->getExample(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-direct {p0, p3}, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->buildSynonymsContent(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    :goto_2
    return-void

    :cond_4
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-direct {p0, p2, v5, v4}, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->addDictionaryContent(Ljava/lang/String;Landroid/text/Spanned;Landroid/text/Spanned;)V

    goto :goto_2
.end method

.method public setPronunciation(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mPronunciation:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setWord(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/DictionaryResultCard;->mWord:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
