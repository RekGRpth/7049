.class Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;
.super Ljava/lang/Object;
.source "EmailController.java"

# interfaces
.implements Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/EmailController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactSelectedCallbackImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/EmailController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/fragments/EmailController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/fragments/EmailController;Lcom/google/android/voicesearch/fragments/EmailController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/EmailController;
    .param p2    # Lcom/google/android/voicesearch/fragments/EmailController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;-><init>(Lcom/google/android/voicesearch/fragments/EmailController;)V

    return-void
.end method


# virtual methods
.method public onBailOut()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/EmailController;->bailOut()V

    return-void
.end method

.method public onContactSelected(Lcom/google/android/speech/contacts/Contact;Z)V
    .locals 1
    .param p1    # Lcom/google/android/speech/contacts/Contact;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/EmailController;

    # setter for: Lcom/google/android/voicesearch/fragments/EmailController;->mToContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/fragments/EmailController;->access$102(Lcom/google/android/voicesearch/fragments/EmailController;Lcom/google/android/speech/contacts/Contact;)Lcom/google/android/speech/contacts/Contact;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/EmailController;->canExecuteAction()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/EmailController;->bailOut()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/EmailController;->showCard()V

    goto :goto_0
.end method

.method public onNoContactFound()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/EmailController;->showCard()V

    return-void
.end method

.method public onShowingDisambiguation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/EmailController$ContactSelectedCallbackImpl;->this$0:Lcom/google/android/voicesearch/fragments/EmailController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/EmailController;->showNoCard()V

    return-void
.end method
