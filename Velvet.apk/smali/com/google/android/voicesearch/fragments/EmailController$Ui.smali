.class public interface abstract Lcom/google/android/voicesearch/fragments/EmailController$Ui;
.super Ljava/lang/Object;
.source "EmailController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;
.implements Lcom/google/android/voicesearch/fragments/CountDownUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/EmailController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract hideContactField()V
.end method

.method public abstract setBody(Ljava/lang/String;)V
.end method

.method public abstract setSubject(Ljava/lang/String;)V
.end method

.method public abstract setToContact(Lcom/google/android/speech/contacts/Contact;)V
.end method

.method public abstract showContactNotFound()V
.end method

.method public abstract showEmptyView()V
.end method

.method public abstract showNewEmail()V
.end method

.method public abstract showSendEmail()V
.end method
