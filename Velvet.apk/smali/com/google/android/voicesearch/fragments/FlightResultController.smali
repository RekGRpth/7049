.class public Lcom/google/android/voicesearch/fragments/FlightResultController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "FlightResultController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/FlightResultController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/FlightResultController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mData:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method


# virtual methods
.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/FlightResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/FlightResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;->toByteArray()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x18

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x1a

    return v0
.end method

.method protected initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/FlightResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/FlightResultController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/FlightResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/FlightResultController$Ui;->setData(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)V

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;->parseFrom([B)Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/FlightResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    :cond_0
    return-void
.end method

.method public start(Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/FlightResultController;->mData:Lcom/google/majel/proto/EcoutezStructuredResponse$FlightResult;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/FlightResultController;->showCardAndPlayTts()V

    return-void
.end method
