.class final Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;
.super Ljava/lang/Object;
.source "GogglesGenericController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/GogglesGenericController;->createData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actions:Ljava/util/List;

.field final synthetic val$fifeImageUrl:Ljava/lang/String;

.field final synthetic val$sessionId:Ljava/lang/String;

.field final synthetic val$subtitle:Ljava/lang/String;

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$fifeImageUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$title:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$subtitle:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$actions:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$sessionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/goggles/GogglesGenericAction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$actions:Ljava/util/List;

    return-object v0
.end method

.method public getFifeImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$fifeImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$sessionId:Ljava/lang/String;

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;->val$title:Ljava/lang/String;

    return-object v0
.end method
