.class public Lcom/google/android/voicesearch/fragments/GogglesGenericController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "GogglesGenericController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;,
        Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;,
        Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mActionListener:Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;

.field private mData:Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    new-instance v0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$1;-><init>(Lcom/google/android/voicesearch/fragments/GogglesGenericController;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->mActionListener:Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;

    return-void
.end method

.method public static createData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/goggles/GogglesGenericAction;",
            ">;)",
            "Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$2;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public initUi()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->mData:Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;->getFifeImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;->hideImage()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->mData:Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;->setTitle(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->mData:Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;->getSubtitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;->setSubtitle(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->mData:Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;->getActions()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->mActionListener:Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;

    invoke-interface {v1, v2, v3}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;->setActions(Ljava/util/List;Lcom/google/android/voicesearch/fragments/GogglesGenericController$ActionListener;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->mData:Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;->getSessionid()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;->setSessionId(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;

    invoke-interface {v1, v0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController$Ui;->setFifeImageUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public start(Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->mData:Lcom/google/android/voicesearch/fragments/GogglesGenericController$Data;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/GogglesGenericController;->showCard()V

    return-void
.end method
