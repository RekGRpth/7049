.class Lcom/google/android/voicesearch/fragments/HelpController$1;
.super Landroid/os/AsyncTask;
.source "HelpController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/HelpController;->lookupContactsForExamples(Ljava/util/Set;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Void;",
        "Ljava/util/Map",
        "<",
        "Landroid/net/Uri;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/android/voicesearch/fragments/HelpController$Contact;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/HelpController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/HelpController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/HelpController$1;->doInBackground([Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/net/Uri;)Ljava/util/Map;
    .locals 9
    .param p1    # [Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/voicesearch/fragments/HelpController$Contact;",
            ">;>;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v4

    move-object v0, p1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v5, v0, v2

    new-instance v1, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    const/4 v7, 0x0

    invoke-direct {v1, v6, v7}, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;-><init>(Lcom/google/android/voicesearch/fragments/HelpController;Lcom/google/android/voicesearch/fragments/HelpController$1;)V

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController;->mContactRetriever:Lcom/google/android/speech/contacts/ContactRetriever;
    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/HelpController;->access$500(Lcom/google/android/voicesearch/fragments/HelpController;)Lcom/google/android/speech/contacts/ContactRetriever;

    move-result-object v6

    const/16 v7, 0xa

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController;->COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/voicesearch/fragments/HelpController;->access$400()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v5, v7, v8, v1}, Lcom/google/android/speech/contacts/ContactRetriever;->getContacts(Landroid/net/Uri;I[Ljava/lang/String;Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;)V

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->contacts:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->access$600(Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->contacts:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->access$600(Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;)Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->contacts:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->access$600(Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;)Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/collect/Iterators;->cycle(Ljava/lang/Iterable;)Ljava/util/Iterator;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/HelpController$1;->onPostExecute(Ljava/util/Map;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/voicesearch/fragments/HelpController$Contact;",
            ">;>;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController;->mContactsForExamples:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/HelpController;->access$700(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/HelpController;->access$800(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController;->mContactsForExamples:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/HelpController;->access$700(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/HelpController;->access$900(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/List;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController;->mExamplesWithContact:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/HelpController;->access$800(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v3, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    # getter for: Lcom/google/android/voicesearch/fragments/HelpController;->mExamples:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/HelpController;->access$900(Lcom/google/android/voicesearch/fragments/HelpController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/HelpController;->showCard()V

    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/HelpController$1;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/HelpController;->showNoCard()V

    goto :goto_1
.end method
