.class Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;
.super Ljava/lang/Object;
.source "HelpController.java"

# interfaces
.implements Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/HelpController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactRowHandler"
.end annotation


# instance fields
.field private final contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/fragments/HelpController$Contact;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/HelpController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/fragments/HelpController;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->contacts:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/fragments/HelpController;Lcom/google/android/voicesearch/fragments/HelpController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/HelpController;
    .param p2    # Lcom/google/android/voicesearch/fragments/HelpController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;-><init>(Lcom/google/android/voicesearch/fragments/HelpController;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->contacts:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public handleCurrentRow(Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    # invokes: Lcom/google/android/voicesearch/fragments/HelpController;->getFirstName(J)Ljava/lang/String;
    invoke-static {v3, v1, v2}, Lcom/google/android/voicesearch/fragments/HelpController;->access$000(Lcom/google/android/voicesearch/fragments/HelpController;J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->contacts:Ljava/util/List;

    new-instance v4, Lcom/google/android/voicesearch/fragments/HelpController$Contact;

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/HelpController$ContactRowHandler;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-direct {v4, v5, v1, v2, v0}, Lcom/google/android/voicesearch/fragments/HelpController$Contact;-><init>(Lcom/google/android/voicesearch/fragments/HelpController;JLjava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
