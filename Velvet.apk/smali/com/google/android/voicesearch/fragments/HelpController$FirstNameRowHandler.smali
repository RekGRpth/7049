.class Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;
.super Ljava/lang/Object;
.source "HelpController.java"

# interfaces
.implements Lcom/google/android/speech/contacts/Cursors$CursorRowHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/HelpController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FirstNameRowHandler"
.end annotation


# instance fields
.field private firstName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/HelpController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/fragments/HelpController;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;->this$0:Lcom/google/android/voicesearch/fragments/HelpController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;->firstName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/fragments/HelpController;Lcom/google/android/voicesearch/fragments/HelpController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/HelpController;
    .param p2    # Lcom/google/android/voicesearch/fragments/HelpController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;-><init>(Lcom/google/android/voicesearch/fragments/HelpController;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;->firstName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public handleCurrentRow(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;->firstName:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/HelpController$FirstNameRowHandler;->firstName:Ljava/lang/String;

    :cond_0
    return-void
.end method
