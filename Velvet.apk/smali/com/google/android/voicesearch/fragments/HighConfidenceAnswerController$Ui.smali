.class public interface abstract Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController$Ui;
.super Ljava/lang/Object;
.source "HighConfidenceAnswerController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/HighConfidenceAnswerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setAnswer(Ljava/lang/String;)V
.end method

.method public abstract setAnswerDescription(Ljava/lang/String;)V
.end method

.method public abstract setImageData(Lcom/google/majel/proto/PeanutProtos$Image;Lcom/google/majel/proto/AttributionProtos$Attribution;)V
.end method

.method public abstract setSource(Lcom/google/majel/proto/AttributionProtos$Attribution;)V
.end method
