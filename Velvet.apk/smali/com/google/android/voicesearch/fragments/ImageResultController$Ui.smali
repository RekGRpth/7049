.class public interface abstract Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;
.super Ljava/lang/Object;
.source "ImageResultController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/ImageResultController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract clearImages()V
.end method

.method public abstract setHeader(Ljava/lang/String;)V
.end method

.method public abstract setImage(Lcom/google/android/voicesearch/speechservice/ImageParcelable;)V
.end method

.method public abstract setImageClickListener(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract setImageList(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/voicesearch/speechservice/ImageParcelable;",
            ">;)V"
        }
    .end annotation
.end method
