.class public Lcom/google/android/voicesearch/fragments/ImageResultController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "ImageResultController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

.field protected mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

.field private final mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Ljava/lang/String;Lcom/google/android/searchcommon/CoreSearchServices;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mPackageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/ImageResultController;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ImageResultController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method private createImageClickListener(Z)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1    # Z

    new-instance v0, Lcom/google/android/voicesearch/fragments/ImageResultController$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/voicesearch/fragments/ImageResultController$1;-><init>(Lcom/google/android/voicesearch/fragments/ImageResultController;Z)V

    return-object v0
.end method

.method private getQueryForMoreImages()Lcom/google/android/velvet/Query;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/ImageResultData;->onlyShowPeanut()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/Query;->EMPTY:Lcom/google/android/velvet/Query;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ImageResultController;->getCardController()Lcom/google/android/voicesearch/CardController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/CardController;->getQuery()Lcom/google/android/velvet/Query;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic attach(Lcom/google/android/voicesearch/fragments/BaseCardUi;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/BaseCardUi;

    check-cast p1, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/ImageResultController;->attach(Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;)V

    return-void
.end method

.method public attach(Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;)V
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    invoke-super {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->attach(Lcom/google/android/voicesearch/fragments/BaseCardUi;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mCoreSearchServices:Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-interface {v0}, Lcom/google/android/searchcommon/CoreSearchServices;->getImageMetadataController()Lcom/google/android/velvet/gallery/ImageMetadataController;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ImageResultController;->getQueryForMoreImages()Lcom/google/android/velvet/Query;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/speechservice/ImageResultData;->getImageList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/gallery/ImageMetadataController;->setQueryWithImageProtos(Lcom/google/android/velvet/Query;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x16

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x16

    return v0
.end method

.method public initUi()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ImageResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;->clearImages()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ImageResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/ImageResultData;->onlyShowPeanut()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/ImageResultController;->createImageClickListener(Z)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;->setImageClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ImageResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/ImageResultData;->getImageList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;->setImageList(Ljava/util/ArrayList;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ImageResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/ImageResultData;->getHeader()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;->setHeader(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;

    check-cast p1, Lcom/google/android/voicesearch/speechservice/ImageResultData;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    return-void
.end method

.method public start(Lcom/google/android/voicesearch/speechservice/ImageResultData;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/speechservice/ImageResultData;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ImageResultController;->showCardAndPlayTts()V

    return-void
.end method
