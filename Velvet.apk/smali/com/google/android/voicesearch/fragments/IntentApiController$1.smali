.class Lcom/google/android/voicesearch/fragments/IntentApiController$1;
.super Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.source "IntentApiController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/IntentApiController;->internalStart(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/IntentApiController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/IntentApiController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController$1;->this$0:Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-direct {p0}, Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController$1;->this$0:Lcom/google/android/voicesearch/fragments/IntentApiController;

    # invokes: Lcom/google/android/voicesearch/fragments/IntentApiController;->handleDone()V
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->access$300(Lcom/google/android/voicesearch/fragments/IntentApiController;)V

    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController$1;->this$0:Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/IntentApiController;->handleError(Lcom/google/android/speech/exception/RecognizeException;)V

    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController$1;->this$0:Lcom/google/android/voicesearch/fragments/IntentApiController;

    # invokes: Lcom/google/android/voicesearch/fragments/IntentApiController;->handleCancel()V
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->access$200(Lcom/google/android/voicesearch/fragments/IntentApiController;)V

    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController$1;->this$0:Lcom/google/android/voicesearch/fragments/IntentApiController;

    # getter for: Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->access$000(Lcom/google/android/voicesearch/fragments/IntentApiController;)Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;->showReadyForSpeech()V

    return-void
.end method

.method public onRecognitionCancelled()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController$1;->this$0:Lcom/google/android/voicesearch/fragments/IntentApiController;

    # invokes: Lcom/google/android/voicesearch/fragments/IntentApiController;->handleCancel()V
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->access$200(Lcom/google/android/voicesearch/fragments/IntentApiController;)V

    return-void
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 2
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasCombinedResult()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController$1;->this$0:Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getCombinedResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v1

    # invokes: Lcom/google/android/voicesearch/fragments/IntentApiController;->handleResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)V
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/IntentApiController;->access$100(Lcom/google/android/voicesearch/fragments/IntentApiController;Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)V

    goto :goto_0
.end method
