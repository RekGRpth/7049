.class public Lcom/google/android/voicesearch/fragments/IntentApiController;
.super Ljava/lang/Object;
.source "IntentApiController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;
    }
.end annotation


# instance fields
.field private final mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

.field private final mAudioStore:Lcom/google/android/speech/audio/AudioStore;

.field private mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

.field private final mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

.field mShowingError:Z

.field private mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/speech/audio/AudioStore;Lcom/google/android/voicesearch/fragments/UberRecognizerController;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/intentapi/IntentApiActivity;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;
    .param p3    # Lcom/google/android/speech/audio/AudioStore;
    .param p4    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/IntentApiController;)Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/IntentApiController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/IntentApiController;Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/IntentApiController;
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/IntentApiController;->handleResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/IntentApiController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->handleCancel()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/fragments/IntentApiController;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/IntentApiController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->handleDone()V

    return-void
.end method

.method private addStoredAudioUriToIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    invoke-virtual {v2}, Lcom/google/android/speech/audio/AudioStore;->getLastAudio()[B

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    sget-object v3, Lcom/google/android/speech/audio/AudioUtils$Encoding;->AMR:Lcom/google/android/speech/audio/AudioUtils$Encoding;

    invoke-static {v2, v3, v0}, Lcom/google/android/speech/audio/AudioProvider;->insert(Landroid/content/Context;Lcom/google/android/speech/audio/AudioUtils$Encoding;[B)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    return-void
.end method

.method private checkIncomingIntent()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->isReturnAudio()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->hasPermissionToRecordAudio(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "IntentApiController"

    const-string v1, "Must have android.permission.RECORD_AUDIO to record audio"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private handleCancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    return-void
.end method

.method private handleDone()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mShowingError:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->handleNoMatch()V

    :cond_0
    return-void
.end method

.method private handleNoMatch()V
    .locals 4

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mShowingError:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    const v1, 0x7f0d03a8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;->showError(IIZ)V

    return-void
.end method

.method private handleResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;)V
    .locals 11
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    const/4 v10, 0x0

    const/4 v9, -0x1

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisCount()I

    move-result v3

    const/4 v6, 0x1

    if-ge v3, v6, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->handleNoMatch()V

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getMaxResults()I

    move-result v6

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getMaxResults()I

    move-result v6

    if-ge v6, v3, :cond_1

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getMaxResults()I

    move-result v3

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-array v0, v3, [F

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesis(I)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesis(I)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getConfidence()F

    move-result v6

    aput v6, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const-string v6, "android.speech.extra.RESULTS"

    invoke-virtual {v4, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v6, "android.speech.extra.CONFIDENCE_SCORES"

    invoke-virtual {v4, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[F)Landroid/content/Intent;

    const-string v7, "query"

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v4, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->isReturnAudio()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-direct {p0, v4}, Lcom/google/android/voicesearch/fragments/IntentApiController;->addStoredAudioUriToIntent(Landroid/content/Intent;)V

    :cond_3
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->isAutoScript()Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v7, "IntentApiController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Recognition results: ["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getPendingIntent()Landroid/app/PendingIntent;

    move-result-object v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v6, v9, v4}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    goto/16 :goto_0

    :cond_5
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getPendingBundleIntent()Landroid/os/Bundle;

    move-result-object v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getPendingBundleIntent()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_6
    :try_start_0
    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getPendingIntent()Landroid/app/PendingIntent;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    const/4 v8, -0x1

    invoke-virtual {v6, v7, v8, v4}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :try_start_1
    const-string v6, "IntentApiController"

    const-string v7, "Not possible to start pending intent"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v6}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v7}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    throw v6
.end method

.method private hasPermissionToRecordAudio(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.permission.RECORD_AUDIO"

    invoke-virtual {v0, v1, p1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private internalStart(Z)V
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    const v3, 0x7f0d03b9

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;->setPromptText(Ljava/lang/String;)V

    :goto_0
    new-instance v0, Lcom/google/android/voicesearch/fragments/IntentApiController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/IntentApiController$1;-><init>(Lcom/google/android/voicesearch/fragments/IntentApiController;)V

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->resendIntentApi(Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getPrompt()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getPrompt()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;->setPromptText(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;->setPromptText(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/intentapi/IntentApiParams;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startIntentApi(Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private start(Lcom/google/android/voicesearch/intentapi/IntentApiParams;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mIntentApiParams:Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->checkIncomingIntent()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/IntentApiController;->internalStart(Z)V

    goto :goto_1
.end method


# virtual methods
.method public attachUi(Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->attachUi(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;)V

    return-void
.end method

.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancel()V

    return-void
.end method

.method public detachUi(Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->detachUi(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;)V

    return-void
.end method

.method public finishWithReturnCode(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    return-void
.end method

.method protected handleError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 5
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->getErrorMessage(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v2

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->getRecognizerIntentError(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v1

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->canResendSameAudio(Lcom/google/android/speech/exception/RecognizeException;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mAudioStore:Lcom/google/android/speech/audio/AudioStore;

    invoke-virtual {v4}, Lcom/google/android/speech/audio/AudioStore;->hasLastAudio()Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v3

    :goto_0
    iput-boolean v3, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mShowingError:Z

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mUi:Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;

    invoke-interface {v3, v2, v1, v0}, Lcom/google/android/voicesearch/fragments/IntentApiController$Ui;->showError(IIZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume(Lcom/google/android/voicesearch/intentapi/IntentApiParams;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/intentapi/IntentApiParams;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mShowingError:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/IntentApiController;->start(Lcom/google/android/voicesearch/intentapi/IntentApiParams;)V

    :cond_0
    return-void
.end method

.method public retryLastRecognition()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mShowingError:Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->internalStart(Z)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/IntentApiController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->stopListening()V

    return-void
.end method
