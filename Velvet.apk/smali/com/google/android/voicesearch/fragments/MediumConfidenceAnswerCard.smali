.class public Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "MediumConfidenceAnswerCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;"
    }
.end annotation


# instance fields
.field private mAnswerView:Landroid/widget/TextView;

.field private mContainer:Landroid/view/ViewGroup;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsShowingSources:Z

.field private mShowSources:Landroid/widget/TextView;

.field private final mSourceViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mSources:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSourceViews:Ljava/util/List;

    return-void
.end method

.method private hideSources()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSources:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method private showSources()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSources:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public handleDetach()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->handleDetach()V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSourceViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSources:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSourceViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    return-void
.end method

.method public isShowingSources()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mIsShowingSources:Z

    return v0
.end method

.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const/4 v3, 0x0

    const v1, 0x7f04006f

    invoke-virtual {p2, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10016a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mContainer:Landroid/view/ViewGroup;

    const v1, 0x7f100169

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mAnswerView:Landroid/widget/TextView;

    const v1, 0x7f040071

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {p2, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSources:Landroid/view/ViewGroup;

    const v1, 0x7f10016b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mShowSources:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mInflater:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mShowSources:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard$1;-><init>(Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public setAnswer(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mAnswerView:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDisclaimer(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mShowSources:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setShowingSources(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mIsShowingSources:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->showSources()V

    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mIsShowingSources:Z

    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mIsShowingSources:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->hideSources()V

    goto :goto_0
.end method

.method public setSourceList(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;)V"
        }
    .end annotation

    const/4 v7, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/majel/proto/AttributionProtos$Attribution;

    invoke-virtual {v1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getSnippetCount()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f040070

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSources:Landroid/view/ViewGroup;

    invoke-virtual {v4, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f10016d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getSnippet(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f10016c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageDomain()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/google/majel/proto/AttributionProtos$Attribution;->getPageUrl()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard$2;

    invoke-direct {v4, p0, v2}, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard$2;-><init>(Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSources:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerCard;->mSourceViews:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method
