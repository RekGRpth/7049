.class public interface abstract Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController$Ui;
.super Ljava/lang/Object;
.source "MediumConfidenceAnswerController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/MediumConfidenceAnswerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract isShowingSources()Z
.end method

.method public abstract setAnswer(Ljava/lang/String;)V
.end method

.method public abstract setDisclaimer(Ljava/lang/String;)V
.end method

.method public abstract setShowingSources(Z)V
.end method

.method public abstract setSourceList(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/AttributionProtos$Attribution;",
            ">;)V"
        }
    .end annotation
.end method
