.class public interface abstract Lcom/google/android/voicesearch/fragments/MessageEditorController$Ui;
.super Ljava/lang/Object;
.source "MessageEditorController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;
.implements Lcom/google/android/voicesearch/fragments/CountDownUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/MessageEditorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract hideContactField()V
.end method

.method public abstract setMessageBody(Ljava/lang/CharSequence;)V
.end method

.method public abstract setToContact(Lcom/google/android/speech/contacts/Contact;)V
.end method

.method public abstract showContactField()V
.end method

.method public abstract showContactNotFound()V
.end method

.method public abstract showEmptyView()V
.end method

.method public abstract showNewMessage()V
.end method

.method public abstract showNumberOnlyField()V
.end method

.method public abstract showSendMessage()V
.end method
