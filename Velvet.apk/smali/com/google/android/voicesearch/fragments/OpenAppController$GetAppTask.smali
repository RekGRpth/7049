.class Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;
.super Landroid/os/AsyncTask;
.source "OpenAppController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/OpenAppController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetAppTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/OpenAppController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/fragments/OpenAppController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;->this$0:Lcom/google/android/voicesearch/fragments/OpenAppController;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/fragments/OpenAppController;Lcom/google/android/voicesearch/fragments/OpenAppController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/OpenAppController;
    .param p2    # Lcom/google/android/voicesearch/fragments/OpenAppController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;-><init>(Lcom/google/android/voicesearch/fragments/OpenAppController;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;->doInBackground([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    aget-object v0, p1, v1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;->this$0:Lcom/google/android/voicesearch/fragments/OpenAppController;

    # getter for: Lcom/google/android/voicesearch/fragments/OpenAppController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/OpenAppController;->access$100(Lcom/google/android/voicesearch/fragments/OpenAppController;)Lcom/google/android/voicesearch/util/AppSelectionHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->findActivities(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenAppController$GetAppTask;->this$0:Lcom/google/android/voicesearch/fragments/OpenAppController;

    # invokes: Lcom/google/android/voicesearch/fragments/OpenAppController;->onAppResults(Ljava/util/List;)V
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/fragments/OpenAppController;->access$200(Lcom/google/android/voicesearch/fragments/OpenAppController;Ljava/util/List;)V

    return-void
.end method
