.class public Lcom/google/android/voicesearch/fragments/OpenBookCard;
.super Lcom/google/android/voicesearch/fragments/PlayMediaCard;
.source "OpenBookCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/OpenBookController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/PlayMediaCard",
        "<",
        "Lcom/google/android/voicesearch/fragments/OpenBookController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/OpenBookController$Ui;"
    }
.end annotation


# instance fields
.field private mAuthorView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaCard;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v5, 0x7f040091

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/OpenBookCard;->createMediaActionEditor(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    const v0, 0x7f1001c8

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenBookCard;->mTitleView:Landroid/widget/TextView;

    const v0, 0x7f1001c9

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenBookCard;->mAuthorView:Landroid/widget/TextView;

    return-object v6
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenBookCard;->mAuthorView:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/voicesearch/fragments/OpenBookCard;->showTextIfNonEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/OpenBookCard;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
