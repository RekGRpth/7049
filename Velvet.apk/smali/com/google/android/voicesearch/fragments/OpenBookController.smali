.class public Lcom/google/android/voicesearch/fragments/OpenBookController;
.super Lcom/google/android/voicesearch/fragments/PlayMediaController;
.source "OpenBookController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/OpenBookController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/PlayMediaController",
        "<",
        "Lcom/google/android/voicesearch/fragments/OpenBookController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccountSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Lcom/google/android/voicesearch/util/AppSelectionHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/CardController;",
            "Lcom/google/android/voicesearch/audio/TtsAudioPlayer;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Lcom/google/android/voicesearch/fragments/PlayMediaController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/OpenBookController;->mAccountSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x20

    return v0
.end method

.method protected getGoogleContentAppIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getBookItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;->getBookAppUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.apps.books"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getBookItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;->getBookAppUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/OpenBookController;->mAccountSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/OpenBookController;->setAccountName(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getMimeType()Ljava/lang/String;
    .locals 1

    const-string v0, "text/book"

    return-object v0
.end method

.method protected getOpenFromSearchActionString()Ljava/lang/String;
    .locals 1

    const-string v0, "android.media.action.TEXT_OPEN_FROM_SEARCH"

    return-object v0
.end method

.method protected getPreviewIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPreviewUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "com.google.android.apps.books"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "books:addToMyEBooks"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x24

    return v0
.end method

.method public initUi()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->initUi()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenBookController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/OpenBookController$Ui;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenBookController;->getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getBookItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/OpenBookController$Ui;->setTitle(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getBookItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$BookItem;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/OpenBookController$Ui;->setAuthor(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/OpenBookController;->uiReady()V

    return-void
.end method
