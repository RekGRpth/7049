.class public Lcom/google/android/voicesearch/fragments/PhoneCallCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "PhoneCallCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/PhoneCallController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;"
    }
.end annotation


# instance fields
.field private mContactNameView:Landroid/widget/TextView;

.field private mContactNotFoundView:Landroid/widget/TextView;

.field private mContactPhoneNumberView:Landroid/widget/TextView;

.field private mContactPhoneTypeView:Landroid/widget/TextView;

.field private mContactPictureView:Landroid/widget/ImageView;

.field private mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const v3, 0x7f04000d

    const v4, 0x7f04000e

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;IIZ)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f02004b

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmIcon(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100061

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactNameView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100049

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactNotFoundView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100062

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneNumberView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100063

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneTypeView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f10004a

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPictureView:Landroid/widget/ImageView;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactNameView:Landroid/widget/TextView;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneNumberView:Landroid/widget/TextView;

    aput-object v1, v0, v5

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneTypeView:Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->clearTextViews([Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    return-object v0
.end method

.method public setToContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 5
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactNameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0d044f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->setConfirmText(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneNumberView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneTypeView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/speech/contacts/Contact;->getLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactNameView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneNumberView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneTypeView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactNotFoundView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/fragments/PhoneCallCard$1;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPictureView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/voicesearch/fragments/PhoneCallCard$1;-><init>(Lcom/google/android/voicesearch/fragments/PhoneCallCard;Landroid/widget/ImageView;Lcom/google/android/voicesearch/ui/ActionEditorView;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/PhoneCallCard$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->uiReady()V

    goto :goto_0
.end method

.method public showContactNotFound()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactNameView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneNumberView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactPhoneTypeView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mContactNotFoundView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->mMainView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->showPanelContent(Z)V

    const v0, 0x7f020068

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->setConfirmIcon(I)V

    const v0, 0x7f0d0441

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PhoneCallCard;->setConfirmText(I)V

    return-void
.end method
