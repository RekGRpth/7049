.class public Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "PhoneCallNumberCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/PhoneCallController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/PhoneCallController$Ui;"
    }
.end annotation


# instance fields
.field private mPhoneNumberView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f04000f

    invoke-virtual {p0, p2, p3, v1}, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    const v1, 0x7f10004b

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;->mPhoneNumberView:Landroid/widget/TextView;

    const v1, 0x7f02004b

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;->setConfirmIcon(I)V

    const v1, 0x7f0d044f

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;->setConfirmText(I)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/TextView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;->mPhoneNumberView:Landroid/widget/TextView;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;->clearTextViews([Landroid/widget/TextView;)V

    return-object v0
.end method

.method public setToContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;->mPhoneNumberView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PhoneCallNumberCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PhoneCallController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PhoneCallController;->uiReady()V

    return-void
.end method

.method public showContactNotFound()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
