.class public abstract Lcom/google/android/voicesearch/fragments/PlayMediaCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "PlayMediaCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;
.implements Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/voicesearch/fragments/PlayMediaController",
        "<*>;>",
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<TT;>;",
        "Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;",
        "Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;"
    }
.end annotation


# instance fields
.field private mAppIconView:Landroid/widget/ImageView;

.field private mBuyInfoView:Landroid/widget/LinearLayout;

.field private mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

.field protected mImageView:Lcom/google/android/velvet/ui/WebImageView;

.field private mOwnedInfoView:Landroid/widget/LinearLayout;

.field private mPlayRatingBarView:Landroid/widget/RatingBar;

.field private mPreviewView:Landroid/widget/TextView;

.field private mPriceView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public createMediaActionEditor(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;
    .param p5    # I

    const/4 v2, 0x0

    invoke-virtual {p0, p2, p3, p5}, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setBailOutEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setContentClickable(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f1001c4

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mBuyInfoView:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f1001cc

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mOwnedInfoView:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f1001c2

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f1001c6

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPriceView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f1001c7

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPlayRatingBarView:Landroid/widget/RatingBar;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f1001ca

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPreviewView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPreviewView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPreviewView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/voicesearch/fragments/PlayMediaCard$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/PlayMediaCard$1;-><init>(Lcom/google/android/voicesearch/fragments/PlayMediaCard;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f040090

    invoke-virtual {p2, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mAppIconView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    return-object v0
.end method

.method public getImageDimensions()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/WebImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAppSelected(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->appSelected(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V

    return-void
.end method

.method public setAppLabel(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmText(I)V

    return-void
.end method

.method public setAppLabel(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmText(Ljava/lang/String;)V

    return-void
.end method

.method public setPrice(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPriceView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d03c3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v5

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setSelectorApps(Ljava/util/List;I)V
    .locals 5
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;I)V"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mAppIconView:Landroid/widget/ImageView;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mAppIconView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmIcon(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f040094

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/AppSelectorView;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v2, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setConfirmIcon(Landroid/view/View;)V

    invoke-virtual {v0, p0}, Lcom/google/android/voicesearch/ui/AppSelectorView;->setOnAppSelectedListener(Lcom/google/android/voicesearch/ui/AppSelectorView$OnAppSelectedListener;)V

    new-instance v2, Lcom/google/android/voicesearch/fragments/PlayMediaCard$2;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/PlayMediaCard$2;-><init>(Lcom/google/android/voicesearch/fragments/PlayMediaCard;)V

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/AppSelectorView;->setAppSelectorOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ui/AppSelectorView;->setSelectorApps(Ljava/util/List;I)V

    goto :goto_0
.end method

.method public showImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public showImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public showImageUri(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUri(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public showOwnedMode(Z)V
    .locals 4
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mOwnedInfoView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mOwnedInfoView:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mBuyInfoView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mBuyInfoView:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method public showPlayStoreRating(D)V
    .locals 4
    .param p1    # D

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPlayRatingBarView:Landroid/widget/RatingBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPlayRatingBarView:Landroid/widget/RatingBar;

    double-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPlayRatingBarView:Landroid/widget/RatingBar;

    const-wide/16 v2, 0x0

    cmpg-double v0, p1, v2

    if-gez v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RatingBar;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showPreview(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPreviewView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->mPreviewView:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method
