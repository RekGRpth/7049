.class public abstract Lcom/google/android/voicesearch/fragments/PlayMediaController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "PlayMediaController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;",
        ">",
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

.field private final mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

.field private final mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

.field private mImageUri:Landroid/net/Uri;

.field protected mLocalResults:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation
.end field

.field protected mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviewEnabled:Z

.field private mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Landroid/content/Intent;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getMediaPreviewIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/voicesearch/fragments/PlayMediaController;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/PlayMediaController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPreviewEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/android/voicesearch/util/AppSelectionHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/PlayMediaController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/voicesearch/fragments/PlayMediaController;Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/PlayMediaController;
    .param p1    # Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/fragments/PlayMediaController;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/PlayMediaController;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getDefaultApp()Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    move-result-object v0

    return-object v0
.end method

.method private getDefaultApp()Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    .locals 4

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->hasPlayStoreIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1, v2}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    :goto_1
    return-object v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getMimeType()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->getSelectedApp(Ljava/lang/String;Ljava/util/Collection;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1, v0}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1, v2}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    goto :goto_1
.end method

.method private getFifeParams(II)Ljava/lang/String;
    .locals 5
    .param p1    # I
    .param p2    # I

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "w%d-h%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMediaPreviewIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemPreviewUrl()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getPreviewIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private hasPlayStoreIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Z
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getIntentFlagList()Ljava/util/List;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private internalStart(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPreviewEnabled:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mImageUri:Landroid/net/Uri;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->setAppSelector(Z)V

    return-void
.end method


# virtual methods
.method protected appSelected(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V
    .locals 5
    .param p1    # Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->setAppLabel(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->isPlayStoreLink(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    :goto_0
    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->showOwnedMode(Z)V

    if-eqz v0, :cond_1

    iget-boolean v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPreviewEnabled:Z

    if-eqz v2, :cond_1

    :goto_1
    invoke-interface {v1, v3}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->showPreview(Z)V

    return-void

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method

.method protected getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    return-object v0
.end method

.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected abstract getGoogleContentAppIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
.end method

.method protected getLocalApps(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Lcom/google/common/collect/ImmutableList;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;",
            ")",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getIsFromSoundSearch()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newLinkedHashSet()Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemOwnedByUser()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getGoogleContentAppIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->findActivities(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getOpenFromSearchIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->findActivities(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    goto :goto_0
.end method

.method protected abstract getMimeType()Ljava/lang/String;
.end method

.method protected abstract getOpenFromSearchActionString()Ljava/lang/String;
.end method

.method protected getOpenFromSearchIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getOpenFromSearchActionString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getOpenFromSearchActionString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method protected getPlayStoreLinks(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Lcom/google/common/collect/ImmutableList;
    .locals 4
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;",
            ")",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper$App;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/common/collect/Sets;->newLinkedHashSet()Ljava/util/LinkedHashSet;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->getPlayStoreLink(Landroid/net/Uri;)Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->isSupported(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    return-object v2
.end method

.method protected abstract getPreviewIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
.end method

.method protected getSelectedApp()Lcom/google/android/voicesearch/util/AppSelectionHelper$App;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    return-object v0
.end method

.method public initUi()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList$Builder;->addAll(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mLocalResults:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList$Builder;->addAll(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList$Builder;->build()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-interface {v0, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v3, v0, v4}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->setSelectorApps(Ljava/util/List;I)V

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mSelectedApp:Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {p0, v3}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->appSelected(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemRating()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemRating()D

    move-result-wide v3

    :goto_0
    invoke-interface {v2, v3, v4}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->showPlayStoreRating(D)V

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPriceTagCount()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPriceTag(I)Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;->getPriceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$PriceTag;->getPrice()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->setPrice(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImage()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImageUrl()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p0, v3}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->setImage(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V

    :cond_2
    return-void

    :cond_3
    const-wide/high16 v3, -0x4010000000000000L

    goto :goto_0
.end method

.method protected internalBailOut()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getMediaPreviewIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->startActivity(Landroid/content/Intent;)Z

    :goto_0
    return-void

    :cond_0
    const-string v1, "PlayMediaController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Called bailout but no preview intent available for action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getActionTypeLog()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/velvet/VelvetStrictMode;->logW(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected internalExecuteAction()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getSelectedApp()Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAppSelectionHelper:Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/voicesearch/util/AppSelectionHelper;->appSelected(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->open(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V

    return-void
.end method

.method protected isPlayStoreLink(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)Z
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mPlayStoreLinks:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected open(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/util/AppSelectionHelper$App;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->isPlayStoreLink(Lcom/google/android/voicesearch/util/AppSelectionHelper$App;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x5b

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getActionTypeLog()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/voicesearch/util/AppSelectionHelper$App;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    return-void
.end method

.method public restoreStart()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->internalStart(Z)V

    return-void
.end method

.method protected setAccountName(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/String;

    if-eqz p2, :cond_0

    const-string v0, "authAccount"

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method protected setAppSelector(Z)V
    .locals 2
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->requireShowCard()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaController$1;-><init>(Lcom/google/android/voicesearch/fragments/PlayMediaController;Z)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected setImage(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImage()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemImage()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v4

    invoke-static {v4, v5, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-le v4, p1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-gt v4, p2, :cond_2

    :cond_0
    invoke-interface {v2, v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->showImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->hasItemImageUrl()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mImageUri:Landroid/net/Uri;

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getFifeParams(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Lcom/google/android/apps/sidekick/FifeImageUrlUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/apps/sidekick/FifeImageUrlUtil;->setImageUriOptions(Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mImageUri:Landroid/net/Uri;

    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mImageUri:Landroid/net/Uri;

    invoke-interface {v2, v4}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->showImageUri(Landroid/net/Uri;)V

    goto :goto_0

    :cond_4
    const-string v4, "?fife"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mImageUri:Landroid/net/Uri;

    goto :goto_1

    :cond_5
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mImageUri:Landroid/net/Uri;

    goto :goto_1
.end method

.method protected setImage(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 4
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/PlayMediaController$Ui;->getImageDimensions()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0, v3, v1}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->setImage(II)V

    :cond_0
    return-void
.end method

.method public start(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMediaController;->mAction:Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->internalStart(Z)V

    return-void
.end method
