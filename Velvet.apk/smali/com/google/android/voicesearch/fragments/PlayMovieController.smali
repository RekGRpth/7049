.class public Lcom/google/android/voicesearch/fragments/PlayMovieController;
.super Lcom/google/android/voicesearch/fragments/PlayMediaController;
.source "PlayMovieController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/PlayMovieController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/PlayMediaController",
        "<",
        "Lcom/google/android/voicesearch/fragments/PlayMovieController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccountSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Lcom/google/android/voicesearch/util/AppSelectionHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/CardController;",
            "Lcom/google/android/voicesearch/audio/TtsAudioPlayer;",
            "Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/voicesearch/util/AppSelectionHelper;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Lcom/google/android/voicesearch/fragments/PlayMediaController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/PlayMovieController;->mAccountSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x1f

    return v0
.end method

.method protected getGoogleContentAppIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;->getMovieAppUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.videos.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.videos"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;->getMovieAppUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMovieController;->mAccountSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/PlayMovieController;->setAccountName(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getMimeType()Ljava/lang/String;
    .locals 1

    const-string v0, "video/movie"

    return-object v0
.end method

.method protected getOpenFromSearchActionString()Ljava/lang/String;
    .locals 1

    const-string v0, "android.media.action.VIDEO_PLAY_FROM_SEARCH"

    return-object v0
.end method

.method protected getPreviewIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMovieController;->getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPreviewUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "force_fullscreen"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.youtube"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x23

    return v0
.end method

.method public initUi()V
    .locals 6

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->initUi()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMovieController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/PlayMovieController$Ui;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMovieController;->getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/google/android/voicesearch/fragments/PlayMovieController$Ui;->setTitle(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;->getGenre()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/google/android/voicesearch/fragments/PlayMovieController$Ui;->setGenre(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;->hasReleaseDate()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;->getReleaseDate()Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionDate;->getYear()I

    move-result v2

    :cond_0
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMovieItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MovieItem;->getPlayTimeMinutes()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemRemainingRentalSeconds()J

    move-result-wide v4

    long-to-int v4, v4

    div-int/lit8 v4, v4, 0x3c

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/PlayMovieController$Ui;->setReleaseAndRuntimeInfo(III)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMovieController;->uiReady()V

    return-void
.end method
