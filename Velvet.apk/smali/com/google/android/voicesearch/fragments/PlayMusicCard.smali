.class public Lcom/google/android/voicesearch/fragments/PlayMusicCard;
.super Lcom/google/android/voicesearch/fragments/PlayMediaCard;
.source "PlayMusicCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/PlayMediaCard",
        "<",
        "Lcom/google/android/voicesearch/fragments/PlayMusicController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;"
    }
.end annotation


# instance fields
.field private mAlbumView:Landroid/widget/TextView;

.field private mArtistImageView:Lcom/google/android/velvet/ui/WebImageView;

.field private mArtistInfoContainer:Landroid/view/ViewGroup;

.field private mArtistTitleView:Landroid/widget/TextView;

.field private mArtistView:Landroid/widget/TextView;

.field private mExplicitView:Landroid/widget/TextView;

.field private mGeneralInfoContainer:Landroid/view/ViewGroup;

.field private mHeadLineView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaCard;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v5, 0x7f040093

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->createMediaActionEditor(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    const v0, 0x7f1001d0

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistInfoContainer:Landroid/view/ViewGroup;

    const v0, 0x7f1001d1

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistTitleView:Landroid/widget/TextView;

    const v0, 0x7f1001d2

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistImageView:Lcom/google/android/velvet/ui/WebImageView;

    const v0, 0x7f1001d3

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mGeneralInfoContainer:Landroid/view/ViewGroup;

    const v0, 0x7f1001d4

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mHeadLineView:Landroid/widget/TextView;

    const v0, 0x7f1001d6

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mAlbumView:Landroid/widget/TextView;

    const v0, 0x7f1001d5

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistView:Landroid/widget/TextView;

    const v0, 0x7f1001d7

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mExplicitView:Landroid/widget/TextView;

    return-object v6
.end method

.method public setImageOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnLayoutChangeListener;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_0
    return-void
.end method

.method public showAlbum(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistInfoContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mGeneralInfoContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mHeadLineView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistView:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->showTextIfNonEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showArtist(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mGeneralInfoContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistInfoContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistImageView:Lcom/google/android/velvet/ui/WebImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mImageView:Lcom/google/android/velvet/ui/WebImageView;

    return-void
.end method

.method public showIsExplicit(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mExplicitView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showMisc(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistInfoContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mGeneralInfoContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mHeadLineView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showPreview(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mHeadLineView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mHeadLineView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    invoke-super {p0, p1}, Lcom/google/android/voicesearch/fragments/PlayMediaCard;->showPreview(Z)V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public showSong(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistInfoContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mGeneralInfoContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mHeadLineView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mAlbumView:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->showTextIfNonEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->mArtistView:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p3}, Lcom/google/android/voicesearch/fragments/PlayMusicCard;->showTextIfNonEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method
