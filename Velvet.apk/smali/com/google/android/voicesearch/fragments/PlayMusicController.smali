.class public Lcom/google/android/voicesearch/fragments/PlayMusicController;
.super Lcom/google/android/voicesearch/fragments/PlayMediaController;
.source "PlayMusicController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/PlayMediaController",
        "<",
        "Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mImagePopulated:Z


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/util/concurrent/ExecutorService;
    .param p5    # Lcom/google/android/voicesearch/util/AppSelectionHelper;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/voicesearch/fragments/PlayMediaController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/voicesearch/util/AppSelectionHelper;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/PlayMusicController;)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/PlayMusicController;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/PlayMusicController;->mImagePopulated:Z

    return v0
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x1e

    return v0
.end method

.method protected getGoogleContentAppIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getMimeType()Ljava/lang/String;
    .locals 1

    const-string v0, "audio/music"

    return-object v0
.end method

.method protected getOpenFromSearchActionString()Ljava/lang/String;
    .locals 1

    const-string v0, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    return-object v0
.end method

.method protected getOpenFromSearchIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->getOpenFromSearchActionString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "query"

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getSuggestedQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getPreviewIntent(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)Landroid/content/Intent;
    .locals 2
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getItemPreviewUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x22

    return v0
.end method

.method public initUi()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->getAction()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->hasSong()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->getSong()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->getAlbum()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->getArtist()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;->showSong(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->getIsExplicit()Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;->showIsExplicit(Z)V

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/PlayMediaController;->initUi()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->uiReady()V

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->hasAlbum()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->getAlbum()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->getArtist()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;->showAlbum(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->hasArtist()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getMusicItem()Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction$MusicItem;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;->showArtist(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;->showMisc(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setImage(Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;)V
    .locals 4
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$PlayMediaAction;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;->getImageDimensions()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gtz v2, :cond_1

    new-instance v2, Lcom/google/android/voicesearch/fragments/PlayMusicController$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/PlayMusicController$1;-><init>(Lcom/google/android/voicesearch/fragments/PlayMusicController;)V

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/PlayMusicController$Ui;->setImageOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0

    :cond_1
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v2, v3}, Lcom/google/android/voicesearch/fragments/PlayMusicController;->setImage(II)V

    goto :goto_0
.end method
