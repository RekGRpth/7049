.class public Lcom/google/android/voicesearch/fragments/PuntCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "PuntCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/PuntController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/PuntController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/PuntController$Ui;"
    }
.end annotation


# instance fields
.field private mDividerView:Landroid/view/View;

.field private mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

.field private mMessageView:Landroid/widget/TextView;

.field private mQueryView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const/4 v2, 0x0

    const v0, 0x7f04009c

    invoke-virtual {p0, p2, p3, v0}, Lcom/google/android/voicesearch/fragments/PuntCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f1000f6

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mQueryView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f1000f5

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMessageView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100074

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mDividerView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setContentClickable(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->showCountDownView(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setNoConfirmIcon()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    return-object v0
.end method

.method public setMessageId(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMessageView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setMessageText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMessageView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setNoQuery()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mQueryView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mDividerView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setQuery(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mQueryView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mDividerView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mQueryView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PuntCard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d045b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showActionButton(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p2}, Lcom/google/android/voicesearch/fragments/PuntCard;->setConfirmText(I)V

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/PuntCard;->setConfirmIcon(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntCard;->mMainContent:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->showCountDownView(Z)V

    return-void
.end method
