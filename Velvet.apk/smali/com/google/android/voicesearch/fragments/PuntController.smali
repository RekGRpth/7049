.class public Lcom/google/android/voicesearch/fragments/PuntController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "PuntController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/PuntController$Ui;,
        Lcom/google/android/voicesearch/fragments/PuntController$Data;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/PuntController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mActionIcon:I

.field private mActionIntent:Landroid/content/Intent;

.field private mActionLabel:I

.field private mMessage:Ljava/lang/CharSequence;

.field private mMessageId:I

.field private mQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    return-void
.end method

.method public static createData(I)Lcom/google/android/voicesearch/fragments/PuntController$Data;
    .locals 2
    .param p0    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {p0, v1, v1, v0}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(IIILandroid/content/Intent;)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v0

    return-object v0
.end method

.method public static createData(IIILandroid/content/Intent;)Lcom/google/android/voicesearch/fragments/PuntController$Data;
    .locals 6
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x0

    move v1, p0

    move-object v2, v0

    move v3, p1

    move v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(Ljava/lang/CharSequence;ILjava/lang/String;IILandroid/content/Intent;)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v0

    return-object v0
.end method

.method private static createData(Ljava/lang/CharSequence;ILjava/lang/String;IILandroid/content/Intent;)Lcom/google/android/voicesearch/fragments/PuntController$Data;
    .locals 7
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/content/Intent;

    new-instance v0, Lcom/google/android/voicesearch/fragments/PuntController$1;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/PuntController$1;-><init>(Ljava/lang/CharSequence;ILjava/lang/String;IILandroid/content/Intent;)V

    return-object v0
.end method

.method public static createData(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/PuntController$Data;
    .locals 6
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, v1

    move v4, v1

    invoke-static/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(Ljava/lang/CharSequence;ILjava/lang/String;IILandroid/content/Intent;)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x19

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessage:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    const-string v1, "msgId"

    iget v2, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessageId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_0
    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const-string v1, "msg"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessage:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x1b

    return v0
.end method

.method protected initUi()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PuntController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/PuntController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessage:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessage:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/PuntController$Ui;->setMessageText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mQuery:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/PuntController$Ui;->setQuery(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mActionIntent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mActionIcon:I

    iget v2, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mActionLabel:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/PuntController$Ui;->showActionButton(II)V

    :cond_0
    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessageId:I

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/PuntController$Ui;->setMessageId(I)V

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/PuntController$Ui;->setNoQuery()V

    goto :goto_1
.end method

.method protected internalExecuteAction()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mActionIntent:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mActionIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/PuntController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "msg"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessage:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessage:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    const-string v1, "msgId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessageId:I

    :cond_1
    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mQuery:Ljava/lang/String;

    goto :goto_0
.end method

.method public start(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/fragments/PuntController$Data;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PuntController$Data;->getMessage()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessage:Ljava/lang/CharSequence;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PuntController$Data;->getMessageId()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mMessageId:I

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PuntController$Data;->getQuery()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mQuery:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PuntController$Data;->getActionLabel()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mActionLabel:I

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PuntController$Data;->getActionIcon()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mActionIcon:I

    invoke-interface {p1}, Lcom/google/android/voicesearch/fragments/PuntController$Data;->getActionIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/PuntController;->mActionIntent:Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/PuntController;->showCard()V

    return-void
.end method
