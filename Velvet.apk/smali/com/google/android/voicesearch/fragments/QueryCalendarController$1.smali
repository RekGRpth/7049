.class Lcom/google/android/voicesearch/fragments/QueryCalendarController$1;
.super Ljava/lang/Object;
.source "QueryCalendarController.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/QueryCalendarController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/util/CalendarTextHelper;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$1;->this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$1;->onResult(Ljava/util/List;)V

    return-void
.end method

.method public onResult(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$1;->this$0:Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->onQueryResult(Ljava/util/List;)V

    return-void
.end method
