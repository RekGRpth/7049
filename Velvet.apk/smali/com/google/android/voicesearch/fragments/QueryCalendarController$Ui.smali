.class public interface abstract Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;
.super Ljava/lang/Object;
.source "QueryCalendarController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/QueryCalendarController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setEventCount(IZ)V
.end method

.method public abstract setLocation(Ljava/lang/String;)V
.end method

.method public abstract setTime(Ljava/lang/String;)V
.end method

.method public abstract setTitle(Ljava/lang/String;)V
.end method

.method public abstract showResultsFound(Z)V
.end method
