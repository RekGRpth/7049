.class public Lcom/google/android/voicesearch/fragments/QueryCalendarController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "QueryCalendarController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

.field private final mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

.field private final mEnqueueTtsCallback:Lcom/google/android/speech/callback/SimpleCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mEventCount:I

.field private mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

.field private mLastEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

.field private final mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field private final mQueryCallback:Lcom/google/android/speech/callback/SimpleCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private mQueryIntervalEndTimeMs:J

.field private mQueryIntervalStartTimeMs:J


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/util/CalendarHelper;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/util/CalendarTextHelper;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/voicesearch/util/CalendarHelper;
    .param p4    # Lcom/google/android/voicesearch/util/LocalTtsManager;
    .param p5    # Lcom/google/android/voicesearch/util/CalendarTextHelper;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    new-instance v0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$1;-><init>(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryCallback:Lcom/google/android/speech/callback/SimpleCallback;

    new-instance v0, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$2;-><init>(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEnqueueTtsCallback:Lcom/google/android/speech/callback/SimpleCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)J
    .locals 2
    .param p0    # Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    iget-wide v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalStartTimeMs:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)J
    .locals 2
    .param p0    # Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    iget-wide v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalEndTimeMs:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)Lcom/google/android/voicesearch/util/CalendarTextHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/fragments/QueryCalendarController;)Lcom/google/android/voicesearch/util/LocalTtsManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/QueryCalendarController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    return-object v0
.end method

.method private enqueueLocalTts()V
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->getCardController()Lcom/google/android/voicesearch/CardController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/CardController;->canPlayLocalTts()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEnqueueTtsCallback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/voicesearch/util/LocalTtsManager;->checkLocaleMatches(Ljava/util/Locale;ZLcom/google/android/speech/callback/SimpleCallback;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public detach()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/LocalTtsManager;->stop()V

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->detach()V

    return-void
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method protected initUi()V
    .locals 11

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v5, 0x1

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->initUi()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v9

    check-cast v9, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;

    iget v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    packed-switch v0, :pswitch_data_0

    iget v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    move v8, v5

    :cond_0
    iget v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    invoke-interface {v9, v0, v8}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setEventCount(IZ)V

    if-eqz v8, :cond_2

    invoke-interface {v9, v10}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setTime(Ljava/lang/String;)V

    :goto_0
    invoke-interface {v9, v5}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->showResultsFound(Z)V

    :goto_1
    return-void

    :pswitch_0
    invoke-interface {v9, v10}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setTitle(Ljava/lang/String;)V

    invoke-interface {v9, v10}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setLocation(Ljava/lang/String;)V

    invoke-interface {v9, v10}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setTime(Ljava/lang/String;)V

    invoke-interface {v9, v8}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->showResultsFound(Z)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getEndTimeMs()J

    move-result-wide v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->formatDisplayTime(JJZ)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9, v6}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setTitle(Ljava/lang/String;)V

    invoke-interface {v9, v10}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setTime(Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getLocation()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setLocation(Ljava/lang/String;)V

    invoke-interface {v9, v5}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->showResultsFound(Z)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getSummary()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setTitle(Ljava/lang/String;)V

    invoke-interface {v9, v6}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setTime(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarTextHelper:Lcom/google/android/voicesearch/util/CalendarTextHelper;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mLastEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getEndTimeMs()J

    move-result-wide v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->formatDisplayTime(JJZ)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController$Ui;->setTime(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected internalBailOut()V
    .locals 4

    iget v1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getStartTimeMs()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/voicesearch/util/CalendarHelper;->createViewCalendarIntent(J)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->startActivity(Landroid/content/Intent;)Z

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-wide v2, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalStartTimeMs:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/voicesearch/util/CalendarHelper;->createViewCalendarIntent(J)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/voicesearch/util/CalendarHelper;->createViewEventIntent(J)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onQueryResult(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    iget v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    const/16 v2, 0x64

    if-gt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    if-nez v0, :cond_1

    iput-object v3, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    iput-object v3, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mLastEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->showCard()V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mFirstEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    iget v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mEventCount:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mLastEvent:Lcom/google/android/voicesearch/util/CalendarHelper$CalendarEvent;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->enqueueLocalTts()V

    goto :goto_1
.end method

.method public start()V
    .locals 7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalStartTimeMs:J

    iget-wide v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalStartTimeMs:J

    const-wide v2, 0x7528ad000L

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalEndTimeMs:J

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->requireShowCard()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mCalendarHelper:Lcom/google/android/voicesearch/util/CalendarHelper;

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalStartTimeMs:J

    iget-wide v3, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryIntervalEndTimeMs:J

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/QueryCalendarController;->mQueryCallback:Lcom/google/android/speech/callback/SimpleCallback;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/voicesearch/util/CalendarHelper;->queryEvents(JJILcom/google/android/speech/callback/SimpleCallback;)V

    return-void
.end method
