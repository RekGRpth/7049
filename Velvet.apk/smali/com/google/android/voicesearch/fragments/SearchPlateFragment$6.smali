.class Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;
.super Ljava/lang/Object;
.source "SearchPlateFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->showTextInput(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

.field final synthetic val$currentUiReference:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;->this$0:Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;->val$currentUiReference:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;->this$0:Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    # getter for: Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->access$300(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;->this$0:Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    # getter for: Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mInputMode:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->access$300(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    move-result-object v0

    sget-object v1, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;->TEXT_FROM_VOICE:Lcom/google/android/voicesearch/fragments/SearchPlateFragment$InputMode;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;->this$0:Lcom/google/android/voicesearch/fragments/SearchPlateFragment;

    # getter for: Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->mSearchPlate:Lcom/google/android/velvet/ui/widget/SearchPlate;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/SearchPlateFragment;->access$400(Lcom/google/android/voicesearch/fragments/SearchPlateFragment;)Lcom/google/android/velvet/ui/widget/SearchPlate;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/ui/util/Animations;->showAndFadeIn(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SearchPlateFragment$6;->val$currentUiReference:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
