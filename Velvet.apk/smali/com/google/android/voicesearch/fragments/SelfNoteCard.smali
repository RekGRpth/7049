.class public Lcom/google/android/voicesearch/fragments/SelfNoteCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "SelfNoteCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/SelfNoteController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/SelfNoteController$Ui;"
    }
.end annotation


# instance fields
.field private mNoteText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f0400b0

    invoke-virtual {p0, p2, p3, v1}, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    const v1, 0x7f100211

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->mNoteText:Landroid/widget/TextView;

    return-object v0
.end method

.method public setNoteText(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->mNoteText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0396

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SelfNoteController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/SelfNoteController;->uiReady()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->mNoteText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public showNewNote()V
    .locals 1

    const v0, 0x7f020051

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->setConfirmIcon(I)V

    const v0, 0x7f0d044c

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->setConfirmText(I)V

    return-void
.end method

.method public showSaveNote()V
    .locals 1

    const v0, 0x7f02007a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->setConfirmIcon(I)V

    const v0, 0x7f0d044d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SelfNoteCard;->setConfirmText(I)V

    return-void
.end method
