.class public Lcom/google/android/voicesearch/fragments/SetAlarmCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "SetAlarmCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/SetAlarmController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;"
    }
.end annotation


# instance fields
.field private isLabelSet:Z

.field private isTimeSet:Z

.field private mLabelView:Landroid/widget/TextView;

.field private mTimeView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private checkUiReady()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->isLabelSet:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->isTimeSet:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SetAlarmController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->uiReady()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v1, 0x7f0400b1

    invoke-virtual {p0, p2, p3, v1}, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    const v1, 0x7f100213

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mLabelView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mLabelView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f100212

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mTimeView:Landroid/widget/TextView;

    const v1, 0x7f020037

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->setConfirmIcon(I)V

    return-object v0
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mLabelView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mLabelView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->isLabelSet:Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->checkUiReady()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mLabelView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mTimeView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mTimeView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->isTimeSet:Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->checkUiReady()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmCard;->mTimeView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
