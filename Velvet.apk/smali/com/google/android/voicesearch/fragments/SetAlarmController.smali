.class public Lcom/google/android/voicesearch/fragments/SetAlarmController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "SetAlarmController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

.field private final mDateFormat:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/text/DateFormat;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Ljava/text/DateFormat;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    const-string v0, "SetAlarmController"

    const-string v1, "created"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mDateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method private getDefaultIntent(Z)Landroid/content/Intent;
    .locals 4
    .param p1    # Z

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SET_ALARM"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->hasTime()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "android.intent.extra.alarm.HOUR"

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getHour()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "android.intent.extra.alarm.MINUTES"

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getMinute()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getAlarmLabel()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "android.intent.extra.alarm.MESSAGE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    if-eqz p1, :cond_2

    const-string v2, "android.intent.extra.alarm.SKIP_UI"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    return-object v0
.end method

.method private getFormattedTime()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->hasTime()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getHour()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getMinute()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mDateFormat:Ljava/text/DateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getHour()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getHour()I

    move-result v0

    return v0
.end method

.method private getMinute()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->getMinute()I

    move-result v0

    return v0
.end method

.method private hasTime()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getTime()Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionDateTimeProtos$ActionTime;->hasHour()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->hasTime()Z

    move-result v0

    return v0
.end method

.method public detach()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->detach()V

    return-void
.end method

.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x12

    return v0
.end method

.method public initUi()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {v2}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->getAlarmLabel()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;->setLabel(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getFormattedTime()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;->setTime(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->canExecuteAction()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0d03e7

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;

    invoke-interface {v1, v0}, Lcom/google/android/voicesearch/fragments/SetAlarmController$Ui;->setConfirmText(I)V

    return-void

    :cond_0
    const v0, 0x7f0d03e8

    goto :goto_0
.end method

.method protected internalBailOut()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getDefaultIntent(Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->getDefaultIntent(Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->startActivity(Landroid/content/Intent;)Z

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->actionComplete()V

    return-void
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {v0, p1}, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    return-void
.end method

.method public restoreStart()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->showCard()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start(Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SetAlarmController;->mAction:Lcom/google/majel/proto/ActionV2Protos$SetAlarmAction;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SetAlarmController;->showCardAndPlayTts()V

    return-void
.end method
