.class Lcom/google/android/voicesearch/fragments/SetReminderController$1;
.super Ljava/lang/Object;
.source "SetReminderController.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/SetReminderController;->internalExecuteAction()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/SetReminderController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/SetReminderController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SetReminderController$1;->this$0:Lcom/google/android/voicesearch/fragments/SetReminderController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderController$1;->this$0:Lcom/google/android/voicesearch/fragments/SetReminderController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->showSaveSuccess()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderController$1;->this$0:Lcom/google/android/voicesearch/fragments/SetReminderController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->actionComplete()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SetReminderController$1;->this$0:Lcom/google/android/voicesearch/fragments/SetReminderController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/SetReminderController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;->showSaveError()V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/SetReminderController$1;->onResult(Ljava/lang/Boolean;)V

    return-void
.end method
