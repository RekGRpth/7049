.class public interface abstract Lcom/google/android/voicesearch/fragments/SetReminderController$Ui;
.super Ljava/lang/Object;
.source "SetReminderController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;
.implements Lcom/google/android/voicesearch/fragments/CountDownUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/SetReminderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setAbsoluteTimeTrigger(Ljava/lang/String;)V
.end method

.method public abstract setArrivingTrigger(ILjava/lang/String;)V
.end method

.method public abstract setArrivingTrigger(Ljava/lang/String;)V
.end method

.method public abstract setLabel(Ljava/lang/String;)V
.end method

.method public abstract setLeavingTrigger(ILjava/lang/String;)V
.end method

.method public abstract setLeavingTrigger(Ljava/lang/String;)V
.end method

.method public abstract setNoTrigger(Z)V
.end method

.method public abstract showSaveError()V
.end method

.method public abstract showSaveSuccess()V
.end method
