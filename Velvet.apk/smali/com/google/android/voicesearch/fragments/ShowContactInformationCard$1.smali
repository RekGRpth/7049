.class Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$1;
.super Ljava/lang/Object;
.source "ShowContactInformationCard.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->access$000(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->access$000(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->copyToClipboard(Lcom/google/android/speech/contacts/Contact;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
