.class Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$5;
.super Lcom/google/android/voicesearch/contacts/ContactListViewListenerAdapter;
.source "ShowContactInformationCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$5;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    invoke-direct {p0}, Lcom/google/android/voicesearch/contacts/ContactListViewListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onContactSelected(Lcom/google/android/speech/contacts/Contact;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$5;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->navigateToContact(Lcom/google/android/speech/contacts/Contact;)V

    return-void
.end method

.method public onContactTouched(Lcom/google/android/speech/contacts/Contact;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$5;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    # setter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->access$002(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;Lcom/google/android/speech/contacts/Contact;)Lcom/google/android/speech/contacts/Contact;

    return-void
.end method
