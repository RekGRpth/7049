.class final enum Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;
.super Ljava/lang/Enum;
.source "ShowContactInformationCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ContactMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

.field public static final enum ADDRESS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

.field public static final enum EMAIL:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

.field public static final enum PHONE:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

.field public static final enum PHONE_AND_SMS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;


# instance fields
.field final actionIconId:I

.field final contactListId:I

.field final dividerId:I

.field final headlineId:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const v3, 0x7f10021b

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    const-string v1, "PHONE"

    const v4, 0x7f10021c

    const v5, 0x7f10021d

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->PHONE:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    new-instance v4, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    const-string v5, "PHONE_AND_SMS"

    const v8, 0x7f10021c

    const v9, 0x7f10021d

    const v10, 0x7f020080

    move v6, v11

    move v7, v3

    invoke-direct/range {v4 .. v10}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;-><init>(Ljava/lang/String;IIIII)V

    sput-object v4, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->PHONE_AND_SMS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    new-instance v3, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    const-string v4, "EMAIL"

    const v6, 0x7f10021e

    const v7, 0x7f10021f

    const v8, 0x7f100220

    move v5, v12

    move v9, v2

    invoke-direct/range {v3 .. v9}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->EMAIL:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    new-instance v3, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    const-string v4, "ADDRESS"

    const v6, 0x7f100221

    const v7, 0x7f100222

    const v8, 0x7f100223

    move v5, v13

    move v9, v2

    invoke-direct/range {v3 .. v9}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->ADDRESS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    sget-object v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->PHONE:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->PHONE_AND_SMS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->EMAIL:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->ADDRESS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    aput-object v1, v0, v13

    sput-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->$VALUES:[Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->headlineId:I

    iput p4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->dividerId:I

    iput p5, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->contactListId:I

    iput p6, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->actionIconId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    return-object v0
.end method

.method public static values()[Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->$VALUES:[Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    invoke-virtual {v0}, [Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    return-object v0
.end method


# virtual methods
.method isActionButtonClickable()Z
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->actionIconId:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
