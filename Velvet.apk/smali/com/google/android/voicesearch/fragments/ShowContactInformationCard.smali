.class public Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "ShowContactInformationCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/ShowContactInformationController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;"
    }
.end annotation


# instance fields
.field private mContactBadge:Landroid/view/View;

.field private mContactNameView:Landroid/widget/TextView;

.field private mContactNotFoundView:Landroid/widget/TextView;

.field private mContactPictureDivider:Landroid/view/View;

.field private mContactPictureView:Landroid/widget/ImageView;

.field private final mCopyToClipboardMenuItemListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

.field private final mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

.field private mSelectedContact:Lcom/google/android/speech/contacts/Contact;

.field private mVisibleContactMethods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$1;-><init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mCopyToClipboardMenuItemListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$2;-><init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mVisibleContactMethods:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)Lcom/google/android/speech/contacts/Contact;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;Lcom/google/android/speech/contacts/Contact;)Lcom/google/android/speech/contacts/Contact;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mCopyToClipboardMenuItemListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object v0
.end method

.method private hideAllContactMethods()V
    .locals 7

    const/16 v6, 0x8

    invoke-static {}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->values()[Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-direct {p0, v3, v6}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setHeadlineVisibility(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;I)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget v5, v3, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->contactListId:I

    invoke-virtual {v4, v5}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mVisibleContactMethods:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    return-void
.end method

.method private setContactListViewCallbacks(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;Lcom/google/android/voicesearch/contacts/ContactListView$Listener;)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;
    .param p2    # Lcom/google/android/voicesearch/contacts/ContactListView$Listener;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget v2, p1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->contactListId:I

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/contacts/ContactListView;

    invoke-virtual {v0, p2}, Lcom/google/android/voicesearch/contacts/ContactListView;->setContactSelectedListener(Lcom/google/android/voicesearch/contacts/ContactListView$Listener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/contacts/ContactListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    return-void
.end method

.method private setHeadlineVisibility(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;I)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget v1, p1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->headlineId:I

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget v1, p1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->dividerId:I

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showContactInformation(Ljava/util/List;Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;)V
    .locals 6
    .param p2    # Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;",
            "Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mVisibleContactMethods:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    invoke-direct {p0, p2, v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setHeadlineVisibility(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;I)V

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mVisibleContactMethods:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget v2, p2, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->contactListId:I

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/contacts/ContactListView;

    invoke-virtual {v0, v3}, Lcom/google/android/voicesearch/contacts/ContactListView;->setVisibility(I)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/speech/contacts/Contact;

    invoke-interface {p1, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/speech/contacts/Contact;

    const v2, 0x7f0400b3

    iget v3, p2, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->actionIconId:I

    const/4 v4, 0x4

    invoke-virtual {p2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->isActionButtonClickable()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/contacts/ContactListView;->setContacts([Lcom/google/android/speech/contacts/Contact;IIIZ)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mVisibleContactMethods:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    invoke-direct {p0, v1, v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setHeadlineVisibility(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;I)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactPictureDivider:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showErrorMessage(IIZ)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->hideAllContactMethods()V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactBadge:Landroid/view/View;

    if-eqz p3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactNotFoundView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0, p2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setConfirmText(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactNotFoundView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->getController()Lcom/google/android/voicesearch/fragments/AbstractCardController;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->uiReady()V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardView;->onAttachedToWindow()V

    sget-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->PHONE:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    new-instance v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$3;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$3;-><init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setContactListViewCallbacks(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;Lcom/google/android/voicesearch/contacts/ContactListView$Listener;)V

    sget-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->EMAIL:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    new-instance v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$4;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$4;-><init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setContactListViewCallbacks(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;Lcom/google/android/voicesearch/contacts/ContactListView$Listener;)V

    sget-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->ADDRESS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    new-instance v1, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$5;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$5;-><init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setContactListViewCallbacks(Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;Lcom/google/android/voicesearch/contacts/ContactListView$Listener;)V

    return-void
.end method

.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const v0, 0x7f0400b4

    invoke-virtual {p0, p2, p3, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setContentClickable(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100061

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactNameView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100049

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactNotFoundView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f10004a

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactPictureView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f10021a

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactPictureDivider:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    const v1, 0x7f100069

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactBadge:Landroid/view/View;

    const v0, 0x7f020068

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setConfirmIcon(I)V

    const v0, 0x7f0d0453

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->setConfirmText(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mMainContentView:Lcom/google/android/voicesearch/ui/ActionEditorView;

    return-object v0
.end method

.method public setContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 5
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactNameView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactNameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactPictureView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;-><init>(Landroid/widget/ImageView;Lcom/google/android/voicesearch/ui/ActionEditorView;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/ActionEditorSetContactPictureTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->hideAllContactMethods()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->mContactPictureDivider:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showContactDetailsNotFound()V
    .locals 3

    const v0, 0x7f0d0455

    const v1, 0x7f0d0454

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->showErrorMessage(IIZ)V

    return-void
.end method

.method public showContactNotFound()V
    .locals 3

    const v0, 0x7f0d0440

    const v1, 0x7f0d0441

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->showErrorMessage(IIZ)V

    return-void
.end method

.method public showEmailAddressNotFound()V
    .locals 3

    const v0, 0x7f0d0457

    const v1, 0x7f0d0454

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->showErrorMessage(IIZ)V

    return-void
.end method

.method public showEmailAddresses(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->EMAIL:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->showContactInformation(Ljava/util/List;Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;)V

    return-void
.end method

.method public showPhoneNumberNotFound()V
    .locals 3

    const v0, 0x7f0d0456

    const v1, 0x7f0d0454

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->showErrorMessage(IIZ)V

    return-void
.end method

.method public showPhoneNumbers(Ljava/util/List;Z)V
    .locals 1
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;Z)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->PHONE_AND_SMS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->showContactInformation(Ljava/util/List;Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->PHONE:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    goto :goto_0
.end method

.method public showPostalAddressNotFound()V
    .locals 3

    const v0, 0x7f0d0458

    const v1, 0x7f0d0454

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->showErrorMessage(IIZ)V

    return-void
.end method

.method public showPostalAddresses(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;->ADDRESS:Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationCard;->showContactInformation(Ljava/util/List;Lcom/google/android/voicesearch/fragments/ShowContactInformationCard$ContactMethod;)V

    return-void
.end method
