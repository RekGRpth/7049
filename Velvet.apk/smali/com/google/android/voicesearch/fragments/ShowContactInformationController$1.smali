.class Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;
.super Ljava/lang/Object;
.source "ShowContactInformationController.java"

# interfaces
.implements Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/ShowContactInformationController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/contacts/ContactSelectController;Lcom/google/android/speech/contacts/ContactLookup;Lcom/google/android/voicesearch/util/EmailSender;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Landroid/content/ClipboardManager;Lcom/google/android/searchcommon/DeviceCapabilityManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBailOut()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->bailOut()V

    return-void
.end method

.method public onContactSelected(Lcom/google/android/speech/contacts/Contact;Z)V
    .locals 2
    .param p1    # Lcom/google/android/speech/contacts/Contact;
    .param p2    # Z

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$000(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/Contact;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # invokes: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->lookUpContactData(Lcom/google/android/speech/contacts/Contact;)V
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$100(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Lcom/google/android/speech/contacts/Contact;)V

    return-void
.end method

.method public onNoContactFound()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->showCard()V

    return-void
.end method

.method public onShowingDisambiguation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->showNoCard()V

    return-void
.end method
