.class Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;
.super Landroid/os/AsyncTask;
.source "ShowContactInformationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->lookUpContactData(Lcom/google/android/speech/contacts/Contact;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

.field final synthetic val$contactId:J


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;J)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iput-wide p2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->val$contactId:J

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 7
    .param p1    # [Ljava/lang/Void;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$200(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :pswitch_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$500(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->val$contactId:J

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$400(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchPhoneNumbers(JLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    # setter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$302(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$300(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    :cond_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$500(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->val$contactId:J

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$400(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchEmailAddresses(JLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    # setter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$602(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$600(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$500(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->val$contactId:J

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$400(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchPostalAddresses(JLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    # setter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$702(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$700(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$500(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->val$contactId:J

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$400(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchPhoneNumbers(JLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    # setter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$302(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$500(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->val$contactId:J

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$400(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchEmailAddresses(JLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    # setter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$602(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$500(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/ContactLookup;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->val$contactId:J

    iget-object v6, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$400(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/speech/contacts/ContactLookup;->fetchPostalAddresses(JLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    # setter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;
    invoke-static {v2, v3}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$702(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$300(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$600(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    # getter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$700(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    :goto_3
    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    # setter for: Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactDetailsFound:Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->access$802(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Z)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->this$0:Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->showCard()V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
