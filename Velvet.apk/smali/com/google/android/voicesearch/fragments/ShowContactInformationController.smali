.class public Lcom/google/android/voicesearch/fragments/ShowContactInformationController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "ShowContactInformationController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mClipboardLabel:Ljava/lang/String;

.field private final mClipboardManager:Landroid/content/ClipboardManager;

.field private mContactDetailsFound:Z

.field private final mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

.field private mContactMethod:I

.field private final mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

.field private final mContactSelectedCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

.field private final mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private mEmailAddresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

.field private mPhoneNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private mPostalAddresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedContact:Lcom/google/android/speech/contacts/Contact;

.field private mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/voicesearch/contacts/ContactSelectController;Lcom/google/android/speech/contacts/ContactLookup;Lcom/google/android/voicesearch/util/EmailSender;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Landroid/content/ClipboardManager;Lcom/google/android/searchcommon/DeviceCapabilityManager;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/voicesearch/contacts/ContactSelectController;
    .param p4    # Lcom/google/android/speech/contacts/ContactLookup;
    .param p5    # Lcom/google/android/voicesearch/util/EmailSender;
    .param p6    # Ljava/util/concurrent/ExecutorService;
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/content/ClipboardManager;
    .param p9    # Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;)V

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p7, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mClipboardLabel:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mClipboardManager:Landroid/content/ClipboardManager;

    iput-object p9, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    new-instance v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$1;-><init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactSelectedCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/Contact;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Lcom/google/android/speech/contacts/Contact;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->lookUpContactData(Lcom/google/android/speech/contacts/Contact;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)I
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Lcom/google/android/speech/contacts/ContactLookup;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactLookup:Lcom/google/android/speech/contacts/ContactLookup;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/ShowContactInformationController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactDetailsFound:Z

    return p1
.end method

.method static getAndClearContactMethodType(Ljava/util/List;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v6, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/ActionV2Protos$ActionContact;

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getPhoneList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->hasType()Z

    move-result v7

    if-eqz v7, :cond_1

    if-nez v6, :cond_2

    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->getType()Ljava/lang/String;

    move-result-object v6

    :cond_2
    invoke-virtual {v4}, Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;->clearType()Lcom/google/majel/proto/ActionV2Protos$ContactPhoneNumber;

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getEmailList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/majel/proto/ActionV2Protos$ContactEmail;

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ContactEmail;->hasType()Z

    move-result v7

    if-eqz v7, :cond_4

    if-nez v6, :cond_5

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ContactEmail;->getType()Ljava/lang/String;

    move-result-object v6

    :cond_5
    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$ContactEmail;->clearType()Lcom/google/majel/proto/ActionV2Protos$ContactEmail;

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionContact;->getAddressList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/majel/proto/ActionV2Protos$ContactPostalAddress;

    invoke-virtual {v5}, Lcom/google/majel/proto/ActionV2Protos$ContactPostalAddress;->hasType()Z

    move-result v7

    if-eqz v7, :cond_7

    if-nez v6, :cond_8

    invoke-virtual {v5}, Lcom/google/majel/proto/ActionV2Protos$ContactPostalAddress;->getType()Ljava/lang/String;

    move-result-object v6

    :cond_8
    invoke-virtual {v5}, Lcom/google/majel/proto/ActionV2Protos$ContactPostalAddress;->clearType()Lcom/google/majel/proto/ActionV2Protos$ContactPostalAddress;

    goto :goto_2

    :cond_9
    return-object v6
.end method

.method private lookUpContactData(Lcom/google/android/speech/contacts/Contact;)V
    .locals 5
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/speech/contacts/Contact;

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v2}, Lcom/google/android/speech/contacts/Contact;->getId()J

    move-result-wide v0

    new-instance v2, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;-><init>(Lcom/google/android/voicesearch/fragments/ShowContactInformationController;J)V

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public callContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->cancelCountDown()V

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getCallIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public copyToClipboard(Lcom/google/android/speech/contacts/Contact;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mClipboardManager:Landroid/content/ClipboardManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mClipboardLabel:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    return-void
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/16 v0, 0x21

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "selected_contact"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "phone_numbers"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/searchcommon/util/Util;->asArrayList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "email_addresses"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/searchcommon/util/Util;->asArrayList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "postal_addresses"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/searchcommon/util/Util;->asArrayList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "contact_method"

    iget v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "type"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "contact_details_found"

    iget-boolean v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactDetailsFound:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x25

    return v0
.end method

.method protected initUi()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->showContactNotFound()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->uiReady()V

    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactDetailsFound:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->setContact(Lcom/google/android/speech/contacts/Contact;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-interface {v2}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->isTelephoneCapable()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->showPhoneNumbers(Ljava/util/List;Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->showEmailAddresses(Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->showPostalAddresses(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->setContact(Lcom/google/android/speech/contacts/Contact;)V

    iget v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    if-nez v1, :cond_3

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->showContactDetailsNotFound()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->showPhoneNumberNotFound()V

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->showEmailAddressNotFound()V

    goto :goto_0

    :cond_5
    iget v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController$Ui;->showPostalAddressNotFound()V

    goto :goto_0
.end method

.method protected internalBailOut()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactDetailsFound:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getShowContactIntent(Lcom/google/android/speech/contacts/Contact;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->startActivity(Landroid/content/Intent;)Z

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    invoke-static {v0}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getEditContactIntent(Lcom/google/android/speech/contacts/Contact;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method protected internalExecuteAction()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->internalBailOut()V

    return-void
.end method

.method public navigateToContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->cancelCountDown()V

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/MapUtil;->getMapsSearchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method public sendEmailToContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->cancelCountDown()V

    new-instance v0, Lcom/google/android/voicesearch/util/EmailSender$Email;

    invoke-direct {v0}, Lcom/google/android/voicesearch/util/EmailSender$Email;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->toRfc822Token()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    iput-object v1, v0, Lcom/google/android/voicesearch/util/EmailSender$Email;->to:[Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailSender:Lcom/google/android/voicesearch/util/EmailSender;

    invoke-virtual {v1, v0, v3, p0}, Lcom/google/android/voicesearch/util/EmailSender;->sendEmail(Lcom/google/android/voicesearch/util/EmailSender$Email;ZLcom/google/android/searchcommon/util/IntentStarter;)V

    return-void
.end method

.method public sendTextToContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->cancelCountDown()V

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getSendSmsIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "selected_contact"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/speech/contacts/Contact;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    const-string v1, "phone_numbers"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;

    const-string v1, "email_addresses"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;

    const-string v1, "postal_addresses"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;

    const-string v1, "contact_method"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;

    const-string v1, "contact_details_found"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactDetailsFound:Z

    return-void
.end method

.method public start(Ljava/util/List;I)V
    .locals 6
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/majel/proto/ActionV2Protos$ActionContact;",
            ">;I)V"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mSelectedContact:Lcom/google/android/speech/contacts/Contact;

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPhoneNumbers:Ljava/util/List;

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mEmailAddresses:Ljava/util/List;

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mPostalAddresses:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput p2, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    iget v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactMethod:I

    if-nez v0, :cond_2

    :cond_0
    :goto_1
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-static {p1}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->getAndClearContactMethodType(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mType:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->requireShowCard()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactSelectController:Lcom/google/android/voicesearch/contacts/ContactSelectController;

    sget-object v2, Lcom/google/android/voicesearch/contacts/ContactSelectMode;->SHOW_CONTACT_INFO:Lcom/google/android/voicesearch/contacts/ContactSelectMode;

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/ShowContactInformationController;->mContactSelectedCallback:Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/contacts/ContactSelectController;->pickContact(Ljava/util/List;Lcom/google/android/voicesearch/contacts/ContactSelectMode;ZLjava/lang/String;Lcom/google/android/voicesearch/contacts/ContactSelectController$ContactSelectedCallback;)V

    return-void

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1
.end method
