.class public Lcom/google/android/voicesearch/fragments/SingleImageResultController;
.super Lcom/google/android/voicesearch/fragments/ImageResultController;
.source "SingleImageResultController.java"


# instance fields
.field private final mRandom:Ljava/util/Random;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Ljava/lang/String;Lcom/google/android/searchcommon/CoreSearchServices;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/searchcommon/CoreSearchServices;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/voicesearch/fragments/ImageResultController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Ljava/lang/String;Lcom/google/android/searchcommon/CoreSearchServices;)V

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleImageResultController;->mRandom:Ljava/util/Random;

    return-void
.end method


# virtual methods
.method public initUi()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleImageResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    invoke-interface {v2}, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;->clearImages()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleImageResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;->setImageClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SingleImageResultController;->mData:Lcom/google/android/voicesearch/speechservice/ImageResultData;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/speechservice/ImageResultData;->getImageList()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SingleImageResultController;->mRandom:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    :goto_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/speechservice/ImageParcelable;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleImageResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v2

    check-cast v2, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;

    invoke-interface {v2, v1}, Lcom/google/android/voicesearch/fragments/ImageResultController$Ui;->setImage(Lcom/google/android/voicesearch/speechservice/ImageParcelable;)V

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
