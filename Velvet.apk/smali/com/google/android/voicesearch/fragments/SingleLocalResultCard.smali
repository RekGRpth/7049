.class public Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "SingleLocalResultCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/SingleLocalResultController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;"
    }
.end annotation


# instance fields
.field private mAddress:Landroid/widget/TextView;

.field private mCallButton:Landroid/widget/TextView;

.field private mCountDownLayout:Landroid/view/View;

.field private mMapImage:Lcom/google/android/velvet/ui/WebImageView;

.field private mMarker:Landroid/widget/TextView;

.field private mPhoneNumber:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;)Lcom/google/android/voicesearch/ui/TravelModeSpinner;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const v3, 0x7f0400b5

    const v4, 0x7f0400b6

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->createActionEditor(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;IIZ)Lcom/google/android/voicesearch/ui/ActionEditorView;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/voicesearch/ui/ActionEditorView;->setContentClickable(Z)V

    const v0, 0x7f100224

    invoke-virtual {v6, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    new-instance v1, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard$1;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard$1;-><init>(Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    new-instance v1, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard$2;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard$2;-><init>(Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v0, 0x7f10022a

    invoke-virtual {v6, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/WebImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mMapImage:Lcom/google/android/velvet/ui/WebImageView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mMapImage:Lcom/google/android/velvet/ui/WebImageView;

    new-instance v1, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard$3;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard$3;-><init>(Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/WebImageView;->setOnDownloadListener(Lcom/google/android/velvet/ui/WebImageView$Listener;)V

    const v0, 0x7f100225

    invoke-virtual {v6, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mMarker:Landroid/widget/TextView;

    const v0, 0x7f100226

    invoke-virtual {v6, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f100227

    invoke-virtual {v6, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mAddress:Landroid/widget/TextView;

    const v0, 0x7f100228

    invoke-virtual {v6, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mPhoneNumber:Landroid/widget/TextView;

    const v0, 0x7f10007c

    invoke-virtual {v6, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mCountDownLayout:Landroid/view/View;

    const v0, 0x7f100229

    invoke-virtual {v6, v0}, Lcom/google/android/voicesearch/ui/ActionEditorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mCallButton:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mCallButton:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard$4;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard$4;-><init>(Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v6
.end method

.method public setActionTypeAndTransportationMethod(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mTravelModeSpinner:Lcom/google/android/voicesearch/ui/TravelModeSpinner;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ui/TravelModeSpinner;->setActionType(II)V

    return-void
.end method

.method public setCountDownLayoutVisible(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mCountDownLayout:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setLocalResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/view/View$OnClickListener;

    const/16 v3, 0x8

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mAddress:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mMarker:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mMarker:Landroid/widget/TextView;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mMarker:Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public setMapImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mMapImage:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setMapImageUrl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mMapImage:Lcom/google/android/velvet/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/WebImageView;->setImageUrl(Ljava/lang/String;)V

    return-void
.end method

.method public showCallAction(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultCard;->mCallButton:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
