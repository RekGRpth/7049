.class public interface abstract Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;
.super Ljava/lang/Object;
.source "SingleLocalResultController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;
.implements Lcom/google/android/voicesearch/fragments/CountDownUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/SingleLocalResultController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setActionTypeAndTransportationMethod(II)V
.end method

.method public abstract setConfirmIcon(I)V
.end method

.method public abstract setConfirmText(I)V
.end method

.method public abstract setCountDownLayoutVisible(Z)V
.end method

.method public abstract setLocalResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
.end method

.method public abstract setMapImageBitmap(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setMapImageUrl(Ljava/lang/String;)V
.end method

.method public abstract showCallAction(Z)V
.end method
