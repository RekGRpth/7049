.class public Lcom/google/android/voicesearch/fragments/SingleLocalResultController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "SingleLocalResultController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private mActionType:I

.field private final mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

.field private mTransportationMethod:I


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/searchcommon/DeviceCapabilityManager;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    return-void
.end method

.method private shouldShowCountDown()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getActionType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-interface {v0}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->isTelephoneCapable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public callResult()V
    .locals 1

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mActionType:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->executeAction(Z)V

    return-void
.end method

.method protected getActionState()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    iget v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mActionType:I

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->getActionTypeLog(I)I

    move-result v0

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "transportation_method"

    iget v2, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mTransportationMethod:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x18

    return v0
.end method

.method public initUi()V
    .locals 10

    const/4 v8, 0x0

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v7, v8}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResult(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    move-result-object v2

    new-instance v4, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$1;

    invoke-direct {v4, p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$1;-><init>(Lcom/google/android/voicesearch/fragments/SingleLocalResultController;)V

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getPhoneNumber()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v6

    check-cast v6, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;

    iget v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mActionType:I

    iget v9, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mTransportationMethod:I

    invoke-interface {v6, v7, v9}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;->setActionTypeAndTransportationMethod(II)V

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v7, v9, v5, v4}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;->setLocalResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    iget v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mActionType:I

    invoke-static {v7}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->getActionLabelStringId(I)I

    move-result v7

    invoke-interface {v6, v7}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;->setConfirmText(I)V

    iget v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mActionType:I

    invoke-static {v7}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->getActionIconImageResource(I)I

    move-result v7

    invoke-interface {v6, v7}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;->setConfirmIcon(I)V

    iget v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mActionType:I

    const/4 v9, 0x4

    if-eq v7, v9, :cond_0

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-interface {v7}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->isTelephoneCapable()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v7, 0x1

    :goto_0
    invoke-interface {v6, v7}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;->showCallAction(Z)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->shouldShowCountDown()Z

    move-result v7

    invoke-interface {v6, v7}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;->setCountDownLayoutVisible(Z)V

    const/4 v3, 0x0

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->hasPreviewImage()Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getPreviewImage()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v1

    array-length v7, v1

    invoke-static {v1, v8, v7}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;->setMapImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v3, 0x1

    :cond_1
    if-nez v3, :cond_3

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v7}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getPreviewImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController$Ui;->setMapImageUrl(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_2
    move v7, v8

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->uiReady()V

    goto :goto_1
.end method

.method protected internalBailOut()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getMapsUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/util/MapUtil;->getMapsIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->startActivity(Landroid/content/Intent;)Z

    return-void
.end method

.method protected internalExecuteAction()V
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v2}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getOriginCount()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResult(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v2, v3}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getLocalResult(I)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;

    move-result-object v0

    iget v2, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mActionType:I

    iget v3, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mTransportationMethod:I

    invoke-static {v2, v1, v0, v3}, Lcom/google/android/voicesearch/fragments/LocalResultUtils;->createIntentForAction(ILcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResult;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->startActivity(Landroid/content/Intent;)Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected parseActionState([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->parseFrom([B)Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "transportation_method"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mTransportationMethod:I

    return-void
.end method

.method public setTransportationMethod(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mTransportationMethod:I

    return-void
.end method

.method public start(Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;)V
    .locals 1
    .param p1    # Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getActionType()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mActionType:I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mResults:Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;

    invoke-virtual {v0}, Lcom/google/majel/proto/EcoutezStructuredResponse$EcoutezLocalResults;->getTransportationMethod()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->mTransportationMethod:I

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->shouldShowCountDown()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->showCardAndPlayTts()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->disableCountDown()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SingleLocalResultController;->showCard()V

    goto :goto_0
.end method

.method public uiReady()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/voicesearch/fragments/AbstractCardController;->uiReady()V

    return-void
.end method
