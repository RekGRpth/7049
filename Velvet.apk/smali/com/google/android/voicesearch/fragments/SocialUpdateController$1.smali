.class Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;
.super Landroid/os/AsyncTask;
.source "SocialUpdateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/fragments/SocialUpdateController;->internalStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/SocialUpdateController;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/SocialUpdateController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;->this$0:Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/content/Intent;
    .locals 4
    .param p1    # [Ljava/lang/Void;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;->this$0:Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->createIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;->this$0:Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    # getter for: Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->access$000(Lcom/google/android/voicesearch/fragments/SocialUpdateController;)Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;->doInBackground([Ljava/lang/Void;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;->this$0:Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    # invokes: Lcom/google/android/voicesearch/fragments/SocialUpdateController;->setIntent(Landroid/content/Intent;)V
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->access$100(Lcom/google/android/voicesearch/fragments/SocialUpdateController;Landroid/content/Intent;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method
