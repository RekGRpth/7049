.class final enum Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;
.super Ljava/lang/Enum;
.source "SocialUpdateController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/SocialUpdateController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SocialNetwork"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

.field public static final enum GOOGLE_PLUS:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

.field public static final enum TWITTER:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;


# instance fields
.field final nameResId:I

.field final notSupportedResId:I

.field final pkg:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    const-string v1, "GOOGLE_PLUS"

    const-string v3, "com.google.android.apps.plus"

    const v4, 0x7f0d0421

    const v5, 0x7f0d0423

    invoke-direct/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->GOOGLE_PLUS:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    new-instance v3, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    const-string v4, "TWITTER"

    const-string v6, "com.twitter.android"

    const v7, 0x7f0d0422

    const v8, 0x7f0d0424

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->TWITTER:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    sget-object v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->GOOGLE_PLUS:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->TWITTER:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    aput-object v1, v0, v9

    sput-object v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->$VALUES:[Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 0
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->pkg:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->nameResId:I

    iput p5, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->notSupportedResId:I

    return-void
.end method

.method static fromActionV2Proto(I)Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->GOOGLE_PLUS:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->TWITTER:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    return-object v0
.end method

.method public static values()[Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->$VALUES:[Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    invoke-virtual {v0}, [Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    return-object v0
.end method
