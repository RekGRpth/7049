.class public interface abstract Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;
.super Ljava/lang/Object;
.source "SocialUpdateController.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/BaseCardUi;
.implements Lcom/google/android/voicesearch/fragments/CountDownUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/SocialUpdateController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Ui"
.end annotation


# virtual methods
.method public abstract setMessageBody(Ljava/lang/CharSequence;)V
.end method

.method public abstract showEmptyView()V
.end method

.method public abstract showNewUpdate(I)V
.end method

.method public abstract showPostUpdate(I)V
.end method
