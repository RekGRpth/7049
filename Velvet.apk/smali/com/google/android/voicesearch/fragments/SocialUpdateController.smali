.class public Lcom/google/android/voicesearch/fragments/SocialUpdateController;
.super Lcom/google/android/voicesearch/fragments/AbstractCardController;
.source "SocialUpdateController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;,
        Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardController",
        "<",
        "Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;",
        ">;"
    }
.end annotation


# instance fields
.field private final mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mIntent:Landroid/content/Intent;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPuntController:Lcom/google/android/voicesearch/fragments/PuntController;

.field private mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

.field private mUpdateText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Landroid/content/pm/PackageManager;Lcom/google/android/voicesearch/fragments/PuntController;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/CardController;
    .param p2    # Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Landroid/content/pm/PackageManager;
    .param p5    # Lcom/google/android/voicesearch/fragments/PuntController;
    .param p6    # Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/AbstractCardController;-><init>(Lcom/google/android/voicesearch/CardController;Lcom/google/android/voicesearch/audio/TtsAudioPlayer;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mPackageManager:Landroid/content/pm/PackageManager;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mPuntController:Lcom/google/android/voicesearch/fragments/PuntController;

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/SocialUpdateController;)Landroid/content/pm/PackageManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/SocialUpdateController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/SocialUpdateController;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/SocialUpdateController;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method private internalStart()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->clearCard()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;-><init>(Lcom/google/android/voicesearch/fragments/SocialUpdateController;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private setIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mPuntController:Lcom/google/android/voicesearch/fragments/PuntController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    iget v1, v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->notSupportedResId:I

    const v2, 0x7f0d045c

    const v3, 0x7f02006e

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->createPlayStoreIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/PuntController;->createData(IIILandroid/content/Intent;)Lcom/google/android/voicesearch/fragments/PuntController$Data;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/PuntController;->start(Lcom/google/android/voicesearch/fragments/PuntController$Data;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->showNoCard()V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mIntent:Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mPuntController:Lcom/google/android/voicesearch/fragments/PuntController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/PuntController;->showNoCard()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->showCard()V

    goto :goto_0
.end method


# virtual methods
.method protected canExecuteAction()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method createIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mUpdateText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->pkg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method createPlayStoreIntent()Landroid/content/Intent;
    .locals 6

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://play.google.com/store/apps/details?id=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    iget-object v5, v5, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->pkg:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method protected getActionTypeLog()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method protected getParcelableState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "updateText"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mUpdateText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string v1, "socialNetwork"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method protected getScreenTypeLog()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method

.method public initUi()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mUpdateText:Ljava/lang/CharSequence;

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;->setMessageBody(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mUpdateText:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;->showEmptyView()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    iget v2, v2, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->nameResId:I

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;->showNewUpdate(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->getUi()Lcom/google/android/voicesearch/fragments/BaseCardUi;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    iget v2, v2, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->nameResId:I

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$Ui;->showPostUpdate(I)V

    goto :goto_0
.end method

.method protected internalBailOut()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->startActivity(Landroid/content/Intent;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreStart()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->internalStart()V

    return-void
.end method

.method protected setParcelableState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "updateText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mUpdateText:Ljava/lang/CharSequence;

    invoke-static {}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->values()[Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    move-result-object v1

    const-string v2, "socialNetwork"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    return-void
.end method

.method public start(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mUpdateText:Ljava/lang/CharSequence;

    invoke-static {p2}, Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;->fromActionV2Proto(I)Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->mSocialNetwork:Lcom/google/android/voicesearch/fragments/SocialUpdateController$SocialNetwork;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/SocialUpdateController;->internalStart()V

    return-void
.end method
