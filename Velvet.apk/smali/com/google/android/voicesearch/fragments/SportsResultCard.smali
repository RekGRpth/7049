.class public Lcom/google/android/voicesearch/fragments/SportsResultCard;
.super Lcom/google/android/voicesearch/fragments/AbstractCardView;
.source "SportsResultCard.java"

# interfaces
.implements Lcom/google/android/voicesearch/fragments/SportsResultController$Ui;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/voicesearch/fragments/AbstractCardView",
        "<",
        "Lcom/google/android/voicesearch/fragments/SportsResultController;",
        ">;",
        "Lcom/google/android/voicesearch/fragments/SportsResultController$Ui;"
    }
.end annotation


# instance fields
.field private mContent:Lcom/google/android/velvet/cards/SportsMatchCard;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/AbstractCardView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private getFormattedStartTime(J)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # J

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/google/android/apps/sidekick/TimeUtilities;->formatDisplayTime(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private setStatus(Lcom/google/android/velvet/cards/SportsMatchCard$Builder;Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V
    .locals 13
    .param p1    # Lcom/google/android/velvet/cards/SportsMatchCard$Builder;
    .param p2    # Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    const/4 v7, 0x0

    invoke-virtual {p2}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getStartDateTimeMillis()J

    move-result-wide v5

    invoke-virtual {p2}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getStatus()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    const-wide/16 v8, 0x0

    cmp-long v8, v5, v8

    if-lez v8, :cond_0

    invoke-direct {p0, v5, v6}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->getFormattedStartTime(J)Ljava/lang/CharSequence;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-virtual {p1, v7}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->setStatus(Ljava/lang/CharSequence;)Lcom/google/android/velvet/cards/SportsMatchCard$Builder;

    return-void

    :pswitch_0
    const-wide/16 v8, 0x0

    cmp-long v8, v5, v8

    if-lez v8, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0d0143

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-direct {p0, v5, v6}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->getFormattedStartTime(J)Ljava/lang/CharSequence;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0d0142

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getCurrentPeriod()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getBaseballData()Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    move-result-object v8

    if-eqz v8, :cond_2

    const v3, 0x7f0d0146

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->getContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v11, v0, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v3, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getElapsedTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v1, v8

    if-lez v1, :cond_3

    int-to-long v8, v1

    invoke-static {v8, v9}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0d0144

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    const/4 v11, 0x1

    aput-object v2, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_2
    const v3, 0x7f0d014a

    goto :goto_1

    :cond_3
    move-object v7, v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCreateView(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # Landroid/os/Bundle;

    new-instance v0, Lcom/google/android/velvet/cards/SportsMatchCard;

    invoke-direct {v0, p1}, Lcom/google/android/velvet/cards/SportsMatchCard;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/SportsResultCard;->mContent:Lcom/google/android/velvet/cards/SportsMatchCard;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SportsResultCard;->mContent:Lcom/google/android/velvet/cards/SportsMatchCard;

    return-object v0
.end method

.method public setInfoLink(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SportsResultCard;->mContent:Lcom/google/android/velvet/cards/SportsMatchCard;

    invoke-virtual {v0}, Lcom/google/android/velvet/cards/SportsMatchCard;->clearActions()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/SportsResultCard;->mContent:Lcom/google/android/velvet/cards/SportsMatchCard;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/voicesearch/fragments/SportsResultCard$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/fragments/SportsResultCard$1;-><init>(Lcom/google/android/voicesearch/fragments/SportsResultCard;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/velvet/cards/SportsMatchCard;->addAction(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setMatchData(Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V
    .locals 12
    .param p1    # Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;

    new-instance v1, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;

    invoke-direct {v1}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$Builder;

    invoke-direct {p0, v1, p1}, Lcom/google/android/voicesearch/fragments/SportsResultCard;->setStatus(Lcom/google/android/velvet/cards/SportsMatchCard$Builder;Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;)V

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->leftContestant()Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getFirstTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getShortName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getFirstTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getScore()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setScore(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getFirstTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getLogoUri()Landroid/net/Uri;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setLogo(Landroid/net/Uri;Landroid/graphics/Rect;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    invoke-virtual {v1}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->rightContestant()Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getSecondTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getShortName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getSecondTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getScore()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setScore(Ljava/lang/String;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getSecondTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getLogoUri()Landroid/net/Uri;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;->setLogo(Landroid/net/Uri;Landroid/graphics/Rect;)Lcom/google/android/velvet/cards/SportsMatchCard$ContestantBuilder;

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getBaseballData()Lcom/google/android/voicesearch/speechservice/BaseballResultData;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v9, 0x3

    new-array v3, v9, [I

    fill-array-data v3, :array_0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getInningsCount()I

    move-result v9

    new-array v7, v9, [I

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getInningsCount()I

    move-result v9

    if-ge v4, v9, :cond_0

    add-int/lit8 v9, v4, 0x1

    aput v9, v7, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;

    invoke-direct {v2, v7, v3}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;-><init>([I[I)V

    invoke-virtual {v2}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->firstContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getFirstTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getShortName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getFirstTeam()Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getRuns()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getFirstTeam()Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getHits()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    const/4 v10, 0x2

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getFirstTeam()Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getErrors()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    invoke-virtual {v2}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->secondContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData;->getSecondTeamData()Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/voicesearch/speechservice/SportsMatchResultData$TeamData;->getShortName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setName(Ljava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getSecondTeam()Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getRuns()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getSecondTeam()Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getHits()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    const/4 v10, 0x2

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getSecondTeam()Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/voicesearch/speechservice/BaseballResultData$BaseballTeamData;->getErrors()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getFirstTeamScores()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->firstContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v8}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setPeriodScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/speechservice/BaseballResultData;->getSecondTeamScores()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/velvet/cards/MatchDetailsView$Builder;->secondContestant()Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v8}, Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;->setPeriodScore(ILjava/lang/String;)Lcom/google/android/velvet/cards/MatchDetailsView$ContestantBuilder;

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v1, v2}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->setMatchDetailsBuilder(Lcom/google/android/velvet/cards/SportsMatchCard$MatchDetailsBuilder;)Lcom/google/android/velvet/cards/SportsMatchCard$Builder;

    :cond_3
    iget-object v9, p0, Lcom/google/android/voicesearch/fragments/SportsResultCard;->mContent:Lcom/google/android/velvet/cards/SportsMatchCard;

    invoke-virtual {v1, v9}, Lcom/google/android/velvet/cards/SportsMatchCard$Builder;->update(Lcom/google/android/velvet/cards/SportsMatchCard;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0d0147
        0x7f0d0148
        0x7f0d0149
    .end array-data
.end method
