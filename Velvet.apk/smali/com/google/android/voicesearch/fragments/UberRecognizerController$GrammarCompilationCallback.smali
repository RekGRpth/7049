.class Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;
.super Ljava/lang/Object;
.source "UberRecognizerController.java"

# interfaces
.implements Lcom/google/android/speech/callback/SimpleCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/UberRecognizerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GrammarCompilationCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/SimpleCallback",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mRecognizerMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field private final mResultsListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams$Mode;)V
    .locals 0
    .param p2    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p3    # Lcom/google/android/speech/params/RecognizerParams$Mode;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->mResultsListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->mRecognizerMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    return-void
.end method

.method private reportError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onError(Ljava/lang/String;Lcom/google/android/speech/exception/RecognizeException;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->mResultsListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->mResultsListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-interface {v0, p1}, Lcom/google/android/speech/listeners/RecognitionEventListener;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onResult(Ljava/lang/Integer;)V
    .locals 3
    .param p1    # Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->mRecognizerMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->mResultsListener:Lcom/google/android/speech/listeners/RecognitionEventListener;

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startEmbeddedRecognitionInternal(Lcom/google/android/speech/params/RecognizerParams$Mode;Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$200(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/speech/params/RecognizerParams$Mode;Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/google/android/speech/embedded/OfflineActionsManager$GrammarCompilationException;

    invoke-direct {v0}, Lcom/google/android/speech/embedded/OfflineActionsManager$GrammarCompilationException;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->reportError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/google/android/speech/exception/NetworkRecognizeException;

    const-string v1, "No network connection"

    invoke-direct {v0, v1}, Lcom/google/android/speech/exception/NetworkRecognizeException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->reportError(Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;->onResult(Ljava/lang/Integer;)V

    return-void
.end method
