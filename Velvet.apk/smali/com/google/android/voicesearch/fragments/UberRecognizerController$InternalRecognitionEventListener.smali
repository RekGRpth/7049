.class Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;
.super Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.source "UberRecognizerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/UberRecognizerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalRecognitionEventListener"
.end annotation


# instance fields
.field private mMajelResponseReceived:Z

.field private mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

.field private final mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

.field private mSoundSearchResponseReceived:Z

.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-direct {p0}, Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;-><init>()V

    new-instance v0, Lcom/google/android/speech/utils/RecognizedText;

    invoke-direct {v0}, Lcom/google/android/speech/utils/RecognizedText;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mSoundSearchResponseReceived:Z

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mMajelResponseReceived:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/voicesearch/fragments/UberRecognizerController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p2    # Lcom/google/android/voicesearch/fragments/UberRecognizerController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)V

    return-void
.end method

.method private dispatchNoMatchException()V
    .locals 2

    const-string v0, "no_match"

    invoke-static {v0}, Lcom/google/android/speech/test/TestPlatformLog;->logError(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    new-instance v1, Lcom/google/android/speech/exception/NoMatchRecognizeException;

    invoke-direct {v1}, Lcom/google/android/speech/exception/NoMatchRecognizeException;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onNoMatch(Lcom/google/android/speech/exception/NoMatchRecognizeException;)V

    return-void
.end method

.method private dispatchNoSoundSearchMatchException()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    new-instance v1, Lcom/google/android/speech/exception/SoundSearchRecognizeException;

    new-instance v2, Lcom/google/android/speech/exception/NoMatchRecognizeException;

    invoke-direct {v2}, Lcom/google/android/speech/exception/NoMatchRecognizeException;-><init>()V

    invoke-direct {v1, v2}, Lcom/google/android/speech/exception/SoundSearchRecognizeException;-><init>(Lcom/google/android/speech/exception/RecognizeException;)V

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onNoSoundSearchMatch(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V

    return-void
.end method

.method private handleGogglesActionHack(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
    .locals 5
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v3}, Lcom/google/android/speech/utils/RecognizedText;->hasCompletedRecognition()Z

    move-result v3

    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v3}, Lcom/google/android/speech/utils/RecognizedText;->getCombinedResult()Ljava/lang/String;

    move-result-object v3

    const-string v4, "scan a barcode"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->clearPeanut()Lcom/google/majel/proto/MajelProtos$MajelResponse;

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$GogglesAction;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$GogglesAction;-><init>()V

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;-><init>()V

    invoke-virtual {v3, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setGogglesActionExtension(Lcom/google/majel/proto/ActionV2Protos$GogglesAction;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    new-instance v3, Lcom/google/majel/proto/PeanutProtos$Peanut;

    invoke-direct {v3}, Lcom/google/majel/proto/PeanutProtos$Peanut;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/majel/proto/PeanutProtos$Peanut;->addActionV2(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->addPeanut(Lcom/google/majel/proto/PeanutProtos$Peanut;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    :cond_0
    return-void
.end method

.method private hasCompletedRecognition()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mSoundSearchResponseReceived:Z

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v0}, Lcom/google/android/speech/utils/RecognizedText;->hasCompletedRecognition()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public onBeginningOfSpeech()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showRecording()V

    return-void
.end method

.method public onDone()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->hasCompletedRecognition()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_3

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->dispatchNoMatchException()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$700(Lcom/google/android/voicesearch/fragments/UberRecognizerController;I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playNoInputSound()V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onDone()V

    const-string v0, "VOICE_SEARCH_COMPLETE"

    invoke-static {v0}, Lcom/google/android/speech/test/TestPlatformLog;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancelInternal(Z)V
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$500(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Z)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showNotListening()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->dispatchNoSoundSearchMatchException()V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mMajelResponseReceived:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onNoMajelResult()V

    goto :goto_1
.end method

.method public onEndOfSpeech()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showRecognizing()V

    return-void
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeLogException(Lcom/google/android/speech/exception/RecognizeException;)V
    invoke-static {p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$600(Lcom/google/android/speech/exception/RecognizeException;)V

    invoke-virtual {p1}, Lcom/google/android/speech/exception/RecognizeException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/speech/test/TestPlatformLog;->logError(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v2, 0x4

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z
    invoke-static {v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$700(Lcom/google/android/voicesearch/fragments/UberRecognizerController;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->hasCompletedRecognition()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playErrorSound()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/prefetch/S3ResultPage;->reportError(Lcom/google/android/speech/exception/RecognizeException;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v1

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v1

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v1, v2, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1000(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->invalidate()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v1}, Lcom/google/android/speech/utils/RecognizedText;->getStableForErrorReporting()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UberRecognizerController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got error after recognizing ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onError(Ljava/lang/String;Lcom/google/android/speech/exception/RecognizeException;)V

    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancelInternal(Z)V
    invoke-static {v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$500(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Z)V

    return-void

    :cond_4
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v1

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showNotListening()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v1

    new-instance v2, Lcom/google/android/speech/exception/SoundSearchRecognizeException;

    invoke-direct {v2, p1}, Lcom/google/android/speech/exception/SoundSearchRecognizeException;-><init>(Lcom/google/android/speech/exception/RecognizeException;)V

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onSoundSearchError(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V

    goto :goto_0
.end method

.method public onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v1

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v1

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->TEXT_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v1, v2, :cond_1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mMajelResponseReceived:Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v1}, Lcom/google/android/speech/utils/RecognizedText;->hasCompletedRecognition()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v1}, Lcom/google/android/speech/utils/RecognizedText;->getCombinedResult()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->handleGogglesActionHack(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1100(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/utils/NetworkInformation;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "UberRecognizerController"

    const-string v1, "Dropping Majel Service Event for single request arch."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    new-instance v1, Lcom/google/android/speech/exception/NetworkRecognizeException;

    const-string v2, "Not connected"

    invoke-direct {v1, v2}, Lcom/google/android/speech/exception/NetworkRecognizeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/prefetch/S3ResultPage;->reportError(Lcom/google/android/speech/exception/RecognizeException;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V

    goto :goto_1

    :cond_5
    const-string v0, "UberRecognizerController"

    const-string v1, "No recognition result from server."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->dispatchNoMatchException()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$700(Lcom/google/android/voicesearch/fragments/UberRecognizerController;I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playNoInputSound()V

    goto :goto_1
.end method

.method public onMediaDataResult([B)V
    .locals 1
    .param p1    # [B

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mTtsDisabled:Lcom/google/common/base/Supplier;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1200(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/common/base/Supplier;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->setAudio([B)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onTtsAvailable()V

    :cond_0
    return-void
.end method

.method public onMusicDetected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showSoundSearchPromotedQuery()V

    return-void
.end method

.method public onNoSpeechDetected()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showNotListening()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancelInternal(Z)V
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$500(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onNoSpeechDetected()V

    const-string v0, "VOICE_SEARCH_COMPLETE"

    invoke-static {v0}, Lcom/google/android/speech/test/TestPlatformLog;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V
    .locals 1
    .param p1    # Lcom/google/speech/s3/PinholeStream$PinholeOutput;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/prefetch/S3ResultPage;->offerPinholeResult(Lcom/google/speech/s3/PinholeStream$PinholeOutput;)V

    :cond_0
    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    const-string v0, "SPEAK_NOW"

    invoke-static {v0}, Lcom/google/android/speech/test/TestPlatformLog;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showListening()V

    return-void
.end method

.method public onRecognitionCancelled()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$700(Lcom/google/android/voicesearch/fragments/UberRecognizerController;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->hasCompletedRecognition()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playNoInputSound()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    invoke-virtual {v0}, Lcom/google/android/velvet/prefetch/S3ResultPage;->cancel()V

    :cond_1
    return-void
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 11
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-static {p1}, Lcom/google/android/speech/test/TestPlatformLog;->logResults(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v0}, Lcom/google/android/speech/utils/RecognizedText;->hasCompletedRecognition()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "UberRecognizerController"

    const-string v1, "Result after completed recognition."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/utils/RecognizedText;->update(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Landroid/util/Pair;

    move-result-object v9

    iget-object v8, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    iget-object v10, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v0

    invoke-interface {v0, v8, v10}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->updateRecognizedText(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v0, p1}, Lcom/google/android/speech/utils/RecognizedText;->update(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Landroid/util/Pair;

    move-result-object v0

    iget-object v7, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mRecognizedText:Lcom/google/android/speech/utils/RecognizedText;

    invoke-virtual {v0}, Lcom/google/android/speech/utils/RecognizedText;->getCombinedResultAlternates()Ljava/util/List;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$700(Lcom/google/android/voicesearch/fragments/UberRecognizerController;I)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playNoInputSound()V

    :cond_3
    :goto_1
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "UberRecognizerController"

    const-string v1, "Empty combined result"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->dispatchNoMatchException()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playRecognitionDoneSound()V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->setFinalRecognizedText(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/searchcommon/SearchSettings;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/searchcommon/SearchSettings;->isSingleRequestArchitectureEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/velvet/prefetch/S3ResultPage;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mClock:Lcom/google/android/searchcommon/util/Clock;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1500(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/searchcommon/util/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/searchcommon/util/Clock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;
    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1600(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/voicesearch/VoiceSearchServices;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;
    invoke-static {v4}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1600(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/voicesearch/VoiceSearchServices;->getMainThreadExecutor()Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;
    invoke-static {v5}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1700(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/searchcommon/google/SearchUrlHelper;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/prefetch/S3ResultPage;-><init>(JLjava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRequestId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mProxiedResult:Lcom/google/android/velvet/prefetch/S3ResultPage;

    invoke-interface {v0, v7, v6, v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/velvet/prefetch/SearchResultPage;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRequestId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$1800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v7, v6, v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/velvet/prefetch/SearchResultPage;)V

    goto/16 :goto_0
.end method

.method public onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V
    .locals 3
    .param p1    # Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;

    iget-boolean v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mSoundSearchResponseReceived:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getResultCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;->getResultList()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/ears/EarsResultParser;->getFirstEarsResultWithMusic(Ljava/util/List;)Lcom/google/audio/ears/proto/EarsService$EarsResult;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$2000(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener$1;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->mSoundSearchResponseReceived:Z

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;->onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    # getter for: Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showNotListening()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;->this$0:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->stopListening()V

    goto :goto_0
.end method
