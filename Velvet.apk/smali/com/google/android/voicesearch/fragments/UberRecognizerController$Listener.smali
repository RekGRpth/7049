.class public interface abstract Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
.super Ljava/lang/Object;
.source "UberRecognizerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/UberRecognizerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onDone()V
.end method

.method public abstract onError(Ljava/lang/String;Lcom/google/android/speech/exception/RecognizeException;)V
.end method

.method public abstract onMajelResult(Lcom/google/majel/proto/MajelProtos$MajelResponse;)V
.end method

.method public abstract onNoMajelResult()V
.end method

.method public abstract onNoMatch(Lcom/google/android/speech/exception/NoMatchRecognizeException;)V
.end method

.method public abstract onNoSoundSearchMatch(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V
.end method

.method public abstract onNoSpeechDetected()V
.end method

.method public abstract onRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/velvet/prefetch/SearchResultPage;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/velvet/prefetch/SearchResultPage;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onSoundSearchError(Lcom/google/android/speech/exception/SoundSearchRecognizeException;)V
.end method

.method public abstract onSoundSearchResult(Lcom/google/audio/ears/proto/EarsService$EarsResultsResponse;)V
.end method

.method public abstract onTtsAvailable()V
.end method
