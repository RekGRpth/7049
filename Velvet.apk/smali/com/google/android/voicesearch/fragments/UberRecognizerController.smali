.class public Lcom/google/android/voicesearch/fragments/UberRecognizerController;
.super Ljava/lang/Object;
.source "UberRecognizerController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;,
        Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;,
        Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;,
        Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;
    }
.end annotation


# static fields
.field static final NO_OP_LISTENER:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

.field private static final NO_OP_UI:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;


# instance fields
.field private mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mClock:Lcom/google/android/searchcommon/util/Clock;

.field private mEarsProviderHelper:Lcom/google/android/ears/EarsContentProviderHelper;

.field private mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

.field private mFlags:I

.field private mGrammarCompilationCallback:Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;

.field private final mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

.field private mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

.field private mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

.field private mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

.field private mRecognitionInProgress:Z

.field private mRecognizer:Lcom/google/android/speech/Recognizer;

.field private mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

.field private mRequestId:Ljava/lang/String;

.field private final mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

.field private mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

.field private mSpeakPrompt:Ljava/lang/String;

.field private final mSpokenBcp47LocaleSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStream:I

.field private final mTtsDisabled:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

.field private mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

.field private mUiThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$2;

    invoke-direct {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$2;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->NO_OP_LISTENER:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    new-instance v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$3;

    invoke-direct {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$3;-><init>()V

    sput-object v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->NO_OP_UI:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    return-void
.end method

.method constructor <init>(Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V
    .locals 1
    .param p3    # Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    .param p4    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p5    # Lcom/google/android/searchcommon/SearchSettings;
    .param p6    # Lcom/google/android/searchcommon/util/Clock;
    .param p7    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;",
            "Lcom/google/android/voicesearch/VoiceSearchServices;",
            "Lcom/google/android/searchcommon/SearchSettings;",
            "Lcom/google/android/searchcommon/util/Clock;",
            "Lcom/google/android/searchcommon/google/SearchUrlHelper;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mStream:I

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpokenBcp47LocaleSupplier:Lcom/google/common/base/Supplier;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mTtsDisabled:Lcom/google/common/base/Supplier;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    iput-object p6, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mClock:Lcom/google/android/searchcommon/util/Clock;

    iput-object p7, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    sget-object v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->NO_OP_UI:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/speech/params/RecognizerParams;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->reallyStartListening(Lcom/google/android/speech/params/RecognizerParams;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/utils/NetworkInformation;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/common/base/Supplier;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mTtsDisabled:Lcom/google/common/base/Supplier;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/TtsAudioPlayer;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/searchcommon/SearchSettings;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSearchSettings:Lcom/google/android/searchcommon/SearchSettings;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/searchcommon/util/Clock;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mClock:Lcom/google/android/searchcommon/util/Clock;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/VoiceSearchServices;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRequestId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/ears/EarsContentProviderHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEarsProviderHelper:Lcom/google/android/ears/EarsContentProviderHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/speech/params/RecognizerParams$Mode;Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p1    # Lcom/google/android/speech/params/RecognizerParams$Mode;
    .param p2    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startEmbeddedRecognitionInternal(Lcom/google/android/speech/params/RecognizerParams$Mode;Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mListener:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Z)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancelInternal(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 0
    .param p0    # Lcom/google/android/speech/exception/RecognizeException;

    invoke-static {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeLogException(Lcom/google/android/speech/exception/RecognizeException;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/fragments/UberRecognizerController;I)Z
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/voicesearch/fragments/UberRecognizerController;)Lcom/google/android/speech/params/RecognizerParams$Mode;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    return-object v0
.end method

.method private cancelInternal(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognitionInProgress:Z

    if-eqz v0, :cond_2

    const-string v0, "no_match"

    invoke-static {v0}, Lcom/google/android/speech/test/TestPlatformLog;->logError(Ljava/lang/String;)V

    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    invoke-virtual {v0}, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->onRecognitionCancelled()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizer:Lcom/google/android/speech/Recognizer;

    invoke-interface {v0}, Lcom/google/android/speech/Recognizer;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showNotListening()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognitionInProgress:Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    invoke-virtual {v0}, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;->invalidate()V

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/TtsAudioPlayer;->stopAudioPlayback()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mGrammarCompilationCallback:Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mGrammarCompilationCallback:Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/speech/embedded/OfflineActionsManager;->detach(Lcom/google/android/speech/callback/SimpleCallback;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mGrammarCompilationCallback:Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;

    :cond_5
    return-void
.end method

.method public static create(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/common/base/Supplier;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .locals 8
    .param p0    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/VoiceSearchServices;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/voicesearch/fragments/UberRecognizerController;"
        }
    .end annotation

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/google/common/base/Suppliers;->ofInstance(Ljava/lang/Object;)Lcom/google/common/base/Supplier;

    move-result-object v2

    sget-object v3, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->NO_OP_LISTENER:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;

    move-object v1, p1

    move-object v4, p0

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;-><init>(Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    return-object v0
.end method

.method public static createForGoogleNow(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .locals 8
    .param p0    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;
    .param p4    # Lcom/google/android/searchcommon/SearchSettings;
    .param p5    # Lcom/google/android/searchcommon/util/Clock;
    .param p6    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/VoiceSearchServices;",
            "Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/searchcommon/SearchSettings;",
            "Lcom/google/android/searchcommon/util/Clock;",
            "Lcom/google/android/searchcommon/google/SearchUrlHelper;",
            ")",
            "Lcom/google/android/voicesearch/fragments/UberRecognizerController;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p1

    move-object v4, p0

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;-><init>(Lcom/google/common/base/Supplier;Lcom/google/common/base/Supplier;Lcom/google/android/voicesearch/fragments/UberRecognizerController$Listener;Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/android/searchcommon/SearchSettings;Lcom/google/android/searchcommon/util/Clock;Lcom/google/android/searchcommon/google/SearchUrlHelper;)V

    return-object v0
.end method

.method private getGrammarType(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/embedded/Greco3Grammar;
    .locals 1
    .param p1    # Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Grammar;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/embedded/Greco3Grammar;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/speech/embedded/Greco3Grammar;->CONTACT_DIALING:Lcom/google/android/speech/embedded/Greco3Grammar;

    goto :goto_0
.end method

.method private getGreco3Mode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/embedded/Greco3Mode;
    .locals 1
    .param p1    # Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_CONTACTS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne p1, v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->GRAMMAR:Lcom/google/android/speech/embedded/Greco3Mode;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->INTENT_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->DICTATION:Lcom/google/android/speech/embedded/Greco3Mode;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/speech/embedded/Greco3Mode;->ENDPOINTER_VOICESEARCH:Lcom/google/android/speech/embedded/Greco3Mode;

    goto :goto_0
.end method

.method private getRecognizerParamsBuilder(Lcom/google/android/speech/params/RecognizerParams$Mode;Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;
    .locals 4
    .param p1    # Lcom/google/android/speech/params/RecognizerParams$Mode;
    .param p2    # Z

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->newBuilder()Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpokenBcp47LocaleSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSpokenBcp47Locale(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->getGrammarType(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/embedded/Greco3Grammar;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setGrammarType(Lcom/google/android/speech/embedded/Greco3Grammar;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->getGreco3Mode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/embedded/Greco3Mode;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setGreco3Mode(Lcom/google/android/speech/embedded/Greco3Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setRecordedAudio(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setMode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setNoSpeechDetected(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    :cond_0
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v3}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setPlayBeep(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    :cond_1
    return-object v0
.end method

.method private isFlagSet(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mFlags:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeInit()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizer:Lcom/google/android/speech/Recognizer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizer:Lcom/google/android/speech/Recognizer;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getMainThreadExecutor()Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getExecutorService()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizerParamsFactory()Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSoundManager()Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getOfflineActionsManager()Lcom/google/android/speech/embedded/OfflineActionsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getTtsAudioPlayer()Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mTtsPlayer:Lcom/google/android/voicesearch/audio/TtsAudioPlayer;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getNetworkInformation()Lcom/google/android/speech/utils/NetworkInformation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getEarsProviderHelper()Lcom/google/android/ears/EarsContentProviderHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEarsProviderHelper:Lcom/google/android/ears/EarsContentProviderHelper;

    :cond_0
    return-void
.end method

.method private static maybeLogException(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 2
    .param p0    # Lcom/google/android/speech/exception/RecognizeException;

    instance-of v0, p0, Lcom/google/android/speech/embedded/Greco3RecognitionEngine$EmbeddedRecognizerUnavailableException;

    if-eqz v0, :cond_0

    const-string v0, "UberRecognizerController"

    const-string v1, "No recognizers available."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "UberRecognizerController"

    const-string v1, "onError"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private prepareRecognition(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 5
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;
    .param p2    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancelInternal(Z)V

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getMode()Lcom/google/android/speech/params/RecognizerParams$Mode;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {p1}, Lcom/google/android/speech/params/RecognizerParams;->getRequestId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRequestId:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognitionInProgress:Z

    if-eqz p2, :cond_0

    new-instance v1, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;

    invoke-direct {v1}, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;-><init>()V

    new-instance v2, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;

    invoke-direct {v2, p0, v4}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/voicesearch/fragments/UberRecognizerController$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->add(Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    invoke-virtual {v1, p2}, Lcom/google/android/speech/listeners/CompositeRecognitionEventListener;->add(Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    move-object v0, v1

    :goto_0
    new-instance v2, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    invoke-direct {v2, v0}, Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;-><init>(Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;

    invoke-direct {v0, p0, v4}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$InternalRecognitionEventListener;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/voicesearch/fragments/UberRecognizerController$1;)V

    goto :goto_0
.end method

.method private reallyStartListening(Lcom/google/android/speech/params/RecognizerParams;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizer:Lcom/google/android/speech/Recognizer;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/speech/Recognizer;->startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method private startEmbeddedRecognitionInternal(Lcom/google/android/speech/params/RecognizerParams$Mode;Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/params/RecognizerParams$Mode;
    .param p2    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->getRecognizerParamsBuilder(Lcom/google/android/speech/params/RecognizerParams$Mode;Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Z)V

    return-void
.end method

.method private startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Z)V
    .locals 5
    .param p1    # Lcom/google/android/speech/params/RecognizerParams;
    .param p2    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p3    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->prepareRecognition(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->isFlagSet(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getLocalTtsManager()Lcom/google/android/voicesearch/util/LocalTtsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpeakPrompt:Ljava/lang/String;

    const-string v2, "voice_start_up"

    iget v3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mStream:I

    new-instance v4, Lcom/google/android/voicesearch/fragments/UberRecognizerController$1;

    invoke-direct {v4, p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$1;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/speech/params/RecognizerParams;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;)V

    :goto_0
    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showInitializingMic()V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->reallyStartListening(Lcom/google/android/speech/params/RecognizerParams;)V

    goto :goto_0
.end method

.method private startOfflineRecognition(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams$Mode;)V
    .locals 6
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Lcom/google/android/speech/params/RecognizerParams$Mode;

    new-instance v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams$Mode;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mGrammarCompilationCallback:Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mGrammarCompilationCallback:Lcom/google/android/voicesearch/fragments/UberRecognizerController$GrammarCompilationCallback;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpokenBcp47LocaleSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/speech/embedded/Greco3Grammar;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/speech/embedded/Greco3Grammar;->CONTACT_DIALING:Lcom/google/android/speech/embedded/Greco3Grammar;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/speech/embedded/Greco3Grammar;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/embedded/Greco3Grammar;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/speech/embedded/OfflineActionsManager;->startOfflineDataCheck(Lcom/google/android/speech/callback/SimpleCallback;Ljava/lang/String;[Lcom/google/android/speech/embedded/Greco3Grammar;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showInitializing()V

    return-void
.end method

.method private startRecognition(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams$Mode;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Lcom/google/android/speech/params/RecognizerParams$Mode;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeInit()V

    iput v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mFlags:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpeakPrompt:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mNetworkInformation:Lcom/google/android/speech/utils/NetworkInformation;

    invoke-virtual {v0}, Lcom/google/android/speech/utils/NetworkInformation;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startOfflineRecognition(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams$Mode;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p2, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->getRecognizerParamsBuilder(Lcom/google/android/speech/params/RecognizerParams$Mode;Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Z)V

    goto :goto_0
.end method

.method private startRecordedAudioRecognition(Lcom/google/android/speech/params/RecognizerParams$Mode;[BLcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/params/RecognizerParams$Mode;
    .param p2    # [B
    .param p3    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeInit()V

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->getRecognizerParamsBuilder(Lcom/google/android/speech/params/RecognizerParams$Mode;Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->prepareRecognition(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showInitializing()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showRecognizing()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizer:Lcom/google/android/speech/Recognizer;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mEventListener:Lcom/google/android/speech/listeners/CancellableRecognitionEventListener;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0, p2, v2, v3}, Lcom/google/android/speech/Recognizer;->startRecordedAudioRecognition(Lcom/google/android/speech/params/RecognizerParams;[BLcom/google/android/speech/listeners/RecognitionEventListener;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public attachUi(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;)V
    .locals 4
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    sget-object v2, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->NO_OP_UI:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSpeechLevelSource()Lcom/google/android/speech/SpeechLevelSource;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->isDefaultSpokenLanguage()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->setLanguage(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpokenBcp47LocaleSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v1}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->setLanguage(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancelInternal(Z)V

    return-void
.end method

.method public detachUi(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    sget-object v1, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->NO_OP_UI:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    sget-object v0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->NO_OP_UI:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause(Z)V
    .locals 6
    .param p1    # Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mMode:Lcom/google/android/speech/params/RecognizerParams$Mode;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showNotListening()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->cancel()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mOfflineActionsManager:Lcom/google/android/speech/embedded/OfflineActionsManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpokenBcp47LocaleSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/speech/embedded/Greco3Grammar;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/speech/embedded/Greco3Grammar;->CONTACT_DIALING:Lcom/google/android/speech/embedded/Greco3Grammar;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/speech/embedded/Greco3Grammar;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/embedded/Greco3Grammar;

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/speech/embedded/OfflineActionsManager;->maybeScheduleGrammarCompilation(Ljava/lang/String;Ljava/util/concurrent/Executor;[Lcom/google/android/speech/embedded/Greco3Grammar;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-interface {v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;->showTapToSpeak()V

    goto :goto_0
.end method

.method public resendIntentApi(Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeInit()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mFlags:I

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->INTENT_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAudioStore()Lcom/google/android/speech/audio/AudioStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore;->getLastAudio()[B

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startRecordedAudioRecognition(Lcom/google/android/speech/params/RecognizerParams$Mode;[BLcom/google/android/speech/listeners/RecognitionEventListener;)V

    return-void
.end method

.method public resendVoiceSearch()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeInit()V

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mVoiceSearchServices:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAudioStore()Lcom/google/android/speech/audio/AudioStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/audio/AudioStore;->getLastAudio()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startRecordedAudioRecognition(Lcom/google/android/speech/params/RecognizerParams$Mode;[BLcom/google/android/speech/listeners/RecognitionEventListener;)V

    return-void
.end method

.method public startCommandRecognitionNoUi(Lcom/google/android/speech/listeners/RecognitionEventListener;ILjava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeInit()V

    iput p2, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mFlags:I

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpeakPrompt:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mUi:Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->detachUi(Lcom/google/android/voicesearch/fragments/UberRecognizerController$Ui;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mStream:I

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_COMMANDS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startOfflineRecognition(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams$Mode;)V

    return-void
.end method

.method public startHandsFreeContactRecognition(Lcom/google/android/speech/listeners/RecognitionEventListener;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/listeners/RecognitionEventListener;

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->HANDS_FREE_CONTACTS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startRecognition(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams$Mode;)V

    return-void
.end method

.method public startIntentApi(Lcom/google/android/speech/listeners/RecognitionEventListener;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/listeners/RecognitionEventListener;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeInit()V

    iput v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mFlags:I

    sget-object v0, Lcom/google/android/speech/params/RecognizerParams$Mode;->INTENT_API:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->getRecognizerParamsBuilder(Lcom/google/android/speech/params/RecognizerParams$Mode;Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setTriggerApplicationId(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Z)V

    return-void
.end method

.method public startSoundSearch(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->maybeInit()V

    const/4 v1, 0x4

    iput v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mFlags:I

    iput-object v4, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mSpeakPrompt:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizerParamsFactory:Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;->newBuilder()Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    const-string v2, "en-US"

    invoke-virtual {v1, v2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSpokenBcp47Locale(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/speech/params/RecognizerParams$Mode;->SOUND_SEARCH:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-virtual {v1, v2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setMode(Lcom/google/android/speech/params/RecognizerParams$Mode;)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    const/16 v2, 0x2b11

    invoke-virtual {v1, v2}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSamplingRate(I)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setSoundSearchTtsEnabled(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->setPlayBeep(Z)Lcom/google/android/speech/params/RecognizerParamsBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/params/RecognizerParamsBuilder;->build()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    invoke-direct {p0, v0, v4, v3}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startListening(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/speech/listeners/RecognitionEventListener;Z)V

    iput v3, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mFlags:I

    return-void
.end method

.method public startVoiceSearch()V
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/speech/params/RecognizerParams$Mode;->VOICE_ACTIONS:Lcom/google/android/speech/params/RecognizerParams$Mode;

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startRecognition(Lcom/google/android/speech/listeners/RecognitionEventListener;Lcom/google/android/speech/params/RecognizerParams$Mode;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognitionInProgress:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->mRecognizer:Lcom/google/android/speech/Recognizer;

    invoke-interface {v0}, Lcom/google/android/speech/Recognizer;->stopListening()V

    :cond_0
    return-void
.end method
