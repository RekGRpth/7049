.class Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$3;
.super Ljava/lang/Object;
.source "EditReminderActivity.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$3;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 1
    .param p1    # Landroid/widget/DatePicker;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$3;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$000(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->clearSymbolicDay()V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$3;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$000(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDate(III)V

    return-void
.end method
