.class Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$4;
.super Ljava/lang/Object;
.source "EditReminderActivity.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$4;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 2
    .param p1    # Landroid/widget/TimePicker;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$4;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$000(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$4;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$000(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setTime(II)V

    return-void
.end method
