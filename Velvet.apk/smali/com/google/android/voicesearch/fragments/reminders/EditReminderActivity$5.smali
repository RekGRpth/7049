.class Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$5;
.super Ljava/lang/Object;
.source "EditReminderActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$5;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$5;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mPresenter:Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$000(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity$5;->this$0:Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;

    # getter for: Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->mTimeTriggerView:Lcom/google/android/velvet/ui/CheckableImageView;
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;->access$100(Lcom/google/android/voicesearch/fragments/reminders/EditReminderActivity;)Lcom/google/android/velvet/ui/CheckableImageView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setTriggerType(I)V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
