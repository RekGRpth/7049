.class public Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;
.super Ljava/lang/Object;
.source "EditReminderPresenter.java"


# instance fields
.field private mConfirmationUrl:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field mDateTimeMs:J

.field private mLabel:Ljava/lang/String;

.field private mLocationAlias:I

.field private mLocationTriggerType:I

.field private final mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

.field private mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

.field mTriggerType:I

.field private final mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;Landroid/content/Context;Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    return-void
.end method

.method public static getTriggerType(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)I
    .locals 1
    .param p0    # Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasAbsoluteTimeTrigger()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->hasLocationTrigger()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDate(Ljava/util/Calendar;)V
    .locals 3
    .param p1    # Ljava/util/Calendar;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDate(III)V

    return-void
.end method

.method private setDefaultDateTime()V
    .locals 4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v0, v2, 0x4

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->MORNING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    iget v2, v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->defaultHour:I

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->MORNING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDateToday()V

    :goto_1
    return-void

    :cond_0
    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->AFTERNOON:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    iget v2, v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->defaultHour:I

    if-ge v0, v2, :cond_1

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->AFTERNOON:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->EVENING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    iget v2, v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->defaultHour:I

    if-ge v0, v2, :cond_2

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->EVENING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->NIGHT:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    iget v2, v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->defaultHour:I

    if-ge v0, v2, :cond_3

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->NIGHT:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->MORNING:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDateTomorrow()V

    goto :goto_1
.end method

.method private setDefaultLocationTrigger()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLocationTriggerType(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLocationAlias(I)V

    return-void
.end method

.method private setTime(Ljava/util/Calendar;)V
    .locals 2
    .param p1    # Ljava/util/Calendar;

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0xc

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setTime(II)V

    return-void
.end method


# virtual methods
.method public clearSymbolicDay()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    sget-object v1, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->WEEKEND:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->TIME_UNSPECIFIED:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    :cond_0
    return-void
.end method

.method public createActionV2()Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .locals 6

    new-instance v1, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    invoke-direct {v1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setLabel(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mConfirmationUrl:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setConfirmationUrlPath(Ljava/lang/String;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    iget v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mTriggerType:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    new-instance v0, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    invoke-direct {v0}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;-><init>()V

    iget-wide v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    invoke-virtual {v0, v4, v5}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;->setTimeMs(J)Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    iget v4, v4, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->actionV2Symbol:I

    invoke-virtual {v0, v4}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;->setSymbolicTime(I)Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setAbsoluteTimeTrigger(Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    iget v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mTriggerType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    new-instance v3, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    invoke-direct {v3}, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;->setType(I)Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    new-instance v4, Lcom/google/majel/proto/ActionV2Protos$Location;

    invoke-direct {v4}, Lcom/google/majel/proto/ActionV2Protos$Location;-><init>()V

    iget v5, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mLocationAlias:I

    invoke-virtual {v4, v5}, Lcom/google/majel/proto/ActionV2Protos$Location;->setAlias(I)Lcom/google/majel/proto/ActionV2Protos$Location;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;->setLocation(Lcom/google/majel/proto/ActionV2Protos$Location;)Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    invoke-virtual {v1, v3}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->setLocationTrigger(Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;)Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    goto :goto_0
.end method

.method getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "label"

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mLabel:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "triggerType"

    iget v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mTriggerType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "locationTriggerType"

    iget v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mLocationTriggerType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "location"

    iget v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mLocationAlias:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "timeMs"

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-eqz v0, :cond_0

    const-string v0, "symbolicTime"

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "confirmationUrl"

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mConfirmationUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-string v0, "label"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLabel(Ljava/lang/String;)V

    const-string v0, "triggerType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setTriggerType(I)V

    const-string v0, "locationTriggerType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLocationTriggerType(I)V

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLocationAlias(I)V

    const-string v0, "timeMs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    const-string v0, "symbolicTime"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->values()[Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    move-result-object v0

    const-string v3, "symbolicTime"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    aget-object v0, v0, v3

    :goto_0
    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDateTime(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    const-string v0, "confirmationUrl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mConfirmationUrl:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public saveReminder(Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mReminderSaver:Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->createActionV2()Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->saveReminder(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;Lcom/google/android/speech/callback/SimpleCallback;)V

    return-void
.end method

.method public setCustomDate()V
    .locals 5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->pickDate(III)V

    return-void
.end method

.method public setCustomTime()V
    .locals 4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->pickTime(II)V

    return-void
.end method

.method public setDate(III)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mContext:Landroid/content/Context;

    iget-wide v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    const/16 v5, 0xa12

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->setCustomDate(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->WEEKEND:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-eq v1, v2, :cond_0

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    invoke-static {v1, v2}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isToday(J)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->showDateToday()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    invoke-static {v1, v2}, Lcom/google/android/voicesearch/util/CalendarTextHelper;->isTomorrow(J)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->showDateTomorrow()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->showCustomDate()V

    goto :goto_0
.end method

.method setDateTime(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V
    .locals 1
    .param p1    # J
    .param p3    # Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {p0, p3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    if-nez p3, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setTime(Ljava/util/Calendar;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDate(Ljava/util/Calendar;)V

    return-void
.end method

.method public setDateToday()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->clearSymbolicDay()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDate(Ljava/util/Calendar;)V

    return-void
.end method

.method public setDateTomorrow()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->clearSymbolicDay()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x6

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDate(Ljava/util/Calendar;)V

    return-void
.end method

.method public setDateWeekend()V
    .locals 3

    sget-object v2, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->WEEKEND:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    rsub-int/lit8 v2, v2, 0xe

    rem-int/lit8 v1, v2, 0x7

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDate(Ljava/util/Calendar;)V

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mLabel:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->setLabel(Ljava/lang/String;)V

    return-void
.end method

.method public setLocationAlias(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mLocationAlias:I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->setLocationAlias(I)V

    return-void
.end method

.method public setLocationTriggerType(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mLocationTriggerType:I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->setLocationTriggerType(I)V

    return-void
.end method

.method public setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    iget v0, v0, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->defaultHour:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setTime(II)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    return-void
.end method

.method public setTime(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mContext:Landroid/content/Context;

    iget-wide v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mDateTimeMs:J

    const/16 v5, 0xa01

    invoke-static {v2, v3, v4, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->setCustomTime(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mSymbolicTime:Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->showCustomTime()V

    :cond_0
    return-void
.end method

.method public setTriggerType(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    iput p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mTriggerType:I

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mUi:Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;->setTriggerType(I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUpFromAction(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;I)V
    .locals 6
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .param p2    # I

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    if-eq p2, v5, :cond_0

    const/4 v3, 0x2

    if-ne p2, v3, :cond_1

    :cond_0
    move v3, v5

    :goto_0
    invoke-static {v3}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLabel(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getConfirmationUrlPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->mConfirmationUrl:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->getTriggerType(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setTriggerType(I)V

    return-void

    :cond_1
    move v3, v4

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDefaultDateTime()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDefaultLocationTrigger()V

    if-nez p2, :cond_2

    move v2, v5

    :goto_2
    goto :goto_1

    :cond_2
    move v2, p2

    goto :goto_2

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getAbsoluteTimeTrigger()Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;->getTimeMs()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;->hasSymbolicTime()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/majel/proto/ActionV2Protos$AbsoluteTimeTrigger;->getSymbolicTime()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;->fromActionV2Symbol(I)Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;

    move-result-object v3

    :goto_3
    invoke-virtual {p0, v4, v5, v3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDateTime(JLcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDefaultLocationTrigger()V

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_3

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getLocationTrigger()Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;

    move-result-object v0

    invoke-virtual {p0, v4}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLocationTriggerType(I)V

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$LocationTrigger;->getLocation()Lcom/google/majel/proto/ActionV2Protos$Location;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$Location;->getAlias()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLocationAlias(I)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDefaultDateTime()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setUpWithDefaults()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setLabel(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setTriggerType(I)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDefaultDateTime()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderPresenter;->setDefaultLocationTrigger()V

    return-void
.end method
