.class public interface abstract Lcom/google/android/voicesearch/fragments/reminders/EditReminderUi;
.super Ljava/lang/Object;
.source "EditReminderUi.java"


# virtual methods
.method public abstract pickDate(III)V
.end method

.method public abstract pickTime(II)V
.end method

.method public abstract setCustomDate(Ljava/lang/String;)V
.end method

.method public abstract setCustomTime(Ljava/lang/String;)V
.end method

.method public abstract setLabel(Ljava/lang/String;)V
.end method

.method public abstract setLocationAlias(I)V
.end method

.method public abstract setLocationTriggerType(I)V
.end method

.method public abstract setSymbolicTime(Lcom/google/android/voicesearch/fragments/reminders/SymbolicTime;)V
.end method

.method public abstract setTriggerType(I)V
.end method

.method public abstract showCustomDate()V
.end method

.method public abstract showCustomTime()V
.end method

.method public abstract showDateToday()V
.end method

.method public abstract showDateTomorrow()V
.end method
