.class public Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;
.super Ljava/lang/Object;
.source "ReminderSaver.java"


# static fields
.field static final QUERY_PARAM_PINFO:Ljava/lang/String; = "pinfo"


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mContext:Landroid/content/Context;

.field private final mCookies:Lcom/google/android/velvet/Cookies;

.field private final mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

.field private final mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/searchcommon/util/HttpHelper;Lcom/google/android/searchcommon/google/SearchUrlHelper;Lcom/google/android/velvet/Cookies;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/searchcommon/util/HttpHelper;
    .param p3    # Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .param p4    # Lcom/google/android/velvet/Cookies;
    .param p5    # Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mContext:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mCookies:Lcom/google/android/velvet/Cookies;

    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)Lcom/google/android/searchcommon/google/SearchUrlHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mSearchUrlHelper:Lcom/google/android/searchcommon/google/SearchUrlHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->getCookieHeader(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)Lcom/google/android/searchcommon/util/HttpHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mHttpHelper:Lcom/google/android/searchcommon/util/HttpHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getCookieHeader(Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mCookies:Lcom/google/android/velvet/Cookies;

    invoke-virtual {v2, p1}, Lcom/google/android/velvet/Cookies;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    const-string v2, "Cookie"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public saveReminder(Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;Lcom/google/android/speech/callback/SimpleCallback;)V
    .locals 3
    .param p1    # Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;",
            "Lcom/google/android/speech/callback/SimpleCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;->getConfirmationUrlPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/speech/callback/SimpleCallback;->onResult(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;-><init>(Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;Lcom/google/majel/proto/ActionV2Protos$AddReminderAction;Lcom/google/android/speech/callback/SimpleCallback;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/voicesearch/fragments/reminders/ReminderSaver$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
