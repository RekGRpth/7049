.class final Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;
.super Ljava/lang/Object;
.source "HybridRecognitionEngine.java"

# interfaces
.implements Lcom/google/android/speech/callback/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine;->callbackWithSource(ILcom/google/android/voicesearch/greco3/ResultsMerger;)Lcom/google/android/speech/callback/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/speech/callback/Callback",
        "<",
        "Lcom/google/android/speech/RecognitionResponse;",
        "Lcom/google/android/speech/exception/RecognizeException;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$merger:Lcom/google/android/voicesearch/greco3/ResultsMerger;

.field final synthetic val$source:I


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/greco3/ResultsMerger;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;->val$merger:Lcom/google/android/voicesearch/greco3/ResultsMerger;

    iput p2, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;->val$source:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;->val$merger:Lcom/google/android/voicesearch/greco3/ResultsMerger;

    iget v1, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;->val$source:I

    invoke-virtual {v0, p1, v1}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->onError(Lcom/google/android/speech/exception/RecognizeException;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic onError(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/speech/exception/RecognizeException;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;->onError(Lcom/google/android/speech/exception/RecognizeException;)V

    return-void
.end method

.method public declared-synchronized onResult(Lcom/google/android/speech/RecognitionResponse;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/RecognitionResponse;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;->val$merger:Lcom/google/android/voicesearch/greco3/ResultsMerger;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/greco3/ResultsMerger;->onResult(Lcom/google/android/speech/RecognitionResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic onResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/speech/RecognitionResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/greco3/HybridRecognitionEngine$2;->onResult(Lcom/google/android/speech/RecognitionResponse;)V

    return-void
.end method
