.class Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;
.super Ljava/lang/Object;
.source "PhoneActionsMerger.java"


# instance fields
.field private mergableAction:Lcom/google/majel/proto/ActionV2Protos$ActionV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getActionV2(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;
    .locals 4
    .param p1    # Lcom/google/speech/s3/S3$S3Response;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->hasMajelServiceEventExtension()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getMajelServiceEventExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/speech/message/S3ResponseProcessor;->processMajelServiceEvent(Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;)Lcom/google/majel/proto/MajelProtos$MajelResponse;

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getMajelServiceEventExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->hasMajel()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/s3/S3$S3Response;->getMajelServiceEventExtension()Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/speech/speech/s3/Majel$MajelServiceEvent;->getMajel()Lcom/google/majel/proto/MajelProtos$MajelResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->getPeanutCount()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->getPeanut(I)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2Count()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->getPeanut(I)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2(I)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v1

    goto :goto_0
.end method

.method private getMergableAction(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;
    .locals 2
    .param p1    # Lcom/google/speech/s3/S3$S3Response;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;->getActionV2(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasPhoneActionExtension()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasCallBusinessActionExtension()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method mergeWithEmbeddedResponses(Lcom/google/speech/s3/S3$S3Response;)V
    .locals 2
    .param p1    # Lcom/google/speech/s3/S3$S3Response;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;->mergableAction:Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;->getMergableAction(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;->mergableAction:Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    invoke-virtual {v0, v1}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->setActionV2Extension(Lcom/google/majel/proto/ActionV2Protos$ActionV2;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    goto :goto_0
.end method

.method process(Lcom/google/speech/s3/S3$S3Response;)V
    .locals 1
    .param p1    # Lcom/google/speech/s3/S3$S3Response;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;->getMergableAction(Lcom/google/speech/s3/S3$S3Response;)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/PhoneActionsMerger;->mergableAction:Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    return-void
.end method
