.class public Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;
.super Landroid/app/IntentService;
.source "DownloadProcessorService.java"


# instance fields
.field private mDataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

.field private mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

.field private mDownloadedLanguagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

.field private mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private buildNotification(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Z)Landroid/app/Notification;
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_0

    const v2, 0x1080082

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v2, 0x7f0d0442

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const v1, 0x7f0d0443

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    :goto_0
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    :cond_0
    const v2, 0x1080078

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v2, 0x7f0d0444

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const v1, 0x7f0d0445

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    goto :goto_0
.end method

.method private clearState()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadedLanguagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    return-void
.end method

.method private deleteDownloadedFile(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "VS.DownloadProcessorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete downloaded file:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private getDestinationDir(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Ljava/io/File;
    .locals 3
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    const-string v1, "g3_models"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private handleCompletedDownload(Landroid/content/Intent;)Z
    .locals 8
    .param p1    # Landroid/content/Intent;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v5, "extra_download_id"

    const-wide/16 v6, -0x1

    invoke-virtual {p1, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-gez v5, :cond_1

    const-string v4, "VS.DownloadProcessorService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received intent without download ID :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    invoke-virtual {v5, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->getDownloadInfo(J)Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    iget v5, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->status:I

    and-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_4

    iget-object v5, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    if-eqz v5, :cond_4

    iget-object v5, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    iput-object v5, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadedLanguagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    iget-object v5, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->fileName:Ljava/lang/String;

    iget-object v6, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0, v5, v6}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->processDownloadedFile(Ljava/lang/String;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/android/speech/embedded/Greco3DataManager;->blockingUpdateResources(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v3, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    iget-object v5, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-virtual {v3, v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->markCompleted(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    :cond_2
    iget-object v3, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->fileName:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->fileName:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->deleteDownloadedFile(Ljava/lang/String;)V

    :cond_3
    move v3, v4

    goto :goto_0

    :cond_4
    iget-object v4, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    iget-object v5, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-virtual {v4, v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->markCompleted(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    :cond_5
    iget-object v4, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->fileName:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->fileName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->deleteDownloadedFile(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    iget-object v4, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    iget-object v5, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-virtual {v4, v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->markCompleted(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    :cond_6
    iget-object v4, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->fileName:Ljava/lang/String;

    if-eqz v4, :cond_7

    iget-object v4, v2, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->fileName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->deleteDownloadedFile(Ljava/lang/String;)V

    :cond_7
    throw v3
.end method

.method private handleFileAtFixedLocationForTesting(Landroid/content/Intent;)Z
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x1

    const-string v4, "language_pack_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "location_abs"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v4}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getEmbeddedRecognitionResourcesList()Ljava/util/List;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/speech/embedded/LanguagePackUtils;->findById(Ljava/lang/String;Ljava/util/List;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->processDownloadedFile(Ljava/lang/String;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v4, v3}, Lcom/google/android/speech/embedded/Greco3DataManager;->blockingUpdateResources(Z)V

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadedLanguagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    invoke-virtual {v4, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;->markCompleted(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private maybeDeleteExistingFiles(Ljava/io/File;)V
    .locals 5
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "VS.DownloadProcessorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to delete file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "VS.DownloadProcessorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to create directory: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private processDownloadedFile(Ljava/lang/String;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->getDestinationDir(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->maybeDeleteExistingFiles(Ljava/io/File;)V

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->processZipArchive(Ljava/lang/String;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, p2, v0}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->writeMetadata(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :catch_0
    move-exception v1

    const-string v3, "VS.DownloadProcessorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error processing download: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private processZipArchive(Ljava/lang/String;Ljava/io/File;)Z
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v10, 0x1

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Ljava/util/zip/ZipFile;

    new-instance v11, Ljava/io/File;

    invoke-direct {v11, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v12, 0x1

    invoke-direct {v4, v11, v12}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/zip/ZipEntry;

    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, p2, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_2
    invoke-virtual {v4, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v6

    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-static {v6, v8}, Lcom/google/common/io/ByteStreams;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v8}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v10

    move-object v3, v4

    :goto_1
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/zip/ZipFile;->close()V

    :cond_0
    throw v10

    :catch_0
    move-exception v5

    :goto_2
    :try_start_5
    const-string v10, "VS.DownloadProcessorService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error processing download: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v10, 0x0

    :try_start_6
    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V

    :cond_1
    :goto_3
    return v10

    :catchall_1
    move-exception v10

    :goto_4
    :try_start_7
    invoke-static {v6}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v7}, Lcom/google/common/io/Closeables;->closeQuietly(Ljava/io/Closeable;)V

    throw v10
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_2
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V

    goto :goto_3

    :catchall_2
    move-exception v10

    goto :goto_1

    :catchall_3
    move-exception v10

    move-object v7, v8

    goto :goto_4

    :catch_1
    move-exception v5

    move-object v7, v8

    goto :goto_2
.end method

.method private showDownloadCompleteNotification(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Z)V
    .locals 3
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;
    .param p2    # Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->buildNotification(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Z)Landroid/app/Notification;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public static start(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->fillIn(Landroid/content/Intent;I)I

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static startForDebuggingWithFixedFile(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;

    const-string v0, "VS.DownloadProcessorService"

    const-string v1, "Rejecting start() call for debug intent."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private writeMetadata(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Ljava/io/File;)V
    .locals 5
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    const-string v2, "metadata"

    invoke-direct {v0, p2, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v2, "VS.DownloadProcessorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Writing to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->toByteArray()[B

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/common/io/Files;->write([BLjava/io/File;)V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getLangugageDownloadHelper()Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadHelper:Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDataManager:Lcom/google/android/speech/embedded/Greco3DataManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->clearState()V

    const-string v1, "language_pack_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->handleFileAtFixedLocationForTesting(Landroid/content/Intent;)Z

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->mDownloadedLanguagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0, v1, v0}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->showDownloadCompleteNotification(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Z)V

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/DownloadProcessorService;->handleCompletedDownload(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method
