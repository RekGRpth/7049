.class public final Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;
.super Ljava/lang/Object;
.source "LanguageDownloadHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DownloadInfo"
.end annotation


# instance fields
.field final downloadId:J

.field final fileName:Ljava/lang/String;

.field final languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

.field final status:I


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;IJ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;
    .param p3    # I
    .param p4    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->fileName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->languagePack:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    iput p3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->status:I

    iput-wide p4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguageDownloadHelper$DownloadInfo;->downloadId:J

    return-void
.end method
