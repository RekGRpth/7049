.class Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;
.super Ljava/lang/Object;
.source "VoiceListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->initViewForUpdateAndDelete(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

.field final synthetic val$delete:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

.field final synthetic val$download:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

.field final synthetic val$textItems:[Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;[Ljava/lang/CharSequence;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    iput-object p2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;->val$download:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    iput-object p3, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;->val$textItems:[Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;->val$delete:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    # getter for: Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mSettings:Lcom/google/android/voicesearch/settings/Settings;
    invoke-static {v1}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->access$200(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;->val$download:Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    invoke-virtual {v2}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;->getBcp47Locale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getDisplayName(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;->val$textItems:[Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3$1;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$3;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
