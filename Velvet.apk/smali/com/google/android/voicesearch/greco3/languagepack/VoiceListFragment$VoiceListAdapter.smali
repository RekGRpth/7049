.class final Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;
.super Landroid/widget/BaseAdapter;
.source "VoiceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VoiceListAdapter"
.end annotation


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    # getter for: Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mLanguageList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->access$600(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    # getter for: Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mLanguageList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->access$600(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-eqz p2, :cond_0

    move-object v0, p2

    :goto_0
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;->this$0:Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;

    # getter for: Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->mLanguageList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->access$600(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;

    # invokes: Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->populateView(Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V
    invoke-static {v2, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;->access$700(Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment;Landroid/view/View;Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$LanguagePack;)V

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/VoiceListFragment$VoiceListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f040066

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
