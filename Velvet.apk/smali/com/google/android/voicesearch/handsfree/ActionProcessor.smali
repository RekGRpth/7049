.class Lcom/google/android/voicesearch/handsfree/ActionProcessor;
.super Ljava/lang/Object;
.source "ActionProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/ActionProcessor$ActionListener;
    }
.end annotation


# instance fields
.field private final mActionListener:Lcom/google/android/voicesearch/handsfree/ActionProcessor$ActionListener;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/handsfree/ActionProcessor$ActionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/ActionProcessor$ActionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/handsfree/ActionProcessor$ActionListener;

    return-void
.end method


# virtual methods
.method public process(Lcom/google/majel/proto/MajelProtos$MajelResponse;)Z
    .locals 7
    .param p1    # Lcom/google/majel/proto/MajelProtos$MajelResponse;

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->getPeanutCount()I

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {p1, v5}, Lcom/google/majel/proto/MajelProtos$MajelResponse;->getPeanut(I)Lcom/google/majel/proto/PeanutProtos$Peanut;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2Count()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2, v5}, Lcom/google/majel/proto/PeanutProtos$Peanut;->getActionV2(I)Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasPhoneActionExtension()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getPhoneActionExtension()Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/majel/proto/ActionV2Protos$PhoneAction;->getContactCount()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasActionV2Extension()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getActionV2Extension()Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->hasPhoneActionExtension()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getActionV2Extension()Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getActionV2Extension()Lcom/google/majel/proto/ActionV2Protos$ActionV2;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/majel/proto/ActionV2Protos$ActionV2;->getPhoneActionExtension()Lcom/google/majel/proto/ActionV2Protos$PhoneAction;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->mergePhoneAction(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)V

    :cond_2
    iget-object v5, p0, Lcom/google/android/voicesearch/handsfree/ActionProcessor;->mActionListener:Lcom/google/android/voicesearch/handsfree/ActionProcessor$ActionListener;

    invoke-interface {v5, v3}, Lcom/google/android/voicesearch/handsfree/ActionProcessor$ActionListener;->onPhoneAction(Lcom/google/majel/proto/ActionV2Protos$PhoneAction;)V

    const/4 v5, 0x1

    goto :goto_0
.end method
