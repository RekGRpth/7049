.class Lcom/google/android/voicesearch/handsfree/ErrorController;
.super Ljava/lang/Object;
.source "ErrorController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/ErrorController$Ui;
    }
.end annotation


# instance fields
.field private final mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

.field private mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

.field private final mResources:Landroid/content/res/Resources;

.field private mUi:Lcom/google/android/voicesearch/handsfree/ErrorController$Ui;

.field private final mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;)V
    .locals 1
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Lcom/google/android/voicesearch/util/LocalTtsManager;
    .param p3    # Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mResources:Landroid/content/res/Resources;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/util/LocalTtsManager;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/handsfree/ErrorController;)Lcom/google/android/voicesearch/handsfree/MainController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/ErrorController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-object v0
.end method


# virtual methods
.method public setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/MainController;

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-void
.end method

.method public start(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->showNoMatch()Lcom/google/android/voicesearch/handsfree/ErrorController$Ui;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mUi:Lcom/google/android/voicesearch/handsfree/ErrorController$Ui;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mUi:Lcom/google/android/voicesearch/handsfree/ErrorController$Ui;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/handsfree/ErrorController$Ui;->setMessage(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mLocalTtsManager:Lcom/google/android/voicesearch/util/LocalTtsManager;

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/ErrorController;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "error-message"

    new-instance v4, Lcom/google/android/voicesearch/handsfree/ErrorController$1;

    invoke-direct {v4, p0}, Lcom/google/android/voicesearch/handsfree/ErrorController$1;-><init>(Lcom/google/android/voicesearch/handsfree/ErrorController;)V

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/google/android/voicesearch/util/LocalTtsManager;->enqueue(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Runnable;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
