.class public Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;
.super Landroid/app/Activity;
.source "HandsFreeActivity.java"


# instance fields
.field private mControllerStarted:Z

.field private mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

.field private mHandler:Landroid/os/Handler;

.field private mHandsFreeMainController:Lcom/google/android/voicesearch/handsfree/MainController;

.field private mShouldFinishOnStop:Z

.field private mSpokenLanguageHelper:Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

.field private final mStartControllerRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity$1;-><init>(Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mStartControllerRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mControllerStarted:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;)Lcom/google/android/voicesearch/handsfree/MainController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mHandsFreeMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-object v0
.end method

.method private createRecognizerController(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/common/base/Supplier;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/VoiceSearchServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/VoiceSearchServices;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/voicesearch/fragments/UberRecognizerController;"
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->create(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/common/base/Supplier;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v0

    return-object v0
.end method

.method private hasDeviceCapabilities()Z
    .locals 3

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mSpokenLanguageHelper:Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->hasResources()Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-interface {v2}, Lcom/google/android/searchcommon/DeviceCapabilityManager;->isTelephoneCapable()Z

    move-result v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 40
    .param p1    # Landroid/os/Bundle;

    const/16 v8, 0x4f

    invoke-static {v8}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getGreco3Container()Lcom/google/android/speech/embedded/Greco3Container;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/speech/embedded/Greco3Container;->getGreco3DataManager()Lcom/google/android/speech/embedded/Greco3DataManager;

    move-result-object v12

    const/4 v8, 0x0

    invoke-virtual {v12, v8}, Lcom/google/android/speech/embedded/Greco3DataManager;->maybeInitialize(Lcom/google/android/speech/callback/SimpleCallback;)Z

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    const/high16 v9, 0x280000

    invoke-virtual {v8, v9}, Landroid/view/Window;->addFlags(I)V

    const v8, 0x7f04004d

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->setContentView(I)V

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v17

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/velvet/VelvetApplication;->getCoreServices()Lcom/google/android/searchcommon/CoreSearchServices;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/CoreSearchServices;->getDeviceCapabilityManager()Lcom/google/android/searchcommon/DeviceCapabilityManager;

    move-result-object v8

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mDeviceCapabilityManager:Lcom/google/android/searchcommon/DeviceCapabilityManager;

    invoke-static/range {p0 .. p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v6

    new-instance v8, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

    move-object/from16 v0, v17

    invoke-direct {v8, v12, v0}, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;-><init>(Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/voicesearch/settings/Settings;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mSpokenLanguageHelper:Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

    new-instance v8, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mSpokenLanguageHelper:Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

    invoke-virtual {v8}, Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;->getSpokenBcp47Locale()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/common/base/Suppliers;->ofInstance(Ljava/lang/Object;)Lcom/google/common/base/Supplier;

    move-result-object v38

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->createRecognizerController(Lcom/google/android/voicesearch/VoiceSearchServices;Lcom/google/common/base/Supplier;)Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    move-result-object v19

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getLocalTtsManager()Lcom/google/android/voicesearch/util/LocalTtsManager;

    move-result-object v10

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSoundManager()Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v5

    new-instance v25, Lcom/google/android/voicesearch/handsfree/ActivityCallback;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/ActivityCallback;-><init>(Landroid/app/Activity;)V

    new-instance v11, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    const v8, 0x7f100111

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v8}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    new-instance v23, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;

    new-instance v8, Lcom/google/android/speech/contacts/ContactLookup;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/speech/contacts/ContactLookup;-><init>(Landroid/content/ContentResolver;)V

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getScheduledExecutorService()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v9

    move-object/from16 v0, v23

    invoke-direct {v0, v8, v9}, Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;-><init>(Lcom/google/android/speech/contacts/ContactLookup;Ljava/util/concurrent/Executor;)V

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getHeadsetAudioRecorderRouter()Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    move-result-object v37

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getOfflineActionsManager()Lcom/google/android/speech/embedded/OfflineActionsManager;

    move-result-object v13

    const-string v8, "audio"

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    new-instance v3, Lcom/google/android/voicesearch/handsfree/AudioRouter;

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getScheduledExecutorService()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getHeadsetAudioRecorderRouter()Lcom/google/android/voicesearch/bluetooth/HeadsetAudioRecorderRouter;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/voicesearch/handsfree/AudioRouter;-><init>(Landroid/media/AudioManager;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/voicesearch/AudioRecorderRouter;)V

    new-instance v7, Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual/range {v39 .. v39}, Lcom/google/android/voicesearch/VoiceSearchServices;->getScheduledExecutorService()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v15

    move-object v8, v3

    move-object v14, v6

    move-object/from16 v16, v5

    invoke-direct/range {v7 .. v17}, Lcom/google/android/voicesearch/handsfree/InitializeController;-><init>(Lcom/google/android/voicesearch/handsfree/AudioRouter;Landroid/content/res/Resources;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;Lcom/google/android/speech/embedded/Greco3DataManager;Lcom/google/android/speech/embedded/OfflineActionsManager;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;Lcom/google/android/voicesearch/settings/Settings;)V

    new-instance v18, Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    move-object/from16 v20, v10

    move-object/from16 v21, v11

    move-object/from16 v22, v17

    move-object/from16 v24, v6

    invoke-direct/range {v18 .. v24}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    new-instance v28, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v8, v11, v5}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Landroid/content/res/Resources;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;)V

    new-instance v29, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v8, v11, v5}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;-><init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Landroid/content/res/Resources;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;)V

    new-instance v30, Lcom/google/android/voicesearch/handsfree/ErrorController;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    move-object/from16 v0, v30

    invoke-direct {v0, v8, v10, v11}, Lcom/google/android/voicesearch/handsfree/ErrorController;-><init>(Landroid/content/res/Resources;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;)V

    new-instance v24, Lcom/google/android/voicesearch/handsfree/MainController;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mSpokenLanguageHelper:Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;

    move-object/from16 v31, v0

    move-object/from16 v26, v7

    move-object/from16 v27, v18

    move-object/from16 v32, v19

    move-object/from16 v33, v10

    move-object/from16 v34, v3

    move-object/from16 v35, v6

    invoke-direct/range {v24 .. v35}, Lcom/google/android/voicesearch/handsfree/MainController;-><init>(Lcom/google/android/voicesearch/handsfree/ActivityCallback;Lcom/google/android/voicesearch/handsfree/InitializeController;Lcom/google/android/voicesearch/handsfree/SpeakNowController;Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;Lcom/google/android/voicesearch/handsfree/ErrorController;Lcom/google/android/voicesearch/handsfree/SpokenLanguageHelper;Lcom/google/android/voicesearch/fragments/UberRecognizerController;Lcom/google/android/voicesearch/util/LocalTtsManager;Lcom/google/android/voicesearch/handsfree/AudioRouter;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mHandsFreeMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mHandsFreeMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v8}, Lcom/google/android/voicesearch/handsfree/MainController;->init()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mHandsFreeMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->destroy()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mControllerStarted:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4e

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mHandsFreeMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->pause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mShouldFinishOnStop:Z

    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->scheduleSendEvents(Landroid/content/Context;)V

    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mStartControllerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->cancelSendEvents(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->hasDeviceCapabilities()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mShouldFinishOnStop:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mStartControllerRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->mShouldFinishOnStop:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->finish()V

    :cond_0
    return-void
.end method
