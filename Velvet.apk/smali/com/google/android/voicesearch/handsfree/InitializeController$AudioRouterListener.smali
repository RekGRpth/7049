.class Lcom/google/android/voicesearch/handsfree/InitializeController$AudioRouterListener;
.super Ljava/lang/Object;
.source "InitializeController.java"

# interfaces
.implements Lcom/google/android/voicesearch/handsfree/AudioRouter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/InitializeController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioRouterListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/InitializeController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/handsfree/InitializeController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/InitializeController$AudioRouterListener;->this$0:Lcom/google/android/voicesearch/handsfree/InitializeController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/handsfree/InitializeController;Lcom/google/android/voicesearch/handsfree/InitializeController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/InitializeController;
    .param p2    # Lcom/google/android/voicesearch/handsfree/InitializeController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/InitializeController$AudioRouterListener;-><init>(Lcom/google/android/voicesearch/handsfree/InitializeController;)V

    return-void
.end method


# virtual methods
.method public onAudioRouteEstablished()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController$AudioRouterListener;->this$0:Lcom/google/android/voicesearch/handsfree/InitializeController;

    # invokes: Lcom/google/android/voicesearch/handsfree/InitializeController;->handleAudioRouteEstablished()V
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->access$600(Lcom/google/android/voicesearch/handsfree/InitializeController;)V

    return-void
.end method

.method public onAudioRouteFailed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/InitializeController$AudioRouterListener;->this$0:Lcom/google/android/voicesearch/handsfree/InitializeController;

    # getter for: Lcom/google/android/voicesearch/handsfree/InitializeController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/InitializeController;->access$700(Lcom/google/android/voicesearch/handsfree/InitializeController;)Lcom/google/android/voicesearch/handsfree/MainController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    return-void
.end method
