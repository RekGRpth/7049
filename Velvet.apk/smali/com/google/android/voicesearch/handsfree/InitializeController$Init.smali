.class final enum Lcom/google/android/voicesearch/handsfree/InitializeController$Init;
.super Ljava/lang/Enum;
.source "InitializeController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/InitializeController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Init"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/voicesearch/handsfree/InitializeController$Init;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

.field public static final enum AUDIO_ROUTE:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

.field public static final enum ERROR:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

.field public static final enum GRAMMAR_COMPILED:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

.field public static final enum GRECO3_DATA:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

.field public static final enum NOTHING:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    const-string v1, "NOTHING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->NOTHING:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    new-instance v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    const-string v1, "AUDIO_ROUTE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->AUDIO_ROUTE:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    new-instance v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    const-string v1, "GRECO3_DATA"

    invoke-direct {v0, v1, v4}, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRECO3_DATA:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    new-instance v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    const-string v1, "GRAMMAR_COMPILED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRAMMAR_COMPILED:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    new-instance v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->ERROR:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->NOTHING:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->AUDIO_ROUTE:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRECO3_DATA:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->GRAMMAR_COMPILED:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->ERROR:Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->$VALUES:[Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/voicesearch/handsfree/InitializeController$Init;
    .locals 1

    const-class v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    return-object v0
.end method

.method public static values()[Lcom/google/android/voicesearch/handsfree/InitializeController$Init;
    .locals 1

    sget-object v0, Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->$VALUES:[Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    invoke-virtual {v0}, [Lcom/google/android/voicesearch/handsfree/InitializeController$Init;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/voicesearch/handsfree/InitializeController$Init;

    return-object v0
.end method
