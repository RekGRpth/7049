.class Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;
.super Ljava/lang/Object;
.source "InterpretationProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;
    }
.end annotation


# instance fields
.field private final mListener:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->mListener:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;

    return-void
.end method

.method private getInterpretationValue(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Ljava/lang/String;
    .locals 7
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    move-object v3, v4

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult()Z

    move-result v5

    if-nez v5, :cond_2

    move-object v3, v4

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisCount()I

    move-result v5

    if-ge v1, v5, :cond_4

    invoke-virtual {v2, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesis(I)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasSemanticResult()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getSemanticResult()Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->getInterpretationValue(Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move-object v3, v4

    goto :goto_0
.end method

.method private getInterpretationValue(Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;->getInterpretationCount()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p1, v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$SemanticResult;->getInterpretation(I)Lspeech/InterpretationProto$Interpretation;

    move-result-object v0

    invoke-virtual {v0}, Lspeech/InterpretationProto$Interpretation;->getSlotCount()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Lspeech/InterpretationProto$Interpretation;->getSlot(I)Lspeech/InterpretationProto$Slot;

    move-result-object v2

    invoke-virtual {v2}, Lspeech/InterpretationProto$Slot;->hasValue()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Lspeech/InterpretationProto$Interpretation;->getSlot(I)Lspeech/InterpretationProto$Slot;

    move-result-object v1

    invoke-virtual {v1}, Lspeech/InterpretationProto$Slot;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getInterpretations(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)[Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->getInterpretationValue(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public handleRecognitionEvent(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Z
    .locals 8
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    const/4 v6, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->getInterpretations(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)[Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    array-length v5, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_3

    aget-object v3, v0, v1

    const-string v7, "_cancel"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->mListener:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;

    invoke-interface {v7}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;->onCancel()V

    :goto_1
    return v6

    :cond_0
    const-string v7, "_okay"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->mListener:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;

    invoke-interface {v7}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;->onConfirm()V

    goto :goto_1

    :cond_1
    const-string v7, "_select"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    :try_start_0
    const-string v7, "_select"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v7, p0, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;->mListener:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;

    invoke-interface {v7, v2}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;->onSelect(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    :catch_0
    move-exception v7

    goto :goto_2
.end method
