.class Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;
.super Ljava/lang/Object;
.source "PhoneCallContactController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$1;,
        Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;,
        Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;,
        Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;
    }
.end annotation


# instance fields
.field private mActionType:I

.field private mContact:Lcom/google/android/speech/contacts/Contact;

.field private final mInterpretationProcessor:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

.field private mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

.field private final mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

.field private final mResources:Landroid/content/res/Resources;

.field private final mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

.field private mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

.field private final mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/UberRecognizerController;Landroid/content/res/Resources;Lcom/google/android/voicesearch/handsfree/ViewDisplayer;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;)V
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/fragments/UberRecognizerController;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # Lcom/google/android/voicesearch/handsfree/ViewDisplayer;
    .param p4    # Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mResources:Landroid/content/res/Resources;

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    new-instance v0, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

    new-instance v1, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$InterpretationProcessorListener;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$1;)V

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;-><init>(Lcom/google/android/voicesearch/handsfree/InterpretationProcessor$Listener;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mInterpretationProcessor:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

    iput-object p3, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    iput-object p4, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mInterpretationProcessor:Lcom/google/android/voicesearch/handsfree/InterpretationProcessor;

    return-object v0
.end method

.method private getTtsPrompt(Lcom/google/android/speech/contacts/Contact;)Ljava/lang/String;
    .locals 6
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d0383

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->spaceOutDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0d0382

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p1, v3}, Lcom/google/android/speech/contacts/Contact;->getLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public callContactByTouch()V
    .locals 3

    const/16 v0, 0xd

    const/high16 v1, 0x1000000

    iget v2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mActionType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getCallIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/handsfree/MainController;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    return-void
.end method

.method public callContactByVoice()V
    .locals 3

    const/16 v0, 0xd

    const/high16 v1, 0x2000000

    iget v2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mActionType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playRecognitionDoneSound()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mContact:Lcom/google/android/speech/contacts/Contact;

    invoke-virtual {v1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/voicesearch/util/PhoneActionUtils;->getCallIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/handsfree/MainController;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    return-void
.end method

.method public cancelByTouch()V
    .locals 3

    const/16 v0, 0xe

    const/high16 v1, 0x1000000

    iget v2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mActionType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mContact:Lcom/google/android/speech/contacts/Contact;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    return-void
.end method

.method public cancelByVoice()V
    .locals 3

    const/16 v0, 0xe

    const/high16 v1, 0x2000000

    iget v2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mActionType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playHandsFreeShutDownSound()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mContact:Lcom/google/android/speech/contacts/Contact;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/MainController;->exit()V

    return-void
.end method

.method public handleVoiceError()V
    .locals 4

    const v3, 0x7f0d0464

    const/16 v0, 0xe

    const/high16 v1, 0x3000000

    iget v2, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mActionType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEventWithSource(IILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/voicesearch/handsfree/MainController;->showError(II)V

    return-void
.end method

.method public setMainController(Lcom/google/android/voicesearch/handsfree/MainController;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/handsfree/MainController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/MainController;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start(Lcom/google/android/speech/contacts/Contact;)V
    .locals 4
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mContact:Lcom/google/android/speech/contacts/Contact;

    const/16 v0, 0xd

    invoke-static {v0, p1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordSpeechEvent(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mRecognizerController:Lcom/google/android/voicesearch/fragments/UberRecognizerController;

    new-instance v1, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$RecognizerListener;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)V

    const/4 v2, 0x3

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->getTtsPrompt(Lcom/google/android/speech/contacts/Contact;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/voicesearch/fragments/UberRecognizerController;->startCommandRecognitionNoUi(Lcom/google/android/speech/listeners/RecognitionEventListener;ILjava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->isNumberOnlyContact()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1c

    iput v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mActionType:I

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->showPhoneCallNumber()Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    invoke-interface {v0, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;->setController(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    invoke-interface {v0, p1}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;->setContact(Lcom/google/android/speech/contacts/Contact;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/handsfree/MainController;->getSpokenLanguageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;->setLanguage(Ljava/lang/String;)V

    return-void

    :cond_0
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mActionType:I

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mViewDisplayer:Lcom/google/android/voicesearch/handsfree/ViewDisplayer;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/ViewDisplayer;->showPhoneCallContact()Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;->mUi:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;

    goto :goto_0
.end method
