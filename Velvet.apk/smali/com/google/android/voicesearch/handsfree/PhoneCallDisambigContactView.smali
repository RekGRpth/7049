.class Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;
.super Landroid/widget/FrameLayout;
.source "PhoneCallDisambigContactView.java"

# interfaces
.implements Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController$Ui;


# instance fields
.field private mController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

.field private mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->initView()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;)Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->mController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    return-object v0
.end method

.method private createContactItem(Landroid/view/LayoutInflater;Lcom/google/android/speech/contacts/Contact;ZLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Lcom/google/android/speech/contacts/Contact;
    .param p3    # Z
    .param p4    # Landroid/view/ViewGroup;

    const v4, 0x7f10006b

    if-eqz p3, :cond_0

    const v1, 0x7f040051

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p1, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    if-eqz p3, :cond_1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/google/android/speech/contacts/Contact;->getLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-object v0

    :cond_0
    const v1, 0x7f040052

    goto :goto_0

    :cond_1
    const v2, 0x7f100061

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/speech/contacts/Contact;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/google/android/speech/contacts/Contact;->getLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private initView()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040050

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f100115

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$1;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->mController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;->createView()V

    return-void
.end method

.method public setContacts(Ljava/util/List;Z)V
    .locals 8
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;Z)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    if-nez p2, :cond_1

    const v6, 0x7f100113

    invoke-virtual {p0, v6}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_0

    const v6, 0x7f100059

    invoke-virtual {p0, v6}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    :cond_0
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    const v6, 0x7f100114

    invoke-virtual {p0, v6}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v2, v6, -0x1

    :goto_0
    if-ltz v2, :cond_2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/speech/contacts/Contact;

    invoke-direct {p0, v3, v0, p2, v4}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->createContactItem(Landroid/view/LayoutInflater;Lcom/google/android/speech/contacts/Contact;ZLandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v6, 0x7f100116

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/voicesearch/handsfree/ui/QuotedTextView;

    add-int/lit8 v7, v2, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/voicesearch/handsfree/ui/QuotedTextView;->setQuotedText(Ljava/lang/CharSequence;)V

    new-instance v6, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$2;

    invoke-direct {v6, p0, v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView$2;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;Lcom/google/android/speech/contacts/Contact;)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setController(Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->mController:Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactController;

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->setLanguage(Ljava/lang/String;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const v0, 0x7f100059

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->showListening()V

    return-void
.end method

.method public showNotListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallDisambigContactView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->showNotListening()V

    return-void
.end method
