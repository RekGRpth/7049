.class Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;
.super Landroid/widget/FrameLayout;
.source "PhoneCallNumberView.java"

# interfaces
.implements Lcom/google/android/voicesearch/handsfree/PhoneCallContactController$Ui;


# instance fields
.field private mController:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

.field private final mPhoneNumberView:Landroid/widget/TextView;

.field private final mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04004f

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f100112

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView$1;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView$1;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f100081

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView$2;

    invoke-direct {v2, p0}, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView$2;-><init>(Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f10004b

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mPhoneNumberView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;)Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mController:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    return-object v0
.end method


# virtual methods
.method public setContact(Lcom/google/android/speech/contacts/Contact;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/contacts/Contact;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/searchcommon/util/ExtraPreconditions;->checkMainThread()V

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mPhoneNumberView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/speech/contacts/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setController(Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;)V
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mController:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mController:Lcom/google/android/voicesearch/handsfree/PhoneCallContactController;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->setLanguage(Ljava/lang/String;)V

    return-void
.end method

.method public showListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->showListening()V

    return-void
.end method

.method public showNotListening()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/PhoneCallNumberView;->mRecognizerViewHelper:Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/handsfree/RecognizerViewHelper;->showNotListening()V

    return-void
.end method
