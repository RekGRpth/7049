.class final Lcom/google/android/voicesearch/handsfree/SpeakNowController$AsyncContactRetrieverListener;
.super Ljava/lang/Object;
.source "SpeakNowController.java"

# interfaces
.implements Lcom/google/android/voicesearch/handsfree/AsyncContactRetriever$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/handsfree/SpeakNowController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AsyncContactRetrieverListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$AsyncContactRetrieverListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/handsfree/SpeakNowController;Lcom/google/android/voicesearch/handsfree/SpeakNowController$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/handsfree/SpeakNowController;
    .param p2    # Lcom/google/android/voicesearch/handsfree/SpeakNowController$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/handsfree/SpeakNowController$AsyncContactRetrieverListener;-><init>(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    return-void
.end method


# virtual methods
.method public onContacts(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/speech/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$AsyncContactRetrieverListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # getter for: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->mMainController:Lcom/google/android/voicesearch/handsfree/MainController;
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$600(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)Lcom/google/android/voicesearch/handsfree/MainController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/handsfree/MainController;->callContacts(Ljava/util/List;)V

    return-void
.end method

.method public onNoContact()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/SpeakNowController$AsyncContactRetrieverListener;->this$0:Lcom/google/android/voicesearch/handsfree/SpeakNowController;

    # invokes: Lcom/google/android/voicesearch/handsfree/SpeakNowController;->handleNoMatch()V
    invoke-static {v0}, Lcom/google/android/voicesearch/handsfree/SpeakNowController;->access$400(Lcom/google/android/voicesearch/handsfree/SpeakNowController;)V

    return-void
.end method
