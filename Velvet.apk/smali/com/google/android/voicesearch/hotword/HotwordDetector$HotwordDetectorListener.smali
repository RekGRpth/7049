.class final Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;
.super Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.source "HotwordDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/hotword/HotwordDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HotwordDetectorListener"
.end annotation


# instance fields
.field private mHotwordFired:Z

.field final synthetic this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/hotword/HotwordDetector;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    invoke-direct {p0}, Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/hotword/HotwordDetector;Lcom/google/android/voicesearch/hotword/HotwordDetector$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/hotword/HotwordDetector;
    .param p2    # Lcom/google/android/voicesearch/hotword/HotwordDetector$1;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;-><init>(Lcom/google/android/voicesearch/hotword/HotwordDetector;)V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$100(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$200(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # invokes: Lcom/google/android/voicesearch/hotword/HotwordDetector;->internalStop()V
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$500(Lcom/google/android/voicesearch/hotword/HotwordDetector;)V

    :cond_0
    return-void
.end method

.method public onMusicDetected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$100(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$200(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$300(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;->onMusicDetected()V

    :cond_0
    return-void
.end method

.method public onReadyForSpeech(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$100(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mStarted:Z
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$200(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$300(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mActive:Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$402(Lcom/google/android/voicesearch/hotword/HotwordDetector;Z)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;
    invoke-static {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$300(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;->onHotwordDetectorStarted()V

    :cond_0
    return-void
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 3
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-object v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mThreadCheck:Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;
    invoke-static {v1}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$100(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;->check()Lcom/google/android/searchcommon/util/ExtraPreconditions$ThreadCheck;

    iget-object v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->this$0:Lcom/google/android/voicesearch/hotword/HotwordDetector;

    # getter for: Lcom/google/android/voicesearch/hotword/HotwordDetector;->mHotwordListener:Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;
    invoke-static {v1}, Lcom/google/android/voicesearch/hotword/HotwordDetector;->access$300(Lcom/google/android/voicesearch/hotword/HotwordDetector;)Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->mHotwordFired:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasPartialResult()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getPartialResult()Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getHotwordFired()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getPartialResult()Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getEndTimeUsec()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;->onHotword(J)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordDetectorListener;->mHotwordFired:Z

    :cond_0
    return-void
.end method
