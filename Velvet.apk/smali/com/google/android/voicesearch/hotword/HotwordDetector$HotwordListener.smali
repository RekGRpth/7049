.class public interface abstract Lcom/google/android/voicesearch/hotword/HotwordDetector$HotwordListener;
.super Ljava/lang/Object;
.source "HotwordDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/hotword/HotwordDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HotwordListener"
.end annotation


# virtual methods
.method public abstract onHotword(J)V
.end method

.method public abstract onHotwordDetectorStarted()V
.end method

.method public abstract onHotwordDetectorStopped()V
.end method

.method public abstract onMusicDetected()V
.end method
