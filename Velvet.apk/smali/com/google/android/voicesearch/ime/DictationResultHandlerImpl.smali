.class public Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;
.super Ljava/lang/Object;
.source "DictationResultHandlerImpl.java"


# instance fields
.field private final mCommitNewTextRunnable:Ljava/lang/Runnable;

.field private mCommitNewTextScheduled:Z

.field private final mDelayBetweenCommittingNewTextMsec:J

.field private mDictationSegmentWithNewText:I

.field private final mDictationSegments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/voicesearch/ime/DictationSegment;",
            ">;"
        }
    .end annotation
.end field

.field private final mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

.field private mNextSegmentId:I

.field private mRequestId:Ljava/lang/String;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private final mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

.field private final mTextFormatter:Lcom/google/android/voicesearch/ime/formatter/TextFormatter;

.field private final mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;Lcom/google/android/speech/logger/SuggestionLogger;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/ime/formatter/TextFormatter;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;
    .param p2    # Lcom/google/android/speech/logger/SuggestionLogger;
    .param p3    # Lcom/google/android/voicesearch/settings/Settings;
    .param p4    # Lcom/google/android/voicesearch/ime/formatter/TextFormatter;
    .param p5    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl$1;-><init>(Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextRunnable:Ljava/lang/Runnable;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-static {p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegments:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p4, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mTextFormatter:Lcom/google/android/voicesearch/ime/formatter/TextFormatter;

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getDelayBetweenCommittingNewTextMsec()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDelayBetweenCommittingNewTextMsec:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mNextSegmentId:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->executeScheduledCommitNewText()V

    return-void
.end method

.method private varargs apply([Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;)V
    .locals 7
    .param p1    # [Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v5

    invoke-static {v5}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    iget-object v5, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v5}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v2}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    :try_start_0
    iget-object v5, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mTextFormatter:Lcom/google/android/voicesearch/ime/formatter/TextFormatter;

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->extractText(Landroid/view/inputmethod/InputConnection;)Landroid/view/inputmethod/ExtractedText;

    move-result-object v6

    invoke-interface {v5, v2, v6}, Lcom/google/android/voicesearch/ime/formatter/TextFormatter;->handleCommit(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/ExtractedText;)V

    move-object v0, p1

    array-length v3, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    if-eqz v4, :cond_1

    invoke-virtual {v4, v2}, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;->apply(Landroid/view/inputmethod/InputConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-interface {v2}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    throw v5
.end method

.method private declared-synchronized executeScheduledCommitNewText()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextScheduled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->commitNewText()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private extractText(Landroid/view/inputmethod/InputConnection;)Landroid/view/inputmethod/ExtractedText;
    .locals 4
    .param p1    # Landroid/view/inputmethod/InputConnection;

    const/4 v2, 0x0

    if-nez p1, :cond_1

    move-object v0, v2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Landroid/view/inputmethod/ExtractedTextRequest;

    invoke-direct {v1}, Landroid/view/inputmethod/ExtractedTextRequest;-><init>()V

    const/4 v3, 0x1

    iput v3, v1, Landroid/view/inputmethod/ExtractedTextRequest;->flags:I

    const/4 v3, 0x0

    invoke-interface {p1, v1, v3}, Landroid/view/inputmethod/InputConnection;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v3, v0, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    if-eqz v3, :cond_2

    iget-object v3, v0, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    instance-of v3, v3, Landroid/text/Spanned;

    if-nez v3, :cond_0

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method private forceCommitAllText()V
    .locals 4

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->getDictationSegmentWithNewText()Lcom/google/android/voicesearch/ime/DictationSegment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationSegment;->getAllNextText()Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->apply([Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private getDictationSegmentWithNewText()Lcom/google/android/voicesearch/ime/DictationSegment;
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegments:Ljava/util/List;

    iget v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegmentWithNewText:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/DictationSegment;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationSegment;->hasMoreText()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationSegment;->isFinal()Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegmentWithNewText:I

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    iget v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegmentWithNewText:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegmentWithNewText:I

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->getDictationSegmentWithNewText()Lcom/google/android/voicesearch/ime/DictationSegment;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private getPartialDictationSegment()Lcom/google/android/voicesearch/ime/DictationSegment;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegments:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/DictationSegment;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationSegment;->isFinal()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/voicesearch/ime/DictationSegment;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v1}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mRequestId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mNextSegmentId:I

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/voicesearch/ime/DictationSegment;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/speech/logger/SuggestionLogger;)V

    iget v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mNextSegmentId:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mNextSegmentId:I

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDictationSegments:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method

.method private declared-synchronized startDictation()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v2}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->extractText(Landroid/view/inputmethod/InputConnection;)Landroid/view/inputmethod/ExtractedText;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    if-nez v0, :cond_1

    :try_start_3
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    :try_start_4
    iget v2, v0, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    add-int/2addr v2, v3

    if-ltz v2, :cond_2

    iget v2, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    if-ge v2, v3, :cond_2

    const-string v2, "DictationResultHandlerImpl"

    const-string v3, "Removing selected text"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, v0, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    iget v4, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    add-int/2addr v3, v4

    invoke-interface {v1, v2, v3}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    const/4 v2, 0x0

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    iget v4, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    sub-int/2addr v3, v4

    invoke-interface {v1, v2, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    :cond_2
    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mTextFormatter:Lcom/google/android/voicesearch/ime/formatter/TextFormatter;

    invoke-interface {v2, v0}, Lcom/google/android/voicesearch/ime/formatter/TextFormatter;->startDictation(Landroid/view/inputmethod/ExtractedText;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto :goto_0

    :catchall_1
    move-exception v2

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method


# virtual methods
.method protected declared-synchronized commitNewText()V
    .locals 6

    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextScheduled:Z

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->getDictationSegmentWithNewText()Lcom/google/android/voicesearch/ime/DictationSegment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationSegment;->getNextText()Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->apply([Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->getDictationSegmentWithNewText()Lcom/google/android/voicesearch/ime/DictationSegment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextScheduled:Z

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextRunnable:Ljava/lang/Runnable;

    invoke-interface {v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextRunnable:Ljava/lang/Runnable;

    iget-wide v4, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mDelayBetweenCommittingNewTextMsec:J

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized handleError()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->forceCommitAllText()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized handlePartialRecognitionResult(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mTextFormatter:Lcom/google/android/voicesearch/ime/formatter/TextFormatter;

    invoke-interface {v2, p1}, Lcom/google/android/voicesearch/ime/formatter/TextFormatter;->formatPartialResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->getPartialDictationSegment()Lcom/google/android/voicesearch/ime/DictationSegment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/DictationSegment;->updatePartial(Ljava/lang/String;)Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->apply([Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;)V

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextScheduled:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->commitNewText()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized handleRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mTextFormatter:Lcom/google/android/voicesearch/ime/formatter/TextFormatter;

    invoke-interface {v3, p1}, Lcom/google/android/voicesearch/ime/formatter/TextFormatter;->formatResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mTextFormatter:Lcom/google/android/voicesearch/ime/formatter/TextFormatter;

    invoke-interface {v3, p2}, Lcom/google/android/voicesearch/ime/formatter/TextFormatter;->formatAlternateSpan(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mTextFormatter:Lcom/google/android/voicesearch/ime/formatter/TextFormatter;

    invoke-interface {v3}, Lcom/google/android/voicesearch/ime/formatter/TextFormatter;->reset()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->getPartialDictationSegment()Lcom/google/android/voicesearch/ime/DictationSegment;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ime/DictationSegment;->updateFinal(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    move-result-object v2

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    invoke-virtual {v0, p3}, Lcom/google/android/voicesearch/ime/DictationSegment;->updatePartial(Ljava/lang/String;)Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    move-result-object v1

    :cond_0
    if-eqz v2, :cond_1

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->apply([Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;)V

    :cond_1
    iget-boolean v3, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextScheduled:Z

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->commitNewText()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized handleStop()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->forceCommitAllText()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized init(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mRequestId:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mNextSegmentId:I

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->startDictation()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 2

    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mNextSegmentId:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextScheduled:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mUiExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->mCommitNewTextRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
