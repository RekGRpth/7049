.class public Lcom/google/android/voicesearch/ime/DictationSegment;
.super Ljava/lang/Object;
.source "DictationSegment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;
    }
.end annotation


# instance fields
.field private mAlternateSpans:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDisplayLength:I

.field private mPartialRecognitionResult:Z

.field private final mRequestId:Ljava/lang/String;

.field private final mSegmentId:I

.field private final mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

.field private mText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/speech/logger/SuggestionLogger;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Lcom/google/android/speech/logger/SuggestionLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mRequestId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mSegmentId:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mPartialRecognitionResult:Z

    iput-object p4, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

    return-void
.end method

.method private static getCommonStart(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v1, v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, p2

    goto :goto_1
.end method

.method private getFinalText()Ljava/lang/CharSequence;
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/DictationSegment;->isFinal()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mRequestId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mSegmentId:I

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mAlternateSpans:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mSuggestionLogger:Lcom/google/android/speech/logger/SuggestionLogger;

    invoke-static/range {v0 .. v5}, Lcom/google/android/speech/alternates/Suggestions;->getSuggestionSpan(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Lcom/google/android/speech/logger/SuggestionLogger;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v0, "DictationResult"

    const-string v1, "Unable to encode text"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getNextSplitPos(Ljava/lang/String;I)I
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # I

    const-string v1, " "

    invoke-virtual {p0, v1, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0
.end method

.method private updateText(Ljava/lang/String;)Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;
    .locals 6
    .param p1    # Ljava/lang/String;

    iget v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    invoke-static {v4, p1, v5}, Lcom/google/android/voicesearch/ime/DictationSegment;->getCommonStart(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v4

    sub-int v0, v3, v4

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    const/16 v3, 0x4a

    invoke-static {v3}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    if-ltz v3, :cond_1

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-eq v3, v4, :cond_1

    iget v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    invoke-static {p1, v3}, Lcom/google/android/voicesearch/ime/DictationSegment;->getNextSplitPos(Ljava/lang/String;I)I

    move-result v1

    :cond_1
    new-instance v2, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    iget v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    sub-int/2addr v3, v0

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;-><init>(ILjava/lang/CharSequence;)V

    iput v1, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    goto :goto_0
.end method


# virtual methods
.method public getAllNextText()Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/DictationSegment;->hasMoreText()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    iget v1, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationSegment;->getFinalText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;-><init>(ILjava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationSegment;->getFinalText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNextText()Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;
    .locals 5

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/voicesearch/ime/DictationSegment;->getNextSplitPos(Ljava/lang/String;I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v0, v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mPartialRecognitionResult:Z

    if-nez v3, :cond_0

    new-instance v2, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    iget v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationSegment;->getFinalText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;-><init>(ILjava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    :goto_0
    return-object v2

    :cond_0
    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    iput v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    new-instance v2, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v1}, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;-><init>(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public hasMoreText()Z
    .locals 2

    iget v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFinal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mPartialRecognitionResult:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateFinal(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;)",
            "Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/DictationSegment;->updateText(Ljava/lang/String;)Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    move-result-object v1

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mAlternateSpans:Ljava/util/List;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mPartialRecognitionResult:Z

    iget v2, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v2, v3, :cond_0

    if-eqz v1, :cond_1

    # getter for: Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;->mDeleteChars:I
    invoke-static {v1}, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;->access$000(Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;)I

    move-result v2

    iget v3, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    add-int/2addr v2, v3

    # getter for: Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;->mNewText:Ljava/lang/CharSequence;
    invoke-static {v1}, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;->access$100(Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    sub-int v0, v2, v3

    :goto_0
    new-instance v1, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/DictationSegment;->getFinalText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;-><init>(ILjava/lang/CharSequence;)V

    :cond_0
    return-object v1

    :cond_1
    iget v0, p0, Lcom/google/android/voicesearch/ime/DictationSegment;->mDisplayLength:I

    goto :goto_0
.end method

.method public updatePartial(Ljava/lang/String;)Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/DictationSegment;->updateText(Ljava/lang/String;)Lcom/google/android/voicesearch/ime/DictationSegment$UpdateText;

    move-result-object v0

    return-object v0
.end method
