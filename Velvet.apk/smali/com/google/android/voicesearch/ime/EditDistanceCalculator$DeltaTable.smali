.class Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;
.super Ljava/lang/Object;
.source "EditDistanceCalculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/ime/EditDistanceCalculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeltaTable"
.end annotation


# instance fields
.field private mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

.field private mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

.field final synthetic this$0:Lcom/google/android/voicesearch/ime/EditDistanceCalculator;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/ime/EditDistanceCalculator;II)V
    .locals 5
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->this$0:Lcom/google/android/voicesearch/ime/EditDistanceCalculator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, p2, [Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    iput-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    new-array v1, p2, [Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    iput-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    # getter for: Lcom/google/android/voicesearch/ime/EditDistanceCalculator;->mMaxNewChars:S
    invoke-static {p1}, Lcom/google/android/voicesearch/ime/EditDistanceCalculator;->access$800(Lcom/google/android/voicesearch/ime/EditDistanceCalculator;)S

    move-result v1

    sub-int v1, p3, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    new-instance v2, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    invoke-direct {v2, p1, v3}, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;-><init>(Lcom/google/android/voicesearch/ime/EditDistanceCalculator;Lcom/google/android/voicesearch/ime/EditDistanceCalculator$1;)V

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v1, v1, v0

    iput-byte v4, v1, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mContiguousChars:B

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v1, v1, v0

    iput-short v4, v1, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mTotalChars:S

    :goto_1
    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    new-instance v2, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    invoke-direct {v2, p1, v3}, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;-><init>(Lcom/google/android/voicesearch/ime/EditDistanceCalculator;Lcom/google/android/voicesearch/ime/EditDistanceCalculator$1;)V

    aput-object v2, v1, v0

    add-int/lit8 v1, v0, 0x1

    int-to-short v0, v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    new-instance v2, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    invoke-direct {v2, p1, v3}, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;-><init>(Lcom/google/android/voicesearch/ime/EditDistanceCalculator;Lcom/google/android/voicesearch/ime/EditDistanceCalculator$1;)V

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mDeadEnd:Z

    goto :goto_1

    :cond_1
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/ime/EditDistanceCalculator;IILcom/google/android/voicesearch/ime/EditDistanceCalculator$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/ime/EditDistanceCalculator;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/voicesearch/ime/EditDistanceCalculator$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;-><init>(Lcom/google/android/voicesearch/ime/EditDistanceCalculator;II)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->toCurrRowDebug()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;)[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;)[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->nextRow()V

    return-void
.end method

.method private nextRow()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    iput-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v1, v1, v3

    iget-boolean v1, v1, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mDeadEnd:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v1, v1, v3

    # invokes: Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->canAddContiguousChar()Z
    invoke-static {v1}, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->access$1000(Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v1, v1, v3

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mDeadEnd:Z

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mPrevRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v2, v2, v3

    aput-object v2, v1, v3

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v1, v1, v3

    iget-short v2, v1, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mTotalChars:S

    add-int/lit8 v2, v2, 0x1

    int-to-short v2, v2

    iput-short v2, v1, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mTotalChars:S

    goto :goto_0
.end method

.method private toCurrRowDebug()Ljava/lang/String;
    .locals 5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v3, v3, v0

    iget-boolean v3, v3, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mDeadEnd:Z

    if-eqz v3, :cond_0

    const-string v3, " D"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v3, "  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v0, 0x1

    int-to-short v0, v3

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$DeltaTable;->mCurrRow:[Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;

    aget-object v3, v3, v0

    iget-short v3, v3, Lcom/google/android/voicesearch/ime/EditDistanceCalculator$Delta;->mTotalChars:S

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
