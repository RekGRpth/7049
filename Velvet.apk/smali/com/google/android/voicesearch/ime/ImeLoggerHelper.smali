.class public Lcom/google/android/voicesearch/ime/ImeLoggerHelper;
.super Ljava/lang/Object;
.source "ImeLoggerHelper.java"


# instance fields
.field private mActive:Z

.field private mFirstImeRun:Z

.field private mStartTimestamp:J

.field private mWaitingForResult:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone()V
    .locals 1

    const/16 v0, 0x27

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method public onEndSession()V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mStartTimestamp:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/16 v0, 0x28

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iput-wide v2, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mStartTimestamp:J

    :cond_0
    return-void
.end method

.method public onError()V
    .locals 1

    const/16 v0, 0x26

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method public onFinishInput()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mActive:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mWaitingForResult:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x2a

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :cond_0
    const/16 v0, 0x29

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mActive:Z

    return-void
.end method

.method public onHideWindow()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onEndSession()V

    return-void
.end method

.method public onInterrupt()V
    .locals 1

    const/16 v0, 0x2a

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method public onPauseRecognition()V
    .locals 1

    const/16 v0, 0x3f

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method public onRestartRecognition()V
    .locals 1

    const/16 v0, 0x40

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    return-void
.end method

.method public onShowWindow()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mFirstImeRun:Z

    return-void
.end method

.method public onStartInputView(Landroid/view/inputmethod/EditorInfo;)V
    .locals 4
    .param p1    # Landroid/view/inputmethod/EditorInfo;

    const/4 v3, 0x0

    const-string v0, ""

    if-eqz p1, :cond_0

    iget-object v1, p1, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/voicesearch/util/TextUtil;->safeToString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/16 v1, 0x23

    invoke-static {v1, v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(ILjava/lang/Object;)V

    iget-boolean v1, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mFirstImeRun:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/voicesearch/VoiceSearchClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mStartTimestamp:J

    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mActive:Z

    iput-boolean v3, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mFirstImeRun:Z

    iput-boolean v3, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mWaitingForResult:Z

    return-void

    :cond_1
    const/16 v1, 0x24

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    goto :goto_0
.end method

.method public setWaitingForResult(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->mWaitingForResult:Z

    return-void
.end method
