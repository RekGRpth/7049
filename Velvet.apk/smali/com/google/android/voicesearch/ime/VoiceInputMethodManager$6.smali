.class Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;
.super Ljava/lang/Object;
.source "VoiceInputMethodManager.java"

# interfaces
.implements Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleCreateInputView()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$200(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->isWaitingForResults()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mBackToPrevImeOnDone:Z
    invoke-static {v0, v1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$602(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;Z)Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$700(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayWorking()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->backToPreviousIme()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$000(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    goto :goto_0
.end method

.method public forceClose()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->backToPreviousIme()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$000(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    return-void
.end method

.method public onDisplayDialectSelectionPopup()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->stopRecording()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$800(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->stopDictation()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$900(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$700(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayAudioNotInitialized()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->maybeUpdateImeSubtypes()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1000(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    return-void
.end method

.method public onUpdateDialect(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)V
    .locals 2
    .param p1    # Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    const/16 v0, 0x43

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1100(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/TemporaryData;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;->getBcp47Locale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/TemporaryData;->setData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->startDictation()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$500(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1200(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/TemporaryData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->extend()V

    return-void
.end method

.method public startRecognition()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$300(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onRestartRecognition()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->startDictation()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$500(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    return-void
.end method

.method public stopRecognition()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$200(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->stopListening()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$300(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onPauseRecognition()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handlePause()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$400(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    return-void
.end method
