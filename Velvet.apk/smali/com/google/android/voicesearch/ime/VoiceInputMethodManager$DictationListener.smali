.class public Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;
.super Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;
.source "VoiceInputMethodManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DictationListener"
.end annotation


# instance fields
.field private mInvalid:Z

.field final synthetic this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/speech/listeners/RecognitionEventListenerAdapter;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    return-void
.end method

.method private getPartial(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Ljava/lang/String;
    .locals 11
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasPartialResult()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getPartialResult()Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getPartCount()I

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSettings:Lcom/google/android/voicesearch/settings/Settings;
    invoke-static {v7}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1700(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;->getDictation()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dictation;->getPartialResultMinConfidence()F

    move-result v5

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    invoke-virtual {v4, v1}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialResult;->getPart(I)Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->hasText()Z

    move-result v7

    if-nez v7, :cond_2

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getStability()D

    move-result-wide v7

    float-to-double v9, v5

    cmpl-double v7, v7, v9

    if-ltz v7, :cond_3

    invoke-virtual {v3}, Lcom/google/speech/recognizer/api/RecognizerProtos$PartialPart;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method private processPartialRecognitionResult(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handlePartialRecognitionResult(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1800(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    return-void
.end method

.method public isValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBeginningOfSpeech()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$700(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayRecording()V

    goto :goto_0
.end method

.method public onDone()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->invalidate()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleDone()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1500(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    goto :goto_0
.end method

.method public onEndOfSpeech()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1300(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playDictationDoneSound()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$200(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->stopListening()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handlePause()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$400(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 3
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->invalidate()V

    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/speech/exception/RecognizeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1300(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playErrorSound()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleError(Lcom/google/android/speech/exception/RecognizeException;)V
    invoke-static {v0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1600(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;Lcom/google/android/speech/exception/RecognizeException;)V

    goto :goto_0
.end method

.method public onNoSpeechDetected()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->invalidate()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1300(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playNoInputSound()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$200(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->cancelRecognition()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handlePause()V
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$400(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    goto :goto_0
.end method

.method public onReadyForSpeech(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$700(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayListening()V

    return-void
.end method

.method public onRecognitionCancelled()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->invalidate()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1300(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->playNoInputSound()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
    invoke-static {v0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$700(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # getter for: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;
    invoke-static {v1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$200(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->isWaitingForResults()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayPause(Z)V

    goto :goto_0
.end method

.method public onRecognitionResult(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)V
    .locals 6
    .param p1    # Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;

    iget-boolean v4, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->mInvalid:Z

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->getPartial(Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->hasResult()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getResult()Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesisCount()I

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "VoiceInputMethodManager"

    const-string v5, "No hypothesis in recognition result."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionResult;->getHypothesis(I)Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->hasAlternates()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getAlternates()Lcom/google/speech/common/Alternates$RecognitionClientAlternates;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/speech/common/Alternates$RecognitionClientAlternates;->getSpanList()Ljava/util/List;

    move-result-object v3

    :cond_3
    iget-object v4, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-virtual {v0}, Lcom/google/speech/recognizer/api/RecognizerProtos$Hypothesis;->getText()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    invoke-static {v4, v5, v3, v1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1400(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/google/speech/recognizer/api/RecognizerProtos$RecognitionEvent;->getEventType()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->invalidate()V

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->this$0:Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    # invokes: Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleDone()V
    invoke-static {v4}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->access$1500(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    goto :goto_0

    :cond_5
    if-eqz v1, :cond_4

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;->processPartialRecognitionResult(Ljava/lang/String;)V

    goto :goto_1
.end method
