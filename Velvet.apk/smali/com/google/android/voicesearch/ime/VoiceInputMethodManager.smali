.class public Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
.super Ljava/lang/Object;
.source "VoiceInputMethodManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;
    }
.end annotation


# instance fields
.field private mBackToPrevImeOnDone:Z

.field private final mBackToPreviousImeRunnable:Ljava/lang/Runnable;

.field private mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/voicesearch/ime/TemporaryData",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/voicesearch/ime/TemporaryData",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

.field private mForcePauseOnStart:Lcom/google/android/voicesearch/ime/TemporaryData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/voicesearch/ime/TemporaryData",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

.field private mInputViewActive:Z

.field private final mReleaseResourcesRunnable:Ljava/lang/Runnable;

.field private final mScreenStateMonitor:Lcom/google/android/voicesearch/ime/ScreenStateMonitor;

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;

.field private final mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

.field private final mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

.field private final mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

.field private final mVoiceImeSubtypeUpdater:Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

.field private final mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

.field private mVoiceLanguageSelector:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

.field private final mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;Lcom/google/android/voicesearch/ime/ScreenStateMonitor;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/ime/ImeLoggerHelper;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;)V
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;
    .param p2    # Lcom/google/android/voicesearch/ime/ScreenStateMonitor;
    .param p3    # Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;
    .param p4    # Lcom/google/android/voicesearch/ime/ImeLoggerHelper;
    .param p5    # Lcom/google/android/voicesearch/settings/Settings;
    .param p6    # Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    .param p7    # Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;
    .param p8    # Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
    .param p9    # Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;
    .param p10    # Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;
    .param p11    # Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/ime/TemporaryData;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    new-instance v0, Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/ime/TemporaryData;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mForcePauseOnStart:Lcom/google/android/voicesearch/ime/TemporaryData;

    new-instance v0, Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/ime/TemporaryData;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mBackToPrevImeOnDone:Z

    new-instance v0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$1;-><init>(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mBackToPreviousImeRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$2;-><init>(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mReleaseResourcesRunnable:Ljava/lang/Runnable;

    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceLanguageSelector:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mScreenStateMonitor:Lcom/google/android/voicesearch/ime/ScreenStateMonitor;

    invoke-static {p5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/settings/Settings;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {p6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-static {p7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeSubtypeUpdater:Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    invoke-static {p8}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-static {p9}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    iput-object p10, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    iput-object p11, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->backToPreviousIme()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->maybeReleaseResources()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->maybeUpdateImeSubtypes()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/TemporaryData;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/TemporaryData;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/List;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleDone()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handleError(Lcom/google/android/speech/exception/RecognizeException;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/settings/Settings;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handlePartialRecognitionResult(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/ImeLoggerHelper;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->handlePause()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->startDictation()V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mBackToPrevImeOnDone:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->stopRecording()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V
    .locals 0
    .param p0    # Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->stopDictation()V

    return-void
.end method

.method private backToPreviousIme()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->maybeReleaseResources()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v0}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->switchToLastInputMethod()V

    return-void
.end method

.method public static create(Landroid/inputmethodservice/InputMethodService;)Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;
    .locals 27
    .param p0    # Landroid/inputmethodservice/InputMethodService;

    invoke-static/range {p0 .. p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getAsyncServices()Lcom/google/android/searchcommon/AsyncServices;

    move-result-object v20

    new-instance v11, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-direct {v11}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v6

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSoundManager()Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    move-result-object v13

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSuggestionLogger()Lcom/google/android/speech/logger/SuggestionLogger;

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/voicesearch/VoiceSearchServices;->getAccountHelper()Lcom/google/android/speech/helper/AccountHelper;

    move-result-object v19

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/voicesearch/VoiceSearchServices;->getScheduledExecutorService()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v24

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/voicesearch/VoiceSearchServices;->getVoiceImeSubtypeUpdater()Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    move-result-object v14

    new-instance v15, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSpeechLevelSource()Lcom/google/android/speech/SpeechLevelSource;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v7}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;-><init>(Landroid/content/Context;Lcom/google/android/speech/SpeechLevelSource;)V

    const-string v7, "power"

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Landroid/inputmethodservice/InputMethodService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/os/PowerManager;

    new-instance v9, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;-><init>(Landroid/content/Context;)V

    new-instance v25, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;-><init>(Landroid/content/Context;Lcom/google/android/voicesearch/settings/Settings;)V

    new-instance v23, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$3;

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$3;-><init>(Lcom/google/android/voicesearch/VoiceSearchServices;)V

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/voicesearch/VoiceSearchServices;->getRecognizerParamsFactory()Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;

    move-result-object v22

    new-instance v4, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v26

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$4;-><init>(Landroid/inputmethodservice/InputMethodService;Landroid/os/PowerManager;Lcom/google/android/voicesearch/VoiceSearchServices;)V

    new-instance v17, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    invoke-interface/range {v20 .. v20}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v7

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-direct {v0, v4, v7, v1}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;-><init>(Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;Ljava/util/concurrent/Executor;Lcom/google/android/voicesearch/speechservice/s3/RecognizerParamsFactory;)V

    new-instance v3, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    new-instance v7, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;

    invoke-direct {v7}, Lcom/google/android/voicesearch/ime/formatter/LatinTextFormatter;-><init>()V

    invoke-interface/range {v20 .. v20}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;-><init>(Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;Lcom/google/android/speech/logger/SuggestionLogger;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/ime/formatter/TextFormatter;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;)V

    new-instance v7, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;

    invoke-interface/range {v20 .. v20}, Lcom/google/android/searchcommon/AsyncServices;->getUiThreadExecutor()Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    move-result-object v10

    move-object/from16 v8, v25

    move-object v12, v6

    move-object/from16 v16, v4

    move-object/from16 v18, v3

    invoke-direct/range {v7 .. v18}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;-><init>(Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;Lcom/google/android/voicesearch/ime/ScreenStateMonitor;Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;Lcom/google/android/voicesearch/ime/ImeLoggerHelper;Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;)V

    return-object v7
.end method

.method private forceReleaseResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->forceExpire()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->maybeReleaseResources()V

    return-void
.end method

.method private handleDone()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onDone()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->hideWaitingForResults()V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mBackToPrevImeOnDone:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->backToPreviousIme()V

    :cond_0
    return-void
.end method

.method private handleError(Lcom/google/android/speech/exception/RecognizeException;)V
    .locals 2
    .param p1    # Lcom/google/android/speech/exception/RecognizeException;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->handleError()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-static {p1}, Lcom/google/android/voicesearch/util/ErrorUtils;->getErrorMessage(Lcom/google/android/speech/exception/RecognizeException;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayError(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onError()V

    return-void
.end method

.method private handlePartialRecognitionResult(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->setWaitingForResult(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->handlePartialRecognitionResult(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private handlePause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->handleStop()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->isWaitingForResults()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayPause(Z)V

    return-void
.end method

.method private handleRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/speech/common/Alternates$AlternateSpan;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->setWaitingForResult(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->getRecognitionParameters()Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParams;->isAlternatesEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p2, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->handleRecognitionResult(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method private interruptDictation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onInterrupt()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->stopRecording()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->backToPreviousIme()V

    :cond_1
    return-void
.end method

.method private isDictationSupportedByField()Z
    .locals 14

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v12, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v12}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v1

    iget v3, v1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v8, v3, 0xfff

    const/16 v12, 0x81

    if-ne v8, v12, :cond_1

    move v7, v11

    :goto_0
    const/16 v12, 0xe1

    if-ne v8, v12, :cond_2

    move v9, v11

    :goto_1
    const/16 v12, 0x12

    if-ne v8, v12, :cond_3

    move v5, v11

    :goto_2
    if-nez v7, :cond_0

    if-nez v9, :cond_0

    if-eqz v5, :cond_4

    :cond_0
    const-string v11, "VoiceInputMethodManager"

    const-string v12, "Voice IME is not supported for password input type"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    return v10

    :cond_1
    move v7, v10

    goto :goto_0

    :cond_2
    move v9, v10

    goto :goto_1

    :cond_3
    move v5, v10

    goto :goto_2

    :cond_4
    if-eqz v1, :cond_7

    iget-object v12, v1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    if-eqz v12, :cond_7

    iget-object v12, v1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v4, v0

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v4, :cond_7

    aget-object v6, v0, v2

    const-string v12, "noMicrophoneKey"

    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_5

    const-string v12, "nm"

    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    :cond_5
    const-string v11, "VoiceInputMethodManager"

    const-string v12, "Voice IME has been disabled for this field"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    move v10, v11

    goto :goto_3
.end method

.method private maybeReleaseResources()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->isExpired()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#releaseResources"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mInputViewActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onFinishInput()V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mInputViewActive:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mScreenStateMonitor:Lcom/google/android/voicesearch/ime/ScreenStateMonitor;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->unregister()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->extend()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v0}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->scheduleSendEvents()V

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->stopDictation()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mReleaseResourcesRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mBackToPreviousImeRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    :goto_1
    return-void

    :cond_1
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "onFinishInput - mImeLoggerHelper is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#releaseResources - schedule"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->scheduleReleaseResources()V

    goto :goto_1
.end method

.method private maybeUpdateImeSubtypes()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeSubtypeUpdater:Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/VoiceImeSubtypeUpdater;->maybeScheduleUpdate(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)V

    return-void
.end method

.method private scheduleReleaseResources()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mReleaseResourcesRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mReleaseResourcesRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->executeDelayed(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private startDictation()V
    .locals 6

    iget-boolean v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mInputViewActive:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v2}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getRecognizer()Lcom/google/android/speech/Recognizer;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/ime/TemporaryData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->createRecognitionParams(Ljava/lang/String;)Lcom/google/android/speech/params/RecognizerParams;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v0}, Lcom/google/android/speech/params/RecognizerParams;->getRequestId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->init(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/ime/TemporaryData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceLanguageSelector:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/ime/TemporaryData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->getEnabledDialects(Ljava/lang/String;)[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setLanguages(Ljava/lang/String;[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayAudioNotInitialized()V

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->cancelRecognition()V

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    new-instance v3, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;

    invoke-direct {v3, p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;-><init>(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    invoke-virtual {v2, v0, v3}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->startRecognizer(Lcom/google/android/speech/params/RecognizerParams;Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$DictationListener;)V

    goto :goto_0
.end method

.method private stopDictation()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->cancelRecognition()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->handleStop()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->reset()V

    :cond_0
    return-void

    :cond_1
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "onFinishInput - mVoiceRecognitionDelegate is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopRecording()V
    .locals 2

    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#stopRecording"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->stopListening()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;->handleStop()V

    return-void
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # Ljava/io/PrintWriter;
    .param p3    # [Ljava/lang/String;

    new-instance v0, Landroid/util/PrintWriterPrinter;

    invoke-direct {v0, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    const-string v1, "VoiceIME state :"

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mDictationResultHandler = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationResultHandler:Lcom/google/android/voicesearch/ime/DictationResultHandlerImpl;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mImeLoggerHelper="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mVoiceInputViewHelper="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mInputViewActive="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mInputViewActive:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  mVoiceLanguageSelector="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceLanguageSelector:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    return-void
.end method

.method public handleConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;

    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#handleConfigurationChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mForcePauseOnStart:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->extend()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->isRecording()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->isListening()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/16 v0, 0x4b

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->extend()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->maybeReleaseResources()V

    return-void
.end method

.method public handleCreateInputView()Landroid/view/View;
    .locals 2

    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#handleCreateInputView"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    new-instance v1, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;

    invoke-direct {v1, p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$6;-><init>(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->getView(Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler$Callback;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public handleDestroy()V
    .locals 2

    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#handleDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceRecognitionHandler:Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/VoiceRecognitionHandler;->cancelRecognition()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->forceReleaseResources()V

    return-void
.end method

.method public handleFinishInput()V
    .locals 3

    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#handleFinishInput "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->maybeReleaseResources()V

    return-void
.end method

.method public handleFinishInputView(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#handleFinishInputView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public handleHideWindow()V
    .locals 2

    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#handleHideWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->backToPreviousIme()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onHideWindow()V

    :cond_0
    return-void
.end method

.method public handleShowWindow(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#handleShowWindow["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onShowWindow()V

    :cond_0
    return-void
.end method

.method public handleStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 5
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "VoiceInputMethodManager"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "#handleStartInputView [active="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v4, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mInputViewActive:Z

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ",keepRecording="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->isExpired()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " ] "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mSoundManager:Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/audio/AudioTrackSoundManager;->setAudioStream(I)V

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mInputViewActive:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->isDictationSupportedByField()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v0}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "VoiceInputMethodManager"

    const-string v3, "Voice IME cannot be started"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mBackToPreviousImeRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v3}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mScreenStateMonitor:Lcom/google/android/voicesearch/ime/ScreenStateMonitor;

    new-instance v3, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$5;

    invoke-direct {v3, p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager$5;-><init>(Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;)V

    invoke-virtual {v0, v3}, Lcom/google/android/voicesearch/ime/ScreenStateMonitor;->register(Lcom/google/android/voicesearch/ime/ScreenStateMonitor$Listener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mImeLoggerHelper:Lcom/google/android/voicesearch/ime/ImeLoggerHelper;

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v3}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/voicesearch/ime/ImeLoggerHelper;->onStartInputView(Landroid/view/inputmethod/EditorInfo;)V

    iput-boolean v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mInputViewActive:Z

    iput-boolean v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mBackToPrevImeOnDone:Z

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->isExpired()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->getData()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceLanguageSelector:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->getDictationBcp47Locale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/TemporaryData;->setData(Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mForcePauseOnStart:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->isExpired()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mForcePauseOnStart:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->extend()V

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceLanguageSelector:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ime/TemporaryData;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->getEnabledDialects(Ljava/lang/String;)[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setLanguages(Ljava/lang/String;[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->displayPause(Z)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->startDictation()V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->isExpired()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mContinueRecording:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->extend()V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mUiThreadExecutor:Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mReleaseResourcesRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/google/android/searchcommon/util/ScheduledSingleThreadedExecutor;->cancelExecute(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/TemporaryData;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceLanguageSelector:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;

    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mDictationBcp47Locale:Lcom/google/android/voicesearch/ime/TemporaryData;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/ime/TemporaryData;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->getEnabledDialects(Ljava/lang/String;)[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->setLanguages(Ljava/lang/String;[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceInputViewHandler:Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ime/view/VoiceInputViewHandler;->restoreState()V

    goto :goto_1
.end method

.method public handleUpdateSelection(IIIIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    if-eq p3, p4, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->interruptDictation()V

    :cond_0
    return-void
.end method

.method public handleViewClicked(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#handleViewClicked["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->interruptDictation()V

    :cond_0
    return-void
.end method

.method public isMaybeForceFullScreen()Z
    .locals 8

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v6}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v3

    if-eqz v3, :cond_2

    iget v6, v3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v7, 0x10000000

    and-int/2addr v6, v7

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget v6, v3, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v7, 0x2000000

    and-int/2addr v6, v7

    if-nez v6, :cond_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodManager;->mVoiceImeInputMethodService:Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;

    invoke-interface {v6}, Lcom/google/android/voicesearch/ime/VoiceImeInputMethodService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v6, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v6

    const v6, 0x7f0c0030

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    cmpl-float v6, v1, v0

    if-gtz v6, :cond_0

    const/4 v5, 0x1

    goto :goto_0
.end method
