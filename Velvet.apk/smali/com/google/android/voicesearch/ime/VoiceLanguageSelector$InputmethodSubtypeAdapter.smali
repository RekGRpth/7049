.class public interface abstract Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;
.super Ljava/lang/Object;
.source "VoiceLanguageSelector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InputmethodSubtypeAdapter"
.end annotation


# virtual methods
.method public abstract getCurrentSubtypeLocale()Ljava/lang/String;
.end method

.method public abstract getEnabledBcp47Locales()[Ljava/lang/String;
.end method

.method public abstract getLastInputMethodSubtypeLocale()Ljava/lang/String;
.end method
