.class public Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;
.super Ljava/lang/Object;
.source "VoiceLanguageSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;
    }
.end annotation


# instance fields
.field private final mInputmethodSubtypeAdapter:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;

.field private final mPhoneLocaleSupplier:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettings:Lcom/google/android/voicesearch/settings/Settings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/voicesearch/settings/Settings;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/voicesearch/settings/Settings;

    invoke-static {p1}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->createInputmethodSubtypeAdapter(Landroid/content/Context;)Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;

    move-result-object v0

    new-instance v1, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$1;

    invoke-direct {v1}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$1;-><init>()V

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;-><init>(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;Lcom/google/common/base/Supplier;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/settings/Settings;Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/settings/Settings;
    .param p2    # Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/voicesearch/settings/Settings;",
            "Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    iput-object p2, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mInputmethodSubtypeAdapter:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;

    iput-object p3, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mPhoneLocaleSupplier:Lcom/google/common/base/Supplier;

    return-void
.end method

.method private static createInputmethodSubtypeAdapter(Landroid/content/Context;)Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    new-instance v1, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$2;

    invoke-direct {v1, v0}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$2;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    return-object v1
.end method


# virtual methods
.method public getDictationBcp47Locale()Ljava/lang/String;
    .locals 6

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mInputmethodSubtypeAdapter:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;

    invoke-interface {v2}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;->getEnabledBcp47Locales()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mInputmethodSubtypeAdapter:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;

    invoke-interface {v2}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;->getLastInputMethodSubtypeLocale()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v3

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v4, v2

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mPhoneLocaleSupplier:Lcom/google/common/base/Supplier;

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getSpokenBcp47Locale(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mInputmethodSubtypeAdapter:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;

    invoke-interface {v3}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;->getCurrentSubtypeLocale()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getSpokenBcp47Locale(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v2, "VoiceLanguageSelector"

    const-string v3, "The subtype of the IME are not aligned with the supported locale"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v1

    const v2, 0x5fb072

    invoke-static {v2}, Lcom/google/android/voicesearch/logger/BugLogger;->record(I)V

    goto :goto_0
.end method

.method public getEnabledDialects(Ljava/lang/String;)[Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;
    .locals 7
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mInputmethodSubtypeAdapter:Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;

    invoke-interface {v4}, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector$InputmethodSubtypeAdapter;->getEnabledBcp47Locales()[Ljava/lang/String;

    move-result-object v0

    array-length v4, v0

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v4}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getVoiceImeMainLanguage(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v1

    const/4 v4, 0x1

    new-array v3, v4, [Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    :cond_0
    return-object v3

    :cond_1
    array-length v4, v0

    new-array v3, v4, [Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/voicesearch/ime/VoiceLanguageSelector;->mSettings:Lcom/google/android/voicesearch/settings/Settings;

    invoke-virtual {v4}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v4

    aget-object v5, v0, v2

    invoke-static {v4, v5}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getSpokenLanguageByBcp47Locale(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;Ljava/lang/String;)Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Dialect;

    move-result-object v4

    aput-object v4, v3, v2

    const-string v4, "VoiceLanguageSelector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "javaLocales "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
