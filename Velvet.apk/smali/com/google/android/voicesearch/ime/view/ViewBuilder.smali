.class public Lcom/google/android/voicesearch/ime/view/ViewBuilder;
.super Ljava/lang/Object;
.source "ViewBuilder.java"


# instance fields
.field private final mLandHeight:I

.field private final mLandLayout:I

.field private final mPortHeight:I

.field private final mPortLayout:I


# direct methods
.method public constructor <init>(Landroid/util/DisplayMetrics;)V
    .locals 8
    .param p1    # Landroid/util/DisplayMetrics;

    const/4 v7, -0x2

    const v6, 0x7f040061

    const v5, 0x7f04005c

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v3, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v3, v4

    float-to-int v2, v3

    iget v3, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v3, v4

    float-to-int v0, v3

    if-le v2, v0, :cond_0

    move v1, v2

    move v2, v0

    move v0, v1

    :cond_0
    const/16 v3, 0x2ee

    if-lt v2, v3, :cond_1

    const/16 v3, 0x4b0

    if-lt v0, v3, :cond_1

    const v3, 0x43a38000

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortHeight:I

    const/high16 v3, 0x43c10000

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandHeight:I

    iput v5, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortLayout:I

    const v3, 0x7f04005f

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandLayout:I

    :goto_0
    return-void

    :cond_1
    const/16 v3, 0x226

    if-lt v2, v3, :cond_2

    const/16 v3, 0x384

    if-lt v0, v3, :cond_2

    const/high16 v3, 0x43ad0000

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortHeight:I

    const/high16 v3, 0x43950000

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandHeight:I

    iput v5, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortLayout:I

    iput v5, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandLayout:I

    goto :goto_0

    :cond_2
    const/16 v3, 0x168

    if-lt v2, v3, :cond_3

    const/16 v3, 0x24e

    if-lt v0, v3, :cond_3

    const/high16 v3, 0x43830000

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortHeight:I

    const/high16 v3, 0x43490000

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandHeight:I

    iput v5, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortLayout:I

    iput v6, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandLayout:I

    goto :goto_0

    :cond_3
    const/16 v3, 0x140

    if-lt v2, v3, :cond_4

    const/16 v3, 0x212

    if-lt v0, v3, :cond_4

    const/high16 v3, 0x43700000

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortHeight:I

    const/high16 v3, 0x43340000

    iget v4, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandHeight:I

    iput v5, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortLayout:I

    iput v6, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandLayout:I

    goto :goto_0

    :cond_4
    iput v7, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortHeight:I

    iput v7, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandHeight:I

    const v3, 0x7f04005d

    iput v3, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortLayout:I

    iput v6, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandLayout:I

    goto :goto_0
.end method

.method public static create(Landroid/content/Context;)Lcom/google/android/voicesearch/ime/view/ViewBuilder;
    .locals 2
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v1, "window"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    new-instance v1, Lcom/google/android/voicesearch/ime/view/ViewBuilder;

    invoke-direct {v1, v0}, Lcom/google/android/voicesearch/ime/view/ViewBuilder;-><init>(Landroid/util/DisplayMetrics;)V

    return-object v1
.end method

.method private getHeight(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandHeight:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortHeight:I

    goto :goto_0
.end method

.method private getLayout(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mLandLayout:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->mPortLayout:I

    goto :goto_0
.end method


# virtual methods
.method public createView(Landroid/content/Context;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/content/Context;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v7, 0x7f0e0062

    invoke-direct {v1, p1, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const-string v7, "layout_inflater"

    invoke-virtual {v1, v7}, Landroid/view/ContextThemeWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v7, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->getLayout(Z)I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f100136

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/ime/view/ViewBuilder;->getHeight(Z)I

    move-result v7

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    return-object v6

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
