.class Lcom/google/android/voicesearch/intentapi/IntentApiActivity$1;
.super Ljava/lang/Object;
.source "IntentApiActivity.java"

# interfaces
.implements Lcom/google/common/base/Supplier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Supplier",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

.field final synthetic val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;Lcom/google/android/voicesearch/VoiceSearchServices;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$1;->this$0:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    iput-object p2, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$1;->val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$1;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$1;->val$vss:Lcom/google/android/voicesearch/VoiceSearchServices;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$1;->this$0:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getSpokenBcp47Locale(Lcom/google/android/speech/SpeechSettings;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
