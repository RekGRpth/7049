.class Lcom/google/android/voicesearch/intentapi/IntentApiActivity$4;
.super Ljava/lang/Object;
.source "IntentApiActivity.java"

# interfaces
.implements Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setIntentApiViewAndAttachCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$4;->this$0:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelRecordingClicked()V
    .locals 1

    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLogger;->recordClientEvent(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$4;->this$0:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    # getter for: Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lcom/google/android/voicesearch/fragments/IntentApiController;
    invoke-static {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->access$100(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;)Lcom/google/android/voicesearch/fragments/IntentApiController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->cancel()V

    return-void
.end method

.method public onStopRecordingClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity$4;->this$0:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    # getter for: Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lcom/google/android/voicesearch/fragments/IntentApiController;
    invoke-static {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->access$100(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;)Lcom/google/android/voicesearch/fragments/IntentApiController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/IntentApiController;->stopListening()V

    return-void
.end method
