.class public Lcom/google/android/voicesearch/intentapi/IntentApiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "IntentApiReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiReceiver;->isOrderedBroadcast()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "android.speech.action.GET_LANGUAGE_DETAILS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {p1}, Lcom/google/android/velvet/VelvetApplication;->fromContext(Landroid/content/Context;)Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getVoiceSearchServices()Lcom/google/android/voicesearch/VoiceSearchServices;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/voicesearch/VoiceSearchServices;->getSettings()Lcom/google/android/voicesearch/settings/Settings;

    move-result-object v1

    const-string v2, "android.speech.extra.LANGUAGE_PREFERENCE"

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getSpokenLocaleBcp47()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "android.speech.extra.ONLY_RETURN_LANGUAGE_PREFERENCE"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.speech.extra.SUPPORTED_LANGUAGES"

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getSupportedBcp47Locales(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v2, "android.speech.extra.SUPPORTED_LANGUAGE_NAMES"

    invoke-virtual {v1}, Lcom/google/android/voicesearch/settings/Settings;->getConfiguration()Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/speech/utils/SpokenLanguageUtils;->getSupportedDisplayNames(Lcom/google/wireless/voicesearch/proto/GstaticConfiguration$Configuration;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/intentapi/IntentApiReceiver;->setResultExtras(Landroid/os/Bundle;)V

    goto :goto_0
.end method
