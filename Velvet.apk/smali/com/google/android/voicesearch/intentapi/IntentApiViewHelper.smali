.class public Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;
.super Ljava/lang/Object;
.source "IntentApiViewHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;

.field private mCancelRecognitionClick:Landroid/view/View$OnClickListener;

.field private mLanguage:Landroid/widget/TextView;

.field private final mMicImage:Landroid/widget/ImageView;

.field private final mProgressBar:Landroid/widget/ProgressBar;

.field private final mPrompt:Landroid/widget/TextView;

.field private final mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

.field private mStopRecognitionClick:Landroid/view/View$OnClickListener;

.field private final mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f10013f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    const v0, 0x7f100147

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    const v0, 0x7f10014a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mPrompt:Landroid/widget/TextView;

    const v0, 0x7f100149

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f100148

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mLanguage:Landroid/widget/TextView;

    const v0, 0x7f100140

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mProgressBar:Landroid/widget/ProgressBar;

    new-instance v0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$1;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mStopRecognitionClick:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$2;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mCancelRecognitionClick:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;)Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;
    .locals 1
    .param p0    # Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mCallback:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;

    return-object v0
.end method


# virtual methods
.method public setCallback(Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;

    iput-object p1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mCallback:Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper$Callback;

    return-void
.end method

.method public setLanguage(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mLanguage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSpeechLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V
    .locals 1
    .param p1    # Lcom/google/android/speech/SpeechLevelSource;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setLevelSource(Lcom/google/android/speech/SpeechLevelSource;)V

    return-void
.end method

.method public setText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mPrompt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mPrompt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showInitializing()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mLanguage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    const v1, 0x7f0200bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mCancelRecognitionClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mPrompt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public showListening()V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0, v3}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mLanguage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    const v1, 0x7f020133

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mCancelRecognitionClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mPrompt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public showRecognizing()V
    .locals 3

    const/4 v1, 0x4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mLanguage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    const v1, 0x7f020130

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mCancelRecognitionClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mPrompt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public showRecording()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mSoundLevels:Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/ui/BitmapSoundLevelsView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mLanguage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    const v1, 0x7f020136

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mMicImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mStopRecognitionClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mPrompt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiViewHelper;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method
