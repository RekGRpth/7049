.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionTypeProcessor"
.end annotation


# instance fields
.field private final mAddActionType:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method private constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 4
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x1000005d

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x1000005e

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x1000006c

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x1000000d

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x10000032

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x1000000e

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x10000048

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x1000002c

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    const-wide/32 v1, 0x1000002d

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;)V
    .locals 0
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
    .param p3    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$1;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    return-void
.end method


# virtual methods
.method public internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 3
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v0

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$ActionTypeProcessor;->mAddActionType:Landroid/util/LongSparseArray;

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->group:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$500(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v1

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->type:I
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$600(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)I

    move-result v2

    or-int/2addr v1, v2

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setActionType(I)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
