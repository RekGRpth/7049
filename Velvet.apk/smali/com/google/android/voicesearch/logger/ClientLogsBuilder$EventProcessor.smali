.class abstract Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.super Ljava/lang/Object;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "EventProcessor"
.end annotation


# instance fields
.field private final mNextEventProcessor:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 0
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;->mNextEventProcessor:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    return-void
.end method


# virtual methods
.method abstract internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
.end method

.method public process(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 1
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;->internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;->mNextEventProcessor:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;->mNextEventProcessor:Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;->process(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
