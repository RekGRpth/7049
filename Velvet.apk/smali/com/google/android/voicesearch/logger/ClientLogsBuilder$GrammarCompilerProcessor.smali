.class Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;
.super Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;
.source "ClientLogsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/voicesearch/logger/ClientLogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GrammarCompilerProcessor"
.end annotation


# instance fields
.field private mContactsCount:I

.field final synthetic this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V
    .locals 1
    .param p2    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;

    iput-object p1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;->this$0:Lcom/google/android/voicesearch/logger/ClientLogsBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;-><init>(Lcom/google/android/voicesearch/logger/ClientLogsBuilder;Lcom/google/android/voicesearch/logger/ClientLogsBuilder$EventProcessor;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;->mContactsCount:I

    return-void
.end method


# virtual methods
.method public internalProcess(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;)Z
    .locals 2
    .param p1    # Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;
    .param p2    # Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    const/high16 v0, 0x20000000

    const/16 v1, 0xe

    invoke-virtual {p1, v0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->data:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->access$700(Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;->mContactsCount:I

    :cond_0
    iget v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;->mContactsCount:I

    if-lez v0, :cond_2

    const/high16 v0, 0x10000000

    const/16 v1, 0x52

    invoke-virtual {p1, v0, v1}, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$LoggedEvent;->is(II)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->hasLatency()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    invoke-direct {v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;-><init>()V

    invoke-virtual {p2, v0}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->setLatency(Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;)Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;

    :cond_1
    invoke-virtual {p2}, Lcom/google/speech/logs/VoicesearchClientLogProto$ClientEvent;->getLatency()Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    move-result-object v0

    iget v1, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;->mContactsCount:I

    invoke-virtual {v0, v1}, Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;->setFactor(I)Lcom/google/speech/logs/VoicesearchClientLogProto$LatencyData;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/voicesearch/logger/ClientLogsBuilder$GrammarCompilerProcessor;->mContactsCount:I

    :cond_2
    const/4 v0, 0x0

    return v0
.end method
